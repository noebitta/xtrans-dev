<?php
/**
 * Log Sms controller.
 *
 * Last updated: 3/23/15, 2:39 PM
 *
 * @author Sopyan Adicandra Ramandani <sopyan.adicandra@gmail.com>
 *
 */

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){
    redirect('index.'.$phpEx,true);
}
//#############################################################################
// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'log_sms/log_sms_body.tpl'));

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai)." 00:00:00";
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir)." 23:59:59";


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"IdLogSms","tbl_log_sms",
    "&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir",
    "WHERE (WaktuKirim BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')" ,"log_sms.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

function sendSms($no_hp,$pesan){
    $userKey 	= 'o23tc4';
    $passKey 	= 'orionaja';
    return sendHttpPost('http://reguler.sms-notifikasi.com/apps/smsapi.php','userkey='.$userKey.'&passkey='.$passKey.'&nohp='.$no_hp.'&pesan='.$pesan);
    //return sendHttpPost();
}

switch($mode){
    case "kirimulang":
        $id     = $HTTP_GET_VARS['id'];
        $sql    = "SELECT * FROM tbl_log_sms WHERE IdLogSms = $id";
        if(!$result = $db->sql_query($sql)){
            echo("Err:".__LINE__);exit;
        }
        $data   = $db->sql_fetchrow($result);

        $res    = sendSms($data['NoTujuan'],$data['Pesan']);

        if($res){
            echo "1";
        }else{
            echo "2";
        }

        exit;

}


$sql	=
    "SELECT
		*
	FROM tbl_log_sms
	WHERE (WaktuKirim BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	ORDER BY WaktuKirim DESC LIMIT $idx_awal_record,$VIEW_PER_PAGE;
	";


if(!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}


$i=1;

while ($row = $db->sql_fetchrow($result)){
    $odd ='odd';
    if (($i % 2)==0){
        $odd = 'even';
    }
    if(strpos($row['Responses'],'Success')){
        $status = 'Sukses';
        $act    = '<a href="#" onclick="return kirimUlang('.$row['IdLogSms'].')">Kirim Ulang</a>';
    }else{
        $status = 'Gagal';
        $act    = '';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'=>$odd,
            'no'=>$i+$idx_page*$VIEW_PER_PAGE,
            'no_tujuan'=>$row['NoTujuan'],
            'nama_penerima'=>$row['NamaPenerima'],
            'waktu_kirim'=>$row['WaktuKirim'],
            'responses'=>$row['Responses'],
            'pesan'=>$row['Pesan'],
            'act'=>$act,
            'status'=>$status
        )
    );
    $i++;
}

$template->assign_vars(array(
        'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('log_sms.'.$phpEx).'">Log SMS</a>',
        'PAGING'		=> $paging,
        'TGL_AWAL'		=> $tanggal_mulai,
        'TGL_AKHIR'		=> $tanggal_akhir,
        'TXT_CARI'		=> $cari,
    )
);

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>