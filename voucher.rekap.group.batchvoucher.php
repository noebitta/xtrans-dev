<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassVoucherDiskon.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$id_group 	= isset($HTTP_GET_VARS['idgroup'])? $HTTP_GET_VARS['idgroup'] : $HTTP_POST_VARS['idgroup'];

if($id_group==""){
	echo("ILEGAL ACCESS!");
	exit;
}

$bcrump	= "<a href=\"".append_sid("main.".$phpEx) ."#voucherdiskon\">Home</a> | <a href=\"".append_sid("voucher.rekap.group.php") ."\">Rekap Voucher Diskon</a> | <a href=\"".append_sid(basename(__FILE__)."?idgroup=$id_group")."\">Daftar Voucher</a>";

function setComboCabangTujuan($cabang_asal){
	//SET COMBO cabang
	global $db;
	
	$Jurusan = new Jurusan();
			
	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal);
	$opt_cabang="<option value=''>silahkan pilih...</otpion>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

$Voucher	= new Voucher();

$mode	= $mode==""?"exp":$mode;

switch($mode){
	
	case "exp":
		// LIST
		$tgl_awal  	= isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
		$tgl_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
		$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
				
		$Cabang	= new Cabang();
		
		$kondisi	=($cari=="")?"":
			" AND (Keterangan LIKE '%$cari%'
			OR KodeVoucher LIKE '%$cari'
			OR NamaPetugasPencetak LIKE '%$cari%'
			OR NamaPenumpang LIKE '%$cari%'
			OR Telp LIKE '%$cari%'
			OR NoTiketBerangkat LIKE '%$cari%')";
			
		$sql = 
			"SELECT *
			FROM tbl_voucher
			WHERE IdGroup='$id_group' $kondisi AND (WaktuDigunakan BETWEEN '$tgl_awal 00:00:00' AND '$tgl_akhir 23:59:59')
			ORDER BY WaktuDigunakan";
		
		$idx_check=0;
		
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);exit;
		}
		
		$i = $idx_page*$VIEW_PER_PAGE+1;
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
			
			if (($i % 2)==0){
				$odd = 'even';
			}
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'no'=>$i,
						'kodevoucher'=>$row["KodeVoucher"],
						'waktubuat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetak'])),
						'dibuatoleh'=>$row['NamaPetugasPencetak'],
						'digunakan'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuDigunakan'])),
						'notiket'=>$row["NoTiketBerangkat"],
						'jadwal'=>$row["KodeJadwalBerangkat"],
						'penumpang'=>$row["NamaPenumpang"],
						'telp'=>$row["Telp"],
						'nilai'=>number_format($row["NilaiVoucher"],0,",","."),
						'cso'=>$row["NamaPetugasPengguna"],
						'keterangan'=>$row['Keterangan']
					)
				);
			
			$i++;
		}
		
		if($i-1<=0){
			$template->assign_block_vars("NO_DATA",array());
		}
		
		$data_group	= $Voucher->getDetailGroup($id_group);
		
		$template->set_filenames(array('body' => 'voucher.rekap.group/batchvoucher.tpl')); 
		$template->assign_vars(array(
			'BCRUMP'    		=> $bcrump,
			'NAMA_GROUP'		=> $data_group["NamaGroup"],
			'ACTION_CARI'		=> append_sid(basename(__FILE__."?idgroup=$id_group")),
			'ID_GROUP'			=> $id_group,
			'TGL_AWAL'			=> $tgl_awal,
			'TGL_AKHIR'			=> $tgl_akhir,
			'CARI'					=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
	break;
			
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>