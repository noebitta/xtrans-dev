<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

function setComboCabang($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	$Cabang	= new Cabang();
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	
	$opt_cabang	="<option value=''>- Semua Cabang -</option>".$opt_cabang;
	
	return $opt_cabang;
	//END SET COMBO CABANG
}


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_kendaraan/laporan_omzet_kendaraan_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND CetakSPJ=1 AND tr.KodeKendaraan=tmk.KodeKendaraan";

$kondisi_cabang	=($kode_cabang=="")?"":" AND tmk.KodeCabang='$kode_cabang' ";
		
$kondisi_cari	=($cari=="")?
	" WHERE tmk.KodeKendaraan LIKE '%' $kondisi_cabang":
	" WHERE (tmk.KodeKendaraan LIKE '$cari%') $kondisi_cabang";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tmk.KodeKendaraan":$sort_by;
	
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tmk.KodeKendaraan","tbl_md_kendaraan tmk",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&cabang=$kode_cabang&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_omzet_kendaraan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql_total_jalan	= "(SELECT COUNT(DISTINCT KodeJadwal,TglBerangkat) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_penumpang	= "(SELECT IS_NULL(COUNT(NoTiket),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_biaya			= "(SELECT IS_NULL(SUM(Jumlah),0) FROM tbl_biaya_op WHERE NoPolisi=tmk.KodeKendaraan 
													AND (TglTransaksi BETWEEN '$tanggal_mulai_mysql' 
													AND '$tanggal_akhir_mysql'))";
$sql_total_omzet					= "(SELECT IS_NULL(SUM(SubTotal),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_kursi			= "(SELECT (SUM(f_jadwal_ambil_jumlah_kursi_by_kode_jadwal(KodeJadwal))/COUNT(KodeKendaraan))*$sql_total_jalan FROM tbl_reservasi_olap tr $kondisi)";
$sql=
	"SELECT 
		tmk.KodeKendaraan,f_cabang_get_name_by_kode(tmk.KodeCabang) AS Cabang,
		JumlahKursi AS LayoutKursi,
		$sql_total_jalan AS Jalan,
		$sql_total_penumpang  AS TotalPenumpang,
		$sql_total_omzet AS TotalOmzet,
		$sql_total_biaya AS TotalBiaya,
		$sql_total_omzet-$sql_total_biaya AS TotalProfit,
		100*$sql_total_penumpang/$sql_total_kursi AS produktifitas
	FROM tbl_md_kendaraan tmk
	$kondisi_cari
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

$temp_tgl	= explode("-",$tanggal_mulai);

$bulan	= $temp_tgl[1];
$tahun	= $temp_tgl[2];
	
if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		//$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_omzet_kendaraan_grafik.'.$phpEx).'&tanggal='.$tanggal_mulai.'&no_polisi='.$row['KodeKendaraan']."\");return false'>Grafik<a/>";
		$act 	="<a href='".append_sid('laporan_omzet_kendaraan_grafik.'.$phpEx).'&no_polisi='.$row['KodeKendaraan'].'&bulan='.$bulan.'&tahun='.$tahun.
						'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$kode_cabang.'&sort_by='.$sort_by.'&order='.$order."'>Grafik<a/>";
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'no_polisi'=>$row['KodeKendaraan'],
					'layout_kursi'=>number_format($row['LayoutKursi'],0,",","."),
					'cabang'=>$row['Cabang'],
					'total_jalan'=>number_format($row['Jalan'],0,",","."),
					'total_penumpang'=>number_format($row['TotalPenumpang'],0,",","."),
					'total_omzet'=>number_format($row['TotalOmzet'],0,",","."),
					'total_biaya'=>number_format($row['TotalBiaya'],0,",","."),
					'total_profit'=>number_format($row['TotalProfit'],0,",","."),
					'produktifitas'=>number_format($row['produktifitas'],0,",","."),
					'act'=>$act
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$temp_var		= "select_".$sort_by;
$$temp_var	= "selected";

$opt_sort_by=
	"<option value='KodeKendaraan' $select_Kode>No.Polisi</option>
	<option value='Cabang'$select_Cabang>Cabang</option>
	<option value='Jalan' $select_Jalan>Total Jalan</option>
	<option value='TotalPenumpang' $select_TotalPenumpang>Total Penumpang</option>
	<option value='TotalOmzet' $select_TotalOmzet>Total Omzet</option>
	<option value='TotalBiaya' $select_TotalBiaya>Total Biaya</option>
	<option value='TotalProfit' $select_TotalProfit>Total Profit</option>
	<option value='produktifitas' $select_produktifitas>Produktifitas</option>";
	
$temp_var		= "select_".$order;
$$temp_var	= "selected";

$opt_order=
	"<option value='asc' $select_asc>Menaik</option>
	<option value='desc' $select_desc>Menurun</option>";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$kode_cabang.
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
	
$script_cetak_pdf="Start('laporan_omzet_kendaraan_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_omzet_kendaraan_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT
	
$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_omzet">Home</a> | <a href="'.append_sid('laporan_omzet_kendaraan.'.$phpEx).'">Laporan Omzet Kendaraan</a>',
	'ACTION_CARI'		=> append_sid('laporan_omzet_kendaraan.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_CABANG'		=> setComboCabang($kode_cabang),
	'OPT_SORT'			=> $opt_sort_by,
	'OPT_ORDER'			=> $opt_order,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>