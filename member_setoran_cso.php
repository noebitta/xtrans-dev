<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberSetoranCso.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act   	 = $HTTP_GET_VARS['act'];
$pesan   = $HTTP_GET_VARS['pesan'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$SetoranTopUpCSO	= new SetoranTopUpCSO();

switch($mode){

//TAMPILKAN SETORAN==========================================================================================================
case 'tampilkan_data':
	
	$sortby    	= $HTTP_GET_VARS['sortby'];
	$ascending	= $HTTP_GET_VARS['ascending'];
	$ascending	= ($ascending==0)?"asc":"desc";
	
	$cari 			= $HTTP_GET_VARS['cari'];
	
	//HEADER TABEL
	$hasil ="
		<table width='100%' class='border'>
    <tr>
      <th width='3%'>#</th>
			<th width='20%'><a href='#' onClick='setOrder(\"waktu_terakhir_transaksi\")'>
				<font color='white'>Waktu Transaksi Terakhir</font></a></th>
			
			<th width='10%'><a href='#' onClick='setOrder(\"username\")'>
				<font color='white'>Username</font></a></th>
			
			<th width='10%'><a href='#' onClick='setOrder(\"nama\")'>
				<font color='white'>Nama</font></a></th>
			
			<th width='20%'><a href='#' onClick='setOrder(\"Jumlah_harus_setor\")'>
				<font color='white'>Uang Harus Disetor (Rp.)</font></a></th>
						
			<th width='10%'>Aksi</th>
    </tr>";
    
	$result	= $SetoranTopUpCSO->ambilData($cari,$sortby,$ascending);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}

		$action = 
			"<input type='button' value='&nbsp;&nbsp;SETORAN&nbsp;&nbsp;&nbsp;' 
				onClick=\"tampilkanDataDetail('".trim($row['username'])."','".trim($row['nama'])."','".trim($row['jumlah_harus_setor'])."','".trim($row['outstanding'])."');\" />";
		
	
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[waktu_terakhir_transaksi]</td>
      <td class='$odd'>$row[username]</td>
      <td class='$odd'>$row[nama]</td>
      <td class='$odd' align='right'>".number_format($row['jumlah_harus_setor'],0,",",".")."</td>
			<td class='$odd' align='center'>$action</td>
    </tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=6 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

//TAMPILKAN SETORAN DETAIL=========================================================================================================
case 'tampilkan_data_detail':

	$operator						= $HTTP_GET_VARS['operator'];
	$nama								= $HTTP_GET_VARS['nama'];
	$uang_harus_setor		= $HTTP_GET_VARS['uang_harus_setor'];
	$uang_outstanding		= $HTTP_GET_VARS['uang_outstanding'];

	
	//HEADER TABEL
	$hasil ="
		<input type='hidden' id='uang_harus_setor' value=$uang_harus_setor />
		<input type='hidden' id='operator' value=$operator />
		<table width='100%'>
			<tr><td width='20%'>Username Operator</td><td width='3%'>:</td><td width='15%'>$operator</td><td width='60%'></td></tr>
			<tr><td>Nama Operator</td><td>:</td><td>$nama</td></tr>
			<tr><td>Uang yang harus disetor</td><td>:</td><td align='right'>".number_format($uang_harus_setor,0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
			<tr><td>Uang Out Standing</td><td>:</td><td align='right'>".number_format($uang_outstanding,0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
			<tr><td colspan=4>Klik disini untuk proses penyetoran --> <input type='button' value='PROSES SETOR' onClick='konfirmasi();'/></td></tr
		</table>
		<table width='100%' class='border'>
    <tr>
      <th width='3%'>#</th>
			<th width='20%'><a href='#'>
				<font color='white'>Waktu Transaksi</font></a></th>
			
			<th width='20%'><a href='#'>
				<font color='white'>ID Top Up</font></a></th>
			
			<th width='20%'><a href='#'>
				<font color='white'>ID Member</font></a></th>
			
			<th width='20%'><a href='#'>
				<font color='white'>Jumlah Top Up (Rp.)</font></a></th>
				
    </tr>";
    
	$result	= $SetoranTopUpCSO->ambilDataDetailTopup($operator);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$action = 
			"<input type='button' value='&nbsp;&nbsp;SETOR&nbsp;&nbsp;&nbsp;' onClick='document.location=\"".append_sid('member_setoran_cso.'.$phpEx)."&username=$row[username]&mode=tampilkan_data_detail"."\"' />";
				
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[waktu_topup]</td>
      <td class='$odd'>$row[id_topup]</td>
      <td class='$odd'>$row[id_member]</td>
      <td class='$odd' align='right'>".number_format($row['jumlah_topup'],0,",",".")."</td>
    </tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=5 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

//PROSES SETOR========================================================================================
case 'setor':
	$total_setor	= $HTTP_GET_VARS['total_setor'];		
	$operator			= $HTTP_GET_VARS['operator'];		
		
	//proses setoran
	$kode_posting	= $SetoranTopUpCSO->setorUangTopUp($total_setor,$total_setor,$operator);
	
	echo($kode_posting);
	
exit;

}//switch mode


$template->set_filenames(array('body' => 'member_setoran_cso.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  	=>$userdata['username'],
   	'BCRUMP'    	=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_setoran_cso.'.$phpEx).'">Setoran Top Up Member</a>',
		'PESAN'				=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>