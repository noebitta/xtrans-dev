<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR,$LEVEL_STAFF_KEUANGAN_PAKET,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

//$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

// LIST
$template->set_filenames(array('body' => 'laporan.paket/paket.perjurusan.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

//$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeJurusan LIKE '$cari%'
		OR KodeCabangAsal LIKE '%$cari%'
		OR KodeCabangTujuan LIKE '%$cari%'
		OR KodeArea LIKE '$cari%'
		OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%'
		OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabangAsal'$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Jurusan":$sort_by;
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeJurusan","tbl_md_jurusan",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan.paket.perjurusan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		IdJurusan,KodeJurusan,
		CONCAT(f_cabang_get_name_by_kode(KodeCabangAsal),'-',f_cabang_get_name_by_kode(KodeCabangTujuan)) AS Jurusan
	FROM tbl_md_jurusan
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF(Layanan='P',1,NULL)),0) AS TotalPaketP,
		IS_NULL(COUNT(IF(Layanan='GD',1,NULL)),0) AS TotalPaketGD,
		IS_NULL(COUNT(IF(Layanan='GA',1,NULL)),0) AS TotalPaketGA,
		IS_NULL(COUNT(IF(Layanan='S',1,NULL)),0) AS TotalPaketS,
		IS_NULL(COUNT(IF(Layanan='CA',1,NULL)),0) AS TotalPaketCA,
		IS_NULL(COUNT(IF(Layanan='CD',1,NULL)),0) AS TotalPaketCD,
		IS_NULL(COUNT(IF(Layanan='I',1,NULL)),0) AS TotalPaketI,
		IS_NULL(SUM(IF(Layanan='P',HargaPaket,0)),0) AS OmzPaketP,
		IS_NULL(SUM(IF(Layanan='GD',HargaPaket,0)),0) AS OmzPaketGD,
		IS_NULL(SUM(IF(Layanan='GA',HargaPaket,0)),0) AS OmzPaketGA,
		IS_NULL(SUM(IF(Layanan='S',HargaPaket,0)),0) AS OmzPaketS,
		IS_NULL(SUM(IF(Layanan='CA',HargaPaket,0)),0) AS OmzPaketCA,
		IS_NULL(SUM(IF(Layanan='CD',HargaPaket,0)),0) AS OmzPaketCD,
		IS_NULL(SUM(IF(Layanan='I',HargaPaket,0)),0) AS OmzPaketI,
		IS_NULL(SUM(IF(JenisPembayaran=0,TotalBayar,0)),0) AS OmzTunai,
		IS_NULL(SUM(IF(JenisPembayaran!=0,TotalBayar,0)),0) AS OmzLangganan,
		IS_NULL(SUM(TotalBayar),0) AS TotalPenjualanPaket
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan),f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//debug
//echo($sql);exit;

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['IdJurusan']		= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']	= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']			= $row['Jurusan'];
	$temp_array[$idx]['TotalPaketP']	= $data_paket_total[$row['IdJurusan']]['TotalPaketP'];
	$temp_array[$idx]['TotalPaketGD']	= $data_paket_total[$row['IdJurusan']]['TotalPaketGD'];
	$temp_array[$idx]['TotalPaketGA']	= $data_paket_total[$row['IdJurusan']]['TotalPaketGA'];
	$temp_array[$idx]['TotalPaketS']	= $data_paket_total[$row['IdJurusan']]['TotalPaketS'];
	$temp_array[$idx]['TotalPaketCA']	= $data_paket_total[$row['IdJurusan']]['TotalPaketCA'];
	$temp_array[$idx]['TotalPaketCD']	= $data_paket_total[$row['IdJurusan']]['TotalPaketCD'];
	$temp_array[$idx]['TotalPaketI']	= $data_paket_total[$row['IdJurusan']]['TotalPaketI'];
	$temp_array[$idx]['OmzPaketP']		= $data_paket_total[$row['IdJurusan']]['OmzPaketP'];
	$temp_array[$idx]['OmzPaketGD']		= $data_paket_total[$row['IdJurusan']]['OmzPaketGD'];
	$temp_array[$idx]['OmzPaketGA']		= $data_paket_total[$row['IdJurusan']]['OmzPaketGA'];
	$temp_array[$idx]['OmzPaketS']		= $data_paket_total[$row['IdJurusan']]['OmzPaketS'];
	$temp_array[$idx]['OmzPaketCA']		= $data_paket_total[$row['IdJurusan']]['OmzPaketCA'];
	$temp_array[$idx]['OmzPaketCD']		= $data_paket_total[$row['IdJurusan']]['OmzPaketCD'];
	$temp_array[$idx]['OmzPaketI']		= $data_paket_total[$row['IdJurusan']]['OmzPaketI'];
	$temp_array[$idx]['OmzTunai']			= $data_paket_total[$row['IdJurusan']]['OmzTunai'];
	$temp_array[$idx]['OmzLangganan']	= $data_paket_total[$row['IdJurusan']]['OmzLangganan'];
	$temp_array[$idx]['OmzTotal']			= $data_paket_total[$row['IdJurusan']]['TotalPenjualanPaket'];
		
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan.paket.detail.php?tglmulai='.$tanggal_mulai.'&tglakhir='.$tanggal_akhir.'&paramfilter='.$temp_array[$idx]['IdJurusan'].'&tipe=jurusan&ket='.$temp_array[$idx]['Jurusan'].'&is_today='.$is_today)."\");return false'>Detail<a/>";		
	
	/*$act 	.="<a href='".append_sid('laporan.paket.perjurusan_grafik.'.$phpEx).'&kode_cabang='.$temp_array[$idx]['KodeCabang'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&sort_by='.$sort_by.'&order='.$order."'>Grafik<a/>";		
	*/

	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total
	$total									= $temp_array[$idx]['Total'];
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'						=>$odd,
				'no'						=>$idx+1,
				'jurusan'				=>$temp_array[$idx]['Jurusan']." (".$temp_array[$idx]['KodeJurusan'].")",
				'total_paket_p'	=>number_format($temp_array[$idx]['TotalPaketP'],0,",","."),
				'total_paket_gd'=>number_format($temp_array[$idx]['TotalPaketGD'],0,",","."),
				'total_paket_ga'=>number_format($temp_array[$idx]['TotalPaketGA'],0,",","."),
				'total_paket_s'	=>number_format($temp_array[$idx]['TotalPaketS'],0,",","."),
				'total_paket_ca'=>number_format($temp_array[$idx]['TotalPaketCA'],0,",","."),
				'total_paket_cd'=>number_format($temp_array[$idx]['TotalPaketCD'],0,",","."),
				'total_paket_i'=>number_format($temp_array[$idx]['TotalPaketI'],0,",","."),
				'omz_paket_p'		=>number_format($temp_array[$idx]['OmzPaketP'],0,",","."),
				'omz_paket_gd'	=>number_format($temp_array[$idx]['OmzPaketGD'],0,",","."),
				'omz_paket_ga'	=>number_format($temp_array[$idx]['OmzPaketGA'],0,",","."),
				'omz_paket_s'		=>number_format($temp_array[$idx]['OmzPaketS'],0,",","."),
				'omz_paket_ca'	=>number_format($temp_array[$idx]['OmzPaketCA'],0,",","."),
				'omz_paket_cd'	=>number_format($temp_array[$idx]['OmzPaketCD'],0,",","."),
				'omz_paket_i'	  =>number_format($temp_array[$idx]['OmzPaketI'],0,",","."),
				'omz_tunai'			=>number_format($temp_array[$idx]['OmzTunai'],0,",","."),
				'omz_langganan'	=>number_format($temp_array[$idx]['OmzLangganan'],0,",","."),
				'omz_total'			=>number_format($temp_array[$idx]['OmzTotal'],0,",","."),
				'act'						=>$act
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&is_today=$is_today&tglawal=".$tanggal_mulai_mysql."&tglakhir=".$tanggal_akhir_mysql.
										"&cari=".$cari."&sortby=".$sort_by."&order=".$order."";
													
$script_cetak_excel="Start('laporan.paket.perjurusan.cetak.excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "is_today".($is_today==""?"1":$is_today);
$$temp_var	= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_paket">Home</a> | <a href="'.append_sid('laporan.paket.perjurusan.'.$phpEx).'">Laporan Omzet Paket per Jurusan</a>',
	'ACTION_CARI'		=> append_sid('laporan.paket.perjurusan.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'IS_TODAY1'			=> $is_today1,
	'IS_TODAY0'			=> $is_today0,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['Nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=Jurusan'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Jurusan ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=TotalPaketP'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan Total paket platinum ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=TotalPaketGD'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Total paket gold diantar ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=TotalPaketGA'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Total paket gold diambil ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=TotalPaketS'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Total paket silver ($order_invert)",
	'A_SORT_9'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=TotalPaketCA'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan Total paket cargo antar($order_invert)",
	'A_SORT_10'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzPaketP'.$parameter_sorting),
	'TIPS_SORT_10'	=> "Urutkan Total omzet paket platinum ($order_invert)",
	'A_SORT_11'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzPaketGD'.$parameter_sorting),
	'TIPS_SORT_11'	=> "Urutkan Total omzet paket gold diantar ($order_invert)",
	'A_SORT_12'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzPaketGA'.$parameter_sorting),
	'TIPS_SORT_12'	=> "Urutkan Total omzet paket gold diambil ($order_invert)",
	'A_SORT_13'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzPaketS'.$parameter_sorting),
	'TIPS_SORT_13'	=> "Urutkan Total omzet paket silver ($order_invert)",
	'A_SORT_14'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzPaketCA'.$parameter_sorting),
	'TIPS_SORT_14'	=> "Urutkan Total omzet paket cargo antar ($order_invert)",
	'A_SORT_15'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzTunai'.$parameter_sorting),
	'TIPS_SORT_15'	=> "Urutkan Total omzet tunai ($order_invert)",
	'A_SORT_16'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzLangganan'.$parameter_sorting),
	'TIPS_SORT_16'	=> "Urutkan Total omzet langganan ($order_invert)",
	'A_SORT_17'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzTotal'.$parameter_sorting),
	'TIPS_SORT_17'	=> "Urutkan Total ($order_invert)",
	'A_SORT_18'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=TotalPaketCD'.$parameter_sorting),
	'TIPS_SORT_18'	=> "Urutkan Total paket cargo diambil ($order_invert)",
	'A_SORT_19'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzPaketCD'.$parameter_sorting),
	'TIPS_SORT_19'	=> "Urutkan Total omzet paket cargo diambil ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>