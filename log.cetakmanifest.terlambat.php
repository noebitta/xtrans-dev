<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Jurusan= new Jurusan();
$Cabang	= new Cabang();

function setComboCabangAsal($kota,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->setComboCabang($kota);
	$opt_cabang="<option value='0'>-tampilkan semua-</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;
			
	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal);
	$opt_cabang="<option value='0'>-tampilkan semua-</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		//echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboJam($id_jurusan,$jam_dipilih){
	//SET COMBO JAM
	
	global $db;
		
	$sql = 
		"SELECT KodeJadwal,JamBerangkat
		  FROM tbl_md_jadwal tmj
		  WHERE IdJurusan=$id_jurusan 
		  ORDER BY JamBerangkat;";
			
	if (!$result = $db->sql_query($sql)){
		//die_error("Gagal $this->ID_FILE 003");
		#echo("Err: $this->ID_FILE $sql". __LINE__);
	}
	
	$opt_jam="<option value='0'>-tampilkan semua-</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($jam_dipilih!=$row['KodeJadwal'])?"":"selected";
			$opt_jam .="<option value='$row[KodeJadwal]' $selected>$row[KodeJadwal] ($row[JamBerangkat])</option>";
		}
	}
	else{
		//echo("Error :".__LINE__);exit;
	}		
	return $opt_jam;
	//END SET COMBO JAM
}

//ACTION OPERATION========================
if ($mode=='get_asal'){

	$kota				= $HTTP_GET_VARS['kota'];
	$cabang_asal= $HTTP_GET_VARS['asal'];
	
	$opt_cabang_tujuan=
		"<select id='opt_cabang_asal' name='opt_cabang_asal' onChange='getUpdateTujuan(this.value)'>".
		setComboCabangAsal($kota,$cabang_asal)
		."</select>";
	
	echo($opt_cabang_tujuan);
	
	exit;
}
else if ($mode=='get_tujuan'){
	$cabang_asal		= $HTTP_GET_VARS['asal'];
	$id_jurusan			= $HTTP_GET_VARS['jurusan'];
	
	$opt_cabang_tujuan=
		"<select id='opt_tujuan' name='opt_tujuan' onChange='getUpdateJam(this.value)'>".
		setComboCabangTujuan($cabang_asal,$id_jurusan)
		."</select>";
	
	echo($opt_cabang_tujuan);
	
	exit;
}
else if ($mode=='get_jam'){
	$id_jurusan	= $HTTP_GET_VARS['jurusan'];
	$jam_dipilih= $HTTP_GET_VARS['jam'];
	
	$opt_cabang_jam=
		"<select id='opt_jam' name='opt_jam'>".
		setComboJam($id_jurusan,$jam_dipilih)
		."</select>";
	
	echo($opt_cabang_jam);
	
	exit;
}

//VIEW MODE========================
$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
$bulan					= isset($HTTP_GET_VARS['opt_bulan'])? $HTTP_GET_VARS['opt_bulan'] : $HTTP_POST_VARS['opt_bulan']; 
$tahun					= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun']; 
$kota_asal			= isset($HTTP_GET_VARS['kota_asal'])? $HTTP_GET_VARS['kota_asal'] : $HTTP_POST_VARS['kota_asal']; 
$cabang_asal		= isset($HTTP_GET_VARS['opt_cabang_asal'])? $HTTP_GET_VARS['opt_cabang_asal'] : $HTTP_POST_VARS['opt_cabang_asal'];
$cabang_tujuan	= isset($HTTP_GET_VARS['opt_tujuan'])? $HTTP_GET_VARS['opt_tujuan'] : $HTTP_POST_VARS['opt_tujuan']; 
$kode_jadwal		= isset($HTTP_GET_VARS['opt_jam'])? $HTTP_GET_VARS['opt_jam'] : $HTTP_POST_VARS['opt_jam']; 
$status					= isset($HTTP_GET_VARS['opt_status'])? $HTTP_GET_VARS['opt_status'] : $HTTP_POST_VARS['opt_status']; 
$is_cari				= isset($HTTP_GET_VARS['is_cari'])? $HTTP_GET_VARS['is_cari'] : $HTTP_POST_VARS['is_cari']; 
$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 

//pengaturan periode
if($tgl_awal>$tgl_akhir){
	$temp_tgl		= $tgl_akhir;
	$tgl_akhir	= $tgl_awal;
	$tgl_awal		= $temp_tgl;
}

$kota_asal	= ($kota_asal!="")?$kota_asal:"BANDUNG";

$is_cari= ($tahun=="")?"1":$is_cari;
$tahun	= ($tahun=="")?date("y"):$tahun;
$bulan	= ($bulan=="")?substr('0'.date("m"),-2):$bulan;

$tgl_maximum	= getMaxDate($bulan,'20'.$tahun);

if($tgl_awal==""){
	$tgl_awal	= date("d");
}
else if($tgl_awal>$tgl_maximum){
	$tgl_awal	= $tgl_maximum;
}

if($tgl_akhir==""){
	$tgl_akhir	= date("d");
}
else if($tgl_akhir>$tgl_maximum){
	$tgl_akhir	= $tgl_maximum;
}

$tgl_awal_mysql	= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_awal,-2);
$tgl_akhir_mysql= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_akhir,-2);
//end pemgaturan periode

//membuat view untuk cabang-cabang per kota sesuai dengan kota yang dikehendaki
if($is_cari=="1"){
		
	//create view
	$sql = 
		"CREATE OR REPLACE VIEW view_list_jurusan_by_kota_".str_replace("/","_",$kota_asal)." AS 
		SELECT tmj.IdJurusan 
		FROM tbl_md_jurusan tmj 
		INNER JOIN tbl_md_cabang tmc ON tmj.KodeCabangAsal=tmc.KodeCabang 
		WHERE Kota='$kota_asal'";
	
	if (!$result = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
}

//mengambil list id jurusan by kota
$sql	= "SELECT IdJurusan FROM view_list_jurusan_by_kota_".str_replace("/","_",$kota_asal)." ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
} 

$list_id_jurusan ="";

while ($row = $db->sql_fetchrow($result)){
	$list_id_jurusan .=$row[0].",";
}
$list_id_jurusan	= substr($list_id_jurusan,0,-1);
//END mengambil list id jurusan by kota

$template->set_filenames(array('body' => 'log.cetakmanifest.terlambat/index.tpl'));

$kondisi	= " WHERE (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND CetakanKe = 1 AND (TIMEDIFF(WaktuCetak,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) >= '00:10:00')";
$kondisi	.=" AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang=(f_jurusan_get_kode_cabang_asal_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal(KodeJadwal))))='$kota_asal'";
$kondisi .= ($cari=="")?"":" AND (NoSPJ LIKE '%$cari' OR KodeJadwal LIKE '%$cari%' OR NoPolisi LIKE '%$cari%' OR Driver LIKE '%$cari%' OR KodeDriver LIKE '%$cari%' OR NamaUserPencetak LIKE '%$cari%')";

if($cabang_asal=='0'){
  $kondisi	.= "";
}
else{

  if($cabang_tujuan=='0'){
    $kondisi	.= " AND (SELECT KodeCabangAsal FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal(t1.KodeJadwal)) LIKE '$cabang_asal' ";
  }
  else{
    if($kode_jadwal=='0'){
      $kondisi	.= " AND f_jadwal_ambil_id_jurusan_by_kode_jadwal(t1.KodeJadwal) LIKE '$cabang_tujuan' ";
    }
    else{
      $kondisi	.= " AND t1.KodeJadwal LIKE '$kode_jadwal' ";
    }
  }
}

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"","tbl_log_cetak_manifest t1",
						"&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&opt_bulan=$bulan&tahun=$tahun&kota_asal=$kota_asal&opt_cabang_asal=$cabang_asal&opt_tujuan=$cabang_tujuan&opt_jam=$kode_jadwal&cari=$cari",
						$kondisi,"log.cetakmanifest.terlambat.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql = 
	"SELECT *,
			TIMEDIFF(WaktuCetak,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) AS Keterlambatan,
			IF(TIMEDIFF(WaktuCetak,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) >'00:10:00',1,0) IsTerlambat
	FROM tbl_log_cetak_manifest t1
	$kondisi
	ORDER BY TglBerangkat DESC, KodeJadwal ASC, JamBerangkat DESC, NoSPJ ASC, CetakanKe DESC LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
					
	if (($i % 2)==0){
		$odd = 'even';
	}

	if($row['IsTerlambat']==1){
		$flagterlambat = "red";
		$lama_terlambat	= $row['Keterlambatan'];
	}
	else{
		$flagterlambat	= "";
		$lama_terlambat	= "";
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'						=>$odd,
				'no'						=>$i,
				'no_spj'				=>$row['NoSPJ'],
				'tgl_berangkat'	=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])),
				'jam'						=>$row['JamBerangkat'],
				'kode_jadwal'		=>$row['KodeJadwal'],
				'layout'				=>$row['JumlahKursiDisediakan'],
				'kendaraan'			=>$row['NoPolisi'],
				'sopir'					=>$row['Driver']."(".$row['KodeDriver'].")",
				'pnp'						=>$row['JumlahPenumpang'],
				'pkt'						=>$row['JumlahPaket'],
				'omz_pnp'				=>number_format($row['TotalOmzet'],0,",","."),
				'omz_pkt'				=>number_format($row['TotalOmzetPaket'],0,",","."),
				'cetakan_ke'		=>$row['CetakanKe']+1,
				'jenis'					=>$row['IsSubJadwal']!=1?"UTAMA":"TRANSIT",
				'pencetak'			=>$row['NamaUserPencetak'],
				'flagterlambat'=>$flagterlambat,
				'keterlambatan'=>$lama_terlambat,
				'waktu_cetak'		=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuCetak']))
			)
		);
	
	$i++;
}


//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tgl_awal=".$tgl_awal_mysql."&tgl_akhir=".$tgl_akhir_mysql."&id_jurusan=".$cabang_tujuan."&kota_asal=".$kota_asal."&cabang_asal=".$cabang_asal."&jam=".$kode_jadwal."&cari=".$cari;
$script_cetak_excel="Start('log.manifest.terlambat.cetak.excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//END KOMPONEN UNTUK EXPORT	

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#log_reservasi">Home</a> | <a href="'.append_sid('log.cetakmanifest.'.$phpEx).'">Log Cetak Manifest Terlambat</a>',
	'CETAK_XL'    	=> $script_cetak_excel,
	'ACTION_CARI'		=> append_sid('log.cetakmanifest.terlambat.'.$phpEx),
	'KOTA'					=> $kota_asal,
	'OPT_KOTA'			=> setComboKota($kota_asal),
  'ASAL'					=> $cabang_asal,
  'ID_JURUSAN'		=> $cabang_tujuan,
  'KODE_JADWAL'		=> $kode_jadwal,
	'TGL_AWAL'			=> $tgl_awal,
	'TGL_AKHIR'			=> $tgl_akhir,
	'BULAN_'.$bulan	=> "selected",
	'TAHUN'					=> $tahun,
	'CARI'					=> $cari,
	'PAGING'				=> $paging
	)
);
  

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>