<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

class Pengumuman{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Pengumuman(){
		$this->ID_FILE="C-PNG";
	}
	
	//BODY
	
	function periksaDuplikasi($kode){
		
		/*
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT f_pengumuman_periksa_duplikasi('$kode') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($kode_pengumuman,$judul_pengumuman,$pengumuman,$kota,$pembuat_pengumuman){
	  
		/*
		IS	: data pengumuman belum ada dalam database
		FS	:Data pengumuman baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_pengumuman_tambah('$kode_pengumuman','$judul_pengumuman','$pengumuman','$kota',$pembuat_pengumuman,NOW())";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah($id_pengumuman,$kode_pengumuman_old,$kode_pengumuman,$judul_pengumuman,$pengumuman,$kota,$pembuat_pengumuman){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman diubah
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_pengumuman_ubah($id_pengumuman,'$kode_pengumuman_old','$kode_pengumuman','$judul_pengumuman','$pengumuman','$kota',$pembuat_pengumuman,NOW())";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_pengumuman){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman dihapus
		*/
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA
		$sql =
			"DELETE FROM tbl_pengumuman
			WHERE IdPengumuman IN($list_pengumuman);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($id_pengumuman){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *,f_user_get_nama_by_userid(PembuatPengumuman) AS NamaPembuat
			FROM tbl_pengumuman
			WHERE IdPengumuman='$id_pengumuman';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *,f_user_get_nama_by_userid(PembuatPengumuman) AS NamaPembuat
			FROM tbl_pengumuman
			WHERE 
				(KodePengumuman LIKE '$pencari' 
				OR JudulPengumuman LIKE '$pencari' 
				OR Pengumuman LIKE '%$pencari%') 
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilSemuaPengumuman($user_id){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql_status_baca	= "SELECT user_id FROM tbl_user_baca_pengumuman tub WHERE tub.IdPengumuman=tp.IdPenguman";
		
		$sql = 
			"SELECT *,f_user_get_nama_by_userid(PembuatPengumuman) AS NamaPembuat,
				IF($user_id IN ($sql_status_baca),1,0) AS StatusBaca
			FROM tbl_pengumuman tp
			ORDER BY WaktuPembuatanPengumuman DESC, KodePengumuman ASC";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilJumlahPengumumanBaru($user_id){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT IF(f_user_get_jumlah_pengumuman_baru($user_id) IS NULL,0,f_user_get_jumlah_pengumuman_baru($user_id)) AS jumlah";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$jumlah	= $row['jumlah'];
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $jumlah;
	}//  END ambilJumlahPengumumanBaru
	
	function sudahBaca($id_pengumuman,$user_id){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_user_baca_pengumuman_tambah('$id_pengumuman','$user_id')";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function updatePengumumanBaru(){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_user_update_pengumuman_baru()";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function resetPengumumanBaru($user_id){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_user_reset_pengumuman_baru($user_id)";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}

	
}
?>