<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$ket_area				= $HTTP_GET_VARS['area']!=""?"Area ".$HTTP_GET_VARS['area']:"Semua Area";
$sort_by				= $HTTP_GET_VARS['sort_by'];
$order					= $HTTP_GET_VARS['order'];

$tanggal_mulai	= $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir	= $HTTP_GET_VARS['tanggal_akhir'];

//$tanggal_mulai	= FormatTglToMySQLDate($tanggal_mulai);
//$tanggal_akhir	= FormatTglToMySQLDate($tanggal_akhir);

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"Nama":$sort_by;

$nama_view	= "v_laporan_insentif_sopir".$userdata['user_id'];

$sql	= "SELECT * FROM $nama_view ORDER BY $sort_by $order;";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Insentif Sopir '.$ket_area.' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'NRP');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Total Hari Kerja');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Manifest Utama');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Manifest Transit');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jum.Pnp Insentif');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Total Insentif');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+3;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeDriver']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['TotalHariKerja']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['TotalTripUtama']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['TotalTripTransit']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['TotalPenumpangInsentif']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['TotalInsentif']);
	
	
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D4:D'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Insentif Sopir '.$ket_area.' per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
