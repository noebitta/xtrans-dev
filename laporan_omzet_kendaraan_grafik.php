<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include_once ($adp_root_path. 'chart/php-ofc-library/open_flash_chart_object.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$no_polisi			= isset($HTTP_GET_VARS['no_polisi'])? $HTTP_GET_VARS['no_polisi'] : $HTTP_POST_VARS['no_polisi'];
$username				= $userdata['username'];

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$bulan					= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$tahun					= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

$bulan	=($bulan!='')?$bulan:date("m");
$tahun	= ($tahun!='')?$tahun:date("Y");

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	//$list_bulan	.="<a href='".append_sid('laporan_omzet_kendaraan_grafik.'.$phpEx).$parameter."'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
	$list_bulan	.="<a href='#' onClick='setGrafik($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}


// LIST
$template->set_filenames(array('body' => 'laporan_omzet_kendaraan/laporan_omzet_kendaraan_grafik_body.tpl')); 



if($bulan<=0){
	$bulan	= 1;
	$tahun	= $temp_tanggal[2]-1;
}

$parameter	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&no_polisi=".$no_polisi."&cabang=".$kode_cabang.
								"&sort_by=".$sort_by."&order=".$order;

$template->assign_vars(array(
	'BCRUMP'    						=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | 
															<a href="'.append_sid('laporan_omzet_kendaraan.'.$phpEx).'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$kode_cabang.'&sort_by='.$sort_by.'&order='.$order.'">Laporan Omzet Kendaraan</a> | 
															<a href="'.append_sid('laporan_omzet_kendaraan_grafik.'.$phpEx).'&tgl_mulai='.$tanggal_mulai.'&no_polisi='.$no_polisi.'">Grafik Produktifitas Kendaraan</a>',
	'LIST_BULAN'						=> "| ".$list_bulan,
	'TAHUN'									=> $tahun,
	'URL'										=> append_sid('laporan_omzet_kendaraan_grafik.'.$phpEx).$parameter,
	'DATA_GRAFIK_BULANAN'		=> append_sid('laporan_omzet_kendaraan_grafik_data.php').'%26mode=bulanan%26tgl=1-'.$bulan.'-'.$tahun.'%26no_polisi='.$no_polisi,
	'DATA_GRAFIK_TAHUNAN'		=> append_sid('laporan_omzet_kendaraan_grafik_data.php').'%26mode=tahunan%26tgl=1-'.$bulan.'-'.$tahun.'%26no_polisi='.$no_polisi
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>