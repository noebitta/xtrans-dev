<?php
//
// CETAK TIKET UNTUK LINUX
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPaket.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassLogSMS.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);			
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];		
$cso							= $userdata['nama'];

$Reservasi	= new Reservasi();
$Paket			= new Paket();

//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
$row	= $Paket->ambilDataDetail($no_tiket);

$no_tiket=$row['NoTiket'];
$nama_pengirim=$row['NamaPengirim'];
$telp_pengirim=$row['TelpPengirim'];
$alamat_pengirim=$row['AlamatPengirim'];
$nama_penerima=$row['NamaPenerima'];
$telp_penerima=$row['TelpPenerima'];
$alamat_penerima=$row['AlamatPenerima'];
$tanggal=$row['TglBerangkat'];
$jam=$row['JamBerangkat'];
$asal=explode("(",$row['NamaAsal']);
$tujuan=explode("(",$row['NamaTujuan']);
$harga_paket=number_format($row['HargaPaket'],0,",",".");
$diskon_paket=number_format($row['Diskon'],0,",",".");
$total_bayar=number_format($row['TotalBayar'],0,",",".");
$operator=$row['NamaCSO'];
$cetak_tiket=$row['CetakTiket'];
$jenis_pembayaran=($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
$kode_jadwal=$row['KodeJadwal'];
$cara_bayar	= $row['CaraBayar'];
$id_jurusan	= $row['IdJurusan'];

$kirim_sms	= false;

$jenis_pembayaran	= $row['CaraPembayaran']==0?"TUNAI":"LANGGANAN";

$layanan	= $LIST_JENIS_LAYANAN_PAKET[$row['Layanan']];

//mengupate flag cetak tiket
if($cetak_tiket!=1){
	
	$cabang_transaksi	=($cara_bayar<$PAKET_CARA_BAYAR_DI_TUJUAN)?$userdata['KodeCabang']:"";
	$Paket->updateCetakTiket($no_tiket,$jenis_pembayaran,$cabang_transaksi);
	
	//SMS REMINDER
	$kirim_sms=false;
	if($kirim_sms){
		$LogSMS	= new LogSMS();
		
		//mengambil asal dan tujuan
		
		$asal		= substr($asal[0],0,10);
		$tujuan	= substr($tujuan[0],0,10);
		
		//Mengirim kepada penerima
		if(in_array(substr($telp_penerima,0,2),$HEADER_NO_TELP)){
			
			$isi_pesan	= "SDR/I ".strtoupper(substr($nama_pengirim,0,10))." MENGIRIMKAN PAKET UNTUK ANDA PADA ".dateparse(FormatMySQLDateToTgl($tanggal))." $jam RESI $no_tiket.PAKET DAPAT DIAMBIL DI XTRANS $tujuan. INFO 0213149777";
				
			$telepon	= "62".substr($telp_penerima,1);
			$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
			
			$response	= sendHttpPost($sms_config['url'],$parameter);	
			
			if($response=="00"){
				$LogSMS->tambah(
					$telp_penerima, $nama_penerima, 2, 
					$no_tiket, $isi_pesan);
			}
		}
	}
	
	//Mengirim kepada pengirim
	/*if(in_array(substr($telp_pengirim,0,2),$HEADER_NO_TELP)){
		
		$isi_pesan	= "TERIMA KASIH SDR/I ".strtoupper(substr($nama_pengirim,0,10))." SUDAH MENGIRIMKAN PAKET PADA ".dateparse(FormatMySQLDateToTgl($tanggal))." $jam RESI:$no_tiket.PAKET DIKIRIM KE BIMOTRANS $tujuan. $keterangan_bayar,INFO 02168636880/02270366326";
					
		$telepon	= "62".substr($telp_pengirim,1);
		$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
		//$response	= sendHttpPost($sms_config['url'],$parameter);	
		
		/*if($response=="00"){
			$LogSMS->tambah(
				$telp_pengirim, $nama_pengirim, 2, 
				$no_tiket, $isi_pesan);
		}*/
		
		
	//}
	
}

$template->set_filenames(array('body' => 'tiket_paket.tpl'));

$template->assign_vars(array(
	'TUJUAN'					=>strtoupper(substr($tujuan[0],0,20)),
	'LAYANAN'					=>$layanan,
	'NO_RESI'					=>$row['NoTiket'],
	'TANGGAL'					=>dateparse(FormatMySQLDateToTgl($tanggal)),
	'JURUSAN'					=>substr($asal[0],0,20)."-".substr($tujuan[0],0,20),
	'JAM_BERANGKAT'		=>$jam,
	'WAKTU_CETAK'			=>dateparseWithTime(date("d-m-Y H:i:s")),
	'JENIS_BARANG'		=>$row['JenisBarang'],
	'KOLI'						=>$row['JumlahKoli'],
	'BERAT'						=>$row['Berat'],
	'NAMA_PENGIRIM'		=>$nama_pengirim,
  'ALAMAT_PENGIRIM'	=>$alamat_pengirim,
	'TELP_PENGIRIM'		=>$telp_pengirim,
	'NAMA_PENERIMA'		=>$nama_penerima,
	'ALAMAT_PENERIMA'	=>$alamat_penerima,
	'TELP_PENERIMA'		=>$telp_penerima,
	'HARGA'						=>$harga_paket,
	'DISKON'					=>$diskon_paket,
	'TOTAL'						=>$total_bayar,
	'KET_PAKET'				=>$row['KeteranganPaket'],
	'INS_KHUSUS'			=>$row['InstruksiKhusus'],
	'JENIS_BAYAR'			=>$jenis_pembayaran,
	'PETUGAS_CETAK'		=>$userdata['nama'])
);

$template->pparse('body');	
	
?>