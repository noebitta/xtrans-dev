<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Nama":$sort_by;

//QUERY LAPORAN				
$sql=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(SubTotal),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi_olap
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND (CaraPembayaran<$PAKET_CARA_BAYAR_DI_TUJUAN OR (CaraPembayaran=$PAKET_CARA_BAYAR_DI_TUJUAN AND StatusDiambil=1))   AND FlagBatal!=1 $kondisi_cabang
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['KodeCabang']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		KodeCabang,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang
	GROUP BY KodeCabang ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Nama']								= $row['Nama'];
	$temp_array[$idx]['Alamat']							= $row['Alamat'];
	$temp_array[$idx]['Kota']								= $row['Kota'];
	$temp_array[$idx]['Telp']								= $row['Telp'];
	$temp_array[$idx]['Fax']								= $row['Fax'];
	$temp_array[$idx]['hp']									= $row['hp'];
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenjualanPaket']= $data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalPaket']					= $data_paket_total[$row['KodeCabang']]['TotalPaket'];
	$temp_array[$idx]['TotalBiaya']					= $data_paket_total[$row['KodeCabang']]['TotalBiaya'];
	$temp_array[$idx]['Total']							= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,60);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Rekap Uang per Cabang','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(40,5,'Cabang','B',0,'C',1);
$pdf->Cell(20,5,'Kode','B',0,'C',1);
$pdf->Cell(40,5,'Alamat','B',0,'C',1);
$pdf->Cell(25,5,'Pnp','B',0,'C',1);
$pdf->Cell(25,5,'Omz.Pnp','B',0,'C',1);
$pdf->Cell(25,5,'Pkt','B',0,'C',1);
$pdf->Cell(25,5,'Omz.Pkt','B',0,'C',1);
$pdf->Cell(25,5,'Disc.','B',0,'C',1);
$pdf->Cell(25,5,'OP Langsung','B',0,'C',1);
$pdf->Cell(25,5,'L/R Kotor','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

$idx=0;

while($idx<count($temp_array)){
	
	//total tiket
	$total_penjualan_tiket	= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket						= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total biaya
	$total_biaya						= $temp_array[$idx]['TotalBiaya'];
	
	//total
	$total							= $temp_array[$idx]['Total'];
	
	$pdf->Cell(5,5,$idx+1,'',0,'C');
	$pdf->Cell(40,5,substr($temp_array[$idx]['Nama'],0,20),'',0,'L');
	//$pdf->MultiCell2(40,5,$temp_array[$idx]['Nama'],'','L');
	$pdf->Cell(20,5,$temp_array[$idx]['KodeCabang'],'',0,'L');
	$pdf->Cell(40,5,substr($temp_array[$idx]['Alamat'],0,15)." ".$temp_array[$idx]['Kota'],'',0,'L');
	//$pdf->MultiCell2(40,5,$temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Kota'],'','L');
	$pdf->Cell(25,5,number_format($total_tiket,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_penjualan_tiket,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_paket,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_penjualan_paket,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_discount,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_biaya,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total,0,",","."),'',0,'R');
	$pdf->Ln();
	$pdf->Cell(280,1,'','B',0,'');
	$pdf->Ln();
	
	$total_penumpang	+=$total_tiket;
	$total_omzet			+=$total_penjualan_tiket;
	$total_paket			+=$total_paket;
	$total_omzet_paket+=$total_penjualan_paket;
	$total_discount		+=$total_discount;
	$total_biaya			+=$total_biaya;
	$total_profit			+=$total;
	
	$idx++;
}

$pdf->Cell(105,5,"TOTAL",'',0,'C');
$pdf->Cell(25,5,number_format($total_penumpang,0,",","."),'',0,'R');
$pdf->Cell(25,5,number_format($total_omzet,0,",","."),'',0,'R');
$pdf->Cell(25,5,number_format($total_paket,0,",","."),'',0,'R');
$pdf->Cell(25,5,number_format($total_omzet_paket,0,",","."),'',0,'R');
$pdf->Cell(25,5,number_format($total_discount,0,",","."),'',0,'R');
$pdf->Cell(25,5,number_format($total_biaya,0,",","."),'',0,'R');
$pdf->Cell(25,5,number_format($total_profit,0,",","."),'',0,'R');
$pdf->Ln();
$pdf->Cell(105,5,"",'',0,'C');$pdf->Cell(175,1,'','B',0,'');
								
$pdf->Output();
						
?>