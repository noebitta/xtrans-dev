<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

class PengaturanUmum{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Pengumuman(){
		$this->ID_FILE="C-PU";
	}
	
	//BODY
	
	function ubahPengaturanUmum(
		$pesan_di_tiket,$alamat,$telp,$email,
		$website,$tgl_mulai_tuslah1,$tgl_akhir_tuslah1,
		$tgl_mulai_tuslah2,$tgl_akhir_tuslah2){		
		//kamus
		global $db;
		
		$sql_header	= "UPDATE tbl_pengaturan_parameter SET NilaiParameter=";
		
		//MENGUBAH PESAN DITIKET
		$sql = $sql_header."'$pesan_di_tiket' WHERE NamaParameter='PESAN_DITIKET'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH ALAMAT PERUSAHAAN
		$sql = $sql_header."'$alamat' WHERE NamaParameter='PERUSH_ALAMAT'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH TELP PERUSAHAAN
		$sql = $sql_header."'$telp' WHERE NamaParameter='PERUSH_TELP'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH EMAIL PERUSAHAAN
		$sql = $sql_header."'$email' WHERE NamaParameter='PERUSH_EMAIL'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH WEB PERUSAHAAN
		$sql = $sql_header."'$website' WHERE NamaParameter='PERUSH_WEB'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH TGL AWAL TUSLAH LUAR KOTA
		$sql = $sql_header."'$tgl_mulai_tuslah1' WHERE NamaParameter='TGL_MULAI_TUSLAH1'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH TGL AKHIR TUSLAH LUAR KOTA
		$sql = $sql_header."'$tgl_akhir_tuslah1' WHERE NamaParameter='TGL_AKHIR_TUSLAH1'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH TGL AWAL TUSLAH DALAM KOTA
		$sql = $sql_header."'$tgl_mulai_tuslah2' WHERE NamaParameter='TGL_MULAI_TUSLAH2'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//MENGUBAH TGL AKHIR TUSLAH DALAM KOTA
		$sql = $sql_header."'$tgl_akhir_tuslah2' WHERE NamaParameter='TGL_AKHIR_TUSLAH2'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//Mengubah Harga Paket Platinum
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_p',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_p'
			WHERE KodeLayanan='P'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE ".__LINE__);
		}
		
		//Mengubah Harga Paket Gold Antar
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_ga',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_ga'
			WHERE KodeLayanan='GA'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//Mengubah Harga Paket Gold Diambil
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_gd',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_gd'
			WHERE KodeLayanan='GD'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//Mengubah Harga Paket silver
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_s',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_s'
			WHERE KodeLayanan='S'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilData(){
		
		/*
		Desc	:Mengembalikan data pengaturan umum
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_pengaturan_parameter
			ORDER BY NamaParameter;";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$data_return	= array();
		
		while ($row=$db->sql_fetchrow($result)){
			$field_name	= $row['NamaParameter'];
			$data_return[$field_name]	= $row['NilaiParameter'];
		}
		
		return $data_return;
		
	}//  END ambilData
	
	function ambilDataPerusahaan(){
		
		/*
		Desc	:Mengembalikan data perusahaan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT NamaParameter, NilaiParameter
			FROM tbl_pengaturan_parameter
			WHERE NamaParameter LIKE 'PERUSH_%'";
		
		$data_return = array();
		
		while ($row=$db->sql_fetchrow($result)){
			$field_name	= $row['NamaParameter'];
			$data_return[$field_name]	= $row['NilaiParameter'];
		}
		
		return $data_return;

	}//  END ambilDataPerusahaan
	
	function ambilPesanUntukDiTiket(){
		
		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT NilaiParameter
			FROM tbl_pengaturan_parameter WHERE NamaParameter='PESAN_DITIKET'";
				
		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);
			
			$pesan	=$row[0];
			
			return $pesan;
		} 
		else{
			//die_error("Err: $this->ID_FILE".__LINE__);
			return "-E-";
		}

	}//  END ambilPesanUntukDiTiket
	
	function ambilTglTuslah(){
		
		/*
		Desc	:Mengembalikan tanggal tuslah
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_pengaturan_parameter
			WHERE NamaParameter LIKE 'TGL_%';";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$data_return	= array();
		
		while ($row=$db->sql_fetchrow($result)){
			$field_name	= $row['NamaParameter'];
			$data_return[$field_name]	= $row['NilaiParameter'];
		}
		
		return $data_return;
		
	}//  END ambilData
	
	function isTuslah($tgl_berangkat){
		
		/*
		Desc	:Mengembalikan tanggal tuslah
		*/
		
		//kamus
		global $db;
		
		$tgl_tuslah	= $this->ambilTglTuslah();
		
		$sql = 
			"SELECT IF('$tgl_berangkat' BETWEEN '".$tgl_tuslah['TGL_MULAI_TUSLAH1']."' AND '".$tgl_tuslah['TGL_AKHIR_TUSLAH1']."',1,0);";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		
		return $row[0]==0?false:true;
		
	}//  END isTuslah
}
?>