<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_CSO,$LEVEL_CSO_PAKET,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari				= isset($HTTP_GET_VARS['txt_cari'])? $HTTP_GET_VARS['txt_cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'paket.checkin/paket.checkin.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$Cabang	= new Cabang();

switch($mode){
	case 'gettujuan':
		
		echo "
			<select name='tujuan' id='tujuan' onChange='getUpdateAsal(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$tujuan,"")."
			</select>";
		
	exit;
		
	case 'getasal':
		echo "
			<select name='asal' id='asal' >
				".$Cabang->setInterfaceComboCabangAsalByTujuan($tujuan,$asal)."
			</select>";
	exit;
}

$kondisi	=($cari=="")?"":
			" AND (KodeSopir LIKE '%$cari%' 
				OR NoSPJ LIKE '%$cari%' 
				OR KodeKendaraan LIKE '%$cari%')";
		

$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan))='$kota'":"";
$kondisi .= $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'":"";

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"TglBerangkat,Jamberangkat":$sort_by;


//PAGING======================================================
/*$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"","tbl_paket",
"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi GROUP BY TglBerangkat,KodeJadwal" ,"paket.checkin.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

/*$sql	=
	"SELECT 
		KodeDriver,Driver,
		NoSPJ,TglSPJ,KodeJadwal,
		TglBerangkat,JamBerangkat,JumlahKursiDisediakan,
		JumlahPaket,NoPolisi,
		TIMEDIFF(TglSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) AS Keterlambatan,
		IF(TIMEDIFF(TglSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) >'00:15:00',1,0) IsTerlambat,
		IF(IsSubJadwal!=1,'UTAMA','TRANSIT') AS Keterangan
	FROM tbl_spj ts
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";
*/

$sql	=
	"SELECT 
		KodeSopir AS KodeDriver,
		NoSPJ,TglCetakSPJ AS TglSPJ,KodeJadwal,
		TglBerangkat,JamBerangkat,
		COUNT(1) AS JumlahPaket,KodeKendaraan AS NoPolisi,
		TIMEDIFF(TglCetakSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) AS Keterlambatan,
		IF(TIMEDIFF(TglCetakSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) >'00:15:00',1,0) IsTerlambat
	FROM tbl_paket
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	GROUP BY TglBerangkat,KodeJadwal
	ORDER BY $sort_by $order";
	
//debug
//echo($sql);exit;
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['IsTerlambat']==1){
		$flagterlambat = "red";
		$lama_terlambat	= $row['Keterlambatan'];
	}
	else{
		$flagterlambat	= "";
		$lama_terlambat	= "";
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('paket.checkin.detail.php?no_spj='.$row['NoSPJ'].'&driver='.$row['Driver'].'&no_polisi='.$row['NoPolisi'].'&tgl_berangkat='.$row['TglBerangkat'].'&kode_jadwal='.$row['KodeJadwal'].'&waktu_berangkat='.dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'])."\");return false'>Detail<a/>";		
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'flagterlambat'=>$flagterlambat,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'jadwal'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
				'kodejadwal'=>$row['KodeJadwal'],
				'berangkat'=>dateparse(FormatMySQLDateToTglWithTime($row['TglSPJ'])),
				'keterlambatan'=>$lama_terlambat,
				'manifest'=>$row['NoSPJ'],
				'sopir'=>$row['Driver'],
				'mobil'=>$row['NoPolisi'],
				'paket'=>number_format($row['JumlahPaket'],0,",","."),
				'keterangan'=>$row['Keterangan'],
				'act'=>$act
			)
		);
	$i++;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&kota=$kota&asal=$asal&tujuan=$tujuan&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$template->assign_vars(array(
	'ACTION_CARI'		=> append_sid('paket.checkin.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'PAGING'				=> $paging,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_KOTA'			=> setComboKota($kota),
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'A_SORT_1'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=TglBerangkat,JamBerangkat'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan berdasarkan Jadwal ($order_invert)",
	'A_SORT_2'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=KodeJadwal'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan berdasarkan KodeJadwal ($order_invert)",
	'A_SORT_3'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=TglSPJ'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan berdasarkan Berangkat ($order_invert)",
	'A_SORT_4'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=Keterlambatan'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan berdasarkan Keterlambatan ($order_invert)",
	'A_SORT_5'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=NoSPJ'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan berdasarkan Manifest($order_invert)",
	'A_SORT_6'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=Driver'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan berdasarkan Sopir ($order_invert)",
	'A_SORT_7'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=NoPolisi'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan berdasarkan Mobil ($order_invert)",
	'A_SORT_8'			=> append_sid('paket.checkin.'.$phpEx.'?sort_by=JumlahPaket'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan berdasarkan Jumlah Pax ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>