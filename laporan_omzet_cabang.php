<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN,$LEVEL_SUPERVISOR))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];

$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_cabang/laporan_omzet_cabang_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

/*if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND KodeCabangAsal='$userdata[KodeCabang]'";	
}	*/		

$kondisi_cari	.= $kondisi_cabang;
		
$sql=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari
	ORDER BY Kota,Nama";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		tmj.KodeCabangAsal AS KodeCabang,
		IS_NULL(COUNT(IF((JenisPenumpang	='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang		='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang		='K' AND JenisPembayaran!=3 AND Total != 0,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang		='KK'AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(((JenisPenumpang	='G' AND JenisPembayaran!=3) OR (JenisPembayaran!=3 AND Total = 0)),NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang		='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,

		IS_NULL(COUNT(IF(JenisPenumpang 	='RB'AND JenisPembayaran!= 3,NoTiket,NULL)),0) AS TotalPenumpangRB,

		IS_NULL(COUNT(IF(JenisPenumpang		='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPenumpang		='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPembayaran	='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		

		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),tr.HargaTiket)),0) AS TotalPenjualanTiket,
		/*IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),HargaTiket)),0) AS TotalPenjualanTiket, */
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM $tbl_reservasi tr LEFT JOIN tbl_md_jurusan tmj ON tr.IdJurusan=tmj.IdJurusan
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY KodeCabangAsal ORDER BY KodeCabangAsal ";

if (!$result = $db->sql_query($sql))
{
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result))
{
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan))
{

	$temp_array[$idx]['KodeCabang']							= $row['KodeCabang'];
	$temp_array[$idx]['Nama']								= $row['Nama'];
	$temp_array[$idx]['Alamat']								= $row['Alamat'];
	$temp_array[$idx]['Kota']								= $row['Kota'];
	$temp_array[$idx]['Telp']								= $row['Telp'];
	$temp_array[$idx]['Fax']								= $row['Fax'];
	$temp_array[$idx]['hp']									= $row['hp'];
	$temp_array[$idx]['TotalPenumpangU']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangG']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangT']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangT'];

	$temp_array[$idx]['TotalPenumpangRB']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangRB'];

	$temp_array[$idx]['TotalPenumpangR']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangVR']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangVR'];
	$temp_array[$idx]['TotalPenumpangV']					= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangV'];
	$temp_array[$idx]['TotalBerangkat']						= $data_tiket_total[$row['KodeCabang']]['TotalBerangkat'];
	$temp_array[$idx]['TotalTiket']							= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalKomisiOnline']					= $data_tiket_total[$row['KodeCabang']]['TotalKomisiOnline'];
	$temp_array[$idx]['TotalPenjualanTiket']				= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']						= $data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['OmzetNet']							= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket']-$data_tiket_total[$row['KodeCabang']]['TotalDiscount']-$data_tiket_total[$row['KodeCabang']]['TotalKomisiOnline'];
	$temp_array[$idx]['TotalPenumpangPerTrip']= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	
	$idx++;
}

$idx=0;
$kota_terakhir	= "";
$gt_trip			= 0;
$gt_pnp_u			= 0;
$gt_pnp_m			= 0;
$gt_pnp_k			= 0;
$gt_pnp_kk			= 0;
$gt_pnp_g			= 0;
$gt_pnp_o			= 0;

$gt_pnp_rb			= 0;

$gt_pnp_r			= 0;
$gt_pnp_vr			= 0;
$gt_pnp_v			= 0;
$gt_pnp_t			= 0;
$gt_omz_pnp			= 0;
$gt_disc			= 0;
$gt_omzet_net		= 0;
$gt_komisi			= 0;

//PLOT DATA
while($idx<=count($temp_array)){
	
	if($kota_terakhir!=$temp_array[$idx]['Kota'] || $idx==count($temp_array)){
		$nama_kota								= "<tr><th colspan='20' style='font-size:14px;'><b>".$temp_array[$idx]['Kota']."</b></th></tr>";
		$kota_terakhir						= $temp_array[$idx]['Kota'];
		$idx_kota									= 1;

		if($idx>0)
		{
		
			$total_per_kota		= 
				"<tr style='background:green;color:white;'>
					<td colspan='4' align='center'><b>Sub Total</b></td>
					<td align='right'>".number_format($total_trip_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_u_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_m_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_k_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_kk_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_g_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_o_perkota,0,",",".")."</td>

					<td align='right'>".number_format($total_pnp_rb_perkota,0,",",".")."</td>


					<td align='right'>".number_format($total_pnp_r_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_vr_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_v_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_t_perkota,0,",",".")."</td>
					<td align='right'>&nbsp;</td>
					<td align='right'>".number_format($total_omz_pnp_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_disc_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_omzet_net_perkota,0,",",".")."</td>
					<td align='right'>&nbsp;</td>
				</tr>";
			
		}

		$total_trip_perkota				= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perkota			= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perkota			= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perkota			= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perkota			= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perkota			= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perkota			= $temp_array[$idx]['TotalPenumpangT'];

		$total_pnp_rb_perkota			= $temp_array[$idx]['TotalPenumpangRB'];
		
		$total_pnp_r_perkota			= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perkota			= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_v_perkota			= $temp_array[$idx]['TotalPenumpangV'];
		$total_pnp_t_perkota			= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perkota		= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perkota				= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perkota	= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];

		
	}
	else
	{
		$nama_kota								= "";
		$idx_kota++;
		$total_trip_perkota				+= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perkota			+= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perkota			+= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perkota			+= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perkota			+= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perkota			+= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perkota			+= $temp_array[$idx]['TotalPenumpangT'];

		$total_pnp_rb_perkota			+= $temp_array[$idx]['TotalPenumpangRB'];
		
		$total_pnp_r_perkota			+= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perkota			+= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_v_perkota			+= $temp_array[$idx]['TotalPenumpangV'];
		$total_pnp_t_perkota			+= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perkota		+= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perkota				+= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perkota	+= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
		$total_per_kota						= "";
	}


	$odd ='odd';
	
	if (($idx % 2)==0)
	{
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_omzet_cabang_detail.php?tglmulai='.$tanggal_mulai.'&tglakhir='.$tanggal_akhir.'&kode='.$temp_array[$idx]['KodeCabang'].'&is_today='.$is_today)."\");return false'>Detail<a/>&nbsp;+&nbsp;";		
	
	$act 	.="<a href='".append_sid('laporan_omzet_cabang_grafik.'.$phpEx).'&kode_cabang='.$temp_array[$idx]['KodeCabang'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&sort_by='.$sort_by.'&order='.$order."'>Grafik<a/>";		
	
	//total tiket
	$total_penjualan_tiket			= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket					= $temp_array[$idx]['TotalTiket'];	

	if($idx<count($temp_array))
	{
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'					=>$odd,
					'no'					=>$idx_kota,
					'nama_kota'				=>$nama_kota,
					'kode_cabang'			=>$temp_array[$idx]['KodeCabang'],
					'cabang'				=>$temp_array[$idx]['Nama'],
					'alamat'				=>$temp_array[$idx]['Alamat'],
					'total_keberangkatan'	=>number_format($temp_array[$idx]['TotalBerangkat'],0,",","."),
					'total_penumpang_u'		=>number_format($temp_array[$idx]['TotalPenumpangU'],0,",","."),
					'total_penumpang_m'		=>number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."),
					'total_penumpang_k'		=>number_format($temp_array[$idx]['TotalPenumpangK'],0,",","."),
					'total_penumpang_kk'	=>number_format($temp_array[$idx]['TotalPenumpangKK'],0,",","."),
					'total_penumpang_g'		=>number_format($temp_array[$idx]['TotalPenumpangG'],0,",","."),
					'total_penumpang_o'		=>number_format($temp_array[$idx]['TotalPenumpangT'],0,",","."),

					'total_penumpang_rb'	=>number_format($temp_array[$idx]['TotalPenumpangRB'],0,",","."),
					
					'total_penumpang_r'		=>number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."),
					'total_penumpang_vr'	=>number_format($temp_array[$idx]['TotalPenumpangVR'],0,",","."),
					'total_penumpang_v'		=>number_format($temp_array[$idx]['TotalPenumpangV'],0,",","."),
					'total_penumpang'		=>number_format($total_tiket,0,",","."),
					'rata_pnp_per_trip'		=>number_format($temp_array[$idx]['TotalPenumpangPerTrip'],0,",","."),
					'total_omzet'			=>number_format($total_penjualan_tiket,0,",","."),
					'total_discount'		=>number_format($total_discount,0,",","."),
					'omzet_net'				=>number_format($temp_array[$idx]['OmzetNet'],0,",","."),
					'act'					=>$act,
					'total_per_kota'		=>$total_per_kota,
				)
			);
	}
	else
	{
		$template->
			assign_block_vars(
				'ROW',
				array(
					'total_per_kota'  =>$total_per_kota
				)
			);
	}

	$gt_trip			+= $temp_array[$idx]['TotalBerangkat'];
	$gt_pnp_u			+= $temp_array[$idx]['TotalPenumpangU'];
	$gt_pnp_m			+= $temp_array[$idx]['TotalPenumpangM'];
	$gt_pnp_k			+= $temp_array[$idx]['TotalPenumpangK'];
	$gt_pnp_kk			+= $temp_array[$idx]['TotalPenumpangKK'];
	$gt_pnp_g			+= $temp_array[$idx]['TotalPenumpangG'];
	$gt_pnp_o			+= $temp_array[$idx]['TotalPenumpangT'];

	$gt_pnp_rb			+= $temp_array[$idx]['TotalPenumpangRB'];
	
	$gt_pnp_r			+= $temp_array[$idx]['TotalPenumpangR'];
	$gt_pnp_vr			+= $temp_array[$idx]['TotalPenumpangVR'];
	$gt_pnp_v			+= $temp_array[$idx]['TotalPenumpangV'];
	$gt_pnp_t			+= $total_tiket;
	$gt_omz_pnp			+= $total_penjualan_tiket;
	$gt_disc			+= $total_discount;
	$gt_omzet_net		+= $temp_array[$idx]['OmzetNet'];
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&is_today=$is_today&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$temp_array[$idx]['KodeCabang'].
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
											
$script_cetak_excel="Start('laporan_omzet_cabang_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "is_today".($is_today==""?"1":$is_today);
$$temp_var	= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_omzet">Home</a> | <a href="'.append_sid('laporan_omzet_cabang.'.$phpEx).'">Laporan Omzet Cabang</a>',
	'ACTION_CARI'		=> append_sid('laporan_omzet_cabang.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'IS_TODAY1'			=> $is_today1,
	'IS_TODAY0'			=> $is_today0,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'				=> $userdata['Nama'],
	'SUMMARY'			=> $summary,
	'PAGING'			=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'GT_TRIP'			=> number_format($gt_trip,0,",","."),
	'GT_PNP_U'			=> number_format($gt_pnp_u,0,",","."),
	'GT_PNP_M'     		=> number_format($gt_pnp_m,0,",","."),			
	'GT_PNP_K'      	=> number_format($gt_pnp_k,0,",","."),		
	'GT_PNP_KK'     	=> number_format($gt_pnp_kk,0,",","."),		
	'GT_PNP_G'      	=> number_format($gt_pnp_g,0,",","."),		
	'GT_PNP_O'      	=> number_format($gt_pnp_o,0,",","."),	

	'GT_PNP_RB'    	  	=> number_format($gt_pnp_rb,0,",","."),		
	
	'GT_PNP_R'     		=> number_format($gt_pnp_r,0,",","."),		
	'GT_PNP_VR'     	=> number_format($gt_pnp_vr,0,",","."),
	'GT_PNP_V'     		=> number_format($gt_pnp_v,0,",","."),		
	'GT_PNP_T'     		=> number_format($gt_pnp_t,0,",","."),		
	'GT_OMZ'        	=> number_format($gt_omz_pnp,0,",","."),		
	'GT_DISC'       	=> number_format($gt_disc,0,",","."),
	'GT_OMZ_NET'    	=> number_format($gt_omzet_net,0,",",".")
	)
);
	      
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>