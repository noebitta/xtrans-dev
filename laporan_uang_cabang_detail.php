<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_CSO,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tglmulai'])? $HTTP_GET_VARS['tglmulai'] : $HTTP_POST_VARS['tglmulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$kode_cabang		= isset($HTTP_GET_VARS['kode'])? $HTTP_GET_VARS['kode'] : $HTTP_POST_VARS['kode'];
$sort_by				= $HTTP_POST_VARS['sortby'];
$order					= $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'laporan_uang_cabang/detail.tpl')); 

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND FlagBatal!=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND PetugasCetakTiket!=0";

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"WaktuPesan":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoTiket",$tbl_reservasi,
"&kode=$kode_cabang&tglmulai=$tanggal_mulai&tglakhir=$tanggal_akhir&is_today=$is_today&sortby=$sort_by&order=$order",
$kondisi,"laporan_uang_cabang_detail.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================


//QUERY TIKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,
		IF(JenisPenumpang!='R',Discount,0) AS Discount,
		IF(JenisPenumpang!='R',Total,HargaTiket) AS Total,
		IF(JenisPenumpang!='R',0,Total-HargaTiket) AS Plus,
		JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasCetakTiket) AS NamaCSOTiket,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan,
		FlagBatal*10+CetakTiket AS Status,
		WaktuCetakTiket,TglCetakSPJ
	FROM 
		$tbl_reservasi
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";	


if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
} 

$i = 1;//$idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'waktu_pesan'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),
				'waktu_cetak'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
				'no_tiket'=>$row['NoTiket'],
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'kode_jadwal'=>$row['KodeJadwal'],
				'nama'=>$row['Nama'],
				'no_kursi'=>$row['NomorKursi'],
				'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
				'discount'=>number_format($row['Discount'],0,",","."),
				'total'=>number_format($row['Total'],0,",","."),
				'plus'=>number_format($row['Plus'],0,",","."),
				'tipe_discount'=>$row['JenisDiscount'],
				'cso'=>$row['NamaCSO'],
				'tiket_by'=>$row['NamaCSOTiket'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
	
	$i++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tglmulai=".$tanggal_mulai."&tglakhir=".$tanggal_akhir."&is_today=".$is_today."&kode=".$kode_cabang."&sortby=".$sort_by."&order=".$order;
													
$script_cetak_excel="Start('laporan_uang_cabang_detail_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(	
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'IS_TODAY'			=> $is_today,
	'KODE'					=> $kode_cabang,
	'ORDER'					=> $order=="ASC"?"DESC":"ASC",
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>