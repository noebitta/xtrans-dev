<?php
//STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');
// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}

$Cabang			= new Cabang();

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$idbiaya        = isset($HTTP_GET_VARS['idbiaya'])? $HTTP_GET_VARS['idbiaya'] : $HTTP_POST_VARS['idbiaya']; // ubah status jika tidak kosong
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang    = isset($HTTP_GET_VARS['kode_cabang'])? $HTTP_GET_VARS['kode_cabang'] : $HTTP_POST_VARS['kode_cabang'];
$tgl            = date('Y-m-d H:i:s');

//INISIALISASI
if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$cabang = $kode_cabang == "" ? $userdata['KodeCabang'] : $kode_cabang;

// mengambil saldo cabang
$sql = "SELECT Nama,SaldoDropCash FROM tbl_md_cabang WHERE KodeCabang = '$cabang'";
if (!$result = $db->sql_query($sql)){
    die("ERROR : ".__LINE__);
}else{
    $kas = $db->sql_fetchrow($result);
}

function setComboCabang($cabang_dipilih){
    //SET COMBO cabang
    global $db;
    global $Cabang;

    $result=$Cabang->ambilData("","Nama,Kota","ASC");
    $opt_cabang="";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
            $opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
        }
    }
    else{
        echo("Error :".__LINE__);exit;
    }
    return $opt_cabang;
    //END SET COMBO CABANG
}

// seleksi mode
if($mode == 'InsertData'){
    global $db;

    $jenisbiaya = $HTTP_GET_VARS['jenis'];
    $jumlah     = $HTTP_GET_VARS['jumlah'];
    $keterangan = $HTTP_GET_VARS['keterangan'];
    //simpan data ke database
    $sql = "INSERT INTO tbl_pengeluaran_dropcash (KodeCabang, JenisPengeluaran, TglBiaya, id_user, Keterangan, Jumlah)
            VALUES ('$userdata[KodeCabang]','$jenisbiaya',NOW(),'$userdata[user_id]','$keterangan',$jumlah)";
    $db->sql_query($sql)or die(mysql_error());

    //update saldo
    $sql = "UPDATE tbl_md_cabang SET SaldoDropCash = SaldoDropCash - $jumlah WHERE KodeCabang = '$userdata[KodeCabang]'";
    $db->sql_query($sql);

    echo("alert('Pengeluaran Drop Cash berhasil dibuat!');window.location.reload(true);");
    exit;
}elseif ($mode == 'Print'){
    $id = $HTTP_GET_VARS['id'];

    $sql =  "SELECT f_user_get_nama_by_userid(id_user) AS Petugas, f_cabang_get_name_by_kode(KodeCabang) AS Cabang,
                    JenisPengeluaran,TglBiaya, Jumlah, Keterangan
             FROM tbl_pengeluaran_dropcash  
             WHERE id_biaya = $id";

    if(!$result = $db->sql_query($sql)){
        die("ERROR : ".__LINE__);
    }

    $row = $db->sql_fetchrow($result);

    $template->assign_vars(array(
            'nama'          => $row['Petugas'],
            'cabang'        => $row['Cabang'],
            'tgl'           => date_format(date_create($row['TglBiaya']),'d-m-Y H:i:s'),
            'jumlah'        => number_format($row['Jumlah'],0,',','.'),
            'jenis'         => $row['JenisPengeluaran'],
            'keterangan'    => $row['Keterangan']
        )
    );
    $template->set_filenames(array('body' => 'drop_cash_cabang_struck.tpl'));
    $template->pparse('body');
    exit;
}


$kondisi_cari	=($cari=="")?"WHERE 1 ":
    " WHERE Nama LIKE '%$cari%' ";

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"id_biaya","tbl_pengeluaran_dropcash","cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&kode_cabang=$kode_cabang",$kondisi_cari."AND DATE(TglBiaya) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'","drop_cash_cabang.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

// QUERY AMBIL BIAYA PETTY CASH
$sql = "SELECT *, f_cabang_get_name_by_kode(tbl_pengeluaran_dropcash.KodeCabang) AS Cabang, nama
        FROM tbl_pengeluaran_dropcash
        JOIN tbl_user ON tbl_user.user_id = tbl_pengeluaran_dropcash.id_user
        $kondisi_cari AND DATE(TglBiaya) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'
        AND tbl_pengeluaran_dropcash.KodeCabang = '$cabang'
        ORDER BY TglBiaya DESC LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if ($result = $db->sql_query($sql)){

    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){


        if($i != 0){
            $baris = 'odd';
        }else{
            $baris = 'even';
        }

        $template->
        assign_block_vars(
            'ROW',
            array(
                'baris'     =>$baris,
                'No'        =>$i,
                'Id'        =>$row['id_biaya'],
                'KodeCabang'=>$row['Nama'],
                'Pembuat'   =>$row['nama'],
                'JenisBiaya'=>$row['JenisPengeluaran'],
                'Keterangan'=>$row['Keterangan'],
                'Jml'       =>"Rp. ".number_format($row['Jumlah'],0,',','.'),
                'TglBuat'   =>date_format(date_create($row['TglBiaya']),'d-m-Y H:i:s'),
            )
        );
        $i++;
    }
}else{
    echo("Err:".__LINE__.mysql_error());exit;
}

if(in_array($userdata['user_level'],[$LEVEL_CSO,$LEVEL_CSO_PAKET])){
    $opt_cabang = "";
}else{
    $opt_cabang = "Cabang <select name='kode_cabang'>".setComboCabang($cabang)."</select>";
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$cabang."&p4=".$cari."";

$script_cetak_excel="Start('drop_cash_cabang_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$page_title = "Drop Cash";
$template->assign_vars(array(
        'BCRUMP'    	=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('drop_cash_cabang.'.$phpEx).'">Pengeluaran Drop Cash</a>',
        'URL'			=> append_sid('drop_cash_cabang.'.$phpEx),
        'CABANG'        => $kas['Nama'],
        'KAS'           => $kas['SaldoDropCash'],
        'LABEL_KAS'     => number_format($kas['SaldoDropCash'],0,',','.'),
        'TGL_AWAL'	    => $tanggal_mulai,
        'TGL_AKHIR'		=> $tanggal_akhir,
        'TXT_CARI'		=> $cari,
        'OPT_CABANG'    => $opt_cabang,
        'CETAK_XL'      => $script_cetak_excel,
        'EXCEL'         => append_sid('drop_cash_cabang_excel.'.$phpEx.'?'.$parameter_cetak)
    )
);
$template->set_filenames(array('body' => 'drop_cash_cabang_body.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>