<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_CSO,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN_PAKET,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

//$is_today  		= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tglmulai'])? $HTTP_GET_VARS['tglmulai'] : $HTTP_POST_VARS['tglmulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$param_filter  	= isset($HTTP_GET_VARS['paramfilter'])? $HTTP_GET_VARS['paramfilter'] : $HTTP_POST_VARS['paramfilter'];
$jenis_laporan	= isset($HTTP_GET_VARS['tipe'])? $HTTP_GET_VARS['tipe'] : $HTTP_POST_VARS['tipe'];
$keterangan			= isset($HTTP_GET_VARS['ket'])? $HTTP_GET_VARS['ket'] : $HTTP_POST_VARS['ket'];

// LIST
$template->set_filenames(array('body' => 'laporan.paket/paket.detail.tpl')); 

//$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

switch($jenis_laporan){
	case 'cabang':
		$kondisi	= "AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$param_filter'";
	break;
	
	case 'jurusan':
		$kondisi	= "AND IdJurusan='$param_filter'";
	break;
}
	
if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"KodeCabangAsal,KodeCabangTujuan,TglBerangkat,JamBerangkat":$sort_by;

$sql	= 
	"SELECT *,
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabangAsal,
		f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) AS KodeCabangTujuan
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi
	ORDER BY $sort_by $order";
	


if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'							=>$odd,
					'no'							=>$i,
					'no_tiket'				=>$row['NoTiket'],
					'kode_jadwal'			=>$row['KodeJadwal'],
					'waktu_berangkat'	=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
					'nama_pengirim'		=>$row['NamaPengirim'],
					'alamat_pengirim'	=>$row['AlamatPengirim'],
					'telp_pengirim'		=>$row['TelpPengirim'],
					'nama_penerima'		=>$row['NamaPenerima'],
					'alamat_penerima'	=>$row['AlamatPenerima'],
					'telp_penerima'		=>$row['TelpPenerima'],
					'berat'						=>$row['Berat'],
					'harga'						=>number_format($row['HargaPaket'],0,",","."),
					'diskon'					=>number_format($row['Diskon'],0,",","."),
					'bayar'						=>number_format($row['TotalBayar'],0,",","."),
					'layanan'					=>$row['Layanan'],
					'jenis_bayar'			=>$row['JenisPembayaran']==0?"TUNAI":"LANGGANAN"
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&paramfilter=".$param_filter."&p3=".$jenis_laporan."";
	
$script_cetak_pdf="Start('laporan_rekap_tiket_detail_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan.paket.detail.cetak.excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(	
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'KETERANGAN'		=> $jenis_laporan." ".$keterangan,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>