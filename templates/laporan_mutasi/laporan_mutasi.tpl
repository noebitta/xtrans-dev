<script language="JavaScript">

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

</script>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td class="whiter" valign="middle" align="center">		
			<form action="{ACTION_CARI}" method="post" name="my_form">
				<input type='hidden' id='is_cari' name='is_cari' value='1'>
				<table width='100%' cellspacing="0">
					<tr class='banner' height=40>
						<td colspan='3' align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Mutasi Penumpang</td>
					</tr>
				</table>
				<table width='100%'>
					<tr>
						<td colspan='3' valign='middle' align='left'>
							<table width='500'>
								<tr><td colspan='3'><font size='3'><b>Filter</b></font></td></tr>
								<tr>
									<td width=100>Periode</td><td width=5>:</td>
									<td>Tgl&nbsp;<input type='text' id='tgl_awal' name='tgl_awal' value="{TGL_AWAL}" size='2' maxlength='2' onkeypress='validasiAngka(event);' />&nbsp;s/d&nbsp;
										<input type="text" id='tgl_akhir' name='tgl_akhir' value="{TGL_AKHIR}" size='2' maxlength='2' onkeypress='validasiAngka(event);' />&nbsp;
										bln
										<select id='opt_bulan' name='opt_bulan'>
											<option value='01' {BULAN_01}>Januari</option>
											<option value='02' {BULAN_02}>Februari</option>
											<option value='03' {BULAN_03}>Maret</option>
											<option value='04' {BULAN_04}>April</option>
											<option value='05' {BULAN_05}>Mei</option>
											<option value='06' {BULAN_06}>Juni</option>
											<option value='07' {BULAN_07}>Juli</option>
											<option value='08' {BULAN_08}>Agustus</option>
											<option value='09' {BULAN_09}>September</option>
											<option value='10' {BULAN_10}>Oktober</option>
											<option value='11' {BULAN_11}>November</option>
											<option value='12' {BULAN_12}>Desember</option>
										</select>&nbsp;thn 20
										<input type='text' id='tahun' name='tahun' value='{TAHUN}' size='2' maxlength='2'>
									</td>
								</tr>
								<tr><td width=100>Kota Berangkat</td><td width=5>:</td><td><select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select></td></tr>
								<tr><td>Tambahan</td><td>:</td><td><input type='text' id='cari' name='cari' value='{CARI}' size=20 />&nbsp;<span class='noticeMessage'>(ex: no tiket, nama penumpang, no.hp,dll)</span></td></tr>
								<tr><td colspan=3 align='center' class='bannernormal'><input type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /></td></tr>
								<!--<tr>
									<td colspan=3 align='center'>
										<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
										<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
									</td>
								</tr>-->
							</table>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>

<table width='100%'>
	<tr>
		<td width='100%' align='left' colspan=13 valign='bottom'>{PAGING}</td>
	</tr>
	<!--<tr>
		<td colspan=13 align='center'>
		<br>
		<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
		<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
		</td>
	</tr>-->
</table>


<table  width='2000'>
	<tr>
		<th width=30  rowspan='2'>No</th>
		<th width=100 rowspan='2'>No.Tiket</th>
		<th width=150 rowspan='2'>Nama</th>
		<th width=100 rowspan='2'>Telp</th>
		<th width=100 rowspan='2'>Issued</th>
		<th width=100 rowspan='2'>Manifest</th>
		<th colspan='5'>Data Sebelum</th>
		<th colspan='5'>Data Sesudah</th>
		<th width=70 rowspan='2'>Diskon</th>
		<th width=100 rowspan='2'>Pemutasi</th>
		<th width=100 rowspan='2'>Waktu Mutasi</th>
	</tr>
	<tr>
		<th width=100>Tgl.Berangkat</th>
		<th width=50>Jam</th>
		<th width=100>Jadwal</th>
		<th width=50>No.Kursi</th>
		<th width=70>Harga Tiket</th>
		<th width=100>Tgl.Berangkat</th>
		<th width=50>Jam</th>
		<th width=100>Jadwal</th>
		<th width=50>No.Kursi</th>
		<th width=70>Harga Tiket</th>
	</tr>
  <!-- BEGIN ROW -->
  <tr class="{ROW.odd}">
		<td><div align="left">{ROW.no}</div></td>
		<td><div align="left">{ROW.no_tiket}</div></td>
		<td><div align="left">{ROW.nama}</div></td>
		<td><div align="left">{ROW.telp}</div></td>
		<td><div align="center">{ROW.issued}</div></td>
		<td><div align="left">{ROW.manifest}</div></td>
		<td><div align="center">{ROW.tgl_berangkat1}</div></td>
		<td><div align="center">{ROW.jam1}</div></td>
		<td><div align="center">{ROW.kode_jadwal1}</div></td>
		<td><div align="center">{ROW.no_kursi1}</div></td>
		<td><div align="right">{ROW.harga_tiket1}</div></td>
		<td><div align="center">{ROW.tgl_berangkat2}</div></td>
		<td><div align="center">{ROW.jam2}</div></td>
		<td><div align="center">{ROW.kode_jadwal2}</div></td>
		<td><div align="center">{ROW.no_kursi2}</div></td>
		<td><div align="right">{ROW.harga_tiket2}</div></td>
		<td><div align="right">{ROW.diskon}</div></td>
		<td><div align="center">{ROW.pemutasi}</div></td>
		<td><div align="center">{ROW.waktu_mutasi}</div></td>
  </tr>  
  <!-- END ROW -->
</table>

<table width='100%'>
	<tr>
		<td width='100%' align='left' colspan=13 valign='bottom'>{PAGING}</td>
	</tr>
	<!--<tr>
		<td colspan=13 align='center'>
		<br>
		<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
		<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
		</td>
	</tr>-->
</table>
