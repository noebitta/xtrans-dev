<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_omzet_bandara.php?sid={SID}",
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
          Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
					Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateTujuan("BDR");

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Bandara</td>
				<td colspan=2 align='right' valign='middle'>
					<br>
						<table>
							<tr><td class='bannernormal'>
								<table cellspacing=0 cellpadding=0 width='100%'>
									<tr>
										<td class='bannernormal'>Asal:&nbsp;</td><td><select name='asal' id='asal' onChange="getUpdateTujuan(this.value)"><option value="BDR">BANDARA SOETA (CGK) (BDR)</option> </select></td>
										<td class='bannernormal'>&nbsp;Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span></td>
										<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
										<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
										<td class='bannernormal' colspan=2>&nbsp;<input type="submit" value="cari" />&nbsp;</td>
									</tr>				
								</table>
							</td></tr>
						</table>
				</td>
			</tr>
			<tr>
				<td colspan=3>
					<table width='100%'>
						<tr>
							<td>
								<table>
									<tr>
										<td>
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
							<td align='right'>
								<!--<table>
									<tr>
										<td align='right' class='border'>
											<a href="{U_GRAFIK}"><img src="{TPL}images/icon_grafik.png" /></a>
											<a href="{U_GRAFIK}">Lihat Grafik</a>
										</td>
									</tr>
								</table>-->
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table class="border" width='100%' >
    <tr>
		 <th width=300>Waktu Transaksi</th>
		 <th width=200>Type</th>
		 <th width=200>Nama</th>
		 <th width=200>Asal</th>
		 <th width=200>Tujuan</th>
		 <th width=200>Harga Tiket</th>
		 <th width=200>Discount</th>
		 <th width=200>Total Bayar</th>
		 <th width=200>Jenis Pembayaran</th>
		 <th width=200>No Body</th>
		 <th width=200>Sopir</th>
		 <th width=200>Pax</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="center"><b>{ROW.waktu_transaksi}</b></div></td>
			 <td ><div align="left">{ROW.type}</div></td>
			 <td><div align="left" >{ROW.nama}</div></td>
			 <td><div align="left" >{ROW.asal}</div></td>
			 <td><div align="left" >{ROW.tujuan}</div></td>
			 <td><div align="left" >{ROW.harga_tiket}</div></td>
			 <td><div align="left" >{ROW.discount}</div></td>
			 <td><div align="left" >{ROW.bayar}</div></td>
			 <td><div align="left" >{ROW.jenis_pembayaran}</div></td>
			 <td ><div align="left">{ROW.no_body}</div></td>
			 <td ><div align="left">{ROW.sopir}</div></td>
			 <td ><div align="right">{ROW.pax}</div></td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>