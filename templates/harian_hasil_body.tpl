<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='80%'>
    <tr>
    <td align='center'>
       <h2>{JUDUL}</h2>
    </td>
    </tr>
    <tr>
    <td align="center">
  		Route : <b>{ROUTE}</b>
    </td>
    </tr>
    <tr>
      <td>
        <table width="100%" class="border">
          <tr>
						<th>No</th>
						<th>Jam</th>
            <th>Jum.Penumpang</th>
            <th>Harga</th>
            <th>Total</th>
          </tr>
          <!-- BEGIN ROW -->
          <tr bgcolor='D0D0D0'>
						<td align="right">{ROW.num}</td>
            <td align="center">{ROW.jam}</td>
            <td align="right" width="20%">{ROW.jlh}</td>
            <td align="right">{ROW.harga}</td>
            <td align="right">{ROW.total}</td>
          </tr>
          <!-- END ROW -->
					<tr bgcolor='FFFFFF'>
						<td colspan="5" align="right">
							Total Pendapatan: {TOTAL_PENDAPATAN}
						</td>
					</tr>
        </table>
      </td>
    </tr>
    </table>	
 </td>
</tr>
</table>