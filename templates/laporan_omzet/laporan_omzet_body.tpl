<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&kodearea='+kode_area.value+'&asal='+asal.value+'&tujuan='+tujuan.value+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateAsal(area){
		
		new Ajax.Updater("rewrite_asal","laporan_omzet.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kodearea="+area+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(area,asal){
		new Ajax.Updater("rewrite_tujuan","laporan_omzet.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=gettujuan&kodearea="+area+"&asal="+asal+"&tujuan={TUJUAN}",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	getUpdateAsal("{KODE_AREA}");
	getUpdateTujuan("{KODE_AREA}","{ASAL}");
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
	<td class="whiter" valign="middle" align="left">		
		<form action="{ACTION_SUBMIT}" method="post">
			<!--HEADER-->
			<table width='100%' cellspacing="0">
				<tr height=40 bgcolor='505050'>
					<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Resume Omzet Penumpang</td>
					<td class="bannerjudul">&nbsp;</td>
				</tr>
				<tr>
					<td colspan=2 align='center' valign='middle'>
						<table>
							<tr>
								<td class='border'><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan=2>
						<table width='100%'>
							<tr>
								<!--<td align='left'>
									<a href="{U_LAPORAN_OMZET_GRAFIK}"><img src="{TPL}images/icon_grafik.png" /></a>
									<a href="{U_LAPORAN_OMZET_GRAFIK}">Lihat Grafik</a>
								</td>-->
								<td align='right' valign='bottom'>
									<select id='kode_area' name='kode_area' onChange="getUpdateAsal(this.value);"><option value="">-semua area-</option>{OPT_AREA}</select>
									<span id='rewrite_asal'></span>
									<span id='rewrite_tujuan'></span>
									<br>
									{LIST_BULAN}
									&nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp;												
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!-- END HEADER-->
			<table class="border" width='100%' >
			<tr>
				<th width=100  rowspan=2>Tanggal</th>
				<th width=100 rowspan=2>Trip</th>
				<th colspan=9>Total Penumpang</th>
				<th width=100 rowspan=2>Pnp<br>/Trip</th>
				<th width=200 rowspan=2>Pendapatan (Rp.)</th>
			</tr>
			<tr>
			<th width=100>U</th>
			<th width=100>M</th>
			<th width=100>K</th>
			<th width=100>KK</th>
			<th width=100>G</th>
			<th width=100>O</th>
			<th width=100>RB</th>
			<th width=100>R</th>
			<th width=100>VR</th>
			<th width=100>T</th>
		 </tr>
			<!-- BEGIN ROW -->
			<tr class="{ROW.odd}">
				<td><div align="center"><font size=3 color='{ROW.font_color}'><b>{ROW.tgl}</b></font></div></td>
				<td><div align="right">{ROW.total_keberangkatan}</div></td>
				<td><div align="right" >{ROW.total_penumpang_u}</div></td>
				<td><div align="right" >{ROW.total_penumpang_m}</div></td>
				<td><div align="right" >{ROW.total_penumpang_k}</div></td>
				<td><div align="right" >{ROW.total_penumpang_kk}</div></td>
				<td><div align="right" >{ROW.total_penumpang_g}</div></td>
				<td><div align="right" >{ROW.total_penumpang_o}</div></td>
				<td><div align="right" >{ROW.total_penumpang_rb}</div></td>
				<td><div align="right" >{ROW.total_penumpang_r}</div></td>
				<td><div align="right" >{ROW.total_penumpang_vr}</div></td>
				<td style="background: yellow;color: black;"><div align="right" >{ROW.total_penumpang}</div></td>
				<td ><div align="right">{ROW.rata_pnp_per_trip}</div></td>
				<td ><div align="right">{ROW.omzet_net}</div></td>
			</tr>
			<!-- END ROW -->
			<tr style="background: yellow; font-weight: bold;">
     		<td align='center'>GRAND TOTAL</td>
     		<td align='right'>{GT_TRIP}</td>
     		<td align='right'>{GT_PNP_U}</td>
     		<td align='right'>{GT_PNP_M}</td>
     		<td align='right'>{GT_PNP_K}</td>
     		<td align='right'>{GT_PNP_KK}</td>
     		<td align='right'>{GT_PNP_G}</td>
     		<td align='right'>{GT_PNP_O}</td>
     		<td align='right'>{GT_PNP_RB}</td>
     		<td align='right'>{GT_PNP_R}</td>
     		<td align='right'>{GT_PNP_VR}</td>
     		<td align='right'>{GT_PNP_T}</td>
     		<td align='right'>&nbsp;</td>
     		<td align='right'>{GT_OMZ_NET}</td>
     </tr>
		</table>
		<table width='100%'>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
		<table width='100%'>
			<tr>
				<td align='left' valign='bottom' colspan=4>
					<table>
						<tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
						<tr><td>M</td><td>=</td><td>Penumpang Mahasiswa/Pelajar/Anak-anak/Lansia/Group</td></tr>
						<tr><td>K</td><td>=</td><td>Penumpang Khusus/Pelanggan Setia</td></tr>
						<tr><td>KK</td><td>=</td><td>Penumpang Keluarga Karyawan</td></tr>
						<tr><td>G</td><td>=</td><td>Penumpang Gratis</td></tr>
						<tr><td>O</td><td>=</td><td>Penumpang Online Tiketux</td></tr>
						<tr><td>RB</td><td>=</td><td>Penumpang Online Red Bus</td></tr>
						<tr><td>R</td><td>=</td><td>Return</td></tr>
						<tr><td>VR</td><td>=</td><td>Voucher Return</td></tr>
						<tr><td>T</td><td>=</td><td>Total Penumpang</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>