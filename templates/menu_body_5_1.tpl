<script type="text/javascript">
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<table width="100%" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">
		<h3 id='laporan_omzet'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Laporan Omzet Penumpang</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_CABANG}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_CABANG}"><span class="genmed">Per Cabang</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_JURUSAN}"><img src="{TPL}images/icon_laporan_jurusan.png" /></a>
					<br />
					<a href="{U_LAPORAN_JURUSAN}"><span class="genmed">Per Jurusan</span></a>
		    </td>

				<td align='center' width='25%'>
					<a href="{U_LAPORAN_JADWAL}"><img src="{TPL}images/icon_laporan_jadwal.png" /></a>
					<br />
					<a href="{U_LAPORAN_JADWAL}"><span class="genmed">Per Jadwal</span></a>
				</td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_OMZET}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_LAPORAN_OMZET}"><span class="genmed">Keseluruhan</span></a>
		    </td>

				<td align='center' width='25%'>
						&nbsp;
				</td>
			
			</tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_BANDARA}"><img src="{TPL}images/icon_laporan_jadwal.png" /></a>
					<br />
					<a href="{U_LAPORAN_BANDARA}"><span class="genmed">Bandara</span></a>
				</td>
			</tr>
		</table>
		<br><br>
		<h3 id='laporan_keuangan'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Laporan Setoran</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_UANG_CABANG}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_UANG_CABANG}"><span class="genmed">Rekap Per Cabang</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_REKAP_UANG_HARIAN}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_REKAP_UANG_HARIAN}"><span class="genmed">Rekap Uang Harian</span></a>
		    </td>

				<td align='center' width='25%'>
					<a href="{U_LAPORAN_SOPIR}"><img src="{TPL}images/icon_laporan_sopir.png" /></a>  
					<br />
					<a href="{U_LAPORAN_SOPIR}"><span class="genmed">Laporan<br>Insentif Sopir</span></a>
		    </td>

		    <td align='center' width='25%'>
					<a href="{U_LAPORAN_VOUCEHR_RETURN}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_LAPORAN_VOUCEHR_RETURN}"><span class="genmed">Voucher Return</span></a>
		    </td>
				
			</tr>
			<tr height=100 valign='top' align='center'>
        <td align='center' width='25%'>
          <a href="{U_LAPORAN_ASURANSI}"><img src="{TPL}images/icon_laporan_asuransi.png" /></a>
          <br />
          <a href="{U_LAPORAN_ASURANSI}"><span class="genmed">Rekap Premi Asuransi</span></a>
        </td>

		    <td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
			
			</tr>
		</table>
		<br><br>
 </td>
</tr>
</table>