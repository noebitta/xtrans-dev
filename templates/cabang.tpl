<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

var kode;

function TanyaHapus(id){
	kode=id;
	dlg_TanyaHapus.show();
}

function HapusData(){
    
  new Ajax.Request("cabang.php?sid={SID}", 
  {
    asynchronous: true,
    method: "get",
    parameters: "kode="+kode+"&mode=hapus",
    onLoading: function(request) 
    {
			
    },
    onComplete: function(request) 
    {
			if(request.responseText==''){
				window.location.reload();
			}
			else{
				alert(request.responseText);
			}
    },
    onSuccess: function(request) 
    {
       
		},
    onFailure: function(request) 
    {
       
    }
  })      
}

// global
var dlg_TanyaHapus,dlg_DataPengguna,dlg_TanyaHapusBtnYes,dlg_TanyaHapusBtnNo;

function init(e) {
  // inisialisasi variabel
  	
	//dialog tanya hapus__________________________________________________________________
	dlg_TanyaHapusBtnYes = document.getElementById("dlg_TanyaHapusBtnYes");
	dlg_TanyaHapusBtnNo = document.getElementById("dlg_TanyaHapusBtnNo");
	dlg_TanyaHapus = dojo.widget.byId("dlg_TanyaHapus");
	dlg_TanyaHapus.setCloseControl(dlg_TanyaHapusBtnYes);
  dlg_TanyaHapus.setCloseControl(dlg_TanyaHapusBtnNo);
	
}

dojo.addOnLoad(init);

</script>

<div dojoType="dialog" width="400" id="dlg_TanyaHapus" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="100" style="display: none;">
<form onsubmit="return false;">
<font color='FFFFFF'><h3>Konfirmasi Hapus</h3></font>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center">
		<br><br>
		Apakah anda yakin akan menghapus data ini?<br><br><br>
	</td>
<tr>
	<td align="center">
		<br>
		<input type="button" onclick="HapusData();" id="dlg_TanyaHapusBtnYes" value="&nbsp&nbsp;&nbsp;Ya&nbsp;&nbsp;&nbsp">
		<input type="button" id="dlg_TanyaHapusBtnNo" value="&nbsp;Tidak&nbsp;">
	</td>
</tr>
</table>
</form>
</div>

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <!-- MODUL -->
		<form action="{U_USER_SHOW}" method="post">
    <table width='100%'>
    <tr>
    <td valign="middle" align="center">
		<h1>DAFTAR CABANG</h1>
		{U_ADD}
    <table width='100%'>
		<tr>
      <td align='right'>
				<!--field cari-->
				<input type="text" name="txt_cari" id="txt_cari" value='{CARI}'>&nbsp;&nbsp;&nbsp;<input type="submit" value="Cari">
      </td>
    </tr>
    </table>
    <table width='100%' class="border">
    <tr>
      <th width='2%'>#</th>
			<th width='20%'>Kode</th>
			<th width='25%'>Cabang</th>
			<th width='25%'>Kota</th>
			<th width='10%'>Aksi</th>
    </tr>
     <!-- BEGIN ROW -->
     <tr bgcolor='D0D0D0'>
       <td class="{ROW.odd}"><div align="left">{ROW.no}</div></td>
       <td class="{ROW.odd}"><div align="left">{ROW.kode}</div></td>
       <td class="{ROW.odd}"><div align="left">{ROW.nama}</div></td>
			 <td class="{ROW.odd}"><div align="left">{ROW.kota}</div></td>
			 <td class="{ROW.odd}" align='center'>{ROW.act}</td>
     </tr>  
     <!-- END ROW -->
    </table>
		</form>
		{PESAN}
		{U_ADD}
 </td>
</tr>
</table>
