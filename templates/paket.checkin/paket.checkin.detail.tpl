<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
	}

	function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
	}
	
	function doCheckIn(){
	
		i=1;
		loop=true;
		list_dipilih="";
		
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				if(chk.checked){
					list_dipilih +=chk.value+",";
				}
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
		if(list_dipilih==""){
			alert("Anda harus memilih paket yang akan di Check-In!");
			exit;			
		}
		
		list_dipilih	= list_dipilih.substring(0,list_dipilih.length-1);
		
		if(confirm("Anda akan melakukan Check In daftar paket, klik 'OK' untuk melanjutkan prosesnya!")){
			
			new Ajax.Request("paket.checkin.detail.php?sid={SID}",{
			 asynchronous: true,
			 method: "post",
			 parameters: "mode=docheckin&listnotiket="+list_dipilih,
			 onLoading: function(request) 
			 {
					document.getElementById("loading").style.display = "block";
			 },
			 onComplete: function(request) 
			 {
					document.getElementById("loading").style.display = "none";
			 },
			 onSuccess: function(request) 
			 {
					window.location.reload();
			 },
			 onFailure: function(request) 
			 {
			 }
			})  
		}
		
		return false;
			
	}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form action="{ACTION_CARI}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr bgcolor='505050' height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Daftar Paket {KETERANGAN}</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
		</table>
		<center><b>#Manifest:</b>{NO_MANIFEST} | <b>Berangkat:</b>{BERANGKAT} | <b>Kode Jadwal:</b>{KODE_JADWAL}<br>
		<b>Driver:</b>{DRIVER} | <b>#Plat:</b>{NO_POLISI}</center>
		<table width='100%' >
			<tr style="height: 16px;">
				<td width='200' align='left'>
					&nbsp;<a href="#" onClick="javascript:history.back();"> << Kembali</a>&nbsp;|&nbsp;<a href="" onClick="return doCheckIn();">Check In</a>
				</td>
				<td align='center'><div id="loading" style="display: none;background-image: url('./templates/images/loading_bar.gif');width: 220px;width: 220px;height: 16px;">&nbsp;</div></td>
				<td width='200' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;
				</td>
			</tr>
		</table>
		<table>
			<tr>
       <th rowspan="2" width="30"></th>
       <th rowspan="2" width="120">#resi</th>
			 <th colspan='3'>Pengirim</th>
			 <th colspan='3'>Penerima</th>
			 <th rowspan="2" width="50">Berat<br>(Kg)</th>
			 <th rowspan="2" width="70">Harga</th>
			 <th rowspan="2" width="70">Diskon</th>
			 <th rowspan="2" width="70">Bayar</th>
			 <th rowspan="2" width="70">Layanan</th>
			 <th rowspan="2" width="70">Jenis Bayar</th>
			 <th rowspan="2" width="200">Remark</th>
     </tr>
     <tr>
 			 <th width="100">Nama</th>
			 <th width="100">Alamat</th>
			 <th width="70">Telp</th>
			 <th width="100">Nama</th>
			 <th width="100">Alamat</th>
			 <th width="70">Telp</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="center">{ROW.check}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
			 <td><div align="left">{ROW.nama_pengirim}</div></td>
			 <td><div align="left">{ROW.alamat_pengirim}</div></td>
			 <td><div align="left">{ROW.telp_pengirim}</div></td>
			 <td><div align="left">{ROW.nama_penerima}</div></td>
			 <td><div align="left">{ROW.alamat_penerima}</div></td>
			 <td><div align="left">{ROW.telp_penerima}</div></td>
			 <td><div align="right">{ROW.berat}</div></td>
			 <td><div align="right">{ROW.harga}</div></td>
			 <td><div align="right">{ROW.diskon}</div></td>
			 <td><div align="right">{ROW.bayar}</div></td>
			 <td><div align="center">{ROW.layanan}</div></td>
			 <td><div align="center">{ROW.jenis_bayar}</div></td>
			 <td><div align="left">{ROW.remark}</div></td>
     </tr>  
     <!-- END ROW -->
		</table>
 </td>
</tr>
</table>