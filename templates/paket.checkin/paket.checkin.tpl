<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateTujuan(kota){
		
		new Ajax.Updater("rewrite_tujuan","paket.checkin.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=gettujuan&kota="+kota+"&tujuan={TUJUAN}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateAsal(tujuan){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_asal","paket.checkin.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal={ASAL}&tujuan="+tujuan+"&mode=getasal",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	getUpdateTujuan("{KOTA}");
	getUpdateAsal("{TUJUAN}");
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40><td align='center' valign='middle' class="bannerjudul">&nbsp;Check-In Paket</td></tr>
			<tr class='banner' height=40>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>Kota Tujuan:&nbsp;<select onchange='getUpdateTujuan(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select></td>
							<td class='bannernormal'>Cabang Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div></td>
							<td class='bannernormal'>&nbsp;Asal:&nbsp;</td><td><div id='rewrite_asal'></div></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30>No</th>
			 <th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jadwal</a></th>
			 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Jadwal</a></th>
			 <th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Berangkat</a></th>
			 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Keterlambatan</a></th>
			 <th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>#Manifest</a></th>
			 <th width=200><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Sopir</a></th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Mobil</a></th>
			 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Jum.Pax</a></th>
			 <th width=150>Keterangan</th>
			 <th width=100>Act</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="center">{ROW.jadwal}</div></td>
			 <td ><div align="left">{ROW.kodejadwal}</div></td>
       <td ><div align="center">{ROW.berangkat}</div></td>
       <td ><div align="center" class='{ROW.flagterlambat}'>{ROW.keterlambatan}</div></td>
			 <td ><div align="left">{ROW.manifest}</div></td>
			 <td ><div align="left">{ROW.sopir}</div></td>
			 <td ><div align="left">{ROW.mobil}</div></td>
			 <td ><div align="right" style="background: green;color: white;"><b>{ROW.paket}</b></div></td>
			 <td ><div align="center">{ROW.keterangan}</div></td>
			 <td ><div align="center">{ROW.act}</div></td>
     </tr>
     <!-- END ROW -->
    </table>
	</form>
 </td>
</tr>
</table>