<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script language="JavaScript">
// komponen khusus dojo 
dojo.require("dojo.widget.Dialog");
	
function Start(page) {
OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=no,resizable=yes");
}

function cetakTiket(no_tiket){
  //mencetak tiket 	
	window.close();
	Start('tiket.php?sid={SID}&tiket_dipilih='+no_tiket+'&mode=cetak_tiket');
}

function bayarTiketDipilih(){
	  //mencetak tiket 
		
		if(document.getElementById('tgl_berangkat')){
			tgl_berangkat = document.getElementById('tgl_berangkat').value;
		}
		else{
			tgl_berangkat="";
		}
		
		if(document.getElementById('kode_jadwal')){
			kode_jadwal 	= document.getElementById('kode_jadwal').value; 
		}
		else{
			kode_jadwal="";
		}
		
		if(document.getElementById('layout_kursi')){
			layout_kursi 	= document.getElementById('layout_kursi').value; 
		}
		else{
			layout_kursi="";
		}
		
		i=1;
		loop=true;
		tiket_dipilih="";
		do{
			str_var='chk_tiket_'+i;
			if(chk_tiket=document.getElementById(str_var)){
				if(chk_tiket.checked){
					if(tiket_dipilih==""){
						tiket_dipilih +=chk_tiket.value;
					}
					else{
						tiket_dipilih +=","+chk_tiket.value;
					}
				}
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
		document.location="member_transaksi.php?sid={SID}&mode=blank&list_tiket="+tiket_dipilih+"&kode_jadwal="+kode_jadwal+"&tgl_berangkat="+tgl_berangkat+"&layout_kursi="+layout_kursi;
	
}

//function konfirmPassword(_tgl_berangkat,_kode_jadwal,_layout_kursi,_id_member,_id_kartu,_list_tiket){
function konfirmPassword(){	
	document.getElementById('password').value=""; 
	dlg_konf_password.show();
}
	
function init(e) {
	//dialog tanya hapus__________________________________________________________________
	dlg_konf_passwordBtnYes = document.getElementById("dlg_konf_passwordBtnYes");
	dlg_konf_passwordBtnNo = document.getElementById("dlg_konf_passwordBtnNo");
	dlg_konf_password = dojo.widget.byId("dlg_konf_password");
	dlg_konf_password.setCloseControl(dlg_konf_passwordBtnYes);
  dlg_konf_password.setCloseControl(dlg_konf_passwordBtnNo);
	
	document.forms.frm_input_kartu.id_kartu.focus();
}

dojo.addOnLoad(init);

</script>

<!--dialog konfirm password-->
<div dojoType="dialog" width="400" id="dlg_konf_password" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="100" style="display: none;">
<font color='FFFFFF'><h3>Otorisasi Transaksi Member</h3></font>
<form name="f_otorisasi" id="f_otorisasi" action="member_transaksi.php?sid={SID}&mode=proses_transaksi" method='post'>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center" colspan=3>
		Untuk melakukan transaksi member, silahkan masukkan password member.
	</td>
</tr>
<tr>
	<td>Password</td>
	<td>:</td>
  <td>
		<Input type='password' id='password' name='password' maxlength=6 />
		{PARAMETER}
	</td>
</tr>
<tr>
	<td align="center" colspan=3>
		<br>
		<input type="button" id="dlg_konf_passwordBtnNo" value="&nbsp;&nbsp;Cancel&nbsp;&nbsp;">
		<input type="submit"  id="dlg_konf_passwordBtnYes" value="&nbsp&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp">
	</td>
</tr>
</table>
</form>
</div>
<!--END  konfirm password-->

<table  width= "100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
		<td align='center'>
			<h2>TRANSAKSI MEMBER</h2>
	</tr>
	<tr>
		<td align='center'>{BODY}</td>
	</tr>
</table>
