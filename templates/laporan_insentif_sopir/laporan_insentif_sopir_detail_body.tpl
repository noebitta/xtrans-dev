<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Detail Manifest</td>
				<td align='right' valign='middle'></td>
			</tr>
			<tr>
				<td colspan=2 align='center'>
					</br>
					{DATA_SOPIR}
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td>
								<table class="border">
									<tr>
										<td><b>Manifest Utama</b></td><td width="1">:</td><td align="right"><b>{TOTAL_MANIFEST}</b> Manifest</td>
									</tr>
									<tr>
										<td><b>Manifest Transit</b></td><td width="1">:</td><td align="right"><b>{TOTAL_MANIFEST_TRANSIT}</b> Manifest</td>
									</tr>
									<tr>
										<td><b>Total Insentif</b></td><td width="1">:</td><td align="right">Rp. <b>{TOTAL_INSENTIF}</b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
			<th width=30>No</th>
			<th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>#Manifest</a></th>
			<th width=200><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Berangkat</a></th>
			<th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Kode Jadwal</a></th>
			<th width=200><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Jadwal</a></th>
			<th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Area</a></th>
			<th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Layout</a></th>
			<th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Jum.Pnp</a></th>
			<th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Jum.Pnp.Ins</a></th>
			<th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Tot.Ins</a></th>
			<th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Keterangan</a></th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="left">{ROW.no_spj}</div></td>
			 <td ><div align="center">{ROW.waktu_cetak}</div></td>
       <td ><div align="left">{ROW.kode_jadwal}</div></td>
       <td ><div align="center">{ROW.waktu_berangkat}</div></td>
       <td ><div align="center">{ROW.area}</div></td>
			 <td ><div align="right">{ROW.layout}</div></td>
			 <td ><div align="right">{ROW.jum_pnp}</div></td>
			 <td ><div align="right">{ROW.jum_pnp_insentif}</div></td>
			 <td ><div align="right">{ROW.total_insentif}</div></td>
			  <td ><div align="center">{ROW.keterangan}</div></td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>