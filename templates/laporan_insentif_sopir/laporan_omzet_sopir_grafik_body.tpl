<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	
	function setGrafik(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
</script>

<table width="100%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_FORM}" method="post" name='myform'>
		<table class="border" width='100%' >
		<tr>
			<td align='center' valign='middle' class="bannerjudul" height='50' width='25%'>Grafik Produktifitas Sopir</td>
			<td colspan=2 align='right' class="bannercari" valign='middle'>
				<table>
					<tr><td align='right'>
						&nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp;	
						<br>
						{LIST_BULAN}								
					</td></tr>
				</table>
			</td>
		</tr>
    <tr>
      <td align='middle' colspan=3>
				<script type="text/javascript" src="chart/js/swfobject.js"></script>
				<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" 
				width="1000" height="500" id="ie_chart" align="middle">
				<param name="allowScriptAccess" value="sameDomain" />
				<param name="movie" value="chart/open-flash-chart.swf?width=1000&height=500&data={DATA_GRAFIK}" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#FFFFFF" />
				<embed src="chart/open-flash-chart.swf?data={DATA_GRAFIK_BULANAN}" quality="high" bgcolor="#FFFFFF" width="1000" height="500" name="chart" align="middle" allowScriptAccess="sameDomain" 
				type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" id="chart"/>
				</object>
			</td>
    </tr>
    <tr>
      <td align='middle' colspan=3>
				<script type="text/javascript" src="chart/js/swfobject.js"></script>
				<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" 
				width="1000" height="500" id="ie_chart" align="middle">
				<param name="allowScriptAccess" value="sameDomain" />
				<param name="movie" value="chart/open-flash-chart.swf?width=1000&height=500&data={DATA_GRAFIK1}" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#FFFFFF" />
				<embed src="chart/open-flash-chart.swf?data={DATA_GRAFIK_TAHUNAN}" quality="high" bgcolor="#FFFFFF" width="1000" height="500" name="chart" align="middle" allowScriptAccess="sameDomain" 
				type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" id="chart"/>
				</object>
			</td>
    </tr>
    </table>
		<table width='100%' >
			<tr>
				<td align='right' width='100%' colspan=3>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>