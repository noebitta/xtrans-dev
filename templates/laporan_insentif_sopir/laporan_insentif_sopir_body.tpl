<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=1000,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Insentif Sopir</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>
								<select id='area' name='area'><option value=''>-semua area-</option>{OPT_AREA}</select>
							</td>
							<td class='bannernormal'>
								&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
								&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
								&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
								<input type="submit" name="btn_cari" value="cari" />&nbsp;								
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td>
								<table class="border">
									<tr>
										<td><b>Jumlah Sopir</b></td><td width="1">:</td><td align="right"><b>{TOTAL_SOPIR}</b> Sopir</td>
									</tr>
									<tr>
										<td><b>Manifest Utama</b></td><td width="1">:</td><td align="right"><b>{TOTAL_MANIFEST}</b> Manifest</td>
									</tr>
									<tr>
										<td><b>Manifest Transit</b></td><td width="1">:</td><td align="right"><b>{TOTAL_MANIFEST_TRANSIT}</b> Manifest</td>
									</tr>
									<tr>
										<td><b>Total Insentif</b></td><td width="1">:</td><td align="right">Rp. <b>{TOTAL_INSENTIF}</b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30>No</th>
			 <th width=200><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Nama Sopir</a></th>
			 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>NRP</a></th>
			 <th width=100><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Total Hari Kerja</a></th>
			 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Manifest Utama</a></th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Manifest Transit</a></th>
			 <th width=200><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Total Pnp Insentif</a></th>
			 <th width=200><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Total Insentif</a></th>
			 <th width=100><a class="th">Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="left">{ROW.nama}</div></td>
			 <td ><div align="left">{ROW.nrp}</div></td>
       <td ><div align="right">{ROW.total_hari_kerja}</div></td>
       <td ><div align="right">{ROW.total_manifest_utama}</div></td>
			  <td ><div align="right">{ROW.total_manifest_transit}</div></td>
			 <td ><div align="right">{ROW.total_penumpang}</div></td>
			 <td ><div align="right">{ROW.total_insentif}</div></td>
       <td ><div align="center"><a href="#" onClick="{ROW.act}">Detail</a></td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>