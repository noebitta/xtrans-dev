<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

function pilihData(kodepelanggan){
	
	window.opener.setDataPelanggan(kodepelanggan);
	window.close();
	return false;
		
}

function setSortId(){
	listHrefSort = [{ARRAY_SORT}];
	
	for (i=0;i<listHrefSort.length;i++){
		document.getElementById("sort"+(i+1)).href=listHrefSort[i];
	}
	
}

function init(e){
	//control dialog paket
	setSortId();
}

dojo.addOnLoad(init);
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<div style="width: 100%; height: 40px; background: #000054;">
			<div class="bannerjudul" style="float: left;margin: 6px 0px 0px 6px;">Pelanggan Paket</div>
			<div style="float: right;color: white;margin: 10px 6px 0px 0px;">
				<form action="{ACTION_CARI}" method="post">
					Filter:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size="50" placeholder="masukkan kata yang ingin dicari" />&nbsp;<input type="submit" value="cari" />
				</form>
			</div>
		</div>
		<br>
		<div align='left' style="float: right;">{PAGING}</div>
		<table width='100%' class="border" style="margin-top: 20px;margin-bottom: 5px;">
    <tr>
       <th width=30>No</th>
			 <th width=100><a class="th" id="sort1"  href="#">Kode Pelanggan</a></th>
			 <th width=150><a class="th" id="sort2"  href="#">Nama Pelanggan</a></th>
			 <th width=150><a class="th" id="sort3"  href="#">Alamat</a></th>
			 <th width=100><a class="th" id="sort4"  href="#">Contact Person</a></th>
			 <th width=100><a class="th" id="sort5"  href="#">Telp CP</a></th>
			 <th width=70> <a class="th" id="sort6"  href="#">Discount</a></th>
			 <th width=100><a class="th" id="sort7"  href="#">Habis Kontrak</a></th>
			 <th width=70> <a class="th" id="sort8" href="#">Status</a></th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
			 <td align="center">{ROW.kodepelanggan}</td>
			 <td align="center">{ROW.namapelanggan}</td>
			 <td align="left">{ROW.alamat}</td>
			 <td align="left">{ROW.contactperson}</td>
			 <td align="center">{ROW.telpcp}</td>
			 <td align="right">{ROW.discount}</td>
			 <td align="center">{ROW.habiskontrak}</td>
       <td align="center">{ROW.status}</td>
     </tr>  
     <!-- END ROW -->
    </table>
		<div style="display:{DISPLAY_NO_DATA};width: 100%;background: yellow;font-size: 16px;">data tidak ditemukan</div>
		<div align='left' style="float: right;">{PAGING}</div>
 </td>
</tr>
</table>