<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

filePath = '{TPL}js/dropdowncalendar/images/';

function hapusData(kodepelanggan){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		new Ajax.Request("paket.pelanggan.control.php?sid={SID}",{
	   asynchronous: true,
	   method: "post",
	   parameters: "mode=delete&kodepelanggan="+kodepelanggan,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function simpan(){
	
	isvalid	= true;
	
	if(namapelanggan.value==''){
		namapelanggan.style.background='red';
		isvalid=false;
	}
	
	if(contactperson.value==''){
		contactperson.style.background='red';
		isvalid=false;
	}
	
	if(telpcontactperson.value==''){
		telpcontactperson.style.background='red';
		isvalid=false;
	}
	
	if(discount.value==''){
		discount.style.background='red';
		isvalid=false;
	}
	
	if(!isvalid){
		exit;
	}
	
	new Ajax.Request("paket.pelanggan.control.php?sid={SID}",{
	 asynchronous: true,
	 method: "post",
	 parameters: "mode=save"+
		"&kodepelanggan="+hdnkodepelanggan.value+
		"&namapelanggan="+namapelanggan.value+
		"&alamatpelanggan="+alamatpelanggan.value+
		"&contactperson="+contactperson.value+
		"&telpcontactperson="+telpcontactperson.value+
		"&discount="+discount.value+
		"&tglhabiskontrak="+tglhabiskontrak.value+
		"&isaktif="+(isaktif.checked?1:0),
	 onLoading: function(request) 
	 {
	 },
	 onComplete: function(request) 
	 {
		
	 },
	 onSuccess: function(request) 
	 {
		dlg_detail_data.show();
		window.location.reload();
	},
	 onFailure: function(request) 
	 {
	 }
	});  
	
	return false;
		
}

function showDetailData(kodepelanggan){
	
	new Ajax.Request("paket.pelanggan.control.php?sid={SID}",{
	 asynchronous: true,
	 method: "post",
	 parameters: "mode=showdatadetail"+
		"&kodepelanggan="+kodepelanggan,
	 onLoading: function(request) 
	 {
	 },
	 onComplete: function(request) 
	 {
		
	 },
	 onSuccess: function(request) 
	 {
		eval(request.responseText);
		
		namapelanggan.style.background='white';
		contactperson.style.background='white';
		telpcontactperson.style.background='white';
		discount.style.background='white';
		showisaktif.style.display	='block';
		
		dlg_detail_data.show();
	},
	 onFailure: function(request) 
	 {
	 }
	});  
	
	return false;
		
}

function ubahStatus(kodepelanggan){
		
		new Ajax.Request("paket.pelanggan.control.php?sid={SID}",{
	   asynchronous: true,
	   method: "post",
	   parameters: "mode=ubahstatus"+
			"&kodepelanggan="+kodepelanggan,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function showDialogDetailData(){
	showkodepelanggan.innerHTML="NaN";
	hdnkodepelanggan.value="";
	namapelanggan.value	= "";
	alamatpelanggan.value	= "";
	contactperson.value	= "";
	telpcontactperson.value	= "";
	discount.value	= "";
	tglhabiskontrak.value	= "{TGL_HABIS_KONTRAK}";
	
	namapelanggan.style.background='white';
	contactperson.style.background='white';
	telpcontactperson.style.background='white';
	discount.style.background='white';
	showisaktif.style.display	='none';
	isaktif.checked=false;
	
	dlg_detail_data.show();
}

function setSortId(){
	listHrefSort = [{ARRAY_SORT}];
	
	for (i=0;i<listHrefSort.length;i++){
		document.getElementById("sort"+(i+1)).href=listHrefSort[i];
	}
	
}

function init(e){
	//control dialog paket
	dlg_detail_data	= dojo.widget.byId("dialogdetaildata");
	setSortId();
}

dojo.addOnLoad(init);
	
</script>

<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>

<!--dialog detail data-->
<div dojoType="dialog" id="dialogdetaildata" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<div style="width: 400px;height: 370px;background: white;">
	<div style="font-size: 16px;text-transform: uppercase;font-weight: bold;text-align: center;"><br>Data Pelanggan Paket</div>
	<br>
	<div style="width: 100%;text-align: center">
		<div style="float: left;margin-left: 20px;width: 120px;">Kode Pelanggan</div><div ><b><span id="showkodepelanggan" name="showkodepelanggan"></span></b><input type="hidden" id="hdnkodepelanggan" name="hdnkodepelanggan"/></div><br>
		<div style="float: left;margin-left: 20px;width: 120px;">Nama Pelanggan</div><div><input type="text" id="namapelanggan" name="namapelanggan" onfocus="this.style.background='white'"/></div><br>
		<div style="float: left;margin-left: 20px;width: 120px;">Alamat Pelanggan</div><div><input type="text" id="alamatpelanggan" name="alamatpelanggan" /></div><br>
		<div style="float: left;margin-left: 20px;width: 120px;">Contact Person</div><div><input type="text" id="contactperson" name="contactperson" onfocus="this.style.background='white'"/></div><br>
		<div style="float: left;margin-left: 20px;width: 120px;">Telp. CP</div><div><input type="text" id="telpcontactperson" name="telpcontactperson" onfocus="this.style.background='white'" onkeypress='validasiNoTelp(event);'/></div><br>
		<div style="float: left;margin-left: 20px;width: 120px;">Discount</div><div><input type="text" id="discount" name="discount" onfocus="this.style.background='white'" onkeypress='validasiAngka(event);' /><br><span style="font-size: 10px;">Discount<=1:Persentase,Discount>1:Rupiah</1=Persentase,></span></div><br>
		<div style="float: left;margin-left: 20px;width: 120px;">Tgl.Habis Kontrak</div><div><input id="tglhabiskontrak" name="tglhabiskontrak" type="text" readonly='yes' size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TBL_HABIS_KONTRAK}"></div>
		<br>
		<span id="showisaktif" name="showisaktif" style="display: block;"><input id="isaktif" name="isaktif" type="checkbox" {ISAKTIF}/>&nbsp; Aktif<br></span>
		<br>
		<input type="button" value="&nbsp;Cancel&nbsp;" onClick="dlg_detail_data.hide();"> &nbsp;
		<input type="button" value="&nbsp;Proses&nbsp;" onclick="simpan();" >
	</div>
</div>
</form>
</div>
<!--END dialog detail data-->

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<div style="width: 100%; height: 40px; background: #000054;">
			<div class="bannerjudul" style="float: left;margin: 6px 0px 0px 6px;">Pelanggan Paket</div>
			<div style="float: right;color: white;margin: 10px 6px 0px 0px;">
				<form action="{ACTION_CARI}" method="post">
					Filter:&nbsp;<select id="status" name="status"><option value="" {OPT_STATUS_SEL}>-semua-</option><option value="0" {OPT_STATUS_SEL0}>Normal</option><option value="1" {OPT_STATUS_SEL1}>Habis Kontrak</option></select>&nbsp;
					<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size="50" placeholder="masukkan kata yang ingin dicari" />&nbsp;<input type="submit" value="cari" />
				</form>
			</div>
		</div>
		<br>
		<div align='left' style="float: left;"><a href="#" onClick="showDialogDetailData();false;">[+]Tambah Pelanggan</a></div>
		<div align='left' style="float: right;">{PAGING}</div>
		<table width='100%' class="border" style="margin-top: 20px;margin-bottom: 5px;">
    <tr>
       <th width=30>No</th>
			 <th width=100><a class="th" id="sort1"  href="#">Kode Pelanggan</a></th>
			 <th width=150><a class="th" id="sort2"  href="#">Nama Pelanggan</a></th>
			 <th width=150><a class="th" id="sort3"  href="#">Alamat</a></th>
			 <th width=100><a class="th" id="sort4"  href="#">Contact Person</a></th>
			 <th width=100><a class="th" id="sort5"  href="#">Telp CP</a></th>
			 <th width=70> <a class="th" id="sort6"  href="#">Discount</a></th>
			 <th width=100><a class="th" id="sort7"  href="#">Habis Kontrak</a></th>
			 <th width=100><a class="th" id="sort8"  href="#">Waktu Didaftarkan</a></th>
			 <th width=100><a class="th" id="sort9"  href="#">Didaftarkan oleh</a></th>
			 <th width=70> <a class="th" id="sort10" href="#">Status</a></th>
			 <th width=70> <a class="th" id="sort11" href="#">Aktif</a></th>
			 <th width=100>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
			 <td align="left">{ROW.kodepelanggan}</td>
			 <td align="center">{ROW.namapelanggan}</td>
			 <td align="left">{ROW.alamat}</td>
			 <td align="left">{ROW.contactperson}</td>
			 <td align="center">{ROW.telpcp}</td>
			 <td align="right">{ROW.discount}</td>
			 <td align="center">{ROW.habiskontrak}</td>
			 <td align="center">{ROW.waktudaftar}</td>
			 <td align="center">{ROW.didaftarkanoleh}</td>
       <td align="center">{ROW.status}</td>
			 <td align="center">{ROW.aktif}</td>
       <td align="center">{ROW.action}</td>
     </tr>  
     <!-- END ROW -->
    </table>
		<div style="display:{DISPLAY_NO_DATA};width: 100%;background: yellow;font-size: 16px;">data tidak ditemukan</div>
		<div align='left' style="float: right;">{PAGING}</div>
    <div align='left'><a href="#" onClick="showDialogDetailData();false;">[+]Tambah Pelanggan</a></div>
 </td>
</tr>
</table>