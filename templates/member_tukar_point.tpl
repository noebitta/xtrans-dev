<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

function validasiAngka(objek){
	temp_nilai=objek.value*0;
	nama_objek=objek.name;
	
	if(temp_nilai!=0){
		alert(nama_objek+" harus angka!");
		objek.setFocus;exit;
	}
	
	if(objek.value<=0){
		alert(nama_objek+" tidak boleh bernilai minus");
		objek.setFocus;exit;
	}
	
}

function FormatUang(uang,separator){
	len_uang = String(uang).length;
	return_val='';
	for (i=len_uang;i>=0;i--){
		if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

		return_val =String(uang).substring(i,i-1)+return_val;
	}
	
	return return_val;
}

function showKonfirmasi(){
	dlg_konfirmasi.show();
}

function tukarPoint(){
	id_promo_poin	= document.getElementById('opt_tukar_point').value;
	id_kartu			= document.getElementById('id_kartu').value;
	document.location="member_tukar_point.php?sid={SID}&mode=tukar&id_promo_poin="+id_promo_poin+"&id_kartu="+id_kartu;
	
}

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=no,resizable=yes");
}

function cetakKwitansi(id_topup){
  //mencetak kwitansi
  Start("member_topup_kwitansi.php?sid={SID}&id_topup="+id_topup);
}

function init(e) {
	
	if(document.getElementById('id_topup')){
		if(document.getElementById('id_topup').value!='' && document.getElementById('id_topup').value!='false'){
			cetakKwitansi(document.getElementById('id_topup').value);
			document.getElementById('id_topup').value='';
		}
	}
	
	//control dialog konfirmasi
	dlg_konfirmasi 				= dojo.widget.byId("dlg_konfirmasi");
  dlg_konfirmasiBtnYes 	= document.getElementById("dlg_konfirmasiBtnYes");
  dlg_konfirmasiBtnNo 	= document.getElementById("dlg_konfirmasiBtnNo");
  dlg_konfirmasi.setCloseControl(dlg_konfirmasiBtnYes);
  dlg_konfirmasi.setCloseControl(dlg_konfirmasiBtnNo);
	
	document.forms.frm_input_kartu.id_kartu.focus();
	
	
}

dojo.addOnLoad(init);



</script>

<div dojoType="dialog" width="400" id="dlg_konfirmasi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="100" style="display: none;">
<form onsubmit="return false;">
<font color='FFFFFF'><h3>Konfirmasi</h3></font>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center">
		<br>
		Silahkan klik OK untuk menfkonfirmasi proses ini
		<br>
	</td>
<tr>
	<td align="center">
		<br> 
			<input type="button" onClick='tukarPoint();' id="dlg_konfirmasiBtnYes" value="&nbsp;&nbsp;OK&nbsp;&nbsp">
			<input type="button" id="dlg_konfirmasiBtnNo" value="&nbsp;Tidak&nbsp;">
	</td>
</tr>
</table>
</form>
</div>

<input type='hidden' id='hdn_asc' value='0' >

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
	<tr>
		<th align='left'>Welcome {USERNAME},</th>
	</tr>
	<tr>
		<td align='left' class="indexer">{BCRUMP}</td>
	</tr>
	<tr>
		<td class="whiter" valign="middle" align="center">
			<table  width= "700" height='400' border="0" cellpadding="0" cellspacing="0">
			  <tr>
					<td align='center'>
						<h2>Tukar point</h2>
				</tr>
				<tr>
					<td align='center' valign='top'>{BODY}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
