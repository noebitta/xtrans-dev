<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
		
	}
	
	function init(e){
		setSortId();
	}
	
	dojo.addOnLoad(init);
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<form action="{ACTION_CARI}" method="post" name="my_form">
			<!--HEADER-->
			<table width='100%' cellspacing="0">
				<tr class='banner' height=40>
					<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Tiket Gratis</td>
					<td align='right' valign='middle'>
						<table>
							<tr>
								<td class='bannernormal'>Periode:&nbsp;</td><td><select name='is_today' id='is_today' onChange="if(this.value=='1'){tanggal_mulai.disabled=true;tanggal_akhir.disabled=true;}else{tanggal_mulai.disabled=false;tanggal_akhir.disabled=false;}"><option value="1" {IS_TODAY1}>Hari ini</option><option value="0" {IS_TODAY0}>Hari lalu</option></select></td>
								<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
								<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
								<td class='bannernormal'>Cari:<input type="text" id="cari" name="cari" value="{CARI}" /></td>	
								<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
							</tr>
						</table>
						<script type="text/javascript">
							if("{IS_TODAY1}"=="selected"){
								document.getElementById("tanggal_mulai").disabled=true;
								document.getElementById("tanggal_akhir").disabled=true;
							}
							else{
								document.getElementById("tanggal_mulai").disabled=false;
								document.getElementById("tanggal_akhir").disabled=false;
							}
						</script>
					</td>
				</tr>
				<tr>
					<td colspan=2 align='center' valign='middle'>
						<table>
							<tr>
								<td>
									<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!-- END HEADER-->
		</form>
		<table width='100%'>
		<tr><td colspan=9 align='center' height=20><span id='loading_proses' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='909090' size=2>sedang memposes...</font></span></td></tr>
    <tr>
			<td width='100%' align='right' colspan=13 valign='bottom'>{PAGING}</td>
		</tr>
		<tr>
      <th width=30>No</th>
			<th width=100><a class="th" id="sort1" href='#'>Tgl.Berangkat</a></th>
			<th width=100><a class="th" id="sort2" href='#'>No.Tiket</a></th>
			<th width=50><a class="th" id="sort3" href='#'>Jam</a></th>
			<th width=100><a class="th" id="sort4" href='#'>Kode Jadwal</a></th>
			<th width=50><a class="th" id="sort5" href='#'>No.Kursi</a></th>
			<th width=200><a class="th" id="sort6" href='#'>Nama</a></th>
			<th width=100><a class="th" id="sort7" href='#'>Telp</a></th>
			<th width=100><a class="th" id="sort8" href='#'>Cetak Tiket</a></th>
			<th width=200><a class="th" id="sort9" href='#'>Tiket by</a></th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
			<td align="center">{ROW.no}</div></td>
			<td align="center">{ROW.tgl_berangkat}</div></td>
			<td align="center">{ROW.no_tiket}</div></td>
			<td align="center">{ROW.jam}</div></td>
			<td align="center">{ROW.kode_jadwal}</div></td>
			<td align="center">{ROW.no_kursi}</div></td>
			<td align="center">{ROW.nama}</div></td>
			<td align="center">{ROW.telp}</div></td>
			<td align="center">{ROW.waktu_cetak_tiket}</div></td>
			<td align="center">{ROW.pencetak_tiket}</div></td>
    </tr>  
    <!-- END ROW -->
  </table>
  <table width='100%'>
		<tr>
			<td width='100%' align='right' colspan=13 valign='bottom'>{PAGING}</td>
		</tr>
		<tr>
			<td colspan=13 align='center'>
				<br><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
			</td>
		</tr>
	</table>
 </td>
</tr>
</table>