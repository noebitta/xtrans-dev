<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	Element.hide('id_member_invalid');
	Element.hide('nama_invalid');
	Element.hide('tempat_lahir_invalid');
	Element.hide('no_ktp_invalid');
	Element.hide('alamat_invalid');
	Element.hide('kota_invalid');
	Element.hide('telp_invalid');
	Element.hide('hp_invalid');
	Element.hide('cabang_invalid');
	
	id_member			= document.getElementById('id_member');
	nama					= document.getElementById('nama');
	tempat_lahir	= document.getElementById('tempat_lahir');
	noktp					= document.getElementById('noktp');
	alamat				= document.getElementById('alamat');
	kota					= document.getElementById('kota');
	telp					= document.getElementById('telp');
	hp						= document.getElementById('hp');
	cabang_daftar	= document.getElementById('cabang_daftar');
	
	valid=true;
	
	if(id_member.value==''){
		valid=false;
		Element.show('id_member_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(tempat_lahir.value==''){
		valid=false;
		Element.show('tempat_lahir_invalid');
	}
	
	if(noktp.value==''){
		valid=false;
		Element.show('no_ktp_invalid');
	}
	
	if(alamat.value==''){
		valid=false;
		Element.show('alamat_invalid');
	}
	
	if(kota.value==''){
		valid=false;
		Element.show('kota_invalid');
	}
	
	if(!cekValue(hp.value) || hp.value==''){	
		valid=false;
		Element.show('hp_invalid');
	}	
	
	if(!cekValue(telp.value)){	
		valid=false;
		Element.show('telp_invalid');
	}
	
	if(cabang_daftar.value==""){	
		valid=false;
		Element.show('cabang_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

if({CETAKKARTU}){
	Start("pengaturan_member_kartu.php?sid={SID}&id={ID_MEMBER_CETAK}");
}

</script>

<form name="frm_data" action="{U_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Member</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='1000'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top'>
				<table width='500' valign='top'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr><td colspan=3><h3><u>Data Member</u></h3></td></tr>
					<tr>
			      <input type="hidden" name="id_member" value="{ID_MEMBER}">
						<td width='200'><u>Kode Member</u></td><td width='5'>:</td>
						<td>
							<input type="text" id="id_member" name="id_member" value="{ID_MEMBER}" maxlength=25 onChange="Element.hide('id_member_invalid');" {READONLY}>
							<span id='id_member_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Nama</u></td><td>:</td>
						<td>
							<input type="text" id="nama" name="nama" value="{NAMA}" maxlength=50 onChange="Element.hide('nama_invalid');">
							<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
						<td>Jenis Kelamin</td><td>:</td>
						<td>
							<select id="jenis_kelamin" name="jenis_kelamin">
								<option value=0 {JK_0}>Laki-laki</option>
								<option value=1 {JK_1}>Perempuan</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Kategori Member</td><td>:</td>
						<td>
							<select id="kategori_member" name="kategori_member">
								<option value=0 {KATEGORI_0}>Member tidur</option>
								<option value=1 {KATEGORI_1}>Member aktif</option>
								<option value=2 {KATEGORI_2}>VIP</option>
							</select>
						</td>
					</tr>
					<tr>
			      <td><u>Tempat Lahir</u></td><td>:</td>
						<td>
							<input type="text" id="tempat_lahir" name="tempat_lahir" value="{TEMPAT_LAHIR}" maxlength=30 onChange="Element.hide('tempat_lahir_invalid');">
							<span id='tempat_lahir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Tanggal Lahir</u></td><td>:</td>
						<td>
							<input id="tgl_lahir" name="tgl_lahir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_LAHIR}">
						</td>
			    </tr>
					<tr>
			      <td><u>No. KTP</u></td><td>:</td>
						<td>
							<input type="text" id="noktp" name="noktp" value="{NO_KTP}" maxlength=30 onChange="Element.hide('no_ktp_invalid');">
							<span id='no_ktp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td valign='top'><u>Alamat</u></td><td  valign='top'>:</td>
						<td>
							<textarea name="alamat" id="alamat" cols="30" rows="3"  maxlength=150 onChange="Element.hide('alamat_invalid');">{ALAMAT}</textarea>
							<span id='alamat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
			      <td><u>Kota</u></td><td>:</td>
						<td>
							<input type="text" id="kota" name="kota" value="{KOTA}" maxlength=30 onChange="Element.hide('kota_invalid');">
							<span id='kota_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Kodepos</td><td>:</td>
						<td>
							<input type="text" id="kode_pos" name="kode_pos" value="{KODE_POS}" maxlength=6>
						</td>
			    </tr>
				</table>
			</td>
			<td height=1 bgcolor='EFEFEF'></td>
			<td align='center' valign='top'>
				<table width=500>
					<tr><td><h2>&nbsp;</h2></td></tr>
					<tr><td colspan=3><h3>&nbsp;</h3></td></tr>
					<tr>
			      <td width='200'><u>Tanggal Registrasi</u></td><td width='5'>:</td>
						<td>
							<input id="tgl_registrasi" name="tgl_registrasi" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_REGISTRASI}">
						</td>
			    </tr>
					<tr>
			      <td><u>Handphone</u></td><td>:</td>
						<td>
							<input type="text" id="hp" name="hp" value="{HP}" maxlength=20 onChange="Element.hide('hp_invalid');">
							<span id='hp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Telp</td><td>:</td>
						<td>
							<input type="text" id="telp" name="telp" value="{TELP}" maxlength=20 onChange="Element.hide('telp_invalid');">
							<span id='telp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Email</td><td>:</td>
						<td>
							<input type="text" id="email" name="email" value="{EMAIL}" maxlength=30 />
						</td>
			    </tr>
					<tr>
			      <td>Pekerjaan</td><td>:</td>
						<td>
							<input type="text" id="pekerjaan" name="pekerjaan" value="{PEKERJAAN}" maxlength=50 />
						</td>
			    </tr>
					<!--<tr>
			      <td><u>Berlaku Hingga</u></td><td>:</td>
						<td>
							<input id="tgl_berlaku" name="tgl_berlaku" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERLAKU}">
						</td>
			    </tr>-->
					<tr>
						<td>Cabang Daftar</td><td>:</td>
						<td>
							<select id="cabang_daftar" name="cabang_daftar" onChange="Element.hide('cabang_invalid');">
								{OPT_CABANG_DAFTAR}
							</select>
							<span id='cabang_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
						<td>Status Aktif</td><td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" id='submode' name="submode" value="{SUB}">
				<br>
				<!-- BEGIN iscetakkartu -->
				<input type="checkbox" name="cetakkartu" /><b>CETAK KARTU SEMENTARA</b><br><br>
				<!-- END iscetakkartu -->
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>