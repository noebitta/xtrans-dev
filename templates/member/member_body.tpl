<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
		
		new Ajax.Request("pengaturan_member.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_member="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_member.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&id_member="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

</script>

<div class="wrapperheader" style="height:50px;">
  <div class="headertittle">&nbsp;Master Member</div>
  <div class="headerfilter" style="padding-top: 7px;text-align: right;">
    <form action="{ACTION_CARI}" method="post">
      <select id="filtertanggal" name="filtertanggal" onchange="toggleFilterByTglReg(this.value);"><option value="">-tanpa filter-</option><option value="0">Tanggal Registrasi</option><option value="1">Tanggal Terakhir Transaksi</option></select>
      <span id="formtanggal" style="display:none;margin-left: 0px">
        &nbsp;
        <input id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
        &nbsp; s/d &nbsp;<input id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
      </span>
      &nbsp;|
      &nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
      <input type="submit" value="cari" />&nbsp;
    </form>
  </div>
</div>
<div><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></div>

<div style="float: left;padding-bottom: 2px;">
  <a href="{U_MEMBER_ADD}">[+]Tambah Member</a>&nbsp;|&nbsp;
  <a href="" onClick="return hapusData('');">[-]Hapus Member</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  <a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
  <a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
</div>
<script type="text/javascript">
  function toggleFilterByTglReg(filterby){
    if(filterby==""){
      document.getElementById("formtanggal").style.display	= "none";
    }
    else{
      document.getElementById("formtanggal").style.display = "inline-block";
      //document.getElementById("formtanggal").style.position	= "fixed";
    }
  }

  toggleFilterByTglReg("{FILTER_BY}");

  filtertanggal.value="{FILTER_BY}";

</script>
<div class="wrappertabledata">
  <table width="1320" class="border">
    <tr>
      <th width="30"></th>
      <th width="100"></th>
      <th width="50">No</th>
      <th width="170"><a class="th" href='{A_SORT_BY_NAMA}'>Nama Member</a></th>
      <th width="100"><a class="th" href='{A_SORT_BY_KODE}'>Kode</a></th>
      <th width="250"><a class="th" href='{A_SORT_BY_ALAMAT}'>Alamat</a></th>
      <th width="100"><a class="th" href='{A_SORT_BY_HP}'>HP</a></th>
      <th width="100"><a class="th" href='{A_SORT_BY_EMAIL}'>Email</a></th>
      <th width="100"><a class="th" href='{A_SORT_BY_TGL_DAFTAR}'>Tgl.Daftar</a></th>
      <th width="70" ><a class="th" href='{A_SORT_BY_TGL_BERANGKAT}'>Tgl.Berangkat</a></th>
      <th width="70" ><a class="th" href='{A_SORT_BY_POINT}'>Point</a></th>
      <th width="70" ><a class="th" href='{A_SORT_BY_FREKWENSI}'>Freq.</a></th>
      <th width="70" ><a class="th" href='{A_SORT_BY_STATUS}'>Aktif</th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
      <td align="center">{ROW.check}</td>
      <td align="center">
        <a href="#" onClick="Start('pengaturan_member_kartu.php?sid={SID}id={ROW.id_member}');" class="b_print" title="Cetak Kartu">&nbsp</a>&nbsp;|&nbsp;
        <a href="pengaturan_member.php?sid={SID}&mode=edit&id={ROW.id_member}" class="b_edit" title="Ubah Data">&nbsp</a>&nbsp;|&nbsp;
        <a href="#" onclick="hapusData('{ROW.id_member}');" class="b_delete" title="Hapus Data">&nbsp</a>&nbsp;
      </td>
      <td align="center">{ROW.no}</td>
      <td align="center">{ROW.nama}</td>
      <td align="center">{ROW.id_member}</td>
      <td align="center">{ROW.alamat}</td>
      <td align="center">{ROW.hp}</td>
      <td align="center">{ROW.email}</td>
      <td align="center">{ROW.tgl_daftar}</td>
      <td align="center">{ROW.tgl_berangkat}</td>
      <td align="right">{ROW.point}</td>
      <td align="right">{ROW.frekwensi}</td>
      <td align="center">{ROW.aktif}</td>
    </tr>
    <!-- END ROW -->
  </table>
  {NO_DATA}
</div>

<div style="float: left;padding-bottom: 2px;">
  <a href="{U_MEMBER_ADD}">[+]Tambah Member</a>&nbsp;|&nbsp;
  <a href="" onClick="return hapusData('');">[-]Hapus Member</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  <a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
  <a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
</div>
