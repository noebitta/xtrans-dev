<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
  <meta http-equiv="Content-Style-Type" content="text/css">
	<link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <link rel="stylesheet" href="{TPL}trav.css" type="text/css" />
  <script language="javaScript" type="text/javascript" src="includes/calendar.js"></script>
  <link href="includes/calendar.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">

  /*var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25961668-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
*/
</script>
<!-- </head> -->
</head>  
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/lib/prototype.js"></script>
<script type='text/javascript' src='{ROOT}js/prototype.js'></script>
<script type='text/javascript' src='{ROOT}js/scriptaculous.js?load=effects'></script>
<script type='text/javascript' src='{ROOT}js/prototip.js'></script>
<link rel="stylesheet" type="text/css" href="{ROOT}css/prototip.css" />
<body >
<!-- BEGIN if_login -->
<div align='center'>
<table width='100%' cellpadding=0 cellspacing=0>
<tr>
	<!--<td class="banner" background='./templates/images/bg_tombol_header.png' STYLE='background-repeat: no-repeat;background-position: right bottom;'>-->
	<td>
		<table width='100%' cellpadding=0 cellspacing=0>
			<tr>
				<td height=40 valign='bottom'>
					&nbsp;<img src="{TPL}images/loto_cta_comp_small1.png" />&nbsp;
					<font color='0000ff' size='4'> Charlie Version</font>
				</td>
				<td valign="bottom" align="right">
					<table>
						<tr><td height=20 align='right' valign='bottom'>
					    <a>Cabang Login: {CABANG_LOGIN}</a>|  
					    <a href="{U_LOGOUT}">Logout</a>
						</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width='100%' cellpadding=0 cellspacing=0>
			<tr>
				<td height=10 class='bannercari' align='center' colspan=2></td>
				<td class='bannercari' align='center' valign='middle' rowspan=2 width=200><font size=2><b>{USERNAME}</b>&nbsp;&nbsp;</font></td>
			</tr>
			<tr>
				<td height=30 align='left' class="indexer">&nbsp;{BCRUMP}</td>
				<td align='right' class="indexer" width=38 background='./templates/images/siku_banner.gif' STYLE='background-repeat: no-repeat;background-position: left;'></td>
			</tr>
		</table>
	</td>
</tr>
</table>
<!-- END if_login -->