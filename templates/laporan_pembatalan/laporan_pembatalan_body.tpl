<script language="JavaScript">

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function validasiAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function getUpdateAsal(kota){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_asal')){
			document.getElementById('rewrite_asal').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_asal","laporan_pembatalan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "kota=" + kota + "&asal={ASAL}&mode=get_asal",
        onLoading: function(request) 
        {
            Element.show('loading_asal');
        },
        onComplete: function(request) 
        {
						Element.hide('loading_asal');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

	
function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_pembatalan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&jurusan={ID_JURUSAN}&mode=get_tujuan",
        onLoading: function(request) 
        {
            Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
						Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJam(id_jurusan){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_jam')){
			document.getElementById('rewrite_jam').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_jam","laporan_pembatalan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "jurusan=" + id_jurusan + "&jam={KODE_JADWAL}&mode=get_jam",
        onLoading: function(request) 
        {
            Element.show('loading_jam');
        },
        onComplete: function(request) 
        {
						Element.hide('loading_jam');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateAsal("{KOTA}"); 	
getUpdateTujuan("{ASAL}"); 	
getUpdateJam("{ID_JURUSAN}");

</script>


<div class="wrapperheader" style="height:50px;">
  <div class="headertittle">&nbsp;Laporan Pembatalan</div>
  <div class="headerfilter" style="padding-top: 7px;text-align: right;">&nbsp;
  </div>
</div>

<div>
  <form action="{ACTION_CARI}" method="post" name="my_form">
    <input type='hidden' id='is_cari' name='is_cari' value='1'>
    <table width='500'>
      <tr><td colspan='3'><font size='3'><b>Filter</b></font></td></tr>
      <tr>
        <td width=100><select id="periode" name="periode"><option value="0" {PERIODE0}>Tgl.Berangkat</option><option value="1" {PERIODE1}>Tgl.Pembatalan</option></select></td><td width=5>:</td>
        <td>Tgl&nbsp;<input type='text' id='tgl_awal' name='tgl_awal' value="{TGL_AWAL}" size='2' maxlength='2' onkeypress='validasiAngka(event);' />&nbsp;s/d&nbsp;
          <input type="text" id='tgl_akhir' name='tgl_akhir' value="{TGL_AKHIR}" size='2' maxlength='2' onkeypress='validasiAngka(event);' />&nbsp;
          bln
          <select id='opt_bulan' name='opt_bulan'>
            <option value='01' {BULAN_01}>Januari</option>
            <option value='02' {BULAN_02}>Februari</option>
            <option value='03' {BULAN_03}>Maret</option>
            <option value='04' {BULAN_04}>April</option>
            <option value='05' {BULAN_05}>Mei</option>
            <option value='06' {BULAN_06}>Juni</option>
            <option value='07' {BULAN_07}>Juli</option>
            <option value='08' {BULAN_08}>Agustus</option>
            <option value='09' {BULAN_09}>September</option>
            <option value='10' {BULAN_10}>Oktober</option>
            <option value='11' {BULAN_11}>November</option>
            <option value='12' {BULAN_12}>Desember</option>
          </select>&nbsp;thn 20
          <input type='text' id='tahun' name='tahun' value='{TAHUN}' size='2' maxlength='2'>
        </td>
      </tr>
      <tr><td width=100>Kota Berangkat</td><td width=5>:</td><td><select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select></td></tr>
      <tr><td width=100>Pool Berangkat</td><td width=5>:</td><td><div id='rewrite_asal'></div><span id='loading_asal' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></td></tr>
      <tr><td>Pool Tujuan</td><td>:</td><td><div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></td></tr>
      <tr><td>Jam Berangkat</td><td>:</td><td><div id='rewrite_jam'></div><span id='loading_jam' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></td></tr>
      <tr><td>Status</td><td>:</td><td><select id='opt_status' name='opt_status'><option value='' {OPT_STATUS_}>-tampilkan semua-</option><option value='0' {OPT_STATUS_0}>BOOKING</option><option value='1' {OPT_STATUS_1}>BAYAR</option></select></td></tr>
      <tr><td>Dibatalkan pada</td><td>:</td>
        <td>
          <select id='dibatalkanpada' name='dibatalkanpada'>
            <option value='' {OPT_BATALPADA}>-tampilkan semua-</option>
            <option value='0' {OPT_BATALPADA0}>Hari H</option>
            <option value='1' {OPT_BATALPADA1}>H-1</option>
            <option value='2' {OPT_BATALPADA2}>H-2</option>
            <option value='3' {OPT_BATALPADA3}>H-3</option>
            <option value='4' {OPT_BATALPADA4}>H-4</option>
            <option value='5' {OPT_BATALPADA5}>H-5</option>
            <option value='6' {OPT_BATALPADA6}>H-6</option>
            <option value='7' {OPT_BATALPADA7}>H-7</option>
            <option value='8' {OPT_BATALPADA8}>H-8</option>
            <option value='9' {OPT_BATALPADA9}>H-9</option>
            <option value='10' {OPT_BATALPADA10}>H-10</option>
            <option value='11' {OPT_BATALPADA11}>kurang dari H-10</option>
          </select>
        </td>
      </tr>
      <tr><td>Tambahan</td><td>:</td><td><input type='text' id='cari' name='cari' value='{CARI}' size=20 />&nbsp;<br><span class='noticeMessage'>(ex: no tiket, nama penumpang, no.hp,dll)</span></td></tr>
      <tr><td colspan=3 align='center' class='bannernormal'><input type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /></td></tr>
    </table>
  </form>
</div>

<div style="float: left;padding-bottom: 0px;">
  <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
</div>
<div style="float: right;padding-bottom: 0px;">
  {PAGING}
</div>
<span id='loading_proses' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='909090' size=2>sedang memposes...</font></span>
<div class="wrappertabledata" >
  <table width="2050" >
    <tr>
      <th width=50>No</th>
      <th width=100>No.Tiket</th>
      <th width=130>Tgl.Berangkat</th>
      <th width=70>Jam</th>
      <th width=100>Kode Jadwal</th>
      <th width=50>No.Kursi</th>
      <th width=200>Nama</th>
      <th width=100>Telp</th>
      <th width=150>Waktu Pesan</th>
      <th width=200>Penjual</th>
      <th width=100>Cetak Tiket</th>
      <th width=200>Tiket by</th>
      <th width=150>Waktu Batal</th>
      <th width=200>Pembatal</th>
      <th width=150>Manifest</th>
      <th width=100>Status</th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
      <td align="center">{ROW.no}</td>
      <td align="center">{ROW.no_tiket}</td>
      <td align="center">{ROW.tgl_berangkat}</td>
      <td align="center">{ROW.jam}</td>
      <td align="center">{ROW.kode_jadwal}</td>
      <td align="center">{ROW.no_kursi}</td>
      <td align="center">{ROW.nama}</td>
      <td align="center">{ROW.telp}</td>
      <td align="center">{ROW.waktu_pesan}</td>
      <td align="center">{ROW.penjual}</td>
      <td align="center">{ROW.waktu_cetak_tiket}</td>
      <td align="center">{ROW.pencetak_tiket}</td>
      <td align="center">{ROW.waktu_batal}</td>
      <td align="center">{ROW.pembatal}</td>
      <td align="center">{ROW.manifest}</td>
      <td align="center">{ROW.status}</td>
    </tr>
    <!-- END ROW -->
  </table>
</div>

<div style="float: left;padding-bottom: 2px;">
  <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  {PAGING}
</div>