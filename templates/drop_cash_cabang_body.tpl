<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");
    dojo.addOnLoad(init);
    function init() {
        //control dialog drop cash
        dialog_dropcash = dojo.widget.byId("dialog_dropcash");
        dialog_dropcash_btn_cancel = document.getElementById("dialog_dropcash_btn_cancel");
        dialog_dropcash.setCloseControl(dialog_dropcash_btn_cancel);
    }
    function save() {
        var saldo = document.getElementById("kas").value;
        var jenis = document.getElementById("jenis").value;
        var jumlah = document.getElementById("jumlah").value;
        var keterangan = document.getElementById("ket").value;

        if(jenis == ""){
            alert("Error !! Jenis Pengeluaran Tidak Boleh Kosong");
        }else if(jumlah == "" || jumlah == 0){
            alert("Error !! Jumlah Pengeluaran Tidak Boleh Kosong");
        }else if(keterangan == ""){
            alert("Error !! Keterangan Tidak Boleh Kosong");
        }else if(saldo < jumlah){
            alert("Error !! Pengeluaran Lebih Besar Dari Sisa Saldo");
            dialog_dropcash.hide();
        }else{
            new Ajax.Request("drop_cash_cabang.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters: "mode=InsertData"+
                "&jenis="+jenis+
                "&jumlah="+jumlah+
                "&keterangan="+keterangan,
                onLoading: function(request){
                    progressbar.show();
                },
                onComplete: function(request){
                    progressbar.hide();
                    eval(request.responseText);
                },
                onSuccess: function(request){
                    eval(request.responseText);
                },
                onFailure: function(request){
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                }
            });
        }

    }
    function cetak(id) {
        window.open("drop_cash_cabang.php?sid={SID}&mode=Print&id="+id,"_blank","");
    }
</script>
<!-- dialog drop cash -->
<div dojoType="dialog" id="dialog_dropcash" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;">
        <table>
            <tr><td><h2>Data Pengeluaran Drop Cash</h2></td></tr>
            <tr>
                <td align='center'>
                    <table bgcolor='white' width='100%'>
                        <tr><td colspan='2' align="center"><h2>Petty Cash</h2></td></tr>
                        <tr>
                            <td align="right">Jenis Biaya: </td>
                            <td>
                                <select id="jenis" name="jenis">
                                    <option value="">-- Jenis Pengeluaran --</option>
                                    <option value="Alat Tulis Kantor">Alat Tulis Kantor</option>
                                    <option value="Perawatan Peralatan">Perawatan Peralatan</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                            </td>
                        </tr>
                        <tr><td align="right">Jumlah : </td><td><input type="text" name="jumlah" id="jumlah"></td></tr>
                        <tr><td align="right">Keterangan : </td><td><textarea name="ket" id="ket" rows="4"></textarea></td></tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="button" id="dialog_dropcash_btn_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
                    <input type="button" onclick="save();" id="dialog_dropcash_btn_ok" value="   Simpan   ">
                </td>
            </tr>
        </table>
    </form>
</div>
<!-- end dialog drop cash -->
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr height=40 class='banner'>
                    <td align='center' valign='middle' class="bannerjudul">
                        DROP CASH {CABANG}, SISA SALDO Rp. {LABEL_KAS}
                        <input type="hidden" name="kas" id="kas" value="{KAS}">
                    </td>
                    <td class="bannernormal" align="right" valign="middle">
                        <form method="post" action="{URL}">
                            &nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                            &nbsp;s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                            &nbsp;{OPT_CABANG}
                            Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                            <input type="submit" value="cari" />&nbsp;
                        </form>
                    </td>
                </tr>
                <tr>
                    <td align='left' valign='middle'>
                        <table>
                            <tr>
                                <td >
                                    <input class="tombol" value="Tambah Biaya" id="tambah_biaya" type="button" onclick="dialog_dropcash.show();">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align='left' valign='middle'>
                        <table>
                            <tr>
                                <td >
                                    <a href="{EXCEL}" target="_blank"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- END HEADER-->
        </td>
        <!-- BODY -->
        <table class="border" width="100%">
            <thead>
            <tr>
                <th width="30">No</th>
                <th width="150">PEMBUAT</th>
                <th width="150">TANGGAL</th>
                <th width="150">JENIS BIAYA</th>
                <th width="100">KETERANGAN</th>
                <th width="100">JUMLAH</th>
                <th width="150">ACTION</th>
            </tr>
            </thead>
            <tbody>
            <!-- BEGIN ROW -->
            <tr class="{ROW.baris}">
                <td><div align="center">{ROW.No}</div></td>
                <td>{ROW.Pembuat}</td>
                <td><div align="center">{ROW.TglBuat}</div></td>
                <td><div>{ROW.JenisBiaya}</div></td>
                <td><div align="center">{ROW.Jml}</div></td>
                <td><div>{ROW.Keterangan}</div></td>
                <td><div align="center"><button type="button" onclick="cetak({ROW.Id})">Cetak</button></div></td>
            </tr>
            <!-- END ROW -->
            </tbody>
        </table>
        <!--END BODY-->
    </tr>
</table>