<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form name="formsubmit" action="{ACTION_FORM}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Omzet Jurusan</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>Periode:&nbsp;</td>
							<td class='bannernormal'><select name='is_today' id='is_today' onChange="if(this.value=='1'){tanggal_mulai.disabled=true;tanggal_akhir.disabled=true;}else{tanggal_mulai.disabled=false;tanggal_akhir.disabled=false;}"><option value="1" {IS_TODAY1}>Hari ini</option><option value="0" {IS_TODAY0}>Hari lalu</option></select></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
					<input type="hidden" id="sort_by" name="sort_by" value="" />
					<input type="hidden" id="order" name="order" value="{ORDER}" />
					
					<script type="text/javascript">
						if("{IS_TODAY1}"=="selected"){
							document.getElementById("tanggal_mulai").disabled=true;
							document.getElementById("tanggal_akhir").disabled=true;
						}
						else{
							document.getElementById("tanggal_mulai").disabled=false;
							document.getElementById("tanggal_akhir").disabled=false;
						}
					</script>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30 rowspan=2 >No</th>
			 <th width=400 rowspan=2>Jurusan</th>
			 <th width=100 rowspan=2>Kode Jurusan</th>
			 <th width=100 rowspan=2>Trip</th>
			 <th colspan=10>Total Penumpang</th>
			 <th width=100 rowspan=2>Pnp<br>/Trip</th>
			 <th width=200 rowspan=2>Tot.Omz.Pnp (Rp.)</th>
			 <th width=200 rowspan=2>Tot.Disc</th>
			 <th width=200 rowspan=2>Total</th>
			 <th width=100 rowspan=2>Action</th>
     </tr>
		 <tr>
			<th>U</th>
			<th>M</th>
			<th>K</th>
			<th>KK</th>
			<th>G</th>
			<th>O</th>
			<th>R</th>
			<th>VR</th>
			<th>V</th>
			<th>T</th>
		 </tr>
     <!-- BEGIN ROW -->
		 {ROW.total_per_kota}
		 {ROW.total_per_area}
		 {ROW.nama_area}
		 {ROW.nama_kota}
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="left">{ROW.jurusan}</div></td>
			 <td ><div align="left">{ROW.kode_jurusan}</div></td>
			 <td ><div align="right">{ROW.total_keberangkatan}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_u}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_m}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_k}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_kk}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_g}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_o}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_r}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_vr}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_v}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang}</div></td>
			 <td ><div align="right">{ROW.rata_pnp_per_trip}</div></td>
			 <td ><div align="right">{ROW.total_omzet}</div></td>
			 <td ><div align="right">{ROW.total_discount}</div></td>
			 <td ><div align="right">{ROW.total}</div></td>
       <td ><div align="center">{ROW.act}</div></td>
     </tr>
     <!-- END ROW -->
		 <tr style="background: yellow; font-weight: bold;">
     		<td colspan='3' align='center'>GRAND TOTAL</td>
     		<td align='right'>{GT_TRIP}</td>
     		<td align='right'>{GT_PNP_U}</td>
     		<td align='right'>{GT_PNP_M}</td>
     		<td align='right'>{GT_PNP_K}</td>
     		<td align='right'>{GT_PNP_KK}</td>
     		<td align='right'>{GT_PNP_G}</td>
     		<td align='right'>{GT_PNP_O}</td>
     		<td align='right'>{GT_PNP_R}</td>
     		<td align='right'>{GT_PNP_VR}</td>
				<td align='right'>{GT_PNP_V}</td>
     		<td align='right'>{GT_PNP_T}</td>
     		<td align='right' bgcolor='white'>&nbsp;</td>
     		<td align='right'>{GT_OMZ}</td>
     		<td align='right'>{GT_DISC}</td>
     		<td align='right'>{GT_OMZ_NET}</td>
     		<td align='right' bgcolor='white'>&nbsp;</td>
     </tr>
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
					<table>
						<tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
						<tr><td>M</td><td>=</td><td>Penumpang Mahasiswa/Pelajar/Anak-anak/Lansia/Group</td></tr>
						<tr><td>K</td><td>=</td><td>Penumpang Khusus/Pelanggan Setia</td></tr>
						<tr><td>KK</td><td>=</td><td>Penumpang Keluarga Karyawan</td></tr>
						<tr><td>G</td><td>=</td><td>Penumpang Gratis</td></tr>
						<tr><td>O</td><td>=</td><td>Penumpang Online Tiketux</td></tr>
						<tr><td>R</td><td>=</td><td>Return</td></tr>
						<tr><td>VR</td><td>=</td><td>Voucher Return</td></tr>
						<tr><td>V</td><td>=</td><td>Voucher</td></tr>
						<tr><td>T</td><td>=</td><td>Total Penumpang</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>