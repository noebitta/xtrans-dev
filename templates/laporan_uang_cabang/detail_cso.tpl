<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Setoran CSO</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>
								&nbsp;Cabang:&nbsp;{CABANG}&nbsp;|
								&nbsp;Tgl:&nbsp;{TGL_AWAL}
								&nbsp; s/d &nbsp;{TGL_AKHIR}
							</td>					
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30>No</th>
			 <th width=200>Nama</th>
			 <th width=100>Tot.Pnp</th>
			 <th width=200>Tunai</th>
			 <th width=200>Debit</th>
			 <th width=200>Kredit</th>
			 <th width=200>Total Setoran</th>
			<!-- <th width=200>Action</th> -->
			 	 
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="left">{ROW.nama}</div></td>
			<td ><div align="right" >{ROW.total_penumpang}</div></td>
			<td ><div align="right">{ROW.total_tunai}</div></td>
			<td ><div align="right">{ROW.total_debit}</div></td>
			<td ><div align="right">{ROW.total_kredit}</div></td>
			<td ><div align="right">{ROW.total_setoran}</div></td>
			<td ><div align="right">{ROW.total_setoran}</div></td>
		<!-- <td ><div align="center">{ROW.act}</div></td> -->
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>