<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Setoran Cabang</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>Periode:&nbsp;</td><td><select name='is_today' id='is_today' onChange="if(this.value=='1'){tanggal_mulai.disabled=true;tanggal_akhir.disabled=true;}else{tanggal_mulai.disabled=false;tanggal_akhir.disabled=false;}"><option value="1" {IS_TODAY1}>Hari ini</option><option value="0" {IS_TODAY0}>Hari lalu</option></select></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
					<script type="text/javascript">
						if("{IS_TODAY1}"=="selected"){
							document.getElementById("tanggal_mulai").disabled=true;
							document.getElementById("tanggal_akhir").disabled=true;
						}
						else{
							document.getElementById("tanggal_mulai").disabled=false;
							document.getElementById("tanggal_akhir").disabled=false;
						}
					</script>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30>No</th>
			<!-- <th width=200>Cabang</th>-->
			 <th width=100>#Cabang</th>
			<!-- <th width=300>Alamat</th> -->
			 <th width=100>Tot.Pnp</th>
			 <th width=200>Tunai Tiket</th>
			 <th width=200>Tunai Paket</th>
			 <th width=200>Debit Tiket</th>
			 <th width=200>Debit Paket</th>
			 <th width=200>Kredit Tiket</th>
			 <th width=200>Kredit Paket</th>
			 

			 <th width=200>Total Setoran</th>
			 <th width=100>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     {ROW.total_per_kota}
     {ROW.nama_kota}
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
   		 <!--<td ><div align="left">{ROW.cabang}</div></td> -->
			 <td ><div align="left">{ROW.kode_cabang}</div></td>
		 <!--<td ><div align="left">{ROW.alamat}</div></td> -->
			 <td ><div align="right" >{ROW.total_penumpang}</div></td>
			 <td ><div align="right">{ROW.total_tunai_tiket}</div></td>
			 <td ><div align="right">{ROW.total_tunai_paket}</div></td>

			 <td ><div align="right">{ROW.total_debit_tiket}</div></td>
			  <td ><div align="right">{ROW.total_debit_paket}</div></td>

			 <td ><div align="right">{ROW.total_kredit_tiket}</div></td>	
			 <td ><div align="right">{ROW.total_kredit_paket}</div></td>
			 <td ><div align="right">{ROW.total_setoran}</div></td>
       <td ><div align="center">{ROW.act}</div></td>
     </tr>
     <!-- END ROW -->
     <tr style="background: yellow; font-weight: bold;">
     		<td colspan='2' align='center'>GRAND TOTAL</td>
     		<td align='right'>{GT_PENUMPANG}</td>
     		<td align='right'>{GT_TUNAI_TIKET}</td>
     		<td align='right'>{GT_TUNAI_PAKET}</td>

     		<td align='right'>{GT_DEBIT_TIKET}</td>
     		<td align='right'>{GT_DEBIT_PAKET}</td>

     		<td align='right'>{GT_KREDIT_TIKET}</td>
     		<td align='right'>{GT_KREDIT_PAKET}</td>

     		<td align='right'>{GT_SETORAN}</td>
				<td align='right'>&nbsp;</td>
     </tr>
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr> 
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>