<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

function Start(page) {
OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=no,resizable=yes");
}

function setOrder(sortby){
	ascending	= document.getElementById('hdn_asc').value;
	tampilkanData(sortby);
	document.getElementById('hdn_asc').value = 1-ascending;
}

function klikCari(){
	if(!document.getElementById('hdn_id_member')){
		tampilkanData('');
	}
	else{
		id_member	= document.getElementById('hdn_id_member').value;
		if(id_member!=''){
			tampilkanDataByMember(id_member);
		}
	}
}

function tampilkanDataByMember(id_member){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		tgl_mula	= document.getElementById('tgl_mula').value;
		tgl_akhir	= document.getElementById('tgl_akhir').value;
		
		new Ajax.Updater("rewrite_hasil","member_laporan_mutasi.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=tampilkan_mutasi_by_member&tgl_mula="+tgl_mula+"&tgl_akhir="+tgl_akhir+"&id_member="+id_member,
        onLoading: function(request) 
        {
            Element.show('progress');
        },
        onComplete: function(request) 
        {
						Element.hide('progress');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function tampilkanData(sortby){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
    
		cari			= document.getElementById('txt_cari').value;
		tgl_mula	= document.getElementById('tgl_mula').value;
		tgl_akhir	= document.getElementById('tgl_akhir').value;
		kategori_member	= document.getElementById('kategori_member').value;
    ascending	= document.getElementById('hdn_asc').value;
		
		new Ajax.Updater("rewrite_hasil","member_laporan_mutasi.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=tampilkan_mutasi&tgl_mula="+tgl_mula+"&tgl_akhir="+tgl_akhir+"&cari="+cari+"&kategori_member="+kategori_member+"&sortby="+sortby+"&ascending="+ascending,
        onLoading: function(request) 
        {
            Element.show('progress');
						//Element.hide('rewritejam');
        },
        onComplete: function(request) 
        {
						Element.hide('progress');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function cetakLaporan(){
  //mencetak laporan 	
	if(!document.getElementById('hdn_id_member')){
		id_member="";
	}
	else{
		id_member	= document.getElementById('hdn_id_member').value;
	}
	
	tgl_mula	= document.getElementById('tgl_mula').value;
	tgl_akhir	= document.getElementById('tgl_akhir').value;
	
  Start("member_cetak_mutasi.php?sid={SID}&id_member="+id_member+"&tgl_mula="+tgl_mula+"&tgl_akhir="+tgl_akhir);
}

{JAVA_SCRIPT_KOREKSI}

function init(e) {
  // inisialisasi variabel
	
	tampilkanData('');
}

dojo.addOnLoad(init);



</script>

<input type='hidden' id='hdn_asc' value='0' >

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='100%'>
    <tr>
    <td valign="middle" align="center">
		<h1>Laporan Mutasi Transaksi Member</h1>
    <table width='100%'>
    <tr>
    </tr>
		<tr>
			<td align='center'>
				<form name="tgl" method="post">
					<table width='100%'>
						<tr>
							<td>Tgl mula</td>
							<td>:</td>
							<td>
								<input readonly="yes" id="tgl_mula" onclick="javascript:cal1.popup();" name="tgl_mula" value="{CURRENT_DATE}" type="Text">						
							</td>
							<td>Tgl akhir</td>
							<td>:</td>
							<td>
								<input readonly="yes" id="tgl_akhir" onclick="javascript:cal2.popup();" name="tgl_akhir" value="{CURRENT_DATE}" type="Text">						
							</td>
							<td>Kategori</td>
							<td>:</td>
							<td>
								<select id='kategori_member'>
									<option value=''>-Semua-</option>
									<option value='KARYAWAN'>KARYAWAN</option>
									<option value='UMUM'>UMUM</option>
									<option value='MAHASISWA'>MAHASISWA</option>
									<option value='MANULA'>MANULA</option>
								</select>
							</td>
							<td>Cari</td>
							<td>:</td>
							<td>
								<!--field cari-->
								<input type="text" name="txt_cari" id="txt_cari">&nbsp;&nbsp;&nbsp;<input type="button" onClick="klikCari();" value="Cari">
							</td>
						</tr>
					</table>
				</form>
				<script language="JavaScript">
					<!-- // create calendar object(s) just after form tag closed
					// specify form element as the only parameter (document.forms['formname'].elements['inputname']);
					// note: you can have as many calendar objects as you need for your application
					var cal1 = new calendar1(document.forms['tgl'].elements['tgl_mula']);
					var cal2 = new calendar1(document.forms['tgl'].elements['tgl_akhir']);
					cal1.year_scroll = true;
					cal1.time_comp = false;
					cal2.year_scroll = true;
					cal2.time_comp = false;
					//-->
				</script>
			</td>
    </tr>
    </table>
    <div id='rewrite_hasil'></div>
		<span id='progress' style='display:none;'><img src='./images/progress.gif' /> Sedang memproses...</span><br>
	{PESAN}
 </td>
</tr>
</table>
