<script language="JavaScript">

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function validasiAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function getUpdateAsal(kota){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_asal')){
			document.getElementById('rewrite_asal').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_asal","laporan_pembatalan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "kota=" + kota + "&asal={ASAL}&mode=get_asal",
        onLoading: function(request) 
        {
            //Element.show('loading_asal');
        },
        onComplete: function(request) 
        {
						//Element.hide('loading_asal');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

	
function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_pembatalan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&jurusan={ID_JURUSAN}&mode=get_tujuan",
        onLoading: function(request) 
        {
            //Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
						//Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJam(id_jurusan){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_jam')){
			document.getElementById('rewrite_jam').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_jam","laporan_pembatalan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "jurusan=" + id_jurusan + "&jam={KODE_JADWAL}&mode=get_jam",
        onLoading: function(request) 
        {
            //Element.show('loading_jam');
        },
        onComplete: function(request) 
        {
						//Element.hide('loading_jam');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateAsal("{KOTA}"); 	
getUpdateTujuan("{ASAL}"); 	
getUpdateJam("{ID_JURUSAN}");

</script>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td class="whiter" valign="middle" align="center">		
			<form action="{ACTION_CARI}" method="post" name="my_form">
				<input type='hidden' id='is_cari' name='is_cari' value='1'>
				<table width='100%' cellspacing="0">
					<tr class='banner' height=40>
						<td colspan='3' align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Koreksi Discount</td>
					</tr>
				</table>
				<table width='100%'>
					<tr>
						<td colspan='3' valign='middle' align='left'>
							<table width='500'>
								<tr><td colspan='3'><font size='3'><b>Filter</b></font></td></tr>
								<tr>
									<td width=100>Periode</td><td width=5>:</td>
									<td>Tgl&nbsp;<input type='text' id='tgl_awal' name='tgl_awal' value="{TGL_AWAL}" size='2' maxlength='2' onkeypress='validasiAngka(event);' />&nbsp;s/d&nbsp;
										<input type="text" id='tgl_akhir' name='tgl_akhir' value="{TGL_AKHIR}" size='2' maxlength='2' onkeypress='validasiAngka(event);' />&nbsp;
										bln
										<select id='opt_bulan' name='opt_bulan'>
											<option value='01' {BULAN_01}>Januari</option>
											<option value='02' {BULAN_02}>Februari</option>
											<option value='03' {BULAN_03}>Maret</option>
											<option value='04' {BULAN_04}>April</option>
											<option value='05' {BULAN_05}>Mei</option>
											<option value='06' {BULAN_06}>Juni</option>
											<option value='07' {BULAN_07}>Juli</option>
											<option value='08' {BULAN_08}>Agustus</option>
											<option value='09' {BULAN_09}>September</option>
											<option value='10' {BULAN_10}>Oktober</option>
											<option value='11' {BULAN_11}>November</option>
											<option value='12' {BULAN_12}>Desember</option>
										</select>&nbsp;thn 20
										<input type='text' id='tahun' name='tahun' value='{TAHUN}' size='2' maxlength='2'>
									</td>
								</tr>
								<tr><td width=100>Kota Berangkat</td><td width=5>:</td><td><select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select></td></tr>
								<tr><td width=100>Pool Berangkat</td><td width=5>:</td><td><div id='rewrite_asal'></div><span id='loading_asal' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></td></tr>
								<tr><td>Pool Tujuan</td><td>:</td><td><div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></td></tr>
								<tr><td>Jam Berangkat</td><td>:</td><td><div id='rewrite_jam'></div><span id='loading_jam' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></td></tr>
								<tr><td>Tambahan</td><td>:</td><td><input type='text' id='cari' name='cari' value='{CARI}' size=20 />&nbsp;<span class='noticeMessage'>(ex: no tiket, nama penumpang, no.hp,dll)</span></td></tr>
								<tr><td colspan=3 align='center' class='bannernormal'><input type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /></td></tr>
								<!--<tr>
									<td colspan=3 align='center'>
										<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
										<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
									</td>
								</tr>-->
							</table>
						</td>
					</tr>
					<tr><td colspan='3'><hr></tr>
				</table>
			</form>
		</td>
	</tr>
</table>

<table width='100%'>
	<tr>
		<td width='100%' align='right' colspan=13 valign='bottom'>{PAGING}</td>
	</tr>
	<!--<tr>
		<td colspan=13 align='center'>
		<br>
		<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
		<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
		</td>
	</tr>-->
</table>


<table  width='2000'>
	<tr>
		<td colspan=9 align='center' height=20><span id='loading_proses' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='909090' size=2>sedang memposes...</font></span></td></tr>
	<tr>
		<th width=30>No</th>
		<th width=100>No.Tiket</th>
		<th width=100>Tgl.Berangkat</th>
		<th width=50>Jam</th>
		<th width=100>Kode Jadwal</th>
		<th width=50>No.Kursi</th>
		<th width=200>Nama</th>
		<th width=100>Telp</th>
		<th width=100>Waktu Pesan</th>
		<th width=100>Cetak Tiket</th>
		<th width=200>Tiket Oleh</th>
		<th width=100>Waktu Koreksi</th>
		<th width=200>Pengkoreksi</th>
		<th width=200>Otorisasi</th>
		<th width=100>Harga Tiket</th>
		<th width=100>Disc. Lama</th>
		<th width=100>Disc. Baru</th>
		<th width=100>Manifest</th>
	</tr>
  <!-- BEGIN ROW -->
  <tr class="{ROW.odd}">
		<td><div align="left">{ROW.no}</div></td>
		<td><div align="left">{ROW.no_tiket}</div></td>
		<td><div align="left">{ROW.tgl_berangkat}</div></td>
		<td><div align="left">{ROW.jam}</div></td>
		<td><div align="left">{ROW.kode_jadwal}</div></td>
		<td><div align="left">{ROW.no_kursi}</div></td>
		<td><div align="left">{ROW.nama}</div></td>
		<td><div align="left">{ROW.telp}</div></td>
		<td><div align="center">{ROW.waktu_pesan}</div></td>
		<td><div align="center">{ROW.waktu_cetak_tiket}</div></td>
		<td><div align="left">{ROW.penjual}</div></td>
		<td><div align="center">{ROW.waktu_koreksi}</div></td>
		<td><div align="left">{ROW.pengkoreksi}</div></td>
		<td><div align="left">{ROW.otorisasi}</div></td>
		<td><div align="right">{ROW.harga_tiket}</div></td>
		<td><div align="right">{ROW.disc_lama}</div></td>
		<td><div align="right">{ROW.disc_baru}</div></td>
		<td><div align="center">{ROW.manifest}</div></td>
  </tr>  
  <!-- END ROW -->
</table>

<table width='100%'>
	<tr>
		<td width='100%' align='right' colspan=13 valign='bottom'>{PAGING}</td>
	</tr>
	<!--<tr>
		<td colspan=13 align='center'>
		<br>
		<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
		<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
		</td>
	</tr>-->
</table>
