<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function sort(field){
		sortby.value	= field;
		
		formsubmit.submit();
	}
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form name="formsubmit" action="{ACTION_FORM}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr bgcolor='505050' height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Detail Laporan Omzet Penumpang</td>
								<td align='right' valign='middle' >
									<table>
										<tr><td class='bannernormal'>
											&nbsp;Periode:&nbsp; {TGL_AWAL}
											&nbsp; s/d &nbsp; {TGL_AKHIR}
											<input type="hidden" id="tglawal" name="tglawal" value="{TGL_AWAL}" />
											<input type="hidden" id="tglakhir" name="tglakhir" value="{TGL_AKHIR}" />
											<input type="hidden" id="is_today" name="is_today" value="{IS_TODAY}" />
											<input type="hidden" id="kode" name="kode" value="{KODE}" />
											<input type="hidden" id="sortby" name="sortby" value="" />
											<input type="hidden" id="order" name="order" value="{ORDER}" />
										</td></tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan=2 align='center' valign='middle'>
									<table>
										<tr>
											<td>
												<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				<h2>Perolehan Tiket</h2>
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<th width=30>No</th>
				<th width=200><a class="th" href="#" onClick="sort('WaktuPesan');">Waktu Pesan</a></th>
				<th width=200><a class="th" href="#" onClick="sort('WaktuCetakTiket');">Cetak Tiket</a></th>
				<th width=200><a class="th" href="#" onClick="sort('NoTiket');">No.Tiket</a></th>
				<th width=200><a class="th" href="#" onClick="sort('TglBerangkat,JamBerangkat');">Cetak Manifest</a></th>
				<th width=100><a class="th" href="#" onClick="sort('KodeJadwal');">Kode Jadwal</a></th>
				<th width=200><a class="th" href="#" onClick="sort('Nama');">Nama</a></th>
				<th width=50><a class="th" href="#" onClick="sort('NomorKursi');">Kursi</a></th>
				<th width=100><a class="th" href="#" onClick="sort('HargaTiket');">Harga</a></th>
				<th width=100><a class="th" href="#" onClick="sort('Discount');">Discount</a></th>
				<th width=100><a class="th" href="#" onClick="sort('Plus');">Plus</a></th>
				<th width=70><a class="th" href="#" onClick="sort('Total');">Total</th>
				<th width=100><a class="th" href="#" onClick="sort('JenisDiscount');">Tipe Disc.</a></th>
				<th width=200><a class="th" href="#" onClick="sort('NamaCSO');">Book oleh</a></th>
				<th width=200><a class="th" href="#" onClick="sort('NamaCSOTiket');">Tiket oleh</a></th>
				<th width=100><a class="th" href="#" onClick="sort('Status');">Status</a></th>
				<th width=100>Ket.</th>
			</tr>
			<!-- BEGIN ROW -->
			<tr class="{ROW.odd}">
				<td><div align="right">{ROW.no}</div></td>
				<td><div align="left">{ROW.waktu_pesan}</div></td>
				<td><div align="left">{ROW.waktu_cetak}</div></td>
				<td><div align="left">{ROW.no_tiket}</div></td>
				<td><div align="left">{ROW.waktu_berangkat}</div></td>
				<td><div align="left">{ROW.kode_jadwal}</div></td>
				<td><div align="left">{ROW.nama}</div></td>
				<td><div align="center">{ROW.no_kursi}</div></td>
				<td><div align="right">{ROW.harga_tiket}</div></td>
				<td><div align="right">{ROW.discount}</div></td>
				<td><div align="right">{ROW.plus}</div></td>
				<td><div align="right">{ROW.total}</div></td>
				<td><div align="left">{ROW.tipe_discount}</div></td>
				<td><div align="left">{ROW.cso}</div></td>
				<td><div align="left">{ROW.tiket_by}</div></td>
				<td><div align="center">{ROW.status}</div></td>
				<td><div align="left">{ROW.ket}</div></td>
			</tr>  
			<!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>