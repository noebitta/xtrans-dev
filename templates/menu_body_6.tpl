<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<table width="100%" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">
		<h3 id='pengelolaan_member'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Pengelolaan Member</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align="center" width='25%'>  
					<a href="{U_MEMBER}"><img src="{TPL}images/icon_master_member.png" /></a>  
					<br />
					<a href="{U_MEMBER}"><span class="genmed">Data Member</span></a>    
			  </td>	
				
				<td align='center' width='25%'>
					<a href="{U_MEMBER_ULTAH}"><img src="{TPL}images/icon_member_ultah.png" /></a>
					<br />
					<a href="{U_MEMBER_ULTAH}"><span class="genmed">Member <br>Berulang Tahun</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_MEMBER_CALON}"><img src="{TPL}images/icon_member_calon.png" /></a>  
					<br />
					<a href="{U_MEMBER_CALON}"><span class="genmed">Pelanggan Potensi<br>Jadi Member</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_MEMBER_FREKWENSI}"><img src="{TPL}images/icon_laporan_cso.png" /></a>  
					<br />
					<a href="{U_MEMBER_FREKWENSI}"><span class="genmed">Frekwensi Berangkat</span></a>
		    </td>
			</tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>  
					<a href="{U_UBAH_PASSWORD}"><img src="{TPL}images/icon_password_big.png" /></a>
					<br />
					<a href="{U_UBAH_PASSWORD}"><span class="genmed">Ubah Password</span></a>
		    </td>
				
			</tr>
		</table>
 </td>
</tr>
</table>