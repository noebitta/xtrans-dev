<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}

	</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0" >
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Peserta Asuransi</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						<table>
						<tr><td class='bannernormal'>
							&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
							&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
							&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
							<input type="submit" value="cari" />&nbsp;								
						</td></tr>
					</table>
					</form>
				</td>
			</tr>
			<tr>
				<td align='center' colspan=3>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b>Total Pendapatan Asuransi: Rp. {TOTAL_PREMI_ASURANSI}<b></td>
				<td colspan=2 align='right'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table width='100%' class="border">
		<tr>
       <th width=30>No</th>
			 <th width=100><a class="th" href='{A_SORT_BY_WAKTU_TRX}'>Waktu Trx.</a></th>
			 <th width=200><a class="th" href='{A_SORT_BY_PLAN}'>Plan</a></th>
			 <th width=200><a class="th" href='{A_SORT_BY_PREMI}'>Premi<br>(Rp.)</a></th>
			 <th width=200><a class="th" href='{A_SORT_BY_NO_TIKET}'>No.Tiket</a></th>
			 <th width=100><a class="th" href='{A_SORT_BY_TGL_BRGKT}'>Tgl.Brgkt</a></th>
			 <th width=300><a class="th" href='{A_SORT_BY_JURUSAN}'>Jurusan</a></th>
			 <th width=200><a class="th" href='{A_SORT_BY_NAMA}'>Nama</a></th>
			 <th width=200><a class="th" href='{A_SORT_BY_TELP}'>Telepon</a></th>
			 <th width=100><a class="th" href='{A_SORT_BY_TGL_LAHIR}'>Tgl.Lahir</a></th>
			 <th width=100><a class="th" href='{A_SORT_BY_PENUTUP}'>CSO</a></th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
       <td align="center">{ROW.waktu_transaksi}</td>
			 <td align="center">{ROW.plan}</td>
			 <td align="right">{ROW.besar_premi}</td>
			 <td align="center">{ROW.no_tiket}</td>
			 <td align="center">{ROW.tgl_berangkat}</td>
       <td align="center">{ROW.jurusan}</td>
			 <td align="center">{ROW.nama}</td>
			 <td align="center">{ROW.telp}</td>
			 <td align="center">{ROW.tgl_lahir}</td>
			 <td align="center">{ROW.cso}</td>
     </tr>  
     <!-- END ROW -->
		 {NO_DATA}
    </table>
    <table width='100%'>
			<tr>
				<td><b>Total Pendapatan Asuransi: Rp. {TOTAL_PREMI_ASURANSI}<b></td>
				<td align='right'>
					{PAGING}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>