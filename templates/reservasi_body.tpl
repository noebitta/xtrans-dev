<input type="hidden" value="{HARGA_MINIMUM_PAKET}" id="hdn_harga_minimum_paket">
<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>
<input type="hidden" value="{SID}" id="hdn_SID">

<input type="hidden" value=0 id="flag_mutasi">
<input type="hidden" value=0 id="flag_mutasi_paket">
<input type="hidden" value='' id="id_jurusan_aktif">

<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi.js"></script>

<!--dialog SPJ-->
<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td bgcolor='ffffff'>
		<table>
			<tr><td><h2>Cetak SPJ</h2></td></tr>
			<tr><td><div id="rewrite_list_mobil"></div></td></tr>
			<tr><td><div id="rewrite_list_sopir"></div></td></tr>
		</table>
		<span id='progress_dialog_spj' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_SPJ_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
		<input type="button" onclick="CetakSPJ();" id="dialog_SPJ_btn_OK" value="&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog SPJ-->

<!--dialog keberangkatan pelanggan-->
<div dojoType="dialog" id="dialog_cari_keberangkatan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='700'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Keberangkatan</h2></td></tr>
			<tr><td><div id="rewrite_keberangkatan_pelanggan"></div></td></tr>
		</table>
		<span id='progress_cari_jadwal' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_jadwal_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog keberangkatan pelanggan-->

<!--dialog Paket-->
<div dojoType="dialog" id="dialog_cari_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='900'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Paket</h2></td></tr>
			<tr><td><div id="rewrite_cari_paket"></div></td></tr>
		</table>
		<span id='progress_cari_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_paket_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog Paket-->

<!--dialog isian paket -->
<div dojoType="dialog" id="dialog_isian_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table cellspacing=0 cellpadding=0 width='100%' height='100%'>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Pengirim</h2></td></tr>
						<tr><td>Telp Pengirim <span id='dlg_paket_telp_pengirim_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_telp_pengirim' type='text' maxlength='15' onFocus="Element.hide('dlg_paket_telp_pengirim_invalid');" onBlur="cariDataPelangganByTelp4Paket(this.value,1)"/></td></tr>
						<tr><td width=200>Nama Pengirim <span id='dlg_paket_nama_pengirim_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td width=5>:</td><td><input id='dlg_paket_nama_pengirim' type='text' maxlength='100' onFocus="Element.hide('dlg_paket_nama_pengirim_invalid');" /></td></tr>
						<tr><td>Alamat Pengirim</td><td>:</td><td><input id='dlg_paket_alamat_pengirim' type='text' maxlength='100' /></td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Penerima</h2></td></tr>
						<tr><td>Telp Penerima <span id='dlg_paket_telp_penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_telp_penerima' type='text' maxlength='15' onFocus="Element.hide('dlg_paket_telp_penerima_invalid');" onBlur="cariDataPelangganByTelp4Paket(this.value,0)"/></td></tr>
						<tr><td width=200>Nama Penerima <span id='dlg_paket_nama_penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td width=5>:</td><td><input id='dlg_paket_nama_penerima' type='text' maxlength='100' onFocus="Element.hide('dlg_paket_nama_penerima_invalid');" /></td></tr>
						<tr><td>Alamat Penerima</td><td>:</td><td><input id='dlg_paket_alamat_penerima' type='text' maxlength='100' /></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Paket</h2></td></tr>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td width='50%'>Jumlah Koli<span id='dlg_paket_koli_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_jumlah_koli' type='text' maxlength='10' onFocus="Element.hide('dlg_paket_koli_invalid');" /></td></tr>
						<tr><td>Berat (Kg) <span id='dlg_paket_berat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_berat' type='text' maxlength='10' onFocus="Element.hide('dlg_paket_berat_invalid'); " onkeypress='validasiInputanAngka(event);' onBlur="hitungHargaPaket(document.getElementById('dlg_paket_layanan').value);" />&nbsp;Kg.</td></tr>
						<tr>
							<td>Jenis Barang</td>
							<td>:</td>
							<td>
								<select id='dlg_paket_jenis_barang'>
									<option value="DOKUMEN">Dokumen</option>
									<option value="PAKET">Paket</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Layanan</td>
							<td>:</td>
							<td>
								<input id='dlg_paket_harga_kg_pertama_p' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_pertama_ga' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_pertama_gd' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_pertama_s' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_pertama_c' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_berikutnya_p' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_berikutnya_ga' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_berikutnya_gd' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_berikutnya_s' type='hidden' value=''/>
								<input id='dlg_paket_harga_kg_berikutnya_c' type='hidden' value=''/>
								<select id='dlg_paket_layanan' onChange="hitungHargaPaket(this.value);">
									<option value="P">Platinum</option>
									<option value="GA">Gold Antar</option>
									<option value="GD">Gold Diambil</option>
									<option value="S">Silver</option>
									<option value="C">Cargo</option>
								</select>
							</td>
						</tr>
						<tr><td>Harga</td><td>:</td><td>Rp. <span id='rewrite_dlg_paket_harga_paket_show'></span></td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr>
							<td>Cara Pembayaran</td>
							<td>:</td>
							<td>
								<select id='dlg_paket_cara_bayar'>
									<option value="0">Tunai</option>
									<option value="1">Langganan</option>
									<option value="2">Bayar di Tujuan</option>
								</select>
							</td>
						</tr>
						<tr><td valign='top'>Isi Barang<span id='dlg_paket_keterangan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td valign='top'>:</td><td><textarea id='dlg_paket_keterangan' rows='3' cols='30' onFocus="Element.hide('dlg_paket_keterangan_invalid');" ></textarea></td></tr>
					<tr><td valign='top'>Instruksi Kuhusus</td><td valign='top'>:</td><td><textarea id='dlg_paket_intruksi_khusus' rows='3' cols='30'></textarea></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_paket.hide();" id="dlg_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="pesanPaket();" id="dlg_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog isian paket-->

<!--dialog ambil paket-->
<div dojoType="dialog" id="dialog_ambil_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<div id="rewrite_ambil_paket"></div>
		<span id='progress_ambil_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_ambil_paket.hide();" id="dlg_ambil_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="prosesAmbilPaket();" id="dlg_ambil_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Ambil&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog ambil paket-->

<!--dialog CETAK ULANG TIKET-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='cetak_ulang_tiket_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiket();" id="dialog_cetak_ulang_tiket_btn_OK" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog CETAK ULANG TIKET-->

<!--dialog OTP-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket_otp" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,silahkan masukkan kode OTP dari pelanggan</td></tr>
			<tr height=30><td>OTP</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password_otp' maxlength='6'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel_otp" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiketByOTP();" id="dialog_cetak_ulang_tiket_btn_OK_otp" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog OTP-->

<!--dialog DISCOUNT-->
<div dojoType="dialog" id="dialog_discount" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Daftar Jenis Harga</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td>Silahkan pilih harga tiket</td><td>:</td><td><div id="rewrite_list_discount_dialog"></div><input type='hidden' id='hdn_bayar_no_tiket'/></td></tr>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='korek_disc_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='korek_disc_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_discount_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="koreksiDiscount(hdn_bayar_no_tiket.value);" id="dialog_discount_btn_OK" value="Simpan perubahan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog DISCOUNT-->

<!--dialog KOREKSI ASURANSI-->
<div dojoType="dialog" id="dialog_koreksi_asuransi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Koreksi Asuransi</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3><div id="rewrite_list_plan_asuransi"></div><input type='hidden' id='hdn_no_tiket_koreksi_asuransi'/></td></tr>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='korek_asuransi_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='korek_asuransi_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_koreksi_asuransi_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="koreksiAsuransi(hdn_no_tiket_koreksi_asuransi.value);" id="dialog_koreksi_asuransi_btn_OK" value="Simpan perubahan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog KOREKSI ASURANSI-->

<!--dialog BATAL-->
<div dojoType="dialog" id="dialog_batal" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pembatalan Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='batal_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='batal_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_batal_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="batal(batal_no_tiket,batal_no_kursi);" id="dialog_batal_btn_OK" value="Batalkan Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog BATAL-->

<!--dialog Input Kode Voucher-->
<div dojoType="dialog" id="dialog_input_voucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Voucher Tiket Balik</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Silahkan masukkan KODE VOUCHER</td></tr>
			<tr height=30><td>Kode Voucher</td><td>:</td><td><input type='password' id='input_kode_voucher'/><span id='loadingvoucher' style="display:none;">&nbsp;&nbsp;&nbsp;&nbsp;silahkan tunggu&nbsp;&nbsp;<img src="./templates/images/loading.gif" /></span></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_input_voucher_btn_ok_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="bayarByVoucher(input_kode_voucher.value);" id="dialog_input_voucher_btn_ok" value="   Bayar   ">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Input Kode Voucher-->

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
<tr>
	<td align='center'>
    <input type="hidden" id="kode_booking_go_show" name="kode_booking_go_show" value="""/>
		<table bgcolor='white' width='100%'>
			<tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
			<tr>
				<td>Kode Voucher:
				<input type='text' name='kode_voucher' id='kode_voucher' size="20">&nbsp;<input type='button' value='Pakai Voucher' onClick='gunakanVoucher();' /></td>
			</tr>
			<tr align="center" style="background-color: #00FF00">
				<td><div id="show_discount"></div></td>
			</tr>
			<tr><td>
				<table width='100%'>
					<tr>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(0);return false;"><img src="{TPL}images/icon_tunai.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(0);return false;"><span class="genmed">Tunai</span></a>    
						</td>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(1);return false;"><img src="{TPL}images/icon_debitcard.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(1);return false;"><span class="genmed">Debit Card</span></a> 
						</td>
					</tr>
					<tr>
						<td align='center' valign='middle'>
							<a href="" onClick="CetakTiket(2);return false;"><img src="{TPL}images/icon_mastercard.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(2);return false;"><span class="genmed">Credit Card</span></a>    
						</td>
						<!--<td align='center' valign='middle'>
							<a href="" onClick="showInputVoucher();return false;"><img src="{TPL}images/icon_voucher.png" /></a>  
							<br />
							<a href="" onClick="showInputVoucher();return false;"><span class="genmed">Voucher</span></a>    
						</td>-->
					</tr>
				</table>
			</td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="reset" id="dialog_pembayaran_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Pilih Pembayaran-->

<!--dialog Pilih Pembayaran untuk goshow-->
<div dojoType="dialog" id="dialogpembayarangoshow" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
  <form onsubmit="return false;">
    <table>
      <tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
      <tr>
        <td align='center'>
          <table bgcolor='white' width='100%'>
            <tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
            <tr><td>
                <input type='hidden' id='jenisbayargoshow' name='jenisbayargoshow' value='' />
                <table width='100%'>
                  <tr>
                    <td width='50%' align='center' valign='middle'>
                      <a href="" onClick="jenisbayargoshow.value=0;pesanKursi(1);return false;"><img src="{TPL}images/icon_tunai.png" /></a>
                      <br />
                      <a href="" onClick="jenisbayargoshow.value=0;pesanKursi(1);return false;"><span class="genmed">Tunai</span></a>
                    </td>
                    <td width='50%' align='center' valign='middle'>
                      <a href="" onClick="jenisbayargoshow.value=1;pesanKursi(1);return false;"><img src="{TPL}images/icon_debitcard.png" /></a>
                      <br />
                      <a href="" onClick="jenisbayargoshow.value=1;pesanKursi(1);return false;"><span class="genmed">Debit Card</span></a>
                    </td>
                  </tr>
                  <tr>
                    <td align='center' valign='middle'>
                      <a href="" onClick="jenisbayargoshow.value=2;pesanKursi(1);return false;"><img src="{TPL}images/icon_mastercard.png" /></a>
                      <br />
                      <a href="" onClick="jenisbayargoshow.value=2;pesanKursi(1);return false;"><span class="genmed">Credit Card</span></a>
                    </td>
                    <td align='center' valign='middle'>
                      &nbsp;
                    </td>
                  </tr>
                </table>
              </td></tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center">
          <input type="reset" id="dialogpembayarangoshowbtncancel" value="Cancel" style="width:100px;">
        </td>
      </tr>
    </table>
  </form>
</div>
<!--END dialog Pilih Pembayaran-->

<!--dialog loading Pembayaran untuk goshow-->
<div dojoType="dialog" id="dialogloadingpembayarangoshow" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
  <div style="color: white; height: 50px; width: 300px;text-align: center;vertical-align: middle;margin-top: 30px;">silahkan tunggu..</div>
</div>
<!--END dialog Pilih Pembayaran-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="whiter" valign="middle" align="center">
 
<table width="100%" cellspacing="0" cellpadding="4" border="0">
<tr>
	<td height='80' width='100%' bgcolor='dd0000'>
		<table width='100%' height='100%' cellspacing=0 cellpadding=0>
			<tr>	
				<td align='left' valign='middle' bgcolor='eeeeee' width='7%' background='./templates/images/icon_cari.png' STYLE='background-repeat: no-repeat;background-position: left top;'></td>
				<td align='left' valign='middle' bgcolor='eeeeee' width='23%'>
					<font color='505050'><b>Cari keberangkatan</b></font><br>
					<input name="txt_cari_jadwal" id="txt_cari_jadwal" type="text">
					<input class='tombol' name="btn_periksajadwal" id="btn_periksajadwal" value="Cari" type="button" onClick="periksaJadwal(txt_cari_jadwal.value)"><br>	
					<font size=1 color='505050'>(masukkan no telepon pelanggan)</font>
				</td>
				<td align='left' valign='middle' bgcolor='eeeeee' width='23%'>
					<font color='505050'><b>Cari paket</b></font><br>
					<input name="txt_cari_paket" id="txt_cari_paket" type="text">
					<input class='tombol' name="btn_periksapaket" id="btn_periksapaket" value="Cari" type="button" onClick="periksaPaket(txt_cari_paket.value)"><br>	
					<font size=1 color='505050'>(masukkan no resi paket/ no.telp pelanggan )</font>
				</td>
				<td align='right' width='55%' valign='top'>
					<table>
						<tr>
							<td width='80' align='center' valign='top'><a class='menu' href="{U_PAKET}"><img src='./templates/images/icon_paket_service.png' /><br>Paket</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_LAPORAN_UANG};return false"><img src='./templates/images/icon_penjualan_uang.png' /><br>Laporan Uang</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_LAPORAN_PENJUALAN};return false"><img src='./templates/images/icon_penjualan.png' /><br>Laporan Penjualan</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="{U_KAS_KECIL}" target="_blank"><img src='./templates/images/icon_pettycash_sml.png' /><br>Drop Cash</a></td>
							<td width='80' align='center' valign='top' class='notifikasi_pengumuman'><div id="rewritepengumuman"></div></td>
							<td width='80' align='center' valign='top'><a class='menu' href={U_UBAH_PASSWORD}><img src='./templates/images/icon_password.png' /><br>Ubah Password</a></td>
						</tr>
					</table>
			</tr>
		</table>
	</td>
</tr>
<tr>
 <td valign="middle" align="left">    
  <table width="100%" border="0" >
		<tr class="border">
			<td width="230" valign="top">       
				<div>           
					<table border="0" width="100%">
						<tr>
							<td colspan=2 align='left'>
								<div class='formHeader'><strong>1. Jadwal</strong></div>
								<hr color='e0e0e0'>
								Tanggal keberangkatan
							</td>
						</tr>
						<tr>
							<td colspan=2 align='center'>
								<!--  kolom kalender -->
								<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
								</iframe>
		
								<form name="formTanggal">
									<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />			
								</form>
								<!--end kolom kalender-->
								
								<input name="update" onclick="getUpdateAsal(kota_asal.value,1)" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></input>
								
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table width='100%'> 
								  <tr><td>KOTA KEBERANGKATAN<br></td></tr>
							    <tr>
										<td>
											<select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							<div id="rewritetype"></div>
							</td>
						</tr>
						<!-- Bagian ini akan kita rewite saat requestForUpdate dipanggil -->
						<tr>
							<td colspan="2" height=150 valign='top'>
								<div id="rewriteall">
									<div id="rewriteasal"></div>
									<div id="rewritetujuan"></div>
									<div id="rewritejadwal"></div>
									<!--<span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span>-->
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><span class="noticeMessage">KETERANGAN:</span></td>
						</tr>
						<tr>
							<td valign="top"><img src='./templates/images/kursi_kosong_ket.gif' /></td>
							<td><span class="noticeMessage">Kursi masih kosong</span></td>
						</tr>
						<tr>
							<td valign="top"><img src='./templates/images/kursi_pesan_ket.gif' /></td>
							<td><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke POOL INI</span></td>
						</tr>
						<tr>
							<td valign="top"><img src='./templates/images/kursi_konfirm_ket.gif' /></td>
							<td><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke POOL ini</span></td>
						</tr>
						<tr>
							<td valign="top"><img src='./templates/images/kursi_pesan1_ket.gif' /></td>
							<td><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke pool PICK-UP</span></td>
						</tr>
						<tr>
							<td valign="top"><img src='./templates/images/kursi_konfirm1_ket.gif' /></td>
							<td><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke pool PICK-UP</span></td>
						</tr>
					</table>
				</div>
				<!-- INFO SISA KURSI KOSONG 2 JAM BERIKUTNYA -->
				<!--<div class='border' align='center'><b>Sisa kursi di jadwal-jadwal berikutnya</b><br><hr>
				<div align='left' id="rewrite_sisa_kursi"></div>
				</div>-->
				<!-- info tweater-->
				<!--<script src='http://widgets.twimg.com/j/2/widget.js'></script>
				<script>
				new TWTR.Widget({
				  version: 2,
				  type: 'profile',
				  rpp: 7,
				  interval: 45000,
				  width: 250,
				  height: 300,
				  theme: {
				    shell: {
				      background: '#e64017',
				      color: '#ffffff'
				    },
				    tweets: {
				      background: '#fad419',
				      color: '#6e6e6e',
				      links: '#0753eb'
				    }
				  },
				  features: {
				    scrollbar: true,
				    loop: true,
				    live: true,
				    hashtags: true,
				    timestamp: true,
				    avatars: false,
				    behavior: 'default'
				  }
				}).render().setUser('@TMCPoldaMetro').start();
				</script>
				<!--end info teater-->
			</td>
			<td width=1 bgcolor='e0e0e0'></td>
			<td valign='top' width='400'>
				<!--LAYOUT KURSI DAN ISIAN DATA PENUMPANG-->
				<table width='100%'>
					<tr>
						<td width='100%' valign='top' align='center'>
							<!--LAYOUT KURSI-->
							<div align='left' class='formHeader'><strong>2. Layout Kursi</strong></div>
							<hr color='e0e0e0'>
							<input type='hidden' name='kodebookingdipilih' id='kodebookingdipilih' value=''> 
							<span id='loading_layout_kursi' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Loading...</font></span>
							<div id="rewritemobil"></div>
						</td>
					</tr>
					<tr width='100%'>
						<td align='center' valign='top' width='50%'>
							<div id="rewritepaket">
							<!-- LAYOUT PAKET-->
							</div>
						</td>
					</tr>
				</table>
			</td>       
			<td width=1 bgcolor='e0e0e0'></td>
			<!--LAYOUT DATA ISIAN PENUMPANG-->
			<td valign='top' width='370'>
				<div align='left' class='formHeader'><strong>3. Data Penumpang</strong></div>
				<hr color='e0e0e0'>							
				<!--data pelanggan-->
				<div align='center'>
					<span id='loading_data_penumpang' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Sedang memproses...</font></span>
					<div id="dataPelanggan"></div>
				</div>
				
					<table width='100%'>
						<tr>
							<td width='100%'>
								<table width="100%"> 
									<!--<tr>
										<td>Kartu:</td>
										<td>
											<input name="hdn_id_member" id="hdn_id_member" type="hidden"/>
											<input name="fkartu" id="fkartu" type="text" onBlur="periksaMember(fkartu.value)" onFocus="Element.hide('label_not_found');"/>
											<input class='tombol' name="btn_periksamember" id="btn_periksamember" value="Cari" type="button" onClick="periksaMember(fkartu.value)">
										</td>
									</tr>-->
									<tr>
										<td><font size=3><b>Id Member:</b></font></td>
										<td>
											<input name="hdn_id_member" id="hdn_id_member" type="hidden" />
											<input name="id_member" id="id_member" type="text" onBlur="if(this.value==''){hdn_id_member.value='';Element.show('rewrite_list_discount');Element.hide('rewrite_keterangan_member');}else{cariDataMemberById(this.value);Element.hide('rewrite_list_discount');Element.show('rewrite_keterangan_member');}" onFocus="Element.hide('progress_cari_member');">
										</td>
									</tr>
									<tr>
										<td colspan=3 align='center' height=30 >
											<span id='label_member_not_found' style='display:none;'>
												<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>ID Member belum pernah terdaftar</b></font></td></tr></table>
											</span>
											<span id='progress_cari_member' style='display:none;'>
												<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data member...</b></font></td></tr></table>
											</span>
										</td>
									</tr>
									<tr>
										<td><font size=3><b>Telepon:</b></font><span id='telp_invalid' style='display:none;'><font color=red><b>(X)</b></font></span></td>
										<td>
											<input name="hdn_no_telp_old" id="hdn_no_telp_old" type="hidden"/>
											<input name="ftelp" id="ftelp" type="text" onkeypress='validasiNoTelp(event);'  onFocus="Element.hide('telp_invalid');Element.hide('progress_cari_penumpang');" onBlur="cariDataPelangganByTelp(ftelp.value)">
											<!--<input class='tombol' name="btn_periksanotelp" id="btn_periksanotelp" value="Cari" type="button" onClick="cariDataPelangganByTelp(ftelp.value)">-->
										</td>
									</tr>
									<tr>
										<td colspan=3 align='center' height=30 >
											<span id='label_not_found' style='display:none;'>
												<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>No.Telp belum pernah terdaftar</b></font></td></tr></table>
											</span>
											<span id='progress_cari_penumpang' style='display:none;'>
												<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data penumpang...</b></font></td></tr></table>
											</span>
										</td>
									</tr>
									<tr>
										<td>Nama: <span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
										<td>
											<input name="fnama" id="fnama" type="text" onFocus="Element.hide('nama_invalid');Element.hide('label_not_found');">
										</td>
									</tr>
									<tr>
										<td>Penumpang:</td>
										<td><span id="rewrite_list_discount"></span><span id="rewrite_keterangan_member" style='display:none'><b>Pelanggan Setia</b></span></td>
									</tr>
									<tr>
										<td>Keterangan: <span id='alamat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
										<td><textarea name="fadd" id="fadd" cols="30" rows="2" onFocus="Element.hide('alamat_invalid');Element.hide('label_not_found');"></textarea>
										</td>
									</tr>
									<tr>
										<td valign="top">Asuransi:</td>
										<td>
											<select id="asuransi" onchange="asuransiManipulateTglLahir('showasuransitgllahir',this.value);">
												<option value="">-TANPA ASURANSI-</option>
												<!-- BEGIN OPT_ASURANSI -->
												<option value="{OPT_ASURANSI.value}" title="{OPT_ASURANSI.title}">{OPT_ASURANSI.keterangan}</option>
												<!-- END OPT_ASURANSI -->
											</select>
											<div id="showasuransitgllahir" style="padding-top: 10px;">
												Tgl Lahir
												<select id="asuransitgllahir"></select>
												<select id="asuransiblnlahir" onchange="setTglLahirAsuransi('asuransitgllahir',this.value,asuransithnlahir.value,asuransitgllahir.value);">
													<option value="1">Jan</option>
													<option value="2">Feb</option>
													<option value="3">Mar</option>
													<option value="4">Apr</option>
													<option value="5">Mei</option>
													<option value="6">Jun</option>
													<option value="7">Jul</option>
													<option value="8">Agu</option>
													<option value="9">Sep</option>
													<option value="10">Okt</option>
													<option value="11">Nov</option>
													<option value="12">Des</option>
												</select>
												<select id="asuransithnlahir" onchange="setTglLahirAsuransi('asuransitgllahir',asuransiblnlahir.value,this.value);"></select>
											</div>
											<script language="javascript">setTglLahirAsuransi('asuransitgllahir',asuransiblnlahir.value,asuransithnlahir.value);asuransiManipulateTglLahir("showasuransitgllahir",asuransi.value);</script>
										</td>
									</tr>
									<tr><td align='center' colspan=2 height=40><input type="button" onclick="setGoShow();" id="hider1" value="               G O   S H O W                "></td></tr>
									<tr><td align='center' colspan=2 height=40><input type="button" onclick="pesanKursi(0);" id="hider1" value="               B O O K I N G                "></td></tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign='top'>
								<table width='100%' >
									<!--<tr><td  align='center' height=40 ><input type="button" onclick="dialog_paket.show();" id="btn_paket" value="                 P A K E T                  "></td></tr>
									<tr><td  align='center' height=40>Jum.Kursi:<input type='text' id='txt_jum_kursi_wl' size='3' maxlength='2' value='1'/>&nbsp;<input type="button" onclick="saveWaitingList();" id="hider2" value="&nbsp;&nbsp;Waiting list&nbsp;&nbsp;"></td></tr>
									<tr><td class="border" align='center'><input type="button" onclick="showDialogSwapKartu();" id="btn_transaksi_member" value="Transaksi Member"/></td></tr>-->
								</table>
							</td>
						</tr>
						<tr>
							<td align='center' valign='top' width='100%' colspan=3>
								<!-- LAYOUT WAITING LIST-->
								<!--<div id="rewritewaitinglist"></div>-->
							</td>
						</tr>
					</table>

			</td>
    </tr>        
  </table>   
 </td>
</tr>
</table>

</td>
</tr>
</table>