<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<table width="100%" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">
		<h3 id='laporan_paket'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Laporan Paket</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_DATA_PAKET}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_DATA_PAKET}"><span class="genmed">Laporan Data Paket</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_PAKET_BELUM_DIAMBIL}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_PAKET_BELUM_DIAMBIL}"><span class="genmed">Laporan Paket<br>Belum Diambil</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_OMZET_PAKET_CABANG}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_LAPORAN_OMZET_PAKET_CABANG}"><span class="genmed">Laporan Omzet Paket<br>Per Cabang</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_OMZET_PAKET_JURUSAN}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_LAPORAN_OMZET_PAKET_JURUSAN}"><span class="genmed">Laporan Omzet Paket<br>Per Jurusan</span></a>
		    </td>
			</tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_PAKET_PELANGGAN}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_PAKET_PELANGGAN}"><span class="genmed">Laporan Paket<br>Pelanggan</span></a>
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
			</tr>
		</table>
 </td>
</tr>
</table>