<script language="JavaScript">

function actCheckAll(){
	obj	= document.getElementById("checkall")
	if (obj.checked){
		selectAll();
	}
	else{
		deselectAll();
	}
}

function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
			
		}
			
		new Ajax.Request("pengaturan_jadwal.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_jadwal="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function setAktifJadwal(isaktif){
	
	if(confirm("Apakah anda yakin akan mengubah status jadwal-jadwal ini?")){
		
		i=1;
		loop=true;
		list_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				if(chk.checked){
					if(list_dipilih==""){
						list_dipilih +=chk.value;
					}
					else{
						list_dipilih +=","+chk.value;
					}
				}
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
		new Ajax.Request("pengaturan_jadwal.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=setaktifjadwal&isaktif="+isaktif+"&list_jadwal="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_jadwal.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&kode_jadwal="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function ubahOnline(kode){

  new Ajax.Request("pengaturan_jadwal.php?sid={SID}",{
    asynchronous: true,
    method: "get",
    parameters: "mode=ubahonline&kode_jadwal="+kode,
    onLoading: function(request)
    {
    },
    onComplete: function(request)
    {

    },
    onSuccess: function(request)
    {
      window.location.reload();
    },
    onFailure: function(request)
    {
    }
  });

  return false;

}

function setOnlineJadwal(isaktif){

  if(confirm("Apakah anda yakin akan mengubah status ONLINE jadwal-jadwal ini?")){

    i=1;
    loop=true;
    list_dipilih="";
    do{
      str_var='checked_'+i;
      if(chk=document.getElementById(str_var)){
        if(chk.checked){
          if(list_dipilih==""){
            list_dipilih +=chk.value;
          }
          else{
            list_dipilih +=","+chk.value;
          }
        }
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

    new Ajax.Request("pengaturan_jadwal.php?sid={SID}",{
      asynchronous: true,
      method: "get",
      parameters: "mode=setonlinejadwal&isaktif="+isaktif+"&list_jadwal="+list_dipilih,
      onLoading: function(request)
      {
      },
      onComplete: function(request)
      {

      },
      onSuccess: function(request)
      {
        window.location.reload();
        deselectAll();
      },
      onFailure: function(request)
      {
      }
    })
  }

  return false;

}
	
</script>
<div class="wrapperheader" style="height:50px;">
  <div class="headertittle">&nbsp;Master Jadwal</div>
  <div class="headerfilter" style="padding-top: 7px;">
    <form action="{ACTION_CARI}" method="post">
      Filter:&nbsp;<select id="isonline" name="isonline"><option value="" {IS_ONLINE}>-semua status online-</option><option value="0" {IS_ONLINE0}>Offline</option><option value="1" {IS_ONLINE1}>Online</option></select>
      &nbsp;<select id="isaktif" name="isaktif"><option value="" {IS_AKTIF}>-semua status aktif-</option><option value="1" {IS_AKTIF1}>Aktif</option><option value="0" {IS_AKTIF0}>Non-aktif</option></select>
      Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
    </form>
  </div>
</div>
<div style="float: left;padding-bottom: 2px;">
  <a href="{U_JADWAL_ADD}">Tambah</a>&nbsp;|&nbsp;
  <a href="" onClick="return hapusData('');">Hapus</a>&nbsp;|&nbsp;
  <a href="" onClick="return setAktifJadwal(1);">Aktifkan</a>&nbsp;|&nbsp;
  <a href="" onClick="return setAktifJadwal(0);">Non-aktifkan</a>
  <!-- BEGIN SET_ONLINE -->
  &nbsp;|&nbsp;<a href="" onClick="return setOnlineJadwal(1);">Online</a>&nbsp;|&nbsp;
  <a href="" onClick="return setOnlineJadwal(0);">Offline</a>
  <!-- END SET_ONLINE -->
</div>
<div style="float: right;padding-bottom: 2px;">
 {PAGING}
</div>
<div class="wrappertabledata">
  <table width='1100' class="border">
    <tr>
      <th width=30><input type="checkbox" id="checkall" onchange="actCheckAll();"></th>
      <th width=30>No</th>
      <th width=300>Kode Jadwal</th>
      <th width=300>Jurusan</th>
      <th width=70>Jam</th>
      <th width=70>Kursi</th>
      <th width=100>Aktif</th>
      <th width=100>Online</th>
      <th width=100>Action</th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
      <td align="center">{ROW.check}</td>
      <td align="center">{ROW.no}</td>
      <td align="center">{ROW.kode}</td>
      <td align="center">{ROW.jurusan}</td>
      <td align="center">{ROW.jam}</td>
      <td align="center">{ROW.kursi}</td>
      <td align="center">{ROW.aktif}</td>
      <td align="center" class="{ROW.classonline}">{ROW.online}</td>
      <td align="center">{ROW.action}</td>
    </tr>
    <!-- END ROW -->
    {NO_DATA}
  </table>
</div>
<div style="float: left;padding-top: 5px;">
  <a href="{U_JADWAL_ADD}">Tambah</a>&nbsp;|&nbsp;
  <a href="" onClick="return hapusData('');">Hapus</a>&nbsp;|&nbsp;
  <a href="" onClick="return setAktifJadwal(1);">Aktifkan</a>&nbsp;|&nbsp;
  <a href="" onClick="return setAktifJadwal(0);">Non-aktifkan</a>
  <!-- BEGIN SET_ONLINE -->
  &nbsp;|&nbsp;<a href="" onClick="return setOnlineJadwal(1);">Online</a>&nbsp;|&nbsp;
  <a href="" onClick="return setOnlineJadwal(0);">Offline</a>
  <!-- END SET_ONLINE -->
</div>
<div style="float: right;padding-top: 5px;">
  {PAGING}
</div>
