<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40><td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Keuangan Voucher Return</td></tr>
			<tr class='banner' height=40>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>Kota: <select id='kota' name='kota'><option value=''>-semua-</option>{OPT_KOTA}</select></td>
							<td class='bannernormal'>Status:&nbsp;</td><td><select name='status' id='status' ><option value="" {STATUS}>-semua-</option><option value="1" {STATUS1}>TERPAKAI</option><option value="0" {STATUS0}>BELUM TERPAKAI</option><option value="2" {STATUS2}>EXPIRED</option></select></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<select name='istgldigunakan' id='istgldigunakan' ><option value="0" {ISTGLDIGUNAKAN0}>Dibuat</option><option value="1" {ISTGLDIGUNAKAN1}>Digunakan</option></select>&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<!--<td>
								<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
							</td><td bgcolor='D0D0D0'></td>-->
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td>
								<table class="border">
									<tr>
										<td><b>Voucher Belum Digunakan</b></td><td width="1">:</td><td align="right">{TOTAL_BELUM_DIGUNAKAN} Voucher</td><td>|</td><td align="right">{TOTAL_RUPIAH_BELUM_DIGUNAKAN}</td>
									</tr>
									<tr>
										<td><b>Voucher Digunakan</b></td><td width="1">:</td><td align="right">{TOTAL_DIGUNAKAN} Voucher</td><td>|</td><td align="right">{TOTAL_RUPIAH_DIGUNAKAN}</td>
									</tr>
									<tr>
										<td><b>Voucher Expired</b></td><td>:</td><td align="right">{TOTAL_EXPIRED} Voucher</td><td>|</td><td align="right">{TOTAL_RUPIAH_EXPIRED}</td>
									</tr>
									<tr><td height=1 bgcolor=red colspan=5></td></tr>
									<tr>
										<td><b>Jumlah Voucher</b></td><td width="1">:</td><td align="right">{TOTAL_VOUCHER} Voucher</td><td>|</td><td align="right">{TOTAL_RUPIAH_VOUCHER}</td>
									</tr>
								</table>
							</td>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
			 <th width=30	><a class="th" >No</a></th>
			 <th width=100><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'	>#Voucher</a></th>
			 <th width=150><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'	>Dibuat</a></th>
			 <th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'	>Digunakan</a></th>
			 <th width=200><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'	>Cabang Cetak</a></th>
			 <th width=150><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'	>#Tiket</a></th>
			 <th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'  >Jadwal</a></th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'  >Penumpang</a></th>
			 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'  >Telp</a></th>
			 <th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'	>Nilai</a></th>
			 <th width=100><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>Petugas</a></th>
			 <th width=100><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>Expired</a></th>
			 <th width=300>Keterangan</th>
			 <th width=100><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>Status</a></th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.kode_voucher}</div></td>
			 <td><div align="center">{ROW.waktu_buat}</div></td>
       <td><div align="center">{ROW.waktu_pakai}</div></td>
       <td><div align="left">{ROW.cabang_cetak}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
			 <td><div align="center">{ROW.kode_jadwal}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
			 <td><div align="left">{ROW.telp}</div></td>
			 <td><div align="right">{ROW.nilai}</div></td>
			 <td><div align="left">{ROW.petugas}</div></td>
			  <td><div align="center">{ROW.expired}</div></td>
			 <td><div align="left">{ROW.keterangan}</div></td>
       <td><div align="center">{ROW.status}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>