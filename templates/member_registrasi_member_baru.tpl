<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

function validasiAngka(objek){
	temp_nilai=objek.value*0;
	nama_objek=objek.name;
	
	if(temp_nilai!=0){
		alert(nama_objek+" harus angka!");
		objek.setFocus;exit;
	}
	
	if(objek.value<=0){
		alert(nama_objek+" tidak boleh bernilai minus");
		objek.setFocus;exit;
	}
	
}

function showKonfirmasi(){
	dlg_konfirmasi.show();
}

function simpanMember(){
  
	//memeriksa apakah field id member  valid
	if(id_member.value==''){
		alert("ID Member tidak boleh kosong!");
		return false;
	}
	
	//memeriksa apakah field nama  valid
	if(nama.value==''){
		alert("Nama tidak boleh kosong!");
		document.forms.f_data_anggota.nama.focus();
		return false;
	}
	
	//memeriksa apakah field no ktp valid
	if(no_ktp.value==''){
		alert("No KTP tidak boleh kosong!");
		document.forms.f_data_anggota.no_ktp.focus();
		return false;
	}
	
	if(password.value==''){
		alert("password tidak boleh kosong!");
		document.forms.f_data_anggota.password.focus();
		return false;
	}
		
	//memeriksa apakah field konf password  valid
	if(konf_password.value==''){
		alert("Konfirmasi password tidak boleh kosong!");
		document.forms.f_data_anggota.konf_password.focus();
		return false;
	}
	
	//memeriksa apakah passwor sudah sama
	if(konf_password.value!=password.value){
		alert("Konfirmasi password tidak sama dengan password!");
		document.forms.f_data_anggota.password.focus();
		password.value="";
		konf_password.value="";
		return false;
	}
	
	parameters= 
		"id_member="+id_member.value+
		"&tgl_register="+tgl_register.value+
		"&nama="+nama.value+
		"&jenis_kelamin="+jenis_kelamin.value+
		"&tempat_lahir="+tempat_lahir.value+
		"&tgl_lahir="+tgl_lahir.value+
		"&kategori_member="+kategori_member.value+
		"&no_ktp="+no_ktp.value+
		"&alamat="+alamat.value+
		"&kota="+kota.value+
		"&kodepos="+kodepos.value+
		"&telp_rumah="+telp_rumah.value+
		"&handphone="+handphone.value+
		"&email="+email.value+
		"&password="+password.value+
		"&konf_password="+konf_password.value+
		"&mode=simpan";
		
	document.location="member_registrasi_member_baru.php?sid={SID}&"+parameters;
   
}

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=no,resizable=yes");
}

function cetakKwitansi(id_topup){
  //mencetak kwitansi
  Start("member_topup_kwitansi.php?sid={SID}&id_topup="+id_topup);
}

function init(e) {
	
	if(document.getElementById('id_topup')){
		if(document.getElementById('id_topup').value!=''){
			cetakKwitansi(document.getElementById('id_topup').value);
			document.getElementById('id_topup').value='';
		}
	}
	
	tgl_register					=document.getElementById("tgl_register");
	id_member							=document.getElementById("hdn_id_member");
	nama									=document.getElementById("nama");
	jenis_kelamin					=document.getElementById("jenis_kelamin");
	tempat_lahir					=document.getElementById("tempat_lahir");
	tgl_lahir							=document.getElementById("tgl_lahir");
	kategori_member				=document.getElementById("opt_kategori_member");
	no_ktp								=document.getElementById("no_ktp");
	alamat								=document.getElementById("alamat");
	kota									=document.getElementById("kota");
	kodepos								=document.getElementById("kodepos");
	telp_rumah						=document.getElementById("telp_rumah");
	handphone							=document.getElementById("handphone");
	email									=document.getElementById("email");
	password							=document.getElementById("password");
	konf_password					=document.getElementById("konf_password");
	
	//control dialog konfirmasi
	dlg_konfirmasi 				= dojo.widget.byId("dlg_konfirmasi");
  dlg_konfirmasiBtnYes 	= document.getElementById("dlg_konfirmasiBtnYes");
  dlg_konfirmasiBtnNo 	= document.getElementById("dlg_konfirmasiBtnNo");
  dlg_konfirmasi.setCloseControl(dlg_konfirmasiBtnYes);
  dlg_konfirmasi.setCloseControl(dlg_konfirmasiBtnNo);
	
	document.forms.frm_input_kartu.id_kartu.focus();
	
	
}

dojo.addOnLoad(init);



</script>

<div dojoType="dialog" width="400" id="dlg_konfirmasi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="100" style="display: none;">
<form onsubmit="return false;">
<font color='FFFFFF'><h3>Konfirmasi</h3></font>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center">
		<br>
		Silahkan klik pada tombol Benar untuk melanjutkan proses
		<br>
	</td>
<tr>
	<td align="center">
		<br> 
			<input type="button" onClick='simpanMember();' id="dlg_konfirmasiBtnYes" value="&nbsp;&nbsp;Benar&nbsp;&nbsp">
			<input type="button" id="dlg_konfirmasiBtnNo" value="&nbsp;Salah&nbsp;">
	</td>
</tr>
</table>
</form>
</div>

<input type='hidden' id='hdn_asc' value='0' >

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
	<tr>
		<th align='left'>Welcome {USERNAME},</th>
	</tr>
	<tr>
		<td align='left' class="indexer">{BCRUMP}</td>
	</tr>
	<tr>
		<td class="whiter" valign="middle" align="center">
			<table  width= "800" height='400' border="0" cellpadding="0" cellspacing="0">
			  <tr>
					<td align='center'>
						<h2>PENDAFTARAN MEMBER BARU</h2>
				</tr>
				<tr>
					<td align='center' valign='top'>{BODY}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
