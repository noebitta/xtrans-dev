<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script language="JavaScript">

function SetBlank(){
  	
			kode.value='';
			nama.value='';
			kota.value='';
			alamat.value='';
			telp.value='';
			fax.value='';
}

function simpan(){
  
	//memeriksa point asal
	if(kode.value==''){
		alert("Kode point rute tidak boleh kosong!");
		exit;
	}
	
	//memeriksa point asal
	if(nama.value==''){
		alert("Nama tidak boleh kosong!");
		exit;
	}
		
  if(aksi.value=='Penambahan'){
		//jika kode kosong, berarti adalah proses penambahan data point_rute baru
		mode="tambah";
	}
	else{
		//jika kode tidak kosong, berarti adalah proses pengubahan data point_rute
		mode="ubah";
	}
	
	new Ajax.Request("cabang_detail.php?sid={SID}", 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"kode_old="+kode_old.value+
			"&kode="+kode.value+
			"&nama="+nama.value+
			"&kota="+kota.value+
			"&alamat="+alamat.value+
			"&telp="+telp.value+
			"&fax="+fax.value+
			"&mode="+mode,
    onLoading: function(request) 
    {
			
    },
    onComplete: function(request) 
    {
			
		},
    onSuccess: function(request) 
    {	
			if(request.responseText!="duplikasi"){
				if(aksi.value=='Penambahan'){
					SetBlank();
				}
				
				alert("Data Cabang berhasil disimpan");
				//alert(request.responseText);
			}
			else{
				alert("Kode Cabang yang anda masukkan sudah ada dalam sistem.");
			}

		},
    onFailure: function(request) 
    {
       
    }
  })      
}

// global
var BtnSimpanCabang,BtnNoCabang;

function init(e) {
  // inisialisasi variabel
  	
	//dialog data point_rute_________________________
	BtnSimpanCabang = document.getElementById("BtnSimpanCabang");
	BtnNoCabang 		= document.getElementById("BtnNoCabang");
	
  aksi				=document.getElementById("aksi");
	kode_old		=document.getElementById("kode_old");
	kode				=document.getElementById("kode");
	nama				=document.getElementById("nama");
	kota				=document.getElementById("kota");
	alamat			=document.getElementById("alamat");
	telp				=document.getElementById("telp");
	fax					=document.getElementById("fax");
	sid					=document.getElementById("sid");
}

dojo.addOnLoad(init);

</script>
<input type='hidden' name='sid' id='sid' value='{SID}'>
<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer" >{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
		<!-- BODY -->
		<font><h2><u>{PESAN} Cabang</u></h2></font>
		
		<input type='hidden' name='aksi' id='aksi' value='{PESAN}'>
		<input type='hidden' name='kode_old' id='kode_old' value='{KODE}'>
		
		<form id='frm_data_point_rute' action="cabang.php?sid={SID}" method="post">		
		<table bgcolor='FFFFFF'>
		<tr>
		  <td class='kolomwajib'>Kode</td>
			<td>:</td>
			<td><input  name="kode" id="kode" type="text"  value='{KODE}'></td>
		</tr>
		<tr>
		  <td class='kolomwajib'>Cabang</td>
			<td>:</td>
			<td><input  name="nama" id="nama" type="text" value='{NAMA}'></td>
		</tr>
		<tr>
		  <td class='kolomwajib'>Kota</td>
			<td>:</td>
			<td>
				<select  name="kota" id="kota" >
				{OPT_KOTA}
				</select>
			</td>
		</tr>
		<tr>
		  <td class='kolomwajib'>Alamat</td>
			<td>:</td>
			<td><input  name="alamat" id="alamat" type="text"  value='{ALAMAT}'></td>
		</tr>
		<tr>
		  <td class='kolomwajib'>Telp</td>
			<td>:</td>
			<td><input  name="telp" id="telp" type="text" value='{TELP}'></td>
		</tr>
		<tr>
		  <td class='kolomwajib'>Fax</td>
			<td>:</td>
			<td><input  name="fax" id="fax" type="text" value='{FAX}'></td>
		</tr>
		<tr>
			<td align="center" colspan='3'>
				<br>
				<input type="button" id="BtnSimpanCabang" onClick="simpan();" value="&nbsp;Simpan&nbsp">
				<input type="submit" id="BtnNoCabang" value="&nbsp;Kembali&nbsp;">
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
</table>