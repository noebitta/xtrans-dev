<script language="JavaScript"  src="{TPL}/js/main.js"></script>

<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<SCRIPT LANGUAGE="JavaScript">

function Start(page) {
OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}
</SCRIPT>

<input type="hidden" value="{SID}" id="hdn_SID">

<input type="hidden" value=0 id="flag_mutasi">
<input type="hidden" value=0 id="flag_mutasi_paket">
<input type="hidden" value='' id="id_jurusan_aktif">
	
<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi_paket.js"></script>

<!--dialog PEMBATALAN-->
<div dojoType="dialog" id="dialog_pembatalan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pembatalan Paket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='pembatalan_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='pembatalan_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_pembatalan_btn_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="batalPaket();" id="dialog_pembatalan_btn_ok" value="Lanjutkan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog PEMBATALAN-->

<!--dialog Paket-->
<div dojoType="dialog" id="dialog_cari_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='900'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Paket</h2></td></tr>
			<tr><td><div id="rewrite_cari_paket"></div></td></tr>
		</table>
		<span id='progress_cari_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_paket_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog Paket-->

<!--dialog ambil paket-->
<div dojoType="dialog" id="dialog_ambil_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<div id="rewrite_ambil_paket"></div>
		<span id='progress_ambil_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_ambil_paket.hide();" id="dlg_ambil_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="prosesAmbilPaket();" id="dlg_ambil_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Ambil&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog ambil paket-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="whiter" valign="middle" align="center">
 
<table width="100%" cellspacing="0" cellpadding="4" border="0">
<tr>
	<td height='80' width='100%' bgcolor='dd0000'>
		<table width='100%' height='100%' cellspacing=0 cellpadding=0>
			<tr>	
				<td align='left' valign='middle' bgcolor='eeeeee' width='7%' background='./templates/images/icon_cari.png' STYLE='background-repeat: no-repeat;background-position: left top;'></td>
				<td align='left' valign='middle' bgcolor='eeeeee' width='23%'>
					<font color='505050'><b>Cari paket</b></font><br>
					<input name="txt_cari_paket" id="txt_cari_paket" type="text">
					<input class='tombol' name="btn_periksapaket" id="btn_periksapaket" value="Cari" type="button" onClick="periksaPaket(txt_cari_paket.value)"><br>	
					<font size=1 color='505050'>(masukkan no resi paket/ no.telp pelanggan )</font>
				</td>
				<td align='right' width='55%' valign='top'>
					<table>
						<tr>
							<td width='80' align='center' valign='top'><a class='menu' href="{U_RESERVASI}"><img src='./templates/images/icon_reservasi.png' /><br>Reservasi</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_CHECKIN_PAKET};return false"><img src='./templates/images/icon_checkin_paket.png' /><br>Check-in Paket</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_LAPORAN_UANG};return false"><img src='./templates/images/icon_penjualan_uang.png' /><br>Laporan Uang</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_LAPORAN_PENJUALAN};return false"><img src='./templates/images/icon_penjualan.png' /><br>Laporan Penjualan</a></td>
							<td width='80' align='center' valign='top' class='notifikasi_pengumuman'><div id="rewritepengumuman"></div></td>
							<td width='80' align='center' valign='top'><a class='menu' href={U_UBAH_PASSWORD}><img src='./templates/images/icon_password.png' /><br>Ubah Password</a></td>
						</tr>
					</table>
			</tr>
		</table>
	</td>
</tr>
<tr>
 <td valign="middle" align="left">    
  <table width="100%" border="0" >
		<tr class="border">
			<td width="230" valign="top">       
				<div>           
					<table border="0" width="100%">
						<tr>
							<td colspan=2 align='left'>
								<div class='formHeader'><strong>1. Jadwal</strong></div>
								<hr color='e0e0e0'>
								Tanggal keberangkatan
							</td>
						</tr>
						<tr>
							<td colspan=2 align='center'>
								<!--  kolom kalender -->
								<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
								</iframe>
		
								<form name="formTanggal">
									<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />			
								</form>
								<!--end kolom kalender-->
								
								<input name="update" onclick="getUpdateAsal(kota_asal.value,1)" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></input>
								
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table width='100%'> 
								  <tr><td>KOTA KEBERANGKATAN<br></td></tr>
							    <tr>
										<td>
											<select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							<div id="rewritetype"></div>
							</td>
						</tr>
						<!-- Bagian ini akan kita rewite saat requestForUpdate dipanggil -->
						<tr>
							<td colspan="2" height=150 valign='top'>
								<div id="rewriteall">
									<div id="rewriteasal"></div>
									<div id="rewritetujuan"></div>
									<div id="rewritejadwal"></div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
			<td width=1 bgcolor='e0e0e0'></td>
			<td valign='top' width='400'>
				<!--LAYOUT KURSI DAN ISIAN DATA PENUMPANG-->
				<table width='100%'>
					<tr width='100%'>
						<td align='center' valign='top' width='50%'>
							<div align='left' class='formHeader'><strong>2. List Paket</strong></div>
							<hr color='e0e0e0'>
							<span id='loading_layout_kursi' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Loading...</font></span>
							<div id="rewritepaket">
							<!-- LAYOUT PAKET-->
							</div>
						</td>
					</tr>
				</table>
			</td>       
			<td width=1 bgcolor='e0e0e0'></td>
			<!--LAYOUT DATA ISIAN PENUMPANG-->
			<td valign='top' width='370'>
				<div align='left' class='formHeader'><strong>3. Data Paket</strong></div>
				<hr color='e0e0e0'>							
				<!--data pelanggan-->
				<div align='center'>
					<span id='loading_data_penumpang' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Sedang memproses...</font></span>
					<div id="dataPelanggan"></div>
				</div>
				
				<!--data isian pengiriman paket-->
				<form onsubmit="return false;">
					<table width='400'>
					<tr>
						<td bgcolor='ffffff' valign='top' align='center'>
							<table cellspacing=0 cellpadding=0 width='100%'>
								<tr>
									<td width='50%' valign='top'>
										<table>
											<tr><td colspan=3><h2>Data Pengirim</h2></td></tr>
											<tr><td>Telp Pengirim</td><td>:</td><td><input id='dlg_paket_telp_pengirim' type='text' maxlength='15' onkeypress='validasiNoTelp(event);' onFocus="this.style.background='white';" onBlur="cariDataPelangganByTelp4Paket(this,1)"/></td></tr>
											<tr><td width=200>Nama Pengirim</td><td width=5>:</td><td><input id='dlg_paket_nama_pengirim' type='text' maxlength='100' onFocus="this.style.background='white';" onkeypress="charCheck(event);" /></td></tr>
											<tr><td valign='top'>Alamat Pengirim</td><td valign='top'>:</td><td><textarea id='dlg_paket_alamat_pengirim' rows='4' cols='30' onkeypress="charCheck(event);"></textarea></td></tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width='50%' valign='top'>
										<table>
											<tr><td colspan=3><h2>Data Penerima</h2></td></tr>
											<tr><td>Telp Penerima</td><td>:</td><td><input id='dlg_paket_telp_penerima' type='text' maxlength='15' onFocus="this.style.background='white';" onkeypress='validasiNoTelp(event);' onBlur="cariDataPelangganByTelp4Paket(this,0)"/></td></tr>
											<tr><td width=200>Nama Penerima</td><td width=5>:</td><td><input id='dlg_paket_nama_penerima' type='text' maxlength='100' onFocus="this.style.background='white';" onkeypress="charCheck(event);"/></td></tr>
											<tr><td valign='top'>Alamat Penerima</td><td valign='top'>:</td><td><textarea id='dlg_paket_alamat_penerima' rows='4' cols='30' onkeypress="charCheck(event);"></textarea></td></tr>
										</table>
									</td>
								</tr>
								<tr><td colspan=2><br><h2>Data Paket</h2></td></tr>
								<tr>
									<td width='50%' valign='top'>
										<input id='dlg_paket_harga_kg_pertama_p' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_pertama_ga' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_pertama_gd' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_pertama_s' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_pertama_ca' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_pertama_cd' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_berikutnya_p' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_berikutnya_ga' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_berikutnya_gd' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_berikutnya_s' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_berikutnya_ca' type='hidden' value=''/>
										<input id='dlg_paket_harga_kg_berikutnya_cd' type='hidden' value=''/>
										<input id='dlg_paket_harga_motor1' type='hidden' value=''/>
										<input id='dlg_paket_harga_motor2' type='hidden' value=''/>
													
										<div style="position: absolute;margin-left: 5px;">Jumlah Koli</div><div style="position: inherit;margin-left: 160px;">:&nbsp;<input id='dlg_paket_jumlah_koli' type='text' maxlength='10' onkeypress='validasiInputanAngka(event);' onFocus="this.style.background='white';" /></div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Berat (Kg)</div><div style="position: inherit;margin-left: 160px;margin-top: -10px;">:&nbsp;<input id='dlg_paket_berat' type='text' maxlength='10' size='10' onkeypress='validasiInputanAngka(event);' onFocus="this.style.background='white';" onBlur="hitungHargaPaket(document.getElementById('dlg_paket_layanan').value);" />&nbsp;Kg.</div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Jenis Barang</div>
										<div style="position: inherit;margin-left: 160px;margin-top: -10px;">:
											<select id='dlg_paket_jenis_barang'>
												<option value="DOKUMEN">Dokumen</option>
												<option value="PAKET">Paket</option>
											</select>
										</div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Layanan</div>
										<div style="position: inherit;margin-left: 160px;margin-top: -10px;">:
											<select id='dlg_paket_layanan' onChange="hitungHargaPaket(this.value);">
												<option value="P">Platinum</option>
												<option value="GA">Gold Antar</option>
												<option value="GD">Gold Diambil</option>
												<option value="S">Silver</option>
												<option value="CA">Cargo Antar</option>
												<option value="CD">Cargo Diambil</option>
												<option value="M1">Motor Kecil</option>
												<option value="M2">Motor Besar</option>
												<option value="I">Internal</option>
											</select>
										</div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Harga</div><div style="position: inherit;margin-left: 160px;margin-top: -10px;">:&nbsp;Rp. <span id='rewrite_dlg_paket_harga_paket_show'></span></div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Cara Bayar</div>
										<div style="position: inherit;margin-left: 160px;margin-top: -10px;">:
											<select id='dlg_paket_cara_bayar' onChange="caraBayarClick(this.value);">
												<option value="0">Tunai</option>
												<option value="1">Langganan</option>
											</select>
											<div id="linkdaftapelanggan" style="display: none; position: absolute ;margin-left: 100px; margin-top: -15px;">&nbsp;<a href="#" onclick="if(document.getElementById('p_jadwal')){window.open('paket.pelanggan.popup.php?sid={SID}','','width=800,height=400');}else{alert('Anda belum memilih jadwal!');}return false;">Data Pelanggan</a></div>
										</div><br>
										<div style="position: inherit;" id="diskondisplay">
											<div style="position: absolute;margin-left: 5px;">Diskon</div>
											<div style="position: inherit;margin-left: 160px;margin-top: -10px;">:
												<select id="diskon" onChange="hitungHargaPaket(dlg_paket_layanan.value);"><option value=0>-none-</option><option value=0.1>10%</option><option value=0.15>15%</option><option value=0.2>20%</option></select>
											</div>
										</div><br>
										<div style="position: inherit;display: none;" id="datapelanggandisplay">
											<input type="hidden" id="kodepelanggan" />
											<input type="hidden" id="besardiskonpelanggan" />
											<div style="position: absolute;margin-left: 5px;">Nama Pelanggan</div><div style="position: inherit;margin-left: 160px;margin-top: -10px;">:&nbsp;<span id='namapelanggandisplay'></span></div><br>
											<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Diskon</div><div style="position: inherit;margin-left: 160px;margin-top: -10px;" id='diskonpelanggandisplay'></div><br>
										</div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Total Bayar</div><div style="position: inherit;margin-left: 160px;margin-top: -10px;">:&nbsp;Rp. <span id='rewrite_total_bayar_show'></span><input type="hidden" id="totalbayar" value=""/></div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: -10px;">Isi Barang</div><div style="position: absolute;margin-left: 160px;margin-top: -10px;vertical-align: top;">:&nbsp;<textarea id='dlg_paket_keterangan' rows='2' cols='30' onFocus="this.style.background='white';" style="position: inherit;" onkeypress="charCheck(event);"></textarea></div><br>
										<div style="position: absolute;margin-left: 5px;margin-top: 40px;">Instruksi Khusus</div><div style="position: absolute;margin-left: 160px;margin-top: 40px;vertical-align: top;">:&nbsp;<textarea id='dlg_paket_intruksi_khusus' rows='2' cols='30' style="position: inherit;" onkeypress="charCheck(event);"></textarea></div><br>
										<div style="position: inherit;margin-top: 60px;">&nbsp;</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
					   <td colspan="2" align="center">  
							<br>
							<input type="button" onClick="resetIsianPaket();" id="dlg_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
							<input type="button" onclick="pesanPaket();" id="dlg_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;">
						 </td>
					</tr>
					</table>
					</form>

			</td>
    </tr>        
  </table>   
 </td>
</tr>
</table>

</td>
</tr>
</table>