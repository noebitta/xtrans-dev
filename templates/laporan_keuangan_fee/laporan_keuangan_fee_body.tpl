<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr height=40 class='banner'>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap Fee Transaksi</td>
				<td class="bannerjudul">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td >
								<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
							</td><td width=1></td>
							<td class='border'>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='left'>
								<a href="{U_LAPORAN_OMZET_GRAFIK}"><img src="{TPL}images/icon_grafik.png" /></a>
								<a href="{U_LAPORAN_OMZET_GRAFIK}">Lihat Grafik</a>
							</td>
							<td align='right' valign='bottom'>
								{LIST_BULAN}
								&nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp;												
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30>Tanggal</th>
			 <th width=200>SPJ</th>
			 <th width=200>Tiket</th>
			 <th width=200>Tiket Batal</th>
			 <th width=200>Paket</th>
			 <th width=200>Fee Tiket</th>
			 <th width=200>Fee Paket</th>
			 <th width=200>Total Fee</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="center"><font size=3 color='{ROW.font_color}'><b>{ROW.tgl}</b></font></div></td>
			 <td ><div align="right">{ROW.spj}</div></td>
			 <td ><div align="right">{ROW.tiket}</div></td>
			 <td ><div align="right">{ROW.tiket_batal}</div></td>
			 <td ><div align="right">{ROW.paket}</div></td>
			 <td ><div align="right">{ROW.fee_tiket}</div></td>
			 <td ><div align="right">{ROW.fee_paket}</div></td>
			 <td ><div align="right">{ROW.total_fee}</div></td>
     </tr>
     <!-- END ROW -->
		 <tr bgcolor='ffff00'>
       <td ><div align="center"><font size=3 color='{ROW.font_color}'><b>TOTAL</b></font></div></td>
			 <td ><div align="right"><b>{SUM_SPJ}</b></div></td>
			 <td ><div align="right"><b>{SUM_TIKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_TIKET_BATAL}</b></div></td>
			 <td ><div align="right"><b>{SUM_PAKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_FEE_TIKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_FEE_PAKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_TOTAL_FEE}</b></div></td>
     </tr>
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>