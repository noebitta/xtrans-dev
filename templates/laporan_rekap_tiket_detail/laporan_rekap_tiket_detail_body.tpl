<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form action="{ACTION_CARI}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr bgcolor='505050' height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Rekap Tiket</td>
								<td align='right' valign='middle' >
									<table>
										<tr><td class='bannernormal'>
											&nbsp;Periode:&nbsp;<input readonly="yes"  id="p0" name="p0" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
											&nbsp; s/d &nbsp;<input readonly="yes"  id="p1" name="p1" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
											<input type="hidden" id="p2" name="p2" value="{PENCARI}" />											
											<input type="hidden" id="p3" name="p3" value="{JENIS_LAPORAN}" />											
										</td></tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan=2 align='center' valign='middle'>
									<table>
										<tr>
											<td>
												<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				<h2>Perolehan Tiket</h2>
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table>
    <tr>
       <th width=30>No</th>
			 <th width=200>Waktu Pesan</th>
			 <th width=200>No.Tiket</th>
			 <th width=200>Waktu Berangkat</th>
			 <th width=100>Kode Jadwal</th>
			 <th width=200>Nama</th>
			 <th width=50>Kursi</th>
			 <th width=100>Harga Tiket</th>
			 <th width=100>Discount</th>
			 <th width=100>Plan Asuransi</th>
			 <th width=100>Premi Asuransi</th>
			 <th width=70>Total</th>
			 <th width=100>Tipe Disc.</th>
			 <th width=200>CSO</th>
			 <th width=100>Status</th>
			 <th width=100>Ket.</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.waktu_pesan}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
       <td><div align="left">{ROW.waktu_berangkat}</div></td>
       <td><div align="left">{ROW.kode_jadwal}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
			 <td><div align="center">{ROW.no_kursi}</div></td>
			 <td><div align="right">{ROW.harga_tiket}</div></td>
			 <td><div align="right">{ROW.discount}</div></td>
			 <td><div align="left">{ROW.plan_asuransi}</div></td>
			 <td><div align="right">{ROW.premi_asuransi}</div></td>
			 <td><div align="right">{ROW.total}</div></td>
			 <td><div align="left">{ROW.tipe_discount}</div></td>
			 <td><div align="left">{ROW.cso}</div></td>
       <td><div align="center">{ROW.status}</div></td>
       <td><div align="left">{ROW.ket}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
<tr><td><hr color='d0d0d0'></td></tr>
<tr>
	<td>
		<table>
		<tr>
				<td align='left' valign='bottom' colspan=3>
				<h2>Perolehan Paket</h2>
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
    <tr>
       <th width=30>No</th>
			 <th width=200>Waktu Pesan</th>
			 <th width=200>Waktu Berangkat</th>
			 <th width=200>No.Tiket</th>
			 <th width=100>Kode Jadwal</th>
			 <th width=200>Dari</th>
			 <th width=200>Untuk</th>
			 <th width=100>Harga Paket</th>
			 <th width=200>CSO</th>
			 <th width=100>Status</th>
			 <th width=100>Ket.</th>
     </tr>
     <!-- BEGIN ROWPAKET -->
     <tr class="{ROWPAKET.odd}">
       <td><div align="right">{ROWPAKET.no}</div></td>
       <td><div align="left">{ROWPAKET.waktu_pesan}</div></td>
       <td><div align="left">{ROWPAKET.waktu_berangkat}</div></td>
			 <td><div align="left">{ROWPAKET.no_tiket}</div></td>
       <td><div align="left">{ROWPAKET.kode_jadwal}</div></td>
			 <td><div align="left">{ROWPAKET.dari}</div></td>
			 <td><div align="left">{ROWPAKET.untuk}</div></td>
			 <td><div align="right">{ROWPAKET.harga_paket}</div></td>
			 <td><div align="left">{ROWPAKET.cso}</div></td>
       <td><div align="center">{ROWPAKET.status}</div></td>
       <td><div align="left">{ROWPAKET.ket}</div></td>
     </tr>  
     <!-- END ROWPAKET -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>