<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function prosesVerifikasi(){
		
		kode_verifikasi	= document.getElementById('kodeverifikasi').value;
		
		if(kode_verifikasi==''){
			alert('Anda belum memasukkan kode Verifikasi!');
			return;
		}
		
		new Ajax.Request("deposit_redbus.php?sid={SID}",
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=verifybydialog&kodereferensi="+document.getElementById("kodereferensi").value+"&kodeverifikasi="+kode_verifikasi,
			onLoading: function(request) 
			{
			},
			onComplete: function(request) 
			{
			},
			onSuccess: function(request) 
			{
				if(request.responseText==1){
					dlg_verifikasi.hide();
					alert("Saldo telah berhasil ditambahkan!");
					location.reload();
				}
				else{
					document.getElementById("kodeverifikasi").value="";
					alert("Kode Verifikasi yang anda masukkan tidak benar!");
				}
			},
			onFailure: function(request) 
			{
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		})
		
		Element.hide('progress_kursi');
	}	
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function init(e) {
		//control dialog verifikasi
		dlg_verifikasi	= dojo.widget.byId("dlgkodeverifikasi");
		btn_cancel 			= document.getElementById("dlgbtncancel");
		dlg_verifikasi.setCloseControl(btn_cancel);
	}
	
	dojo.addOnLoad(init);
	
</script>

<!--dialog OTP-->
<div dojoType="dialog" id="dlgkodeverifikasi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Verifikasi</h2></td></tr>
<tr>
	<td align='center'>
		<input type="hidden" id="kodereferensi" name="kodereferensi" />
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3 align='center'>Untuk melakukan proses ini,silahkan masukkan kode Verifikasi anda</td></tr>
			<tr height=30><td align='right'>Kode Verifikasi</td><td>:</td><td><input type='password' id='kodeverifikasi' maxlength='6'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dlgbtncancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="prosesVerifikasi();" id="dlgbtnverifikasi" value="Verifikasi">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog OTP-->

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40><td align='center' valign='middle' class="bannerjudul">&nbsp;Top Up Deposit RedBus</td></tr>
			<tr class='banner' height=40>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<!--<td class='bannernormal'>Status:&nbsp;</td><td><select name='status' id='status' ><option value="" {STATUS}>-semua-</option><option value="1" {STATUS1}>Dibayar</option><option value="0" {STATUS0}>Booking</option><option value="2" {STATUS2}>Batal</option></select></td>-->
							<td class='bannernormal'>Status:&nbsp;<select id='filterstatus' name='filterstatus'><option value='' {OPT_STATUS}>-semua-</option><option value=0 {OPT_STATUS0}>Not Verified</option><option value=1 {OPT_STATUS1}>Verified</option></select></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<!--<td>
								<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
							</td><td bgcolor='D0D0D0'></td>-->
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td>
								<b>Saldo Deposit: Rp. {SALDO_DEPOSIT}</b></br></br>
								<a href="{ACT_ADD}"> + Tambah Deposit</a>
							</td>
							<td align='right' valign='bottom'>
								{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
			<tr>
				<th width=30><a class="th" >No</th>
				<th width=200><a class="th" >Waktu Trx</th>
				<th width=200><a class="th" >#Ref</th>
				<th width=150><a class="th" >Jumlah</th>
				<th width=200><a class="th" >User</th>
				<th width=150><a class="th" >Status</th>
				<th width=150><a class="th" >Verifikator</th>
				<th width=200><a class="th" >Waktu Verikasi</th>
				<th width=100><a class="th" >Action</th>
			</tr>
			<!-- BEGIN ROW -->
			<tr class="{ROW.odd}">
				<td><div align="right">{ROW.no}</div></td>
				<td><div align="center">{ROW.waktu_trx}</div></td>
				<td><div align="left">{ROW.kode_referensi}</div></td>
				<td><div align="right">{ROW.jumlah}</div></td>
				<td><div align="center">{ROW.user}</div></td>
				<td><div align="center">{ROW.status}</div></td>
				<td><div align="center">{ROW.verifikator}</div></td>
				<td><div align="center">{ROW.waktu_verifikasi}</div></td>
				<td><div align="center">{ROW.action}</div></td>
			</tr>  
			<!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>