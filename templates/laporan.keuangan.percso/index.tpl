<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}

</script>

<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<form action="{ACTION_CARI}" method="post">
  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">&nbsp;Laporan Keuangan Per CSO</div>
    <div class="headerfilter" style="padding-top: 7px;">
      <input type='hidden' id='is_cari' name='is_cari' value='1'>
      Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">&nbsp;
      s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
      Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;">
    <input type="hidden" id="idsort" name="idsort" value="{SORT_BY}" >
    <a href="#" onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>&nbsp;|&nbsp;
    Urutkan:<select id="order" name="order" onchange="form.submit();"><option value="ASC" {ORDER_BY_ASC}>A->Z</option><option value="DESC" {ORDER_BY_DESC}>Z->A</option></select>
  </div>
  <div style="float: right;padding-bottom: 2px;">
    {PAGING}
  </div>
</form>
<div class="wrappertabledata">
  <table width="2550" class="flat">
    <tr>
      <th width=30 rowspan=2 >No</th>
      <th width=150 rowspan=2><a class="th" href="{A_SORT}&idsort=1">Nama</a></th>
      <th width=100 rowspan=2><a class="th" href="{A_SORT}&idsort=2">NRP</a></th>
      <th width=100 rowspan=2><a class="th" href="{A_SORT}&idsort=3">Cabang</a></th>
      <th colspan=9>Pax Tiket</th>
      <th colspan=5>Uang Tiket (Rp.)</th>
      <th colspan=8>Pax Paket</th>
      <th colspan=4>Uang Paket (Rp.)</th>
      <th width="150" rowspan="2"><a class="th" href="{A_SORT}&idsort=30">Total Setor (Rp.)</a></th>
      <th width="100" rowspan="2">Action</th>
    </tr>
    <tr>
      <th width="60"><a class="th" href="{A_SORT}&idsort=4">U</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=5">M</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=6">K</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=7">KK</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=8">G</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=9">R</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=10">VR</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=11">V</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=12">T</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=13">Tunai</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=14">Debit</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=15">Cr.Card</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=16">Discount</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=17">Total</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=18">P</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=19">GD</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=20">GA</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=21">S</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=22">CA</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=23">CD</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=24">I</a></th>
      <th width="60"><a class="th" href="{A_SORT}&idsort=25">T</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=26">Umum</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=27">Langganan</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=28">Discount</a></th>
      <th width="100"><a class="th" href="{A_SORT}&idsort=29">Total</a></th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
      <td align="right">{ROW.no}</td>
      <td align="left" title="Nama CSO">{ROW.nama}</td>
      <td align="left" title="NRP">{ROW.nrp}</td>
      <td align="left" title="Cabang">{ROW.cabang}</td>
      <td align="right" title="Tiket Umum">{ROW.tiket_pax_u}</td>
      <td align="right" title="Tiket Mahasiswa">{ROW.tiket_pax_m}</td>
      <td align="right" title="Tiket Khusus">{ROW.tiket_pax_k}</td>
      <td align="right" title="Tiket Keluarga Karyawan">{ROW.tiket_pax_kk}</td>
      <td align="right" title="Tiket Gratis">{ROW.tiket_pax_g}</td>
      <td align="right" title="Tiket Return">{ROW.tiket_pax_r}</td>
      <td align="right" title="Tiket Voucher Return">{ROW.tiket_pax_vr}</td>
      <td align="right" title="Tiket Voucher">{ROW.tiket_pax_v}</td>
      <td align="right" title="Total Tiket">{ROW.tiket_pax_total}</td>
      <td align="right" title="Tiket Bayar Tunai">{ROW.tiket_omz_tunai}</td>
      <td align="right" title="Tiket Bayar Debit">{ROW.tiket_omz_debit}</td>
      <td align="right" title="Tiket Bayar Kartu Kredit">{ROW.tiket_omz_credit}</td>
      <td align="right" title="Total Discount Tiket">{ROW.tiket_omz_discount}</td>
      <td align="right" title="Total Omzet Tiket">{ROW.tiket_omz_total}</td>
      <td align="right" >{ROW.paket_pax_p}</td>
      <td align="right" >{ROW.paket_pax_gd}</td>
      <td align="right" >{ROW.paket_pax_ga}</td>
      <td align="right" >{ROW.paket_pax_s}</td>
      <td align="right" >{ROW.paket_pax_ca}</td>
      <td align="right" >{ROW.paket_pax_cd}</td>
      <td align="right" >{ROW.paket_pax_i}</td>
      <td align="right" >{ROW.paket_pax_total}</td>
      <td align="right" >{ROW.paket_omz_umum}</td>
      <td align="right" >{ROW.paket_omz_langganan}</td>
      <td align="right" >{ROW.paket_omz_discount}</td>
      <td align="right" >{ROW.paket_omz_total}</td>
      <td align="right" >{ROW.total_setor}</td>
      <td align="center">{ROW.act}</td>
    </tr>
    <!-- END ROW -->
  </table>
</div>
<div style="float: left;padding-bottom: 2px;">
  <a href="#" onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  {PAGING}
</div>