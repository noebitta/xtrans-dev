<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">	
	// komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function showDialogBuatVoucher(){
		nilaivoucher.value="";
		keterangan.value="";
		jumlahcetak.value="";
		dlg_buat_voucher.show();
	}
	
	function getUpdateTujuan(asal){
		// fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		document.getElementById('rewrite_list_jurusan').innerHTML = "";
		
		new Ajax.Updater("rewrite_list_jurusan","voucher.group.batchvoucher.php?sid={SID}", 
		{
			asynchronous: true,
			method: "get",
			parameters: "mode=gettujuan&asal="+asal,
			onLoading: function(request) 
			{
					Element.show('loading_tujuan');
			},
			onComplete: function(request) 
			{
					Element.hide('loading_tujuan');
			},
			onFailure: function(request) 
			{ 
				assignError(request.responseText); 
			}
		});   
	}
	
	function buatVoucher(){
		
		is_valid=true;
		
		if(nilaivoucher.value==""){
			nilaivoucher.style.background = 'red';
			is_valid = false;
		}
		
		if(keterangan.value==""){
			keterangan.style.background = 'red';
			is_valid = false;
		}
		
		if(jumlahcetak.value==""){
			jumlahcetak.style.background = 'red';
			is_valid = false;
		}
		
		if(!is_valid){
			exit;
		}
		
		new Ajax.Request("voucher.group.batchvoucher.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters: "mode=buatvoucher"+
				"&idgroup="+{ID_GROUP}+
				"&jurusan="+jurusan.value+
				"&expireddate="+expireddate.value+
				"&ishargatetap="+ishargatetap.value+
				"&nilaivoucher="+nilaivoucher.value+
				"&isbolehweekend="+isbolehweekend.value+
				"&ispersentase="+ispersentase.value+
				"&keterangan="+keterangan.value+
				"&jumlahcetak="+jumlahcetak.value,
			onLoading: function(request){
				progressbar.show();
			},
			onComplete: function(request){
				progressbar.hide();
				dlg_buat_voucher.hide();
			},
			onSuccess: function(request){
				eval(request.responseText);
			},
			onFailure: function(request){
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		});
	
	}

  function suspendVoucher(idgroup,batchid,issuspend){

    keteranganact = issuspend=="1"?"MEMBUKA ":"men-";

    if(!confirm("Apakah anda yakin akan "+keteranganact+"suspend voucher-voucher pada 'Batch' ini?")){
      exit;
    }

    new Ajax.Request("voucher.group.batchvoucher.php?sid={SID}",{
      asynchronous: true,
      method: "get",
      parameters: "mode=suspendvoucher"+
      "&idgroup="+idgroup+
      "&batchid="+batchid+
      "&issuspend="+issuspend,
      onLoading: function(request){
        progressbar.show();
      },
      onComplete: function(request){
        progressbar.hide();
      },
      onSuccess: function(request){
        eval(request.responseText);
      },
      onFailure: function(request){
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    });

  }

	function hapusVoucher(idgroup,batchid){

    if(!confirm("Apakah anda yakin akan MENGHAPUS voucher-voucher pada 'Batch' ini?")){
      exit;
    }

    new Ajax.Request("voucher.group.batchvoucher.php?sid={SID}",{
      asynchronous: true,
      method: "get",
      parameters: "mode=hapusvoucher"+
      "&idgroup="+idgroup+
      "&batchid="+batchid,
      onLoading: function(request){
        progressbar.show();
      },
      onComplete: function(request){
        progressbar.hide();
      },
      onSuccess: function(request){
        eval(request.responseText);
      },
      onFailure: function(request){
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    });

  }

	function init(e){
		//control dialog paket
		dlg_buat_voucher						= dojo.widget.byId("dialogbuatvoucher");
		dlg_buat_voucher_btn_cancel = document.getElementById("dialogbuatvouchercancel");
		getUpdateTujuan(null);
	}
	
	dojo.addOnLoad(init);
	
</script>

<!--dialog Create Voucher-->
<div dojoType="dialog" id="dialogbuatvoucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width="400">
<tr>
	<td bgcolor='ffffff' align='center'>
		<table>
			<tr><td colspan='2'><h2>Buat Voucher</h2></td></tr>
			<tr><td align='right'>Digunakan dari :</td><td><select id='cabangasal' name='cabangasal' onChange="getUpdateTujuan(this.value);">{OPT_ASAL}</select></td></tr>
			<tr><td align='right'>Digunakan ke :</td><td><div id="rewrite_list_jurusan"></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span></td></tr>
			<tr><td align='right'>Tgl.Expired:</td><td><input id="expireddate" name="expireddate" type="text" readonly='yes' size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_EXPIRED}"></td></tr>
			<tr><td align='right'><select id='ishargatetap'><option value='0'>Diskon</option><option value='1'>Harga</option></select></td><td><select id='ispersentase'><option value='0'>Rp</option><option value='1'>%</option></select>&nbsp;<input id="nilaivoucher" name="nilaivoucher" type="text" onkeypress='validasiAngka(event);' onfocus="this.style.background='white'"></td></tr>
			<tr><td align='right'>Waktu Pemakaian:</td><td><select id='isbolehweekend'><option value='0'>Hanya Week Day</option><option value='1'>Semua Hari</option></select></td></tr>
			<tr><td align='right'>Keterangan:</td><td><input id="keterangan" name="keterangan" type="text" onfocus="this.style.background='white'"/></td></tr>
			<tr><td align='right'>Jumlah Cetak:</td><td><input id="jumlahcetak" name="jumlahcetak" type="text" maxlength='4' onkeypress='validasiAngka(event);' onfocus="this.style.background='white'"/></td></tr>
		</table>
		<span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialogbuatvouchercancel" value="&nbsp;Cancel&nbsp;" onClick="dlg_buat_voucher.hide();"> 
		<input type="button" onclick="buatVoucher();" id="dialogbuatvoucherproses" value="&nbsp;Proses&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog create voucher-->

<div class="wrapperheader" style="height:50px;">
  <div class="headertittle">&nbsp;Daftar Voucher</div>
  <div class="headerfilter" style="padding-top: 7px;">
    <form action="{ACTION_CARI}" method="post">
      &nbsp;Tgl:&nbsp;<input readonly="yes"  id="tglawal" name="tglawal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">&nbsp;
      s/d &nbsp;<input readonly="yes"  id="tglakhir" name="tglakhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">&nbsp;
      Cari:<input type="text" id="cari" name="cari" value="{CARI}" />&nbsp;<input name="btn_cari" type="submit" value="cari" />
    </form>
  </div>
</div>
<div style="text-align: center;font-size: 16px;padding-top: 5px;">Nama Korporat: "{NAMA_GROUP}"</div>
<div style="float: left;padding-bottom: 2px;"><a href="#" onclick="showDialogBuatVoucher();return false;">[+]Buat Voucher</a></div>
<div class="wrappertabledata">
  <table width='1380' >
    <tr>
      <th width=150></th>
      <th width=30>No</th>
      <th width=150>Waktu Buat</th>
      <th width=50>Qty</th>
      <th width=150>Dibuat Oleh</th>
      <th width=150>Jenis</th>
      <th width=150>Nilai</th>
      <th width=150>Settlement</th>
      <th width=150>Hari Berlaku</th>
      <th width=100>Expired</th>
      <th width=150>Keterangan</th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
      <td align="center">{ROW.action}</td>
      <td align="center">{ROW.no}</td>
      <td align="center">{ROW.waktubuat}</td>
      <td align="right">{ROW.jumlah}</td>
      <td align="center">{ROW.dibuatoleh}</td>
      <td align="center">{ROW.jenis}</td>
      <td align="right">{ROW.nilai}</td>
      <td align="center">{ROW.settlement}</td>
      <td align="center">{ROW.hariberlaku}</td>
      <td align="center">{ROW.expired}</td>
      <td align="center">{ROW.keterangan}</td>
    </tr>
    <!-- END ROW -->
  </table>
</div>
<!-- BEGIN NO_DATA -->
<div style="font-size: 18px;background: yellow;text-align: center;">Tidak ada data ditemukan</div>
<!-- END NO_DATA -->
<div style="float: left;padding-bottom: 2px;"><a href="#" onclick="showDialogBuatVoucher();return false;">[+]Buat Voucher</a></div>