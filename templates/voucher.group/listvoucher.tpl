<script src="{TPL}js/main.js" type="text/javascript"></script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<input name="idgroup" type="hidden" value="{ID_GROUP}" />
		<input name="waktucetak" type="hidden" value="{WAKTU_CETAK}" />
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Daftar Voucher</td>
				<td align="right" class="bannernormal">Cari:<input type="text" id="cari" name="cari" value="{CARI}" placeholder="kode voucher,nama penumpang" size="30"/>&nbsp;<input name="btn_cari" type="submit" value="cari" />&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<div style="text-align: center;font-size: 16px;padding-top: 5px;">Nama Group: "{NAMA_GROUP}"</div><br>
					<center><a href="{URL_EXPORT}" class="b_exp_excel" style="width: 120px;">Export Excel</a></center>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
			 <th width=30	>No</th>
			 <th width=120>#Voucher</th>
			 <th width=150>Waktu Buat</th>
			 <th width=150>Dibuat Oleh</th>
			 <th width=150>Digunakan</th>
			 <th width=150>#Tiket</th>
			 <th width=150>Jadwal</th>
			 <th width=150>Penumpang</th>
			 <th width=150>Telp</th>
			 <th width=120>Nilai</th>
			 <th width=150>CSO</th>
			 <th width=150>Keterangan</th>
			 <th width=70>Act</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
			 <td align="center">{ROW.kodevoucher}</td>
       <td align="center">{ROW.waktubuat}</td>
			 <td align="center">{ROW.dibuatoleh}</td>
			 <td align="center">{ROW.digunakan}</td>
       <td align="center">{ROW.notiket}</td>
			 <td align="center">{ROW.jadwal}</td>
			 <td align="center">{ROW.penumpang}</td>
			 <td align="center">{ROW.telp}</td>
			 <td align="right">Rp. {ROW.nilai}</td>
			 <td align="center">{ROW.cso}</td>
			 <td align="center">{ROW.keterangan}</td>
			 <td align="center">{ROW.act}</td>
     </tr>  
     <!-- END ROW -->
    </table>
		<!-- BEGIN NO_DATA -->
		<div style="font-size: 18px;background: yellow;text-align: center;">Tidak ada data ditemukan</div>
    <!-- END NO_DATA -->
	</form>
 </td>
</tr>
</table>