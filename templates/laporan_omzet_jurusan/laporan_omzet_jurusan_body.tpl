<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Omzet Jurusan</td>
				<td align='right' valign='middle'>
					<table>
						<tr><td class='bannernormal'>
							&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
							&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
							&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
							<input type="submit" value="cari" />&nbsp;								
						</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30 rowspan=2 >No</th>
			 <th width=400 rowspan=2><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jurusan</a></th>
			 <th width=100 rowspan=2><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Jurusan</a></th>
			 <th width=100 rowspan=2><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Trip</a></th>
			 <th colspan=7>Total Penumpang</th>
			 <th width=100 rowspan=2><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>Pnp<br>/Trip</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>Tot.Omz.Pnp (Rp.)</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>Tot.Pkt</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_14}' title='{TIPS_SORT_14}'>Tot.Omz.Pkt</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_15}' title='{TIPS_SORT_15}'>Tot.Disc</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_16}' title='{TIPS_SORT_16}'>Tot.Biaya Langsung</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_17}' title='{TIPS_SORT_17}'>Tot.Laba Kotor</a></th>
			 <th width=100 rowspan=2>Action</a></th>
     </tr>
		 <tr>
			<th><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>U</a></th>
			<th><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>M</a></th>
			<th><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>K</a></th>
			<th><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>KK</a></th>
			<th><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>G</a></th>
			<th><a class="th" href='{A_SORT_18}' title='{TIPS_SORT_18}'>O</a></th>
			<th><a class="th" href='{A_SORT_19}' title='{TIPS_SORT_19}'>RB</a></th>
			<th><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>T</a></th>
		 </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="left">{ROW.jurusan}</div></td>
			 <td ><div align="left">{ROW.kode_jurusan}</div></td>
			 <td ><div align="right">{ROW.total_keberangkatan}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_u}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_m}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_k}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_kk}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_g}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_o}</div></td>
			  <td width=60><div align="right" >{ROW.total_penumpang_rb}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang}</div></td>
			 <td ><div align="right">{ROW.rata_pnp_per_trip}</div></td>
			 <td ><div align="right">{ROW.total_omzet}</div></td>
			 <td ><div align="right">{ROW.total_paket}</div></td>
			 <td ><div align="right">{ROW.total_omzet_paket}</div></td>
			 <td ><div align="right">{ROW.total_discount}</div></td>
			 <td ><div align="right">{ROW.total_biaya}</div></td>
			 <td ><div align="right">{ROW.total_profit}</div></td>
       <td ><div align="center">{ROW.act}</div></td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
					<table>
						<tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
						<tr><td>M</td><td>=</td><td>Penumpang Mahasiswa/Pelajar/Anak-anak/Lansia/Group</td></tr>
						<tr><td>K</td><td>=</td><td>Penumpang Khusus/Pelanggan Setia</td></tr>
						<tr><td>KK</td><td>=</td><td>Penumpang Keluarga Karyawan</td></tr>
						<tr><td>G</td><td>=</td><td>Penumpang Gratis</td></tr>
						<tr><td>O</td><td>=</td><td>Penumpang Online Tiketux</td></tr>
						<tr><td>RB</td><td>=</td><td>Penumpang Online Red Bus</td></tr>
						<tr><td>T</td><td>=</td><td>Total Penumpang</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>