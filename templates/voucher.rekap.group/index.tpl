<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Rekap Voucher Diskon</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Periode&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tglawal" name="tglawal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
						&nbsp; s/d &nbsp;<input readonly="yes"  id="tglakhir" name="tglakhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
						Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" size=20 />&nbsp;<input type="submit" value="cari" />
					</form>
				</td>
			</tr>
		</table>
		<br>
		<table width='100%' class="border">
    <tr>
       <th width=30>No</th>
			 <th width=100>Nama Korporat</th>
			 <th width=50>Pembayaran</th>
			 <th width=100>Dicetak</th>
			 <th width=100>Dipakai</th>
			 <th width=100>Settlement</th>
			 <th width=70>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
			 <td align="center">{ROW.namagrup}</td>
       <td align="center">{ROW.pembayaran}</td>
			 <td align="right">{ROW.dicetak}</td>
			 <td align="right">{ROW.dipakai}</td>
			 <td align="right">Rp.{ROW.settlement}</td>
       <td align="center">{ROW.action}</td>
     </tr>  
     <!-- END ROW -->
    </table>
		<!-- BEGIN NO_DATA -->
		<div style="font-size: 18px;background: yellow;">Tidak ada data ditemukan</div>
    <!-- END NO_DATA -->
 </td>
</tr>
</table>