<script language="JavaScript">

var kode;

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('nama_invalid');
	Element.hide('hp_invalid');
	
	kode			= document.getElementById('kode_sopir');
	nama			= document.getElementById('nama');
	hp				= document.getElementById('hp');
		
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(!cekValue(hp.value)){	
		valid=false;
		Element.show('hp_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data_mobil" action="{U_ADD}" method="post" onSubmit='return validateInput();'>
<input type="hidden" name="id" value="{ID}">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Grup Voucher Diskon</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='800'>
				<table width='400'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <td><u>Nama Grup</u></td><td>:</td>
						<td>
							<input type="text" id="namagroup" name="namagroup" value="{NAMA_GRUP}" maxlength='50' onfocus="this.style.backgroud='white';">
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Deskripsi</td><td  valign='top'>:</td><td><textarea name="deskripsi" id="deskripsi" cols="30" rows="3"  maxlength=150>{DESKRIPSI}</textarea></td>
			    </tr>
					<tr>
						<td>Settlement</td><td>:</td>
						<td>
							<select id="issettlement" name="issettlement">
								<option value=1 {SETSEL_1}>YA</option>
								<option value=0 {SETSEL_0}>TIDAK</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Status</td><td>:</td>
						<td>
							<select id="isaktif" name="isaktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>