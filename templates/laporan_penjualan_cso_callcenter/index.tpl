<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
	}
	
	function init(e){
		setSortId();
	}
	
	dojo.addOnLoad(init);
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr bgcolor='505050' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Penjualan CSO Call Center</td>
				<td align='right' valign='middle' >
					<table>
						<tr><td class='bannernormal'>
							Cabang&nbsp;:&nbsp;
							<select name="cabang">
								<option value="" {SELECT_ALL_CABANG}>-semua cabang-</option>
								<!-- BEGIN CABANG -->
								<option value="{CABANG.value}" {CABANG.selected}>{CABANG.nama}</option>
								<!-- END CABANG -->
							</select>
							&nbsp;Periode:&nbsp;<input readonly="yes"  id="tglawal" name="tglawal" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
							&nbsp; s/d &nbsp;<input readonly="yes"  id="tglakhir" name="tglakhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
							&nbsp;Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" />&nbsp;	
							<input type="submit" value="cari" />&nbsp;								
						</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%'>
    <tr>
      <th width=30>No</th>
			<th width=200><a class="th" href='#' id="sort1">Nama</a></th>
			<th width=100><a class="th" href='#' id="sort2">NRP</a></th>
			<th width=100><a class="th" href='#' id="sort3">Call Center</a></th>
			<th width=70> <a class="th" href='#' id="sort4">Booking</a></th>
			<th width=70> <a class="th" href='#' id="sort5">Tiket</a></th>
			<th width=200><a class="th" href='#' id="sort6">Booking Terawal</a></th>
			<th width=200><a class="th" href='#' id="sort7">Booking Terakhir</a></th>
			<th width=100>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align='center' title='No.'>{ROW.no}</td>
       <td align='center' title='Nama'>{ROW.nama}</td>
			 <td align='center' title='NRP'>{ROW.nrp}</td>
       <td align='center' title='Cabang'>{ROW.cabang}</td>
			 <td align='right'  title='Booking'>{ROW.booking}</td>
			 <td align='right'	title='Tiket'>{ROW.tiket}</td>
			 <td align='center'	title='Booking Terawal'>{ROW.booking_terawal}</td>
			 <td align='center'	title='Booking Terakhir'>{ROW.booking_terakhir}</td>
       <td align='center'	title='Action'>{ROW.act}</td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='center'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>