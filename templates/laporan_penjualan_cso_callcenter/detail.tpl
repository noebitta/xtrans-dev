<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form action="{ACTION_CARI}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr bgcolor='505050' height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Detail Penjualan CSO: {NAMA_CSO} | Periode {PERIODE}</td>
								<td align='right' valign='middle' >
								</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
		</table>
		<table class="border">
    <tr>
       <th width=30>No</th>
			 <th width=200>Nama</th>
			 <th width=100>Telp</th>
			 <th width=100>No.Tiket</th>
			 <th width=150>Waktu Pesan</th>
			 <th width=150>Kode Jadwal</th>
			 <th width=150>Waktu Berangkat</th>
			 <th width=50>Kursi</th>
			 <th width=100>Harga Tiket</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
			 <td align="center">{ROW.nama}</td>
			 <td align="center">{ROW.telp}</td>
			 <td align="center">{ROW.no_tiket}</td>
			 <td align="center">{ROW.waktu_pesan}</td>
			 <td align="center">{ROW.kode_jadwal}</td>
			 <td align="center">{ROW.waktu_berangkat}</td>
			 <td align="center">{ROW.no_kursi}</td>
			 <td align="right">{ROW.harga_tiket}</td>
     </tr>  
     <!-- END ROW -->
    </table>
 </td>
</tr>
</table>