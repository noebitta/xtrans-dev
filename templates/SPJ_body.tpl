<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript" src="./browser/bowser.min.js"></script>
<script type="text/javascript">
	nama_browser	= bowser.name;
	
	if(nama_browser.toUpperCase()!='FIREFOX'){
		alert("Anda harus menggunakan browser Mozilla Firefox.");
		window.close();
	}
	
  function print_doc()
  {
		//set printer default
		var printers = jsPrintSetup.getPrintersList().split(',');
		jsPrintSetup.setPrinter(printers[0]);
		jsPrintSetup.setOption('orientation', jsPrintSetup.kPortraitOrientation);
		// set top margins in millimeters
		jsPrintSetup.setOption('marginTop', 5);
		jsPrintSetup.setOption('marginBottom', 5);
		jsPrintSetup.setOption('marginLeft', 5);
		jsPrintSetup.setOption('marginRight', 5);
		// set page header
		jsPrintSetup.setOption('headerStrLeft', '');
		jsPrintSetup.setOption('headerStrCenter', '');
		jsPrintSetup.setOption('headerStrRight', '');
		// set empty page footer
		jsPrintSetup.setOption('footerStrLeft', '');
		jsPrintSetup.setOption('footerStrCenter', '');
		jsPrintSetup.setOption('footerStrRight', '');
		// Suppress print dialog
		jsPrintSetup.setSilentPrint(true);/** Set silent printing */
		// Do Print
		//jsPrintSetup.print();
		jsPrintSetup.printWindow(window);
		// Restore print dialog
		//jsPrintSetup.setSilentPrint(false); /** Set silent printing back to false */
		window.close();
		
  }
	
 </script>	 

<body class='spj'>
<div class="tiket_body"> -- [MANIFEST] -- {DUPLIKAT}</div><br>
<div class="tiket_body">{NAMA_PERUSAHAAN}</div>
<div class="tiket_body">{ALAMAT_PERUSAHAAN}</div>
<div class="tiket_body">{TELP_PERUSAHAAN}</div><br>

<div class="tiket_body">{NO_SPJ}</div>
<div class="tiket_body">{TGL_BERANGKAT}</div>
<div class="tiket_body">{JURUSAN}</div><br>

<div class="tiket_body">Tgl.Cetak:</div>
<div class="tiket_body">{TGL_CETAK}</div><br>

<div class="tiket_body">Mobil:{NO_POLISI}</div>
<div class="tiket_body">Sopir:{SOPIR}</div><br>

<div class="tiket_body">DAFTAR PAKET {TIDAK_ADA_PAKET}</div>
<!-- BEGIN ROW_PAKET -->
<div class="tiket_body">({ROW_PAKET.IDX_PAKET_NO}) Resi:{ROW_PAKET.NO_TIKET_PAKET}</div>   
<div class="tiket_body">Tujuan:{ROW_PAKET.TUJUAN}</div>   
<div class="tiket_body">dr:{ROW_PAKET.NAMA_PENGIRIM}</div>   
<div class="tiket_body">{ROW_PAKET.TELP_PENGIRIM}</div><br>  
<div class="tiket_body">ke:{ROW_PAKET.NAMA_PENERIMA}</div>   
<div class="tiket_body">{ROW_PAKET.TELP_PENERIMA}</div><br> 
<!-- END ROW_PAKET -->

<div class="tiket_body">Jumlah Paket: {JUMLAH_PAKET}</div>   
<div class="tiket_body">Omzet Paket: Rp.{OMZET_PAKET}</div><br>
   
<div class="tiket_body">DAFTAR PENUMPANG {TIDAK_ADA_PENUMPANG}</div>   
<table width='300'>
	<!-- BEGIN ROW_PENUMPANG -->
	<tr>
		<td valign='top' width='30' class="tiket_body">{ROW_PENUMPANG.NOMOR_KURSI}</td>
		<td class="tiket_body">
			{ROW_PENUMPANG.NAMA}
			{ROW_PENUMPANG.NO_TIKET}<br>
			{ROW_PENUMPANG.TELP}<br>
			a:{ROW_PENUMPANG.ASAL}<br>
			t:{ROW_PENUMPANG.TUJUAN}<br><br>
		</td>
	</tr>
	<!-- END ROW_PENUMPANG -->
</table>

<div class="tiket_body">Jumlah Penumpang:{JUMLAH_PENUMPANG}</div>   
<div class="tiket_body">{JENIS_PENUMPANG}</div><br>

<div class="tiket_body">Ozt.Pnp:{OMZET_PENUMPANG}</div><br>
<div class="tiket_body">{LIST_OMZET_PENUMPANG}</div><br>
-------------------------------------
<div class="tiket_body">Tunai:{PENDAPATAN_TUNAI}</div><br>


<div class="tiket_body">CSO: {CSO}</div><br>


<div class="tiket_body"><font size='2'>&nbsp;&nbsp;&nbsp;&nbsp;{EMAIL_PERUSAHAAN}</font></div>
<div class="tiket_body"><font size='2'>&nbsp;&nbsp;&nbsp;&nbsp;{WEBSITE_PERUSAHAAN}</font></div>
<br>
<br>
-
<br>
	<div class="tiket_body">----------potong disini-----------</div><br>
<div class="tiket_body"> -- [TIKET RITASE SOPIR] --</div>
<div class="tiket_body">{NAMA_PERUSAHAAN}</div>
<div class="tiket_body">{NO_SPJ}</div>
<div class="tiket_body">{TGL_BERANGKAT}</div>
<div class="tiket_body">{JURUSAN}</div>
<div class="tiket_body">Tgl.Cetak:</div>
<div class="tiket_body">{TGL_CETAK}</div>
<div class="tiket_body">Mobil:{NO_POLISI}</div>
<div class="tiket_body">Sopir:{SOPIR}</div>
<div class="tiket_body">Jumlah Penumpang:{JUMLAH_PENUMPANG}</div>   
<script language="javascript">
	print_doc();
</script>
</body>
</html>