<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_omzet_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
          Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
					Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateTujuan("{ASAL}");

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Jadwal</td>
				<td colspan=2 align='right' valign='middle'>
					<br>
						<table>
							<tr><td class='bannernormal'>
								<table cellspacing=0 cellpadding=0 width='100%'>
									<tr>
										<td class='bannernormal'>Asal:&nbsp;</td><td><select name='asal' id='asal' onChange="getUpdateTujuan(this.value)">{OPT_ASAL}</select></td>
										<td class='bannernormal'>&nbsp;Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span></td>
										<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
										<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
										<td class='bannernormal' colspan=2>&nbsp;<input type="submit" value="cari" />&nbsp;</td>
									</tr>				
								</table>
							</td></tr>
						</table>
				</td>
			</tr>
			<tr>
				<td colspan=3>
					<table width='100%'>
						<tr>
							<td>
								<table>
									<tr>
										<td>
											<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
										</td><td bgcolor='D0D0D0'></td>
										<td>
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
							<td align='right'>
								<!--<table>
									<tr>
										<td align='right' class='border'>
											<a href="{U_GRAFIK}"><img src="{TPL}images/icon_grafik.png" /></a>
											<a href="{U_GRAFIK}">Lihat Grafik</a>
										</td>
									</tr>
								</table>-->
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table class="border" width='100%' >
    <tr>
       <th width=30 rowspan=2>Jam</th>
			 <th width=100 rowspan=2>Total Berangkat</th>
			 <th colspan=10>Total Penumpang</th>
			 <th width=100 rowspan=2>Pnp<br>/Trip</th>
			 <th width=200 rowspan=2>Total Omzet</th>
			 <th width=200 rowspan=2>Total Discount</th>
     </tr>
		 <tr>
			 <th>U</th><th>M</th><th>K</th><th>KK</th><th>G</th><th>O</th><th>RB</th><th>R</th><th>VR</th><th>V</th><th>T</th>
		 </tr>	
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="center"><font size=3 color='{ROW.font_color}'><b>{ROW.jam}</b></font></div></td>
			 <td ><div align="right">{ROW.total_keberangkatan}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_u}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_m}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_k}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_kk}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_g}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_t}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_rb}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_r}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_vr}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_v}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang}</div></td>
			 <td ><div align="right">{ROW.rata_pnp_per_trip}</div></td>
			 <td ><div align="right">{ROW.total_omzet}</div></td>
			 <td ><div align="right">{ROW.total_discount}</div></td>
     </tr>
     <!-- END ROW -->
		 <tr bgcolor='ffff00'>
       <td ><div align="center"><font size=3 color='{ROW.font_color}'><b>TOTAL</b></font></div></td>
			 <td ><div align="right"><b>{SUM_KEBERANGKATAN}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_U}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_M}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_K}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_KK}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_G}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_T}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_RB}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_R}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_VR}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG_V}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG}</b></div></td>
			 <td ><div align="right"><b>{RATA_PNP_PER_TRIP}</b></div></td>
			 <td ><div align="right"><b>{SUM_OMZET}</b></div></td>
			 <td ><div align="right"><b>{SUM_DISCOUNT}</b></div></td>
     </tr>
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
					<table>
						<tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
						<tr><td>M</td><td>=</td><td>Penumpang Mahasiswa/Pelajar/Anak-anak/Lansia/Group</td></tr>
						<tr><td>K</td><td>=</td><td>Penumpang Khusus/Pelanggan Setia</td></tr>
						<tr><td>KK</td><td>=</td><td>Penumpang Keluarga Karyawan</td></tr>
						<tr><td>G</td><td>=</td><td>Penumpang Gratis</td></tr>
						<tr><td>O</td><td>=</td><td>Penumpang Online</td></tr>
						<tr><td>RB</td><td>=</td><td>Penumpang Online Red Bus</td></tr>
						<tr><td>T</td><td>=</td><td>Total Penumpang</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>