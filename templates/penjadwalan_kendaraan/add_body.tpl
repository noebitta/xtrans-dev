<script language="JavaScript">

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_jadwal_invalid');
	
	kode_jadwal	= document.getElementById('kode_jadwal');
	no_pol			= document.getElementById('opt_mobil');
	sopir				= document.getElementById('opt_sopir1');
	
	if(!document.getElementById('opt_tujuan')){
		alert("Anda belum memilih tujuan");
		valid=false;
	}
	
	if(kode_jadwal.value==''){
		valid=false;
		Element.show('kode_jadwal_invalid');
	}
	
	if(no_pol.value==''){
		valid=false;
		Element.show('no_pol_invalid');
	}
	
	if(sopir.value==''){
		valid=false;
		Element.show('sopir_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","pengaturan_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&jurusan={ID_JURUSAN}&mode=get_tujuan",
        onLoading: function(request) 
        {
            Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
						Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateTujuan("{ASAL}");

</script>

<form name="frm_data_mobil" action="{U_JADWAL_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td colspan=3><h2>{JUDUL}</h2></td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40 bgcolor='D0D0D0'>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
		<tr>
			<td align='center' valign='top' width='800'>
				<table width='800'>   
					<tr>
			      <input type="hidden" name="kode_jadwal_old" value="{KODE_JADWAL_OLD}">
						<td width='200'><u>Kode Jadwal</u></td><td width='5'>:</td><td><input type="text" id="kode_jadwal" name="kode_jadwal" value="{KODE_JADWAL}" maxlength=50 onChange="Element.hide('kode_jadwal_invalid');"><span id='kode_jadwal_invalid' style='display:none;'><font color=red>&nbsp;tidak benar</font></span></td>
			    </tr>
					<tr>
			      <td>Asal</td><td>:</td>
						<td>
							<select id='opt_asal' name='opt_asal' onChange="getUpdateTujuan(this.value)">
								{OPT_ASAL}
							</select>
						</td>
			    </tr>
					<tr>
			      <td>Tujuan</td><td>:</td>
						<td>
							<div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Waktu berangkat</u></td><td>:</td>
						<td>
							<select id='opt_jam' name='opt_jam'>
								{OPT_JAM}
							</select>&nbsp;:&nbsp;
							<select id='opt_menit' name='opt_menit'>
								{OPT_MENIT}
							</select>
						</td>
			    </tr>
					<tr>
			      <td><u>Kendaraan</u></td><td>:</td>
						<td>
							<table width='100%'>
								<tr>
									<td width='10'>
										<select id='opt_mobil' name='opt_mobil' onChange="Element.hide('no_pol_invalid');">
										{OPT_MOBIL}
										</select>
									</td>
									<td>
										<span id='no_pol_invalid' style='display:none;'><font color=red>&nbsp;tidak benar</font></span>
									</td>
								</tr>
							</table>
						</td>
			    </tr>
					<tr>
			      <td><u>Sopir1</u></td><td>:</td><td>
							<table width='100%'>
								<tr>
									<td width=10>
										<select id='opt_sopir1' name='opt_sopir1' onChange="Element.hide('sopir_invalid');">
											{OPT_SOPIR1}
										</select>
									</td>
									<td>
										<span id='sopir_invalid' style='display:none;'><font color=red>&nbsp;tidak benar</font></span>
									</td>
								</tr>
							</table>
						</td>
			    </tr>
					<tr>
			      <td>Sopir2</td><td>:</td><td>
							<select id='opt_sopir2' name='opt_sopir2'>
								{OPT_SOPIR2}
							</select>
						</td>
			    </tr>
					<tr>
			      <td>Jum. Kursi</td><td>:</td>
						<td>
							<select id='opt_kursi' name='opt_kursi'>
								{OPT_KURSI}
							</select>
						</td>
			    </tr>
					<tr>
						<td>Status Aktif</td><td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
</form>