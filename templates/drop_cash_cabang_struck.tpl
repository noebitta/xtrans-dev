<html>
<head>
    <link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript" src="./browser/bowser.min.js"></script>
<script type="text/javascript">
    nama_browser	= bowser.name;

    if(nama_browser.toUpperCase()!='FIREFOX'){
        alert("Anda harus menggunakan browser Mozilla Firefox.");
        window.close();
    }

    function print_doc()
    {
        //set printer default
        var printers = jsPrintSetup.getPrintersList().split(',');
        jsPrintSetup.setPrinter(printers[0]);
        jsPrintSetup.setOption('orientation', jsPrintSetup.kPortraitOrientation);
        // set top margins in millimeters
        jsPrintSetup.setOption('marginTop', 5);
        jsPrintSetup.setOption('marginBottom', 5);
        jsPrintSetup.setOption('marginLeft', 5);
        jsPrintSetup.setOption('marginRight', 5);
        // set page header
        jsPrintSetup.setOption('headerStrLeft', '');
        jsPrintSetup.setOption('headerStrCenter', '');
        jsPrintSetup.setOption('headerStrRight', '');
        // set empty page footer
        jsPrintSetup.setOption('footerStrLeft', '');
        jsPrintSetup.setOption('footerStrCenter', '');
        jsPrintSetup.setOption('footerStrRight', '');
        // Suppress print dialog
        jsPrintSetup.setSilentPrint(false);/** Set silent printing */

        // Do Print

        //jsPrintSetup.print();
        jsPrintSetup.printWindow(window);
        // Restore print dialog
        jsPrintSetup.setSilentPrint(false); /** Set silent printing back to false */

    bV = parseInt(navigator.appVersion);
        if (bV >= 4) window.print();

        window.close();

    }

</script>

<body class='tiket'>

<div style="font-size:14px;width:225px;">
    <table class="container" cellpadding="0" >
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td colspan="2" align="center">EXTRANS</td>
        </tr>
        <tr>
            <td colspan="2" align="center">Drop Cash</td>
        </tr>
        <!-- ./header -->
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td valign="top">Tanggal Buat</td>
            <td align="right">{tgl}</td>
        </tr>

        <tr>
            <td valign="top">Pembuat</td>
            <td align="right">{nama}</td>
        </tr>

        <tr>
            <td>Cabang</td>
            <td align="right">{cabang}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td>Jenis Biaya </td>
            <td align="right">{jenis}</td>
        </tr>
        <tr>
            <td>Jumlah</td>
            <td align="right">Rp. {jumlah}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td align="right">{keterangan}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td>Tanggal Cetak</td>
            <td align="right">{tgl}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td colspan="2" align="center">Tanda Tangan Bukti</td>
        </tr>
        <tr>
            <td height="75" valign="top">Pembuat</td>
        </tr>
        <tr>
            <td>{nama}</td>
        </tr>
    </table>
</div>
</body>

<script language="javascript">
    print_doc();
</script>
</html>
