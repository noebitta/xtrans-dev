<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript" src="./browser/bowser.min.js"></script>
<script type="text/javascript">

	nama_browser	= bowser.name;
	
	if(nama_browser.toUpperCase()!='FIREFOX'){
		alert("Anda harus menggunakan browser Mozilla Firefox.");
		window.close();
	}
	
  function print_doc()
  {
		//set printer default
		//var printers = jsPrintSetup.getPrintersList().split(',');
		//jsPrintSetup.setPrinter(printers[0]);
		jsPrintSetup.setOption('orientation', jsPrintSetup.kPortraitOrientation);
		//jsPrintSetup.undefinePaperSize(44);
		//jsPrintSetup.definePaperSize(44,119, 'resi_paket','resi_paket215x140', 'Resi Paket', 215, 140,jsPrintSetup.kPaperSizeMillimeters);
		//jsPrintSetup.setPaperSizeData(43);
		// set top margins in millimeters
		jsPrintSetup.setOption('marginTop', 5);
		jsPrintSetup.setOption('marginBottom', 5);
		jsPrintSetup.setOption('marginLeft', 5);
		jsPrintSetup.setOption('marginRight', 5);
		// set page header
		jsPrintSetup.setOption('headerStrLeft', '');
		jsPrintSetup.setOption('headerStrCenter', '');
		jsPrintSetup.setOption('headerStrRight', '');
		// set empty page footer
		jsPrintSetup.setOption('footerStrLeft', '');
		jsPrintSetup.setOption('footerStrCenter', '');
		jsPrintSetup.setOption('footerStrRight', '');
		// Suppress print dialog
		jsPrintSetup.setSilentPrint(false);/** Set silent printing */
		// Do Print
		//jsPrintSetup.print();
		jsPrintSetup.printWindow(window);
		// Restore print dialog
		//jsPrintSetup.setSilentPrint(false); /** Set silent printing back to false */
		window.close();
		
  }
	
</script>	 

<body class='tiket'>
<div style="font-size:14px;width:750px;line-height:12px;">
	<div style="height:40px;"></div>
	<div style="float: left;font-size:20px;font-weight: bold;">{TUJUAN} [{LAYANAN}]</div><div style="float: right;font-size:16px;font-weight: bold;">{NO_RESI}</div><br>
	Tanggal:{TANGGAL}|Jurusan: {JURUSAN}|Jadwal Kirim: {JAM_BERANGKAT}<br>
	Jenis Barang:{JENIS_BARANG}|Koli: {KOLI}|Berat: {BERAT} KG<br>
	<br>
	<div style="width:750px;vertical-align: top;height: 80px;">
		<div style="position: absolute;left: 0px;width: 370px;">
			PENGIRIM<br>
			Nama:{NAMA_PENGIRIM}<br>
			Alamat:{ALAMAT_PENGIRIM}<br>
			Telp:{TELP_PENGIRIM}
		</div>
		<div style="position: absolute;left: 375px;width: 370px;">
			PENERIMA<br>
			Nama:{NAMA_PENERIMA}<br>
			Alamat:{ALAMAT_PENERIMA}<br>
			<font style="font-size: 16px;font-weight: bold;">Telp:{TELP_PENERIMA}</font>
		</div>
	</div>
	<div style="position: absolute;left: 0px;">Harga:</div><div style="position: absolute;left: 40px;width: 120px;text-align: right;">Rp. {HARGA}</div><div style="position: absolute;left: 170px;width: 400px;">Isi:{KET_PAKET}</div><div style="position: absolute;left: 405px;width: 345px;text-align: left;font-size: 12px;">Dengan menyetujui resi ini saya selaku pengirim
     telah membaca, memahami dan menyetujui
     seluruh syarat pengiriman yang tertera pada bagian
     belakang resi ini</div><br>
	<div style="position: absolute;left: 0px;">Diskon:</div><div style="position: absolute;left: 40px;width: 120px;text-align: right;">Rp. {DISKON}</div><div style="position: absolute;left: 170px;width: 400px;">Ins.Khusus:{INS_KHUSUS}</div><br>
	--------------------------<br>
	<div style="position: absolute;left: 0px;">Total:</div><div style="position: absolute;left: 40px;width: 120px;text-align: right;">Rp. {TOTAL}</div><br>
	Jenis Pembayaran:{JENIS_BAYAR} | Waktu Cetak:{WAKTU_CETAK}<br><br>
	<div style="position: absolute;left: 0px;width: 150px;text-align: center;">CSO Asal</div>
	<div style="position: absolute;left: 155px;width: 150px;text-align: center;">Pengirim</div>
	<div style="position: absolute;left: 310px;width: 150px;text-align: center;">CSO Tujuan</div>
	<div style="position: absolute;left: 460px;width: 150px;text-align: center;">Penerima</div>
	<div style="position: absolute;left: 620px;width: 150px;text-align: left;">KTP:<br>Telp:<br>Tgl:<br>Jam:</div>
	<div style="height:60px;text-align: center;"></div>
	<div style="position: absolute;left: 0px;width: 150px;text-align: center;">{PETUGAS_CETAK}</div>
	<div style="position: absolute;left: 155px;width: 150px;text-align: center;">{NAMA_PENGIRIM}</div>
	<div style="position: absolute;left: 310px;width: 150px;text-align: center;"></div>
	<div style="position: absolute;left: 460px;width: 150px	;text-align: left;"></div>
</div>
</body>

<script language="javascript">
	print_doc();
</script>
</html>