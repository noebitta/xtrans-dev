<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_omzet_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
          Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
					Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateTujuan("{ASAL}");

</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<form action="{ACTION_CARI}" method="post" name="my_form">
		<input type='hidden' id='is_cari' name='is_cari' value='1'>
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Paket Belum Diambil</td>
				<td colspan=2 align='right' valign='middle'>
					<br>
						<table>
							<tr><td class='bannernormal'>
								<table cellspacing=0 cellpadding=0 width='100%'>
									<tr>
										<td class='bannernormal'>Diterima di Cabang:&nbsp;</td><td><select name='tujuan' id='tujuan'>{OPT_TUJUAN}</select></td>
										<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tgl_awal" name="tgl_awal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
										<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tgl_akhir" name="tgl_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
										<td class='bannernormal' colspan=2>&nbsp;&nbsp;Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;</td>
									</tr>				
								</table>
							</td></tr>
						</table>
				</td>
			</tr>
			<tr><td align='center' colspan='3'><br><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></td></tr>
		</table>
		</form>
		<table >
		<tr>
    <tr>
			<td width='100%' align='right' colspan=16 valign='bottom'>{PAGING}</td>
		</tr>
		<tr>
       <th rowspan="2" width="30">No</th>
       <th rowspan="2" width="120">#resi</th>
       <th rowspan="2" width="100">#Jadwal</th>
			 <th rowspan="2" width="100">Waktu Berangkat</th>
			 <th colspan='3'>Pengirim</th>
			 <th colspan='3'>Penerima</th>
			 <th rowspan="2" width="50">Berat<br>(Kg)</th>
			 <th rowspan="2" width="70">Harga</th>
			 <th rowspan="2" width="70">Diskon</th>
			 <th rowspan="2" width="70">Bayar</th>
			 <th rowspan="2" width="70">Layanan</th>
			 <th rowspan="2" width="100">Jenis Bayar</th>
     </tr>
     <tr>
 			 <th width="100">Nama</th>
			 <th width="100">Alamat</th>
			 <th width="70">Telp</th>
			 <th width="100">Nama</th>
			 <th width="100">Alamat</th>
			 <th width="70">Telp</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="left">{ROW.no}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
			 <td><div align="left">{ROW.kode_jadwal}</div></td>
			 <td><div align="center">{ROW.waktu_berangkat}</div></td>
			 <td><div align="left">{ROW.nama_pengirim}</div></td>
			 <td><div align="left">{ROW.alamat_pengirim}</div></td>
			 <td><div align="left">{ROW.telp_pengirim}</div></td>
			 <td><div align="left">{ROW.nama_penerima}</div></td>
			 <td><div align="left">{ROW.alamat_penerima}</div></td>
			 <td><div align="left">{ROW.telp_penerima}</div></td>
			 <td><div align="right">{ROW.berat}</div></td>
			 <td><div align="right">{ROW.harga}</div></td>
			 <td><div align="right">{ROW.diskon}</div></td>
			 <td><div align="right">{ROW.bayar}</div></td>
			 <td><div align="center">{ROW.layanan}</div></td>
			 <td><div align="center">{ROW.jenis_bayar}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
    <table width='100%'>
			<tr>
				<td width='100%' align='right' colspan=13 valign='bottom'>{PAGING}</td>
			</tr>
			<tr>
				<td colspan=13 align='center'>
					<br>
					<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
				</td>
			</tr>
			<tr>
				<td align="left">
					<table>
						<tr><td colspan="3">LAYANAN:<td></tr>
						<tr><td>P</td><td>:</td><td>PLATINUM</td><tr>
						<tr><td>GD</td><td>:</td><td>GOLD DIANTAR</td><tr>
						<tr><td>GA</td><td>:</td><td>GOLD DIAMBIL</td><tr>
						<tr><td>S</td><td>:</td><td>SILVER</td><tr>
						<tr><td>CA</td><td>:</td><td>CARGO ANTAR</td><tr>
						<tr><td>CD</td><td>:</td><td>CARGO DIAMBIL</td><tr>
					</table>	
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>