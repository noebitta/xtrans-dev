<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<div class="wrapperheader" style="height:50px;">
  <div class="headertittle">&nbsp;Laporan Omzet Paket per Jurusan</div>
  <div class="headerfilter" style="padding-top: 7px;">
    <form action="{ACTION_CARI}" method="post">
      <input type='hidden' id='is_cari' name='is_cari' value='1'>
      Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">&nbsp;
      s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
      Cari:&nbsp;<input type="text" id="cari" name="cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
    </form>
  </div>
</div>
<br>
<div style="float: left;padding-bottom: 2px;">
  <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  {PAGING}
</div>
<div class="wrappertabledata">
  <table width="1490">
    <tr>
      <th width=30 rowspan=2 >No</th>
      <th width=200 rowspan=2><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jurusan</a></th>
      <th colspan=7>Total Paket (Qty)</th>
      <th colspan=7>Total Paket (Rp.)</th>
      <th colspan=3>Omzet</th>
      <th width=100 rowspan=2>Action</a></th>
    </tr>
    <tr>
      <th><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>P</a></th>
      <th><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>GD</a></th>
      <th><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>GA</a></th>
      <th><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>S</a></th>
      <th><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>CA</a></th>
      <th><a class="th" href='{A_SORT_18}' title='{TIPS_SORT_18}'>CD</a></th>
      <th><a class="th" href='{A_SORT_18}' title='{TIPS_SORT_19}'>I</a></th>
      <th><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>P</a></th>
      <th><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>GD</a></th>
      <th><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>GA</a></th>
      <th><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>S</a></th>
      <th><a class="th" href='{A_SORT_14}' title='{TIPS_SORT_14}'>CA</a></th>
      <th><a class="th" href='{A_SORT_19}' title='{TIPS_SORT_19}'>CD</a></th>
      <th><a class="th" href='{A_SORT_19}' title='{TIPS_SORT_20}'>I</a></th>
      <th><a class="th" href='{A_SORT_15}' title='{TIPS_SORT_15}'>Tunai</a></th>
      <th><a class="th" href='{A_SORT_16}' title='{TIPS_SORT_16}'>Langganan</a></th>
      <th><a class="th" href='{A_SORT_17}' title='{TIPS_SORT_17}'>Total</a></th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
      <td ><div align="right">{ROW.no}</div></td>
      <td ><div align="left">{ROW.jurusan}</div></td>
      <td width=60><div align="right" >{ROW.total_paket_p}</div></td>
      <td width=60><div align="right" >{ROW.total_paket_gd}</div></td>
      <td width=60><div align="right" >{ROW.total_paket_ga}</div></td>
      <td width=60><div align="right" >{ROW.total_paket_s}</div></td>
      <td width=60><div align="right" >{ROW.total_paket_ca}</div></td>
      <td width=60><div align="right" >{ROW.total_paket_cd}</div></td>
      <td width=60><div align="right" >{ROW.total_paket_i}<div></td>
      <td width=60><div align="right" >{ROW.omz_paket_p}</div></td>
      <td width=60><div align="right" >{ROW.omz_paket_gd}</div></td>
      <td width=60><div align="right" >{ROW.omz_paket_ga}</div></td>
      <td width=60><div align="right" >{ROW.omz_paket_s}</div></td>
      <td width=60><div align="right" >{ROW.omz_paket_ca}</div></td>
      <td width=60><div align="right" >{ROW.omz_paket_cd}</div></td>
      <td width=60><div align="right" >{ROW.omz_paket_i}</div></td>
      <td width="60"><div align="right">{ROW.omz_tunai}</div></td>
      <td width="60"><div align="right">{ROW.omz_langganan}</div></td>
      <td width="60"><div align="right">{ROW.omz_total}</div></td>
      <td ><div align="center">{ROW.act}</div></td>
    </tr>
    <!-- END ROW -->
  </table>
</div>
<div style="float: left;padding-bottom: 2px;">
  <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  {PAGING}
</div>