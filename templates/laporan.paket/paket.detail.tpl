<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form action="{ACTION_CARI}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr bgcolor='505050' height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Rekap Detail Paket {KETERANGAN}</td>
								<td align='right' valign='middle' >
									<table>
										<tr><td class='bannernormal'>
											&nbsp;Periode:&nbsp;{TGL_AWAL}&nbsp; s/d &nbsp;{TGL_AKHIR}
										</td></tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan=2 align='center' valign='middle'>
									<table>
										<tr>
											<td>
												<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
		</table>
		<table>
			<tr>
       <th rowspan="2" width="30">No</th>
       <th rowspan="2" width="120">#resi</th>
       <th rowspan="2" width="100">#Jadwal</th>
			 <th rowspan="2" width="100">Waktu Berangkat</th>
			 <th colspan='3'>Pengirim</th>
			 <th colspan='3'>Penerima</th>
			 <th rowspan="2" width="50">Berat<br>(Kg)</th>
			 <th rowspan="2" width="70">Harga</th>
			 <th rowspan="2" width="70">Diskon</th>
			 <th rowspan="2" width="70">Bayar</th>
			 <th rowspan="2" width="70">Layanan</th>
			 <th rowspan="2" width="100">Jenis Bayar</th>
     </tr>
     <tr>
 			 <th width="100">Nama</th>
			 <th width="100">Alamat</th>
			 <th width="70">Telp</th>
			 <th width="100">Nama</th>
			 <th width="100">Alamat</th>
			 <th width="70">Telp</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="left">{ROW.no}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
			 <td><div align="center">{ROW.kode_jadwal}</div></td>
			 <td><div align="center">{ROW.waktu_berangkat}</div></td>
			 <td><div align="left">{ROW.nama_pengirim}</div></td>
			 <td><div align="left">{ROW.alamat_pengirim}</div></td>
			 <td><div align="left">{ROW.telp_pengirim}</div></td>
			 <td><div align="left">{ROW.nama_penerima}</div></td>
			 <td><div align="left">{ROW.alamat_penerima}</div></td>
			 <td><div align="left">{ROW.telp_penerima}</div></td>
			 <td><div align="right">{ROW.berat}</div></td>
			 <td><div align="right">{ROW.harga}</div></td>
			 <td><div align="right">{ROW.diskon}</div></td>
			 <td><div align="right">{ROW.bayar}</div></td>
			 <td><div align="center">{ROW.layanan}</div></td>
			 <td><div align="center">{ROW.jenis_bayar}</div></td>
     </tr>  
     <!-- END ROW -->
		</table>
 </td>
</tr>
</table>