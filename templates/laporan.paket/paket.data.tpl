<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_omzet_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
          //Element.show('loading_tujuan');
        },
        onComplete: function(request)
        {
          //Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });

}

getUpdateTujuan("{ASAL}");

</script>
<div class="wrapperheader" style="height:50px;">
  <div class="headertittle">&nbsp;Laporan Data Paket</div>
  <div class="headerfilter" style="padding-top: 7px;">
    <form action="{ACTION_CARI}" method="post">
      <input type='hidden' id='is_cari' name='is_cari' value='1'>
      Asal:&nbsp;<select name='asal' id='asal' onChange="getUpdateTujuan(this.value)">{OPT_ASAL}</select>&nbsp;
      Tujuan:&nbsp;<span id='rewrite_tujuan'></span><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span>&nbsp;
      Tgl:&nbsp;<input readonly="yes"  id="tgl_awal" name="tgl_awal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">&nbsp;
      s/d &nbsp;<input readonly="yes"  id="tgl_akhir" name="tgl_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
      Cari:&nbsp;<input type="text" id="cari" name="cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
    </form>
  </div>
</div>
<br>
<div style="float: left;padding-bottom: 2px;">
  <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  {PAGING}
</div>
<div class="wrappertabledata">
  <table width="2130">
    <tr>
      <th rowspan="2" width="30">No</th>
      <th rowspan="2" width="200">Status</th>
      <th rowspan="2" width="120">#resi</th>
      <th rowspan="2" width="150">#Jadwal</th>
      <th rowspan="2" width="200">Waktu Berangkat</th>
      <th colspan='3'>Pengirim</th>
      <th colspan='3'>Penerima</th>
      <th rowspan="2" width="50">Berat<br>(Kg)</th>
      <th rowspan="2" width="70">Harga</th>
      <th rowspan="2" width="70">Diskon</th>
      <th rowspan="2" width="70">Bayar</th>
      <th rowspan="2" width="70">Layanan</th>
      <th rowspan="2" width="100">Jenis Bayar</th>
    </tr>
    <tr>
      <th width="150">Nama</th>
      <th width="250">Alamat</th>
      <th width="100">Telp</th>
      <th width="150">Nama</th>
      <th width="250">Alamat</th>
      <th width="100">Telp</th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}">
      <td align="center">{ROW.no}</td>
      <td style="{ROW.style_status}" align="center">{ROW.status}</td>
      <td align="center">{ROW.no_tiket}</td>
      <td align="center">{ROW.kode_jadwal}</td>
      <td align="center">{ROW.waktu_berangkat}</td>
      <td align="center">{ROW.nama_pengirim}</td>
      <td align="center">{ROW.alamat_pengirim}</td>
      <td align="center">{ROW.telp_pengirim}</td>
      <td align="center">{ROW.nama_penerima}</td>
      <td align="center">{ROW.alamat_penerima}</td>
      <td align="center">{ROW.telp_penerima}</td>
      <td align="right">{ROW.berat}</td>
      <td align="right">{ROW.harga}</td>
      <td align="right">{ROW.diskon}</td>
      <td align="right">{ROW.bayar}</td>
      <td align="center">{ROW.layanan}</td>
      <td align="center">{ROW.jenis_bayar}</td>
    </tr>
    <!-- END ROW -->
  </table>
</div>
<div style="float: left;padding-bottom: 2px;">
  <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
</div>
<div style="float: right;padding-bottom: 2px;">
  {PAGING}
</div>