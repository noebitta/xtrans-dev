<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<table width="100%" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='80%' class='border'>
	    <tr><td colspan=4 class="menuutama">&nbsp;Operasional</td></tr>
	    <tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
		    <td align='center' width='25%'>
			    <!-- MODUL -->
					<a href="{U_RESERVASI}"><img src="{TPL}images/icon_booking.png" />
					<br/>
					<span class="genmed">Reservasi</span> </a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_PENJADWALAN}"><img src="{TPL}images/icon_penjadwalan.png" /></a>  
					<br />
					<a href="{U_PENJADWALAN}"><span class="genmed">Penjadwalan<br>Kendaraan</span></a>
		    </td>
				
				<td align='center'>
					<a href="{U_PENGUMUMAN}"><img src="{TPL}images/icon_pengumuman.png" /></a>
					<br />
					<a href="{U_PENGUMUMAN}"><span class="genmed">Pengumuman</span></a>
		    </td>
				<td align='center' width='25%'>
					<a href="{U_MEMO}"><img src="{TPL}images/icon_memo.png" /></a>
					<br />
					<a href="{U_MEMO}"><span class="genmed">Memo</span></a>
		    </td>
			</tr>
			</tr>
			<tr colspan=4><td><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_BATAL}"><img src="{TPL}images/icon_batal.png" /></a>  
					<br />
					<a href="{U_BATAL}"><span class="genmed">Pembatalan</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LOG_BATAL}"><img src="{TPL}images/icon_laporan_tiket_batal.png" /></a>  
					<br />
					<a href="{U_LOG_BATAL}"><span class="genmed">Laporan<br>Pembatalan</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_UBAHPASS}"><img src="{TPL}images/icon_menu_password.png" /></a>
					<br />
					<a href="{U_UBAHPASS}"><span class="genmed">Ubah Password</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_DAFTAR_MANIFEST}"><img src="{TPL}images/icon_laporan_kendaraan.png" /></a>
					<br />
					<a href="{U_DAFTAR_MANIFEST}"><span class="genmed">Daftar Manifest</span></a>
		    </td>

			</tr>
      <tr colspan=4><td><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
          <a href="{U_DAFTAR_PELANGGAN}"><img src="{TPL}images/icon_user_online.png" /></a>
          <br />
          <a href="{U_DAFTAR_PELANGGAN}"><span class="genmed">Daftar Pelanggan</span></a>
		    </td>

				<td align='center' width='25%'>
					&nbsp;
		    </td>

				<td align='center' width='25%'>
					&nbsp;
		    </td>

				<td align='center' width='25%'>
					&nbsp;
		    </td>

			</tr>
		</table>
		<br><br>
		<h3 id='laporan_callcenter'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Call Center</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_CSO_CALLCENTER}"><img src="{TPL}images/icon_lap_callcenter.png" /></a>
					<br />
					<a href="{U_LAPORAN_CSO_CALLCENTER}"><span class="genmed">CSO Call Center</span></a>
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>

		    <td align='center' width='25%'>
					&nbsp;
		    </td>
			
			</tr>
		</table>
		<br><br>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Laporan Omzet Penumpang</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_CABANG}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_CABANG}"><span class="genmed">Per Cabang</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_JURUSAN}"><img src="{TPL}images/icon_laporan_jurusan.png" /></a>
					<br />
					<a href="{U_LAPORAN_JURUSAN}"><span class="genmed">Per Jurusan</span></a>
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>

		    <td align='center' width='25%'>
					&nbsp;
		    </td>
			</tr>
		</table>
		<br><br>
		<h3 id='laporan_keuangan'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Laporan Setoran</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_SOPIR}"><img src="{TPL}images/icon_laporan_sopir.png" /></a>  
					<br />
					<a href="{U_LAPORAN_SOPIR}"><span class="genmed">Laporan Insentif<br>Sopir</span></a>
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
			</tr>
		</table>
		<h3 id='laporan_paket'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Laporan Paket</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_DATA_PAKET}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_DATA_PAKET}"><span class="genmed">Laporan Data Paket</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_PAKET_BELUM_DIAMBIL}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_PAKET_BELUM_DIAMBIL}"><span class="genmed">Laporan Paket<br>Belum Diambil</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_OMZET_PAKET_CABANG}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_LAPORAN_OMZET_PAKET_CABANG}"><span class="genmed">Laporan Omzet Paket<br>Per Cabang</span></a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_OMZET_PAKET_JURUSAN}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_LAPORAN_OMZET_PAKET_JURUSAN}"><span class="genmed">Laporan Omzet Paket<br>Per Jurusan</span></a>
		    </td>
			</tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_PAKET_PELANGGAN}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_PAKET_PELANGGAN}"><span class="genmed">Laporan Paket<br>Pelanggan</span></a>
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
			</tr>
		</table>
		<br><br>
		<h3 id='tiketux'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Tiketux</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_TIKETUX}"><img src="{TPL}images/icon_laporan_tiketux.png" /></a>  
					<br />
					<a href="{U_LAPORAN_TIKETUX}"><span class="genmed">Penjualan Tiketux</span></a>
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
				
				<td align='center' width='25%'>
					&nbsp;
		    </td>
			</tr>
		</table>
		<br><br>
		<h3 id='master_data'></h3>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Master Data</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>  
					<a href="{U_JADWAL}"><img src="{TPL}images/icon_master_jadwal.png" /></a>  
					<br />
					<a href="{U_JADWAL}"><span class="genmed">Jadwal</span></a>
		    </td>
				<td align='center' width='25%'>  
					<a href="{U_SOPIR}"><img src="{TPL}images/icon_master_sopir.png" /></a>  
					<br />
					<a href="{U_SOPIR}"><span class="genmed">Sopir</span></a>
		    </td>
				<td align='center' width='25%'>  
					<a href="{U_MOBIL}"><img src="{TPL}images/icon_master_mobil.png" /></a>  
					<br />
					<a href="{U_MOBIL}"><span class="genmed">Mobil</span></a>
		    </td>
				<td align='center' width='25%'>
					&nbsp;
		    </td>
			</tr>
		</table>
	
 </td>
</tr>
</table>