<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

var id_member;

function setOrder(sortby){
	ascending	= document.getElementById('hdn_asc').value;
	tampilkanData(sortby);
	document.getElementById('hdn_asc').value = 1-ascending;
}

function tampilkanData(sortby){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
 
    ascending	= document.getElementById('hdn_asc').value;
		
		new Ajax.Updater("rewrite_hasil","member_laporan_kartu_belum_aktif.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=tampilkan&sortby="+sortby+"&ascending="+ascending,
        onLoading: function(request) 
        {
            Element.show('progress');
						//Element.hide('rewritejam');
        },
        onComplete: function(request) 
        {
						Element.hide('progress');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function tanyaHapus(id){
	id_member=id;
	dlg_TanyaHapus.show();
}

function hapusData(){
    
  new Ajax.Request("member_laporan_kartu_belum_aktif.php?sid={SID}", 
  {
    asynchronous: true,
    method: "get",
    parameters: "id_member="+id_member+"&mode=hapus",
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
			if(request.responseText=="berhasil"){
				alert("Data berhasil dihapus!");
				tampilkanData('');
			}
			else{
				alert("Data GAGAL dihapus!");
			}
		},
    onSuccess: function(request) 
    {
		},
    onFailure: function(request) 
    {
    }
  })      
}

function init(e) {
  // inisialisasi variabel
	//dialog tanya hapus__________________________________________________________________
	dlg_TanyaHapusBtnYes = document.getElementById("dlg_TanyaHapusBtnYes");
	dlg_TanyaHapusBtnNo = document.getElementById("dlg_TanyaHapusBtnNo");
	dlg_TanyaHapus = dojo.widget.byId("dlg_TanyaHapus");
	dlg_TanyaHapus.setCloseControl(dlg_TanyaHapusBtnYes);
  dlg_TanyaHapus.setCloseControl(dlg_TanyaHapusBtnNo);
	
	tampilkanData('');
}

dojo.addOnLoad(init);



</script>

<div dojoType="dialog" width="400" id="dlg_TanyaHapus" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="100" style="display: none;">
<form onsubmit="return false;">
<font color='FFFFFF'><h3>Konfirmasi Hapus</h3></font>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center">
		<br><br>
		Apakah anda yakin akan menghapus data ini?<br><br><br>
	</td>
<tr>
	<td align="center">
		<br>
		<input type="button" onclick="hapusData();" id="dlg_TanyaHapusBtnYes" value="&nbsp&nbsp;&nbsp;Ya&nbsp;&nbsp;&nbsp">
		<input type="button" id="dlg_TanyaHapusBtnNo" value="&nbsp;Tidak&nbsp;">
	</td>
</tr>
</table>
</form>
</div>

<input type='hidden' id='hdn_asc' value='0' >

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='100%'>
    <tr>
    <td valign="middle" align="center">
		<h1>DAFTAR KARTU YANG BELUM AKTIF</h1>
    <table width='100%'>
    <tr>
    </tr>
    </table>
    <div id='rewrite_hasil'></div>
		<span id='progress' style='display:none;'><img src='./images/progress.gif' /> Sedang memproses...</span><br>
	{PESAN}
 </td>
</tr>
</table>
