<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValTahun(tahun){
	cek_value=tahun*0;
	
	if(cek_value!=0){
		return false;
	}
	
	if((tahun*1)<1920 || (tahun*1>2050)){
		return false;
	}
	else{
		return true;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_kendaraan_invalid');
	Element.hide('no_polisi_invalid');
	Element.hide('km_invalid');
	Element.hide('tahun_pembuatan_invalid');
	
	kode_kendaraan		= document.getElementById('kode_kendaraan');
	no_polisi					= document.getElementById('no_polisi');
	no_stnk						= document.getElementById('no_stnk');
	km								= document.getElementById('km');
	tahun_pembuatan		= document.getElementById('tahun_pembuatan');
	
	if(kode_kendaraan.value==''){
		valid=false;
		Element.show('kode_kendaraan_invalid');
	}
	
	if(no_polisi.value==''){
		valid=false;
		Element.show('no_polisi_invalid');
	}
	
	if(!cekValTahun(tahun_pembuatan.value)){	
		valid=false;
		Element.show('tahun_pembuatan_invalid');
	}
	
	cek_value=km.value*0;
	
	if(cek_value!=0){
		valid=false;
		Element.show('km_invalid');
	}
	
	if(km.value==''){
		km.value=0;
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data_mobil" action="{U_MOBIL_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Mobil</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='400'>
				<table width='400'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr><td colspan=3><h3><u>Data Umum Kendaraan</u></h3></td></tr>
					<tr>
			      <input type="hidden" name="kode_kendaraan_old" value="{KODE_KENDARAAN_OLD}">
						<td width='200'><u>Kode Body</u></td><td width='5'>:</td><td><input type="text" id="kode_kendaraan" name="kode_kendaraan" value="{KODE_KENDARAAN}" maxlength=15 onChange="Element.hide('kode_kendaraan_invalid');"><span id='kode_kendaraan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    </tr>
					<tr>
			      <input type="hidden" name="no_polisi_old" value="{NO_POLISI_OLD}">
						<td width='200'><u>Nomor Polisi</u></td><td width='5'>:</td><td><input type="text" id="no_polisi" name="no_polisi" value="{NO_POLISI}" maxlength=15 onChange="Element.hide('no_polisi_invalid');"><span id='no_polisi_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    </tr>
					<tr>
			      <td>Jenis</td><td>:</td><td><input type="text" name="jenis" value="{JENIS}" maxlength=50></td>
			    </tr>
					<tr>
			      <td>Merek</td><td>:</td><td><input type="text" name="merek" value="{MEREK}" maxlength=50></td>
			    </tr>
					<tr>
			      <td><u>Tahun pembuatan</u></td><td>:</td><td><input type="text" id="tahun_pembuatan" name="tahun_pembuatan" value="{TAHUN_PEMBUATAN}" maxlength=4 onChange="Element.hide('tahun_pembuatan_invalid');"><span id='tahun_pembuatan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    </tr>
					<tr>
			      <td>Warna</td><td>:</td><td><input type="text" name="warna" value="{WARNA}" maxlength=50></td>
			    </tr>
					<tr>
			      <td><u>Jum. Kursi</u></td><td>:</td><td><select id="jumlah_kursi" name="jumlah_kursi">{KURSI}</select></td>
			    </tr>
					<tr>
			      <td><u>Sopir 1</u></td><td>:</td><td><select id='sopir1' name='sopir1'>{SOPIR1}</select></td>
			    </tr>
					<tr>
			      <td>Sopir 2</td><td>:</td><td><select id='sopir2' name='sopir2'>{SOPIR2}</select></td>
			    </tr>
					
					<tr><td colspan=3><h3><u>Data Operasional</u></h3></td></tr>
					<tr>
			      <td>KM terakhir</td><td>:</td><td><input type="text" id="km" name="km" value="{KM}" maxlength=10 onChange="Element.hide('km_invalid');"/><span id='km_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    </tr>
					<tr>
						<td>Kepemilikan Cabang</td><td>:</td>
						<td>
							<select id='cabang' name='cabang'>
								{OPT_CABANG}
							</select>
						</td>
					</tr>
					<tr>
			      <td>Status Aktif</td><td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
			    </tr>
					
					<tr><td colspan=3 height=40 valign='bottom'><h3><u>Data Fisik Kendaraan</u></h3></td></tr>
					<tr>
			      <td>Tgl.Serah Terima</td><td>:</td>
						<td><input id="tglserahterima" name="tglserahterima" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_SERAH_TERIMA}"></td>
			    </tr>
					<tr>
			      <td>No Rangka</td><td>:</td><td><input type="text" name="no_rangka" value="{NO_RANGKA}" maxlength=50></td>
			    </tr>
					<tr>
			      <td>No Mesin</td><td>:</td><td><input type="text" id="no_mesin" name="no_mesin" value="{NO_MESIN}" maxlength=50></td>
			    </tr>
					<tr>
			      <td>ATPM</td><td>:</td><td><input type="text" id="atpm" name="atpm" value="{ATPM}" maxlength=100></td>
			    </tr>
					
					<tr><td colspan=3 height=40 valign='bottom'><h3><u>Data STNK</u></h3></td></tr>
					<tr>
			      <td>No STNK</td><td>:</td><td><input type="text" id='no_stnk' name="no_stnk" value="{NO_STNK}" maxlength=50 ></td>
			    </tr>
					<tr>
			      <td>Berlaku Hingga</td><td>:</td>
						<td><input id="stnkberlakuhingga" name="stnkberlakuhingga" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{STNK_BERLAKU_HINGGA}"></td>
			    </tr>
					<tr>
			      <td>Pajak Berlaku</td><td>:</td>
						<td><input id="pajakberlakuhingga" name="pajakberlakuhingga" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{PAJAK_BERLAKU_HINGGA}"></td>
			    </tr>
				</table>
			</td>
			<td width=1 bgcolor='D0D0D0'></td>
			<td align='center' valign='top'>
				<table width='400'>   
					<tr><td colspan=3 height=40 valign='bottom'><h3><u>Data KIR</u></h3></td></tr>
					<tr>
			      <td>No KIR</td><td>:</td><td><input type="text" id='nokir' name="nokir" value="{NO_KIR}" maxlength=50 ></td>
			    </tr>
					<tr>
			      <td>KIR Berlaku Hingga</td><td>:</td>
						<td><input id="kirberlakuhingga" name="kirberlakuhingga" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{KIR_BERLAKU_HINGGA}"></td>
			    </tr>
					
					<tr><td colspan=3 height=40 valign='bottom'><h3><u>Data Leasing</u></h3></td></tr>
					<tr>
			      <td>No Kontrak</td><td>:</td><td><input type="text" id='nokontrak' name="nokontrak" value="{NO_KONTRAK}" maxlength=50 ></td>
			    </tr>
					<tr>
			      <td>Perusahaan Leasing</td><td>:</td><td><input type="text" id='perusahaanleasing' name="perusahaanleasing" value="{PERUSAHAAN_LEASING}" maxlength=100 ></td>
			    </tr>
					<tr>
			      <td>Harga OTR</td><td>:</td><td><input type="text" id='hargaotr' name="hargaotr" value="{HARGA_OTR}" maxlength=12 onkeypress='validasiAngka(event);' ></td>
			    </tr>
					<tr>
			      <td>Angsuran/Bulan (Rp.)</td><td>:</td><td><input type="text" id='angsuranperbulan' name="angsuranperbulan" value="{ANGSURAN_PERBULAN}" maxlength=12 onkeypress='validasiAngka(event);' ></td>
			    </tr>
					<tr>
			      <td>Awal Angsuran</td><td>:</td><td><input type="text" id='awalangsuran' name="awalangsuran" value="{AWAL_ANGSURAN}" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" ></td>
			    </tr>
					<tr>
			      <td>Tgl. Jatuh Tempo</td><td>:</td>
						<td><input id="tgljatuhtempo" name="tgljatuhtempo" type="text" maxlength=2 size=4 onkeypress='validasiAngka(event);'  value="{TGL_JATUH_TEMPO}"></td>
			    </tr>
					
					<tr><td colspan=3 height=40 valign='bottom'><h3><u>Data Asuransi</u></h3></td></tr>
					<tr>
			      <td>No Polis</td><td>:</td><td><input type="text" id='nopolis' name="nopolis" value="{NO_POLIS}" maxlength=50></td>
			    </tr>
					<tr>
			      <td>Perusahaan Asuransi</td><td>:</td><td><input type="text" id="perusahaanasuransi" name="perusahaanasuransi" value="{PERUSAHAAN_ASURANSI}" maxlength=100></td>
			    </tr>
					<tr>
			      <td>TJH</td><td>:</td><td><input type="text" id='tjh' name="tjh" value="{TJH}" maxlength=50></td>
			    </tr>
					<tr>
			      <td>Asuransi Berlaku Hingga</td><td>:</td>
						<td><input id="asuransiberlakuhingga" name="asuransiberlakuhingga" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{ASURANSI_BERLAKU_HINGGA}"></td>
			    </tr>
					
					<tr><td colspan=3 height=40 valign='bottom'><h3><u>Data BPKB</u></h3></td></tr>
					<tr>
			      <td>No BPKB</td><td>:</td><td><input type="text" name="no_bpkb" value="{NO_BPKB}" maxlength=50></td>
			    </tr>
					<tr>
			      <td>Tgl. Diterima</td><td>:</td>
						<td><input id="tglbpkbditerima" name="tglbpkbditerima" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BPKB_DITERIMA}"></td>
			    </tr>
					<tr>
			      <td>Tgl. Dilepas</td><td>:</td>
						<td><input id="tglbpkbdilepas" name="tglbpkbdilepas" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BPKB_DILEPAS}"></td>
			    </tr>
					<tr>
			      <td>No Pol. Hitam</td><td>:</td><td><input type="text" id="nopolhitam" name="nopolhitam" value="{NO_POL_HITAM}" maxlength=20></td>
			    </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>