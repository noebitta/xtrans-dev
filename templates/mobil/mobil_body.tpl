<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
			
		new Ajax.Request("pengaturan_mobil.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_mobil="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_mobil.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&kode_kendaraan="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Mobil</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
					</form>
				</td>
			</tr>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_MOBIL_ADD}">[+]Tambah Mobil</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Mobil</a>&nbsp;|&nbsp;
					<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
		<table width="2800" class="border">
		<tr>
			<th width=30 rowspan='2'></th>
			<th width=30 rowspan='2'>No</th>
			<th colspan='7'>Fisik</th>
			<th colspan='3'>STNK</th>
			<th colspan='2'>KIR</th>
			<th colspan='6'>Leasing</th>
			<th colspan='4'>Asuransi</th>
			<th colspan='3'>BPKB</th>
			<th width=100 rowspan='2'>Aktif</th>
			<th width=100 rowspan='2'>Action</th>
		</tr>
    <tr>
			 <th width=100>Tgl.Serah Terima</th>
			 <th width=100>#Rangka</th>
			 <th width=100>#Mesin</th>
			 <th width=100>#Body</th>
			 <th width=300>Merek & Jenis</th>
			 <th width=70>Kursi</th>
			 <th width=100>ATPM</th>
			 <th width=100>#Plat</th>
			 <th width=100>STNK Expired</th>
			 <th width=100>Pajak Expired</th>
			 <th width=100>#KIR</th>
			 <th width=100>KIR Expired</th>
			 <th width=100>#Kontrak</th>
			 <th width=100>Pers.Leasing</th>
			 <th width=100>Harga OTR</th>
			 <th width=100>Angsuran/bln</th>
			 <th width=100>Awal Angsuran</th>
			 <th width=100>Tgl.Jatuh Tempo</th>
			 <th width=100>#Polis</th>
			 <th width=100>Perus.Asuransi</th>
			 <th width=100>TJH</th>
			 <th width=100>Asuransi Expired</th>
			 <th width=100>Tgl.Diterima</th>
			 <th width=100>Tgl.Dilepas</th>
			 <th width=100>#Plat Hitam</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
				<td><div align="center">{ROW.check}</div></td>
				<td><div align="right">{ROW.no}</div></td>
				<td><div align="center">{ROW.tglserahterima}</div></td>
				<td><div align="center">{ROW.norangka}</div></td>
				<td><div align="center">{ROW.nomesin}</div></td>
				<td><div align="center">{ROW.kode}</div></td>
				<td><div align="center">{ROW.merek}</div></td>
				<td><div align="center">{ROW.kapasitas}</div></td>
				<td><div align="center">{ROW.atpm}</div></td>
				<td><div align="center">{ROW.plat}</div></td>
				<td><div align="center">{ROW.stnkexpired}</div></td>
				<td><div align="center">{ROW.pajakexpired}</div></td>
				<td><div align="center">{ROW.nokir}</div></td>
				<td><div align="center">{ROW.kirexpired}</div></td>
				<td><div align="center">{ROW.nokontrak}</div></td>
				<td><div align="center">{ROW.perusahaanleasing}</div></td>
				<td><div align="right">{ROW.hargaotr}</div></td>
				<td><div align="right">{ROW.angsuranperbulan}</div></td>
				<td><div align="center">{ROW.awalangsuran}</div></td>
				<td><div align="center">{ROW.tgljatuhtempo}</div></td>
				<td><div align="center">{ROW.nopolis}</div></td>
				<td><div align="center">{ROW.perusahaanasuransi}</div></td>
				<td><div align="center">{ROW.tjh}</div></td>
				<td><div align="center">{ROW.masaberlakuasuransi}</div></td>
				<td><div align="center">{ROW.tglbpkbditerima}</div></td>
				<td><div align="center">{ROW.tglbpkbdilepas}</div></td>
				<td><div align="center">{ROW.nopolhitam}</div></td>
				<td><div align="center">{ROW.aktif}</div></td>
				<td><div align="center">{ROW.action}</div></td>
     </tr>  
     <!-- END ROW -->
		 {NO_DATA}
    </table>
    <table width='100%'>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_MOBIL_ADD}">[+]Tambah Mobil</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Mobil</a>&nbsp;|&nbsp;
					<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>