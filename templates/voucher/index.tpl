<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script type="text/javascript">

	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function showDialogBuatVoucher(){
		nilaivoucher.value="";
		keterangan.value="";
		jumlahcetak.value="";
		dlg_buat_voucher.show();
	}
	
	function getUpdateTujuan(asal){
		// fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		document.getElementById('rewrite_list_jurusan').innerHTML = "";
		
		new Ajax.Updater("rewrite_list_jurusan","laporan_voucher.php?sid={SID}", 
		{
			asynchronous: true,
			method: "get",
			parameters: "mode=gettujuan&asal="+asal,
			onLoading: function(request) 
			{
					Element.show('loading_tujuan');
			},
			onComplete: function(request) 
			{
					Element.hide('loading_tujuan');
			},
			onFailure: function(request) 
			{ 
				assignError(request.responseText); 
			}
		});   
	}
	
	function buatVoucher(){
		
		is_valid=true;
		
		if(nilaivoucher.value==""){
			nilaivoucher.style.background = 'red';
			is_valid = false;
		}
		
		if(keterangan.value==""){
			keterangan.style.background = 'red';
			is_valid = false;
		}
		
		if(jumlahcetak.value==""){
			jumlahcetak.style.background = 'red';
			is_valid = false;
		}
		
		if(!is_valid){
			exit;
		}
		
		new Ajax.Request("laporan_voucher.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters: "mode=buatvoucher"+
				"&jurusan="+jurusan.value+
				"&expireddate="+expireddate.value+
				"&ishargatetap="+ishargatetap.value+
				"&nilaivoucher="+nilaivoucher.value+
				"&isbolehweekend="+isbolehweekend.value+
				"&keterangan="+keterangan.value+
				"&jumlahcetak="+jumlahcetak.value,
			onLoading: function(request){
				progressbar.show();
			},
			onComplete: function(request){
				progressbar.hide();
				dlg_buat_voucher.hide();
			},
			onSuccess: function(request){
				eval(request.responseText);
			},
			onFailure: function(request){
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		});
	
	}
	
	function init(e){
		//control dialog paket
		dlg_buat_voucher						= dojo.widget.byId("dialogbuatvoucher");
		dlg_buat_voucher_btn_cancel = document.getElementById("dialogbuatvouchercancel");
		
		getUpdateTujuan(null);
	}
	
	dojo.addOnLoad(init);
	
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>

<!--dialog Create Voucher-->
<div dojoType="dialog" id="dialogbuatvoucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width="400">
<tr>
	<td bgcolor='ffffff' align='center'>
		<table>
			<tr><td colspan='2'><h2>Buat Voucher</h2></td></tr>
			<tr><td align='right'>Digunakan dari :</td><td><select id='cabangasal' name='cabangasal' onChange="getUpdateTujuan(this.value);">{OPT_ASAL}</select></td></tr>
			<tr><td align='right'>Digunakan ke :</td><td><div id="rewrite_list_jurusan"></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span></td></tr>
			<tr><td align='right'>Tgl.Expired:</td><td><input id="expireddate" name="expireddate" type="text" readonly='yes' size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_EXPIRED}"></td></tr>
			<tr><td align='right'><select id='ishargatetap'><option value='0'>Diskon</option><option value='1'>Harga</option></select></td><td>Rp.<input id="nilaivoucher" name="nilaivoucher" type="text" onkeypress='validasiAngka(event);' onfocus="this.style.background='white'"></td></tr>
			<tr><td align='right'>Waktu Pemakaian:</td><td><select id='isbolehweekend'><option value='0'>Hanya Week Day</option><option value='1'>Semua Hari</option></select></td></tr>
			<tr><td align='right'>Keterangan:</td><td><input id="keterangan" name="keterangan" type="text" onfocus="this.style.background='white'"/></td></tr>
			<tr><td align='right'>Jumlah Cetak:</td><td><input id="jumlahcetak" name="jumlahcetak" type="text" maxlength='4' onkeypress='validasiAngka(event);' onfocus="this.style.background='white'"/></td></tr>
		</table>
		<span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialogbuatvouchercancel" value="&nbsp;Cancel&nbsp;" onClick="dlg_buat_voucher.hide();"> 
		<input type="button" onclick="buatVoucher();" id="dialogbuatvoucherproses" value="&nbsp;Proses&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog create voucher-->

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40><td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Voucher</td></tr>
			<tr class='banner' height=40>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>Kota: <select id='kota' name='kota'><option value=''>-semua-</option>{OPT_KOTA}</select></td>
							<td class='bannernormal'>Status:&nbsp;</td><td><select name='status' id='status' ><option value="" {STATUS}>-semua-</option><option value="1" {STATUS1}>TERPAKAI</option><option value="0" {STATUS0}>BELUM TERPAKAI</option><option value="2" {STATUS2}>EXPIRED</option></select></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<select name='istgldigunakan' id='istgldigunakan' ><option value="0" {ISTGLDIGUNAKAN0}>Dibuat</option><option value="1" {ISTGLDIGUNAKAN1}>Digunakan</option></select>&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<!--<td>
								<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
							</td><td bgcolor='D0D0D0'></td>-->
							<td class='border'>&nbsp;<a href='#' onClick="showDialogBuatVoucher();">Buat Voucher</a>&nbsp;</td>
							<td class='border'>&nbsp;<img src="{TPL}/images/icon_msexcel.png"><a href='#' onClick="{CETAK_XL}">Cetak ke MS EXCEL</a>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td>
								<table class="border">
									<tr>
										<td><b>Voucher Belum Digunakan</b></td><td width="1">:</td><td align="right">{TOTAL_BELUM_DIGUNAKAN} Voucher</td>
									</tr>
									<tr>
										<td><b>Voucher Digunakan</b></td><td width="1">:</td><td align="right">{TOTAL_DIGUNAKAN} Voucher</td>
									</tr>
									<tr>
										<td><b>Voucher Expired</b></td><td>:</td><td align="right">{TOTAL_EXPIRED} Voucher</td>
									</tr>
									<tr><td height=1 bgcolor=red colspan=5></td></tr>
									<tr>
										<td><b>Jumlah Voucher</b></td><td width="1">:</td><td align="right">{TOTAL_VOUCHER} Voucher</td>
									</tr>
								</table>
							</td>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
			 <th width=30	>No</th>
			 <th width=100><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'	>#Voucher</a></th>
			 <th width=150><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'	>Dibuat</a></th>
			 <th width=150>Dibuat Oleh</th>
			 <th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'	>Digunakan</a></th>
			 <th width=150><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'	>#Tiket</a></th>
			 <th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'  >Jadwal</a></th>
			 <th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'  >Penumpang</a></th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'  >Telp</a></th>
			 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'	>Nilai</a></th>
			 <th width=100>CSO</th>
			 <th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Expired</a></th>
			 <th width=200>Keterangan</th>
			 <th width=100><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>Status</a></th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.kode_voucher}</div></td>
			 <td><div align="center">{ROW.waktu_buat}</div></td>
			 <td><div align="center">{ROW.dibuat_oleh}</div></td>
       <td><div align="center">{ROW.waktu_pakai}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
			 <td><div align="center">{ROW.kode_jadwal}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
			 <td><div align="left">{ROW.telp}</div></td>
			 <td><div align="right">{ROW.nilai}</div></td>
			 <td><div align="left">{ROW.cso}</div></td>
			  <td><div align="center">{ROW.expired}</div></td>
			 <td><div align="left">{ROW.keterangan}</div></td>
       <td><div align="center">{ROW.status}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>