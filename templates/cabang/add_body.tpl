<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validasiAngka(evt){
    var theEvent = evt || window.event;

    var key = theEvent.keyCode || theEvent.which;

    key = String.fromCharCode(key);

    var regex = /[0-9]/;

    if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 ||
        [evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;

    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        theEvent.preventDefault();
    }
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('nama_invalid');
	Element.hide('telp_invalid');
	Element.hide('fax_invalid');
	
	kode			= document.getElementById('kode');
	nama			= document.getElementById('nama');
	telp			= document.getElementById('telp');
	fax				= document.getElementById('fax');
		
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(!cekValue(telp.value)){	
		valid=false;
		Element.show('telp_invalid');
	}
	
	if(!cekValue(fax.value)){	
		valid=false;
		Element.show('fax_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data_mobil" action="{U_CABANG_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Cabang</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='400'>
				<table width='400'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      		<input type="hidden" name="kode_old" value="{KODE_OLD}">
						<td width='200'><u>Kode Cabang</u></td><td width='5'>:</td>
						<td>
							<input type="text" id="kode" name="kode" value="{KODE}" maxlength=50 onChange="Element.hide('kode_invalid');">
							<span id='kode_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    	</tr>
					<tr>
			      		<td><u>Nama</u></td><td>:</td>
						<td>
							<input type="text" id="nama" name="nama" value="{NAMA}" maxlength=100 onChange="Element.hide('nama_invalid');">
							<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    	</tr>
					<tr>
			      		<td valign='top'>Alamat</td><td  valign='top'>:</td>
						<td>
							<textarea name="alamat" id="alamat" cols="30" rows="3"  maxlength=150>{ALAMAT}</textarea>
						</td>
			    	</tr>
					<tr>
			      		<td>Kota</td><td>:</td>
						<td>
							<select id='kota' name='kota'>{OPT_KOTA}</select>
						</td>
			    	</tr>
					<tr>
			      		<td>Telp</td><td>:</td>
						<td>
							<input type="text" id="telp" name="telp" value="{TELP}" maxlength=50 onChange="Element.hide('telp_invalid');">
							<span id='telp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    	</tr>
					<tr>
			      		<td>Fax</td><td>:</td>
						<td>
							<input type="text" id="fax" name="fax" value="{FAX}" maxlength=50 onChange="Element.hide('fax_invalid');">
							<span id='fax_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    	</tr>
					<tr>
						<td>Saldo Drop Cash</td><td>:</td>
						<td>
							<input type="text" id="saldo" name="saldo" value="{SALDO}" onkeypress="return validasiAngka(event);">
						</td>
					</tr>
					<tr>
			      		<td>Tipe Cabang</td><td>:</td>
						<td>
							<select id='tipe_cabang' name='tipe_cabang'>{OPT_TIPE_CABANG}</select>
						</td>
			    	</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>