function ShowDialogKode(jenis){
	document.getElementById('jenis_pembatalan').value=jenis;
	//document.getElementById('kode').value='';
	dlg_TanyaHapus.show();
	//HapusBerdasarkanJenis();
}

function ValidasiAngka(objek,kolom){
	temp_nilai=objek.value*0;
	
	if(temp_nilai!=0){
		alert(kolom+" harus angka!");
		objek.setFocus;exit;
	}
	
	if(objek.value<0){
		alert(kolom+" tidak boleh kurang dari 0!");
		objek.setFocus;exit;
	}
	
}

function getUpdateAsal(kota_asal,mode){
	// fungsi ini mengubah RUTE menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   			
	

	document.getElementById('rewritepaket').innerHTML="";	
		
	//mengambil tanggal yang diinput user dan rute
	tgl_user  = document.getElementById('p_tgl_user').value;
	
	//memeriksa combo rewritejadwal sudah ditampilkan atau belum
	
	temp_combo_rute=document.getElementById('rewritejadwal').innerHTML;
	
	if (temp_combo_rute != ""){
		//jika sudah muncul, akan diambil nilai dari combo rute
		jam=document.getElementById('p_jadwal').value;
		id_jurusan	= document.getElementById('tujuan').value;
	}
	else{
		//jika belum dimunculkan, maka nilai dari rute akan diisi dengan kosong
		jam='';
		mode=0;
	}
	
	if(mode==0){
		
		document.getElementById('rewritetujuan').innerHTML = "";
		document.getElementById('rewritejadwal').innerHTML = "";
		
		new Ajax.Updater("rewriteasal","reservasi.php?sid="+SID, {
			asynchronous: true,
			method: "get",

			parameters: "mode=asal&kota_asal="+kota_asal,
			onLoading: function(request){
				Element.show('progress_asal');
			},
			onComplete: function(request){
				Element.show('rewriteasal');
				Element.hide('progress_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});
		
	}
	else{
		getUpdatePaket();
		getPengumuman();
		getListHargaPaket();
	}
	
			
}

function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
 
		new Ajax.Updater("rewritetujuan","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&mode=tujuanpaket",
        onLoading: function(request) 
        {
            Element.show('progress_tujuan');
            document.getElementById('rewritejadwal').innerHTML="";
						//Element.hide('rewritejadwal');
        },
        onComplete: function(request) 
        {
            Element.show('rewritetujuan');
						Element.hide('progress_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJadwal(id_jurusan){
				
	tgl=document.getElementById('p_tgl_user').value;
	
	// mengupdate JAM sesuai dengan RUTE yang dipilih
	document.getElementById('id_jurusan_aktif').value = id_jurusan;
	
	document.getElementById('rewritepaket').innerHTML="";	
	
	new Ajax.Updater("rewritejadwal","reservasi.php?sid="+SID, 
	{
		asynchronous: true,
		method: "get",
		parameters: "tgl=" + tgl + "&id_jurusan=" + id_jurusan +"&mode=jam",
		onLoading: function(request) 
		{
			document.getElementById('rewritejadwal').innerHTML="";	
			Element.show('progress_jam');
		},
			onComplete: function(request) 
		{
			Element.hide('progress_jam');
		  Element.show('rewritejadwal');
		},                
		onFailure: function(request) 
		{ 
			assignError(request.responseText);
		}
	});
}

function changeRuteJam(){
	Element.show('loading_layout_kursi');
	getUpdatePaket();
	Element.hide('loading_layout_kursi');
	getListHargaPaket();
}

var obj_jadwal_dipilih;

function pilihJadwal(obj){
	if(obj_jadwal_dipilih){
		obj_jadwal_dipilih.className='a';
	}
	
	obj.className='selected';
	obj_jadwal_dipilih=obj;
}

function getPengumuman(){

		new Ajax.Updater("rewritepengumuman","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=pengumuman",
        onLoading: function(request) 
        {
            
        },
        onComplete: function(request) 
        {   
					
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getUpdatePaket(){
		//mereset timer waktu refresh
		resetTimerRefresh();
		
    // update informasi layout-paket sesuai TANGGAL, RUTE dan JAM
    var tgl  							= document.getElementById('p_tgl_user').value;
    var jadwal  					= document.getElementById('p_jadwal').value;
    var flag_mutasi_paket = document.getElementById('flag_mutasi_paket').value;
    
		new Ajax.Updater("rewritepaket","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "tanggal=" + tgl + "&jadwal="+ jadwal + "&flag_mutasi=" + flag_mutasi_paket  + "&mode=paketlayout",
        onLoading: function(request) 
        {
					Element.show('progress_paket'); 
        },
        onComplete: function(request) 
        {   	    
					Element.hide('progress_paket'); 
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function showPaket(no_tiket){
	document.getElementById('flag_mutasi_paket').value=0; 
	
	new Ajax.Updater("dataPelanggan","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "no_tiket="+no_tiket+"&mode=paketdetail",
        onLoading: function(request) 
        {
					Element.show('loading_data_penumpang');
        },
        onComplete: function(request) 
        {              
					Element.hide('loading_data_penumpang');
				
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });  
}

function batalPaket(){
	
	no_tiket	= document.getElementById("kode_booking").value;
	
	if(confirm("Apakah anda yakin akan membatalkan paket ini?")){
	
		new Ajax.Request("reservasi.php?sid="+SID,{
			asynchronous: true,
			method: "post",
			parameters: "mode=paketbatal"+
				"&no_tiket="+no_tiket+
				"&username="+document.getElementById("pembatalan_username").value+
				"&password="+document.getElementById("pembatalan_password").value,
			onLoading: function(request) {
				Element.show('loading_data_penumpang');
			},
			onComplete: function(request) 
			{
			
			},
			onSuccess: function(request) {			
				
				if(request.responseText==1){
					document.getElementById('dataPelanggan').innerHTML='';
					getUpdatePaket();
				}
				else{
					alert("Anda tidak memiliki wewenang untuk membatalkan tiket ini!");
				}
				dialog_pembatalan.hide();
				Element.hide('loading_data_penumpang');
			},
			onFailure: function(request) 
			{
			}
			})  
	}
	
	return false;
		
}

function getListHargaPaket(){
	
	id_jurusan	= document.getElementById('id_jurusan_aktif').value;
	
	if(id_jurusan==""){
		return;
	}
	
	new Ajax.Request("reservasi.php?sid="+SID,{
  
    asynchronous: true,
    method: "get",
    parameters: "id_jurusan="+id_jurusan+
								"&mode=ambil_list_harga_paket&paket=0",
    onLoading: function(request){
			document.getElementById('dlg_paket_jumlah_koli').value="";
			document.getElementById('dlg_paket_berat').value="";
			hitungHargaPaket(document.getElementById('dlg_paket_layanan').value);
		},
    onComplete: function(request){},
    onSuccess: function(request){		
			eval(request.responseText);
		},
    onFailure: function(request){
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  });
			
}


function hitungHargaPaket(jenis_layanan){
	
	berat	 = document.getElementById('dlg_paket_berat').value;
	
	if(berat<=0 || berat==''){
		document.getElementById('rewrite_dlg_paket_harga_paket_show').innerHTML	=  0;
		document.getElementById('rewrite_total_bayar_show').innerHTML	=  0;
		return;
	}
	
	berat_minimum	= 1;
	
	switch(jenis_layanan){
		case 'P':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_p').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_p').value;
		break;
		
		case 'GA':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_ga').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_ga').value; 
		break;
		
		case 'GD':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_gd').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_gd').value; 
		break;
		
		case 'S':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_s').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_s').value; 
		break;

		case 'CA':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_ca').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_ca').value;
			
			berat_minimum	= 50;
		break;
	
		case 'CD':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_cd').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_cd').value; 
			
			berat_minimum	= 50;
		break;

		case 'M1':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_motor1').value; 
			harga_paket_kg_berikutnya	= 0;
			berat_minimum	= 500;
		break;
	
		case 'M2':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_motor2').value; 
			harga_paket_kg_berikutnya	= 0;
			berat_minimum	= 500;
		break;


    case 'I':
      harga_paket_kg_pertama		= 0;
      harga_paket_kg_berikutnya	= 0;
      berat_minimum	= 500;
      break;
	}
	
	berat	= berat>=berat_minimum?berat:berat_minimum;
	
	total_harga_paket	= harga_paket_kg_pertama*1 + harga_paket_kg_berikutnya*(berat-berat_minimum);
	
	if(dlg_paket_cara_bayar.value==0){
		total_bayar	= total_harga_paket-(document.getElementById("diskon").value*total_harga_paket);
	}
	else{
		besardiskon	= document.getElementById("besardiskonpelanggan").value;
		nominaldiskon	= besardiskon>1?besardiskon:besardiskon*total_harga_paket;
		total_bayar	= total_harga_paket-nominaldiskon;
		document.getElementById('diskonpelanggandisplay').innerHTML=":&nbsp;Rp."+formatAngka(nominaldiskon);
	}
	
	
	
	document.getElementById('rewrite_dlg_paket_harga_paket_show').innerHTML	=  FormatUang(total_harga_paket,'.');
	document.getElementById('rewrite_total_bayar_show').innerHTML	=  FormatUang(total_bayar,'.');
	document.getElementById('totalbayar').value	=  total_bayar;
	
}

function pesanPaket(){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	valid=true;
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
  
	if ((document.getElementById('dlg_paket_telp_pengirim').value=='') || !cekValueNoTelp(document.getElementById('dlg_paket_telp_pengirim').value)){
		document.getElementById('dlg_paket_telp_pengirim').style.background="red";
		valid=false;
  }
	
	if ((document.getElementById('dlg_paket_nama_pengirim').value=='') || (document.getElementById('dlg_paket_nama_pengirim').value==String.Empty)){
		document.getElementById('dlg_paket_nama_pengirim').style.background="red";
		valid=false;
	}
	
	if ((document.getElementById('dlg_paket_telp_penerima').value=='') || !cekValueNoTelp(document.getElementById('dlg_paket_telp_penerima').value)){
		document.getElementById('dlg_paket_telp_penerima').style.background="red";
		valid=false;
  }
	
	if ((document.getElementById('dlg_paket_nama_penerima').value=='') || (document.getElementById('dlg_paket_nama_penerima').value==String.Empty)){
		document.getElementById('dlg_paket_nama_penerima').style.background="red";
		valid=false;
	}
	
	if (document.getElementById('dlg_paket_keterangan').value==''){
		document.getElementById('dlg_paket_keterangan').style.background="red";
		valid=false;
  }
	
	if (document.getElementById('dlg_paket_jumlah_koli').value=='' || document.getElementById('dlg_paket_jumlah_koli').value==0){
		document.getElementById('dlg_paket_jumlah_koli').style.background="red";
		valid=false;
  }
	
	if (document.getElementById('dlg_paket_berat').value=="" || document.getElementById('dlg_paket_berat').value=="0"){
		document.getElementById('dlg_paket_berat').style.background="red";
		valid=false;
  }
	
  if(!valid){
		return;
	}
	
	if(document.getElementById('dlg_paket_cara_bayar').value=="1"){
		if(document.getElementById('kodepelanggan').value==""){
			alert("Anda belum memilih pelanggan paket!");
			return;
		}
		
		diskon = document.getElementById('besardiskonpelanggan').value;
		
	}
	else{
		diskon = document.getElementById('diskon').value;
	}
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters:
			"mode=pakettambah"+
			"&namapengirim="+document.getElementById('dlg_paket_nama_pengirim').value+
			"&alamatpengirim="+document.getElementById('dlg_paket_alamat_pengirim').value+
			"&teleponpengirim="+document.getElementById('dlg_paket_telp_pengirim').value+
			"&namapenerima="+document.getElementById('dlg_paket_nama_penerima').value+
			"&alamatpenerima="+document.getElementById('dlg_paket_alamat_penerima').value+
			"&teleponpenerima="+document.getElementById('dlg_paket_telp_penerima').value+
			"&keterangan="+document.getElementById('dlg_paket_keterangan').value+
			"&jumlahkoli="+document.getElementById('dlg_paket_jumlah_koli').value+
			"&berat="+document.getElementById('dlg_paket_berat').value+
			"&jenisbarang="+document.getElementById('dlg_paket_jenis_barang').value+
			"&layanan="+document.getElementById('dlg_paket_layanan').value+
			"&carabayar="+document.getElementById('dlg_paket_cara_bayar').value+
			"&tanggal=" +document.getElementById('p_tgl_user').value+
			"&kodejadwal="+document.getElementById('p_jadwal').value+
			"&instruksikhusus="+document.getElementById('dlg_paket_intruksi_khusus').value+
			"&nospj="+no_spj+
			"&diskon="+diskon+
			"&kodepelanggan="+document.getElementById('kodepelanggan').value,
			
    onLoading: function(request) 
    {
			Element.show('progress_paket');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {
    	
			if(request.responseText==1){
				getUpdatePaket();
				Element.hide('progress_paket');
				resetIsianPaket();
			}
			else{
				alert("Terjadi kegagalan!");
			}
			
			Element.hide('progress_paket');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function ubahPaket(){
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_tiket="+document.getElementById("kode_booking").value+
								"&nama_pengirim="+document.getElementById("nama_pengirim").value+
								"&alamat_pengirim="+document.getElementById("alamat_pengirim").value+
								"&telp_pengirim="+document.getElementById("telp_pengirim").value+
								"&nama_penerima="+document.getElementById("nama_penerima").value+
								"&alamat_penerima="+document.getElementById("alamat_penerima").value+
								"&telp_penerima="+document.getElementById("telp_penerima").value+
								"&mode=paketubah",
    onLoading: function(request) 
    {
			Element.show('progress_paket');
    },
    onComplete: function(request) 
    {
		
    },
    onSuccess: function(request) 
    {
       Element.hide('progress_paket');
			 alert("Data telah diubah!");
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function resetIsianPaket(){
	
  document.getElementById('dlg_paket_telp_pengirim').value		="";	
	document.getElementById('dlg_paket_nama_pengirim').value		="";
	document.getElementById('dlg_paket_alamat_pengirim').value	="";
	
	document.getElementById('dlg_paket_telp_penerima').value		="";		
	document.getElementById('dlg_paket_nama_penerima').value		="";		
	document.getElementById('dlg_paket_alamat_penerima').value	="";
	
	document.getElementById('dlg_paket_jumlah_koli').value			="";			
	document.getElementById('dlg_paket_berat').value						="";
	document.getElementById('dlg_paket_jenis_barang').options[0].setAttribute("selected", "selected");
	document.getElementById('dlg_paket_layanan').options[0].setAttribute("selected", "selected");
	document.getElementById('dlg_paket_cara_bayar').options[0].setAttribute("selected", "selected");
	caraBayarClick(document.getElementById('dlg_paket_cara_bayar').value);
	
	document.getElementById('dlg_paket_keterangan').value			="";
	document.getElementById('dlg_paket_intruksi_khusus').value	="";
	
	
	document.getElementById('diskon').options[0].setAttribute("selected", "selected");
	
	hitungHargaPaket(document.getElementById('dlg_paket_layanan').value);
}

function ambilDataPaket(no_resi){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		dialog_cari_paket.hide();
		
		document.getElementById('rewrite_ambil_paket').innerHTML="";
		
		if(no_resi==""){
			alert("Anda belum memasukkan no resi paket yang akan dicari!");
			return;
		}
		
		dialog_ambil_paket.show();
		
		new Ajax.Updater("rewrite_ambil_paket","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "no_tiket=" +no_resi+"&mode=paketdetail&submode=ambil",
	        onLoading: function(request){
						Element.show('progress_ambil_paket');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_ambil_paket');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
		
}

function prosesAmbilPaket(){
  
  // melakukan saving / update transaksi/
	
	no_tiket					= document.getElementById('dlg_ambil_paket_no_tiket').value;	
	nama_pengambil		= document.getElementById('dlg_ambil_paket_nama_pengambil').value;	
	no_ktp_pengambil	= document.getElementById('dlg_ambil_paket_no_ktp_pengambil').value;				
	cara_bayar				= document.getElementById('hdn_paket_cara_pembayaran').value;			
	
	if(cara_bayar==2){
		if(confirm("Paket ini akan dibayar oleh pelanggan yang mengambil paket, " + 
				"silahkan informasikan terlebih dahulu kepada pelanggan."+
				"Klik OK untuk melanjutkan proses pengambilan atau klik CANCEL untuk membatalkannya!")){
			}
			else{
				return false;
			}
	}
	
	valid=true;
	Element.hide('dlg_ambil_paket_nama_pengambil_invalid');
	Element.hide('dlg_ambil_paket_no_ktp_pengambil_invalid');
	
  if (no_tiket==''){
		return false;
	}
  
	if (nama_pengambil=='' || (nama_pengambil==String.Empty)){
		Element.show('dlg_ambil_paket_nama_pengambil_invalid');
		valid=false;
  }
	
	if ((no_ktp_pengambil=='') || (no_ktp_pengambil==String.Empty)){
		Element.show('dlg_ambil_paket_no_ktp_pengambil_invalid');
		valid=false;
	}
	
  if(!valid){
		return false;
	}

  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"no_tiket="+no_tiket+"&nama_pengambil="+nama_pengambil+"&no_ktp_pengambil="+no_ktp_pengambil+
			"&mode=paketambil",
			
    onLoading: function(request) 
    {
			Element.show('progress_ambil_paket');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			
			if(request.responseText==1){
				Element.hide('progress_ambil_paket');
				dialog_ambil_paket.hide();
			}
			else{
				alert("Terjadi kegagalan pengambilan paket!");
			}
			
			Element.hide('progress_paket');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  }) 
}

function CetakTiket(jenis_pembayaran){
  //mencetak tiket 
	
	if(!document.getElementById('kode_booking')){
		kode_booking	= document.getElementById('kode_booking_go_show').value;
		cetak_tiket	= 0;
	}
	else{
		kode_booking	= document.getElementById('kode_booking').value;
		cetak_tiket	= document.getElementById('cetak_tiket')?document.getElementById('cetak_tiket').value:0;
	}
	
	no_tiket		= document.getElementById('kode_booking').value;
	parameter		= "tiket_paket.php?sid="+SID+"&no_tiket='"+no_tiket+"'&jenis_pembayaran="+jenis_pembayaran;
	//parameter		= "tiket_paket_backup.php?sid="+SID+"&no_tiket='"+no_tiket+"'&jenis_pembayaran="+jenis_pembayaran;
	
  Start(parameter);
	
	dialog_pembayaran.hide();
	getUpdatePaket();
	
	document.getElementById("dataPelanggan").innerHTML="";
}

function CetakTiketPDF(jenis_pembayaran){
  //mencetak tiket

	if(!document.getElementById('kode_booking')){
		kode_booking	= document.getElementById('kode_booking_go_show').value;
		cetak_tiket	= 0;
	}
	else{
		kode_booking	= document.getElementById('kode_booking').value;
		cetak_tiket	= document.getElementById('cetak_tiket')?document.getElementById('cetak_tiket').value:0;
	}

	no_tiket		= document.getElementById('kode_booking').value;
	parameter		= "tiket_paket_pdf.php?sid="+SID+"&no_tiket='"+no_tiket+"'&jenis_pembayaran="+jenis_pembayaran;
	//parameter		= "tiket_paket_backup.php?sid="+SID+"&no_tiket='"+no_tiket+"'&jenis_pembayaran="+jenis_pembayaran;

  Start(parameter);

	//dialog_pembayaran.hide();
	getUpdatePaket();

	document.getElementById("dataPelanggan").innerHTML="";
}

function cariDataPelangganByTelp4Paket(komponen,flag){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
	
	if(komponen.value==""){
		return;
	}
	
	if(!cekValueNoTelp(komponen.value)){	
		alert("Nomor telepon yang anda masukkan tidak benar!");
		return;
	}
		
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_telp="+komponen.value+
								"&mode=periksa_no_telp&paket=1&flag="+flag,
    onLoading: function(request) 
    {
			komponen.placeholder="mencari...";
    },
    onComplete: function(request) 
    {
			komponen.placeholder="";
    },
    onSuccess: function(request) 
    {		
			if(request.responseText!=0){
				eval(request.responseText);
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function periksaPaket(no_resi){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		
		document.getElementById('rewrite_cari_paket').innerHTML="";
		
		if(no_resi==""){
			alert("Anda belum memasukkan no resi paket yang akan dicari!");
			return;
		}
		
		dialog_cari_paket.show();
		
		new Ajax.Updater("rewrite_cari_paket","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "no_resi=" +no_resi+"&mode=cari_paket",
	        onLoading: function(request){
						Element.show('progress_cari_paket');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_cari_paket');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
}

function setFlagMutasiPaket(){
	
	if(document.getElementById('flag_mutasi_paket').value!=1){
		if(confirm("Anda akan mengaktifkan mode mutasi, " + 
			"ketika anda memilih menekan tombol mutasi pada daftar paket, maka secara otomatis paket lama akan dimutasikan ke jadwal yang baru."+
			"Klik OK untuk melanjutkan proses mutasi atau klik CANCEL untuk keluar dari mode mutasi!")){
			lanjut = true;
		}
		else{
			lanjut = false;
		}
	}
	else{
		lanjut=true;
	}
	
	if(lanjut){
		document.getElementById('flag_mutasi_paket').value	= 1-document.getElementById('flag_mutasi_paket').value;
		
		if(document.getElementById('flag_mutasi_paket').value==1){
			document.getElementById('btn_mutasi_paket').value="     Batalkan Mutasi     ";
		}
		else{
			document.getElementById('btn_mutasi_paket').value="      Mutasi Paket       ";
		}
		
		getUpdatePaket();
	}
}

function mutasiPaket(){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	tgl  					= document.getElementById('p_tgl_user').value;
  kode_jadwal  	= document.getElementById('p_jadwal').value;  
	no_tiket			= document.getElementById('kode_booking').value;
	
	if(!confirm("Apakah anda yakin akan memindahkan paket ke jadwal ini?")){
		exit;
	}
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"no_tiket="+no_tiket+"&tanggal=" +tgl+ "&kode_jadwal="+kode_jadwal+
			"&no_spj="+no_spj+"&mode=mutasipaket",
			
    onLoading: function(request) 
    {
			Element.show('progress_paket');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			if(request.responseText==1){
				document.getElementById('flag_mutasi_paket').value=0;
				document.getElementById('dataPelanggan').innerHTML='';
				
				getUpdatePaket();
				
			}
			else{
				alert("Terjadi kegagalan!");
			}
			
			Element.hide('progress_paket');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function caraBayarClick(carabayardipilih){
	if(carabayardipilih=='0'){
		linkdaftapelanggan.style.display='none';
		diskondisplay.style.display='block';
		diskonpelanggandisplay.style.display='none';
		datapelanggandisplay.style.display='none';
	}
	else{
		linkdaftapelanggan.style.display='block';
		diskondisplay.style.display='none';
		diskonpelanggandisplay.style.display='block';
		datapelanggandisplay.style.display='block';
	}

	kodepelanggan.value="";
	namapelanggandisplay.innerHTML	= "";
	diskonpelanggandisplay.innerHTML= "";
	besardiskonpelanggan.value="0";
	hitungHargaPaket(dlg_paket_layanan.value);
}

function setDataPelanggan(kodepelanggan){
	
	new Ajax.Request("paket.pelanggan.popup.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: 
			"mode=setdata"+
			"&kodepelanggan="+kodepelanggan+
			"&harga="+totalbayar.value,
			
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {
			eval(request.responseText);
			hitungHargaPaket(dlg_paket_layanan.value);
			
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  }) ;
}

function tampilkanDialogPembatalan(){
	document.getElementById('pembatalan_username').value	= '';
	document.getElementById('pembatalan_password').value	= '';
	dialog_pembatalan.show();
}


// global
var dialog_SPJ,dialog_ubah_tiket,btn_SPJ_OK,btn_SPJ_Cancel,msg;

function init(e) {
  // inisialisasi variabel
	SID =document.getElementById("hdn_SID").value;
	
	//control dialog cari paket
	dialog_cari_paket = dojo.widget.byId("dialog_cari_paket");
  btn_cari_paket_Cancel = document.getElementById("dialog_cari_paket_btn_Cancel");
  dialog_cari_paket.setCloseControl(btn_cari_paket_Cancel);
	
	//control dialog ambil paket
	dialog_ambil_paket 						= dojo.widget.byId("dialog_ambil_paket");
  dlg_ambil_paket_button_ok 		= document.getElementById("dlg_ambil_paket_button_ok");
  dlg_ambil_paket_button_cancel = document.getElementById("dlg_ambil_paket_button_cancel");
	
	//control dialog box batal
	dialog_pembatalan = dojo.widget.byId("dialog_pembatalan");
  dialog_pembatalan_btn_ok = document.getElementById("dialog_pembatalan_btn_ok");
  dialog_pembatalan_btn_cancel = document.getElementById("dialog_pembatalan_btn_cancel");
  dialog_pembatalan.setCloseControl(dialog_pembatalan_btn_cancel);
		
	getPengumuman();
	
	caraBayarClick(dlg_paket_cara_bayar.value);
	
	//getUpdateAsal(kota_asal.value,1);
	
}

dojo.addOnLoad(init);

var jdwl,tgl;
var waktu_refresh=10;//detik
var temp_waktu=waktu_refresh;
var g_flag_refresh=1;

function beginrefresh(){

	if (temp_waktu==1) {
		if(g_flag_refresh==1){
			
			temp_waktu=waktu_refresh;
			
			beginrefresh();
			
			//merefresh pengumumman
			getPengumuman();
			getUpdatePaket();
			
		}
	}
	else {
		if(g_flag_refresh==1){
			//get the minutes, seconds remaining till next refersh
			temp_waktu-=1;
			setTimeout("beginrefresh()",1000);
		}
	}
	
}

function resetTimerRefresh(){
	temp_waktu=waktu_refresh;
}

function setAutoRefreshOff(){
	g_flag_refresh=0;
}

function setAutoRefreshOn(){
	g_flag_refresh=1;
	beginrefresh();
}

//call the function beginrefresh
window.onload=beginrefresh();
//-->