var SID="";

function findPosY(obj) {
  var curtop = 0;

  if(obj.offsetParent) {
    while(1) {
      curtop += obj.offsetTop;
      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;
    }
  } else if(obj.y) {
    curtop += obj.y;
  }

  return curtop;
}

function findPosX(obj) {
  var curleft = 0;

  if(obj.offsetParent) {
    while(1) {
      curleft += obj.offsetLeft;
      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;
    }
  } else if(obj.x) {
    curleft += obj.x;
  }

  obj.style.position = "static";

  return curleft;
}

function cekValueNoTelp(no_telp){
	//cek 1
	jum_digit=no_telp.length;
	
	if(jum_digit<8){
		//alert(1);
		return false;
	}
	
	//cek 2
	kode_inisial=no_telp.substring(0,1);
	
	if(kode_inisial!=0){
		//alert(2);
		return false;
	}
	
	//cek 3
	kode_area=no_telp.substring(1,2)*1;
	
	if(kode_area<=1){
		//alert(3);
		return false;
	}
	
	return true;
	
}

function validasiNoTelp(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function validasiAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function charCheck(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[']/;
	
	if( regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}


function echeck(str){

	var at="@"
	var dot="."
	var lat=str.indexOf(at)
	var lstr=str.length
	var ldot=str.indexOf(dot)
	
	if (str.indexOf(at)==-1){
	  return false
	}

	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
	  return false
	}

	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
	   return false
	}

	if (str.indexOf(at,(lat+1))!=-1){
	   return false
	}

	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
	   return false
	}

	if (str.indexOf(dot,(lat+2))==-1){
	   return false
	}
	
	if (str.indexOf(" ")!=-1){
	   return false
	}

 	return true;
}

function submitPilihanMenu(menu_dipilih){
	
	document.getElementById('menu_dipilih').value	= menu_dipilih;
	
	if(menu_dipilih=='top_menu_operasional'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_operasional.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_lap_reservasi'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_lap_reservasi.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_lap_keuangan'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_lap_keuangan.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_tiketux'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_tiketux.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_pengelolaan_member'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_pengelolaan_member.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_pengaturan'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_pengaturan.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
}

function pilihTopMenu(menu_dipilih){
	document.getElementById(menu_dipilih).setAttribute("class", "top_menu_on_select");
}

function setMenuOver(menu_dipilih,komponen_drop_down){
	
	if(document.getElementById(komponen_drop_down)){
		document.getElementById(komponen_drop_down).style.top=findPosY(document.getElementById(menu_dipilih))+17;
		document.getElementById(komponen_drop_down).style.left=findPosX(document.getElementById(menu_dipilih));
	}
	
	if(komponen_drop_down!=''){
		Element.show(komponen_drop_down)
	}
	
	if(menu_dipilih!= document.getElementById("top_menu_dipilih").value){
		document.getElementById(menu_dipilih).setAttribute("class", "top_menu_on_focus");
	}
}

function setMenuOut(menu_dipilih,komponen_drop_down){
	if(komponen_drop_down!=''){
		Element.hide(komponen_drop_down)
	}
	
	if(menu_dipilih!= document.getElementById("top_menu_dipilih").value){
		document.getElementById(menu_dipilih).setAttribute("class", "top_menu_lost_focus");
	}
}

function openNewPage(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function formatAngka(num) {
	num = num.toString().replace(/\$|\,/g,'');
	
	if(isNaN(num)) num = "0";
	
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	
	if(cents<10) cents = "0" + cents;
	
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'+
		num.substring(num.length-(4*i+3));
	
	return (((sign)?'':'-') + num);
}

function formatAngkaKalkulasi(str){
	hasil	=str.toString().replace(/\./g,'');
	return hasil;
}

function ubahPassword(password_lama,password_baru,konfirmasi_password){
	
	if(password_baru==''){
		alert("Anda BELUM MEMASUKKAN Password baru anda!");
		return false;
	}
	
	if(password_baru!=konfirmasi_password){
		alert("Konfirmasi Password Baru anda tidak sama dengan Password Baru yang anda masukkan!");
		return false;
	}
	
	new Ajax.Request("./../_auth/auth.php?sid="+SID,{
			asynchronous: true,
			method: "post",
			parameters: "mode=ubah_password&password_lama="+password_lama+"&password_baru="+password_baru,
			onLoading: function(request){
				
			},
	    onComplete: function(request){
			},
			onSuccess: function(request){			
				
				if(request.responseText==1){
					alert("Password BERHASIL DIUBAH!");
					Element.hide('pop_up_ubah_password');
				}
				else{
					alert("Password lama yang anda masukkan tidak benar!");
				}
			},
			onFailure: function(request) 
			{
				
			}
		})
}

function showUbahPassword(){
	password_lama.value='';
	password_baru.value='';
	konfirmasi_password.value='';
	document.getElementById("pop_up_ubah_password").style.left=findPosX(document.getElementById("show_ubah_password"))-187;
	Element.show("pop_up_ubah_password");
	
}

function strClearence(str){
	string_output	= str.replace(/&/gi,"%26");
	return string_output;
}

function Right(str, n){
	/*
      IN: str - the string we are RIGHTing
      n - the number of characters we want to return

      RETVAL: n characters from the right side of the string
       */
  if (n <= 0)     // Invalid bound, return blank string
		return "";
  else if (n > String(str).length)   // Invalid bound, return
		return str;                     // entire string
  else { // Valid bound, return appropriate substring
		var iLen = String(str).length;
    return String(str).substring(iLen, iLen - n);
  }
}

function FormatUang(uang,separator){
	len_uang = String(uang).length;
	return_val='';
	for (i=len_uang;i>=0;i--){
		if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

		return_val =String(uang).substring(i,i-1)+return_val;
	}
	
	return return_val;
}