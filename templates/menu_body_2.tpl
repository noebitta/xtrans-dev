<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<table width="100%" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='80%' class='border'>
	    <tr><td colspan=4 class="menuutama">&nbsp;Operasional</td></tr>
	    <tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
		    <td align='center' width='25%'>
			    <!-- MODUL -->
					<a href="{U_RESERVASI}"><img src="{TPL}images/icon_booking.png" />
					<br/>
					<span class="genmed">Reservasi</span> </a>
		    </td>
				
				<td align='center' width='25%'>
					<a href="{U_UBAHPASS}"><img src="{TPL}images/icon_menu_password.png" /></a>
					<br />
					<a href="{U_UBAHPASS}"><span class="genmed">Ubah Password</span></a>
		    </td>
				<td align='center' width='25%'>
					<a href="{U_USER_LOGIN}"><img src="{TPL}images/icon_user_online.png" /></a>  
					<br />
					<a href="{U_USER_LOGIN}"><span class="genmed">Status On-Line<br>User</span></a>  
				</td>
				<td align='center' width='25%'>
					<a href="{U_PROMO}"><img src="{TPL}images/icon_promo.png" /></a>  
					<br />
					<a href="{U_PROMO}"><span class="genmed">Promo</span></a>
		    </td>
				<td align='center' width='25%'>
					<a href="{U_BATAL}"><img src="{TPL}images/icon_batal.png" /></a>  
					<br />
					<a href="{U_BATAL}"><span class="genmed">Pembatalan</span></a>
		    </td>
			</tr>
		</table>
		<br><br>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Laporan</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center' width='25%'>
					<a href="{U_LAPORAN_CSO}"><img src="{TPL}images/icon_laporan_cso.png" /></a>
					<br />
					<a href="{U_LAPORAN_CSO}"><span class="genmed">Laporan Penjualan<br>CSO</span></a>
		    </td>
				
				<td align='center'>
					<a href="{U_LAPORAN_CABANG}"><img src="{TPL}images/icon_laporan_cabang.png" /></a>
					<br />
					<a href="{U_LAPORAN_CABANG}"><span class="genmed">Laporan Penjualan<br>Cabang</span></a>
		    </td>
				
				<td align='center'>
					<a href="{U_LAPORAN_JURUSAN}"><img src="{TPL}images/icon_laporan_jurusan.png" /></a>
					<br />
					<a href="{U_LAPORAN_JURUSAN}"><span class="genmed">Laporan Jurusan</span></a>
		    </td>
				
				<td align='center'>
					<a href="{U_LAPORAN_JADWAL"><img src="{TPL}images/icon_laporan_jadwal.png" /></a>
					<br />
					<a href="{U_LAPORAN_JADWAL}"><span class="genmed">Laporan Jadwal</span></a>
		    </td>
			
			</tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center'>
					<a href="{U_LAPORAN_OMZET}"><img src="{TPL}images/icon_laporan_omzet.png" /></a>  
					<br />
					<a href="{U_LAPORAN_OMZET}"><span class="genmed">Laporan Omzet</span></a>
		    </td>
				
				<td align='center'>
					<a href="{U_LAPORAN_KENDARAAN}"><img src="{TPL}images/icon_laporan_kendaraan.png" /></a>  
					<br />
					<a href="{U_LAPORAN_KENDARAAN}"><span class="genmed">Laporan Kendaraan</span></a>
		    </td>
				
				<td align='center'>
					<a href="{U_LAPORAN_SOPIR}"><img src="{TPL}images/icon_laporan_sopir.png" /></a>  
					<br />
					<a href="{U_LAPORAN_SOPIR}"><span class="genmed">Laporan Sopir</span></a>
		    </td>
				
				<td align='center'>
					<a href="{U_LAPORAN_OP}"><img src="{TPL}images/icon_laporan_biaya_op.png" /></a>  
					<br />
					<a href="{U_LAPORAN_OP}"><span class="genmed">Laporan Biaya OP</span></a>
		    </td>
			</tr>
		</table>
		<br><br>
		<table width='80%' class='border'>
			<tr><td colspan=4 class="menuutama">&nbsp;Master Data</td></tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align="center">  
					<a href="{U_CABANG}"><img src="{TPL}images/icon_master_cabang.png" /></a>  
					<br />
					<a href="{U_CABANG}"><span class="genmed">Cabang</span></a>    
			  </td>	
				<td align='center'>
					<a href="{U_JURUSAN}"><img src="{TPL}images/icon_master_jurusan.png" /></a>  
					<br />
					<a href="{U_JURUSAN}"><span class="genmed">Jurusan</span></a>
		    </td>
				<td align='center'>
					<a href="{U_JADWAL}"><img src="{TPL}images/icon_master_jadwal.png" /></a>  
					<br />
					<a href="{U_JADWAL}"><span class="genmed">Jadwal</span></a>
		    </td>    
	    </tr>
			<tr><td colspan=4><br></td></tr>
			<tr height=100 valign='top' align='center'>
				<td align='center'>
					<a href="{U_SOPIR}"><img src="{TPL}images/icon_master_sopir.png" /></a>  
					<br />
					<a href="{U_SOPIR}"><span class="genmed">Sopir</span></a>
		    </td>
				<td align='center'>
					<a href="{U_MOBIL}"><img src="{TPL}images/icon_master_mobil.png" /></a>  
					<br />
					<a href="{U_MOBIL}"><span class="genmed">Mobil</span></a>
		    </td>
				<td align="center">  
					<a href="{U_USER}"><img src="{TPL}images/icon_master_user.png" /></a>  
					<br />
				 <a href="{U_USER}"><span class="genmed">User</span></a>    
		    </td>	
			</tr>
		</table>
 </td>
</tr>
</table>