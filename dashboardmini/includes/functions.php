<?php

function generateNoTiket($kode_jadwal){
	$nourut = rand(100,999);
	$no_tiket = "T".dateYMD().$kode_jadwal.$nourut;
	
	return $no_tiket;
}

function generateKodeBooking($kode_jadwal){
	return date("ymdhis").$kode_jadwal.rand(100,999);
}

function generateNoTiketPaket(){
	$temp	= array("-",
		"1","2","3","4","5","6","7","8","9",
		"A","B","C","D","E","F","G","H","I","J",
		"K","L","M","N","O","P","Q","R","S","T",
		"U","V","W","X","Y","Z",
		"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
		"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
		"U1","V1","W1","X1","Y1","Z1");
	
	$y		= $temp[date("y")*1];
	$m		= $temp[date("m")*1];
	$d		=	$temp[date("d")*1];
	$j		= $temp[date("j")*1];
	$mn		= $temp[date("i")*1];
	$s		= $temp[date("s")*1];
	$rnd1	= $temp[rand(1,61)];
	$rnd2	= $temp[rand(1,61)];
	$rnd3	= $temp[rand(1,61)];
	$rnd3	= $temp[rand(1,61)];
	
	return "P".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
}

function generateKodeBookingPenumpang(){
	global $db;
	
	$temp	= array("-",
		"1","2","3","4","5","6","7","8","9",
		"A","B","C","D","E","F","G","H","I","J",
		"K","L","M","N","O","P","Q","R","S","T",
		"U","V","W","X","Y","Z",
		"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
		"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
		"U1","V1","W1","X1","Y1","Z1");
	
	$y		= $temp[date("y")*1];
	$m		= $temp[date("m")*1];
	$d		=	$temp[date("d")*1];
	$j		= $temp[date("j")*1];
	$mn		= $temp[date("i")*1];
	$s		= $temp[date("s")*1];
	
	
	do{
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		
		$kode_booking	= "B".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
	
		$sql = "SELECT COUNT(1) FROM tbl_reservasi WHERE KodeBooking='$kode_booking'";
					
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
	
	}while($row[0]>0);
		
	
	
	return $kode_booking;
}

function generateNoTiketPenumpang(){
	$temp	= array("-",
		"1","2","3","4","5","6","7","8","9",
		"A","B","C","D","E","F","G","H","I","J",
		"K","L","M","N","O","P","Q","R","S","T",
		"U","V","W","X","Y","Z",
		"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
		"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
		"U1","V1","W1","X1","Y1","Z1");
	
	$y		= $temp[date("y")*1];
	$m		= $temp[date("m")*1];
	$d		=	$temp[date("d")*1];
	$j		= $temp[date("j")*1];
	$mn		= $temp[date("i")*1];
	$s		= $temp[date("s")*1];
	$rnd1	= $temp[rand(1,61)];
	$rnd2	= $temp[rand(1,61)];
	$rnd3	= $temp[rand(1,61)];
	$rnd3	= $temp[rand(1,61)];
	
	return "T".$y.$m.$d.$j.$mn.$s.$rnd1.$rnd2;
}

$bulan_string = array(
  	1	=> 'Jan',
  	2 => 'Feb',
  	3 => 'Mar',
  	4 => 'Apr',
  	5 => 'Mei',
		6	=> 'Jun',
  	7 => 'Jul',
  	8 => 'Agu',
  	9 => 'Sep',
  	10=> 'Okt',
  	11=> 'Nov',
  	12=> 'Des'
  );

function setComboTanggal($tgl_dipilih = ""){
	
	$opt_return	= "";
	
	$tgl_dipilih	= $tgl_dipilih==""?date("d"):$tgl_dipilih;
	
	for($tgl=1;$tgl<=31;$tgl++){
		$selected	= $tgl!=$tgl_dipilih?"":"selected";
		$opt_return	.="<option value='$tgl' $selected>$tgl</option> ";
	}
	
	return $opt_return;
}

function setComboBulan($bln_dipilih = ""){
	global $bulan_string;
	
	$opt_return	= "";
	
	$bln_dipilih	= $bln_dipilih==""?date("m"):$bln_dipilih;
	
	for($bln=1;$bln<=12;$bln++){
		$selected	= $bln!=$bln_dipilih?"":"selected";
		$opt_return	.="<option value='$bln' $selected>".$bulan_string[$bln]."</option> ";
	}
	
	return $opt_return;
}
	
function setComboTahun($thn_dipilih = ""){
	
	$opt_return	= "";
	
	$thn_dipilih	= $thn_dipilih==""?date("Y"):$thn_dipilih;
	$thn_max			= date("Y")+3;
	
	for($thn=2010;$thn<=$thn_max;$thn++){
		$selected	= $thn!=$thn_dipilih?"":"selected";
		$opt_return	.="<option value='$thn' $selected>$thn</option> ";
	}
	
	return $opt_return;
}

	
function setComboKota($kota_dipilih){
	//SET COMBO kota
	
	global $LIST_KOTA;
	
	$opt_kota="";
		
	for($idx=0;$idx<count($LIST_KOTA);$idx++){
			$selected	=($kota_dipilih!=$LIST_KOTA[$idx])?"":"selected";
			$opt_kota .="<option value='$LIST_KOTA[$idx]' $selected>$LIST_KOTA[$idx]</option>";
		}
			
	return $opt_kota;
	//END SET COMBO KOTA
}

function setComboLayoutKursi($layout_dipilih){
	
	$temp_var		= "kursi_".$layout_dipilih;
	$$temp_var	= "selected";
	
	$opt_kursi="
		<option value=9 $kursi_9>9 kursi</option>
		<option value=11 $kursi_11>11 kursi</option>
		<option value=131 $kursi_131 >13A kursi</option>
		<option value=132 $kursi_132 >13B kursi</option>
		<option value=19 $kursi_19 >19 kursi</option>
		<option value=29 $kursi_29 >29 kursi</option>";
	
	return $opt_kursi;
}

function setComboUserLevel($level_dipilih){
	
	global $LEVEL_ADMIN;
	global $LEVEL_MANAJEMEN;
	global $LEVEL_MANAJER;
	global $LEVEL_SUPERVISOR;
	global $LEVEL_CSO;
	global $LEVEL_SCHEDULER;
	global $LEVEL_KASIR;
	global $LEVEL_KEUANGAN;
	global $LEVEL_CCARE;
	global $USER_LEVEL;
		
	$temp_var		= "level_".str_replace(".","_",$level_dipilih);
	$$temp_var	= "selected";
	
	$opt_kursi=
		"<option value='$LEVEL_ADMIN' $level_0_0		>Admin</option>
		<option value='$LEVEL_MANAJEMEN' $level_1_0	>Manajemen</option>
		<option value='$LEVEL_MANAJER' $level_1_2 	>Manajer</option>
		<option value='$LEVEL_SUPERVISOR' $level_1_3>Supervisor</option>
		<option value='$LEVEL_CSO' $level_2_0 			>CSO</option>
		<option value='$LEVEL_SCHEDULER' $level_3_0 >Scheduler</option>
		<option value='$LEVEL_KASIR' $level_4_0 		>Kasir</option>
		<option value='$LEVEL_KEUANGAN' $level_5_0 	>Keuangan</option>
		<option value='$LEVEL_CCARE' $level_6_0 	>Customer Care</option>
		";
	
	return $opt_kursi;
}

function getUserLevel($level_dipilih){
	
	global $LEVEL_ADMIN;
	global $LEVEL_MANAJEMEN;
	global $LEVEL_MANAJER;
	global $LEVEL_SUPERVISOR;
	global $LEVEL_CSO;
	global $LEVEL_SCHEDULER;
	global $LEVEL_KASIR;
	global $LEVEL_KEUANGAN;
	global $LEVEL_CCARE;
	
	switch($level_dipilih){
		case $LEVEL_ADMIN:
			$user_level	= "Administrator";
			break;
		case $LEVEL_MANAJEMEN:
			$user_level	= "Manajemen";
			break;
		case$LEVEL_MANAJER:
			$user_level	= "Manajer";
			break;
		case$LEVEL_SUPERVISOR:
			$user_level	= "Supervisor";
			break;
		case $LEVEL_CSO:
			$user_level="CSO";
			break;
		case $LEVEL_SCHEDULER:
			$user_level="Scheduler";
			break;
		case $LEVEL_KASIR:
			$user_level="Kasir";
			break;
		case $LEVEL_KEUANGAN:
			$user_level="Keuangan";
			break;
		case $LEVEL_CCARE:
			$user_level="Customer Care";
			break;
	}
	
	return $user_level;
}

// konversi 12.10.2007 ke 12/10/2007
function dateparse($tgl)
{  
  $temp_tgl	= explode("-",$tgl);
	
	$d = $temp_tgl[0];
  $m = $temp_tgl[1];
  $y = $temp_tgl[2];
	
	/*$d = substr($tgl,0,2);
		$m = substr($tgl,3,2);
		$y = substr($tgl,6,4);*/
	
  $bulan = array(
  					'01' => 'jan',
  					'02' => 'feb',
  					'03' => 'mar',
  					'04' => 'apr',
  					'05' => 'may',
  					'06' => 'jun',
  					'07' => 'jul',
  					'08' => 'aug',
  					'09' => 'sep',
						'1' => 'jan',
  					'2' => 'feb',
  					'3' => 'mar',
  					'4' => 'apr',
  					'5' => 'may',
  					'6' => 'jun',
  					'7' => 'jul',
  					'8' => 'aug',
  					'9' => 'sep',
  					'10' => 'oct',
  					'11' => 'nov',
  					'12' => 'dec'
  				);
  return $d.'-'.$bulan[$m].'-'.$y;  
}

function dateparseWithTime($tgl)
{  
	$temp	= explode(" ",$tgl);
	
	$temp_tgl	= explode("-",$temp[0]);
	
  $d = $temp_tgl[0];
  $m = $temp_tgl[1];
  $y = $temp_tgl[2];
	
  $bulan = array(
  					'01' => 'jan',
  					'02' => 'feb',
  					'03' => 'mar',
  					'04' => 'apr',
  					'05' => 'may',
  					'06' => 'jun',
  					'07' => 'jul',
  					'08' => 'aug',
  					'09' => 'sep',
  					'10' => 'oct',
  					'11' => 'nov',
  					'12' => 'dec'
  				);
  return $d.'-'.$bulan[$m].'-'.$y." ".$temp[1];  
}

function dateparseD_Y_M($tgl)
{  
	$temp_tgl	= explode("-",$tgl);
	$d = $temp_tgl[0];
  $m = $temp_tgl[1];
  $y = $temp_tgl[2];
	
	
	
  $bulan = array(
  					'01' => 'jan',
  					'02' => 'feb',
  					'03' => 'mar',
  					'04' => 'apr',
  					'05' => 'may',
  					'06' => 'jun',
  					'07' => 'jul',
  					'08' => 'aug',
  					'09' => 'sep',
  					'10' => 'oct',
  					'11' => 'nov',
  					'12' => 'dec',
						'1' => 'jan',
  					'2' => 'feb',
  					'3' => 'mar',
  					'4' => 'apr',
  					'5' => 'may',
  					'6' => 'jun',
  					'7' => 'jul',
  					'8' => 'aug',
  					'9' => 'sep'
  				);
  return $d.'-'.$bulan[$m].'-'.$y;  
}

// konversi  bulan
function BulanString($bln)
{  

  $bulan = array(
  					'01' => 'Januari',
  					'02' => 'Februari',
  					'03' => 'Maret',
  					'04' => 'April',
  					'05' => 'Mei',
  					'06' => 'Juni',
  					'07' => 'Juli',
  					'08' => 'Agustus',
  					'09' => 'September',
						'1' => 'Januari',
  					'2' => 'Februari',
  					'3' => 'Maret',
  					'4' => 'April',
  					'5' => 'Mei',
  					'6' => 'Juni',
  					'7' => 'Juli',
  					'8' => 'Agustus',
  					'9' => 'September',
  					'10' => 'Oktober',
  					'11' => 'November',
  					'12' => 'Desember'
  				);
					
  return $bulan[$bln];  
}

function BulanStringShort($bln)
{  

  $bulan = array(
  					'01' => 'Jan',
  					'02' => 'Feb',
  					'03' => 'Mar',
  					'04' => 'Apr',
  					'05' => 'Mei',
  					'06' => 'Jun',
  					'07' => 'Jul',
  					'08' => 'Agu',
  					'09' => 'Sep',
						'1' => 'Jan',
  					'2' => 'Feb',
  					'3' => 'Mar',
  					'4' => 'Apr',
  					'5' => 'Mei',
  					'6' => 'Jun',
  					'7' => 'Jul',
  					'8' => 'Agu',
  					'9' => 'Sep',
  					'10' => 'Okt',
  					'11' => 'Nov',
  					'12' => 'Des'
  				);
					
  return $bulan[$bln];  
}

// konversi 12.10.2007 ke 12-oct-2007
function TanggalToDate($tgl)
{
	$d = substr($tgl,0,2);
  	$m = substr($tgl,3,2);
  	$y = substr($tgl,6,4);
  return $d.'/'.$m.'/'.$y;
}

// konversi 2008-12-30  ke 30-12-2008
function FormatMySQLDateToTgl($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[2];
  $m 			= $temp[1];
  $y 			= $temp[0];
  return substr('0'.$d,-2).'-'.substr('0'.$m,-2).'-'.$y;
}

// konversi 2008-12-30  ke 30-12-2008
function FormatMySQLDateToTglWithTime($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[2];
  $m 			= $temp[1];
  $y 			= $temp[0];
  return $d.'-'.$m.'-'.$y." ".substr($temp_tgl[1],0,5);
}


// konversi  30-12-2008 ke 2008-12-30
function FormatTglToMySQLDate($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[0];
  $m 			= $temp[1];
  $y 			= $temp[2];
	
  return $y.'-'.substr("0".$m,-2).'-'.substr("0".$d,-2)." ".substr($temp_tgl[1],0,5);;
}

// konversi 2008-12-30  ke 30-12-2008
function FormatMySQLDateToTglEng($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[2];
  $m 			= $temp[1];
  $y 			= $temp[0];
  return substr('0'.$m,-2).'/'.substr('0'.$d,-2).'/'.$y;
}

function dateNow($with_time=false){
		
		/*
		Desc	:Mengembalikan tanggal hari ini
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT NOW() AS tgl_sekarang";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$temp	= explode(" ",$row['tgl_sekarang']);
			
			if(!$with_time){
				$return	= $temp[0];
			}
			else{
				$return = $row['tgl_sekarang'];
			}
			
			return $return;
		} 
		else{
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
}//  END dateNow

function dateNowD_MMM_Y(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT NOW() AS tgl_sekarang";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$temp_tgl	= explode(" ",$row['tgl_sekarang']);
			
			$temp	= explode("-",$temp_tgl[0]);
			
			$d	= $temp[2];
			$m	=  BulanStringShort($temp[1]);
			$y	= $temp[0];
			
			return $d."-".$m."-".$y;
		} 
		else{
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
}//  END dateNow


function dateY_M_D(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini dengan format YYYYMMDD
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT DATE(NOW())";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
		return $row[0];
		
		
}//  END dateYMD

function dateD_M_Y(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini dengan format DDMMYYYY
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				DAY({fn NOW()}) as tgl, 
				MONTH({fn NOW()}) as bln,
				YEAR({fn NOW()}) as thn";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$tgl	= $row['tgl'];
			$bln 	= $row['bln'];
			$thn	= $row['thn'];
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
		$tgl	= ($tgl<10)?"0".$tgl:$tgl;
		$bln	= ($bln<10)?"0".$bln:$bln;
		
		return $tgl."-".$bln."-".$thn;
		
		
}//  END dateYMD

function dateYMD(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini dengan format YYYYMMDD
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				DAY({fn NOW()}) as tgl, 
				MONTH({fn NOW()}) as bln,
				YEAR({fn NOW()}) as thn";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$tgl	= $row['tgl'];
			$bln 	= $row['bln'];
			$thn	= substr($row['thn'],-2);
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
		$tgl	= ($tgl<10)?"0".$tgl:$tgl;
		$bln	= ($bln<10)?"0".$bln:$bln;
		
		return $thn.$bln.$tgl;
		
		
}//  END dateYMD

function getMaxDate($bln,$thn){
	$bulan = array(
  					'01' => 31,
  					'02' => 28,
  					'03' => 31,
  					'04' => 30,
  					'05' => 31,
  					'06' => 30,
  					'07' => 31,
  					'08' => 31,
  					'09' => 30,
						'1' => 31,
  					'2' => 28,
  					'3' => 31,
  					'4' => 30,
  					'5' => 31,
  					'6' => 30,
  					'7' => 31,
  					'8' => 31,
  					'9' => 30,
  					'10' => 31,
  					'11' => 30,
  					'12' => 31
  				);
	
	if($thn%4==0){
		$bulan['02']	= 29;
		$bulan['2']		= 29;
	}
	
	return $bulan[$bln];
}

function HariStringShort($hr)
{  

  /*$hari = array(
  					'01' => 'Min',
  					'02' => 'Sen',
  					'03' => 'Sel',
  					'04' => 'Rab',
  					'05' => 'Kam',
  					'06' => 'Jum',
  					'07' => 'Sab',
						'1' => 'Min',
  					'2' => 'Sen',
  					'3' => 'Sel',
  					'4' => 'Rab',
  					'5' => 'Kam',
  					'6' => 'Jum',
  					'7' => 'Sab',
  				);        */
					
	 $hari = array(
  					'07' => 'Min',
  					'01' => 'Sen',
  					'02' => 'Sel',
  					'03' => 'Rab',
  					'04' => 'Kam',
  					'05' => 'Jum',
  					'06' => 'Sab',
						'7' => 'Min',
  					'1' => 'Sen',
  					'2' => 'Sel',
  					'3' => 'Rab',
  					'4' => 'Kam',
  					'5' => 'Jum',
  					'6' => 'Sab',
  				);        
					
  return $hari[$hr];  
}

function now(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				CONVERT(CHAR(25),{fn NOW()},103) waktu_sekarang";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row['waktu_sekarang'];
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
	}//  END now
/*
function pagingData($idx_page,$nama_tabel,$cari,$kondisi,$nama_file,$view_per_page,$page_per_section,$idx_awal_record){
	global $db;
	global $idx_awal_record;
	
	//menghitung jumlah data
		$idx_page = ($idx_page!='')?$idx_page:0;
		$kondisi	=($kondisi!='')?$kondisi:'';
		
		$sql = "SELECT ISNULL(COUNT(*),0) AS JumData FROM $nama_tabel $kondisi";
		
		if ($result = $db->sql_query($sql)){
			$my_data = $db->sql_fetchrow($result);
			$jum_data=$my_data['JumData'];
			
			$jum_halaman=ceil($jum_data/$view_per_page);
		}
		else{
			$jum_data=0;
			$jum_halaman=0;
			//die_error('Cannot Load mobil',__FILE__,__LINE__,$sql);
		}
		
		$jum_halaman_for_idx=($jum_halaman>0)?$jum_halaman-1:0;
		
		$idx_next=($idx_page+1<$jum_halaman_for_idx)?$idx_page+1:$jum_halaman_for_idx;
		$idx_prev=($idx_page-1>0)?$idx_page-1:0;
		
		$page_first 	=($idx_page!=$idx_prev)?"<a href='".append_sid("$nama_file?page=0&cari=$cari")."'>First</a>&nbsp;&nbsp;&nbsp;":"";
		$page_last 		=($idx_page!=$idx_next)?"&nbsp;&nbsp;&nbsp;<a href='".append_sid("$nama_file?page=$jum_halaman_for_idx&cari=$cari")."'>Last</a>":"";
		
		$page_next	= ($idx_page!=$idx_next)?
			"<a href='".append_sid("$nama_file.".$phpEx."?page=$idx_next&cari=$cari")."'>Next</a>":"";
			
		$page_prev 	=($idx_page!=$idx_prev)?
			"<a href='".append_sid("$nama_file.".$phpEx."?page=$idx_prev&cari=$cari")."'>Prev</a>":"";
		
		$section_idx_page="";
		
		$start_iterasi	=(floor(($idx_page)/$page_per_section)*$page_per_section)+1;
		
		$max_iterasi		=($start_iterasi+$page_per_section-1<$jum_halaman)?$start_iterasi+$page_per_section-1:$jum_halaman;
		
		if($start_iterasi>1){
			$temp_idx=$start_iterasi-2;
			$extend_section_prev	="<a href='".append_sid("$nama_file?page=$temp_idx&cari=$cari")."'>...</a>";
		}
		
		$extend_section_next=($max_iterasi<$jum_halaman)?
				"<a href='".append_sid("$nama_file?page=$max_iterasi&cari=$cari")."'>...</a>":"";
		
		for($idx=$start_iterasi;$idx<=$max_iterasi;$idx++){
			$temp_idx=$idx-1;
			$section_idx_page .=($temp_idx!=$idx_page)?"<a href='".append_sid("$nama_file?page=$temp_idx&cari=$cari")."'>&nbsp;<u>$idx</u>&nbsp;</a>":"&nbsp;[$idx]&nbsp;";
		}
		
		$section_idx_page =$extend_section_prev.$section_idx_page.$extend_section_next;
		
		$paging= "<a>Halaman:</a>&nbsp;".$page_first.$page_prev."&nbsp;&nbsp;".$section_idx_page."&nbsp;&nbsp;".$page_next.$page_last;
		
		$idx_awal_record	= $idx_page*$view_per_page;
		
		return $paging;
}*/

function pagingData($idx_page,$field_id,$nama_tabel,$parameter,$kondisi,$nama_file,$view_per_page,$page_per_section,$idx_awal_record){
	global $db;
	global $idx_awal_record;
	
	//menghitung jumlah data
		$idx_page = ($idx_page!='')?$idx_page:0;
		$kondisi	=($kondisi!='')?$kondisi:'';
		
		$sql = "SELECT COUNT(DISTINCT($field_id)) AS JumData FROM $nama_tabel $kondisi";
		
		if ($result = $db->sql_query($sql)){
			$my_data = $db->sql_fetchrow($result);
			$jum_data=$my_data['JumData'];
			
			$jum_halaman=ceil($jum_data/$view_per_page);
		}
		else{
			$jum_data=0;
			$jum_halaman=0;
			//die_error('Cannot Load mobil',__FILE__,__LINE__,$sql);
		}
		
		$jum_halaman_for_idx=($jum_halaman>0)?$jum_halaman-1:0;
		
		$idx_next=($idx_page+1<$jum_halaman_for_idx)?$idx_page+1:$jum_halaman_for_idx;
		$idx_prev=($idx_page-1>0)?$idx_page-1:0;
		
		$page_first 	=($idx_page!=$idx_prev)?"<a href='".append_sid("$nama_file?page=0".$parameter)."'>First</a>&nbsp;&nbsp;&nbsp;":"";
		$page_last 		=($idx_page!=$idx_next)?"&nbsp;&nbsp;&nbsp;<a href='".append_sid("$nama_file?page=$jum_halaman_for_idx".$parameter)."'>Last</a>":"";
		
		$page_next	= ($idx_page!=$idx_next)?
			"<a href='".append_sid("$nama_file?page=$idx_next".$parameter)."'>Next</a>":"";
			
		$page_prev 	=($idx_page!=$idx_prev)?
			"<a href='".append_sid("$nama_file?page=$idx_prev".$parameter)."'>Prev</a>":"";
		
		$section_idx_page="";
		
		$start_iterasi	=(floor(($idx_page)/$page_per_section)*$page_per_section)+1;
		
		$max_iterasi		=($start_iterasi+$page_per_section-1<$jum_halaman)?$start_iterasi+$page_per_section-1:$jum_halaman;
		
		if($start_iterasi>1){
			$temp_idx=$start_iterasi-2;
			$extend_section_prev	="<a href='".append_sid("$nama_file?page=$temp_idx".$parameter)."'>...</a>";
		}
		
		$extend_section_next=($max_iterasi<$jum_halaman)?
				"<a href='".append_sid("$nama_file?page=$max_iterasi".$parameter)."'>...</a>":"";
		
		for($idx=$start_iterasi;$idx<=$max_iterasi;$idx++){
			$temp_idx=$idx-1;
			$section_idx_page .=($temp_idx!=$idx_page)?"<a href='".append_sid("$nama_file?page=$temp_idx".$parameter)."'>&nbsp;<u>$idx</u>&nbsp;</a>":"&nbsp;[$idx]&nbsp;";
		}
		
		$section_idx_page =$extend_section_prev.$section_idx_page.$extend_section_next;
		
		$paging= "<a>Jumlah Data: </a><b>".number_format($jum_data,0,",",".")."</b> | <a>Halaman:</a>&nbsp;".$page_first.$page_prev."&nbsp;&nbsp;".$section_idx_page."&nbsp;&nbsp;".$page_next.$page_last;
		
		$idx_awal_record	= $idx_page*$view_per_page;
		
		return $paging;
}

// Memparse Teks Biar ga Pake karakter ilegal yang bisa dipake SQL-Inject
function adp_clean($username)
{
  $username = substr(htmlspecialchars(str_replace("\'", "'", trim($username))), 0, 25);
  $username = adp_rtrim($username, "\\");
  $username = str_replace("'", "\'", $username);
  return $username;
}

// Right Trim
function adp_rtrim($str, $charlist = false)
{
	if ($charlist === false)
	{
		return rtrim($str);
	}
	$php_version = explode('.', PHP_VERSION);
	if ((int) $php_version[0] < 4 || ((int) $php_version[0] == 4 && (int) $php_version[1] < 1))
	{
		while ($str{strlen($str)-1} == $charlist)
		{
			$str = substr($str, 0, strlen($str)-1);
		}
	}
	else
	{
		$str = rtrim($str, $charlist);
	}
	return $str;
}

// Inisialisasi userdata
function init_userprefs($userdata)
{
	global $template, $adp_root_path;
	global $nav_links;
	global $config;
/*	Navigasi Untuk Menunya Mozilla
	// navigation links
	$nav_links['top'] = array (
		'url' => append_sid($adp_root_path . 'index.' . $phpEx),
		'title' => sprintf($lang['Forum_Index'], $board_config['sitename'])
	);
	$nav_links['search'] = array (
		'url' => append_sid($adp_root_path . 'search.' . $phpEx),
		'title' => $lang['Search']
	);
	$nav_links['help'] = array (
		'url' => append_sid($adp_root_path . 'faq.' . $phpEx),
		'title' => $lang['FAQ']
	);
	$nav_links['author'] = array (
		'url' => append_sid($adp_root_path . 'memberlist.' . $phpEx),
		'title' => $lang['Memberlist']
	);
*/	
	$theme = setup_style($config['template']); // siapin template 
	return;
}

// nyiapin template
function setup_style($style)
{
	global $template, $adp_root_path;
	// lokasi template kita
	$template_path = 'templates/' ;
	// nama templatenya
	$template_name = $style;
	// bikin objek template
	$template = new Template($adp_root_path . $template_path . $template_name);
}

// encoding ip
function encode_ip($dotquad_ip)
{
	$ip_sep = explode('.', $dotquad_ip);
	return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
}

// decoding ip
function decode_ip($int_ip)
{
	$hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
	return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
}

// bikin link buat paging
function generate_pagination($base_url, $num_items, $per_page, $start_item, $add_prevnext_text = TRUE)
{
	global $lang;
	$total_pages = ceil($num_items/$per_page);
	if ( $total_pages == 1 )
	{
		return '';
	}
	$on_page = floor($start_item / $per_page) + 1;
	$page_string = '';
	if ( $total_pages > 10 )
	{
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;

		for($i = 1; $i < $init_page_max + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
			if ( $i <  $init_page_max )
			{
				$page_string .= ", ";
			}
		}

		if ( $total_pages > 3 )
		{
			if ( $on_page > 1  && $on_page < $total_pages )
			{
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';

				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++)
				{
					$page_string .= ($i == $on_page) ? '<b>' . $i . '</b>' : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
					if ( $i <  $init_page_max + 1 )
					{
						$page_string .= ', ';
					}
				}

				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			}
			else
			{
				$page_string .= ' ... ';
			}

			for($i = $total_pages - 2; $i < $total_pages + 1; $i++)
			{
				$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>'  : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
				if( $i <  $total_pages )
				{
					$page_string .= ", ";
				}
			}
		}
	}
	else
	{
		for($i = 1; $i < $total_pages + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
			if ( $i <  $total_pages )
			{
				$page_string .= ', ';
			}
		}
	}

	if ( $add_prevnext_text )
	{
		if ( $on_page > 1 )
		{
			$page_string = ' <a href="' . append_sid($base_url . "&amp;start=" . ( ( $on_page - 2 ) * $per_page ) ) . '">' . $lang['Previous'] . '</a>&nbsp;&nbsp;' . $page_string;
		}

		if ( $on_page < $total_pages )
		{
			$page_string .= '&nbsp;&nbsp;<a href="' . append_sid($base_url . "&amp;start=" . ( $on_page * $per_page ) ) . '">' . $lang['Next'] . '</a>';
		}

	}

	$page_string = $lang['Goto_page'] . ' ' . $page_string;
	return $page_string;
}

// pregmatic quotation
function adp_preg_quote($str, $delimiter)
{
	$text = preg_quote($str);
	$text = str_replace($delimiter, '\\' . $delimiter, $text);
	return $text;
}

// realpath
function adp_realpath($path)
{
	global $adp_root_path;
	return (!@function_exists('realpath') || !@realpath($adp_root_path . 'includes/functions.php')) ? $path : @realpath($path);
}

// pesan error
function die_error($message, $line='', $file ='',$sql='')
{
  global $template,$config;
  global $db;
  global $phpEx;
  global $adp_root_path;
  
  if (!defined('HEADER_INC'))
  {
	if ( empty($template) )
	{
		$template = new Template($adp_root_path . 'templates/' . $config['template']);
	}
	if ( empty($theme) )
	{
		$theme = setup_style($config['template']);
	}
	include($adp_root_path . 'includes/page_header.'.$phpEx);
  }   
     
  $server_protocol = $config['protocol'];
  $server_name     = $config['name'];
  $back = $server_protocol.$server_name.'/'.$config['script'].'/';
  
  $template->set_filenames(array('error' => 'error_body.tpl')); 
  $template->assign_vars(
    array(
    'MESSAGE_TITLE' => 'ERROR',
    'ERROR_MESSAGE' => '{'.$file .' :: '. $line .'}<br /><br />' . $message .'<br /><p class="genmed">'. $sql.'</p>',
    'U_BACK' => $back
    )
    );
  $template->pparse('error');
  if ( !defined('IN_ADMIN') )
  {
	include($adp_root_path . 'includes/page_tail.'.$phpEx);
  }
  else
  {
	include($adp_root_path . 'admin/page_footer_admin.'.$phpEx);
  } 
  exit;
}

// pesan biasa yang menggunakan satu click here
function die_message($message, $back1='',$back2='')
{
  global $template,$config;
  global $db;
  global $phpEx;
  global $adp_root_path;

  
  if (!defined('HEADER_INC'))
  {
	if ( empty($template) )
	{
		$template = new Template($adp_root_path . 'templates/' . $config['template']);
	}
	if ( empty($theme) )
	{
		$theme = setup_style($config['template']);
	}
	include($adp_root_path . 'includes/page_header.'.$phpEx);
  } 
  
  $template->set_filenames(array('error' => 'message_body.tpl')); 
  $template->assign_vars(
    array(
    'MESSAGE_TITLE'=>'Message',
    'MESSAGE_CONTENT'=>$message,
    'U_BACK1' => $back1,
    'U_BACK2' => $back2,
    )
    );
  $template->pparse('error');
  
  if ( !defined('IN_ADMIN') )
  {
	include($adp_root_path . 'includes/page_tail.'.$phpEx);
  }
  else
  {
	include($adp_root_path . 'admin/page_footer_admin.'.$phpEx);
  }   
  exit;
}


// redirection ke lokasi lain
function redirect($url)
{
	global $db,$config;
	if (!empty($db))
	{
		$db->sql_close(); // kalo mo redirect, close koneksi database kita
	}
	if (strstr(urldecode($url), "\n") || strstr(urldecode($url), "\r"))
	{	    
		die('Tried to redirect to potentially insecure url.'); // cek kalo ada usaha jahat ;p
	}
	$server_protocol = $config['protocol'];
	$server_name = ($config['script']!="")?$config['name'] . '/' . $config['script']:".";
	$server_port = 80;
	$script_name = '';	
	$script_name = ($script_name == '') ? $script_name : './' . $script_name;
	// url yang mo diloncatin ;p
	$url = preg_replace('#^\/?(.*?)\/?$#', '/\1', trim($url));
	if (@preg_match('/Microsoft|WebSTAR|Xitami/', getenv('SERVER_SOFTWARE')))
	{
		header('Refresh: 0; URL=' . $server_protocol . $server_name . $script_name . $url);
		echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><meta http-equiv="refresh" content="0; url=' . $server_protocol . $server_name . $script_name . $url . '"><title>Redirect</title></head><body><div align="center">If your browser does not support meta redirection please click <a href="' . $server_protocol . $server_name . $server_port . $script_name . $url . '">HERE</a> to be redirected</div></body></html>';
		exit;
	}	
	header('Location: ' . $server_protocol . $server_name . $script_name . $url);
	exit;
}

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

?>