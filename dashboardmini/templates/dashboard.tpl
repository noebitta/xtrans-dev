	
<form name="pilih_periode" action="{U_ACTION}" method="post">
<div align='left'>
	{DEBUG}
	<h3>Periode:</h3>
	<select name='tgl_awal'>{OPT_TANGGAL_AWAL}</select>&nbsp;<select name='bln_awal'>{OPT_BULAN_AWAL}</select>&nbsp;<select name='thn_awal'>{OPT_TAHUN_AWAL}</select>
	&nbsp;s/d&nbsp;<br>
	<select name='tgl_akhir'>{OPT_TANGGAL_AKHIR}</select>&nbsp;<select name='bln_akhir'>{OPT_BULAN_AKHIR}</select>&nbsp;<select name='thn_akhir'>{OPT_TAHUN_AKHIR}</select>
	&nbsp;
	<input type='submit' value=' Update ' />
	</div>
</form>
<br>
<!-- JAKARTA -->
<div class='menuutama'>&nbsp;JAKARTA</div>
<table width='230'>
	<tr><td colspan='3'><b>Penumpang</b></td></tr>
	<tr><td width='100'>Total Umum</td><td width='1'>:</td><td align='right'>{JUM_TIKET_U_JAKARTA}</td><tr>
	<tr><td width='100'>Total Member</td><td width='1'>:</td><td align='right'>{JUM_TIKET_M_JAKARTA}</td><tr>
	<tr><td width='100'>Total Khusus</td><td width='1'>:</td><td align='right'>{JUM_TIKET_K_JAKARTA}</td><tr>
	<tr><td width='100'>Total Group</td><td width='1'>:</td><td align='right'>{JUM_TIKET_G_JAKARTA}</td><tr>
	<tr><td width='100'>Total Karyawan</td><td width='1'>:</td><td align='right'>{JUM_TIKET_KK_JAKARTA}</td><tr>
	<tr><td width='100'>Total</td><td width='1'>:</td><td align='right'>{JUM_TIKET_JAKARTA}</td><tr>
	<tr><td width='100'>Perolehan</td><td width='1'>:</td><td align='right'>{OMZET_TIKET_JAKARTA}</td></tr>
	<tr><td width='100'>Book Batal</td><td width='1'>:</td><td align='right'>{JUM_BOOK_BATAL_JAKARTA}</td></tr>
	<tr><td width='100'>Load Faktor</td><td width='1'>:</td><td align='right'>{LOAD_FAKTOR_JAKARTA}</td></tr>
	<tr><td colspan='3'><br><b>Keberangkatan</b></td></tr>
	<tr><td width='100'>Jumlah</td><td width='1'>:</td><td align='right'>{JUM_KEBERANGKATAN_JAKARTA}</td></tr>
	</tr>
</table>
<br>
<!-- BANDUNG -->
<div class='menuutama'>&nbsp;BANDUNG</div>
<table width='230'>
	<tr><td colspan='3'><b>Penumpang</b></td></tr>
	<tr><td width='100'>Total Umum</td><td width='1'>:</td><td align='right'>{JUM_TIKET_U_BANDUNG}</td><tr>
	<tr><td width='100'>Total Member</td><td width='1'>:</td><td align='right'>{JUM_TIKET_M_BANDUNG}</td><tr>
	<tr><td width='100'>Total Khusus</td><td width='1'>:</td><td align='right'>{JUM_TIKET_K_BANDUNG}</td><tr>
	<tr><td width='100'>Total Group</td><td width='1'>:</td><td align='right'>{JUM_TIKET_G_BANDUNG}</td><tr>
	<tr><td width='100'>Total Karyawan</td><td width='1'>:</td><td align='right'>{JUM_TIKET_KK_BANDUNG}</td><tr>
	<tr><td width='100'>Total</td><td width='1'>:</td><td align='right'>{JUM_TIKET_BANDUNG}</td><tr>
	<tr><td width='100'>Perolehan</td><td width='1'>:</td><td align='right'>{OMZET_TIKET_BANDUNG}</td></tr>
	<tr><td width='100'>Book Batal</td><td width='1'>:</td><td align='right'>{JUM_BOOK_BATAL_BANDUNG}</td></tr>
	<tr><td width='100'>Load Faktor</td><td width='1'>:</td><td align='right'>{LOAD_FAKTOR_BANDUNG}</td></tr>
	<tr><td colspan='3'><br><b>Keberangkatan</b></td></tr>
	<tr><td width='100'>Jumlah</td><td width='1'>:</td><td align='right'>{JUM_KEBERANGKATAN_BANDUNG}</td></tr>
	</tr>
</table>
<br>
<!-- JURUSAN TERAMAI -->
<div class='menuutama'>&nbsp;5 JURUSAN TERAMAI</div>
<table width='300'>
	<tr>
		<th width=30>No</th>
		<th width=100>Asal</a></th>
		<th width=100>Tujuan</a></th>
		<th width=70>Tiket</th>
	</tr>
	<!-- BEGIN ROW -->
	<tr class="{ROW.odd}">
		<td><div align="right">{ROW.no}</div></td>
		<td><div align="left">{ROW.asal}</div></td>
		<td><div align="left">{ROW.tujuan}</div></td>
		<td><div align="right">{ROW.tiket}</div></td>
	</tr>  
 <!-- END ROW -->
 {NO_DATA}
</table>
<br>
<!-- JURUSAN TERSEPI -->
<div class='menuutama'>&nbsp;5 JURUSAN TERSEPI</div>
<table width='300'>
	<tr>
		<th width=30>No</th>
		<th width=100>Asal</a></th>
		<th width=100>Tujuan</a></th>
		<th width=70>Tiket</th>
	</tr>
	<!-- BEGIN ROW_SEPI -->
	<tr class="{ROW_SEPI.odd}">
		<td><div align="right">{ROW_SEPI.no}</div></td>
		<td><div align="left">{ROW_SEPI.asal}</div></td>
		<td><div align="left">{ROW_SEPI.tujuan}</div></td>
		<td><div align="right">{ROW_SEPI.tiket}</div></td>
	</tr>  
 <!-- END ROW_SEPI -->
 {NO_DATA}
</table>
<br>
<!-- CABANG TERAMAI -->
<div class='menuutama'>&nbsp;5 CABANG TERAMAI</div>
<table width='300'>
	<tr>
		<th width=30>No</th>
		<th width=200>Cabang</a></th>
		<th width=70>Tiket</th>
	</tr>
	<!-- BEGIN ROW_CABANG_RAMAI -->
	<tr class="{ROW_CABANG_RAMAI.odd}">
		<td><div align="right">{ROW_CABANG_RAMAI.no}</div></td>
		<td><div align="left">{ROW_CABANG_RAMAI.cabang}</div></td>
		<td><div align="right">{ROW_CABANG_RAMAI.tiket}</div></td>
	</tr>  
 <!-- END ROW_CABANG_RAMAI -->
 {NO_DATA}
</table>
<br>
<!-- CABANG TERSEPIT -->
<div class='menuutama'>&nbsp;5 CABANG TERSEPI</div>
<table width='300'>
	<tr>
		<th width=30>No</th>
		<th width=200>Cabang</a></th>
		<th width=70>Tiket</th>
	</tr>
	<!-- BEGIN ROW_CABANG_SEPI -->
	<tr class="{ROW_CABANG_SEPI.odd}">
		<td><div align="right">{ROW_CABANG_SEPI.no}</div></td>
		<td><div align="left">{ROW_CABANG_SEPI.cabang}</div></td>
		<td><div align="right">{ROW_CABANG_SEPI.tiket}</div></td>
	</tr>  
 <!-- END ROW_CABANG_SEPI -->
 {NO_DATA}
</table>
<br>
						