<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];

//parameter tanggal awal
$tgl_awal  = isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal'];
$bln_awal  = isset($HTTP_GET_VARS['bln_awal'])? $HTTP_GET_VARS['bln_awal'] : $HTTP_POST_VARS['bln_awal'];
$thn_awal  = isset($HTTP_GET_VARS['thn_awal'])? $HTTP_GET_VARS['thn_awal'] : $HTTP_POST_VARS['thn_awal'];

//parameter tanggal akhir
$tgl_akhir  = isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir'];
$bln_akhir  = isset($HTTP_GET_VARS['bln_akhir'])? $HTTP_GET_VARS['bln_akhir'] : $HTTP_POST_VARS['bln_akhir'];
$thn_akhir  = isset($HTTP_GET_VARS['thn_akhir'])? $HTTP_GET_VARS['thn_akhir'] : $HTTP_POST_VARS['thn_akhir'];

$tgl_periode_awal		= $thn_awal."-".$bln_awal."-".$tgl_awal;
$tgl_periode_akhir	= $thn_akhir."-".$bln_akhir."-".$tgl_akhir;

// LIST

//MENGAMBIL CABANG-CABANG JAKARTA
$sql=
	'SELECT 
		GROUP_CONCAT(CONCAT("'."'".'",CAST(KodeCabang AS CHAR),"'."'".'") SEPARATOR ",") AS KodeCabang
	FROM tbl_md_cabang
	WHERE Kota="JAKARTA"
	GROUP BY Kota';
	
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$list_kode_cabang	= $row[0];

//MENGAMBIL ID JURUSAN DARI JAKARTA
$sql=
	'SELECT 
		GROUP_CONCAT(CONCAT("'."'".'",CAST(IdJurusan AS CHAR),"'."'".'") SEPARATOR ",") AS IdJurusan
	FROM tbl_md_jurusan
	WHERE KodeCabangAsal IN ('.$list_kode_cabang.')';
	
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$list_id_jurusan	= $row[0];

//DATA  TIKET BATAL
$sql	= 
	"SELECT IS_NULL(COUNT(NoTiket),0) AS TotalTiket
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND (CetakTiket=0 OR CetakTiket='' OR FlagBatal=1) AND NoSPJ!=''
		AND IdJurusan IN ($list_id_jurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$jumlah_tiket_batal_jakarta	= $row[0];

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		IS_NULL(COUNT(IF(JenisPenumpang='U' OR JenisPenumpang='',NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M',NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K',NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK',NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G',NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(SubTotal),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND CetakTiket=1 AND FlagBatal!=1 
		AND IdJurusan IN ($list_id_jurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$total_penumpang_u_jakarta	= $row['TotalPenumpangU'];
$total_penumpang_m_jakarta	= $row['TotalPenumpangM'];
$total_penumpang_k_jakarta	= $row['TotalPenumpangK'];
$total_penumpang_g_jakarta	= $row['TotalPenumpangG'];
$total_penumpang_kk_jakarta	= $row['TotalPenumpangKK'];
$total_tiket_jakarta				= $row['TotalTiket'];
$total_omzet_jakarta				= $row['TotalPenjualanTiket'];
$load_faktor_jakarta				= $row['TotalBerangkat']>0?$row['TotalTiket']/$row['TotalBerangkat']:0;
$total_keberangkatan_jakarta= $row['TotalBerangkat'];


//BANDUNG
//MENGAMBIL CABANG-CABANG BANDUNG
$sql=
	'SELECT 
		GROUP_CONCAT(CONCAT("'."'".'",CAST(KodeCabang AS CHAR),"'."'".'") SEPARATOR ",") AS KodeCabang
	FROM tbl_md_cabang
	WHERE Kota="BANDUNG"
	GROUP BY Kota';
	
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$list_kode_cabang	= $row[0];

//MENGAMBIL ID JURUSAN DARI BANDUNG
$sql=
	'SELECT 
		GROUP_CONCAT(CONCAT("'."'".'",CAST(IdJurusan AS CHAR),"'."'".'") SEPARATOR ",") AS IdJurusan
	FROM tbl_md_jurusan
	WHERE KodeCabangAsal IN ('.$list_kode_cabang.')';
	
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$list_id_jurusan	= $row[0];

//DATA  TIKET BATAL
$sql	= 
	"SELECT IS_NULL(COUNT(NoTiket),0) AS TotalTiket
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND (CetakTiket=0 OR CetakTiket='' OR FlagBatal=1) AND NoSPJ!=''
		AND IdJurusan IN ($list_id_jurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$jumlah_tiket_batal_bandung	= $row[0];

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		IS_NULL(COUNT(IF(JenisPenumpang='U' OR JenisPenumpang='',NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M',NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K',NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK',NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G',NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(SubTotal),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND CetakTiket=1 AND FlagBatal!=1 
		AND IdJurusan IN ($list_id_jurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$total_penumpang_u_bandung	= $row['TotalPenumpangU'];
$total_penumpang_m_bandung	= $row['TotalPenumpangM'];
$total_penumpang_k_bandung	= $row['TotalPenumpangK'];
$total_penumpang_g_bandung	= $row['TotalPenumpangG'];
$total_penumpang_kk_bandung	= $row['TotalPenumpangKK'];
$total_tiket_bandung				= $row['TotalTiket'];
$total_omzet_bandung				= $row['TotalPenjualanTiket'];
$load_faktor_bandung				= $row['TotalBerangkat']>0?$row['TotalTiket']/$row['TotalBerangkat']:0;
$total_keberangkatan_bandung= $row['TotalBerangkat'];

//5 JURUSAN TERAMAI
$sql	= 
	"SELECT
		IdJurusan,IS_NULL(COUNT(NoTiket),0) AS TotalTiket
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND CetakTiket=1 AND FlagBatal!=1 
	GROUP BY IdJurusan
  ORDER BY TotalTiket DESC
  LIMIT 0,5";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$idx	= 0;
$list_id_jurusan	= "";
while($row = $db->sql_fetchrow($result)){
	$data_jurusan_ramai[$idx]['IdJurusan']	= $row[0];
	$data_jurusan_ramai[$idx]['TotalTiket']	= $row[1];
	$list_id_jurusan	.="'".$row[0]."',";
	$idx++;
}

//5 JURUSAN TERSEPI
$sql	= 
	"SELECT
		IdJurusan,IS_NULL(COUNT(NoTiket),0) AS TotalTiket
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND CetakTiket=1 AND FlagBatal!=1 
	GROUP BY IdJurusan
  ORDER BY TotalTiket ASC
  LIMIT 0,5";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$idx	= 0;

while($row = $db->sql_fetchrow($result)){
	$data_jurusan_sepi[$idx]['IdJurusan']	= $row[0];
	$data_jurusan_sepi[$idx]['TotalTiket']	= $row[1];
	$list_id_jurusan	.="'".$row[0]."',";
	$idx++;
}

$list_id_jurusan	= substr($list_id_jurusan,0,-1);

//ambil asal dan tujuan
if($list_id_jurusan!=''){
	$sql	= 
		"SELECT
			IdJurusan, KodeCabangAsal,KodeCabangTujuan
		FROM tbl_md_jurusan
		WHERE IdJurusan IN($list_id_jurusan)";
			
	if (!$result = $db->sql_query($sql)){
		echo("Err: ".__LINE__);exit;
	}
	
	while($row = $db->sql_fetchrow($result)){
		$data_jurusan[$row[0]]['KodeCabangAsal']	= $row[1];
		$data_jurusan[$row[0]]['KodeCabangTujuan']	= $row[2];
	}
}

//5 CABANG TERAMAI
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)
	ORDER BY TotalTiket DESC
	LIMIT 0,5";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$idx	= 0;

while($row = $db->sql_fetchrow($result)){
	$data_cabang_ramai[$idx]['KodeCabang']	= $row[0];
	$data_cabang_ramai[$idx]['TotalTiket']	= $row[1];
	$list_cabang	.="'".$row[0]."',";
	$idx++;
}

//5 CABANG TERSEPI
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tgl_periode_awal' AND '$tgl_periode_akhir') 
		AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)
	ORDER BY TotalTiket ASC
	LIMIT 0,5";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

$idx	= 0;

while($row = $db->sql_fetchrow($result)){
	$data_cabang_sepi[$idx]['KodeCabang']	= $row[0];
	$data_cabang_sepi[$idx]['TotalTiket']	= $row[1];
	$list_cabang	.="'".$row[0]."',";
	$idx++;
}

$list_cabang	= substr($list_cabang,0,-1);

for($idx=0;$idx<5;$idx++){ 
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$template->assign_block_vars(
		'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'asal'=>$data_jurusan[$data_jurusan_ramai[$idx]['IdJurusan']]['KodeCabangAsal'],
				'tujuan'=>$data_jurusan[$data_jurusan_ramai[$idx]['IdJurusan']]['KodeCabangTujuan'],
				'tiket'=>number_format($data_jurusan_ramai[$idx]['TotalTiket'],0,".",",")
			)
	);
	
	$template->assign_block_vars(
		'ROW_SEPI',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'asal'=>$data_jurusan[$data_jurusan_sepi[$idx]['IdJurusan']]['KodeCabangAsal'],
				'tujuan'=>$data_jurusan[$data_jurusan_sepi[$idx]['IdJurusan']]['KodeCabangTujuan'],
				'tiket'=>number_format($data_jurusan_sepi[$idx]['TotalTiket'],0,".",",")
			)
	);
	
	$template->assign_block_vars(
		'ROW_CABANG_RAMAI',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'cabang'=>$data_cabang_ramai[$idx]['KodeCabang'],
				'tiket'=>number_format($data_cabang_ramai[$idx]['TotalTiket'],0,".",",")
			)
	);
	
	$template->assign_block_vars(
		'ROW_CABANG_SEPI',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'cabang'=>$data_cabang_sepi[$idx]['KodeCabang'],
				'tiket'=>number_format($data_cabang_sepi[$idx]['TotalTiket'],0,".",",")
			)
	);
}

//WRITE CONTENT
$template->set_filenames(array('body' => 'dashboard.tpl')); 

$template->assign_vars(array(
	'U_ACTION'					=> append_sid('dashboard.'.$phpEx),
	'OPT_TANGGAL_AWAL'	=> setComboTanggal($tgl_awal),
	'OPT_BULAN_AWAL'		=> setComboBulan($bln_awal),
	'OPT_TAHUN_AWAL'		=> setComboTahun($thn_awal),
	'OPT_TANGGAL_AKHIR'	=> setComboTanggal($tgl_akhir),
	'OPT_BULAN_AKHIR'		=> setComboBulan($bln_akhir),
	'OPT_TAHUN_AKHIR'		=> setComboTahun($thn_akhir),
	'JUM_TIKET_U_JAKARTA'		=> number_format($total_penumpang_u_jakarta,0,".",","),
	'JUM_TIKET_M_JAKARTA'		=> number_format($total_penumpang_m_jakarta,0,".",","),
	'JUM_TIKET_K_JAKARTA'		=> number_format($total_penumpang_k_jakarta,0,".",","),
	'JUM_TIKET_G_JAKARTA'		=> number_format($total_penumpang_g_jakarta,0,".",","),
	'JUM_TIKET_KK_JAKARTA'	=> number_format($total_penumpang_kk_jakarta,0,".",","),
	'JUM_TIKET_JAKARTA'			=> number_format($total_tiket_jakarta,0,".",","),
	'OMZET_TIKET_JAKARTA'		=> "Rp.".number_format($total_omzet_jakarta,0,".",","),
	'JUM_BOOK_BATAL_JAKARTA'=> number_format($jumlah_tiket_batal_jakarta,0,".",","),
	'LOAD_FAKTOR_JAKARTA'		=> number_format($load_faktor_jakarta,0,".",",")."/Berangkat",
	'JUM_KEBERANGKATAN_JAKARTA'		=> number_format($total_keberangkatan_jakarta,0,".",","),
	'JUM_TIKET_U_BANDUNG'		=> number_format($total_penumpang_u_bandung,0,".",","),
	'JUM_TIKET_M_BANDUNG'		=> number_format($total_penumpang_m_bandung,0,".",","),
	'JUM_TIKET_K_BANDUNG'		=> number_format($total_penumpang_k_bandung,0,".",","),
	'JUM_TIKET_G_BANDUNG'		=> number_format($total_penumpang_g_bandung,0,".",","),
	'JUM_TIKET_KK_BANDUNG'	=> number_format($total_penumpang_kk_bandung,0,".",","),
	'JUM_TIKET_BANDUNG'			=> number_format($total_tiket_bandung,0,".",","),
	'OMZET_TIKET_BANDUNG'		=> "Rp.".number_format($total_omzet_bandung,0,".",","),
	'JUM_BOOK_BATAL_BANDUNG'=> number_format($jumlah_tiket_batal_bandung,0,".",","),
	'LOAD_FAKTOR_BANDUNG'		=> number_format($load_faktor_bandung,0,".",",")."/Berangkat",
	'JUM_KEBERANGKATAN_BANDUNG'		=> number_format($total_keberangkatan_bandung,0,".",",")
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>