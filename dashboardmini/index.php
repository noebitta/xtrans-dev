<?php
//
// Cipaganti Travel Index Page
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = "./";

//detecting browser
/*$mobile_browser = '0';

if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
$mobile_browser++;
}

if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
$mobile_browser++;
}

$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
$mobile_agents = array(
'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
'wapr','webc','winw','winw','xda ','xda-');

if (in_array($mobile_ua,$mobile_agents)) {
$mobile_browser++;
}

if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0) {
$mobile_browser++;
}

if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0) {
$mobile_browser = 0;
}

if ($mobile_browser > 0) {
	// do something
	//echo 'Mobile Browser';
	
}
else {
	//echo 'PC Browser';
	//header( 'Location: http://www.hemantha.com/index2.html' );
	// do something else
	//echo("Aplikasi ini hanya dapat diakses melalui mobile");
	//exit;
}
*/

include($adp_root_path . 'common.php');
	
// SESSION 
$userdata = session_pagestart($user_ip,777);  // note .. kode 777 itu bisa diganti sama kode kamu sendiri (harus unik!!!)
init_userprefs($userdata);

if( $userdata['session_logged_in'] )
{  
  //redirect(append_sid('main.'.$phpEx),true);  // user aktif ga usa login, langsung redirect ke main
}

// HEADER
include($adp_root_path . 'includes/page_header.php');

$images_random	= rand(1,10);

// TEMPLATE
$template->set_filenames(array('body' => 'main_body.tpl')); 
$template->assign_vars(array(
	'U_LOGIN'						=>append_sid('auth.php'),
	'IMAGE_BACKGROUND'	=>"main".$images_random.".png"
	));

// PARSE
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>