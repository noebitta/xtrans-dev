<?php
//
// AUTENTIFIKASI (login n logout)
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';

include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Login n Out
init_userprefs($userdata);

// session id check
if (!empty($HTTP_POST_VARS['sid']) || !empty($HTTP_GET_VARS['sid']))
{
	$sid = (!empty($HTTP_POST_VARS['sid'])) ? $HTTP_POST_VARS['sid'] : $HTTP_GET_VARS['sid'];
}
else
{
	$sid = '';
}

$mode  = $HTTP_GET_VARS['mode'];
if ($mode=='out')
{
	 // LOGOUT
	 if( $userdata['session_logged_in'] )
	 {
		//mencatat log logout  user
		//$sql ="CALL sp_log_user_logout($userdata[user_id])";
		
		
		$sql =
			"SELECT id_log
			FROM tbl_log_user
			WHERE
				user_id='$userdata[user_id]'
				AND waktu_logout IS NULL
			ORDER BY waktu_login DESC LIMIT 0,1";
	 
		if ( (!$result = $db->sql_query($sql))){    
			die_error('Error in registering logout'.$sql);
		}
		
		$row=$db->sql_fetchrow($result);
		
		$id_log	= $row['id_log'];
		
	  $sql	= 
			"UPDATE tbl_log_user
			SET waktu_logout=NOW()
			WHERE id_log='$id_log'";
		
		if ( (!$result = $db->sql_query($sql))){    
			die_error('Error in registering logout'.$sql);
		}
	  
		// terminate the session
		session_end($userdata['session_id'], $userdata['user_id']);
	 }
	 // kembali ke halaman utama
	 redirect(append_sid("index.$phpEx", true));
}
else
{ 
	 // LOGIN
	 /*if( $userdata['session_logged_in'] )
	 { 
		  // pernah login dan aktif, tidak perlu login lagi.. so kita...redirect
		  //redirect(append_sid('main.'.$phpEx),true);
	 }*/
	 $uname = adp_clean($HTTP_POST_VARS['username']); // we clean
	 $upass = $HTTP_POST_VARS['password'];
	 
	 /*$sql ="SELECT user_id, username, user_password, user_active, user_level,status_online
					FROM  tbl_user
					WHERE username = '" . str_replace("\\'", "''", $uname) . "' AND berlaku>=NOW() AND user_level<=".$LEVEL_CCARE;*/
	
	 $sql ="SELECT user_id, username, user_password, user_active, user_level,status_online
					FROM  tbl_user
					WHERE username = '" . str_replace("\\'", "''", $uname) . "' AND user_level<=".$LEVEL_CCARE;
	 
	 
	 if ( (!$result = $db->sql_query($sql)) )
	 {    
		die_error('Error in obtaining userdata',__LINE__, __FILE__);
	 }
	 
	 
	 if( $row = $db->sql_fetchrow($result) )
	 { 
		// PASSWORD MATCH
		if( md5($upass) == $row['user_password'] && $row['user_active']){
		    // register ulang session
			$autologin = ( isset($HTTP_POST_VARS['autologin']) ) ? TRUE : 0;
			$session_id = session_begin($row['user_id'], $user_ip, PAGE_INDEX, FALSE, $autologin);
			$userdata['user_level']=$row[4];
			
			if($session_id)
			{
				//mencatat log login user
				$sql ="CALL sp_log_user_login($row[user_id],'$user_ip')";
	 
				if ( (!$result = $db->sql_query($sql))){    
					die_error('Error in registering login');
				}
			 
				if(in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER))){	
					$url = ( !empty($HTTP_POST_VARS['redirect']) ) ? str_replace('&amp;', '&', htmlspecialchars($HTTP_POST_VARS['redirect'])) : "main.$phpEx";
				}
				elseif($userdata['user_level']==$LEVEL_SUPERVISOR){
					$url = ( !empty($HTTP_POST_VARS['redirect']) ) ? str_replace('&amp;', '&', htmlspecialchars($HTTP_POST_VARS['redirect'])) : "pembatalan.$phpEx";
				}
				
				redirect(append_sid($url, true));
			}
			else
			{
				die_error("Couldn't start session : login");//,__LINE__, __FILE__);
			}
		}
		// PASSWORD TIDAK MATCH
		else{
			$keterangan_error = $lang['Error_login'];
			
			$redirect = ( !empty($HTTP_POST_VARS['redirect']) ) ? str_replace('&amp;', '&', htmlspecialchars($HTTP_POST_VARS['redirect'])) : '';
			$redirect = str_replace('?', '&', $redirect);
			if (strstr(urldecode($redirect), "\n") || strstr(urldecode($redirect), "\r"))
			{
				die_error('Tried to redirect to potentially insecure url.');
			}
			$template->assign_vars(array('META' => "<meta http-equiv=\"refresh\" content=\"3;url=login.$phpEx?redirect=$redirect\">"));
			$message =  $keterangan_error. '<br /><br />' . 
				  sprintf($lang['Click_return_login'], "<a href=\"login.$phpEx?redirect=$redirect\">", '</a>') . 
				  '<br /><br />' .  
				  sprintf($lang['Click_return_index'], '<a href="' . append_sid("index.$phpEx") . '">', '</a>');
			die_message($message);
		}
	 }
	 // HASIL QUERY KOSONG (NO SUCH USER)
	 else
	 {
		$redirect = ( !empty($HTTP_POST_VARS['redirect']) ) ? str_replace('&amp;', '&', htmlspecialchars($HTTP_POST_VARS['redirect'])) : "";
		$redirect = str_replace("?", "&", $redirect);
		if (strstr(urldecode($redirect), "\n") || strstr(urldecode($redirect), "\r"))
		{
			die_error('Tried to redirect to potentially insecure url.');
		}
		$template->assign_vars(array('META' => "<meta http-equiv=\"refresh\" content=\"3;url=login.$phpEx?redirect=$redirect\">"));
		$message = $lang['Error_login'] . '<br /><br />' . 
					sprintf($lang['Click_return_login'], "<a href=\"login.$phpEx?redirect=$redirect\">", '</a>') . 
		        '<br /><br />' .  
		        sprintf($lang['Click_return_index'], '<a href="' . append_sid("index.$phpEx") . '">', '</a>');
		die_message($message);
   }  
} 

?>