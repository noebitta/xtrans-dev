<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

function ambilDataDetail($kode_jadwal){
		
	/*
	ID	:007
	Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
	*/
	
	//kamus
	global $db;
	
	$sql = 
		"SELECT 
			KodeJadwal,JamBerangkat,IdJurusan,
			JumlahKursi,FlagSubJadwal,KodeJadwalUtama,FlagAktif
		FROM tbl_md_jadwal
		WHERE KodeJadwal='$kode_jadwal';";		
	
	if ($result = $db->sql_query($sql)){
		$row=$db->sql_fetchrow($result);
		
		$data_jadwal['KodeJadwal']			= $row['KodeJadwal'];
		$data_jadwal['JamBerangkat']		= $row['JamBerangkat'];
		$data_jadwal['IdJurusan']				= $row['IdJurusan'];
		$data_jadwal['JumlahKursi']			= $row['JumlahKursi'];
		$data_jadwal['FlagSubJadwal']		= $row['FlagSubJadwal'];
		$data_jadwal['KodeJadwalUtama']	= $row['KodeJadwalUtama'];
		$data_jadwal['FlagAktif']				= $row['FlagAktif'];
		
		//mengambil kodecabang asal dan tujuan
		$sql	=
		"SELECT KodeCabangAsal,KodeCabangTujuan 
		FROM tbl_md_jurusan 
		WHERE IdJurusan='$row[IdJurusan]'";
			
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$data_jadwal['Asal']		= $row['KodeCabangAsal'];
			$data_jadwal['Tujuan']	= $row['KodeCabangTujuan'];
		}                                   
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		//mengambil nama cabang asal
		$sql	=
			"SELECT Nama
		  FROM tbl_md_cabang
		  WHERE KodeCabang='$data_jadwal[Asal]'";
		
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			
			$data_jadwal['NamaAsal']	= $row['Nama'];
			
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		//mengambil nama cabang tujuan
		$sql	=
			"SELECT Nama
		  FROM tbl_md_cabang
		  WHERE KodeCabang='$data_jadwal[Tujuan]'";
		
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			
			$data_jadwal['NamaTujuan']	= $row['Nama'];
			
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $data_jadwal;
	} 
	else{
		die_error("Err: $this->ID_FILE".__LINE__);
	}
		
}//  END ambilData


function pembatalan($no_tiket){
	  
	/*
	IS	: data transaksi sudah ada  dalam database
	FS	:tiket dengan no tiket dibatalkan 
	*/
	
	//kamus
	global $db;
	global $userdata;
	
	//Mengupdate flag batal pada tgl reservasi
	$sql = "CALL sp_reservasi_batal('$no_tiket',$userdata[user_id],NOW());";
							
	if (!$db->sql_query($sql)){
		die_error("Err $this->ID_FILE".__LINE__);
	}
	
	//MENGUPDATE TBL POSISI, MEMBEBASKAN STATUS KURSI YANG DIBATALKAN
	$sql =
		"UPDATE tbl_posisi_detail 
			SET StatusKursi = 0, Nama=NULL, NoTiket=NULL,
			KodeBooking=NULL,Session=NULL,StatusBayar=0
		WHERE NoTiket='$no_tiket';";
	
	if (!$db->sql_query($sql)){
		die_error("Err $this->ID_FILE".__LINE__);
	}
	
	return true;
}
	
	if ($mode=='konfirmasi'){
		$template->set_filenames(array('body' => 'pembatalan_konfirmasi_body.tpl')); 
	
		// membatalkan reservasi
		$no_tiket = $HTTP_POST_VARS['no_tiket'];
		
		$sql = 
			"SELECT 
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,HargaTiket,Charge,SubTotal,
				Discount,Total,PetugasPenjual,
				CetakTiket,
				PetugasCetakTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,
				WaktuCetakTiket,
				WaktuMutasi,Pemutasi,JenisPenumpang,IdJurusan,
				JamBerangkat
			FROM tbl_reservasi
			WHERE NoTiket='$no_tiket' AND FlagBatal!=1";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$data_kursi	= $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		if($data_kursi['NoTiket']!=""){
			$show_tombol_batal	= "<input type='submit' value='Batalkan' />&nbsp;";
			
			$harga_tiket	=number_format($data_kursi['HargaTiket'],0,",",".");
			$discount			=number_format($data_kursi['Discount'],0,",",".");
			$sub_total		=number_format($data_kursi['SubTotal'],0,",",".");
			$total				=number_format($data_kursi['Total'],0,",",".");
			$nama_cso			=$data_kursi['NamaCSO'];
			
			$data_jadwal	= ambilDataDetail($data_kursi['KodeJadwal']);
			
			$write_data_tiket = 
				"<table border='0' class='border' width='100%' height='100%'> 
					<tr>
						<input type='hidden' id='no_tiket' value='$no_tiket' />
						<input type='hidden' id='no_kursi' value='$data_kursi[NomorKursi]' />
						<td align='center' colspan=3><h1>Kursi: $data_kursi[NomorKursi]</h1></td>
					</tr>
					<tr>
						<td>No Tiket</td><td>:</td>
						<td>$no_tiket</td>
					</tr>
					<tr>
						<td>Tgl. Berangkat</td><td>:</td>
						<td>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['TglBerangkat']))."</td>
					</tr>
					<tr>
						<td>Jurusan</td><td>:</td>
						<td>$data_jadwal[NamaAsal] - $data_jadwal[NamaTujuan] $data_jadwal[JamBerangkat]</td>
					</tr>
					<tr>
						<td>Nama</td><td>:</td>
						<td>$data_kursi[Nama]</td>
					</tr>
					<tr>
						<td valign='top'>Alamat</td><td valign='top'>:</td>
						<td>$data_kursi[Alamat]</td>
					</tr>
					<tr>
						<td>Telepon</td><td>:</td>
						<td>$data_kursi[Telp]</td>
					</tr>
					<tr>
						<td colspan=3 heigh=1 bgcolor='D0D0D0'></td>
					</tr>
					<tr>
						<td>Harga tiket</td><td>:</td>
						<td align='right'>Rp. $harga_tiket&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td>Discount</td><td>:</td>
						<td align='right'>Rp. $discount&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td colspan=3 heigh=1 bgcolor='D0D0D0'></td>
					</tr>
					<tr>
						<td>Total</td><td>:</td>
						<td align='right'><strong>Rp. $total</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td colspan=3 height=1 bgcolor='red'></td>
					</tr>
					<tr>
						<td>CSO</td><td>:</td>
						<td><strong>$nama_cso</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td>Waktu Pesan</td><td>:</td>
						<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuPesan']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				</table>";
		}
		else{
			$write_data_tiket	= "
				<div align='center'> 
					<font color='red'><h3>Data tidak ditemukan</h3></font>
				</div>";
		}
	}
	elseif($mode=='pembatalan'){
		$template->set_filenames(array('body' => 'pembatalan_konfirmasi_body.tpl')); 
		
		$no_tiket	= $HTTP_POST_VARS['no_tiket'];// nourut
		
		//periksa wewenang
		$write_data_tiket	= pembatalan($no_tiket)?"<h2>Data Tiket BERHASIL DIBATALKAN</h2>":"<font color='red'><h2>Data Tiket GAGAL DIBATALKAN</h2></font>";
		
	}
	else {
		// LIST
		$template->set_filenames(array('body' => 'pembatalan_body.tpl'));		
	}      
	
	$template->assign_vars(array(
			'U_ACTION'		=> append_sid('pembatalan.'.$phpEx),
			'DATA_TIKET'	=> $write_data_tiket,
			'NO_TIKET'		=> $no_tiket,
			'U_BACK'			=> append_sid("pembatalan.php?mode=show"),
			'TOMBOL_BATAL'=> $show_tombol_batal
		)
	);

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>