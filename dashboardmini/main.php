<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,201); 
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################
// HEADER
		
$template->set_filenames(array('body' => 'menu_body.tpl')); 

$template->assign_vars(array
  ( 'U_ACTION'  	=> append_sid('dashboard.'.$phpEx.''),
		'OPT_TANGGAL'	=> setComboTanggal(),
		'OPT_BULAN'		=> setComboBulan(),
		'OPT_TAHUN'		=> setComboTahun()
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>