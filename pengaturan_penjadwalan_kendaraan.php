<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassPenjadwalanKendaraan.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_SCHEDULER))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tanggal				= isset($HTTP_GET_VARS['tanggal'])? $HTTP_GET_VARS['tanggal'] : $HTTP_POST_VARS['tanggal']; 
$cabang_asal		= isset($HTTP_GET_VARS['opt_cabang_asal'])? $HTTP_GET_VARS['opt_cabang_asal'] : $HTTP_POST_VARS['opt_cabang_asal']; 
$cabang_tujuan	= isset($HTTP_GET_VARS['opt_tujuan'])? $HTTP_GET_VARS['opt_tujuan'] : $HTTP_POST_VARS['opt_tujuan']; 

if($tanggal==""){
	$tanggal = date("d-m-Y");
	$tombol_cari=false;
}
else{
	$tanggal;
	$tombol_cari=true;
}

$tanggal_mysql	= FormatMySQLDateToTgl($tanggal);

$Jurusan= new Jurusan();
$Sopir	= new Sopir();
$Mobil	= new Mobil();
$Cabang	= new Cabang();
$PenjadwalanKendaraan = new PenjadwalanKendaraan();

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;
	global $Mobil;
			
	$result=$Mobil->ambilDataForComboBox();
	$selected_default	= ($kode_kendaraan!="")?"":"selected";

	$opt_mobil="<option value='' $selected_default>- silahkan pilih kendaraan  -</option>";
	
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
			$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[JumlahKursi] Seat $row[Merek] $row[Jenis]</option>";
		}
	}
	else{
		echo("err :".__LINE__);exit;
	}		
	
	return $opt_mobil;
	//END SET COMBO MOBIL
}

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	global $Sopir;
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	
	$selected_default	= ($kode_sopir_dipilih!="")?"":"selected";
	$opt_sopir="<option value='' $selected_default>- silahkan pilih sopir  -</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
	
}

function setComboCabangAsal($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;
			
	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal);
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

//BODY 
	if ($mode=='update'){
		$tgl					= $HTTP_GET_VARS['tgl'];
		$tgl_mysql		= FormatTglToMySQLDate($HTTP_GET_VARS['tgl']);
		$kode_jadwal	= $HTTP_GET_VARS['kode_jadwal'];
		$kode_kendaraaan = $HTTP_GET_VARS['kodekendaraaan'];
		$kode_sopir		= $HTTP_GET_VARS['kode_sopir'];
		$aktif				= $HTTP_GET_VARS['aktif'];
		$submode			= $HTTP_GET_VARS['submode'];
		
		//MEMERIKSA JIKA AKAN MENONAKTIFKAN JADWAL, AKAN DIPERIKSA TERLEBIH DAHULU APAKAH MASIH ADA PENUMPANG DI JADWAL TERSEBUT ATAU TIDAK
		if($aktif==0){
			$sql = 
				"SELECT COUNT(1)
				FROM tbl_reservasi
				WHERE KodeJadwal='$kode_jadwal'
					AND TglBerangkat='$tgl_mysql'
					AND (FlagBatal!=1 OR FlagBatal IS NULL)";
					
			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
			
			$data_penumpang=$db->sql_fetchrow($result);
			
			if($data_penumpang[0]>0){
				//MASIH ADA PENUMPANG DI JADWAL YANG AKAN DINONAKTIFKAN
				echo(0);
				exit;
			}
		}

		$sql = 
			"SELECT IdPenjadwalan
			FROM tbl_penjadwalan_kendaraan 
			WHERE TglBerangkat='$tgl_mysql' 
				AND KodeJadwal='$kode_jadwal'";
		
		if ($result = $db->sql_query($sql)){
		  $row = $db->sql_fetchrow($result);
			
			if($row[0]!=""){
				if($submode==0){
					//ubah kendaraan
					$PenjadwalanKendaraan->ubah($row[0],$kode_kendaraaan,"");		
				}
				else{
					//ubah sopir
					$PenjadwalanKendaraan->ubah($row[0],"",$kode_sopir);		
				}
			}
			else{
				$sql_jadwal	=
					"SELECT JamBerangkat,IdJurusan FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal'";
				
				if ($result = $db->sql_query($sql_jadwal)){
					$row_jadwal = $db->sql_fetchrow($result);
				}
				else{
					echo("Err:".__LINE__);
					exit;
				}
				
				$PenjadwalanKendaraan->tambah(
					$tgl_mysql,$row_jadwal['JamBerangkat'],
					$kode_jadwal,$row_jadwal['IdJurusan'], 
					$kode_kendaraaan,$kode_sopir,$aktif);
				
			}
		}
		else{
			echo("Err:".__LINE__);
			exit;
		}
		
		echo(1);
		exit;
	}
	else if ($mode=='ubahstatus'){
		// aksi hapus jadwal
		$id = $HTTP_GET_VARS['id'];
	
		echo($PenjadwalanKendaraan->ubahStatus($id)?1:0);
			
		exit;
	} 
	else if ($mode=='get_tujuan'){
		$cabang_asal		= $HTTP_GET_VARS['asal'];
		$id_jurusan			= $HTTP_GET_VARS['jurusan'];
		
		$opt_cabang_tujuan=
			"<select id='opt_tujuan' name='opt_tujuan'>".
			setComboCabangTujuan($cabang_asal,$id_jurusan)
			."</select>";
		
		echo($opt_cabang_tujuan);
		
		exit;
	}
	else if ($mode=='setdaftarmobil'){
		$mobil_dipilih	= $HTTP_POST_VARS['mobil'];
		$kode_jadwal		= $HTTP_POST_VARS['kodejadwal'];
		$tanggal				= $HTTP_POST_VARS['tanggal'];
		$aktif					= $HTTP_POST_VARS['aktif'];
		
		$return	=
			"<select name='dlgcbomobil' id='dlgcbomobil'>"
				.setComboMobil($mobil_dipilih).
			"</select>";
		
		echo($return);
		
		exit;
	}
	else if ($mode=='setdaftarsopir'){
		$sopir_dipilih	= $HTTP_POST_VARS['sopir'];
		$kode_jadwal		= $HTTP_POST_VARS['kodejadwal'];
		$tanggal				= $HTTP_POST_VARS['tanggal'];
		$aktif					= $HTTP_POST_VARS['aktif'];
		
		$return			=
			"<select name='dlgcbosopir' id='dlgcbosopir'>"
				.setComboSopir($sopir_dipilih).
			"</select>";
		
		echo($return);
		
		exit;
	}
	else {
		// LIST
		$template->set_filenames(array('body' => 'penjadwalan_kendaraan/jadwal_body.tpl')); 
		
		if($tombol_cari){
			
			$kondisi	= ($cabang_tujuan!='')?" WHERE tmj.IdJurusan LIKE $cabang_tujuan ":"";
			
			//PAGING======================================================
			$idx_page 	= ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:$HTTP_POST_VARS['page'];
			$idx_page 	= ($idx_page!='')?$idx_page:0;
			$paging		= pagingData($idx_page,"tmj.KodeJadwal","tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'",
									"&tanggal=$tanggal&opt_cabang_asal=$cabang_asal&opt_tujuan=$cabang_tujuan",
									$kondisi,"pengaturan_penjadwalan_kendaraan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
			//END PAGING======================================================
			
			$kondisi_sub_query	= " WHERE TglBerangkat='$tanggal_mysql' AND KodeJadwal=tmj.KodeJadwal";
			
			$sql=
				"SELECT tmj.KodeJadwal,tmj.IdJurusan,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan)) as Asal,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tmj.IdJurusan)) as Tujuan,
					tmj.JamBerangkat,IdPenjadwalan,tpk.StatusAktif,NoPolisi,KodeKendaraan,
					IF(tpk.LayoutKursi IS NULL,tmj.JumlahKursi,tpk.LayoutKursi) AS JumlahKursi,KodeDriver,NamaDriver,
					IF(FlagSubJadwal!=1,FlagAktif,IF((SELECT FlagAktif FROM tbl_md_jadwal WHERE KodeJadwal=tmj.KodeJadwalUtama LIMIT 0,1)=1,FlagAktif,2))	AS FlagAktifDefault,
					IF(FlagSubJadwal!=1,tpk.StatusAktif,IF((SELECT IF(COUNT(StatusAktif)=0,1,StatusAktif) FROM tbl_penjadwalan_kendaraan WHERE KodeJadwal=tmj.KodeJadwalUtama AND TglBerangkat='$tanggal_mysql' LIMIT 0,1)=1,tpk.StatusAktif,2)) AS FlagAktif,
					tpk.Remark,tmj.FlagSubJadwal
				FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'
				$kondisi 
				ORDER BY tmj.JamBerangkat,tmj.KodeJadwal LIMIT $idx_awal_record,$VIEW_PER_PAGE";
			
			$idx_check=0;
			
			
			if (!$result = $db->sql_query($sql)){
        //die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
        echo("Err:".__LINE__);exit;
      }

			$i = $idx_page*$VIEW_PER_PAGE+1;

			while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
        $tambahan_remark  = "";

				if (($i % 2)==0){
					$odd = 'even';
				}


				//JADWAL BELUM PERNAH DIBUAT
				if($row['IdPenjadwalan']==''){
					$flag_status_invert	= 1-$row['FlagAktifDefault'];

					if($row['FlagAktifDefault']==1){
            $status="<a href='#' onClick='simpan(\"$tanggal\",\"$row[KodeJadwal]\",$flag_status_invert); return false;'>Aktif</a>";
					}
					else{
						$odd	= "red";

            if($row['FlagAktifDefault']!=2 && $row['FlagAktif']!=2){
              $status="<a href='#' onClick='simpan(\"$tanggal\",\"$row[KodeJadwal]\",$flag_status_invert); return false;'>Nonaktif</a>";
            }
            else{
              $status="<font color='white'>Nonaktif</font>";
              $tambahan_remark  = "|Jadwal Induk Dinonaktifkan";
            }
					}
				}
        else{
          if(($row['FlagAktif']==1 && $row['FlagAktifDefault']!=2) || ($row['FlagAktif']=='' && $row['FlagAktifDefault']==1)){
            $status="<a href='' onClick='return ubahStatus(\"$row[IdPenjadwalan]\")'>Aktif</a>";
          }
          else{
            $odd	= "red";

            if($row['FlagAktifDefault']!=2 && $row['FlagAktif']!=2){
              $status="<a href='' onClick='return ubahStatus(\"$row[IdPenjadwalan]\")'>Nonaktif</a>";
            }
            else{
              $status="<font color='white'>Nonaktif</font>";
              $tambahan_remark  = "|Jadwal Induk Dinonaktifkan";
            }

          }
        }

				if($row['NoPolisi']=='' && $row['FlagAktif']==1){
					$odd	= "pink";
				}

				$opt_kendaraan	= "<span style='width: 100px;margin-left: 5px;'>".($row['KodeKendaraan']==""?"<font color='".($odd!='red'?"red":"white")."'>BELUM DIATUR</font></span>":"<b>$row[KodeKendaraan]</b> | $row[NoPolisi]</span>")." <input style='font-size: 12px; width: 25px;margin-left: 5px;position: static;' type='button' value='...' onclick='setDaftarMobil(\"$row[KodeKendaraan]\",\"$row[KodeJadwal]\",\"$tanggal_mysql\",1);'/>";

				$opt_sopir			= "<span style='width: 100px;margin-left: 5px;'>".($row['KodeDriver']==""?"<font color='".($odd!='red'?"red":"white")."'>BELUM DIATUR</font></span>":"<b>$row[NamaDriver]</b> | $row[KodeDriver]</span>")." <input style='font-size: 12px; width: 25px;margin-left: 5px;position: static;' type='button' value='...' onclick='setDaftarSopir(\"$row[KodeDriver]\",\"$row[KodeJadwal]\",\"$tanggal_mysql\",1);'/>";
	

				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'kode'=>$row['KodeJadwal'],
							'jurusan'=>$row['Asal']."-".$row['Tujuan'],
							'jam'=>$row['JamBerangkat'],
							'nopol'=>$opt_kendaraan,
							'kode_sopir'=>$opt_sopir,
							'kursi'=>$row['JumlahKursi'],
							'status'=>$status,
							'remark'=>$row['Remark'].$tambahan_remark
						)
					);

				$i++;
			}

			$no_data	= "";
		}
		else{
			$no_data	= "<tr><td colspan='20' style='background-color: yellow;font-size:16px; text-align: center;'>Silahkan pilih asal dan tujuan lalu klik tombol CARI</td></tr>";
		}
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx).'">Jadwal Kendaraan</a>',
			'U_JADWAL_ADD'	=> append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx),
			'ASAL'					=> $cabang_asal,
			'ID_JURUSAN'		=> $cabang_tujuan,
			'CABANG_ASAL'		=> setComboCabangAsal($cabang_asal),
			'CABANG_TUJUAN'	=> setComboCabangTujuan($cabang_asal,$cabang_tujuan),
			'TANGGAL'				=> $tanggal,
			'HALAMAN'				=> $idx_page,
			'PAGING'				=> $paging,
			'FIRST_LOAD'		=> !$tombol_cari,
			'NO_DATA'				=> $no_data
			)
		);
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>