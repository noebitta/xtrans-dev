<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$fil_status = isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"WaktuCetakTiket":$sort_by;

switch($fil_status){
	case "0":
		$status	= "Booking";
		break;
	case "1":
		$status	= "Dibayar";
		break;
	case "2":
		$status	= "Batal";
		break;
	
}

//QUERY
$sql	= 
	"SELECT *,f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM v_laporan_keuangan_tiketux".$userdata['user_id']."
	ORDER BY $sort_by $order";
		
if ($result = $db->sql_query($sql)){
		
	$i=1;
	
	$objPHPExcel = new PHPExcel();          
  $objPHPExcel->setActiveSheetIndex(0);  
  $objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
  $objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
  
	//HEADER
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Penjualan Tiketux per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir));
	$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Filter Kota: '.$kota.', Asal: '.$asal.', Tujuan: '.$tujuan.', Status Tiket: '.$status);
	$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Waktu Transaksi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('C4', '#tiket');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Waktu Berangkat');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Kode Jadwal');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Nama');
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('G4', '#Kursi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Harga Tiket');
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Discount');
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('J4', 'Komisi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Total');
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('L4', 'Tipe Discount');
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('M4', 'Status');
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('N4', 'Keterangan');
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	
	$idx=0;
	
	while ($row = $db->sql_fetchrow($result)){
		$idx++;
		$idx_row=$idx+4;
		
		if($row['FlagBatal']!=1){
		
			if($row['CetakTiket']!=1){
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			
			$keterangan="";
			
		}
		else{
			
			$status	= "BATAL ".$row['CetakTiket'];
			
			$keterangan	= $row['NamaCSOPembatalan'];
			
		}
	
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoTiket']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeJadwal']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Nama']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NomorKursi']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['HargaTiket']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Discount']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['Komisi']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Total']-$row['Komisi']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['JenisDiscount']);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $status);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $keterangan);
		
	}
	$temp_idx=$idx_row;
	
	$idx_row++;		
	
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':G'.$idx_row);
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row,'=SUM(H4:H'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row,'=SUM(I4:I'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row,'=SUM(J4:J'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row,'=SUM(K4:K'.$temp_idx.')');
	
		
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
  
	if ($idx>0){
		header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Penjualan Tiketux per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output'); 
	}
}
else{
	die_error('Err:',__LINE__);
}   


?>
