<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Paket{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Paket(){
		$this->ID_FILE="C-PKT";
	}
	
	//BODY
	
	function periksaDuplikasi($no_tiket){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT f_paket_periksa_duplikasi('$no_tiket') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah(
		$no_tiket, $kode_cabang, $kode_jadwal,
		$id_jurusan, $kode_kendaraan , $kode_sopir ,
		$tgl_berangkat, $jam_berangkat , $nama_pengirim ,
		$alamat_pengirim, $telp_pengirim,
		$nama_penerima, $alamat_penerima, $telp_penerima,
		$harga_paket,$diskon,$total_bayar,$keterangan_paket,$instruksi_khusus, $petugas_penjual,
		$komisi_paket_CSO, $komisi_paket_sopir,
		$kode_akun_pendapatan, $kode_akun_komisi_paket_CSO, $kode_akun_komisi_paket_sopir,
		$jumlah_koli,$berat,$jenis_barang,
		$layanan,$cara_bayar,$kode_pelanggan){
	  
		/*
		IS	: data paket belum ada dalam database
		FS	:Data paket baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		$sql=
			"INSERT INTO tbl_paket (
				NoTiket, KodeCabang, KodeJadwal,
				IdJurusan, KodeKendaraan, KodeSopir,
				TglBerangkat, JamBerangkat, NamaPengirim,
				AlamatPengirim, TelpPengirim, WaktuPesan,
				NamaPenerima, AlamatPenerima, TelpPenerima,
				HargaPaket,Diskon,TotalBayar,KeteranganPaket, PetugasPenjual,
				NoSPJ, TglCetakSPJ,
				CetakSPJ, KomisiPaketCSO, KomisiPaketSopir,
				KodeAkunPendapatan,KodeAkunKomisiPaketCSO,KodeAkunKomisiPaketSopir,
				JumlahKoli,Berat,JenisBarang,
				Layanan,CaraPembayaran,
				FlagBatal,InstruksiKhusus,KodePelanggan)
			VALUES(
				'$no_tiket', '$kode_cabang', '$kode_jadwal',
				'$id_jurusan', '', '',
				'$tgl_berangkat', '$jam_berangkat' , '$nama_pengirim' ,
				'$alamat_pengirim', '$telp_pengirim', NOW(),
				'$nama_penerima', '$alamat_penerima', '$telp_penerima',
				'$harga_paket','$diskon','$total_bayar','$keterangan_paket', '$petugas_penjual',
				'', '',
				'0', '$komisi_paket_CSO', '$komisi_paket_sopir',
				'$kode_akun_pendapatan', '$kode_akun_komisi_paket_CSO', '$kode_akun_komisi_paket_sopir',
				'$jumlah_koli','$berat','$jenis_barang',
				'$layanan','$cara_bayar',
				0,'$instruksi_khusus','$kode_pelanggan');";
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function pembatalan($no_tiket,$petugas_pembatalan){
	  
		/*
		IS	: data paket sudah ada dalam database
		FS	:Data paket dibatalkan 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"CALL sp_paket_batal('$no_tiket','$petugas_pembatalan',NOW());";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function  updateCetakTiket($no_tiket,$jenis_pembayaran,$cabang_transaksi=""){
	  
		/*
		IS	: data paket sudah ada dalam database
		FS	:Data paket dibatalkan 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"CALL sp_paket_update_cetak_tiket('$no_tiket','$jenis_pembayaran','$cabang_transaksi');";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilData($kondisi){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$kondisi	= ($kondisi=='')?1:$kondisi;
		
		$sql = 
			"SELECT *,f_user_get_nama_by_userid(PetugasPemberi) AS PetugasPemberi
			FROM tbl_paket
			WHERE $kondisi;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilDataDetail($no_tiket){
		
		/*
		Desc	:Mengembalikan data paket sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *,
			f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS NamaAsal,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS NamaTujuan,
			f_user_get_nama_by_userid(PetugasPemberi) AS NamaPetugasPemberi
			FROM tbl_paket
			WHERE NoTiket='$no_tiket';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function  updatePaketDiambil($no_tiket,$nama_pengambil,$no_ktp_pengambil,$petugas_pemberi,$cabang_ambil){
	  
		/*
		IS	: data paket sudah ada dalam database
		FS	:Data paket dibatalkan 
		*/
		
		//kamus
		global $db;
		global $PAKET_CARA_BAYAR_DI_TUJUAN;
		
		//memeriksa Cara  bayar
		$sql = 
			"SELECT CaraPembayaran
			FROM tbl_paket
			WHERE NoTiket='$no_tiket';";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		$cara_bayar	= $row[0];
		
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		if($cara_bayar!=$PAKET_CARA_BAYAR_DI_TUJUAN){
			$sql = 
				"CALL sp_paket_ambil_paket('$no_tiket','$nama_pengambil','$no_ktp_pengambil','$petugas_pemberi');";
		}
		else{
			$sql =
				"UPDATE tbl_paket
			  SET
			    NamaPengambil='$nama_pengambil',
			    NoKTPPengambil='$no_ktp_pengambil',
			    PetugasPemberi='$petugas_pemberi',
			    WaktuPengambilan=NOW(),
			    StatusDiambil=1,
					KodeCabang='$cabang_ambil'
			  WHERE
			    NoTiket = '$no_tiket'";
		}
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilDaftarHarga($id_jurusan){
		
		/*
		Desc	:Mengembalikan data-data harga paket
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				HargaPaketPKiloPertama,HargaPaketPKiloBerikut,
				HargaPaketGAKiloPertama,HargaPaketGAKiloBerikut,
				HargaPaketGDKiloPertama,HargaPaketGDKiloBerikut,
				HargaPaketSKiloPertama,HargaPaketSKiloBerikut,
				HargaPaketCAKiloPertama,HargaPaketCAKiloBerikut,
				HargaPaketCDKiloPertama,HargaPaketCDKiloBerikut,
				HargaPaketM1,HargaPaketM2
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilDaftarHarga
	
	/*function ambilHargaPaketByKodeLayanan($kode_layanan){
		
		/*
		Desc	:Mengembalikan data-data harga paket
		*/
		
		//kamus
		/*global $db;
		
		$sql = 
			"SELECT HargaKiloPertama,HargaKiloBerikutnya
			FROM tbl_md_paket_daftar_layanan
			WHERE KodeLayanan='$kode_layanan';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilDaftarHarga*/
	
	function ambilHargaPaketByKodeLayanan($kode_jadwal,$kode_layanan){
		
		/*
		Desc	:Mengembalikan data-data harga paket
		*/
		
		//kamus
		global $db;
		
		if(substr($kode_layanan,0,1)!="M"){
			$field1	= "HargaPaket".$kode_layanan."KiloPertama,";
			$field2	= "HargaPaket".$kode_layanan."KiloBerikut";
		}
		else{
			$field1	= "HargaPaket".$kode_layanan;
			$field2	= "";
		}


		$sql = 
			"SELECT 
				$field1 $field2
			FROM tbl_md_jurusan
			WHERE IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal');";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilHargaPaketByKodeLayanan
	
	function mutasiPaket(
		$no_tiket, $kode_jadwal,$tgl_berangkat, 
		$jam_berangkat ,$pemutasi){
		
		//kamus
		global $db;
		
		$sql	=
			"UPDATE tbl_paket
				SET
				KodeJadwal='$kode_jadwal',
				IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'),
				TglBerangkat='$tgl_berangkat',
				JamBerangkat='$jam_berangkat',
				FlagMutasi=1,
				WaktuMutasi=CONCAT(NOW(),';',WaktuMutasi),
				UserPemutasi=CONCAT('$pemutasi',';',UserPemutasi)
			WHERE
				NoTiket = '$no_tiket';";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
	
		return true;
	}
	
	function checkInPaket($no_tiket){
	  
		/*
		IS	: data paket sudah ada dalam database
		FS	:Data paket dibatalkan 
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		$sql =
			"UPDATE tbl_paket
			SET
				IsSampai=1,
				WaktuSampai=NOW(),
				UserCheckIn=".$userdata['user_id']."
			WHERE
				NoTiket IN ($no_tiket)";
				
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
			
		return true;
	}
	
	//PELANGGAN PAKET=========================================================================================================
	function generateKodePelanggan(){
		global $db;
		
		do{
			$kode_pelanggan = "PPL".date("ymdH").rand(10,99);
			
			$sql = "SELECT COUNT(1) FROM tbl_paket_pelanggan WHERE KodePelanggan='$kode_pelanggan'";
						
			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE ".__LINE__);
			}
			
			$row=$db->sql_fetchrow($result);
		
		}while($row[0]>0);
			
		
		
		return $kode_pelanggan;
	}
	
	function tambahPelanggan(
		$NamaPelanggan,$AlamatPelanggan,$ContactPerson,
		$TelpCP,$BesarDiscount,$TglHabisKontrak,
		$DidaftarkanOleh){
		
		global $db;
		
		$KodePelanggan	= $this->generateKodePelanggan();
		
		$sql =
			"INSERT INTO tbl_paket_pelanggan(
				KodePelanggan,NamaPelanggan,AlamatPelanggan,
				ContactPerson,TelpCP,BesarDiscount,
				TglHabisKontrak,WaktuCatatDaftar,DidaftarkanOleh)
			VALUES(
				'$KodePelanggan','$NamaPelanggan','$AlamatPelanggan',
				'$ContactPerson','$TelpCP','$BesarDiscount',
				'$TglHabisKontrak',NOW(),'$DidaftarkanOleh'
			)";
						
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $sql $this->ID_FILE ".__LINE__);
		}
			
		return true;
	}
	
	function ambilDataPelanggan($kode_pelanggan){
		
		/*
		Desc	:Mengembalikan data-data harga paket
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_paket_pelanggan
			WHERE KodePelanggan='$kode_pelanggan';";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		$row	= $db->sql_fetchrow($result);
		
		return $row;
		
	}//  END ambilDaftarHarga
	
	
	function ubahPelanggan(
		$KodePelanggan, $NamaPelanggan,$AlamatPelanggan,
		$ContactPerson,$TelpCP,$BesarDiscount,
		$TglHabisKontrak,$IsAktif){
		
		global $db;
		
		$sql =
			"UPDATE tbl_paket_pelanggan SET
				NamaPelanggan='$NamaPelanggan',
				AlamatPelanggan='$AlamatPelanggan',
				ContactPerson='$ContactPerson',
				TelpCP='$TelpCP',
				BesarDiscount='$BesarDiscount',
				TglHabisKontrak='$TglHabisKontrak',
				IsAktif='$IsAktif'
			WHERE KodePelanggan='$KodePelanggan'";
						
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
			
		return true;
	}
	
	function ubahAktifPelanggan($KodePelanggan){
		
		global $db;
		
		$sql =
			"UPDATE tbl_paket_pelanggan
			SET IsAktif=1-IsAktif
			WHERE KodePelanggan='$KodePelanggan'";
						
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
			
		return true;
	}
	
	function hapusPelanggan($KodePelanggan){
		
		global $db;
		
		$sql =
			"DELETE FROM tbl_paket_pelanggan
			WHERE KodePelanggan='$KodePelanggan'";
						
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
			
		return true;
	}
}
?>