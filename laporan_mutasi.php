<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Jurusan= new Jurusan();
$Cabang	= new Cabang();


// LIST
$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
$bulan					= isset($HTTP_GET_VARS['opt_bulan'])? $HTTP_GET_VARS['opt_bulan'] : $HTTP_POST_VARS['opt_bulan']; 
$tahun					= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun']; 
$kota_asal			= isset($HTTP_GET_VARS['kota_asal'])? $HTTP_GET_VARS['kota_asal'] : $HTTP_POST_VARS['kota_asal']; 
$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 

//pengaturan periode
if($tgl_awal>$tgl_akhir){
	$temp_tgl		= $tgl_akhir;
	$tgl_akhir	= $tgl_awal;
	$tgl_awal		= $temp_tgl;
}

$kota_asal	= ($kota_asal!="")?$kota_asal:"BANDUNG";

$tahun	= ($tahun=="")?date("y"):$tahun;
$bulan	= ($bulan=="")?substr('0'.date("m"),-2):$bulan;

$tgl_maximum	= getMaxDate($bulan,'20'.$tahun);

if($tgl_awal==""){
	$tgl_awal	= date("d");
}
else if($tgl_awal>$tgl_maximum){
	$tgl_awal	= $tgl_maximum;
}

if($tgl_akhir==""){
	$tgl_akhir	= date("d");
}
else if($tgl_akhir>$tgl_maximum){
	$tgl_akhir	= $tgl_maximum;
}

$tgl_awal_mysql	= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_awal,-2);
$tgl_akhir_mysql= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_akhir,-2);
//end pemgaturan periode

$template->set_filenames(array('body' => 'laporan_mutasi/laporan_mutasi.tpl')); 

$kondisi	= " WHERE (TglBerangkatSebelumnya BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') ";
$kondisi	.=" AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang=(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusanSebelumnya)))='$kota_asal'";
$kondisi .= " AND(TglBerangkatMutasi BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') ";

$kondisi .= ($cari=="")?"":" AND (NamaPenumpang LIKE '%$cari%' OR TelpPenumpang LIKE '$cari%' OR NoTiket LIKE '$cari%' OR KodeBookingSebelumnya LIKE '$cari%' OR KodeBookingMutasi LIKE '$cari%' OR NamaUserMutasi LIKE '%$cari%')";

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoTiket","tbl_log_mutasi",
						"&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&opt_bulan=$bulan&tahun=$tahun&kota_asal=$kota_asal&cari=$cari",
						$kondisi,"laporan_mutasi.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql = 
	"SELECT *
	FROM tbl_log_mutasi
	$kondisi
	ORDER BY TglBerangkatMutasi,KodeJadwalMutasi DESC LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
					
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'						=>$odd,
				'no'						=>$i,
				'no_tiket'			=>$row['NoTiket'],
				'nama'					=>$row['NamaPenumpang'],
				'telp'					=>$row['TelpPenumpang'],
				'issued'				=>$row['IsCetakTiket']==0?"TIDAK":"YA",
				'manifest'			=>$row['WaktuCetakSPJ']=="0000-00-00 00:00:00"?"BELUM":dateparse(FormatMySQLDateToTgl($row['WaktuCetakSPJ'])),
				'tgl_berangkat1'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkatSebelumnya'])),
				'jam1'					=>$row['JamBerangkatSebelumnya'],
				'kode_jadwal1'	=>$row['KodeJadwalSebelumnya'],
				'no_kursi1'			=>$row['NomorKursiSebelumnya'],
				'harga_tiket1'	=>number_format($row['HargaTiketSebelumnya'],0,",","."),
				'tgl_berangkat2'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkatMutasi'])),
				'jam2'					=>$row['JamBerangkatMutasi'],
				'kode_jadwal2'	=>$row['KodeJadwalMutasi'],
				'no_kursi2'			=>$row['NomorKursiMutasi'],
				'harga_tiket2'	=>number_format($row['HargaTiketMutasi'],0,",","."),
				'diskon'				=>number_format($row['TotalDiskon'],0,",","."),
				'waktu_mutasi'	=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuMutasi'])),
				'pemutasi'			=>$row['NamaUserMutasi']
			)
		);
	
	$i++;
}


//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir."&opt_bulan=".$bulan."&tahun=".$tahun."&kota_asal=".$kota_asal."&opt_cabang_asal=".$cabang_asal."&opt_tujuan=".$cabang_tujuan."&opt_jam=".$kode_jadwal."&status=".$status."&cari=".$cari;
$script_cetak_excel="Start('laporan_mutasi_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//END KOMPONEN UNTUK EXPORT	

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#log_reservasi">Home</a> | <a href="'.append_sid('laporan_mutasi.'.$phpEx).'">Laporan Mutasi Penumpang</a>',
	'CETAK_XL'    	=> $script_cetak_excel,
	'ACTION_CARI'		=> append_sid('laporan_mutasi.'.$phpEx),
	'KOTA'					=> $kota_asal,
	'OPT_KOTA'			=> setComboKota($kota_asal),
	'TGL_AWAL'			=> $tgl_awal,
	'TGL_AKHIR'			=> $tgl_akhir,
	'BULAN_'.$bulan	=> "selected",
	'TAHUN'					=> $tahun,
	'CARI'					=> $cari,
	'PAGING'				=> $paging
	)
);
  

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>