<?php

//chdir("/var/www/html/");
require_once('sendmail/PHPMailer_v5_1/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

function sendEmail($host,$smtp_debug,$smtp_auth,$smtp_secure,$port,$username,$password,$display_name,$adress_to,$address_name,$subject,$content){
  
  $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

  $mail->IsSMTP(); // telling the class to use SMTP
  
  try {
    $mail->Host = $host; // SMTP server
    $mail->SMTPDebug = $smtp_debug; // enables SMTP debug information (for testing)
    $mail->SMTPAuth = $smtp_auth; // enable SMTP authentication
    $mail->SMTPSecure = $smtp_secure; // sets the prefix to the servier
    $mail->Port = $port; // set the SMTP port for the GMAIL server
    $mail->Username = $username; // GMAIL username
    $mail->Password = $password; // GMAIL password
    
    $mail->AddAddress($adress_to, $address_name); //Set Send To Address
    
    $mail->SetFrom($username, $display_name); // Set Send From Address
    //$mail->AddReplyTo('noreply@tbk.co.id', 'No Reply');
    
    $mail->Subject = $subject;
    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
    //$mail->MsgHTML(file_get_contents('/home/admin/www/sendemail/content.html'));
    $mail->MsgHTML($content);
    
    //$mail->AddAttachment('/images/asdf.pdf'); // attachment
    //$mail->AddAttachment('/images/asd.wav'); // attachment
    
    
    $mail->Send();
    return "Message Sent OK</p>\n";
  } catch (phpmailerException $e) {
    return $e->errorMessage(); //Pretty error messages from PHPMailer
  } catch (Exception $e) {
    return $e->getMessage(); //Boring error messages from anything else!
  }
}


?>