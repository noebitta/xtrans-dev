<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassMemberTransaksi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){  
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER

$cso	= $userdata['nama']." ($userdata[username])";
					
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,1,'',0,0,'R');
    }
}
					
//set kertas & file
$pdf=new PDF('P','cm','tiket');
$pdf->Open();
$pdf->AliasNbPages();
//$pdf->AddPage();
$pdf->Setmargins(1,0,0,0);
$pdf->SetFont('courier','',10);

					
$id_topup			= $HTTP_GET_VARS['id_topup'];		

$TransaksiMember	= new TransaksiMember();
$data_topup				= $TransaksiMember->ambilDataDetailTopup($id_topup);

$Member				= new Member();
$data_member	= $Member->ambilDataDetail($data_topup['id_member']);		
		
$no_kwitansi		=trim($data_topup['id_topup']);
$nama						=trim($data_member['nama']);
$alamat					=trim($data_member['alamat']);
$telp						=trim($data_member['telp_rumah'])."/".trim($data_member['handphone']);
$waktu_transaksi=trim($data_topup['waktu_topup']);
$jumlah_topup		=number_format(trim($data_topup['jumlah_topup']),0,",",".");
$deposit				=number_format(trim($data_member['saldo']),0,",",".");
$point					=trim($data_member['point']);
$operator				=trim($data_topup['operator']);

$spasi	= 0.5;

// Header
$pdf->AddPage();
$pdf->Ln();
$pdf->Cell(0,0.3,'','',0,'');$pdf->Ln();

$pdf->SetFont('courier','B',12);
$pdf->Cell(0,$spasi,"KWITANSI TOP UP DEPOSIT MEMBER CIPAGANTI SHUTTLE SERVICE",'',0,'');
$pdf->Ln();		
$pdf->SetFont('courier','',10);			
$pdf->Cell(0,$spasi,"No Kwitansi: ".$no_kwitansi.'/'.$operator,'',0,'');
$pdf->Ln();
$pdf->Cell(0,$spasi,"Waktu transaksi: ".$waktu_transaksi,'',0,'');	
$pdf->Ln();

//content
$pdf->Cell(1.5,1,'',0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Nama",0,0,'');$pdf->Cell(7,$spasi,": ".$nama,0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Alamat",0,0,'');$pdf->Cell(7,$spasi,": ".$alamat,0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Telp",0,0,'');$pdf->Cell(7,$spasi,": ".$telp,0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Jum.Topup",0,0,'');$pdf->Cell(7,$spasi,": Rp.".$jumlah_topup,0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Deposit",0,0,'');$pdf->Cell(7,$spasi,": ".$deposit,0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Point",0,0,'');$pdf->Cell(7,$spasi,": ".$point,0,0,'');

//footer
$pdf->Ln();
$pdf->Cell(5,2,'',0,0,'C');
$pdf->Ln();
$pdf->Cell(14,0.4,$cso,0,0,'C');
$pdf->Ln();
$pdf->Cell(5,0.1,'','',0,'C');$pdf->Ln();

$pdf->Output();

?>