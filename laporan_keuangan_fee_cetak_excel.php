<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$bulan  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tahun  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];

//INISIALISASI
$PengaturanUmum	= new PengaturanUmum();
$fee_transaksi	= $PengaturanUmum->ambilFeeTransaksi();
$fee_tiket			= $fee_transaksi['FeeTiket']; 
$fee_paket			= $fee_transaksi['FeePaket']; 
		
//QUERY
//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}
	
//FEE PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
		IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket,
		IS_NULL(SUM(IF(FlagBatal=1,1,0)),0) AS TotalTiketBatal
	FROM tbl_reservasi
	WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1
	GROUP BY DATE(WaktuCetakTiket)
	ORDER BY DATE(WaktuCetakTiket) ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
//FEE PAKET
$sql=
	"SELECT 
		WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
	FROM tbl_paket
	WHERE MONTH(WaktuPesan)=$bulan AND YEAR(WaktuPesan)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY DATE(WaktuPesan)
	ORDER BY DATE(WaktuPesan)";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//FEE SPJ
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoSPJ),0) AS TotalSPJ
	FROM tbl_spj
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_spj = $db->sql_query($sql)){
	$data_spj = $db->sql_fetchrow($result_spj);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
$sum_spj									= 0;
$sum_tiket								= 0;
$sum_tiket_batal					= 0;
$sum_fee_tiket						= 0;
$sum_paket								= 0;
$sum_fee_paket						= 0;
$sum_total_fee						= 0;
	
//EXPORT KE MS-EXCEL

			
$i=1;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap Fee periode '.BulanString($bulan).' '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Jum.SPJ');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jum.Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Jum.Tiket Batal.');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jum.Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Fee Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Fee Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Total Fee');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);


$idx=0;

for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$idx++;
	
	$idx_row=$idx+3;
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	//FEE PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_tiket				= $data_penumpang['TotalTiket']; 
		$total_tiket_batal	= $data_penumpang['TotalTiketBatal']; 
		$total_fee_tiket		= $total_tiket*$fee_tiket;
		$data_penumpang 		= $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_tiket				= 0;
		$total_tiket_batal	= 0;
		$total_fee_tiket		= 0;
	}
	
	//FEE PAKET
	if($data_paket['Tanggal']==$idx_tgl+1){
		$total_paket				= $data_paket['TotalPaket']; 
		$total_fee_paket		= ($fee_paket<=1)?$data_paket['TotalOmzetPaket']*$fee_paket:$total_paket*$fee_paket;
		$data_paket 				= $db->sql_fetchrow($result_paket);
	}
	else{
		$total_paket			= 0;
		$total_fee_paket	= 0;
	}
	
	//FEE SPJ
	if($data_spj['Tanggal']==$idx_tgl+1){
		$total_spj		= $data_spj['TotalSPJ']; 
		$data_spj 		= $db->sql_fetchrow($result_spj);
	}
	else{
		$total_spj		= 0;
	}
	
	$total_fee		=	$total_fee_tiket+$total_fee_paket;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $tgl_transaksi);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $total_spj);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $total_tiket);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $total_tiket_batal);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $total_paket);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $total_fee_tiket);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $total_fee_paket);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $total_fee);
	
	$temp_hari++;
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, '=SUM(B4:B'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, '=SUM(C4:C'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D4:D'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');

	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Rekap Fee periode '.BulanString($bulan).' '.$tahun.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
 
  
  
?>
