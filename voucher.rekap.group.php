<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassVoucherDiskon.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

$bcrump	= "<a href=\"".append_sid("main.".$phpEx) ."#voucherdiskon\">Home</a> | <a href=\"".append_sid("voucher.rekap.group.php") ."\">Rekap Voucher Diskon</a>";

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Voucher	= new Voucher();

$tgl_awal 	= isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
$tgl_akhir 	= isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

$tgl_awal		= $tgl_awal==""?dateD_M_Y():$tgl_awal;
$tgl_akhir	= $tgl_akhir==""?dateD_M_Y():$tgl_akhir;

$tgl_mysql_awal		= FormatTglToMySQLDate($tgl_awal);
$tgl_mysql_akhir	= FormatTglToMySQLDate($tgl_akhir);

$kondisi	=($cari=="")?"":
	" AND(NamaGroup LIKE '%$cari%' 
		OR Deskripsi LIKE '%$cari%')";

//MENGAMBIL DATA GROUP
$sql = 
	"SELECT tvg.*,COUNT(KodeVoucher) AS VoucherDicetak,
		COUNT(IF(DATE(WaktuCetak) BETWEEN '$tgl_mysql_awal' AND '$tgl_mysql_akhir',KodeVoucher,NULL)) AS VoucherDicetak,
		SUM(IF(WaktuDigunakan BETWEEN '$tgl_mysql_awal 00:00:00' AND '$tgl_mysql_akhir 23:59:59',1,0)) AS VoucherDipakai,
		SUM(IF(WaktuDigunakan BETWEEN '$tgl_mysql_awal 00:00:00' AND '$tgl_mysql_akhir 23:59:59',TotalBayar,0)) AS TotalSettlement
	FROM tbl_voucher_group tvg LEFT JOIN tbl_voucher tv ON tvg.Id=tv.IdGroup
	WHERE 1 $kondisi
	GROUP BY IdGroup
	ORDER BY NamaGroup";

$idx_check=0;

if (!$result = $db->sql_query($sql)){
	die_error("Query Error",__LINE__,__FILE__,"");
}

$i = $idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['IsAktif']){
		$status="<a href='' onClick='return ubahStatus(\"$row[Id]\")'>Aktif</a>";
	}
	else{
		$odd	= "red";
		$status="<a href='' onClick='return ubahStatus(\"$row[Id]\")'>Tidak Aktif</a>";
	}
	
	$idx_check++;
	
	$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
	
	$act   ="<a href='".append_sid('voucher.rekap.group.batchvoucher.php?idgroup='.$row[0].'&tglawal='.$tgl_mysql_awal.'&tglakhir='.$tgl_mysql_akhir)."' class='b_browse' title='lihat daftar voucher'>&nbsp;</a>&nbsp;|&nbsp;";
	
	$event_print	= ($row['TotalSettlement']>0)?"Start('".append_sid('voucher.rekap.group.cetaktagihan.php?idgroup='.$row[0].'&tglawal='.$tgl_mysql_awal.'&tglakhir='.$tgl_mysql_akhir)."');":"alert('Tidak ada tagihan yang bisa dicetak!');";
	
	$act  .="<a href='' onClick=\"$event_print\" class='b_print' title='cetak tagihan'>&nbsp;</a>";

	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'namagrup'=>$row['NamaGroup'],
				'pembayaran'=>($row['IsSettlement']==1?"SETTLEMENT":"TUNAI"),
				'dicetak'=>number_format($row['VoucherDicetak'],0,",","."),
				'dipakai'=>number_format($row['VoucherDipakai'],0,",","."),
				'settlement'=>number_format($row['TotalSettlement'],0,",","."),
				'aktif'	=>$status,
				'action'=>$act
			)
		);
	
	$i++;
}

if($i-1<=0){
	$template->assign_block_vars("NO_DATA",array());
}


$template->set_filenames(array('body' => 'voucher.rekap.group/index.tpl')); 
$template->assign_vars(array(
	'BCRUMP'    		=> $bcrump,
	'U_ADD'					=> append_sid(basename(__FILE__)."?mode=add"),
	'ACTION_CARI'		=> append_sid(basename(__FILE__)),
	'TGL_AWAL'			=> $tgl_awal,
	'TGL_AKHIR'			=> $tgl_akhir,
	'CARI'					=> $cari,
	'NO_DATA'				=> $no_data,
	'PAGING'				=> $paging
	)
);


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>