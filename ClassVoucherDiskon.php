<?php

class Voucher{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas

	//CONSTRUCTOR
	function Voucher(){
		$this->ID_FILE="C-VCR";
	}

	//BODY

	public function generateBatchId(){
		global $userdata;

		return md5(date("YmdHis").$userdata['user_id']);
	}

	private function generateKodeVoucher(){
		/*$temp	= array("9",
			"1","2","3","4","5","6","7","8","9",
			"U7","V6","W5","X4","Y3","Z2",
			"K1","L2","M3","N4","A5","P6","Q7","R8","S9","T9",
			"A3","B2","C1","D4","E5","F6","G7","H9","I8","J9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","A","P","Q","R","S","T",
			"U","V","W","X","Y","Z");
		
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		$rnd4	= $temp[rand(1,61)];*/

		$j		= date("j")*1;
		$mn		=date("i")*1;
		$s		=date("s")*1;
		$rnd1	=rand(1,61);
		$rnd2	=rand(1,61);
		$rnd3	=rand(1,61);
		$rnd4	=rand(1,61);

		return "VC".$rnd1.$j.$rnd2.$mn.$rnd3.$s.$rnd4;
	}

	private function getKodeVoucher(){
		global $db;

		$i=0;

		do{
			$i++;
			$kode_voucher	= $this->generateKodeVoucher();

			$sql =
				"SELECT COUNT(1) AS Duplikasi
				FROM tbl_voucher
				WHERE KodeVoucher='$kode_voucher';";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}

			$row=$db->sql_fetchrow($result);

			$duplikasi	= $row['Duplikasi']==0?false:true;

		}while($duplikasi && $i<=100);

		$kode_voucher .= !$duplikasi && $i<=100?"":"X";

		return $kode_voucher;
	}

	function setSignature(
		$kode_voucher,$no_tiket,$id_jurusan,
		$kode_jadwal,$petugas_pengguna){

		global $config;

		return md5($petugas_pengguna."@".$kode_jadwal."#".$no_tiket."$".$kode_voucher."%".$id_jurusan."&".$config['key_token']);

	}

	function setVoucherReturn(
		$no_tiket_berangkat,$id_jurusan_berangkat,$kode_jadwal_berangkat,
		$nilai_voucher,$petugas_cetak,$is_boleh_week_end,
		$masa_berlaku_voucher){

		global $db;
		global $config;

		$kode_voucher	= $this->getKodeVoucher();

		$sql =
			"INSERT INTO tbl_voucher(
				KodeVoucher,NoTiketBerangkat,IdJurusanBerangkat,
				KodeJadwalBerangkat,CabangBerangkat,CabangTujuan,
				PetugasPencetak,WaktuCetak,ExpiredDate,
				NilaiVoucher,IsBolehWeekEnd,IsReturn,
				Keterangan)
			VALUES(
				'".$kode_voucher."','".$no_tiket_berangkat."','".$id_jurusan_berangkat."',
				'".$kode_jadwal_berangkat."',f_jurusan_get_kode_cabang_tujuan_by_jurusan(".$id_jurusan_berangkat."),f_jurusan_get_kode_cabang_asal_by_jurusan(".$id_jurusan_berangkat."),
				'".$petugas_cetak."',NOW(),ADDDATE(DATE(NOW()),INTERVAL ".$masa_berlaku_voucher." DAY),
				'".$nilai_voucher."','".$is_boleh_week_end."',1,
				'Voucher Return');";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return 	$kode_voucher;
	}

	function verifyVoucher($kode_voucher,$tgl_berangkat){
		global $db;

		$sql =
			"SELECT
				KodeVoucher,
				IF(ExpiredDate>=DATE(NOW()),1,0),
				IF(IsBolehWeekEnd=1 OR (IsBolehWeekEnd=0 AND DAYOFWEEK('$tgl_berangkat') IN (2,3,4,5)),1,0),
				IdGroup,
				IsSuspend,
				IF(WaktuDigunakan IS NULL,1,0)
			FROM tbl_voucher
			WHERE
				KodeVoucher='$kode_voucher';";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);


		if($db->sql_numrows($result)<=0){
			$data_return['status']=false;
			$data_return['error']='INVALID';
			return $data_return;
		}

		//memeriksa apakah korporat di blokir atau tidak

		$data_korporat	= $this->getDetailGroup($row['IdGroup']);

		if($data_korporat["IsAktif"]==0){
			$data_return['status']=false;
			$data_return['error']='GROUP_BLOCKED';
			return $data_return;
		}

		if($row[1]==0){
			$data_return['status']=false;
			$data_return['error']='EXPIRED';
			return $data_return;
		}

		if($row[2]==0){
			$data_return['status']=false;
			$data_return['error']='INVALID_DAY';
			return $data_return;
		}

		if($row[5]==0){
			$data_return['status']=false;
			$data_return['error']='USED';
			return $data_return;
		}

		if($row["IsSuspend"]==1){
			$data_return['status']=false;
			$data_return['error']='SUSPEND';
			return $data_return;
		}

		$data_return['status']=true;
		$data_return['error']='OK';
		return $data_return;

	}

	function getNilaiVoucher($kode_voucher){
		global $db;

		$sql =
			"SELECT NilaiVoucher
			FROM tbl_voucher
			WHERE
				KodeVoucher='$kode_voucher'
				AND ExpiredDate>=DATE(NOW())
				AND NoTiket IS NULL;";


		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		return	$row[0]!=''?$row[0]:0;

	}

	function pakaiVoucher(
		$kode_voucher,$tgl_berangkat,$no_tiket,$id_jurusan,
		$kode_jadwal,$petugas_pengguna,$nama_petugas,
		$nama,$telp,$signature){

		global $db;

		$data_return	= $this->verifyVoucher($kode_voucher,$tgl_berangkat);

		//cek signature
		if($this->setSignature($kode_voucher,$no_tiket,$id_jurusan,
				$kode_jadwal,$petugas_pengguna)!=$signature || !$data_return['status']){
			return false;
		}

		//MENGKALKULASI PERHITUNGAN VOUCHER
		$sql =
			"SELECT HargaTiket FROM tbl_reservasi
			WHERE NoTiket='".$no_tiket."';";

		if (!$result=$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$data_tiket		= $db->sql_fetchrow($result);
		$harga_tiket	= $data_tiket[0];

		$data_voucher	= $this->getDataVoucher($kode_voucher);

		$nilai_voucher= $data_voucher['NilaiVoucher']>1?$data_voucher['NilaiVoucher']:$harga_tiket*$data_voucher['NilaiVoucher'];

		$discount	= $data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher;

		if(($harga_tiket-$nilai_voucher)==0){
			$jenis_penumpang = 'G';
		}else{
			$jenis_penumpang = 'V';
		}

		//QUERY NORMAL
		$sql =
			"UPDATE tbl_reservasi
			SET
				Discount=IF($discount<=HargaTiket,$discount,HargaTiket),
				JenisDiscount='VOUCHER',
				Total=HargaTiket-Discount,
				JenisPenumpang='$jenis_penumpang'
			WHERE NoTiket='".$no_tiket."';";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$sql =
			"UPDATE tbl_voucher
			SET
				NoTiketBerangkat='".$no_tiket."',
				NamaPenumpang='$nama',
				Telp='$telp',
				IdJurusanBerangkat='".$id_jurusan."',
				KodeJadwalBerangkat='".$kode_jadwal."',
				CabangBerangkat=f_jurusan_get_kode_cabang_asal_by_jurusan($id_jurusan),
				CabangTujuan=f_jurusan_get_kode_cabang_tujuan_by_jurusan($id_jurusan),
				WaktuDigunakan=NOW(),
				PetugasPengguna='".$petugas_pengguna."',
				NamaPetugasPengguna='".$nama_petugas."',
				TotalBayar='".($harga_tiket-$discount)."'
			WHERE KodeVoucher='".$kode_voucher."';";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}


		return true;
	}

	function getDataVoucher($kode_voucher){
		global $db;

		$sql =
			"SELECT *
			FROM tbl_voucher
			WHERE KodeVoucher='$kode_voucher';";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);


		return $row;
	}

	function getDataVoucherByNoTiketBerangkat($no_tiket){
		global $db;

		$sql =
			"SELECT *
			FROM tbl_voucher
			WHERE NoTiketBerangkat='$no_tiket';";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);


		return $row;
	}

	function getDataVoucherByNoTiketPulang($no_tiket){
		global $db;

		$sql =
			"SELECT *
			FROM tbl_voucher
			WHERE NoTiket='$no_tiket';";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);


		return $row;
	}

	function setVoucher(
		$BatchId, $IdGroup, $IdJurusanBerangkat,
		$PetugasPencetak, $NamaPetugasPencetak, $ExpiredDate,
		$NilaiVoucher, $IsHargaTetap, $IsBolehWeekEnd,
		$Keterangan, $IsSettlement, $JumlahCetak,$IsVoucherOnline=0){

		global $db;
		global $config;

		$sql =
			"INSERT INTO tbl_voucher(
				BatchId, IdGroup, KodeVoucher,
				IdJurusanBerangkat,PetugasPencetak,NamaPetugasPencetak,
				WaktuCetak,ExpiredDate,NilaiVoucher,
				IsHargaTetap,IsBolehWeekEnd,Keterangan,
				IsSettlement,IsVoucherOnline)
			VALUES";

		for($i=1;$i<=$JumlahCetak;$i++){
			$KodeVoucher	= $this->generateKodeVoucher();

			$sql .=
				"('$BatchId','$IdGroup','$KodeVoucher',
				'$IdJurusanBerangkat','$PetugasPencetak','$NamaPetugasPencetak',
				NOW(),'$ExpiredDate','$NilaiVoucher',
				'$IsHargaTetap','$IsBolehWeekEnd','$Keterangan',
				'$IsSettlement','$IsVoucherOnline'),";
		}

		$sql	= substr($sql,0,-1).";";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
	}

	function getDataVoucherByNoTelp($no_telp){
		global $db;

		$sql =
			"SELECT *
			FROM tbl_voucher
			WHERE Keterangan LIKE '%$no_telp' AND IsVoucherOnline = 1;";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);


		return $row;
	}

	function suspendBatchVoucher($IdGroup,$BatchId,$IsSuspend){

		global $db;

		$sql =
			"UPDATE tbl_voucher SET
				IsSuspend=1-$IsSuspend
			WHERE IdGroup='$IdGroup' AND BatchId='$BatchId';";

		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);
			return false;
		}

		return true;
	}

	function hapusBatchVoucher($IdGroup,$BatchId){

		global $db;

		$sql =
			"DELETE FROM tbl_voucher
			WHERE IdGroup='$IdGroup' AND BatchId='$BatchId';";

		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);
			return false;
		}

		return true;
	}

	function tambahGroup(
		$nama_group,$deskripsi,$is_settlement,
		$is_aktif){

		global $db;

		$sql =
			"INSERT INTO tbl_voucher_group(
				NamaGroup,Deskripsi,IsSettlement,
				IsAktif)
			VALUES(
				'".$nama_group."','".$deskripsi."','".$is_settlement."',
				'".$is_aktif."');";

		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);
			return 0;
		}

		return 	$db->sql_nextid();
	}

	function ubahGroup(
		$nama_group,$deskripsi,$is_settlement,
		$is_aktif,$id){

		global $db;

		$sql =
			"UPDATE tbl_voucher_group SET 
				NamaGroup='$nama_group',Deskripsi='$deskripsi',IsSettlement='$is_settlement',
				IsAktif='$is_aktif'
			WHERE Id='$id';";

		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);
			return false;
		}

		return 	true;
	}

	function getDetailGroup($id){
		global $db;

		$sql =
			"SELECT *
			FROM tbl_voucher_group
			WHERE Id='$id';";

		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);
			exit;
		}

		$row=$db->sql_fetchrow($result);


		return $row;
	}

	function hapusGroup($id){
		global $db;

		$sql =
			"DELETE 
			FROM tbl_voucher_group
			WHERE Id IN ($id);";

		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);
			return false;
		}

		return true;
	}

	function ubahStatusAktifGroup($id){

		global $db;

		$sql =
			"UPDATE tbl_voucher_group SET 
				IsAktif=1-IsAktif
			WHERE Id='$id';";

		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);
			return false;
		}

		return true;
	}

}

?>