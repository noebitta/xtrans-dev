<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

$Cabang			= new Cabang();

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang    = isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$data_cabang = $Cabang->ambilDataDetail($kode_cabang);

//INISIALISASI
if($HTTP_POST_VARS["p4"]!=""){
    $cari=$HTTP_POST_VARS["p4"];
}
else{
    $cari=$HTTP_GET_VARS["p4"];
}

$kondisi_cari	=($cari=="")?"WHERE 1 ":
    " WHERE Nama LIKE '%$cari%' ";

// QUERY AMBIL BIAYA PETTY CASH
$sql = "SELECT *, f_cabang_get_name_by_kode(tbl_pengeluaran_dropcash.KodeCabang) AS Cabang, tbl_user.nama as Petugas
        FROM tbl_pengeluaran_dropcash
        JOIN tbl_user ON tbl_user.user_id = tbl_pengeluaran_dropcash.id_user
        $kondisi_cari AND DATE(TglBiaya) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'
        AND tbl_pengeluaran_dropcash.KodeCabang = '$kode_cabang'
        ORDER BY TglBiaya DESC";

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Daftar Pengeluaran Drop Cash Cabang :'.$data_cabang['Nama'].' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Pembuat');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Jenis');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jumlah');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

if ($result = $db->sql_query($sql)){

    $i=1;
    $idx=0;
    while ($row = $db->sql_fetchrow($result)){

        $idx++;
        $idx_row=$idx+3;

        $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Petugas']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, dateparse(FormatMySQLDateToTglWithTime($row['TglBiaya'])));
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['JenisPengeluaran']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['Jumlah']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Keterangan']);
    }

    $temp_idx=$idx_row;

    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

    if ($idx>0){
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Daftar Pengeluaran Drop Cash Cabang:'.$data_cabang['Nama'].' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir.'.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

}else{
    die("Error : ".__LINE__);
}

?>