<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode    		= $HTTP_GET_VARS['mode'];
$submode 		= $HTTP_GET_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'blank';

$Member	=	new Member();

switch($mode){
//MENAMPILKAN KOLOM ISIAN BLANK==========================================================================================================
case 'blank':

	$opt_jenis_kelamin="
		<option selected value='L'>Lelaki</option>
		<option value='P'>Perempuan</option>";
		
	$opt_kategori_member="
		<option value='KARYAWAN'>KARYAWAN</option>
		<option value='UMUM'>UMUM</option>
		<option selected value='MAHASISWA'>MAHASISWA</option>
		<option value='MANULA'>MANULA</option>";
	
	$pesan="Penambahan";
	
	$template->set_filenames(array('body' => 'member_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  					=>$userdata['username'],
	   	'BCRUMP'    					=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member.'.$phpEx).'">Member</a>',
			'PESAN'								=>$pesan,
			'OPT_JENIS_KELAMIN'		=>$opt_jenis_kelamin,
			'OPT_KATEGORI_MEMBER'	=>$opt_kategori_member,
			'TANGGAL_REGISTRASI'	=>dateNow()
		));
break;

//TAMBAH MEMBER BARU ==========================================================================================================
case 'tambah_member':
	
	//ambil isi dari parameter
	
	$id_member						= "MBR".dateYMD().rand(100,999);
	$nama									=$HTTP_GET_VARS['nama'];
	$jenis_kelamin				=$HTTP_GET_VARS['jenis_kelamin'];
	$kategori_member			=$HTTP_GET_VARS['kategori_member'];
	$tempat_lahir					=$HTTP_GET_VARS['tempat_lahir'];
	$tgl_register					=$HTTP_GET_VARS['tgl_register'];
	$tgl_lahir						=$HTTP_GET_VARS['tgl_lahir'];
	$no_ktp								=$HTTP_GET_VARS['no_ktp'];
	$alamat								=$HTTP_GET_VARS['alamat'];
	$kota									=$HTTP_GET_VARS['kota'];
	$kodepos							=$HTTP_GET_VARS['kodepos'];
	$telp_rumah						=$HTTP_GET_VARS['telp_rumah'];
	$handphone						=$HTTP_GET_VARS['handphone'];
	$password							=$HTTP_GET_VARS['password'];
	$konf_password				=$HTTP_GET_VARS['konf_password'];
	$email								=$HTTP_GET_VARS['email'];
	
	if($password!=$konf_password){
		echo("false password");
		exit;
	}
	
	if($Member->periksaDuplikasi($id_member)){
		
		$berhasil	= $Member->tambah(
			$id_member, $nama, $jenis_kelamin, $tempat_lahir, 
			$tgl_lahir, $no_ktp, $tgl_register, $alamat, 
			$kota, $kodepos, $telp_rumah, $handphone,$email,$id_kartu,$kategori_member,
			$password);
		
		if($berhasil){echo("berhasil");}else{echo("gagal");}
	}
	else{
		echo("duplikasi");
	}

	
exit;

//UBAH MEMBER BARU ==========================================================================================================
case 'ubah_member':
	
	//ambil isi dari inputan
	$id_member_old				=$HTTP_GET_VARS['id_member_old'];
	$nama									=$HTTP_GET_VARS['nama'];
	$jenis_kelamin				=$HTTP_GET_VARS['jenis_kelamin'];
	$kategori_member			=$HTTP_GET_VARS['kategori_member'];
	$tempat_lahir					=$HTTP_GET_VARS['tempat_lahir'];
	$tgl_register					=$HTTP_GET_VARS['tgl_register'];
	$tgl_lahir						=$HTTP_GET_VARS['tgl_lahir'];
	$no_ktp								=$HTTP_GET_VARS['no_ktp'];
	$alamat								=$HTTP_GET_VARS['alamat'];
	$kota									=$HTTP_GET_VARS['kota'];
	$kodepos							=$HTTP_GET_VARS['kodepos'];
	$telp_rumah						=$HTTP_GET_VARS['telp_rumah'];
	$handphone						=$HTTP_GET_VARS['handphone'];
	$email								=$HTTP_GET_VARS['email'];
	$password							=$HTTP_GET_VARS['password'];
	$konf_password				=$HTTP_GET_VARS['konf_password'];
	
	if($password!=$konf_password){
		echo("false password");
		exit;
	}
	
	if($Member->periksaDuplikasi($id_member) || $id_member==$id_member_old){
		$Member->ubah(
			$id_member_old, $nama, $jenis_kelamin, $tempat_lahir, 
			$tgl_lahir, $no_ktp, $tgl_register, $alamat, 
			$kota, $kodepos, $telp_rumah, $handphone,$email,$kategori_member,
			$id_member_old,$password
		);
		
		echo("berhasil");
	}
	else{
		echo("duplikasi");
	}
exit;

//AMBIL DATA MEMBER==========================================================================================================
case 'ambil_data_member':
	
	$id_member    = $HTTP_GET_VARS['id_member'];
	
	$row	= $Member->ambilDataDetail($id_member);
	
	$nama									=$row['nama'];
	$jenis_kelamin				=$row['jenis_kelamin'];
	$kategori_member			=$row['kategori_member'];
	$tempat_lahir					=$row['tempat_lahir'];
	$tgl_lahir						=$row['tgl_lahir'];
	$no_ktp								=$row['no_ktp'];
	$tgl_registrasi				=$row['tgl_registrasi'];
	$alamat								=$row['alamat'];
	$kota									=$row['kota'];
	$kodepos							=$row['kode_pos'];
	$telp_rumah						=$row['telp_rumah'];
	$handphone						=$row['handphone'];
	$email								=$row['email'];
	$deposit							=$row['saldo'];
	$point								=$row['point'];
	$diubah_oleh					=$row['diubah_oleh'];
	$tgl_diubah						=$row['tgl_diubah'];
	
	//memeriksa apakah data member dengan id_member ditemukan dalam database
	if($nama==""){
		//jika tidak ditemukan, akan langsung didirect ke halaman member
		redirect(append_sid('member.'.$phpEx),true); 
	}
	
	$pesan="Pengubahan";
	
	//option jenis kelamin
	$temp_var="selected_jenis_kelamin".$jenis_kelamin;
	$$temp_var="selected";
	
	$opt_jenis_kelamin="
		<option $selected_jenis_kelaminL value='L'>Lelaki</option>
		<option $selected_jenis_kelaminP value='P'>Perempuan</option>";
	
	//option kategori member
	$temp_var="selected_".trim($kategori_member);
	$$temp_var="selected";
	
	$opt_kategori_member="
		<option $selected_KARYAWAN value='KARYAWAN'>KARYAWAN</option>
		<option $selected_UMUM value='KARYAWAN'>UMUM</option>
		<option $selected_MAHASISWA value='MAHASISWA'>MAHASISWA</option>
		<option $selected_MANULA value='MANULA'>MANULA</option>";
	
	//menampilkan tombol ubah saldo dan poin jika otoritasnya adalah admin
	if($userdata['level_pengguna']==$LEVEL_ADMIN){
		$show_tombol_ubah_saldo_poin="
			<tr bgcolor='FFFF00'>
				<td colspan=3><strong>
					silahkan <a href='#' onClick='showDialogUbahSaldo();'>klik disini</a> untuk mengkoreksi nilai saldo member<br>
					atau jika anda ingin mengkoreksi jumlah poin member, silahkan <a href='#' onClick='showDialogUbahPoin();'>klik disini</a>
					</strong>
				</td>
			</tr>";
	}
	
	$template->set_filenames(array('body' => 'member_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  =>$userdata['username'],
	   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member.'.$phpEx).'">Member</a>',
	   	'U_ADD' =>'<a href="#" onClick="TampilkanDialogMember(\'\');">Tambah Member</a>',
			'PESAN'	=>$pesan,
			'ID_MEMBER'	=>$id_member,
			'NAMA'=>$nama,
			'OPT_JENIS_KELAMIN'=>$opt_jenis_kelamin,
			'OPT_KATEGORI_MEMBER'=>$opt_kategori_member,
			'TEMPAT_LAHIR'=>$tempat_lahir,
			'TANGGAL_LAHIR'=>$tgl_lahir,
			'NO_KTP'=>$no_ktp,
			'TANGGAL_REGISTRASI'=>$tgl_registrasi,
			'ALAMAT'=>$alamat,
			'KOTA'=>$kota,
			'KODEPOS'=>$kodepos,
			'TELP_RUMAH'=>$telp_rumah,
			'HANDPHONE'=>$handphone,
			'EMAIL'=>$email,
			'DEPOSIT'=>number_format($deposit,0,",","."),
			'POINT'=>$point,
			'DIUBAH_OLEH'=>$diubah_oleh,					
			'TGL_DIUBAH'=>$tgl_diubah,
			'KOLOM_TOMBOL_UBAH_SALDO_POIN'=>$show_tombol_ubah_saldo_poin,
			'SID'=>$userdata['sid']
  ));
	
break;
} //switch mode

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>