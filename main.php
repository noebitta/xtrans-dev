<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,201); 
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']==$LEVEL_SCHEDULER){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################
// HEADER
	
// TEMPLATE
switch($userdata['user_level']){
	case $LEVEL_CSO:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $LEVEL_CSO_PAKET:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $LEVEL_ADMIN :
		$template->set_filenames(array('body' => 'menu_body_0.tpl')); 
	break;
	
	case $LEVEL_MANAJEMEN :
		$template->set_filenames(array('body' => 'menu_body_1.tpl')); 
	break;
	
	case $LEVEL_MANAJER :
		$template->set_filenames(array('body' => 'menu_body_1_2.tpl')); 
	break;
	
	case $LEVEL_SUPERVISOR :
		$template->set_filenames(array('body' => 'menu_body_1_3.tpl')); 
	break;

	case $LEVEL_SUPERVISOR_PAKET :
		$template->set_filenames(array('body' => 'menu_body_1_4.tpl'));
	break;
	
	case $LEVEL_SCHEDULER :
		$template->set_filenames(array('body' => 'menu_body_3.tpl')); 
	break;
	
	case $LEVEL_KEUANGAN :
		$template->set_filenames(array('body' => 'menu_body_5.tpl')); 
	break;

	case $LEVEL_STAFF_KEUANGAN :
		$template->set_filenames(array('body' => 'menu_body_5_1.tpl'));
	break;

	case $LEVEL_STAFF_KEUANGAN_PAKET :
		$template->set_filenames(array('body' => 'menu_body_5_2.tpl'));
	break;
	
	case $LEVEL_CCARE :
		$template->set_filenames(array('body' => 'menu_body_6.tpl')); 
	break;
	
}


$template->assign_vars(array
  ( 'BCRUMP'    							=>'<a href="'.append_sid('main.'.$phpEx) .'">Home',
    'U_PENGATURAN'  						=>append_sid('pengaturan.'.$phpEx.''),
    'U_RESERVASI'   						=>append_sid('reservasi.'.$phpEx.''),
		'U_USER_LOGIN'						=>append_sid('pengaturan_user_list_login.'.$phpEx),
		'U_UBAHPASS'   						=>append_sid('ubah_password.'.$phpEx.''),
		'U_PENGUMUMAN'  					=>append_sid('pengaturan_pengumuman.'.$phpEx.''),
		'U_PENJADWALAN' 					=>append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx.''),
		'U_DAFTAR_MANIFEST'					=>append_sid('daftar_manifest.'.$phpEx.''),
		'U_BATAL' 							=>append_sid('pembatalan.'.$phpEx.''),
    'U_JENIS_DISCOUNT'						=>append_sid('jenis_discount.'.$phpEx),
    'U_CABANG'								=>append_sid('pengaturan_cabang.'.$phpEx),
    'U_JURUSAN'								=>append_sid('pengaturan_jurusan.'.$phpEx),
    'U_JADWAL'								=>append_sid('pengaturan_jadwal.'.$phpEx),
    'U_SOPIR'								=>append_sid('pengaturan_sopir.'.$phpEx),
    'U_MOBIL'								=>append_sid('pengaturan_mobil.'.$phpEx),
    'U_USER'								=>append_sid('pengaturan_user.'.$phpEx),
		'U_PROMO'							=>append_sid('pengaturan_promo.'.$phpEx),
		'U_PENGATURAN_ASURANSI'				=>append_sid('pengaturan_asuransi.'.$phpEx),
		'U_MEMBER'							=>append_sid('pengaturan_member.'.$phpEx),
		'U_MEMBER_ULTAH'					=>append_sid('pengaturan_member_ultah.'.$phpEx),
		'U_MEMBER_CALON'					=>append_sid('pengaturan_member_calon_member.'.$phpEx),
		//'U_MEMBER_CALON'		=>append_sid('main.'.$phpEx),
		'U_MEMBER_FREKWENSI'				=>append_sid('pengaturan_member_frekwensi.'.$phpEx),
		'U_MEMBER_HAMPIR_EXPIRED'			=>append_sid('pengaturan_member_hampir_expired.'.$phpEx),
		'U_PENGATURAN_UMUM'					=>append_sid('pengaturan_umum.'.$phpEx),
		'U_LAPORAN_CSO_CALLCENTER'			=>append_sid('laporan_penjualan_cso_callcenter.'.$phpEx),
		'U_LAPORAN_CSO'						=>append_sid('laporan_penjualan_cso.'.$phpEx),
		'U_LAPORAN_UANG_CSO'				=>append_sid('laporan_uang_user.'.$phpEx.'?mode=cso'),
		'U_LAPORAN_CABANG'					=>append_sid('laporan_omzet_cabang.'.$phpEx),
		'U_LAPORAN_JURUSAN'					=>append_sid('laporan_omzet_jurusan.'.$phpEx),
		'U_LAPORAN_JADWAL'					=>append_sid('laporan_omzet_jadwal.'.$phpEx),
		'U_LAPORAN_OMZET'					=>append_sid('laporan_omzet.'.$phpEx),
		'U_LAPORAN_BANDARA'					=>append_sid('laporan_omzet_bandara.'.$phpEx),
		'U_LAPORAN_OMZET_CSO'				=>append_sid('laporan.omzet.percso.'.$phpEx),
		'U_LAPORAN_KENDARAAN'				=>append_sid('laporan_omzet_kendaraan.'.$phpEx),
		'U_LAPORAN_SOPIR'					=>append_sid('laporan_insentif_sopir.'.$phpEx),
		'U_LAPORAN_UANG_CABANG'				=>append_sid('laporan_uang_cabang.'.$phpEx),
		'U_LAPORAN_UANG_CSO'				=>append_sid('laporan.keuangan.percso.'.$phpEx),
		'U_LAPORAN_DATA_PAKET'				=>append_sid('laporan.paket.data.'.$phpEx),
		'U_LAPORAN_PAKET_BELUM_DIAMBIL'		=>append_sid('laporan.paket.belumdiambil.'.$phpEx),
		'U_LAPORAN_OMZET_PAKET_CABANG'		=>append_sid('laporan.paket.percabang.'.$phpEx),
		'U_LAPORAN_OMZET_PAKET_JURUSAN'		=>append_sid('laporan.paket.perjurusan.'.$phpEx),
		'U_LAPORAN_PAKET_PELANGGAN'			=>append_sid('paket.pelanggan.laporan.'.$phpEx),
		'U_REKAP_UANG_HARIAN'				=>append_sid('laporan_keuangan_harian.'.$phpEx),
		'U_REKAP_FEE_TRANSAKSI'				=>append_sid('laporan_keuangan_fee.'.$phpEx),
		'U_AREA'							=>append_sid('pengaturan_area.'.$phpEx),
		'U_PAKET_PELANGGAN'					=>append_sid('paket.pelanggan.'.$phpEx),
		'U_LAPORAN_ASURANSI'				=>append_sid('laporan_asuransi.'.$phpEx),
		'U_LAPORAN_TIKETUX'					=>append_sid('laporan_keuangan_tiketux.'.$phpEx),
		'U_LAPORAN_REDBUS'					=>append_sid('laporan_keuangan_redbus.'.$phpEx),
		'U_DEPOSIT_TIKETUX'					=>append_sid('deposit_tiketux.'.$phpEx),
		'U_DEPOSIT_REDBUS'					=>append_sid('deposit_redbus.'.$phpEx),
		'U_LAPORAN_TRX_DEPOSIT'				=>append_sid('deposit_tiketux_lap_trx.'.$phpEx),
		'U_LAPORAN_TRX_DEPOSIT_REDBUS'		=>append_sid('deposit_redbus_lap_trx.'.$phpEx),
		'U_LAPORAN_VOUCEHR_RETURN'			=>append_sid('laporan_keuangan_voucher_return.'.$phpEx),
		'U_MEMO'							=>append_sid('pengaturan_memo.'.$phpEx),
		'U_GRUP_VOUCHER_DISKON'				=>append_sid('voucher.group.'.$phpEx),
		'U_REKAP_GRUP_VOUCHER_DISKON'		=>append_sid('voucher.rekap.group.'.$phpEx),
		'U_VOUCHER_DISKON'					=>append_sid('voucher.'.$phpEx),
		'U_LOG_BATAL'						=>append_sid('laporan_pembatalan.'.$phpEx),
		'U_LOG_KOREKSI'						=>append_sid('laporan_koreksi_disc.'.$phpEx),
		'U_LOG_MUTASI'						=>append_sid('laporan_mutasi.'.$phpEx),
		'U_LOG_CETAK_TIKET'					=>append_sid('log.cetaktiket.'.$phpEx),
		'U_LOG_SMS'							=>append_sid('log_sms.'.$phpEx),
		'U_LOG_MANIFEST'					=>append_sid('log.cetakmanifest.'.$phpEx),
		'U_LOG_MANIFEST_TERLAMBAT'			=>append_sid('log.cetakmanifest.terlambat.'.$phpEx),
		'U_LOG_TIKET_KK'					=>append_sid('log.tiketkk.'.$phpEx),
		'U_LOG_TIKET_G'						=>append_sid('log.tiketg.'.$phpEx),
		'U_MACADDRESS'						=>append_sid('pengaturan_macaddress.'.$phpEx),
		'U_UBAH_PASSWORD'					=>append_sid('ubah_password.'.$phpEx),
		'U_DAFTAR_PELANGGAN'				=>append_sid('pelanggan.'.$phpEx),
		'U_STATISTIK'						=>append_sid('pengaturan_statistik.'.$phpEx),
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>