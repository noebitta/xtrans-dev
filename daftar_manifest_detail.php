<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$no_spj						= $HTTP_GET_VARS['no_spj'];
$kode_driver			= $HTTP_GET_VARS['kode_driver'];
$driver						= $HTTP_GET_VARS['driver'];
$no_polisi				= $HTTP_GET_VARS['no_polisi'];
$kode_jadwal			= $HTTP_GET_VARS['kode_jadwal'];
$waktu_berangkat	= $HTTP_GET_VARS['waktu_berangkat'];

// LIST
$template->set_filenames(array('body' => 'daftar_manifest/daftar_manifest_detail_body.tpl')); 

//MENGAMBIL DATA SPJ
$sql	=
	"SELECT KodeJadwal,TglBerangkat,IsSubJadwal FROM tbl_spj WHERE NoSPJ='$no_spj'";

if (!$result = $db->sql_query($sql)){
	die_error("Err $this->ID_FILE".__LINE__);
}

$row = $db->sql_fetchrow($result);

$kode_jadwal	= $row['KodeJadwal'];
$tgl_berangkat= substr($row['TglBerangkat'],0,10);
$is_sub_jadwal= $row['IsSubJadwal'];

if($is_sub_jadwal!=1){
	//mengambil data spj lain yang menginduk pada jadwal ini
		
	$sql =
		"SELECT NoSPJ
		FROM tbl_spj ts INNER JOIN tbl_md_jadwal tmj ON ts.KodeJadwal=tmj.KodeJadwal
		WHERE KodeJadwalUtama='$kode_jadwal'";
		
	if(!$result=$db->sql_query($sql)){
		echo("err:".__LINE__);exit;
	}
	
	$list_no_spj	= "'$no_spj',";
	
	while($row = $db->sql_fetchrow($result)){
		$list_no_spj .="'".$row[0]."',";
	}
	
	$list_no_spj	= substr($list_no_spj,0,-1);
}
else{
	$list_no_spj="'$no_spj'";
}

$sql	=
	"SELECT 
		NoTiket,KodeBooking,Nama,
		Alamat,Telp,NomorKursi
	FROM 
		tbl_reservasi
	WHERE NoSPJ IN($list_no_spj) AND CetakTiket=1 AND FlagBatal!=1
	ORDER BY NomorKursi;";

	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

if($db->sql_affectedrows()<=0){
	$sql	=
		"SELECT 
			NoTiket,KodeBooking,Nama,
			Alamat,Telp,NomorKursi
		FROM 
			tbl_reservasi_olap
		WHERE NoSPJ IN($list_no_spj) AND CetakTiket=1 AND FlagBatal!=1
		ORDER BY NomorKursi;";
	
		
	if(!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'no_kursi'=>$row['NomorKursi'],
				'nama'=>$row['Nama'],
				'kode_book'=>$row['KodeBooking'],
				'no_tiket'=>$row['NoTiket'],
				'telp'=>$row['Telp'],
				'alamat'=>$row['Alamat'],
			)
		);
	$i++;
}

$template->assign_vars(array(
	'BCRUMP'    	=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('daftar_manifest.'.$phpEx).'">Daftar Manifest</a>',
	'NO_MANIFEST'	=> $no_spj,
	'BERANGKAT'		=> $waktu_berangkat,
	'KODE_JADWAL'	=> $kode_jadwal,
	'DRIVER'			=> $driver,
	'NO_POLISI'		=> $no_polisi,
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>