<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
$bulan					= isset($HTTP_GET_VARS['opt_bulan'])? $HTTP_GET_VARS['opt_bulan'] : $HTTP_POST_VARS['opt_bulan']; 
$tahun					= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun']; 
$kota_asal			= isset($HTTP_GET_VARS['kota_asal'])? $HTTP_GET_VARS['kota_asal'] : $HTTP_POST_VARS['kota_asal']; 
$cabang_asal		= isset($HTTP_GET_VARS['opt_cabang_asal'])? $HTTP_GET_VARS['opt_cabang_asal'] : $HTTP_POST_VARS['opt_cabang_asal']; 
$cabang_tujuan	= isset($HTTP_GET_VARS['opt_tujuan'])? $HTTP_GET_VARS['opt_tujuan'] : $HTTP_POST_VARS['opt_tujuan']; 
$kode_jadwal		= isset($HTTP_GET_VARS['opt_jam'])? $HTTP_GET_VARS['opt_jam'] : $HTTP_POST_VARS['opt_jam']; 
$status					= isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status']; 
$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 

//pengaturan periode
if($tgl_awal>$tgl_akhir){
	$temp_tgl		= $tgl_akhir;
	$tgl_akhir	= $tgl_awal;
	$tgl_awal		= $temp_tgl;
}

$tahun	= ($tahun=="")?date("y"):$tahun;
$bulan	= ($bulan=="")?substr('0'.date("m"),-2):$bulan;

$tgl_maximum	= getMaxDate($bulan,'20'.$tahun);

if($tgl_awal==""){
	$tgl_awal	= date("d");
}
else if($tgl_awal>$tgl_maximum){
	$tgl_awal	= $tgl_maximum;
}

if($tgl_akhir==""){
	$tgl_akhir	= date("d");
}
else if($tgl_akhir>$tgl_maximum){
	$tgl_akhir	= $tgl_maximum;
}

$tgl_awal_mysql	= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_awal,-2);
$tgl_akhir_mysql= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_akhir,-2);
//end pemgaturan periode

//pengaturan pemilihan tabel pencarian
$tbl_pencarian	= ($bulan>=substr('0'.date("m"),-2) && $tahun>=date("y"))?"tbl_reservasi tr":"tbl_reservasi_olap tr";
//end pengatran pemilihan tabel pencarian

//mengambil list id jurusan by kota
$sql	= "SELECT IdJurusan FROM view_list_jurusan_by_kota_".str_replace("/","_",$kota_asal)." ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
} 

$list_id_jurusan ="";

while ($row = $db->sql_fetchrow($result)){
	$list_id_jurusan .=$row[0].",";
}
$list_id_jurusan	= substr($list_id_jurusan,0,-1);
//END mengambil list id jurusan by kota

$kondisi_status	= $status==''?'%':$status;

$kondisi	= " WHERE (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND FlagBatal=1 AND IdJurusan IN($list_id_jurusan) AND CetakTiket LIKE '$kondisi_status' ";
	
$kondisi	.= ($cari=="")?"":" AND (Nama LIKE '%$cari%' OR Telp LIKE '%$cari' OR NoTiket LIKE '%$cari' OR KodeBooking LIKE '%$cari')";

if($cabang_asal=='0'){
	$kondisi	.= "";
}
else{
	
	if($cabang_tujuan=='0'){
		$kondisi	.= " AND (SELECT KodeCabangAsal FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=tr.IdJurusan) LIKE '$cabang_asal' ";
	}
	else{
		if($kode_jadwal=='0'){
			$kondisi	.= " AND IdJurusan LIKE '$cabang_tujuan' ";
		}
		else{
			$kondisi	.= " AND KodeJadwal LIKE '$kode_jadwal' ";
		}
	}
}	

$sql = 
	"SELECT *
	FROM $tbl_pencarian
	$kondisi 
	ORDER BY TglBerangkat,JamBerangkat,KodeJadwal,NomorKursi";

$idx_check=0;


if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;

//mengambil data User
$sql_user = 
	"SELECT user_id,nama,nrp FROM tbl_user";

if ($result_user = $db->sql_query($sql_user)){
  while ($row_user = $db->sql_fetchrow($result_user)){
		$data_user[$row_user['user_id']]	= $row_user['nama'].' ('.$row_user['nrp'].')';
  }
} 
//end mengambil data User

	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,60);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Pembatalan','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,$tgl_awal.' s/d '.$tgl_akhir.' Bulan:'.$bulan.' Tahun: 20'.$tahun,'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',8);
$pdf->SetTextColor(255);
$pdf->Cell(10,3,'#','B',0,'C',1);
$pdf->Cell(20,3,'No.Tiket','B',0,'C',1);
$pdf->Cell(20,3,'Tgl.Brkt','B',0,'C',1);
$pdf->Cell(10,3,'Jam','B',0,'C',1);
$pdf->Cell(25,3,'Kd.Jdwl','B',0,'C',1);
$pdf->Cell(10,3,'Seat','B',0,'C',1);
$pdf->Cell(35,3,'Nama','B',0,'C',1);
$pdf->Cell(25,3,'Telp','B',0,'C',1);
$pdf->Cell(20,3,'Wkt.Pesan','B',0,'C',1);
$pdf->Cell(35,3,'Penjual','B',0,'C',1);
$pdf->Cell(20,3,'Wkt.Batal','B',0,'C',1);
$pdf->Cell(35,3,'Pembatal','B',0,'C',1);
$pdf->Cell(15,3,'Status','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',8);
$pdf->SetTextColor(0);
//CONTENT

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	
	$pdf->Cell(10,3,$idx+1,'',0,'R');
	$pdf->Cell(20,3,$row['NoTiket'],'',0,'L');
	$pdf->Cell(20,3,dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])),'',0,'C');
	$pdf->Cell(10,3,$row['JamBerangkat'],'',0,'C');
	$pdf->Cell(25,3,$row['KodeJadwal'],'',0,'L');
	$pdf->Cell(10,3,$row['NomorKursi'],'',0,'C');
	$pdf->MultiCell2(35,3,$row['Nama'],'','L');
	$pdf->Cell(25,3,$row['Telp'],'',0,'L');
	$pdf->MultiCell2(20,3,dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),'','L');
	$pdf->MultiCell2(35,3,$data_user[$row['PetugasPenjual']],'','L');
	$pdf->MultiCell2(20,3,dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuPembatalan'])),'','L');
	$pdf->MultiCell2(35,3,$data_user[$row['PetugasPembatalan']],'','L');
	$pdf->Cell(15,3,($row['CetakTiket']==0)?"Book":"Bayar",'',0,'C');	
	$pdf->Ln(0);
	$pdf->Cell(280,1,'','B',0,'');
	$pdf->Ln();
	
	$idx++;
}						
		
$pdf->Output();
						
?>