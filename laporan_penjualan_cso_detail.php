<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['p0'])? $HTTP_GET_VARS['p0'] : $HTTP_POST_VARS['p0'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$user_id				= isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];

// LIST
$template->set_filenames(array('body' => 'laporan_penjualan_cso/laporan_penjualan_cso_detail_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND PetugasPenjual=$user_id";

$kondisi_cari	=($cari=="")?"":
	" AND NoTiket LIKE '$cari'";
	
$kondisi	= $kondisi.$kondisi_cari;

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"WaktuPesan":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"NoTiket","tbl_reservasi_olap",	"&p2=$user_id&cari=$cari&p0=$tanggal_mulai&p1=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi,"laporan_penjualan_cso_detail.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,Discount,Total,JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM 
		tbl_reservasi_olap
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";	


if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$pesanan	= (!$row['FlagPesanan'])?"Go Show":"Pesanan";
		
		if($row['FlagBatal']!=1){
			if($row['CetakTiket']!=1){
				$odd	= "blue";
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			$keterangan="";
		}
		else{
			$odd	= 'red';
			$status	="BATAL";
			$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'waktu_pesan'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),
					'no_tiket'=>$row['NoTiket'],
					'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
					'kode_jadwal'=>$row['KodeJadwal'],
					'nama'=>$row['Nama'],
					'no_kursi'=>$row['NomorKursi'],
					'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
					'discount'=>number_format($row['Discount'],0,",","."),
					'total'=>number_format($row['Total'],0,",","."),
					'tipe_discount'=>$row['JenisDiscount'],
					'cso'=>$row['NamaCSO'],
					'status'=>$status,
					'ket'=>$keterangan
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> |  <a href="'.append_sid('laporan_penjualan_cso.'.$phpEx).'">Laporan Penjualan CSO</a>',
	'ACTION_CARI'		=> append_sid('laporan_penjualan_cso_detail.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'ID_USER'				=> $user_id,
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>