<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_CSO,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tglmulai'])? $HTTP_GET_VARS['tglmulai'] : $HTTP_POST_VARS['tglmulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$kode_cabang		= isset($HTTP_GET_VARS['kode'])? $HTTP_GET_VARS['kode'] : $HTTP_POST_VARS['kode'];
$sort_by				= $HTTP_POST_VARS['sortby'];
$order					= $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'laporan_uang_cabang/detail.tpl')); 

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND FlagBatal!=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND PetugasCetakTiket!=0";

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"WaktuPesan":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoTiket",$tbl_reservasi,
"&kode=$kode_cabang&tglmulai=$tanggal_mulai&tglakhir=$tanggal_akhir&is_today=$is_today&sortby=$sort_by&order=$order",
$kondisi,"laporan_uang_cabang_detail.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================


//QUERY TIKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,
		IF(JenisPenumpang!='R',Discount,0) AS Discount,
		IF(JenisPenumpang!='R',Total,HargaTiket) AS Total,
		IF(JenisPenumpang!='R',0,Total-HargaTiket) AS Plus,
		JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasCetakTiket) AS NamaCSOTiket,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan,
		FlagBatal*10+CetakTiket AS Status,
		WaktuCetakTiket,TglCetakSPJ
	FROM 
		$tbl_reservasi
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";	


if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap Detail Setorang Cabang per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Waktu Pesan');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Cetak Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'No.Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Cetak Manifest');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Kode Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Harga');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Plus');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Tipe Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Book Oleh');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Tiket Oleh');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);

$idx=0;
$idx_row	= 4;

while ($row = $db->sql_fetchrow($result)){
		
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])));
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NomorKursi']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['HargaTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['Discount']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Total']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['Plus']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $row['JenisDiscount']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['NamaCSO']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $row['NamaCSOTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $status);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $keterangan);


	$idx++;
	$idx_row++;
}
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Rekap Detail Setorang Cabang per Tanggal '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}

?>