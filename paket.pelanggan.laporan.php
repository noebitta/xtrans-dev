<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_STAFF_KEUANGAN_PAKET,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

// LIST
$template->set_filenames(array('body' => 'paket.pelanggan/laporan.paket.index.tpl')); 

$tgl_awal		= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
$tgl_akhir	= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
$cari				= $HTTP_POST_VARS["txt_cari"]!=""?$HTTP_POST_VARS["txt_cari"]:$HTTP_GET_VARS["cari"];

$tgl_awal					= ($tgl_awal!='')?$tgl_awal:dateD_M_Y();
$tgl_akhir				= ($tgl_akhir!='')?$tgl_akhir:dateD_M_Y();
$tgl_awal_mysql		= FormatTglToMySQLDate($tgl_awal);
$tgl_akhir_mysql	= FormatTglToMySQLDate($tgl_akhir);

$kondisi = 
		"(tpp.KodePelanggan LIKE '%$cari' 
		OR NamaPelanggan LIKE '%$cari%' 
		OR AlamatPelanggan LIKE '%$cari%'
		OR ContactPerson LIKE '%$cari%'
		OR TelpCP LIKE '%$cari%')";
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,
	"tpp.KodePelanggan","tbl_paket_pelanggan tpp LEFT JOIN tbl_paket tp ON tpp.KodePelanggan=tp.KodePelanggan  AND tp.CetakTiket=1 AND tp.FlagBatal!=1 AND (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql')",
	"cari=$cari&status=$status&sort_by=$sort_by&order=$order","WHERE 1 AND $kondisi","paket.pelanggan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"NamaPelanggan":$sort_by;

$sql = 
	"SELECT
		tpp.*,
		IS_NULL(COUNT(tp.NoTiket),0) AS TotalPax,
		IS_NULL(SUM(tp.Diskon),0) AS TotalDiscount,
		IS_NULL(SUM(tp.TotalBayar),0) AS TotalBayar
	FROM tbl_paket_pelanggan tpp LEFT JOIN tbl_paket tp ON tpp.KodePelanggan=tp.KodePelanggan AND tp.CetakTiket=1 AND tp.FlagBatal!=1 AND (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql')
	WHERE 1 AND $kondisi
	GROUP BY tpp.KodePelanggan
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if(!$result = $db->sql_query($sql)){
	//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
	echo("Err: ".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick=\"Start('".append_sid('paket.pelanggan.laporan.detail.php?tglmulai='.$tgl_awal.'&tglakhir='.$tgl_akhir.'&kodepelanggan='.$row['KodePelanggan'])."');return false;\">Detail<a/>";		

	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$row['IsAktif']==1?$odd:"red",
				'no'=>$i,
				'kodepelanggan'=>$row['KodePelanggan'],
				'namapelanggan'=>$row['NamaPelanggan'],
				'alamat'=>$row['AlamatPelanggan'],
				'contactperson'=>$row['ContactPerson'],
				'telpcp'=>$row['TelpCP'],
				'discount'=>($row['BesarDiscount']>1)?"Rp.".number_format($row['BesarDiscount'],0,",","."):(($row['BesarDiscount']*100)."%"),
				'habiskontrak'=>FormatMySQLDateToTgl($row['TglHabisKontrak']),
				'totalpax'=>number_format($row['TotalPax'],0,",","."),
				'totaldiscount'=>number_format($row['TotalDiscount'],0,",","."),
				'totalbayar'=>number_format($row['TotalBayar'],0,",","."),
				'action'=>$act
			)
		);
	
	$i++;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&cari=$cari&status=$status&order=$order_invert";

$array_sort	= 
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=KodePelanggan'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=NamaPelanggan'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=AlamatPelanggan'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=ContactPerson'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=TelpCP'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=BesarDiscount'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=TglHabisKontrak'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=TotalPax'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=TotalDiscount'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.laporan.php?sort_by=TotalBayar'.$parameter_sorting)."'";
	
$template->assign_vars(array(
	'BCRUMP'    				=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_paket">Home</a> | <a href="'.append_sid('paket.pelanggan.laporan.'.$phpEx).'">Laporan Paket Pelanggan</a>',
	'ACTION_CARI'				=> append_sid('paket.pelanggan.laporan.'.$phpEx),
	'DISPLAY_NO_DATA'		=> $i-1>0?"none":"block",
	'TXT_CARI'					=> $cari,
	'ARRAY_SORT'				=> $array_sort,
	'TGL_AWAL'					=> $tgl_awal,
	'TGL_AKHIR'					=> $tgl_akhir,
	'PAGING'						=> $paging
	)
);
		    
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>