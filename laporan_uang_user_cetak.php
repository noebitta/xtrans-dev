<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CSO,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tanggal 	= $HTTP_GET_VARS['tgl'];
$username	= $userdata['username'];

	//mengambil data setoran CSO berdasarkan tanggal dan username CSO
	
	$sql=
	"SELECT CSO,nama,nrp,telp,hp,kode_cabang,dbo.f_CabangGetNameByKode(kode_cabang) AS cabang, 
		SUM(dbo.f_HitungPenjualanTunaiGoshow(Total,pesanan,jenispembayaran)) AS TotalTunaiGoshow, 
	  SUM(dbo.f_HitungPenjualanDebitGoshow(Total,pesanan,jenispembayaran)) AS TotalDebitGoshow, 
	  SUM(dbo.f_HitungPenjualanKreditGoshow(Total,pesanan,jenispembayaran)) AS TotalKreditGoshow, 
	  SUM(dbo.f_HitungPenjualanCpgtCGoshow(Total,pesanan,jenispembayaran)) AS TotalCpgtCGoshow, 
	  SUM(dbo.f_HitungPenjualanTunaiPesanan(Total,pesanan,jenispembayaran)) AS TotalTunaiPesanan, 
	  SUM(dbo.f_HitungPenjualanDebitPesanan(Total,pesanan,jenispembayaran)) AS TotalDebitPesanan, 
	  SUM(dbo.f_HitungPenjualanKreditPesanan(Total,pesanan,jenispembayaran)) AS TotalKreditPesanan, 
	  SUM(dbo.f_HitungPenjualanCpgtCPesanan(Total,pesanan,jenispembayaran)) AS TotalCpgtCPesanan, 
	  SUM(PesanKursi) AS tiket, SUM(discount) AS TotalDiscount,
	  SUM(Total) AS Total
	FROM TbReservasi INNER JOIN tbl_user ON tbl_user.username=TbReservasi.cso 
	WHERE (waktu_cetak_tiket BETWEEN CONVERT(datetime,'$tanggal 00:00:00',105) AND CONVERT(datetime,'$tanggal 23:59:59',105)) 
	  AND CetakTiket=1
	  AND (Setoran = 0 OR Setoran IS NULL) 
	  AND CSO='$username' 
	GROUP BY CSO,nama,nrp,telp,hp,kode_cabang,cabang";	
		
	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		
		$total_tunai_goshow		= $row['TotalTunaiGoshow'];
		$total_debit_goshow		= $row['TotalDebitGoshow'];
		$total_kredit_goshow	= $row['TotalKreditGoshow'];
		$total_cpgtc_goshow		= $row['TotalCpgtCGoshow'];
		$total_tunai_pesanan	= $row['TotalTunaiPesanan'];
		$total_debit_pesanan	= $row['TotalDebitPesanan'];
		$total_kredit_pesanan	= $row['TotalKreditPesanan'];
		$total_cpgtc_pesanan	= $row['TotalCpgtCPesanan'];
		$total_discount				= $row['TotalDiscount'];
		
		$total_setor	= $total_tunai_goshow+$total_debit_goshow+$total_kredit_goshow+$total_cpgtc_goshow+$total_tunai_pesanan+$total_debit_pesanan+$total_kredit_pesanan+$total_cpgtc_pesanan;
		$total_omzet	= $total_setor+$total_discount;
		$total_tunai	= $total_tunai_goshow+$total_tunai_pesanan;
		$total_debit	= $total_debit_goshow+$total_debit_pesanan;
		$total_kredit	= $total_kredit_goshow+$total_kredit_pesanan;
		$total_cpgtc	= $total_cpgtc_goshow+$total_cpgtc_pesanan;
		
		$template->set_filenames(array('body' => 'laporan_uang_user/laporan_uang_user_struk_body.tpl')); 
		$template->assign_vars(array(
			'TGL_SETOR'    				=> $row['TglSetor'],
			'NAMA'								=> $row['nama'],
			'NRP'									=> $row['nrp'],
			'CABANG'							=> $row['cabang'],
			'TELP'								=> $row['telp']."/".$row['hp'],
			'TGL_TRANSAKSI'				=> $tanggal,
			'TOTAL_TUNAI_GOSHOW'	=> number_format($total_tunai_goshow,0,",","."),
			'TOTAL_DEBIT_GOSHOW'	=> number_format($total_debit_goshow,0,",","."),
			'TOTAL_KREDIT_GOSHOW'	=> number_format($total_kredit_goshow,0,",","."),
			'TOTAL_CPGTC_GOSHOW'	=> number_format($total_cpgtc_goshow,0,",","."),
			'TOTAL_TUNAI_PESANAN'	=> number_format($total_tunai_pesanan,0,",","."),
			'TOTAL_DEBIT_PESANAN'	=> number_format($total_debit_pesanan,0,",","."),
			'TOTAL_KREDIT_PESANAN'=> number_format($total_kredit_pesanan,0,",","."),
			'TOTAL_CPGTC_PESANAN'	=> number_format($total_cpgtc_pesanan,0,",","."),
			'TOTAL_TUNAI'					=> number_format($total_tunai,0,",","."),
			'TOTAL_DEBIT'					=> number_format($total_debit,0,",","."),
			'TOTAL_KREDIT'				=> number_format($total_kredit,0,",","."),
			'TOTAL_CPGTC'					=> number_format($total_cpgtc,0,",","."),
			'TIKET'								=> number_format($row['tiket'],0,",","."),
			'TOTAL_SETOR'					=> number_format($total_setor,0,",","."),
			'TOTAL_DISCOUNT'			=> number_format($total_discount,0,",","."),
			'TOTAL_OMZET'					=> number_format($total_omzet,0,",","."),
			'CABANG_SETOR'				=> $row['CabangSetor'],
			'CSO'									=> $row['nama'],
			'KASIR'								=> ""
			)
		);
		$template->pparse('body');
	}
	else{
		$return="<font size=3 color='D0D0D0'>Gagal mengambil data</font>";
	}

?>