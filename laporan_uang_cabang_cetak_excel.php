<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$is_today  			= isset($HTTP_GET_VARS['is_today']);
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

/*if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}		*/	

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";


$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Nama":$sort_by;

//QUERY LAPORAN				
$sql=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		KodeCabangAsal AS KodeCabang,
		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalTunai,
		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalDebit,
		IS_NULL(SUM(IF(JenisPembayaran=2,Total,0)),0) AS TotalKredit,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),tr.HargaTiket)),0) AS TotalSetor, 
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM $tbl_reservasi tr LEFT JOIN tbl_md_jurusan tmj ON tr.IdJurusan=tmj.IdJurusan
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2 AND PetugasCetakTiket!=0
	GROUP BY KodeCabangAsal ORDER BY KodeCabangAsal ";
	
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND (CaraPembayaran<$PAKET_CARA_BAYAR_DI_TUJUAN OR (CaraPembayaran=$PAKET_CARA_BAYAR_DI_TUJUAN AND StatusDiambil=1))   AND FlagBatal!=1 $kondisi_cabang
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['KodeCabang']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		KodeCabang,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang
	GROUP BY KodeCabang ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Nama']								= $row['Nama'];
	$temp_array[$idx]['Alamat']							= $row['Alamat'];
	$temp_array[$idx]['Kota']								= $row['Kota'];
	$temp_array[$idx]['Telp']								= $row['Telp'];
	$temp_array[$idx]['Fax']								= $row['Fax'];
	$temp_array[$idx]['hp']									= $row['hp'];
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenjualanPaket']= $data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalPaket']					= $data_paket_total[$row['KodeCabang']]['TotalPaket'];
	$temp_array[$idx]['TotalBiaya']					= $data_paket_total[$row['KodeCabang']]['TotalBiaya'];
	$temp_array[$idx]['Total']							= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap Uang Cabang per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Kode');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Alamat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Total Omzet Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Total Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Total Omzet Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Disc');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'OP Langsung');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'L/R Kotor');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

$idx=0;
$idx_row=4;
while ($idx<count($temp_array)){
	
	//total tiket
	$total_penjualan_tiket	= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket						= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total biaya
	$total_biaya						= $temp_array[$idx]['TotalBiaya'];
	
	//total
	$total							= $temp_array[$idx]['Total'];
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['Kode']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Area']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $total_tiket!=''?$total_tiket:0);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $total_penjualan_tiket!=''?$total_penjualan_tiket:0);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $total_paket!=''?$total_paket:0);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $total_penjualan_paket!=''?$total_penjualan_paket:0);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $total_discount!=''?$total_discount:0);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $total_biaya!=''?$total_biaya:0);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $total!=''?$total:0);
	
	$idx_row++;
	$idx++;
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row,'=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(K4:K'.$temp_idx.')');
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Rekap Uang Cabang per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
