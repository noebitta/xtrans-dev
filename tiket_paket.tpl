<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript" src="./browser/bowser.min.js"></script>
<script type="text/javascript">

	nama_browser	= bowser.name;
	
	if(nama_browser.toUpperCase()!='FIREFOX'){
		alert("Anda harus menggunakan browser Mozilla Firefox.");
		window.close();
	}
	
  function print_doc()
  {
		//set printer default
		//var printers = jsPrintSetup.getPrintersList().split(',');
		//jsPrintSetup.setPrinter(printers[0]);
		jsPrintSetup.setOption('orientation', jsPrintSetup.kPortraitOrientation);
		//jsPrintSetup.undefinePaperSize(44);
		//jsPrintSetup.definePaperSize(44,119, 'resi_paket','resi_paket215x140', 'Resi Paket', 215, 140,jsPrintSetup.kPaperSizeMillimeters);
		//jsPrintSetup.setPaperSizeData(43);
		// set top margins in millimeters
		jsPrintSetup.setOption('marginTop', 5);
		jsPrintSetup.setOption('marginBottom', 5);
		jsPrintSetup.setOption('marginLeft', 5);
		jsPrintSetup.setOption('marginRight', 5);
		// set page header
		jsPrintSetup.setOption('headerStrLeft', '');
		jsPrintSetup.setOption('headerStrCenter', '');
		jsPrintSetup.setOption('headerStrRight', '');
		// set empty page footer
		jsPrintSetup.setOption('footerStrLeft', '');
		jsPrintSetup.setOption('footerStrCenter', '');
		jsPrintSetup.setOption('footerStrRight', '');
		// Suppress print dialog
		jsPrintSetup.setSilentPrint(false);/** Set silent printing */
		// Do Print
		//jsPrintSetup.print();
		jsPrintSetup.printWindow(window);
		// Restore print dialog
		//jsPrintSetup.setSilentPrint(false); /** Set silent printing back to false */
		window.close();
		
  }
	
</script>	 

<body class='tiket'>
<div style="font-size:12px;width:750px;line-height:12px;">
	<div style="height:30px;"></div>
	<table cellpadding="0" cellspacing="0" style="border-collapse: collapse;border-spacing: 0px;">
		<tr>
			<td style="font-size:18px;font-weight: bold;padding: 0px;" colspan="5">{TUJUAN} [{LAYANAN}]</td>
			<td style="font-size:14px;font-weight: bold;padding: 0px;" colspan="2" align="right">{NO_RESI}</td>
		</tr>
		<tr>
			<td colspan="7">Tanggal:{TANGGAL}|Jurusan: {JURUSAN}|Jadwal Kirim: {JAM_BERANGKAT}</td>
		</tr>
		<tr>
			<td colspan="7">Jenis Barang:{JENIS_BARANG}|Koli: {KOLI}|Berat: {BERAT} KG</td>
		</tr>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">PENGIRIM</td>
			<td colspan="2">PENERIMA</td>
		</tr>
		<tr>
			<td colspan="5">Nama:{NAMA_PENGIRIM}</td>
			<td colspan="2">Nama:{NAMA_PENERIMA}</td>
		</tr>
		<tr>
			<td colspan="5">Alamat:{ALAMAT_PENGIRIM}</td>
			<td colspan="2">Alamat:{ALAMAT_PENERIMA}</td>
		</tr>
		<tr>
			<td colspan="5">Telp:{TELP_PENGIRIM}</td>
			<td colspan="2" style="font-weight: bold;font-size: 14px;">Telp:{TELP_PENERIMA}</td>
		</tr>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
			<td>Harga</td>
			<td>:Rp.</td>
			<td align="right">{HARGA}</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>Isi:{KET_PAKET}</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td rowspan="3" style="font-size: 10px">Dengan menyetujui resi ini saya selaku pengirim
				telah membaca, memahami dan menyetujui
				seluruh syarat pengiriman yang tertera pada bagian
				belakang resi ini</td>
		</tr>
		<tr>
			<td>Diskon</td>
			<td>:Rp.</td>
			<td align="right">{DISKON}</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>Ins.Khusus:{INS_KHUSUS}</td>
		</tr>
		<tr>
			<td colspan="4">------------------------</td>
			<td></td>
		</tr>
		<tr>
			<td>Total</td>
			<td>:Rp.</td>
			<td align="right">{TOTAL}</td>
		</tr>
	</table>
	Jenis Pembayaran:{JENIS_BAYAR} | Waktu Cetak:{WAKTU_CETAK}<br><br>
	<div style="position: absolute;left: 0px;width: 150px;text-align: center;">CSO Asal</div>
	<div style="position: absolute;left: 155px;width: 150px;text-align: center;">Pengirim</div>
	<div style="position: absolute;left: 310px;width: 150px;text-align: center;">CSO Tujuan</div>
	<div style="position: absolute;left: 460px;width: 150px;text-align: center;">Penerima</div>
	<div style="position: absolute;left: 620px;width: 150px;text-align: left;">KTP:<br>Telp:<br>Tgl:<br>Jam:</div>
	<div style="height:60px;text-align: center;"></div>
	<div style="position: absolute;left: 0px;width: 150px;text-align: center;">{PETUGAS_CETAK}</div>
	<div style="position: absolute;left: 155px;width: 150px;text-align: center;">{NAMA_PENGIRIM}</div>
	<div style="position: absolute;left: 310px;width: 150px;text-align: center;"></div>
	<div style="position: absolute;left: 460px;width: 150px	;text-align: left;"></div>
</div>
</body>

<script language="javascript">
	print_doc();
</script>
</html>