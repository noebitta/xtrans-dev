<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_CSO,$LEVEL_CSO_PAKET))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//METHODS
function getDataSPJ($tgl_berangkat,$kode_jadwal){
	global $db;
	
	$sql=
		"SELECT 
			NoSPJ,KodeDriver,Driver,NoPolisi
		FROM tbl_spj
		WHERE KodeJadwal='$kode_jadwal' AND TglBerangkat='$tgl_berangkat'";

	if (!$result= $db->sql_query($sql)){
			echo("Error:".__LINE__);exit;
	}
	
	return $db->sql_fetchrow($result);
	
}

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$tanggal		= $HTTP_GET_VARS['p0'];
	
//INISIALISASI
$PengaturanUmum	= new PengaturanUmum();
$User						= new User();
$useraktif			= $userdata['user_id'];
$data_user			= $User->ambilDataDetail($useraktif);


$kondisi_tiket	=
	"DATE(WaktuCetakTiket)='$tanggal'
	AND CetakTiket=1 
	AND FlagBatal!=1 
	AND PetugasCetakTiket=$useraktif";

$kondisi_paket	=
	"DATE(WaktuPesan)='$tanggal'
	AND CetakTiket=1 
	AND FlagBatal!=1
	AND IF(CaraPembayaran!=$PAKET_CARA_BAYAR_DI_TUJUAN,PetugasPenjual=$useraktif,PetugasPemberi=$useraktif)";

	
$temp_tanggal_cari	= explode(" ",$tanggal);
$temp_tanggal_cari	= explode("-",$temp_tanggal_cari[0]);
$tahun_cari		= $temp_tanggal_cari[0];
$bulan_cari		= $temp_tanggal_cari[1];

$tanggal_sekarang	= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang		= $temp_tanggal_sekarang[0];
$bulan_sekarang		= $temp_tanggal_sekarang[1];

if($tahun_cari==$tahun_sekarang && $bulan_cari==$bulan_sekarang){
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else{
	$tbl_reservasi	= "tbl_reservasi_olap";
}
	

/*//mengambil total pendapatan dari asuransi
$Asuransi	= new Asuransi();
$total_pendapatan_asuransi	= $Asuransi->ambilTotalPendapatanAsuransi($tanggal,$tanggal,$useraktif);
*/

$data_perusahaan	= $PengaturanUmum->ambilDataPerusahaan();

$line_space	=0.3;

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
	
	var $javascript;
	var $n_js;

	function IncludeJS($script) {
	    $this->javascript=$script;
	}

	function _putjavascript() {
	    $this->_newobj();
	    $this->n_js=$this->n;
	    $this->_out('<<');
	    $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
	    $this->_out('>>');
	    $this->_out('endobj');
	    $this->_newobj();
	    $this->_out('<<');
	    $this->_out('/S /JavaScript');
	    $this->_out('/JS '.$this->_textstring($this->javascript));
	    $this->_out('>>');
	    $this->_out('endobj');
	}

	function _putresources() {
	    parent::_putresources();
	    if (!empty($this->javascript)) {
	        $this->_putjavascript();
	    }
	}

	function _putcatalog() {
	    parent::_putcatalog();
	    if (isset($this->javascript)) {
	        $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
	    }
	}
	
	function AutoPrint($dialog=false)
	{
	    //Embed some JavaScript to show the print dialog or start printing immediately
	    $param=($dialog ? 'true' : 'false');
	    $script="print($param);";
	    $this->IncludeJS($script);
	}
}
			
//set kertas & file
$pdf=new PDF('P','cm','spjkecil');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(0.1,0,0,0);
$pdf->SetFont('courier','',10);
		
// Header
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('courier','',13);
$pdf->Cell(6.4,$line_space,"REKAP SETORAN",'',0,'C');$pdf->Ln();$pdf->Ln();
$pdf->SetFont('courier','',11);
$pdf->Cell(6.4,$line_space,$data_perusahaan['PERUSH_NAMA'],'',0,'');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(6.4,$line_space,$data_perusahaan['PERUSH_ALAMAT'],'',0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,$data_perusahaan['PERUSH_TELP'],'',0,'');$pdf->Ln();

$pdf->SetFont('courier','',10);
//content
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,"CSO    :".$data_user['nama'],'',0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,"Tgl.Trx:".dateparse(FormatMySQLDateToTgl($tanggal)),'',0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();

//QUERY DATA TIKET
$sql=
	"SELECT 
		KodeJadwal,TglBerangkat,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS Asal,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan,
		JamBerangkat,
		COUNT(NoTiket) AS JumlahPenumpang,
		IS_NULL(SUM(IF(JenisPembayaran<3 OR JenisPembayaran=5,Total,0)),0) AS Total,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran<3 OR JenisPembayaran=5,Discount,0),0)),0) AS TotalDiscount,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND (JenisPembayaran<3 OR JenisPembayaran=5),NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND (JenisPembayaran<3 OR JenisPembayaran=5),NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND (JenisPembayaran<3 OR JenisPembayaran=5),NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND (JenisPembayaran<3 OR JenisPembayaran=5),NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND (JenisPembayaran<3 OR JenisPembayaran=5),NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='V',NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND (JenisPembayaran<3 OR JenisPembayaran=5),NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IdJurusan
	FROM $tbl_reservasi
	WHERE $kondisi_tiket
	GROUP BY KodeJadwal,JamBerangkat
	ORDER BY Asal,Tujuan,JamBerangkat ";

if (!$result_penumpang_detail = $db->sql_query($sql)){
	echo("Error:".__LINE__);exit;
}

//DETAIL DATA TIKET
$pdf->Ln();
$pdf->Cell(6.4,$line_space,'DETAIL PENUMPANG',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$jurusan_temp	="";

$sub_total_per_jurusan	= 0;

while ($data_penumpang_detail = $db->sql_fetchrow($result_penumpang_detail)){
	
	//MENGAMBIL DATA SPJ
	$data_spj	= getDataSPJ($data_penumpang_detail['TglBerangkat'],$data_penumpang_detail['KodeJadwal']);
	
	if($data_spj['NoSPJ']!=""){
		$ket_nospj	= $data_spj['NoSPJ'];
		$ket_driver	= $data_spj['KodeDriver']."|".$data_spj['Driver'];;
		$ket_nopol	= $data_spj['NoPolisi'];
	}
	else{
		$ket_nospj	= "Belum Cetak";
		$ket_driver	= "Belum Cetak";
		$ket_nopol	= "Belum Cetak";
	}
	
	if($jurusan_temp!=$data_penumpang_detail['IdJurusan']){
		if($jurusan_temp!=""){
			$pdf->Cell(3.4,$line_space,"Sub Total:",'',0,'');$pdf->Cell(3,$line_space,number_format($sub_total_per_jurusan,0,",",".").'  ','',0,'R');$pdf->Ln();
			$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();$pdf->Ln();
			$sub_total_per_jurusan	= 0;
		}
		else{
			//NOTHING
		}
		
		$pdf->Cell(1,$line_space,$data_penumpang_detail['Asal'].'-'.$data_penumpang_detail['Tujuan'],'',0,'L');$pdf->Ln();
		$jurusan_temp	= $data_penumpang_detail['IdJurusan'];
	}
	
	$pdf->Ln();
	$pdf->Cell(1,$line_space,"Jam:",'',0,'');$pdf->Cell(5.4,$line_space,$data_penumpang_detail['JamBerangkat'],'',0,'L');$pdf->Ln();
	$pdf->Cell(1,$line_space,$ket_driver,'',0,'L');$pdf->Ln();

	//list jenis penumpang
	$list_jenis_penumpang	= "";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangU']>0?"|U:".$data_penumpang_detail['TotalPenumpangU']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangM']>0?"|M:".$data_penumpang_detail['TotalPenumpangM']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangK']>0?"|K:".$data_penumpang_detail['TotalPenumpangK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangKK']>0?"|KK:".$data_penumpang_detail['TotalPenumpangKK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangG']>0?"|G:".$data_penumpang_detail['TotalPenumpangG']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangT']>0?"|O:".$data_penumpang_detail['TotalPenumpangT']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangR']>0?"|R:".$data_penumpang_detail['TotalPenumpangR']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangV']>0?"|V:".$data_penumpang_detail['TotalPenumpangV']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangVR']>0?"|VR:".$data_penumpang_detail['TotalPenumpangVR']:"";
	
	$pdf->Cell(1,$line_space,"Pnp:",'',0,'');$pdf->Cell(5.4,$line_space,$data_penumpang_detail['JumlahPenumpang'].$list_jenis_penumpang,'',0,'L');$pdf->Ln();

	$pdf->Ln();
	$pdf->Cell(3.4,$line_space,"Tunai:",'',0,'');$pdf->Cell(3,$line_space,number_format($data_penumpang_detail['Total'],0,",",".").'  ','',0,'R');$pdf->Ln();
	$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();

	$total_pendapatan_tunai	+= $data_penumpang_detail['Total'];
	$sub_total_per_jurusan	+= $data_penumpang_detail['Total'];
}

$pdf->Cell(3.4,$line_space,"Sub Total:",'',0,'');$pdf->Cell(3,$line_space,number_format($sub_total_per_jurusan,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();



//QUERY PAKET
$sql=
	"SELECT 
		NoTiket,LEFT(NamaPengirim,20) AS Nama,KodeJadwal,HargaPaket
	FROM tbl_paket
	WHERE $kondisi_paket
	ORDER BY DATE(WaktuPesan) ";

if (!$result_paket_detail = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//DETAIL DATA TIKET
$pdf->Ln();
$pdf->Cell(6.4,$line_space,'DETAIL PAKET',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();

$total_paket=0;
$total_omzet_paket=0;

while ($data_paket_detail = $db->sql_fetchrow($result_paket_detail)){
	$pdf->Cell(2.4,$line_space,"No.Tiket :",'',0,'');$pdf->Cell(4,$line_space,$data_paket_detail['NoTiket'],'',0,'L');$pdf->Ln();
	$pdf->Cell(2.4,$line_space,"Nama     :",'',0,'');$pdf->Cell(4,$line_space,$data_paket_detail['Nama'],'',0,'L');$pdf->Ln();
	$pdf->Cell(2.4,$line_space,"Kode Jdwl:",'',0,'');$pdf->Cell(4,$line_space,$data_paket_detail['KodeJadwal'],'',0,'L');$pdf->Ln();
	$pdf->Cell(2.4,$line_space,"Total    : Rp.",'',0,'');$pdf->Cell(4,$line_space,number_format($data_paket_detail['HargaPaket'],0,",",".").'  ','',0,'R');$pdf->Ln();
	$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
	
	$total_omzet_paket +=$data_paket_detail['HargaPaket'];
	$total_paket++;
}

$total_pendapatan_tunai += $total_omzet_paket;

$pdf->Ln();
$pdf->Cell(6.4,$line_space,'REKAP UANG PAKET',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Jum. Pkt:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_paket,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Tunai:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_omzet_paket,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Ln();

//QUERY ASURANSI
$sql=
  "SELECT
    NamaPlan,ta.BesarPremi,COUNT(1) AS JumlahPremi,SUM(ta.BesarPremi) AS TotalPremi
	FROM tbl_asuransi ta LEFT JOIN tbl_md_plan_asuransi tmpa ON ta.PlanAsuransi=tmpa.IdPlanAsuransi
	WHERE DATE(WaktuTransaksi)='$tanggal'
		AND PetugasTransaksi=$useraktif
	GROUP BY NamaPlan,BesarPremi ";

if (!$result_asuransi= $db->sql_query($sql)){
  //die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
  echo("Error:".__LINE__);exit;
}

if($db->sql_numrows($result_asuransi)>0) {
  $pdf->Cell(6.4, $line_space, 'ASURANSI', 0, 0, '');
  $pdf->Ln();
  $pdf->Cell(6.4, $line_space, '-----------------------------------------', 0, 0, '');
  $pdf->Ln();

  $total_jumlah_asuransi = 0;
  $total_omzet_asuransi = 0;
  while ($data_asuransi = $db->sql_fetchrow($result_asuransi)) {
    $pdf->Cell(4, $line_space, $data_asuransi["NamaPlan"] . "(Rp." . number_format($data_asuransi["BesarPremi"]) . "):", '', 0, '');
    $pdf->Cell(2, $line_space, number_format($data_asuransi["JumlahPremi"], 0, ",", "."), '', 0, 'R');
    $pdf->Ln();
    $total_jumlah_asuransi += $data_asuransi["JumlahPremi"];
    $total_omzet_asuransi += $data_asuransi["TotalPremi"];
  }

  $total_pendapatan_tunai += $total_omzet_asuransi;

  $pdf->Ln();
  $pdf->Cell(6.4, $line_space, '-----------------------------------------', 0, 0, '');
  $pdf->Ln();
  $pdf->Cell(3.4, $line_space, "Jum. Asuransi:", '', 0, '');
  $pdf->Cell(3, $line_space, number_format($total_jumlah_asuransi, 0, ",", ".") . '  ', '', 0, 'R');
  $pdf->Ln();
  $pdf->Cell(3.4, $line_space, "Tot. Asuransi:", '', 0, '');
  $pdf->Cell(3, $line_space, "Rp." . number_format($total_omzet_asuransi, 0, ",", ".") . '  ', '', 0, 'R');
  $pdf->Ln();
  $pdf->Ln();
}

/*$pdf->Cell(6.4,$line_space,'REKAP BIAYA',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Total Biaya:",'',0,'');$pdf->Cell(3,$line_space,"-".number_format($total_biaya,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Ln();

$pdf->Ln();
$pdf->Cell(6.4,$line_space,'REKAP UANG ASURANSI',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Uang Asuransi:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_pendapatan_asuransi,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Ln();
*/

$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"TOTAL SETORAN  :",'',0,'');$pdf->Cell(3,$line_space,number_format($total_pendapatan_tunai,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,"Tgl. Cetak:".dateparseWithTime(FormatMySQLDateToTglWithTime(dateNow(true))),'',0,'');$pdf->Ln();
$pdf->Ln();

//PESAN SPONSOR

$pesan_sponsor	= $PengaturanUmum->ambilPesanUntukDiTiket();

if(strlen($pesan_sponsor)>30){
	$arr_kata	= explode(" ",$pesan_sponsor);
	
	$temp_pesan_sponsor="";
	$jumlah_kata	= count($arr_kata);
	
	$idx	= 0;
	
	while($idx<$jumlah_kata){
		
		if(strlen($temp_pesan_sponsor." ".$arr_kata[$idx])<30){
			$temp_pesan_sponsor	= $temp_pesan_sponsor." ".$arr_kata[$idx];
			$idx++;
		}
		else{
			$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
			$temp_pesan_sponsor	= "";
			
		}
	}
	
	$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
}
else{
	$pdf->Cell(6.4,$line_space,$pesan_sponsor,0,0,'C');$pdf->Ln();
}

$pdf->Cell(6.4,$line_space,"-- Terima Kasih --",0,0,'C');$pdf->Ln();
$pdf->SetFont('courier','',8);
$pdf->Cell(6.4,$line_space,$data_perusahaan['EmailPerusahaan'],0,0,'C');$pdf->Ln();
$pdf->SetFont('courier','',8);
$pdf->Cell(6.4,$line_space,$data_perusahaan['WebSitePerusahaan'],0,0,'C');$pdf->Ln();

$db->sql_rowseek(0,$result_asuransi);

if($db->sql_numrows($result_asuransi)>0) {
  $pdf->Cell(6.4, $line_space,'-------------potong disini---------------', 0, 0, ''); $pdf->Ln();
  $pdf->Ln();
  $pdf->SetFont('courier','',13);
  $pdf->Cell(6.4,$line_space,"REKAP ASURANSI",'',0,'C');$pdf->Ln();$pdf->Ln();
  $pdf->SetFont('courier','',11);
  $pdf->Cell(6.4,$line_space,$data_perusahaan['PERUSH_NAMA'],'',0,'');$pdf->Ln();
  $pdf->SetFont('courier','',10);
  $pdf->Cell(6.4,$line_space,$data_perusahaan['PERUSH_ALAMAT'],'',0,'');$pdf->Ln();
  $pdf->Cell(6.4,$line_space,$data_perusahaan['PERUSH_TELP'],'',0,'');$pdf->Ln();

  $pdf->SetFont('courier','',10);
  //content
  $pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
  $pdf->Cell(6.4,$line_space,"CSO    :".$data_user['nama'],'',0,'');$pdf->Ln();
  $pdf->Cell(6.4,$line_space,"Tgl.Trx:".dateparse(FormatMySQLDateToTgl($tanggal)),'',0,'');$pdf->Ln();
  $pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();

  $total_jumlah_asuransi = 0;
  $total_omzet_asuransi = 0;

  while ($data_asuransi = $db->sql_fetchrow($result_asuransi)) {
    $pdf->Cell(4, $line_space, $data_asuransi["NamaPlan"] . "(Rp." . number_format($data_asuransi["BesarPremi"]) . "):", '', 0, '');
    $pdf->Cell(2, $line_space, number_format($data_asuransi["JumlahPremi"], 0, ",", "."), '', 0, 'R');
    $pdf->Ln();
    $total_jumlah_asuransi += $data_asuransi["JumlahPremi"];
    $total_omzet_asuransi += $data_asuransi["TotalPremi"];
  }

  $total_pendapatan_tunai += $total_omzet_asuransi;

  $pdf->Cell(6.4, $line_space, '-----------------------------------------', 0, 0, '');$pdf->Ln();
  $pdf->Cell(3.4, $line_space, "Jum. Asuransi:", '', 0, '');
  $pdf->Cell(3, $line_space, number_format($total_jumlah_asuransi, 0, ",", ".") . '  ', '', 0, 'R');$pdf->Ln();
  $pdf->Cell(3.4, $line_space, "Tot. Asuransi:", '', 0, '');
  $pdf->Cell(3, $line_space, "Rp." . number_format($total_omzet_asuransi, 0, ",", ".") . '  ', '', 0, 'R');$pdf->Ln();
  $pdf->Cell(6.4, $line_space, '-----------------------------------------', 0, 0, '');$pdf->Ln();
  $pdf->Cell(6.4,$line_space,"Tgl. Cetak:".dateparseWithTime(FormatMySQLDateToTglWithTime(dateNow(true))),'',0,'');$pdf->Ln();
  $pdf->Ln();
  $pdf->Ln();
}

$pdf->AutoPrint(true);
$pdf->Output();


?>