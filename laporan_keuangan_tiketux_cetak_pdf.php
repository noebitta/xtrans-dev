<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$fil_status = isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"WaktuCetakTiket":$sort_by;

switch($fil_status){
	case "0":
		$status	= "Booking";
		break;
	case "1":
		$status	= "Dibayar";
		break;
	case "2":
		$status	= "Batal";
		break;
	
}

//QUERY
$sql	= 
	"SELECT *,f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM v_laporan_keuangan_tiketux".$userdata['user_id']."
	ORDER BY $sort_by $order";
		
if(!$result = $db->sql_query($sql)){
	die_error('Err:',__LINE__);
}  
		
$i=1;

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,40);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Penjualan Tiketux','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Filter Asal: '.$asal.', Tujuan: '.$tujuan.', Status Tiket: '.$status,'',0,'L');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(15,5,'#','B',0,'C',1);
$pdf->Cell(24,5,'Wkt.trx','B',0,'C',1);
$pdf->Cell(30,5,'#Tiket','B',0,'C',1);
$pdf->Cell(24,5,'Wkt.Brgkt','B',0,'C',1);
$pdf->Cell(20,5,'Kd.Jdwl','B',0,'C',1);
$pdf->Cell(30,5,'Nama','B',0,'C',1);
$pdf->Cell(20,5,'#Kursi','B',0,'C',1);
$pdf->Cell(15,5,'Harga','B',0,'C',1);
$pdf->Cell(15,5,'Disc','B',0,'C',1);
$pdf->Cell(15,5,'Total','B',0,'C',1);
$pdf->Cell(30,5,'Tp.Disc','B',0,'C',1);
$pdf->Cell(16,5,'Sts','B',0,'C',1);
$pdf->Cell(30,5,'Ket.','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);

$idx=0;

$sum_harga		= 0;
$sum_discount	= 0;
$sum_total		= 0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	
	if($row['FlagBatal']!=1){
	
		if($row['CetakTiket']!=1){
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		
		$keterangan="";
		
	}
	else{
		
		$status	= "BATAL ".$row['CetakTiket'];
		
		$keterangan	= $row['NamaCSOPembatalan'];
		
	}

	$pdf->Cell(15,5,$idx,'',0,'L');
	$pdf->MultiCell2(24,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),'','C');
	$pdf->Cell(30,5,$row['NoTiket'],'',0,'C');
	$pdf->MultiCell2(24,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),'','C');
	$pdf->MultiCell2(20,5,$row['KodeJadwal'],'','C');
	$pdf->MultiCell2(30,5,$row['Nama'],'','C');
	$pdf->Cell(20,5,$row['NomorKursi'],'',0,'C');
	$pdf->Cell(15,5,number_format($row['HargaTiket'],0,",","."),'',0,'R');
	$pdf->Cell(15,5,number_format($row['Discount'],0,",","."),'',0,'R');
	$pdf->Cell(15,5,number_format($row['Total'],0,",","."),'',0,'R');
	$pdf->MultiCell2(30,5,$row['JenisDiscount'],'','C');
	$pdf->Cell(16,5,$status,'',0,'C');
	$pdf->MultiCell2(30,5,$keterangan,'','C');
	$pdf->Ln();
	
	$sum_harga		+= $row['HargaTiket'];
	$sum_discount	+= $row['Discount'];
	$sum_total		+= $row['Total'];
	
}
	
$pdf->Ln();

//FOOTER
$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
#$pdf->Cell(5,5,'','B',0,'C',1);
$pdf->Cell(100,5,'TOTAL','B',0,'C',1);$pdf->Ln();
$pdf->SetTextColor(0);
$pdf->Cell(100,5,"Total Tiket: ".number_format($idx,0,",","."),'',0,'R');$pdf->Ln();
$pdf->Cell(100,5,"Total Penjualan: Rp. ".number_format($sum_harga,0,",","."),'',0,'R');$pdf->Ln();
$pdf->Cell(100,5,"Total Discount: Rp. -".number_format($sum_discount,0,",","."),'',0,'R');$pdf->Ln();
$pdf->SetTextColor(255);
$pdf->Cell(100,5,"Grand Total: Rp. ".number_format($sum_total,0,",","."),'B',0,'R',1);$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
									
$pdf->Output(); 


?>
