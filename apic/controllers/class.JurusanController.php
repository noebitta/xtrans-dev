<?php
/**
 * Jurusan controller.
 *
 * Last update: Feb 09, 2012 23:00
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class JurusanController extends Controller
{
	private $_jurusanModel;

	public function __construct()
	{
		parent::__construct();

		$this->_jurusanModel = $this->loadModel('Jurusan');
	}

	/**
	 * Mendapatkan daftar cabang.
	 *
	 * URL: GET /cabang
	 *
	 * Paramaters: -
	 *
	 * Requires authentication: YES
	 *
	 * HTTP method: GET
	 */
	public function index()
	{
		$this->setRequestMethod('GET');
		//$this->authenticate(1);

		$data		= array();
		$jurusans	= array();
		$results    = array();
		$status		= 'ZERO_RESULTS';

		$jurusanList = $this->_jurusanModel->getList();

		if (is_array($jurusanList) && sizeof($jurusanList)) {
			$status	 = 'OK';

			for ($i = 0; $i < sizeof($jurusanList); $i++) {
				$jurusans[$i]['id'] 	 			= (int) $jurusanList[$i]->IdJurusan;
				$jurusans[$i]['kode'] 	 			= $jurusanList[$i]->KodeJurusan;
				$jurusans[$i]['cabang_asal'] 	 	= $jurusanList[$i]->KodeCabangAsal;
				$jurusans[$i]['cabang_tujuan'] 	 	= $jurusanList[$i]->KodeCabangTujuan;
				$jurusans[$i]['harga_tiket'] 	 	= (int) $jurusanList[$i]->HargaTiket;
				$jurusans[$i]['harga_tiket_tuslah'] = (int) $jurusanList[$i]->HargaTiketTuslah;
				$jurusans[$i]['flag_tiket_tuslah']  = (int) $jurusanList[$i]->FlagTiketTuslah;
				$jurusans[$i]['flag_aktif'] 		= (int) $jurusanList[$i]->FlagAktif;
			}

			$results = array('jurusan' => $jurusans);
		}

		$data['results']	= $results;
		$data['status'] 	= $status;

		$this->sendResponse($data);
	}
}
?>