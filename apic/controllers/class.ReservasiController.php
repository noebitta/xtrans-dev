<?php
/**
 * Reservasi controller.
 *
 * Last update: Feb 09, 2012 22:33
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class ReservasiController extends Controller
{
	private $_reservasiModel;
	private $_jadwalModel;
	private $_posisiModel;

	public function __construct()
	{
		parent::__construct();

		$this->_reservasiModel = $this->loadModel('Reservasi');
		$this->_jadwalModel    = $this->loadModel('Jadwal');
		$this->_posisiModel    = $this->loadModel('Posisi');
	}

	/**
	 * Medapatkan nomor kursi yang telah dibooking.
	 *
	 * URL: GET /reservasi/kursi
	 *
	 * Parameters:
	 * - kodejadwal = kode jadwal, ex: DU-CPT06
	 * - tanggal = tanggal keberangkatan, format: yyyy-mm-dd
	 *
	 * Requires authentication: YES
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: GET
	 */
	public function kursi()
	{
		$this->setRequestMethod('GET');
		//$this->authenticate(1);

		$kode       = $_GET['kodejadwal'];
		$tanggal    = $_GET['tanggal'];

		$data       = array();
		$results    = array();
		$status     = 'ZERO_RESULTS';

		$nomorKursi = $this->_reservasiModel->getPosisiKursi($kode, $tanggal);

		if (!empty($nomorKursi)) {
			$status	 = 'OK';
			$results = array('nomorkursi' => $nomorKursi);
		}

		$data['results'] = $results;
		$data['status']  = $status;

		$this->sendResponse($data);
	}

	/**
	 * Insert data reservasi.
	 *
	 * URL: /reservasi/add
	 *
	 * Parameters:
	 *
	 * Requires authentication: YES
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: POST
	 */
	public function add()
	{
		$this->setRequestMethod('POST');
		//$this->authenticate(1);

		$kodeCabang		= $_POST['kode_cabang'];
		$kodeJadwal     = $_POST['kode_jadwal'];
		$idJurusan		= $_POST['id_jurusan'];
		$tglBerangkat	= $_POST['tgl_berangkat'];
		$jamBerangkat	= $_POST['jam_berangkat'];
		$namaPemesan	= $_POST['nama_pemesan'];
		$alamatPemesan	= $_POST['alamat_pemesan'];
		$telpPemesan	= $_POST['telp_pemesan'];
		$waktuPesan		= $_POST['waktu_pesan'];
		$nomorKursi		= $_POST['nomor_kursi']; //separated by coma (1,2,3)
		$namaPenumpang  = $_POST['nama_penumpang']; //separated by coma (1,2,3)
		$hargaTiket		= $_POST['harga_tiket'];
		$totalHarga		= $_POST['total_harga'];
		$subTotal		= $_POST['sub_total'];

		try {
			$nomorKursis    = explode(',', $nomorKursi);
			$namaPenumpangs = explode(',', $namaPenumpang);

		//	$this->dbObj->beginTrans();

			if (is_array($nomorKursis) && sizeof($nomorKursis)) {
				$data 			= array();
				$data2 			= array();
				$noTikets 		= array();
				$kodeBooking	= '';
				$nomorTikets    = '';

				$kodeBooking	= $this->_createKodeBooking($namaPemesan . (rand(1,10) * $nomorKursis[0]));

				for ($i = 0; $i < sizeof($nomorKursis); $i++) {
					$nomorTiket 	= $this->_createKodeTiket($namaPemesan . (rand(1,10) * $nomorKursis[$i]));
					$noTikets[$i] 	= $nomorTiket;

					$data       = array();
					$data[] 	= "NoTiket          = '$nomorTiket'";
					$data[]		= "KodeCabang       = '$kodeCabang'";
					$data[]		= "KodeJadwal       = '$kodeJadwal'";
					$data[]		= "IdJurusan        = '$idJurusan'";
					$data[]		= "TglBerangkat     = '$tglBerangkat'";
					$data[]		= "JamBerangkat     = '$jamBerangkat'";
					$data[]		= "KodeBooking      = '$kodeBooking'";
					$data[]		= "Nama             = '$namaPemesan'";
					$data[]     = "Alamat           = '$alamatPemesan'";
					$data[]     = "HP               = '$telpPemesan'";
					$data[]		= "Telp             = '$telpPemesan'";
					$data[]     = "WaktuPesan       = '$waktuPesan'";
					$data[]     = "NomorKursi       = '$nomorKursis[$i]'";
					$data[]     = "PetugasPenjual   = 0";
					$data[]     = "HargaTiket       = '$hargaTiket'";
					$data[]     = "Total            = '$totalHarga'";
					$data[]     = "SubTotal         = '$subTotal'";

					$this->dbObj->insertRecord('tbl_reservasi', $data);

					$data2[]    = "K$nomorKursis[$i] = 1";
					$data2[]    = "T$nomorKursis[$i] = '$nomorTiket'";
					$data2[]    = "Nama$nomorKursis[$i] = '$namaPenumpangs[$i]'";
				}

				$nomorTikets  = implode(',', $noTikets);

				if ($this->_posisiModel->exist($kodeJadwal, $tglBerangkat)) {
					$this->dbObj->updateRecord('tbl_posisi', $data2, array("KodeJadwal = '$kodeJadwal'",
																		   "TglBerangkat = '$tglBerangkat'"));
				} else {
					$jumlahKursi = $this->_jadwalModel->getJumlahKursi($kodeJadwal);
					$sisaKursi   = $jumlahKursi - sizeof($nomorKursis);

					$data2[] = "KodeJadwal    = '$kodeJadwal'";
					$data2[] = "TglBerangkat  = '$tglBerangkat'";
					$data2[] = "jamBerangkat  = '$jamBerangkat'";
					$data2[] = "JumlahKursi   = '$jumlahKursi'";
					$data2[] = "SisaKursi     = '$sisaKursi'";
					$data2[] = "SessionTime   = '" . date('Y-m-d H:i:s') . "'";

					$this->dbObj->insertRecord('tbl_posisi', $data2);
				}
			}

		//	$this->dbObj->completeTrans();

			$results	= array('kode_booking' => $kodeBooking, 'no_tiket' => $nomorTikets);
			$response 	= array('status' => 'OK', 'results' => $results);
		} catch (DbException $ex) {
			$this->dbObj->rollbackTrans();

			Error::store('Reservasi', $ex->getMessage());

			$response = array('status' => 'ERROR', 'error' => 'Database error');
		}

		$this->sendResponse($response);
	}

	/**
	 * Membuat kode booking
	 *
	 * @param string $data Data
	 *
	 * @return string Kode booking
	 */
	private function _createKodeBooking($data)
	{
		list($usec,$sec) = explode(' ',microtime());

		$str    = dechex($usec).dechex($sec);
		$str	= substr(md5($str.$data).'4A',9,-18).'TTX';
		$val	= strtoupper($str);

		return $val;
	}

	/**
	 * Membuat nomor tiket
	 *
	 * @param string $data Data
	 *
	 * @return string Nomor tiket
	 */
	function _createKodeTiket($data)
	{
		list($usec,$sec) = explode(' ',microtime());
        $key    = dechex($usec).dechex($sec);

		$data   = $data . $key;
		$str	= "TM".substr(md5($data).'4A',9,-18).'TTX';
		$val	= strtoupper($str);

		return $val;
	}
}
?>