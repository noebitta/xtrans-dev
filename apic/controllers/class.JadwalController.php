<?php
/**
 * Jadwal controller.
 *
 * Last update: Feb 09, 2012 22:44
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */
class JadwalController extends Controller
{
	private $_jadwalModel;

	public function __construct()
	{
		parent::__construct();

		$this->_jadwalModel = $this->loadModel('Jadwal');
	}

	/**
	 * Medapatkan jadwal keberangkatan untuk jurusan dan tanggal tertentu.
	 *
	 * URL: GET /jadwal
	 *
	 * Parameters:
	 * - jurusan = id jurusan
	 * - tanggal = tanggal keberangkatan, format: yyyy-mm-dd
	 *
	 * Requires authentication: YES
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: GET
	 */
	public function index()
	{
		$this->setRequestMethod('GET');

		$jurusan 	= $_GET['jurusan'];
		$tanggal    = $_GET['tanggal'];

		$data		= array();
		$jadwals	= array();
		$results	= array();
		$status		= 'ZERO_RESULTS';

		$jadwalList      = $this->_jadwalModel->getListByJurusan($jurusan);
		$penjadwalanList = $this->_jadwalModel->getPenjadwalanList($jurusan, $tanggal);
		$reservasiList   = $this->_jadwalModel->getReservasiList($jurusan, $tanggal);

		if (is_array($jadwalList) && sizeof($jadwalList)) {
			$status	 = 'OK';
			$j       = 0;

			for ($i = 0; $i < sizeof($jadwalList); $i++) {
				$kode = $jadwalList[$i]->KodeJadwal;

				if (!$this->_jadwalAktif($penjadwalanList, $kode)) {
					continue;
				}

				$jumlahBooking = $this->_getJumlahBooking($reservasiList, $kode);

				$jadwals[$j]['kode'] 	 		 = $kode;
				$jadwals[$j]['jam_berangkat'] 	 = $jadwalList[$i]->JamBerangkat;
				$jadwals[$j]['jumlah_kursi'] 	 = (int) $jadwalList[$i]->JumlahKursi;
				$jadwals[$j]['jumlah_booking'] 	 = (int) $jumlahBooking;

				$j++;
			}

			$results = array('jadwal' => $jadwals);
		}

		$data['results']	= $results;
		$data['status'] 	= $status;

		$this->sendResponse($data);
	}

	/**
	 * Cek jadwal aktif
	 *
	 * @param array $list Daftar penjadwalan
	 * @param string $kode Kode jadwal
	 *
	 * @return boolean TRUE jika aktif dan sebaliknya.
	 */
	private function _jadwalAktif($list, $kode)
	{
		$res = true;

		if (is_array($list) && sizeof($list)) {
			for ($i = 0; $i < sizeof($list); $i++) {
				if ($kode == $list[$i]->KodeJadwal && $list[$i]->StatusAktif != 1) {
					$res = false;

					break;
				}
			}
		}

		return $res;
	}

	/**
	 * Mendapatkan jumlah booking untuk suatu kode keberangkatan
	 *
	 * @param array $list Daftar reservasi
	 * @param string $kode Kode jadwal
	 *
	 * @return int Jumlah booking
	 */
	private function _getJumlahBooking($list, $kode)
	{
		$res = 0;

		if (is_array($list) && sizeof($list)) {
			for ($i = 0; $i < sizeof($list); $i++) {
				if ($list[$i]->KodeJadwal == $kode) {
					$res = $list[$i]->JumlahBooking;

					break;
				}
			}
		}

		return $res;
	}
}
?>