<?php
/**
 * Sync controller.
 *
 * Last update: Feb 09, 2012 22:44
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class SyncController extends Controller
{
	private $_cabangModel;
	private $_jurusanModel;
	private $_jadwalModel;

	public function __construct()
	{
		parent::__construct();

		$this->_cabangModel  = $this->loadModel('Cabang');
		$this->_jurusanModel = $this->loadModel('Jurusan');
		$this->_jadwalModel	 = $this->loadModel('Jadwal');
	}

	/**
	 * Mendapatkan daftar cabang.
	 *
	 * URL: GET /cabang
	 *
	 * Paramaters: -
	 *
	 * Requires authentication: YES
	 *
	 * HTTP method: GET
	 */
	public function cabang()
	{
		$this->setRequestMethod('GET');
		//$this->authenticate(1);

		$data		= array();
		$cabangs	= array();
		$results	= array();
		$status		= 'ZERO_RESULTS';

		$cabangList = $this->_cabangModel->getList();

		if (is_array($cabangList) && sizeof($cabangList)) {
			$status	 = 'OK';

			for ($i = 0; $i < sizeof($cabangList); $i++) {
				$cabangs[$i]['kode'] 	  = $cabangList[$i]->KodeCabang;
				$cabangs[$i]['nama'] 	  = firstCapital($cabangList[$i]->Nama);
				$cabangs[$i]['alamat'] 	  = firstCapital($cabangList[$i]->Alamat);
				$cabangs[$i]['kota'] 	  = firstCapital($cabangList[$i]->Kota);
				$cabangs[$i]['telpon'] 	  = $cabangList[$i]->Telp;
				$cabangs[$i]['fax'] 	  = $cabangList[$i]->Fax;
				$cabangs[$i]['flag_agen'] = (int) $cabangList[$i]->FlagAgen;
			}

			$results = array('cabang' => $cabangs);
		}

		$data['results'] = $results;
		$data['status']  = $status;

		$this->sendResponse($data);
	}

	/**
	 * Mendapatkan daftar jurusan yang aktif.
	 *
	 * URL: GET /jurusan
	 *
	 * Paramaters: -
	 *
	 * Requires authentication: YES
	 *
	 * HTTP method: GET
	 */
	public function jurusan()
	{
		$this->setRequestMethod('GET');
		//$this->authenticate(1);

		$data		= array();
		$jurusans	= array();
		$results    = array();
		$status		= 'ZERO_RESULTS';

		$jurusanList = $this->_jurusanModel->getList();

		if (is_array($jurusanList) && sizeof($jurusanList)) {
			$status	 = 'OK';

			for ($i = 0; $i < sizeof($jurusanList); $i++) {
				$jurusans[$i]['id'] 	 			= (int) $jurusanList[$i]->IdJurusan;
				$jurusans[$i]['kode'] 	 			= $jurusanList[$i]->KodeJurusan;
				$jurusans[$i]['cabang_asal'] 	 	= $jurusanList[$i]->KodeCabangAsal;
				$jurusans[$i]['cabang_tujuan'] 	 	= $jurusanList[$i]->KodeCabangTujuan;
				$jurusans[$i]['harga_tiket'] 	 	= (int) $jurusanList[$i]->HargaTiket;
				$jurusans[$i]['harga_tiket_tuslah'] = (int) $jurusanList[$i]->HargaTiketTuslah;
				$jurusans[$i]['flag_tiket_tuslah']  = (int) $jurusanList[$i]->FlagTiketTuslah;
				$jurusans[$i]['flag_aktif'] 		= (int) $jurusanList[$i]->FlagAktif;
			}

			$results = array('jurusan' => $jurusans);
		}

		$data['results']	= $results;
		$data['status'] 	= $status;

		$this->sendResponse($data);
	}

	/**
	 * Medapatkan seluruh jadwal keberangkatan aktif.
	 *
	 * URL: GET /jadwal
	 *
	 * Parameters:-
	 *
	 * Requires authentication: YES
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: GET
	 */
	public function jadwal()
	{
		$this->setRequestMethod('GET');
		//$this->authenticate(1);
//                echo "ASU";
		$data		= array();
		$jadwals	= array();
		$results	= array();
		$status		= 'ZERO_RESULTS';

		$jadwalList = $this->_jadwalModel->getList();

		if (is_array($jadwalList) && sizeof($jadwalList)) {
			$status	 = 'OK';
			$j       = 0;

			for ($i = 0; $i < sizeof($jadwalList); $i++) {
				$kode = $jadwalList[$i]->KodeJadwal;

				$jadwals[$i]['kode'] 	 		= $kode;
				$jadwals[$i]['id_jurusan'] 	 	= (int) $jadwalList[$i]->IdJurusan;
				$jadwals[$i]['jam_berangkat'] 	= $jadwalList[$i]->JamBerangkat;
				$jadwals[$i]['jumlah_kursi'] 	= (int) $jadwalList[$i]->JumlahKursi;
				$jadwals[$i]['flag_aktif'] 		= (int) $jadwalList[$i]->FlagAktif;
			}

			$results = array('jadwal' => $jadwals);
		}

		$data['results']	= $results;
		$data['status'] 	= $status;

		$this->sendResponse($data);
	}
}
?>
