<?php
/**
 * Database config file.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

$cfg['db']['host']     = 'localhost';
$cfg['db']['db']       = 'tiketux-bimo';
$cfg['db']['user']     = 'root';
$cfg['db']['password'] = 'root@bimotrans';