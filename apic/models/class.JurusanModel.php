<?php
/**
 * Jurusan model.
 *
 * Last update: Feb 09, 2012 22:57
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class JurusanModel extends Model
{
	public function __construct()
	{
		parent::__construct();

		$this->_table = 'tbl_md_jurusan';
	}

	/**
	 * Mendapatkan daftar jurusan.
	 *
	 * @return array Daftar jurusan
	 */
	public function getList()
	{
		return $this->findAll();
	}
}
?>