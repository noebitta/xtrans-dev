<?php
/**
 * Cabang model.
 *
 * Last update: Feb 10, 2012 13:31
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class JadwalModel extends Model
{
	public function __construct()
	{
		parent::__construct();

		$this->_table = 'tbl_md_jadwal';
	}

	/**
	 * Mendapatkan daftar jadwal aktif
	 *
	 * @return object Daftar jadwal
	 */
	public function getList()
	{
		return $this->findAll(array('filter' => array('FlagAktif = 1', "KodeJadwal NOT LIKE '%-E1%'")));
	}

	/**
	 * Mendapatkan daftar jadwal untuk suatu jurusan
	 *
	 * @param int $jurusan Id Jurusan
	 *
	 * @return object Daftar jurusan
	 */
	public function getListByJurusan($jurusan)
	{
		global $cfg;

		$sql = "SELECT
						*
				FROM
						tbl_md_jadwal
				WHERE
						IdJurusan = '$jurusan'
						AND
						FlagAktif = 1
						AND
						KodeJadwal NOT LIKE '%-E1%'
				ORDER BY
						JamBerangkat ASC";

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan daftar penjadwalan untuk jurusan tertentu dan tanggal tertentu
	 *
	 * @param int $jurusan Id Jurusan
	 * @param string $tanggal Tanggal keberangkatan
	 *
	 * @return array Daftar penjadwalan
	 */
	public function getPenjadwalanList($jurusan, $tanggal)
	{
		global $cfg;

		$sql = "SELECT
						*
				FROM
						tbl_penjadwalan_kendaraan
				WHERE
						IdJurusan = '$jurusan'
						AND
						TglBerangkat = '$tanggal'
				ORDER BY
						JamBerangkat ASC";

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan data reservasi untuk jurusan dan tanggal tertentu
	 *
	 * @param int $jurusan Id jurusan
	 * @param string $tanggal Tanggal keberangkatan
	 *
	 * @return array Daftar reservasi
	 */
	public function getReservasiList($jurusan, $tanggal)
	{
		global $cfg;

		$sql = "SELECT
						COUNT(j.KodeJadwal) AS JumlahBooking,
						j.KodeJadwal
				FROM
						tbl_md_jadwal j
				JOIN
						tbl_reservasi r
				USING(KodeJadwal)
				WHERE
						j.IdJurusan = '$jurusan'
						AND
						r.TglBerangkat = '$tanggal'
						AND
						FlagBatal != 1
				GROUP BY
						j.KodeJadwal";

		$res = array();

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	public function getPosisiKursi($kode, $tanggal)
	{
		global $cfg;

		$sql = "SELECT
						GROUP_CONCAT(CONVERT(NomorKursi, CHAR(1)) ORDER BY NomorKursi)  AS NomorKursi
				FROM
						tbl_reservasi
				WHERE
						KodeJadwal = '$kode'
						AND
						TglBerangkat ='$tanggal'
				GROUP BY
						KodeJadwal";

		$res = array();

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan jumlah kursi dari suatu kode jadwal.
	 *
	 * @param string $kode Kode jadwal
	 *
	 * @return int Jumlah kursi
	 */
	public function getJumlahKursi($kode)
	{
		global $cfg;

		$sql = "SELECT
						JumlahKursi
				FROM
						tbl_md_jadwal
				WHERE
						KodeJadwal = '$kode'";

		$res = 0;

		try {
			$this->_dbObj->query($sql);

			$data = $this->_dbObj->fetch();
			$res  = $data->JumlahKursi;
		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}
}
?>