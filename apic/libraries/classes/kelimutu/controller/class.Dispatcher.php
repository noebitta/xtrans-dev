<?php
/**
 * Kelimutu API Framework
 *
 * Last updated: May 29, 2012, 15:40
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 */

/**
 * Loader class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.Loader.php');

/**
 * SystemException class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.SystemException.php');

/**
 * Controller class
 */
require_once(CLASS_DIR . '/kelimutu/controller/class.Controller.php');


/**
 * Dispatcher class
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 *
 */
class Dispatcher
{
    /**
     * Controller
     *
     * @var string
     */
    private $_controller;

    /**
     * Action
     *
     * @var string
     */
    private $_action;

    /**
     * Response type, xml or json
     *
     * @var string
     */
    private $_contentType;

    /**
     * Enable debug
     */
    private $_enableDebug = false;


    /**
     * Dispatch url
     *
     * @return void
     */
    public function dispatch()
    {
        $this->_parseURL($_GET['url']);
        //echo $_GET['url'];exit;
        //default controller
        if (empty($this->_controller)) {
            $this->_controller = 'Home';
        }
         
        $this->_controller = ucfirst($this->_controller) . 'Controller';

        try {
            Loader::loadClass(ucfirst($this->_controller) , ROOT_DIR . '/controllers');

            if (class_exists($this->_controller)) {
                $controllerObj = new $this->_controller;

                $controllerObj->setContentType($this->_contentType);
                $controllerObj->enableDebug($this->_enableDebug);

                if (method_exists($controllerObj, $this->_action)) {
                    $action = $this->_action;

                    $controllerObj->$action();

                    $controllerObj->parseLog();
                } else {
                    Error::store('Controller', "Method '" . $this->_action . "' for controller '"
                                 . $this->_controller ."' not found!");

                    $this->_requestNotFound();
                }
            } else {
                Error::store('Controller', "Controller class '" . $this->_controller . "' was not found!");

                $this->_requestNotFound();
            }
        } catch (SystemException $e) {
            Error::store('Controller', "Controller file '" . $this->_controller . "' was not found!");

            $this->_requestNotFound();
        }
    }

    /**
     * Enable debugging
     *
     * @param boolean $enable Enable debug (TRUE|FALSE)
     *
     * @return void
     */
    public function enableDebug($enable)
    {
        $this->_enableDebug = $enable;
    }

    /**
     * Show request not found error (HTTP 404)
     *
     * @return void
     */
    private function _requestNotFound()
    {
        $controllerObj = new Controller();
        
        $controllerObj->enableDebug($this->_enableDebug);
        $controllerObj->setContentType($this->_contentType);
        $controllerObj->sendResponse('', 404, 'The requested url was not found');
    }

    /**
     * Parse URL
     *
     * @param string $url URL
     *
     * @return void
     */
    private function _parseURL($url)
    {
        $turl = $url;

        if (strpos($url, '.')) {
            $turl       = substr($url, 0, strpos($url, '.'));
            $response   = substr($url, strpos($url, '.') + 1);
        }

        $route = Router::get('/' . $turl);

        if (!empty($route)) {
            if (!empty($route['controller'])) {
                $this->_controller = $route['controller'];
            }

            if (!empty($route['action'])) {
                $this->_action = $route['action'];
            }

            $this->_contentType = (empty($response)) ? 'html' : $response;
        } else {
            $url = strtolower($url);

            if (strpos($url, "/") === FALSE) {
                $this->_controller = $url;

                if (strpos($url, ".")) {
                    $urls = explode(".", $url);

                    $this->_controller  = $urls[0];
                    $this->_contentType = $urls[1];
                    $this->_action      = 'index';
                }
            } else {
                $urls = explode("/", $url);

                $this->_controller  = $urls[0];
                $this->_action      = '';
                $this->_contentType = '';

                if (sizeof($urls) > 1) {
                    $action         = $urls[1];
                    $this->_action  = $action;

                    if (strpos($action, ".")) {
                        $actions            = explode(".", $action);

                        $this->_action      = $actions[0];
                        $this->_contentType = $actions[1]; ;
                    } else {
                        if ($action == 'xml' || $action == 'json') {
                            $this->_contentType = $action;
                        } else {
                            $this->_action = $action;
                        }
                    }
                }
            }
        }
    }
}
?>
