<?php
/**
 * Kelimutu API Framework
 *
 * Last updated: Feb 10, 2012, 12:37
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 */


/**
 * Router class
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 *
 */
final class Router
{
    /**
     * Routes
     *
     * @var array
     */
    static private $_routes = array();

    /**
     * Add route
     *
     * @param string $path Route path
     * @param array $controller Controller handler
     *
     * @return void
     */
    static public function add($path, $controller)
    {  
        self::$_routes[$path] = $controller;
    }

    /**
     * Get route
     *
     * @param string $path Route path
     *
     * @return array Controller handler
     */
    static public function get($path)
    {
        return (array_key_exists($path, self::$_routes)) ? self::$_routes[$path] : NULL;
    }
}
?>
