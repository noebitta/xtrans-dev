<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassMemberTransaksi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER

$cso	= $userdata['nama']." ($userdata[username])";
					
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);
		
$id_member 	= trim($HTTP_GET_VARS['id_member']);
$tgl_mula		= trim($HTTP_GET_VARS['tgl_mula']);
$tgl_akhir 	= trim($HTTP_GET_VARS['tgl_akhir']);

$Member	= new Member();
$TransaksiMember = new TransaksiMember();
	
$data_member	=	$Member->ambilDataDetail($id_member);

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,80);
$pdf->Ln(25);
$pdf->Cell(40,4,'ID Member','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,trim($data_member['id_member']),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Kategori Member','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,trim($data_member['kategori_member']),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Nama','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,trim($data_member['nama']),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Alamat','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,trim($data_member['alamat'])." ".trim($data_member['kota']),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Telepon','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,trim($data_member['telp_rumah'])." / ".trim($data_member['handphone']),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Email','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,trim($data_member['email']),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Saldo','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,"Rp. ".number_format(trim($data_member['saldo']),0,",","."),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Point','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,number_format(trim($data_member['point']),'',0,''),'',0,'');$pdf->Ln();
$pdf->Cell(40,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,now(),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(50,5,'ID Mutasi','B',0,'C',1);
$pdf->Cell(40,5,'Waktu Trx','B',0,'C',1);
$pdf->Cell(40,5,'Referensi','B',0,'C',1);
$pdf->Cell(20,5,'Topup','B',0,'C',1);
$pdf->Cell(20,5,'Trx','B',0,'C',1);
$pdf->Cell(20,5,'Balance','B',0,'C',1);
$pdf->Cell(10,5,'','B',0,'C',1);
$pdf->Cell(60,5,'Keterangan','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

$result	= $TransaksiMember->ambilMutasiByMember($tgl_mula,$tgl_akhir,$id_member);
				
while ($row = $db->sql_fetchrow($result)){   
	$i++;
	
	$pdf->Cell(5,4,$i,0,0,'');
	$pdf->Cell(50,4,trim($row['id_mutasi']),'',0,'L');
	$pdf->Cell(40,4,trim($row['waktu_transaksi']),'',0,'L');
	$pdf->Cell(40,4,trim($row['referensi']),'',0,'L');
	$pdf->Cell(20,4,number_format(trim($row['jum_mutasi_topup']),0,",","."),'',0,'R');
	$pdf->Cell(20,4,number_format(trim($row['jum_mutasi_transaksi']),0,",","."),'',0,'R');
	$pdf->Cell(20,4,number_format(trim($row['saldo']),0,",","."),'',0,'R');
	$pdf->Cell(10,4,'','',0,'C');
	$pdf->MultiCell(60,4,trim($row['keterangan']),0,'J');
	$pdf->Ln();	

}
										
$pdf->Output();
						
?>