<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_CCARE))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$cari  					= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$username				= $userdata['username'];

//INISIALISASI
$filter_by	    = isset($HTTP_POST_VARS['filtertanggal'])? $HTTP_POST_VARS['filtertanggal'] : $HTTP_GET_VARS['filtertanggal'];

$tanggal_mulai  = $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir  = $HTTP_GET_VARS['tanggal_akhir'];
		
$filter_by_tglreg	= $HTTP_GET_VARS['filterbytglreg'];
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);	

$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";

$kondisi_tgl  = "";

if($filter_by!=""){
  $kondisi_tgl	= " AND (".($filter_by=="0"?"TglRegistrasi":"TglBerangkat")." BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') ";
}
$kondisi =($cari=="")?"":
			" AND (IdMember LIKE '%$cari%' 
				OR Nama LIKE '%$cari%' 
				OR Alamat LIKE '%$cari%' 
				OR Handphone LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR Email LIKE '%$cari%'
				OR Pekerjaan LIKE '%$cari%')";

$sql =
  "SELECT tmm.*, tmfb.FrekwensiBerangkat AS Frekwensi
			FROM tbl_md_member tmm LEFT JOIN tbl_member_frekwensi_berangkat tmfb ON tmm.IdMember=tmfb.IdMember
			WHERE  1 $kondisi $kondisi_tgl
			$kondisi_sort";
	
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
	  
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1','Data Member Per Tanggal '.dateNowD_MMM_Y());
	  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'ID Member');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Alamat');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Telp');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Email');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Pekerjaan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Point');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Frekwensi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Tgl.Lahir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Tgl.Expired');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Cabang Daftar');
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Status');
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		
		$idx=0;
		
		while ($row = $db->sql_fetchrow($result)){
			$idx++;
			$idx_row=$idx+3;
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['IdMember']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Alamat']." ".$row['Kota']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['Handphone']."/".$row['Telp']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Email']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Pekerjaan']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['Point']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Frekwensi']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, dateparse(FormatTglToMySQLDate($row['TglLahir'])));
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, dateparse(FormatTglToMySQLDate($row['ExpiredDate'])));
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['CabangDaftar']);
			
			if($row['FlagAktif']){
				$status="Aktif";
			}
			else{
				$status="Tidak Aktif";
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $status);
			
		}
		$temp_idx=$idx_row;
		
		$idx_row++;		
		
		set_time_limit(60);
		
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Data Member Per Tanggal '.dateNowD_MMM_Y().'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
		
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
  
?>
