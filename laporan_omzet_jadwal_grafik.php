<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include_once ($adp_root_path. 'chart/php-ofc-library/open_flash_chart_object.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	$list_bulan	.="<a href='#' onClick='setGrafik($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}


// LIST
$parameter	= "%26tanggal_mulai=".$tanggal_mulai."%26tanggal_akhir=".$tanggal_akhir."%26asal=".$asal."%26tujuan=".$tujuan;

$template->set_filenames(array('body' => 'laporan_omzet_jadwal/laporan_omzet_jadwal_grafik_body.tpl')); 

$template->assign_vars(array(
	'BCRUMP'    						=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_omzet">Home</a> | 
															<a href="'.append_sid('laporan_omzet_jadwal.'.$phpEx).str_replace("%26","&",$parameter).'">Laporan Omzet Per Jam</a> | 
															<a href="'.append_sid('laporan_omzet_jadwal_grafik.'.$phpEx).str_replace("%26","&",$parameter).'">Grafik Omzet Per Jam</a>',
	'LIST_BULAN'						=> "| ".$list_bulan,
	'TAHUN'									=> $tahun,
	'URL'										=> append_sid('laporan_omzet_jadwal_grafik.'.$phpEx),
	'DATA_GRAFIK_PER_JAM'		=> append_sid('laporan_omzet_jadwal_grafik_data.php').$parameter
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>