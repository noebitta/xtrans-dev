<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_CSO,$LEVEL_CSO_PAKET))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];


//INISIALISASI
$Asuransi	= new Asuransi();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND (CetakTiket=1 OR TglBerangkat>=DATE(NOW())) AND PetugasPenjual='$userdata[user_id]'";
 
$kondisi_cari	=($cari=="")?"":
	" AND NoTiket LIKE '%$cari' 
	OR KodeBooking LIKE '%$cari'";
	
$kondisi	= $kondisi.$kondisi_cari;


$temp_tanggal_cari	= explode("-",$tanggal_mulai_mysql);
$tahun_cari		= $temp_tanggal_cari[0];
$bulan_cari		= $temp_tanggal_cari[1];

$tanggal_sekarang	= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang		= $temp_tanggal_sekarang[0];
$bulan_sekarang		= $temp_tanggal_sekarang[1];

if($tahun_cari==$tahun_sekarang && $bulan_cari==$bulan_sekarang){
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else{
	$tbl_reservasi	= "tbl_reservasi_olap";
}

//QUERY
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,Discount,Total,JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM 
		$tbl_reservasi
	$kondisi
	ORDER BY WaktuPesan ASC";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}


//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Penjualan '.$userdata['nama'].' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Waktu Pesan');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'No.Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Waktu Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Kode Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Harga Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Plan Asuransi');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Premi Asuransi');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Tipe Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'CSO');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

$idx=0;
$idx_row=4;

while ($row = $db->sql_fetchrow($result)){
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	//data asuransi
	$data_asuransi			= $Asuransi->ambilDataDetailByNoTiket($row['NoTiket']);
	
	if($data_asuransi['IdAsuransi']!=""){
		$data_plan_asuransi	= $Asuransi->ambilDataDetail($data_asuransi['PlanAsuransi']);
		$plan_asuransi	= "<font color='green'><b>".$data_plan_asuransi['NamaPlan']."</b></font>";
		$besar_premi		= $data_asuransi['BesarPremi'];
	}
	else{
		$plan_asuransi	= "NO PLAN";
		$besar_premi		= 0;
	}
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])));
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NomorKursi']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['HargaTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Discount']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $plan_asuransi);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $besar_premi);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['Total']+$besar_premi);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $row['JenisDiscount']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['NamaCSO']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $status);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $keterangan);
	
	$idx_row++;
	$idx++;
}

$temp_idx=$idx_row-1;

//$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':D'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(K4:K'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(L4:L'.$temp_idx.')');
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Penjualan '.$userdata['nama'].' per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
  
?>
