<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Deposit{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Deposit(){
		$this->ID_FILE="C-DPS";
	}
	
	//BODY
	
	function generateKodeReferensi(){
		global $db;
		
		$temp	= array("0",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		
		
		do{
			$rnd1	= $temp[rand(1,61)];
			$rnd2	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			
			$kode_referensi	= "TU".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s.$rnd3;
		
			$sql = "SELECT COUNT(1) FROM tbl_deposit_log_topup WHERE KodeReferensi='$kode_referensi'";
						
			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE ".__LINE__);
			}
			
			$row=$db->sql_fetchrow($result);
		
		}while($row[0]>0);
			
		
		
		return $kode_referensi;
	}
	
	function generateKodeOTP(){
		
		$temp	= array("7",
			"1","2","3","4","5","6","7","8","9","7",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","N","P","Q","R","S","T",
			"U","V","W","X","Y","Z");
		
		$rnd1	= $temp[rand(1,36)];
		$rnd2	= $temp[rand(1,36)];
		$rnd3	= $temp[rand(1,36)];
		$rnd4	= $temp[rand(1,36)];
		$rnd5	= $temp[rand(1,36)];
		$rnd6	= $temp[rand(1,36)];
			
		$kode_otp	= $rnd6.$rnd5.$rnd4.$rnd3.$rnd2.$rnd1;	
		
		return $kode_otp;
	}
	
	function tambah($kode_referensi, $kode_otp, $jumlah,$petugas_topup){
	  
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_deposit_log_topup (
				KodeReferensi,WaktuTransaksi,Jumlah,
				OTP,PetugasTopUp)
				VALUES(
				'$kode_referensi',NOW(),'".$jumlah."',
				'$kode_otp','".$petugas_topup."'
				)";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}

	function tambahRedbus($kode_referensi, $kode_otp, $jumlah,$petugas_topup){

		//kamus
		global $db;

		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_deposit_log_topup_redbus (
				KodeReferensi,WaktuTransaksi,Jumlah,
				OTP,PetugasTopUp)
				VALUES(
				'$kode_referensi',NOW(),'".$jumlah."',
				'$kode_otp','".$petugas_topup."'
				)";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return true;
	}
	
	function verifikasi($kode_referensi,$kode_otp){
	  
		//kamus
		global $db;
		global $userdata;
		
		//MEMVERIFIKASI DATA
		
		$sql = 
			"UPDATE tbl_deposit_log_topup SET IsVerified=1,WaktuVerifikasi=NOW(),PetugasVerifikasi='".$userdata['user_id']."' WHERE KodeReferensi='$kode_referensi' AND OTP='$kode_otp' AND IsVerified=0";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		if($db->sql_affectedrows()>0){
			
			$sql = "CALL sp_deposit_topup('$kode_referensi');";
			
			if (!$db->sql_query($sql)){
				die_error("Err: $this->ID_FILE ".__LINE__);
			}
			
			return true;
		}
		else{
			return false;
		}
		
	}

	function verifikasiRedbus($kode_referensi,$kode_otp){

		//kamus
		global $db;
		global $userdata;

		//MEMVERIFIKASI DATA

		$sql =
			"UPDATE tbl_deposit_log_topup_redbus SET IsVerified=1,WaktuVerifikasi=NOW(),PetugasVerifikasi='".$userdata['user_id']."' WHERE KodeReferensi='$kode_referensi' AND OTP='$kode_otp' AND IsVerified=0";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		if($db->sql_affectedrows()>0){

			$sql = "CALL sp_deposit_topup_redbus('$kode_referensi');";

			if (!$db->sql_query($sql)){
				die_error("Err: $this->ID_FILE ".__LINE__);
			}

			return true;
		}
		else{
			return false;
		}

	}
	
	function getSaldoDeposit(){
		global $db;
		
		$sql = "SELECT NilaiParameter FROM tbl_pengaturan_parameter WHERE NamaParameter='DEPOSIT_SALDO'";
						
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
			
		$row=$db->sql_fetchrow($result);
		
		return $row[0];
	}

	function getSaldoDepositRedbus(){
		global $db;

		$sql = "SELECT NilaiParameter FROM tbl_pengaturan_parameter WHERE NamaParameter='DEPOSIT_SALDO_REDBUS'";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		return $row[0];
	}
	
	function getTotalTransaksi($tgl_awal,$tgl_akhir,$jenis_trx=1){
		global $db;
		
		$sql = "SELECT SUM(Jumlah) FROM tbl_deposit_log_trx WHERE (DATE(WaktuTransaksi) BETWEEN '$tgl_awal' AND '$tgl_akhir') AND IsDebit=$jenis_trx";
						
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
			
		$row=$db->sql_fetchrow($result);
		
		return $row[0];
	}

	function getTotalTransaksiRedbus($tgl_awal,$tgl_akhir,$jenis_trx=1){
		global $db;

		$sql = "SELECT SUM(Jumlah) FROM tbl_deposit_log_trx_redbus WHERE (DATE(WaktuTransaksi) BETWEEN '$tgl_awal' AND '$tgl_akhir') AND IsDebit=$jenis_trx";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		return $row[0];
	}

  function refundTiket($no_tiket){
    global $db;

    $sql = "CALL sp_deposit_refund('$no_tiket')";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE ".__LINE__);
    }

    return true;
  }

  function refundTiketRedBus($no_tiket){
    global $db;

    $sql = "CALL sp_deposit_refund_redbus('$no_tiket')";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE ".__LINE__);
    }

    return true;
  }

}
?>