<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act    = $HTTP_GET_VARS['act'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Member	= new Member();

switch($mode){

//TAMPILKAN MEMBER BARU ==========================================================================================================
case 'tampilkan_member':
	
	$sortby    	= $HTTP_GET_VARS['sortby'];
	$ascending	= $HTTP_GET_VARS['ascending'];
	$ascending	= ($ascending==0)?"asc":"desc";
	
	if($sortby=='') $sortby='id_member';
	
	$cari 			= $HTTP_GET_VARS['cari'];
	
	//HEADER TABEL
	$hasil ="
		<table width='100%' class='border'>
    <tr>
      <th>#</th>
			<th><a href='#' onClick='setOrder(\"id_member\")'>
				<font color='white'>ID</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"nama\")'>
				<font color='white'>Nama</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"alamat\")'>
				<font color='white'>Alamat</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"kota\")'>
				<font color='white'>Kota</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"telp_rumah\")'>
				<font color='white'>Telp</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"handphone\")'>
				<font color='white'>HP</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"email\")'>
				<font color='white'>Email</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"kategori_member\")'>
				<font color='white'>Kategori member</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"no_seri_kartu\")'>
				<font color='white'>No.Seri Kartu</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"tgl_registrasi\")'>
				<font color='white'>Tgl Daftar</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"saldo\")'>
				<font color='white'>Deposit</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"point\")'>
				<font color='white'>Point</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"diubah_oleh\")'>
				<font color='white'>Diubah oleh</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"tgl_diubah\")'>
				<font color='white'>Tgl diubah</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"status_member\")'>
				<font color='white'>Status</font></a></th>
			
			<th>Aksi</th>
    </tr>";
    
	$result	= $Member->ambilData($cari,$sortby,$ascending);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$on_click="onClick='ubahStatus(\"$row[id_member]\");'";
		
		$action = 
			"<input type='button' value='&nbsp;&nbsp;Ubah&nbsp;&nbsp;&nbsp;' onClick='document.location=\"".append_sid('member_detail.'.$phpEx)."&id_member=$row[id_member]&mode=ambil_data_member"."\"' />
			<input type='button' value='&nbsp;&nbsp;Hapus&nbsp;&nbsp;' onclick=\"TanyaHapus('$row[id_member]');\"  />
			<input type='button' value='Reg.Kartu' onclick=\"registerKartu('$row[id_member]');\" />";
		
		if($row['status_member']){
			$status	= "Aktif";
		}
		else if(!$row['status_member'] && $row['nama']!=''){
			$status	= "Nonaktif";
			$odd='red';
		}
		else if(!$row['status_member'] && $row['nama']==''){
			$status	= "Terdaftar";
			$odd='yellow';
			$action="";
			$on_click="";
		}
		//".md5('00'.trim($row['id_member']).trim($row['no_ktp']))."
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[id_member]</td>
      <td class='$odd'>$row[nama]</td>
      <td class='$odd'>$row[alamat]</td>
      <td class='$odd'>$row[kota]</td>
      <td class='$odd'>$row[telp_rumah]</td>
      <td class='$odd'>$row[handphone]</td>
      <td class='$odd'>$row[email]</td>
      <td class='$odd'>$row[kategori_member]</td>
      <td class='$odd'>$row[no_seri_kartu]</td>
      <td class='$odd'>$row[tgl_registrasi]</td>
			<td class='$odd' align='right'>".number_format($row['saldo'],0,",",".")."</td>
			<td class='$odd' align='right'>".number_format($row['point'],0,",",".")."</td>
			<td class='$odd'>$row[diubah_oleh]</td>
			<td class='$odd'>$row[tgl_diubah]</td>
			<td class='$odd' align='center'><a href='#' $on_click>$status</a></td>
			<td class='$odd' align='center'>$action</td>
    </tr>
		<tr>
			<td colspan=14 height=3 bgcolor='D0D0D0'></td>
		</tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=14 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

//UBAH STATUS MEMBER ==========================================================================================================
case 'ubah_status':
	$id_member    = $HTTP_GET_VARS['id_member'];  
	
	if(!$Member->ubahStatus($id_member)) echo("false");else echo("true");
	
exit;
//switch mode

//HAPUS MEMBER ==========================================================================================================
case 'hapus_member':
	
	//ambil isi dari inputan
	$id_member	= $HTTP_GET_VARS['id_member'];	
	
	if($Member->hapus($id_member)){
		echo("berhasil");
	}
	else{
		echo("gagal");
	}
exit;
}//switch mode

$template->set_filenames(array('body' => 'member.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  =>$userdata['username'],
   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member.'.$phpEx).'">Member</a>',
   	'U_ADD' =>'<a href="'.append_sid('member_detail.'.$phpEx) .'">Tambah Member</a>',
   	'U_USER_SHOW'=>append_sid('member.'.$phpEx.'?mode=tampilkan_member'),
		'SID'=>$userdata['sid']
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>