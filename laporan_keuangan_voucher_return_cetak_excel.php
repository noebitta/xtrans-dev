<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$fil_status = isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"WaktuDigunakan":$sort_by;

switch($fil_status){
		case 0:
			//belum terpakai
			$status	= "Open";
			break;
		
		case 1:
			//terpakai
			$status	= "TERPAKAI";
			break;
		
		case 2:
			$status	= "EXPIRED";
			break;
		
	}

//QUERY
$sql	= 
	"SELECT tv.*,tr.Nama,tr.Telp,tu.nama as Petugas,IF(NoTiketPulang IS NULL,IF(ExpiredDate>DATE(NOW()),0,2),1) AS Status
	FROM (tbl_voucher_return tv
		LEFT JOIN tbl_reservasi tr ON tv.NoTiketPulang=tr.NoTiket)
		LEFT JOIN tbl_user tu ON tv.PetugasPengguna=tu.user_id
	WHERE 1 $kondisi
	ORDER BY $sort_by $order";
		
if ($result = $db->sql_query($sql)){
		
	$i=1;
	
	$objPHPExcel = new PHPExcel();          
  $objPHPExcel->setActiveSheetIndex(0);  
  $objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
  $objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
  
	//HEADER
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Voucher per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir));
	$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Filter Kota: '.$kota.', Status: '.$status);
	$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('B4', '#Voucher');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Tgl. Dibuat');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Tgl. Digunakan');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Cabang');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('F4', '#Tiket');
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Jadwal');
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Nama Penumpang');
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Telepon');
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('J4', 'Nilai Voucher');
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Petugas');
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('L4', 'Expired');
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('M4', 'Keterangan');
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('N4', 'Status');
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	
	$idx=0;
	
	while ($row = $db->sql_fetchrow($result)){
		$idx++;
		$idx_row=$idx+4;
		
		switch($row['Status']){
		case 0:
			//belum terpakai
			$status	= "Open";
			break;
		
		case 1:
			//terpakai
			$status	= "TERPAKAI";
			break;
		
		case 2:
			$status	= "EXPIRED";
			break;
		
	}
	
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['KodeVoucher']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetak'])));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuDigunakan'])));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['CabangTujuan']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NoTiketPulang']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['KodeJadwalPulang']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['Nama']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Telp']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['NilaiVoucher']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Petugas']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, dateparse(FormatMySQLDateToTgl($row['ExpiredDate'])));
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, "");
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $status);
		
	}
	$temp_idx=$idx_row;
	
	$idx_row++;		
	
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':G'.$idx_row);
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row,'=SUM(J4:J'.$temp_idx.')');
	
	$sql	= 
		"SELECT COUNT(KodeVoucher) AS TotalVoucher,SUM(IF(NoTiketPulang IS NULL AND ExpiredDate>DATE(NOW()),1,0)) AS TotalBelumTerpakai,
			SUM(IF(ExpiredDate <=DATE(NOW()),1,0)) AS TotalExpired,
			SUM(NilaiVoucher) AS TotalRupiahVoucher,SUM(IF(NoTiketPulang IS NULL AND ExpiredDate>DATE(NOW()),NilaiVoucher,0)) AS TotalRupiahBelumTerpakai,
			SUM(IF(ExpiredDate <=DATE(NOW()),NIlaiVoucher,0)) AS TotalRupiahExpired
		FROM (tbl_voucher_return tv
			LEFT JOIN tbl_reservasi tr ON tv.NoTiketPulang=tr.NoTiket)
			LEFT JOIN tbl_user tu ON tv.PetugasPengguna=tu.user_id
			WHERE 1 $kondisi";
			
	if (!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	
	$data_total = $db->sql_fetchrow($result);

	$idx_row	+= 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Voucher Belum digunakan:');$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $data_total['TotalBelumTerpakai']);$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $data_total['TotalRupiahBelumTerpakai']);
	$idx_row	++;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Voucher digunakan:');$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $data_total['TotalVoucher']-$data_total['TotalBelumTerpakai']-$data_total['TotalExpired']);$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $data_total['TotalRupiahVoucher']-$data_total['TotalRupiahBelumTerpakai']-$data_total['TotalRupiahExpired']);
	$idx_row	++;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Voucher Expired:');$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $data_total['TotalExpired']);$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row,$data_total['TotalRupiahExpired']);
	$idx_row	++;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Jumlah Voucher:');$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $data_total['TotalVoucher']);$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $data_total['TotalRupiahVoucher']);
	
	
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
  
	if ($idx>0){
		header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Voucher per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output'); 
	}
}
else{
	die_error('Err:',__LINE__);
}   


?>
