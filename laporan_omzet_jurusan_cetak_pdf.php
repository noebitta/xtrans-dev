<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$sql=
	"SELECT 
		IdJurusan,KodeJurusan,f_cabang_get_name_by_kode(KodeCabangAsal) AS CabangAsal,
		f_cabang_get_name_by_kode(KodeCabangTujuan) AS CabangTujuan,
		KodeCabangAsal
	FROM tbl_md_jurusan
	$kondisi_cari";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF(JenisPenumpang='U' OR JenisPenumpang='',NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M',NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K',NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK',NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G',NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(SubTotal),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi_olap
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['IdJurusan']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['IdJurusan']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['IdJurusan']					= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']				= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']						= $row['CabangAsal']."->".$row['CabangTujuan'];
	$temp_array[$idx]['KodeCabangAsal']			= $row['KodeCabangAsal'];
	$temp_array[$idx]['TotalPenumpangU']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangU']+0;
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangM']+0;
	$temp_array[$idx]['TotalPenumpangK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangK']+0;
	$temp_array[$idx]['TotalPenumpangKK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangKK']+0;
	$temp_array[$idx]['TotalPenumpangG']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangG']+0;
	$temp_array[$idx]['TotalBerangkat']			= $data_tiket_total[$row['IdJurusan']]['TotalBerangkat']+0;
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['IdJurusan']]['TotalTiket']+0;
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['IdJurusan']]['TotalPenjualanTiket']+0;
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['IdJurusan']]['TotalDiscount']+0;
	$temp_array[$idx]['TotalPenjualanPaket']= $data_paket_total[$row['IdJurusan']]['TotalPenjualanPaket']+0;
	$temp_array[$idx]['TotalPaket']					= $data_paket_total[$row['IdJurusan']]['TotalPaket']+0;
	$temp_array[$idx]['TotalBiaya']					= $data_paket_total[$row['IdJurusan']]['TotalBiaya']+0;
	$temp_array[$idx]['TotalPenumpangPerTrip']= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']							= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,60);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Omzet per Jurusan','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(40,5,'Jurusan','B',0,'C',1);
$pdf->Cell(20,5,'Kode','B',0,'C',1);
$pdf->Cell(30,5,'Pnp','B',0,'C',1);
$pdf->Cell(30,5,'Omz.Pnp','B',0,'C',1);
$pdf->Cell(30,5,'Pkt','B',0,'C',1);
$pdf->Cell(30,5,'Omz.Pkt','B',0,'C',1);
$pdf->Cell(30,5,'Disc.','B',0,'C',1);
$pdf->Cell(30,5,'OP Langsung','B',0,'C',1);
$pdf->Cell(30,5,'L/R Kotor','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

$idx=0;

while($idx<count($temp_array)){
	
	//total tiket
	$total_penjualan_tiket	= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket						= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total biaya
	$total_biaya						= $temp_array[$idx]['TotalBiaya'];
	
	//total
	$total									= $temp_array[$idx]['Total'];
	
	$pdf->Cell(5,5,$idx+1,'',0,'C');
	//$pdf->Cell(50,5,$row['Nama'],'',0,'L');
	$temp_jurusan	= $pdf->multiCellAutoParse($temp_array[$idx]['Jurusan'],15);
	$pdf->Cell(40,5,$temp_jurusan[0],'','L');
	//$pdf->Cell(40,5,substr($temp_array[$idx]['Jurusan'],0,15),'','L');
	$pdf->Cell(20,5,$temp_array[$idx]['KodeJurusan'],'',0,'L');
	//$pdf->Cell(40,5,$row['Alamat']." ".$row['Area'],'B',0,'L');
	$pdf->Cell(30,5,number_format($total_tiket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_penjualan_tiket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_paket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_penjualan_paket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_discount,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_biaya,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total,0,",","."),'',0,'R');
	
	for($i=1;$i<count($temp_jurusan);$i++){
		$pdf->Ln();
		$pdf->Cell(5,5,"",'',0,'C');$pdf->Cell(40,5,$temp_jurusan[$i],'','L');
	}

	$pdf->Ln(4);
	$pdf->Cell(275,1,'','B',0,'');
	$pdf->Ln();
	
	$total_penumpang	+=$total_tiket;
	$total_omzet			+=$total_penjualan_tiket;
	$total_paket			+=$total_paket;
	$total_omzet_paket+=$total_penjualan_paket;
	$total_discount		+=$total_discount;
	$total_biaya			+=$total_biaya;
	$total_profit			+=$total;
	
	$idx++;
}

$pdf->Cell(65,5,"TOTAL",'',0,'C');
$pdf->Cell(30,5,number_format($total_penumpang,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($total_omzet,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($total_paket,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($total_omzet_paket,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($total_discount,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($total_biaya,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($total_profit,0,",","."),'',0,'R');
$pdf->Ln();
$pdf->Cell(65,5,"",'',0,'C');$pdf->Cell(210,1,'','B',0,'');
								
$pdf->Output();
						
?>