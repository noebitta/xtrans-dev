<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']>$LEVEL_MANAJER,$LEVEL_KEUANGAN){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// bila bukan admin dan owner, direct langsung ke main

include($adp_root_path . 'includes/page_header.php');

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

function OptionMember($member){
  global $db;
  //$rute = ($rute == '(none)') ? '' : $rute;
	  
      $sql = "SELECT     DISTINCT nama,user_id
                FROM     tbl_user";
	  $opt = "";
	  if (!$result = $db->sql_query($sql))
	  {
		die_error('Cannot Load Customer',__LINE__,__FILE__,$sql);
	  } else
	  {
		while ($row = $db->sql_fetchrow($result))
		{
		  $opt .= ($rute == $row[1]) ? "<option selected=selected value='$row[1]'>$row[0]</option>" : "<option value='$row[1]'>$row[0]</option>";
		}    
	  }
  $opt = "<option>(none)</option>" . $opt;  
  return $opt;
}

$user = OptionMember($member);
if ($mode=='result') 
	{
		$user = $HTTP_POST_VARS['user'];
		$start = $HTTP_POST_VARS['start'];
		$start =  dateparse($start);
		$finish = $HTTP_POST_VARS['akhir'];
		$finish = dateparse($finish);
		
		
		$sql = "SELECT     TbReservasi.KodeJadwal, TbReservasi.NoUrut, convert(char(20),TbReservasi.TGLBerangkat,104), TbMDJadwal.Harga
					FROM         TbReservasi CROSS JOIN
                      TbMDJadwal
					  WHERE     (tb.reservasi.kodejadwal = TbMDJadwal.kode) 
							AND (TbReservasi.idUser =$user) 
							AND (TbReservasi.TGLBerangkat BETWEEN CONVERT(datetime, '$start') 
							AND CONVERT(datetime, '$finish'))";
					  if (!$result = $db->sql_query($sql))
				 	{
				 	
				 	}
				 		$i=1;
				 		while ($row = $db->sql_fetchrow($result))
				 		{
				 			$odd ='odd';
				 			if (($i % 2)==0)
				 			{
				   			$odd = 'even';
				 			}
				 			$template ->assign_block_vars('ROW',array('odd'=>$odd,
				 												'num'=>$i,
				 												'kode'=>$row[0],
				 												'no'=>$row[1],
				 												'date' =>$row[2],
				 												'harga'=>$row[3],
				 												));	
				 			$i++;
				 		}
				 		
				 		$sqln = "SELECT nama from tbl_user where user_id=$user";
						if (!$resultn= $db->sql_query($sqln))
							{
								
							}
							while ($rown = $db->sql_fetchrow($resultn)){
									$nama = $rown[0];
								}
				 		
				 		
						$template->set_filenames(array('body' => 'member_hasil_body.tpl'));
						$template->assign_vars (
								  array(
								    'BCRUMP'   =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('laporan.'.$phpEx).'">Laporan</a> \ <a href="'.append_sid('laporan_member.'.$phpEx).'">Laporan Member</a>',
								      'JUDUL'    => 'Laporan Pemesanan user <font color="red">'.$nama.'</font> Pada Tanggal '.$start.' Sampai '.$finish
									   )); 
	}
else
{

	$template->set_filenames(array('body' => 'laporan_member.tpl')); 
	$template->assign_vars (
	  array('BCRUMP'   =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('laporan.'.$phpEx).'">Laporan</a> \ <a href="'.append_sid('laporan_member.'.$phpEx).'">Laporan member</a>',
	        'O_MEMBER' => $user,
	        'U_MEMBER_LAPOR'=>append_sid('laporan_member.'.$phpEx.'?mode=result')
	        ));
}
$template->pparse('body');

include($adp_root_path . 'includes/page_tail.php');
?>