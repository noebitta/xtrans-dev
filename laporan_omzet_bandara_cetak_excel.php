<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
	redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];


$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Cabang								= new Cabang();

if(in_array($userdata['user_level'],array($LEVEL_ADMIN))){
	$kondisi_cabang	= ($kode_cabang=="")?"":" AND KodeCabang='$kode_cabang'";
	$cabang_default	= "";
}
else{
	$kondisi_cabang	= " AND KodeCabang='$userdata[KodeCabang]'";
	$cabang_default	= $userdata['KodeCabang'];
}

//AMBIL DATA CABANG
//Cabang Asal
$data_cabang_asal		= $Cabang->ambilDataDetail($asal);
$data_cabang_tujuan	= $Cabang->ambilDataDetail($tujuan);

$keterangan_asal	= ($data_cabang_asal['Nama']=="")?"Semua Asal":"$data_cabang_asal[Nama] ($data_cabang_asal[KodeCabang]) $data_cabang_asal[Kota]";
$keterangan_tujuan= ($data_cabang_tujuan['Nama']=="")?"Semua Tujuan":"$data_cabang_tujuan[Nama] ($data_cabang_tujuan[KodeCabang]) $data_cabang_tujuan[Kota]";

if($asal!="" && $tujuan!=""){
	$kondisi_cabang.= " AND IdJurusan='$tujuan'";
}


$sql=
	"SELECT 
		IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
		JamBerangkat,HargaTiket,KodeKendaraan,
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS Asal,
		f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) AS Tujuan,
		f_sopir_get_nama_by_id(KodeSopir) as NamaSopir,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(IF(JenisPenumpang='U' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangU,
		IS_NULL(SUM(IF(JenisPenumpang='M' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangM,
		IS_NULL(SUM(IF(JenisPenumpang='K' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangK,
		IS_NULL(SUM(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangKK,
		IS_NULL(SUM(IF(JenisPenumpang='G' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangG,
		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangT,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,0),Total)),0) AS TotalOmzet,
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM tbl_reservasi_olap
	WHERE  (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_cabang
	GROUP BY JamBerangkat
	ORDER BY JamBerangkat ";


if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

//memasukkan ke array
$data_laporan	= array();

while($row = $db->sql_fetchrow($result)){
	$data_laporan[$row['JamBerangkat']]['JamBerangkat']			= $row['JamBerangkat'];
	$data_laporan[$row['JamBerangkat']]['Mobil']  = $row['KodeKendaraan'];
	$data_laporan[$row['JamBerangkat']]['Sopir']  = $row['NamaSopir'];
	$data_laporan[$row['JamBerangkat']]['HargaTiket']  = $row['HargaTiket'];
	$data_laporan[$row['JamBerangkat']]['Asal']  = $row['Asal'];
	$data_laporan[$row['JamBerangkat']]['Tujuan']  = $row['Tujuan'];
	$data_laporan[$row['JamBerangkat']]['TotalBerangkat']		= $row['TotalBerangkat'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangU']	= $row['TotalPenumpangU'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangM']	= $row['TotalPenumpangM'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangK']	= $row['TotalPenumpangK'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangKK']	= $row['TotalPenumpangKK'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangG']	= $row['TotalPenumpangG'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangT']	= $row['TotalPenumpangT'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangR']	= $row['TotalPenumpangR'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangVR']	= $row['TotalPenumpangVR'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangV']	= $row['TotalPenumpangV'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpang']		= $row['TotalPenumpang'];
	$data_laporan[$row['JamBerangkat']]['TotalOmzet']				= $row['TotalOmzet'];
	$data_laporan[$row['JamBerangkat']]['TotalDiscount']		= $row['TotalDiscount'];
}

//jika ada dalam filter tanggal harus mengambil dari tbl_reservasi
//note: tiap hari transaksi di tbl_reservasi di backup ke tbl_reservasi_olap
if($tanggal_akhir_mysql>=date("Y-m-d")){
	$sql=
		"SELECT
			IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
			JamBerangkat,HargaTiket,KodeKendaraan,
			f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS Asal,
			f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) AS Tujuan,
			f_sopir_get_nama_by_id(KodeSopir) as NamaSopir,
			IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
			IS_NULL(SUM(IF(JenisPenumpang='U' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangU,
			IS_NULL(SUM(IF(JenisPenumpang='M' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangM,
			IS_NULL(SUM(IF(JenisPenumpang='K' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangK,
			IS_NULL(SUM(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangKK,
			IS_NULL(SUM(IF(JenisPenumpang='G' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangG,
			IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangT,
			IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
			IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
			IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
			IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
			IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,0),Total)),0) AS TotalOmzet,
			IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
		FROM tbl_reservasi
		WHERE  (TglBerangkat BETWEEN '".date("Y-m-d")."' AND '$tanggal_akhir_mysql')
			AND CetakTiket=1 AND FlagBatal!=1
			$kondisi_cabang
		GROUP BY JamBerangkat
		ORDER BY JamBerangkat ";

	if (!$result = $db->sql_query($sql)){
		//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
		echo("Err:".__LINE__);exit;
	}

	//memasukkan ke array
	while($row = $db->sql_fetchrow($result)){
		$data_laporan[$row['JamBerangkat']]['JamBerangkat']			= $row['JamBerangkat'];
		$data_laporan[$row['JamBerangkat']]['Mobil']  = $row['KodeKendaraan'];
		$data_laporan[$row['JamBerangkat']]['Sopir']  = $row['NamaSopir'];
		$data_laporan[$row['JamBerangkat']]['HargaTiket']  = $row['HargaTiket'];
		$data_laporan[$row['JamBerangkat']]['Asal']  = $row['Asal'];
		$data_laporan[$row['JamBerangkat']]['Tujuan']  = $row['Tujuan'];
		$data_laporan[$row['JamBerangkat']]['TotalBerangkat']		+= $row['TotalBerangkat'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangU']	+= $row['TotalPenumpangU'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangM']	+= $row['TotalPenumpangM'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangK']	+= $row['TotalPenumpangK'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangKK']	+= $row['TotalPenumpangKK'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangG']	+= $row['TotalPenumpangG'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangT']	+= $row['TotalPenumpangT'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangR']	+= $row['TotalPenumpangR'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangVR']	+= $row['TotalPenumpangVR'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangV']	+= $row['TotalPenumpangV'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpang']		+= $row['TotalPenumpang'];
		$data_laporan[$row['JamBerangkat']]['TotalOmzet']				+= $row['TotalOmzet'];
		$data_laporan[$row['JamBerangkat']]['TotalDiscount']		+= $row['TotalDiscount'];
	}
}

//EXPORT KE MS-EXCEL

$i=1;

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:O1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Bandara Per Jam periode '.$tanggal_mulai." s/d ".$tanggal_akhir." Asal:".$keterangan_asal." Tujuan:".$keterangan_tujuan);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Jam');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Type');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Asal');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Tujuan');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Harga Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Total Bayar');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Jenis Pembayaran');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'No Body');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Sopir');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Pax');

$idx_row	=4;
$temp_row=$idx_row;

foreach($data_laporan as $row_laporan){

	$total_berangkat		= $row_laporan['TotalBerangkat'];
	$total_penumpang_u	= $row_laporan['TotalPenumpangU'];
	$total_penumpang_m	= $row_laporan['TotalPenumpangM'];
	$total_penumpang_k	= $row_laporan['TotalPenumpangK'];
	$total_penumpang_kk	= $row_laporan['TotalPenumpangKK'];
	$total_penumpang_g	= $row_laporan['TotalPenumpangG'];
	$total_penumpang_t	= $row_laporan['TotalPenumpangT'];
	$total_penumpang_r	= $row_laporan['TotalPenumpangR'];
	$total_penumpang_vr	= $row_laporan['TotalPenumpangVR'];
	$total_penumpang_v	= $row_laporan['TotalPenumpangV'];
	$total_penumpang		= $row_laporan['TotalPenumpang'];
	$total_omzet				= $row_laporan['TotalOmzet'];
	$total_discount			= $row_laporan['TotalDiscount'];

	$rata_penumpang_per_trip	=($total_berangkat>0)?$total_penumpang/$total_berangkat:0;

	$objPHPExcel->getActiveSheet()->setCellValue('A'.$temp_row,$row_laporan['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$temp_row, 'Shuttle');
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$temp_row, '');
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$temp_row, $row_laporan['Asal']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$temp_row, $row_laporan['Tujuan']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$temp_row, $total_omzet);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$temp_row, $total_discount);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$temp_row, $total_omzet-$total_discount);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$temp_row, 'CASH');
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$temp_row, $row_laporan['Mobil']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$temp_row, $row_laporan['Sopir']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$temp_row, $total_penumpang);

	$temp_row++;
}

$temp_idx			= $temp_row;
$temp_idx_2		= $temp_row-1;

$objPHPExcel->getActiveSheet()->setCellValue('A'.$temp_idx, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$temp_idx, '=SUM(F'.$idx_row.':F'.$temp_idx_2.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$temp_idx, '=SUM(G'.$idx_row.':G'.$temp_idx_2.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$temp_idx, '=SUM(H'.$idx_row.':H'.$temp_idx_2.')');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$temp_idx, '=SUM(L'.$idx_row.':L'.$temp_idx_2.')');


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

if ($temp_row>$idx_row){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Laporan Bandara Per Jam '.$tanggal_mulai." s/d ".$tanggal_akhir.'.xls"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}

?>
