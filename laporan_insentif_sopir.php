<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassArea.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$area  					= isset($HTTP_GET_VARS['area'])? $HTTP_GET_VARS['area'] : $HTTP_POST_VARS['area'];	
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$username				= $userdata['username'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$Area = new Area();

//=METHODS===============================================
function setComboArea($area_dipilih){
	//SET COMBO AREA
	global $db;
	global $Area;
			
	$result=$Area->ambilData("","KodeArea","ASC");
	$opt="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($area_dipilih!=$row['KodeArea'])?"":"selected";
			$opt .="<option value='$row[KodeArea]' $selected>$row[KodeArea] ($row[NamaArea])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt;
	//END SET AREA
}

//=CONTROL=============================================================

// LIST
$template->set_filenames(array('body' => 'laporan_insentif_sopir/laporan_insentif_sopir_body.tpl')); 

//--PREPARASI DATA-----------------------
$nama_view	= "v_laporan_insentif_sopir".$userdata['user_id'];
if($HTTP_POST_VARS['btn_cari']=='cari' || $tanggal_mulai==''){
	
	$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
	$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
	$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
	$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
	
	//DROP VIEW--
	$sql	= "DROP VIEW IF EXISTS $nama_view;";
	
	if (!$result_laporan = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	//END DROP VIEW ---
	
	//--QUERY DATA SOPIR-----------------------
	$cari	= $HTTP_POST_VARS["txt_cari"]!=""?$HTTP_POST_VARS["txt_cari"]:$HTTP_GET_VARS["cari"];
	
	$kondisi_cari	=($cari=="")?"":" AND (KodeSopir LIKE '$cari%' OR Nama LIKE '%$cari%')";
	
	$kondisi_area	= $area!="" ? " AND KodeArea='$area'":"";
	
	/*$que_sopir	=
		"SELECT KodeSopir,Nama,0 AS TotalHariKerja,0 AS TotalTrip,0 AS TotalPenumpang,0 AS TotalPenumpangInsentif,0 TotalInsentif
		FROM tbl_md_sopir 
		WHERE 1 $kondisi_cari";
	
	//--END QUERY DATA SOPIR-----------------------
	
	//--CERATE VIEW LAPORAN-----------------------
	$sql	= "CREATE VIEW $nama_view AS $que_sopir;";
	
	if (!$result_laporan = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	
	//--END CERATE VIEW LAPORAN-----------------------
	*/
	
	//--QUERY DATA INSENTIF DARI TABEL SPJ-----------------------
	$que_data	= 
		"SELECT 
			KodeDriver,Nama,
			COUNT(DISTINCT(TglBerangkat)) AS TotalHariKerja,
			COUNT(DISTINCT(IF(IsSubJadwal!=1,NoSPJ,NULL))) AS TotalTripUtama,
			COUNT(DISTINCT(IF(IsSubJadwal=1,NoSPJ,NULL))) AS TotalTripTransit,
			SUM(JumlahPenumpang) AS TotalPenumpang,
			SUM(JumPnpInsentif) AS TotalPenumpangInsentif,
			SUM(JumPnpInsentif*InsentifSopir) AS TotalInsentif
		FROM tbl_md_sopir tms LEFT JOIN tbl_spj ts ON tms.KodeSopir=ts.KodeDriver
		WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		$kondisi_area
		$kondisi_cari
		GROUP BY KodeDriver";
	//--END QUERY DATA INSENTIF DARI TABEL SPJ-----------------------
	
	//--CERATE VIEW LAPORAN-----------------------
	$sql	= "CREATE VIEW $nama_view AS $que_data;";
	
	if (!$result_laporan = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	//--END CERATE VIEW LAPORAN-----------------------
	
	//--END CERATE VIEW LAPORAN ADD DATA-----------------------

	
}
//--END PREPARASI DATA-----------------------


$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Nama":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeDriver",$nama_view,
"&cari=$cari&area=$area&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_insentif_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	= "SELECT * FROM $nama_view ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$parameters	= "&area=".$area."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kode_driver=".$row['KodeDriver'];	
	$act	="Start('laporan_insentif_sopir_detail.php?sid=".$userdata['session_id'].$parameters."');return false;";

	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'nama'=>$row['Nama'],
				'nrp'=>$row['KodeDriver'],
				'total_hari_kerja'=>number_format($row['TotalHariKerja'],0,",","."),
				'total_manifest_utama'=>number_format($row['TotalTripUtama'],0,",","."),
				'total_manifest_transit'=>number_format($row['TotalTripTransit'],0,",","."),
				'total_penumpang'=>number_format($row['TotalPenumpangInsentif'],0,",","."),
				'total_insentif'=>number_format($row['TotalInsentif'],0,",","."),
				'act'=>$act
			)
		);
	
	$i++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&area=".$area."&sort_by=".$sort_by."&order=".$order."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir;								
$script_cetak_excel="Start('laporan_insentif_sopir_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&area=$area&order=$order_invert";


//MENGAMBIL DETAIL TOTAL
$sql	= "SELECT COUNT(DISTINCT(KodeDriver)) AS TotalSopir,SUM(TotalTripUtama) AS TotalManifestUtama,SUM(TotalTripTransit) AS TotalManifestTransit,SUM(TotalInsentif) AS TotalInsentif  FROM $nama_view;";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_sopir	= $db->sql_fetchrow($result);

	
$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_keuangan">Home</a> | <a href="'.append_sid('laporan_insentif_sopir.'.$phpEx).'">Laporan Insentif Sopir</a>',
	'ACTION_CARI'		=> append_sid('laporan_insentif_sopir.'.$phpEx),
	'OPT_AREA'			=> setComboArea($area),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'TOTAL_SOPIR'		=> $data_sopir['TotalSopir'],
	'TOTAL_MANIFEST'=> $data_sopir['TotalManifestUtama'],
	'TOTAL_MANIFEST_TRANSIT'=> $data_sopir['TotalManifestTransit'],
	'TOTAL_INSENTIF'=> number_format($data_sopir['TotalInsentif'],0,",","."),
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_insentif_sopir.'.$phpEx.'?sort_by=Nama'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan berdasarkan Nama Sopir ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_insentif_sopir.'.$phpEx.'?sort_by=KodeDriver'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan berdasarkan NRP ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_insentif_sopir.'.$phpEx.'?sort_by=TotalHariKerja'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan berdasarkan total hari kerja($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_insentif_sopir.'.$phpEx.'?sort_by=TotalTripUtama'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan berdasarkan total ritase utama ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_insentif_sopir.'.$phpEx.'?sort_by=TotalPenumpangInsentif'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan berdasarkan jumlah penumpang insentif($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_insentif_sopir.'.$phpEx.'?sort_by=TotalInsentif'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan berdasarkan total penerimaan insentif ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_insentif_sopir.'.$phpEx.'?sort_by=TotalTripUtama'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan berdasarkan total ritase transit ($order_invert)"
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>