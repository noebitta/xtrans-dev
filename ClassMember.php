<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Member{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Member(){
		$this->ID_FILE="C-MBR";
	}
	
	//BODY
	
	function kodeValidasi($id_member,$no_ktp,$deposit,$point){
		return md5($deposit.$point.$id_member.$no_ktp);
	}
	
	function generateIdMember(){
		$temp	= array("-",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		
		return $y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
	}
	
	function periksaDuplikasi($id_member,$email,$handphone){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika id_member tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT f_member_periksa_duplikasi('$id_member','$email','$handphone') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti id_member sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah(
		$id_member,$nama, $jenis_kelamin,
	  $kategori_member,$tempat_lahir,$tgl_lahir,
	  $no_ktp,$tgl_registrasi,$alamat,
	  $kota,$kode_pos,$telp,
	  $handphone,$email, $pekerjaan,
	  $point,$expired_date, $id_kartu, 
	  $no_seri_kartu,$kata_sandi,$flag_aktif,
		$cabang_daftar){
		  
		/*
		IS	: data member belum ada dalam database
		FS	:Data member baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"CALL sp_member_tambah(
				'$id_member', '$nama', '$jenis_kelamin',
			  '$kategori_member','$tempat_lahir','$tgl_lahir',
			  '$no_ktp','$tgl_registrasi','$alamat',
			  '$kota','$kode_pos','$telp',
			  '$handphone','$email','$pekerjaan',
			  '$point','$expired_date','$id_kartu', 
			  '$no_seri_kartu','$kata_sandi','$flag_aktif',
				'$cabang_daftar');";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah(
		$id_member,$nama, $jenis_kelamin,
	  $kategori_member,$tempat_lahir,$tgl_lahir,
	  $no_ktp,$tgl_registrasi,$alamat,
	  $kota,$kode_pos,$telp,
	  $handphone,$email, $pekerjaan,
	  $point,$expired_date, $id_kartu, 
	  $no_seri_kartu,$kata_sandi,$flag_aktif,
		$cabang_daftar){
	  
		/*
		IS	: data member sudah ada dalam database
		FS	:Data member diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"CALL sp_member_ubah(
				'$id_member', '$nama', '$jenis_kelamin',
			  '$kategori_member','$tempat_lahir','$tgl_lahir',
			  '$no_ktp','$tgl_registrasi','$alamat',
			  '$kota','$kode_pos','$telp',
			  '$handphone','$email','$pekerjaan',
			  '$point','$expired_date','$id_kartu', 
			  '$no_seri_kartu','$kata_sandi','$flag_aktif',
				'$cabang_daftar');";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_member){
	  
		/*
		IS	: data sopir sudah ada dalam database
		FS	:Data sopir dihapus
		*/
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA
		$sql =
			"DELETE FROM tbl_md_member
			WHERE IdMember IN($list_member);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ubahStatusAktif($id_member){
	  
		/*
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql ="CALL sp_member_ubah_status_aktif('$id_member');";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
	
	function ambilData($id_member){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *,f_cabang_get_name_by_kode(CabangDaftar) AS NamaCabangDaftar,DATEDIFF(ExpiredDate,NOW()) AS MasaBerlaku
			FROM tbl_md_member
			WHERE IdMember='$id_member';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilPoint($id_member){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT Point
			FROM tbl_md_member
			WHERE IdMember='$id_member';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		} 
		else{
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return $row[0]!=""?$row[0]:0;
		
	}//  END ambilPoint
	
	function registerKartu($id_member,$id_kartu){
	  
		/*
		ID	: 008
		IS	: data member sudah ada dalam database
		FS	: kartu teregister
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return "false otoritas";
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		$id_kartu	= md5($id_kartu);
		
		$sql = 
			"SELECT 
				COUNT(id_member) as jumlah_data
			FROM $this->TABEL1
			WHERE 
				id_kartu='$id_kartu' AND NOT id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti id_member sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 008-01");
		}
		
		if(!$ditemukan){
			//MENAMBAHKAN DATA KEDALAM DATABASE
			$sql =
				"UPDATE $this->TABEL1
				SET 
					id_kartu='$id_kartu',
					diubah_oleh='$useraktif',tgl_diubah={fn NOW()}
				WHERE id_member='$id_member';";
									
			if (!$db->sql_query($sql)){
				die_error("Gagal $this->ID_FILE 008-02");
			}
			
			return "true";
		}
		else{
			return "false duplikasi";
		}
		
	}//end registerKartu
	
	function ambilDataByIdKartu($id_kartu){
		
		/*
		ID	:009
		Desc	:Mengembalikan data member berdasarkan id kartu
		ket	: ID_KARTU YANG MASUK ADALAH ID KARTU YANG SUDAH DI MD5
		*/
		
		//kamus
		global $db;
		
		$id_kartu=trim($id_kartu);
		
		$sql = 
			"SELECT 
				id_member,nama,jenis_kelamin,tempat_lahir,
				CONVERT(CHAR(10),tgl_lahir,103) as tgl_lahir,no_ktp,CONVERT(CHAR(10),tgl_registrasi,103) as tgl_registrasi,
				alamat,kota,kode_pos,telp_rumah,handphone,email,
				status_member,saldo,point,CONVERT(CHAR(25),waktu_transaksi_terakhir,103) as waktu_transaksi_terakhir,
				jumlah_transaksi_terakhir,expired_date,id_kartu,
				diubah_oleh,CONVERT(CHAR(10),tgl_diubah,103) as tgl_diubah,kode_validasi,kategori_member
			FROM $this->TABEL1
			WHERE id_kartu='$id_kartu';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Gagal $this->ID_FILE 009");//.$sql);
		}
		
	}//  END ambilDataByIdKartu
	
	function registerKartuBaru($no_seri_kartu,$id_kartu){
	  
		/*
		ID	: 010
		IS	: data kartu belum ada
		FS	: kartu teregister
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return "false otoritas";
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		$id_kartu	= md5($id_kartu);
		
		$sql = 
			"SELECT 
				COUNT(id_kartu) as jumlah_data
			FROM $this->TABEL1
			WHERE 
				id_kartu='$id_kartu'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti id_member sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 010-01");
		}
		
		if(!$ditemukan){
			$id_member	= "MBR".dateYMD().rand(100,999);
			//MENAMBAHKAN DATA KEDALAM DATABASE
			$sql =
				"INSERT INTO $this->TABEL1(
					id_member,no_seri_kartu,id_kartu,status_member,
					diubah_oleh,tgl_diubah
				)
				VALUES(
					'$id_member','$no_seri_kartu','$id_kartu',0,
					'$useraktif',{fn NOW()}
				)";
									
			if (!$db->sql_query($sql)){
				die_error("Gagal $this->ID_FILE 010-02");
			}
			
			return "true";
		}
		else{
			return "false duplikasi";
		}
		
	}//end registerKartu
	
	function ambilDataKartuBelumAktif($order_by,$asc){
		
		/*
		ID	:011
		Desc	:Mengembalikan kartu yang belum aktif
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT 
				id_member,no_seri_kartu,
				diubah_oleh,CONVERT(CHAR(10),tgl_diubah,103) as tgl_diubah
			FROM $this->TABEL1
			WHERE 
				status_member=0 AND nama IS NULL
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 011");
		}
		
	}//  END ambilDataKartuBelumAktif
	
	function ambilDataByNoSeriKartu($no_seri_kartu){
		
		/*
		ID	:012
		Desc	:Mengembalikan data member berdasarkan no seri kartu
		*/
		
		//kamus
		global $db;

		$sql = 
			"SELECT 
				id_member,nama,jenis_kelamin,tempat_lahir,
				CONVERT(CHAR(10),tgl_lahir,103) as tgl_lahir,no_ktp,CONVERT(CHAR(10),tgl_registrasi,103) as tgl_registrasi,
				alamat,kota,kode_pos,telp_rumah,handphone,email,
				status_member,saldo,point,CONVERT(CHAR(25),waktu_transaksi_terakhir,103) as waktu_transaksi_terakhir,
				jumlah_transaksi_terakhir,expired_date,id_kartu,
				diubah_oleh,CONVERT(CHAR(10),tgl_diubah,103) as tgl_diubah,kode_validasi,kategori_member
			FROM $this->TABEL1
			WHERE no_seri_kartu = '$no_seri_kartu';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Gagal $this->ID_FILE 012");
		}
		
	}//  END ambilDataByNoSeriKartu
	
	function ubahPassword($id_member,$password){
	  
		/*
		ID	: 013
		IS	: data member sudah ada dalam database
		FS	:password member diubah
		*/
		
		//kamus
		global $db;

		
		$password=md5($password);
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE $this->TABEL1
			SET kata_sandi='$password'
			WHERE id_member='$id_member';";
								
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 013 ");
		}
		
		return true;
	} //ubahPassword
	
	function verifikasiPassword($id_member,$password){
		
		/*
		ID	:014
		Desc	:Mengembalikan true jika verfikasi password benar
		*/
		
		//kamus
		global $db;

		$sql = 
			"SELECT kata_sandi
			FROM $this->TABEL1
			WHERE id_member = '$id_member';";
		
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			
			if(md5($password)==trim($row['kata_sandi'])){
				return true;
			}
			else{
				return false;
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 014 ");
		}
		
	}//  END verifikasiPassword
	
}
?>