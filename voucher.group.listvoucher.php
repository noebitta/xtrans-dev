<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassVoucherDiskon.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$waktu_cetak= isset($HTTP_GET_VARS['waktucetak'])? $HTTP_GET_VARS['waktucetak'] : $HTTP_POST_VARS['waktucetak'];
$id_group 	= isset($HTTP_GET_VARS['idgroup'])? $HTTP_GET_VARS['idgroup'] : $HTTP_POST_VARS['idgroup'];

if($id_group==""){
	echo("ILEGAL ACCESS!");
	exit;
}

$bcrump	= "<a href=\"".append_sid("main.".$phpEx) ."#voucherdiskon\">Home</a> | <a href=\"".append_sid("voucher.group.php") ."\">Grup Voucher Diskon</a> | <a href=\"".append_sid("voucher.group.batchvoucher.php?idgroup=$id_group&tglawal=$tgl_awal&tglakhir=$tgl_akhir") ."\">Batch Voucher</a> | <a href=\"".append_sid(basename(__FILE__)."?idgroup=$id_group&tglawal=$tgl_awal&tglakhir=$tgl_akhir")."\">Daftar Voucher</a>";

$Voucher	= new Voucher();

$mode	= $mode==""?"exp":$mode;

switch($mode){
	
	case "exp":
		// LIST
		$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
						
		$kondisi	=($cari=="")?"":
			" AND (Keterangan LIKE '%$cari%'
			OR KodeVoucher LIKE '%$cari'
			OR NamaPetugasPencetak LIKE '%$cari%'
			OR NamaPenumpang LIKE '%$cari%'
			OR Telp LIKE '%$cari%'
			OR NoTiketBerangkat LIKE '%$cari%')";
			
		$sql = 
			"SELECT *
			FROM tbl_voucher
			WHERE IdGroup='$id_group' $kondisi AND WaktuCetak='$waktu_cetak'
			ORDER BY WaktuCetak";
		
		$idx_check=0;
		
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);exit;
		}
		
		$i = $idx_page*$VIEW_PER_PAGE+1;
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
			
			if (($i % 2)==0){
				$odd = 'even';
			}
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'no'=>$i,
						'kodevoucher'=>$row["KodeVoucher"],
						'waktubuat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetak'])),
						'dibuatoleh'=>$row['NamaPetugasPencetak'],
						'digunakan'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuDigunakan'])),
						'notiket'=>$row["NoTiketBerangkat"],
						'jadwal'=>$row["KodeJadwalBerangkat"],
						'penumpang'=>$row["NamaPenumpang"],
						'telp'=>$row["Telp"],
						'nilai'=>number_format($row["NilaiVoucher"],0,",","."),
						'cso'=>$row["NamaPetugasPengguna"],
						'keterangan'=>$row['Keterangan']
					)
				);
			
			$i++;
		}
		
		if($i-1<=0){
			$template->assign_block_vars("NO_DATA",array());
		}
		
		$data_group	= $Voucher->getDetailGroup($id_group);
		
		$template->set_filenames(array('body' => 'voucher.group/listvoucher.tpl')); 
		$template->assign_vars(array(
			'BCRUMP'    		=> $bcrump,
			'NAMA_GROUP'		=> $data_group["NamaGroup"],
			'ACTION_CARI'		=> append_sid(basename(__FILE__)),
			'ID_GROUP'			=> $id_group,
			'WAKTU_CETAK'		=> $waktu_cetak,
			'CARI'					=> $cari,
			'URL_EXPORT'		=> append_sid("voucher.group.listvoucher.exportexcel.php?idgroup=$id_group&waktucetak=".$waktu_cetak),
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
	break;
			
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>