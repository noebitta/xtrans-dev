<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN_PAKET,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

//$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai_mysql  = $HTTP_GET_VARS['tglawal'];
$tanggal_akhir_mysql  = $HTTP_GET_VARS['tglakhir'];
$cari  								= $HTTP_GET_VARS['cari'];
$sort_by							= $HTTP_GET_VARS['sortby'];
$order								= $HTTP_GET_VARS['order'];

//INISIALISASI
//$tbl_reservasi	= $is_today=="1" || $is_today==""?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeJurusan LIKE '$cari%'
		OR KodeCabangAsal LIKE '%$cari%'
		OR KodeCabangTujuan LIKE '%$cari%'
		OR KodeArea LIKE '$cari%'
		OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%'
		OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabangAsal'$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;

$sql=
	"SELECT 
		IdJurusan,KodeJurusan,
		CONCAT(f_cabang_get_name_by_kode(KodeCabangAsal),'-',f_cabang_get_name_by_kode(KodeCabangTujuan)) AS Jurusan
	FROM tbl_md_jurusan
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF(Layanan='P',1,NULL)),0) AS TotalPaketP,
		IS_NULL(COUNT(IF(Layanan='GD',1,NULL)),0) AS TotalPaketGD,
		IS_NULL(COUNT(IF(Layanan='GA',1,NULL)),0) AS TotalPaketGA,
		IS_NULL(COUNT(IF(Layanan='S',1,NULL)),0) AS TotalPaketS,
		IS_NULL(COUNT(IF(Layanan='CA',1,NULL)),0) AS TotalPaketCA,
		IS_NULL(COUNT(IF(Layanan='CD',1,NULL)),0) AS TotalPaketCD,
		IS_NULL(COUNT(IF(Layanan='I',1,NULL)),0) AS TotalPaketI,
		IS_NULL(SUM(IF(Layanan='P',HargaPaket,0)),0) AS OmzPaketP,
		IS_NULL(SUM(IF(Layanan='GD',HargaPaket,0)),0) AS OmzPaketGD,
		IS_NULL(SUM(IF(Layanan='GA',HargaPaket,0)),0) AS OmzPaketGA,
		IS_NULL(SUM(IF(Layanan='S',HargaPaket,0)),0) AS OmzPaketS,
		IS_NULL(SUM(IF(Layanan='CA',HargaPaket,0)),0) AS OmzPaketCA,
		IS_NULL(SUM(IF(Layanan='CD',HargaPaket,0)),0) AS OmzPaketCD,
		IS_NULL(SUM(IF(Layanan='I',HargaPaket,0)),0) AS OmzPaketI,
		IS_NULL(SUM(IF(JenisPembayaran=0,TotalBayar,0)),0) AS OmzTunai,
		IS_NULL(SUM(IF(JenisPembayaran!=0,TotalBayar,0)),0) AS OmzLangganan,
		IS_NULL(SUM(TotalBayar),0) AS TotalPenjualanPaket
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan),f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//debug
//echo($sql);exit;

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['IdJurusan']		= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']	= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']			= $row['Jurusan'];
	$temp_array[$idx]['TotalPaketP']	= $data_paket_total[$row['IdJurusan']]['TotalPaketP']!=""?$data_paket_total[$row['IdJurusan']]['TotalPaketP']:0;
	$temp_array[$idx]['TotalPaketGD']	= $data_paket_total[$row['IdJurusan']]['TotalPaketGD']!=""?$data_paket_total[$row['IdJurusan']]['TotalPaketGD']:0;
	$temp_array[$idx]['TotalPaketGA']	= $data_paket_total[$row['IdJurusan']]['TotalPaketGA']!=""?$data_paket_total[$row['IdJurusan']]['TotalPaketGA']:0;
	$temp_array[$idx]['TotalPaketS']	= $data_paket_total[$row['IdJurusan']]['TotalPaketS']!=""?$data_paket_total[$row['IdJurusan']]['TotalPaketS']:0;
	$temp_array[$idx]['TotalPaketCA']	= $data_paket_total[$row['IdJurusan']]['TotalPaketCA']!=""?$data_paket_total[$row['IdJurusan']]['TotalPaketCA']:0;
	$temp_array[$idx]['TotalPaketCD']	= $data_paket_total[$row['IdJurusan']]['TotalPaketCD']!=""?$data_paket_total[$row['IdJurusan']]['TotalPaketCD']:0;
	$temp_array[$idx]['TotalPaketI']	= $data_paket_total[$row['IdJurusan']]['TotalPaketI']!=""?$data_paket_total[$row['IdJurusan']]['TotalPaketI']:0;
	$temp_array[$idx]['OmzPaketP']		= $data_paket_total[$row['IdJurusan']]['OmzPaketP']!=""?$data_paket_total[$row['IdJurusan']]['OmzPaketP']:0;
	$temp_array[$idx]['OmzPaketGD']		= $data_paket_total[$row['IdJurusan']]['OmzPaketGD']!=""?$data_paket_total[$row['IdJurusan']]['OmzPaketGD']:0;
	$temp_array[$idx]['OmzPaketGA']		= $data_paket_total[$row['IdJurusan']]['OmzPaketGA']!=""?$data_paket_total[$row['IdJurusan']]['OmzPaketGA']:0;
	$temp_array[$idx]['OmzPaketS']		= $data_paket_total[$row['IdJurusan']]['OmzPaketS']!=""?$data_paket_total[$row['IdJurusan']]['OmzPaketS']:0;
	$temp_array[$idx]['OmzPaketCA']		= $data_paket_total[$row['IdJurusan']]['OmzPaketCA']!=""?$data_paket_total[$row['IdJurusan']]['OmzPaketCA']:0;
	$temp_array[$idx]['OmzPaketCD']		= $data_paket_total[$row['IdJurusan']]['OmzPaketCD']!=""?$data_paket_total[$row['IdJurusan']]['OmzPaketCD']:0;
	$temp_array[$idx]['OmzPaketI']		= $data_paket_total[$row['IdJurusan']]['OmzPaketI']!=""?$data_paket_total[$row['IdJurusan']]['OmzPaketI']:0;
	$temp_array[$idx]['OmzTunai']			= $data_paket_total[$row['IdJurusan']]['OmzTunai']!=""?$data_paket_total[$row['IdJurusan']]['OmzTunai']:0;
	$temp_array[$idx]['OmzLangganan']	= $data_paket_total[$row['IdJurusan']]['OmzLangganan']!=""?$data_paket_total[$row['IdJurusan']]['OmzLangganan']:0;
	$temp_array[$idx]['OmzTotal']			= $data_paket_total[$row['IdJurusan']]['TotalPenjualanPaket']!=""?$data_paket_total[$row['IdJurusan']]['TotalPenjualanPaket']:0;
		
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Paket per Jurusan Tanggal '.$tanggal_mulai_mysql.' s/d '.$tanggal_akhir_mysql);
$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Jurusan');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('C3:I3');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Total Paket (qty)');
$objPHPExcel->getActiveSheet()->setCellValue('C4', 'P');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'GD');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'GA');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'S');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'CA');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'CD');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I4', 'I');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('J3:P3');
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Total Paket (Rp.)');
$objPHPExcel->getActiveSheet()->setCellValue('J4', 'P');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K4', 'GD');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L4', 'GA');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M4', 'S');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N4', 'CA');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O4', 'CD');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P4', 'I');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('Q3:S3');
$objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Omzet');
$objPHPExcel->getActiveSheet()->setCellValue('Q4', 'Tunai');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('R4', 'Langganan');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('S4', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);

$idx=0;
$idx_row=5;

while ($idx<count($temp_array)){
	
		
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Jurusan']." (".$temp_array[$idx]['KodeJurusan'].")");
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['TotalPaketP']);
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['TotalPaketGD']);
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$idx]['TotalPaketGA']);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$idx]['TotalPaketS']);
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $temp_array[$idx]['TotalPaketCA']);
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $temp_array[$idx]['TotalPaketCD']);
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $temp_array[$idx]['TotalPaketI']);
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $temp_array[$idx]['OmzPaketP']);
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $temp_array[$idx]['OmzPaketGD']);
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $temp_array[$idx]['OmzPaketGA']);
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $temp_array[$idx]['OmzPaketS']);
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $temp_array[$idx]['OmzPaketCA']);
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $temp_array[$idx]['OmzPaketCD']);
  $objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $temp_array[$idx]['OmzPaketI']);
  $objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $temp_array[$idx]['OmzTunai']);
  $objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, $temp_array[$idx]['OmzLangganan']);
  $objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, $temp_array[$idx]['OmzTotal']);
	
	$idx_row++;
	$idx++;
}

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':B'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, "TOTAL");
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row,"=SUM(C5:C".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row,"=SUM(D5:D".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row,"=SUM(E5:E".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row,"=SUM(F5:F".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row,"=SUM(G5:G".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row,"=SUM(H5:H".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row,"=SUM(I5:I".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row,"=SUM(J5:J".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row,"=SUM(K5:K".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row,"=SUM(L5:L".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row,"=SUM(M5:M".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row,"=SUM(N5:N".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row,"=SUM(O5:O".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row,"=SUM(P5:P".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row,"=SUM(Q5:Q".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row,"=SUM(Q5:Q".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row,"=SUM(Q5:Q".($idx_row-1).")");
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet Paket per Jurusan Tanggal '.$tanggal_mulai_mysql.' sd '.$tanggal_akhir_mysql.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
