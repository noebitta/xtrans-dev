<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cso  					= isset($HTTP_GET_VARS['cso'])? $HTTP_GET_VARS['cso'] : $HTTP_POST_VARS['cso'];
$nama_cso  			= isset($HTTP_GET_VARS['namacso'])? $HTTP_GET_VARS['namacso'] : $HTTP_POST_VARS['namacso'];
$tgl_awal  			= isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
$tgl_akhir 			= isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];

// LIST
$template->set_filenames(array('body' => 'laporan_penjualan_cso_callcenter/detail.tpl')); 

//MENGAMBIL PENGATURAN CABANG CALL CENTER
$sql = 
	"SELECT *
	FROM tbl_pengaturan_parameter
	WHERE NamaParameter LIKE 'CALLCENTER_%'
	ORDER BY NamaParameter;";
				
if(!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}
		
$data_cc	= array();
		
while ($row=$db->sql_fetchrow($result)){
	$field_name	= $row['NilaiParameter'];
	$data_cc[$field_name]	= $row['Deskripsi'];
	$cabangs_callcenter	.= "'$row[NilaiParameter]',";
}

$cabangs_callcenter	= substr($cabangs_callcenter,0,-1);

$kondisi=
	"(DATE(WaktuPesan) BETWEEN '$tgl_awal' AND '$tgl_akhir')
		AND CabangPesan IN($cabangs_callcenter)
		AND PetugasPenjual='$cso'";

$sql	= 
	"SELECT *
	FROM tbl_reservasi_olap tr
	WHERE $kondisi
	ORDER BY WaktuPesan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//PLOT DATA
while($row=$db->sql_fetchrow($result)){
	
	$idx++;
	
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
		
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx,
				'waktu_pesan'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),
				'no_tiket'=>$row['NoTiket'],
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'kode_jadwal'=>$row['KodeJadwal'],
				'nama'=>$row['Nama'],
				'telp'=>$row['Telp'],
				'no_kursi'=>$row['NomorKursi'],
				'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
			)
	);
		
} 


$template->assign_vars(array(
	'NAMA_CSO'			=> $nama_cso,
	'PERIODE'				=> dateparse(FormatMySQLDateToTgl($tgl_awal))." s/d ".dateparse(FormatMySQLDateToTgl($tgl_akhir))
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>