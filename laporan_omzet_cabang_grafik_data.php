<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; 
$submode 		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 

//DATA GRAFIK

$tanggal  	= isset($HTTP_GET_VARS['tgl'])? $HTTP_GET_VARS['tgl'] : $HTTP_POST_VARS['tgl'];
$kode_cabang	= isset($HTTP_GET_VARS['kode_cabang'])? $HTTP_GET_VARS['kode_cabang'] : $HTTP_POST_VARS['kode_cabang'];

$kode_cabang	= (!in_array($userdata['user_level'],array($LEVEL_SUPERVISOR)))?$kode_cabang:$userdata['kode_cabang'];


$arr_tanggal	= explode("-",$tanggal);
$bulan	= $arr_tanggal[1];
$tahun	= $arr_tanggal[2];

//INISIALISASI
include_once( 'chart/php-ofc-library/open-flash-chart.php' );
$g = new graph();

$data	= array();
$axis	= array();

$sql=
	"SELECT f_cabang_get_name_by_kode('$kode_cabang') Cabang";

if ($result = $db->sql_query($sql)){
	$row 			= $db->sql_fetchrow($result);
	$cabang		= $row['Cabang'];
}

//GRAFIK OMZET==============================================================================================
if($mode=='bulanan' && $submode=='omzet'){
	//AMBIL HARI
	$sql=
		"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		$temp_hari	= $row['Hari'];
	}
	
	//omzet PENUMPANG
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(SUM(SubTotal),0) AS TotalOmzet
		FROM tbl_reservasi_olap
		WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY TglBerangkat
		ORDER BY TglBerangkat";
	
	
	if (!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	//omzet PAKET
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(SUM(HargaPaket),0) AS TotalOmzet
		FROM tbl_paket
		WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY TglBerangkat
		ORDER BY TglBerangkat";
	
	if (!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	$data_paket 		= $db->sql_fetchrow($result_paket);
		
	for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
		
		$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

		$axis[$idx_tgl]		= $idx_tgl+1 ."(".HariStringShort($idx_str_hari).")";
		
		//penumpang
		if($data_penumpang['Tanggal']==$idx_tgl+1){
			$data[$idx_tgl]	= number_format($data_penumpang['TotalOmzet'],0,"",""); 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_tgl]		= 0;
		}
		
		if($data_paket['Tanggal']==$idx_tgl+1){
			$data1[$idx_tgl]	= number_format($data_paket['TotalOmzet'],0,"",""); 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_tgl]		= 0;
		}
		
		$temp_hari++;
	}
	
	$judul_grafik	="Grafik Omzet Cabang: ".$cabang." Bulan: ".BulanString($bulan)." $tahun ";
	$legend	="Tanggal";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Omzet Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Omzet Paket', 12 );
	$g->set_y_legend( 'Jumlah (Rp.)', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);	

	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
} 
elseif($mode=='tahunan' && $submode=='omzet'){

	$pembagi	= 1000;
	
	//OMZET PENUMPANG
	$sql=
		"SELECT 
			MONTH(TglBerangkat) bulan,
			IS_NULL(SUM(SubTotal),0)/$pembagi AS TotalOmzet
		FROM tbl_reservasi_olap
		WHERE YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY MONTH(TglBerangkat),YEAR(TglBerangkat)
		ORDER BY bulan";

	if(!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	
	//OMZET PAKET
	$sql=
		"SELECT 
			MONTH(TglBerangkat) bulan,
			IS_NULL(SUM(HargaPaket),0)/$pembagi AS TotalOmzet
		FROM tbl_paket
		WHERE YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY MONTH(TglBerangkat),YEAR(TglBerangkat)
		ORDER BY bulan";

	if(!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_paket = $db->sql_fetchrow($result_paket);
		
	for($idx_bln=0;$idx_bln<12;$idx_bln++){
		$axis[$idx_bln]	= BulanString($idx_bln+1);
		
		//DATA PENUMPANG
		if($data_penumpang['bulan']==$idx_bln+1){
			$data[$idx_bln]	= $data_penumpang['TotalOmzet']; 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_bln]=0;
		}
		
		//DATA PAKET
		if($data_paket['bulan']==$idx_bln+1){
			$data1[$idx_bln]	= $data_paket['TotalOmzet']; 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_bln]=0;
		}
	}	
	
	
	$judul_grafik	="Grafik Omzet Cabang: $cabang Tahun: $tahun";
	$legend	="Bulan";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Omzet Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Omzet Paket', 12 );
	$g->set_y_legend( 'Jumlah (Rp.) - x '.$pembagi, 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);

	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
} //GRAFIK PROFIT & BIAYA=============================================================================================================================
elseif($mode=='bulanan' && $submode=='profit_biaya'){
	//AMBIL HARI
	$sql=
		"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		$temp_hari	= $row['Hari'];
	}
	
	//BIAYA
	$sql_total_biaya	= 
		"SELECT 
			WEEKDAY(TglTransaksi)+1 AS Hari,DAY(TglTransaksi) AS Tanggal,
			IS_NULL(SUM(Jumlah),0) AS TotalBiaya 
		FROM tbl_biaya_op tbop 
		WHERE MONTH(TglTransaksi)=$bulan AND YEAR(TglTransaksi)=$tahun AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang'
		GROUP BY TglTransaksi
		ORDER BY TglTransaksi";
	
	if (!$result_biaya = $db->sql_query($sql_total_biaya)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_biaya = $db->sql_fetchrow($result_biaya);
	
	//omzet PENUMPANG
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(SUM(SubTotal),0) AS TotalOmzet
		FROM tbl_reservasi_olap
		WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY TglBerangkat
		ORDER BY TglBerangkat";
	
	
	if (!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	
	//omzet PAKET
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(SUM(HargaPaket),0) AS TotalOmzet
		FROM tbl_paket
		WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY TglBerangkat
		ORDER BY TglBerangkat";
	
	if (!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_paket 		= $db->sql_fetchrow($result_paket);
		
	for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
		
		$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

		$axis[$idx_tgl]		= $idx_tgl+1 ."(".HariStringShort($idx_str_hari).")";
		
		//penumpang
		if($data_penumpang['Tanggal']==$idx_tgl+1){
			$data[$idx_tgl]	= number_format($data_penumpang['TotalOmzet'],0,"",""); 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_tgl]		= 0;
		}
		
		//PAKET
		if($data_paket['Tanggal']==$idx_tgl+1){
			$data[$idx_tgl]	+= number_format($data_paket['TotalOmzet'],0,"",""); 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data[$idx_tgl]		+= 0;
		}
		
		//BIAYA
		if($data_biaya['Tanggal']==$idx_tgl+1){
			$data1[$idx_tgl]	= number_format($data_biaya['TotalBiaya'],0,"",""); 
			$data_biaya = $db->sql_fetchrow($result_biaya);
		}
		else{
			$data1[$idx_tgl]		= 0;
		}
		
		$data2[$idx_tgl]	= $data[$idx_tgl]-$data1[$idx_tgl];
		
		$temp_hari++;
	}
	
	$judul_grafik	="Grafik Profit & Biaya Cabang: ".$cabang." Bulan: ".BulanString($bulan)." $tahun ";
	$legend	="Tanggal";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);
	$g->set_data($data2);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Omzet', 12 );
	$g->line_hollow( 2, 4, '0xff0000', 'Biaya', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Laba / Rugi Kotor', 12 );
	$g->set_y_legend( 'Jumlah (Rp.)', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1),max($data2));
	
	$max_value_y	= max($temp_max_value);	

	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
} 
elseif($mode=='tahunan' && $submode=='profit_biaya'){

	$pembagi	= 1000;

	//BIAYA
	$sql_total_biaya	= 
		"SELECT 
			MONTH(TglTransaksi) bulan,
			IS_NULL(SUM(Jumlah),0)/$pembagi AS TotalBiaya 
		FROM tbl_biaya_op
		WHERE YEAR(TglTransaksi)=$tahun AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang'
		GROUP BY MONTH(TglTransaksi),YEAR(TglTransaksi)
		ORDER BY bulan";
	
	if(!$result_biaya = $db->sql_query($sql_total_biaya)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_biaya = $db->sql_fetchrow($result_biaya);
	
	//OMZET PENUMPANG
	$sql=
		"SELECT 
			MONTH(TglBerangkat) bulan,
			IS_NULL(SUM(SubTotal),0)/$pembagi AS TotalOmzet
		FROM tbl_reservasi_olap
		WHERE YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY MONTH(TglBerangkat),YEAR(TglBerangkat)
		ORDER BY bulan";

	if(!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	
	//OMZET PAKET
	$sql=
		"SELECT 
			MONTH(TglBerangkat) bulan,
			IS_NULL(SUM(HargaPaket),0)/$pembagi AS TotalOmzet
		FROM tbl_paket
		WHERE YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY MONTH(TglBerangkat),YEAR(TglBerangkat)
		ORDER BY bulan";

	if(!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_paket = $db->sql_fetchrow($result_paket);
		
	for($idx_bln=0;$idx_bln<12;$idx_bln++){
		$axis[$idx_bln]	= BulanString($idx_bln+1);
		
		//DATA PENUMPANG
		if($data_penumpang['bulan']==$idx_bln+1){
			$data[$idx_bln]	= $data_penumpang['TotalOmzet']; 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_bln]=0;
		}
		
		//DATA PAKET
		if($data_paket['bulan']==$idx_bln+1){
			$data[$idx_bln]	+= $data_paket['TotalOmzet']; 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data[$idx_bln] +=0;
		}
		
		//DATA BIAYA
		if($data_biaya['bulan']==$idx_bln+1){
			$data1[$idx_bln]	= $data_biaya['TotalBiaya']; 
			$data_biaya = $db->sql_fetchrow($result_biaya);
		}
		else{
			$data1[$idx_bln]=0;
		}
		
		$data2[$idx_bln]	= $data[$idx_bln]-$data1[$idx_bln];
		
	}	
	
	$judul_grafik	="Grafik Profit & Biaya Cabang: $cabang Tahun: $tahun";
	$legend	="Bulan";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);
	$g->set_data($data2);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Omzet', 12 );
	$g->line_hollow( 2, 4, '0xff0000', 'Biaya', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Laba / Rugi Kotor', 12 );
	$g->set_y_legend( 'Jumlah (Rp.) - x '.$pembagi, 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1),max($data2));
	
	$max_value_y	= max($temp_max_value);

	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
}//GRAFIK PRODUKTIFITAS============================================================================================= 
elseif($mode=='bulanan' && $submode=='produktifitas'){
	//AMBIL HARI
	$sql=
		"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		$temp_hari	= $row['Hari'];
	}
		
	//LOAD PENUMPANG
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang
		FROM tbl_reservasi_olap
		WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY TglBerangkat
		ORDER BY TglBerangkat";
	
	
	if (!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	//LOAD PAKET
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(COUNT(NoTiket),0) AS TotalPaket
		FROM tbl_paket
		WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY TglBerangkat
		ORDER BY TglBerangkat";
	
	if (!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	$data_paket 		= $db->sql_fetchrow($result_paket);
		
	for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
		
		$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

		$axis[$idx_tgl]		= $idx_tgl+1 ."(".HariStringShort($idx_str_hari).")";
		
		//penumpang
		if($data_penumpang['Tanggal']==$idx_tgl+1){
			$data[$idx_tgl]	= number_format($data_penumpang['TotalPenumpang'],0,"",""); 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_tgl]		= 0;
		}
		
		//PAKET
		if($data_paket['Tanggal']==$idx_tgl+1){
			$data1[$idx_tgl]	= number_format($data_paket['TotalPaket'],0,"",""); 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_tgl]		= 0;
		}
		
		$temp_hari++;
	}
	
	$judul_grafik	="Grafik Load Penumpang & Paket Cabang: ".$cabang." Bulan: ".BulanString($bulan)." $tahun";
	$legend	="Tanggal";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Load Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Load Paket', 12 );
	$g->set_y_legend( 'Jumlah', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);
	
	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
} 
elseif($mode=='tahunan' && $submode=='produktifitas'){
	
	//LOAD PENUMPANG
	$sql=
		"SELECT 
			MONTH(TglBerangkat) bulan,
			IS_NULL(COUNT(SubTotal),0) AS TotalPenumpang
		FROM tbl_reservasi_olap
		WHERE YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY MONTH(TglBerangkat),YEAR(TglBerangkat)
		ORDER BY bulan";

	if(!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	
	//LOAD PAKET
	$sql=
		"SELECT 
			MONTH(TglBerangkat) bulan,
			IS_NULL(COUNT(HargaPaket),0) AS TotalPaket
		FROM tbl_paket
		WHERE YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
		GROUP BY MONTH(TglBerangkat),YEAR(TglBerangkat)
		ORDER BY bulan";

	if(!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_paket = $db->sql_fetchrow($result_paket);
		
	for($idx_bln=0;$idx_bln<12;$idx_bln++){
		$axis[$idx_bln]	= BulanString($idx_bln+1);
		
		//DATA PENUMPANG
		if($data_penumpang['bulan']==$idx_bln+1){
			$data[$idx_bln]	= $data_penumpang['TotalPenumpang']; 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_bln]=0;
		}
		
		//DATA PAKET
		if($data_paket['bulan']==$idx_bln+1){
			$data1[$idx_bln]	= $data_paket['TotalPaket']; 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_bln]=0;
		}
	}	
	
	$judul_grafik	="Grafik Load Penumpang & Paket Cabang: $cabang Tahun: $tahun";
	$legend	="Bulan";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Load Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Load Paket', 12 );
	$g->set_y_legend( 'Jumlah', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);
	
	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
	
} 

$g->title($judul_grafik, '{font-size: 20px; color: #736AFF}' );

$g->set_x_labels($axis);
$g->set_x_label_style( 12, '0x000000', 0, 2 );
$g->set_x_legend($legend, 13, '#736AFF' );

$g->set_y_max($max_value);

$g->y_label_steps(5);
echo $g->render();
?>