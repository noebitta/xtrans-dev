<?php
//
// CETAK TIKET UNTUK LINUX
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CCARE))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER	
$Member	= new Member();
$id_member		= $HTTP_GET_VARS['id'];		
	
$data_member	= $Member->ambilData($id_member);

$template->set_filenames(array('body' => 'member/member_kartu.tpl')); 
$template->assign_vars(array(
  'ID_MEMBER' 	=> $data_member['IdMember'],
	'NAMA'    		=> $data_member['Nama'],
	'TGL_REG'			=> dateparse(FormatMySQLDateToTgl($data_member['TglRegistrasi'])),
	'CABANG_REG'	=> $data_member['NamaCabangDaftar'],
  )
);

$template->pparse('body');	
?>