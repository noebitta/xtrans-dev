<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$PengaturanUmum	= new PengaturanUmum();


	if ($mode=='save'){
		// aksi penyimpanan 
		$pesan_di_tiket = $HTTP_POST_VARS['pesan_di_tiket'];
		$alamat  				= $HTTP_POST_VARS['alamat'];
		$telp						= $HTTP_POST_VARS['telp'];
		$email					= $HTTP_POST_VARS['email'];
		$website 				= $HTTP_POST_VARS['website'];
		$tanggal_mulai1	= $HTTP_POST_VARS['tanggal_mulai_tuslah1'];
		$tanggal_akhir1	= $HTTP_POST_VARS['tanggal_akhir_tuslah1'];
		$tanggal_mulai2	= $HTTP_POST_VARS['tanggal_mulai_tuslah2'];
		$tanggal_akhir2	= $HTTP_POST_VARS['tanggal_akhir_tuslah2'];
		
		$terjadi_error=false;
		
		$judul="Ubah Data Pengaturan Umum";
		
		if($PengaturanUmum->ubahPengaturanUmum(
				$pesan_di_tiket,$alamat,$telp,$email,
				$website,FormatTglToMySQLDate($tanggal_mulai1),FormatTglToMySQLDate($tanggal_akhir1),
				FormatTglToMySQLDate($tanggal_mulai2),FormatTglToMySQLDate($tanggal_akhir2))){
					
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
					
		}
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'pengaturan_umum/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_umum.'.$phpEx).'">Pengaturan Umum</a>',
			 'JUDUL'		=>$judul,
			 'MODE'   	=> 'save',
			 'PESAN_DI_TIKET'	=> $pesan_di_tiket,
			 'ALAMAT'    			=> $alamat,
			 'TELP'    	=> $telp,
			 'EMAIL'   	=> $email,
			 'WEBSITE'	=> $website,
			 'PESAN'		=> $pesan,
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'TGL_MULAI_TUSLAH1'	=> $tanggal_mulai1,
			 'TGL_AKHIR_TUSLAH1'	=> $tanggal_akhir1,
			  'TGL_MULAI_TUSLAH2'	=> $tanggal_mulai2,
			 'TGL_AKHIR_TUSLAH2'	=> $tanggal_akhir2,
			 'U_ADD_ACT'=>append_sid('pengaturan_umum.'.$phpEx)
			)
		);
	
	} 
	else{
		//show data
		
		$row = $PengaturanUmum->ambilData();
		
		$tanggal_mulai1	= ($row['TGL_MULAI_TUSLAH1']!='' && $row['TGL_MULAI_TUSLAH1']!='')?FormatMySQLDateToTgl($row['TGL_MULAI_TUSLAH1']):dateD_M_Y();
		$tanggal_akhir1	= ($row['TGL_AKHIR_TUSLAH1']!='' && $row['TGL_AKHIR_TUSLAH1']!='')?FormatMySQLDateToTgl($row['TGL_AKHIR_TUSLAH1']):dateD_M_Y();
		
		$tanggal_mulai2	= ($row['TGL_MULAI_TUSLAH2']!='' && $row['TGL_MULAI_TUSLAH2']!='')?FormatMySQLDateToTgl($row['TGL_MULAI_TUSLAH2']):dateD_M_Y();
		$tanggal_akhir2	= ($row['TGL_AKHIR_TUSLAH2']!='' && $row['TGL_AKHIR_TUSLAH2']!='')?FormatMySQLDateToTgl($row['TGL_AKHIR_TUSLAH2']):dateD_M_Y();
		
		$template->set_filenames(array('body' => 'pengaturan_umum/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_umum.'.$phpEx).'">Pengaturan Umum</a> ',
			 'JUDUL'		=>'Ubah Data Pengaturan Umum',
			 'MODE'   	=> 'save',
			 'PESAN_DI_TIKET'	=> $row['PESAN_DITIKET'],
			 'ALAMAT'    			=> $row['PERUSH_ALAMAT'],
			 'TELP'    	=> $row['PERUSH_TELP'],
			 'EMAIL'   	=> $row['PERUSH_EMAIL'],
			 'WEBSITE'	=> $row['PERUSH_WEB'],
			 'TGL_MULAI_TUSLAH1'	=> $tanggal_mulai1,
			 'TGL_AKHIR_TUSLAH1'	=> $tanggal_akhir1,
			 'TGL_MULAI_TUSLAH2'	=> $tanggal_mulai2,
			 'TGL_AKHIR_TUSLAH2'	=> $tanggal_akhir2,
			 'U_ADD_ACT'=>append_sid('pengaturan_umum.'.$phpEx)
			 )
		);
	}       

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>