<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR))){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER

$tanggal_mulai  = isset($HTTP_GET_VARS['p0'])? $HTTP_GET_VARS['p0'] : $HTTP_POST_VARS['p0'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];

$username				= $userdata['username'];

// LIST
//$template->set_filenames(array('body' => 'laporan.keuangan.percso/index.tpl'));

//$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$id_sort				= isset($HTTP_GET_VARS['idsort'])? $HTTP_GET_VARS['idsort'] : $HTTP_POST_VARS['idsort'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$pencari				= isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];

//$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

/*SELECT SORT*/


//DATA PENJUALAN TIKET
$sql	=
  "SELECT
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,Discount,Total,JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM
		tbl_reservasi
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	AND CetakTiket=1 AND PetugasCetakTiket='$pencari'
	ORDER BY WaktuCetakTiket ASC";

if (!$result = $db->sql_query($sql)){
  echo("Err: ".__LINE__);exit;
}


$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Keuangan Per CSO Tanggal '.$tanggal_mulai_mysql.' s/d '.$tanggal_akhir_mysql);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Waktu Pesan');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'No Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Waktu Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Kode Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Harga Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Tipe Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'CSO');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Ket');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

$idx=0;
$idx_row=4;

//PLOT DATA
while($row = $db->sql_fetchrow($result)){
  if($row['FlagBatal']!=1){
    if($row['CetakTiket']!=1){
      $odd	= "blue";
      $status	= "Book";
    }
    else{
      $status	= "OK";
    }
    $keterangan="";
  }
  else{
    $odd	= 'red';
    $status	="BATAL";
    $keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
  }

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])));
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoTiket']);
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])));
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeJadwal']);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Nama']);
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NomorKursi']);
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['HargaTiket']);
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Discount']);
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['Total']);
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['JenisDiscount']);
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['NamaCSO']);
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $status);
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $keterangan);

  $idx_row++;
  $idx++;

}
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

if ($idx>0){
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Keuangan Detail per CSO Tanggal '.$tanggal_mulai_mysql.' sd '.$tanggal_akhir_mysql.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');
}
else{
  echo("TIDAK ADA DATA YANG AKAN DIEKSPOR!");
}
?>
