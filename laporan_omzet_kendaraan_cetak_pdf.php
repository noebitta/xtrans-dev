<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND CetakSPJ=1 AND tr.KodeKendaraan=tmk.KodeKendaraan";

$kondisi_cabang	=($kode_cabang=="")?"":" AND tmk.KodeCabang='$kode_cabang' ";
		
$kondisi_cari	=($cari=="")?
	" WHERE tmk.KodeKendaraan LIKE '%' $kondisi_cabang":
	" WHERE (tmk.KodeKendaraan LIKE '$cari%') $kondisi_cabang";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tmk.KodeKendaraan":$sort_by;
		
//QUERY
$sql_total_jalan			= "(SELECT COUNT(DISTINCT(NoSPJ)) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_penumpang	= "(SELECT IS_NULL(COUNT(NoTiket),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_biaya			= "(SELECT IS_NULL(SUM(Jumlah),0) FROM tbl_biaya_op WHERE NoPolisi=tmk.KodeKendaraan 
													AND (TglTransaksi BETWEEN '$tanggal_mulai_mysql' 
													AND '$tanggal_akhir_mysql'))";
$sql_total_omzet					= "(SELECT IS_NULL(SUM(SubTotal),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_kursi			= "(SELECT SUM(IS_NULL(JumlahKursiDisediakan,0)) FROM tbl_spj 
													WHERE NoPolisi=tmk.KodeKendaraan)";
$sql=
	"SELECT 
		tmk.KodeKendaraan,f_cabang_get_name_by_kode(tmk.KodeCabang) AS Cabang,
		JumlahKursi AS LayoutKursi,
		$sql_total_jalan AS Jalan,
		$sql_total_penumpang  AS TotalPenumpang,
		$sql_total_omzet AS TotalOmzet,
		$sql_total_biaya AS TotalBiaya,
		$sql_total_omzet-$sql_total_biaya AS TotalProfit,
		100*$sql_total_penumpang/$sql_total_kursi AS produktifitas
	FROM tbl_md_kendaraan tmk
	$kondisi_cari
	ORDER BY $sort_by $order";

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,80);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Omzet Kendaraan','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(20,5,'No.Pol','B',0,'C',1);
$pdf->Cell(20,5,'Seat','B',0,'C',1);
$pdf->Cell(50,5,'Cabang','B',0,'C',1);
$pdf->Cell(30,5,'Ttl.Jalan','B',0,'C',1);
$pdf->Cell(30,5,'Jum.Pnp','B',0,'C',1);
$pdf->Cell(30,5,'Omzet','B',0,'C',1);
$pdf->Cell(30,5,'OP Langsung','B',0,'C',1);
$pdf->Cell(30,5,'L/R Kotor','B',0,'C',1);
$pdf->Cell(30,5,'Load Fak.','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$pdf->Cell(5,5,$i,'',0,'C');
		$pdf->Cell(20,5,$row['KodeKendaraan'],'',0,'L');
		$pdf->Cell(20,5,$row['LayoutKursi'].'  ','',0,'R');
		$pdf->MultiCell2(50,5,$row['Cabang'],'','L');
		$pdf->Cell(30,5,number_format($row['Jalan'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['TotalPenumpang'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['TotalOmzet'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['TotalBiaya'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['TotalProfit'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['produktifitas'],0,",",".").'%','',0,'R');
		$pdf->Ln(0);
		$pdf->Cell(275,1,'','B',0,'');
		$pdf->Ln();
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
} 
										
$pdf->Output();
						
?>