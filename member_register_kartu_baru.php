<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode	= $HTTP_GET_VARS['mode'];
$mode	=($mode=='')?"blank":$mode;

$Member			= new Member();

$body="";

switch ($mode){
	case 'blank':
		
		$id_member	= $HTTP_GET_VARS['id_member'];
		
		//MENGAMBIL DATA MEMBER
		$data_member	=	$Member->ambilDataDetail($id_member);
		
		$body=
			"<table width='80%'>
				<tr>
					<td width='20%'>ID MEMBER</td><td width='5%'>:</td><td width='75%'>$id_member</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td><td>$data_member[nama]</td>
				</tr>
				<tr>
					<td>Alamat</td><td>:</td><td>$data_member[alamat] $data_member[kota]</td>
				</tr>
					<tr>
					<td>Telp</td><td>:</td><td>$data_member[telp_rumah] $data_member[handphone]</td>
				</tr>
				<tr><td colspan=3 bgcolor='d0d0d0'></td></tr>
				<tr>
					<td colspan=3 align='center'>Silahkan gesekkan kartu member yang akan diregristasi ke alat pembaca</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action='".append_sid('member_register_kartu.'.$phpEx)."&mode=register_kartu"."' method='post'>
							<input type='hidden' id='hdn_id_member' name='hdn_id_member' value='$id_member' />
							<input type='password' id='id_kartu' name='id_kartu' />
						</form>
					</td>
				</tr>
			</table>";
		
	break;
	
	//MEREGISTER KARTU=============================================================================================
	case 'register_kartu':
		$no_seri_kartu	= $HTTP_POST_VARS['no_seri_kartu'];		
		$id_kartu				= $HTTP_POST_VARS['id_kartu'];		
		
		if($no_seri_kartu==''){
			$pesan='No Seri Kartu tidak boleh kosong!';
			break;
		}
		
		if($id_kartu==''){
			$pesan ='Kartu belum digesek!';
			break;
		}
		
		$return	= $Member->registerKartuBaru($no_seri_kartu,$id_kartu);
		
		if($return=="true"){
			$pesan	="<font color='green'>KARTU BERHASIL DIDAFTARKAN!</font>";
		}
		else if($return=="false duplikasi"){
			$pesan	="<font color='red'>KARTU ANDA SUDAH TERDAFTAR UNTUK MEMBER LAIN!</font>";
		}
		else if($return=="false otoritas"){
			$pesan	="<font color='red'>ANDA TIDAK BERHAK UNTUK MELAKUKAN PROSES INI!</font>";
		}
		else{
			$pesan	="<font color='red'>TERJADI KESALAHAN DALAM PROSES!</font>";
		}
		$no_seri_kartu='';
	break;
}

include($adp_root_path . 'includes/page_header.php');
$template->set_filenames(array('body' => 'member_register_kartu_baru.tpl')); 
$template->assign_vars (
	array(
		'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_register_kartu_baru.'.$phpEx).'">Register kartu baru</a>',
		'U_ACTION'  =>append_sid('member_register_kartu_baru.'.$phpEx)."&mode=register_kartu",
		'NO_SERI_KARTU' =>$no_seri_kartu,
		'PESAN'    	=>"<h2>$pesan</h2>",
		'BODY'			=>$body
	)
);
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>