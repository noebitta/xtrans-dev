<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_CSO,$LEVEL_CSO_PAKET))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$cari  					= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$sort_by				= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$order					= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$username				= $userdata['username'];

//INISIALISASI
$Asuransi	= new Asuransi();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND (CetakTiket=1 OR TglBerangkat>=DATE(NOW())) AND PetugasPenjual='$userdata[user_id]'";
 
$kondisi_cari	=($cari=="")?"":
	" AND NoTiket LIKE '%$cari' 
	OR KodeBooking LIKE '%$cari'";
	
$kondisi	= $kondisi.$kondisi_cari;


$temp_tanggal_cari	= explode("-",$tanggal_mulai_mysql);
$tahun_cari		= $temp_tanggal_cari[0];
$bulan_cari		= $temp_tanggal_cari[1];

$tanggal_sekarang	= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang		= $temp_tanggal_sekarang[0];
$bulan_sekarang		= $temp_tanggal_sekarang[1];

if($tahun_cari==$tahun_sekarang && $bulan_cari==$bulan_sekarang){
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else{
	$tbl_reservasi	= "tbl_reservasi_olap";
}

//QUERY
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,Discount,Total,JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM 
		$tbl_reservasi
	$kondisi
	ORDER BY WaktuPesan ASC";
	
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}
	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,60);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Penjualan '.$userdata['nama'],'',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$k1= 25;
$k2= 20;
$k3= 25;
$k4= 20;
$k5= 30;
$k6= 10;
$k7= 15;
$k8= 15;
$k9= 20;
$k10= 15;
$k11= 15;
$k12= 20;
$k13= 25;
$k14= 15;
$k15= 25;

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell($k1,5,'Pesan','B',0,'C',1);
$pdf->Cell($k2,5,'No.Tiket','B',0,'C',1);
$pdf->Cell($k3,5,'Berangkat','B',0,'C',1);
$pdf->Cell($k4,5,'Kd.Jadwal','B',0,'C',1);
$pdf->Cell($k5,5,'Nama','B',0,'C',1);
$pdf->Cell($k6,5,'Kursi','B',0,'C',1);
$pdf->Cell($k7,5,'Tiket','B',0,'C',1);
$pdf->Cell($k8,5,'Disc.','B',0,'C',1);
$pdf->Cell($k9,5,'Plan','B',0,'C',1);
$pdf->Cell($k10,5,'Premi','B',0,'C',1);
$pdf->Cell($k11,5,'Total','B',0,'C',1);
$pdf->Cell($k12,5,'Tipe Disc.','B',0,'C',1);
$pdf->Cell($k13,5,'CSO','B',0,'C',1);
$pdf->Cell($k14,5,'Status','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',9);
$pdf->SetTextColor(0);
//CONTENT

$idx=0;

$panjang_garis	= $k1+$k2+$k3+$k4+$k5+$k6+$k7+$k8+$k9+$k10+$k11+$k12+$k13+$k14;

while ($row = $db->sql_fetchrow($result)){
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	//data asuransi
	$data_asuransi			= $Asuransi->ambilDataDetailByNoTiket($row['NoTiket']);
	
	if($data_asuransi['IdAsuransi']!=""){
		$data_plan_asuransi	= $Asuransi->ambilDataDetail($data_asuransi['PlanAsuransi']);
		$plan_asuransi	= "<font color='green'><b>".$data_plan_asuransi['NamaPlan']."</b></font>";
		$besar_premi		= $data_asuransi['BesarPremi'];
	}
	else{
		$plan_asuransi	= "NO PLAN";
		$besar_premi		= 0;
	}
	
	$sum_uang_tiket	= $row['HargaTiket'];
	$sum_discount		= $row['Discount'];
	$sum_tiket++;
	$sum_premi				= $besar_premi;
	$sum_total				= $row['Total']+$besar_premi;
	
	$pdf->Cell(5,5,$idx+1,'',0,'C');
	$pdf->MultiCell2($k1,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),'','C');
	$pdf->Cell($k2,5,$row['NoTiket'],'',0,'C');
	$pdf->MultiCell2($k3,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),'','C');
	$pdf->Cell($k4,5,$row['KodeJadwal'],'',0,'C');
	$pdf->Cell($k5,5,$row['Nama'],'',0,'L');
	$pdf->Cell($k6,5,$row['NomorKursi'],'',0,'C');
	$pdf->Cell($k7,5,number_format($row['HargaTiket'],0,",","."),'',0,'R');
	$pdf->Cell($k8,5,number_format($row['Discount'],0,",","."),'',0,'R');
	$pdf->Cell($k9,5,$plan_asuransi,'',0,'C');
	$pdf->Cell($k10,5,number_format($besar_premi,0,",","."),'',0,'R');
	$pdf->Cell($k11,5,number_format($row['Total']+$besar_premi,0,",","."),'',0,'R');
	$pdf->Cell($k12,5,$row['JenisDiscount'],'',0,'L');
	$pdf->Cell($k13,5,$row['NamaCSO'],'',0,'L');
	$pdf->Cell($k14,5,$status,'',0,'C');
	$pdf->Ln(0);
	$pdf->Cell($panjang_garis,1,'','B',0,'');
	$pdf->Ln();
	
	$idx_row++;
	$idx++;
}

$pdf->Cell(5,5,'','',0,'C');
$pdf->Cell($k1+$k2+$k3+$k4+$k5+$k6,5,'TOTAL','',0,'L');
$pdf->Cell($k7,5,number_format($sum_uang_tiket,0,",","."),'',0,'R');
$pdf->Cell($k8,5,number_format($sum_discount,0,",","."),'',0,'R');
$pdf->Cell($k9,5,'','',0,'R');
$pdf->Cell($k10,5,number_format($sum_premi,0,",","."),'',0,'R');
$pdf->Cell($k11,5,number_format($sum_total,0,",","."),'',0,'R');

										
$pdf->Output();
						
?>