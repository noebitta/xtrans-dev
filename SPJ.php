<?php
//
// SPJ
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassPromo.php');	


// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

//METHODS============================================================================

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	$Sopir = new Sopir();
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	$opt_sopir="<option value=''>- silahkan pilih sopir  -</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
}

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;
	$Mobil = new Mobil();
			
	$result=$Mobil->ambilDataForComboBox();
	$opt_mobil="<option value=''>- silahkan pilih kendaraan  -</option>";
	
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
			$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[Merek] $row[Jenis]</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	
	return $opt_mobil;
	//END SET COMBO MOBIL
}

//PROCESS==================================================================

$tgl_berangkat		= $HTTP_GET_VARS['tgl_berangkat'];
$kode_jadwal			= $HTTP_GET_VARS['kode_jadwal'];
$sopir_sekarang		= $HTTP_GET_VARS['sopir_sekarang'];
$mobil_sekarang		= $HTTP_GET_VARS['mobil_sekarang'];
$aksi							= $HTTP_GET_VARS['aksi'];
$debug						= $HTTP_GET_VARS['debug'];

if($aksi==0){
	$Jadwal						= new Jadwal();
	$BiayaOperasional	= new BiayaOperasional();
	
	//load data
	$str_option_sopir=setComboSopir($sopir_sekarang);
	$str_option_mobil=setComboMobil($mobil_sekarang);
	
	#mengambil data jadwal
	$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
	
	$is_sub_jadwal = false;
	
	if($data_jadwal['FlagSubJadwal']!=1){
		$kode_jadwal_utama	= $kode_jadwal;
	}
	else{
		$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];
		$is_sub_jadwal	= true;
	}
	
	$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($kode_jadwal_utama);
	
	echo("
		<table bgcolor='white' width='100%'>
		  <tr>
				<td colspan=2>Silahkan pilih mobil</td>
				<td  width='1'>:</td>
				<td>
					<select name='list_mobil' id='list_mobil'>$str_option_mobil</select>
					<span id='list_mobil_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
		  <tr>
				<td colspan=2>Silahkan pilih Sopir</td>
				<td>:</td>
				<td>
					<select name='list_sopir' id='list_sopir'>$str_option_sopir</select>
					<span id='list_sopir_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
			<tr><td colspan=3><br><h2>Biaya-biaya</h2></td></tr>
			<tr>
				<td colspan=2>Biaya Sopir</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaSopir'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td colspan=2>Biaya Tol</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaTol'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td colspan=2>Biaya Parkir</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaParkir'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td colspan=2>Biaya BBM</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaBBM'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr><td colspan=4 height=1 bgcolor='red'></td></tr>
			<tr>
				<td colspan=2>Total Biaya</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaSopir']+$data_biaya['BiayaTol']+$data_biaya['BiayaBBM']+$data_biaya['BiayaParkir'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
	  </table>");  
	exit;		
}
else 
if($aksi==1){
	//tombol OK di klik untuk mencetak SPJ
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	$Sopir			= new Sopir();
	$Mobil			= new Mobil();
	$Promo	= new Promo();
	$BiayaOperasional	= new BiayaOperasional();
	
	$sopir_dipilih= $HTTP_GET_VARS['sopir_dipilih'];
	$mobil_dipilih= $HTTP_GET_VARS['mobil_dipilih'];
	$no_spj				= $HTTP_GET_VARS['no_spj'];
	
	$nourut = rand(1000,9999);
	$useraktif=$userdata['user_id'];
	
	if ($tgl_berangkat=='' || $kode_jadwal==''){
		exit;
	}
	
	#mengambil data jadwal
	$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
	
	$is_sub_jadwal	= $data_jadwal['FlagSubJadwal'];
	
	if($is_sub_jadwal!=1){
		$kode_jadwal_utama			= $kode_jadwal;
	}
	else{
		$kode_jadwal_utama			= $data_jadwal['KodeJadwalUtama'];
	}
	
	$jam_berangkat	= $data_jadwal['JamBerangkat'];
	$list_kode_jadwal		= "'$kode_jadwal'";
	
	//mengambil layout kursi
	
	$layout_kursi = $Reservasi->ambilLayoutKursiByKodeJadwal($kode_jadwal_utama);
	
	$row	= $Sopir->ambilDataDetail($sopir_dipilih);
	$nama_sopir = $row['Nama'];
	
	$row				= $Mobil->ambilDataDetail($mobil_dipilih);
	$no_polisi 	= $row['NoPolisi'];
	
	
	//mendirect pencetakan spj sesuai dengan layout kursi
	
	//mengambil data tiket
	
	$row	= $Reservasi->ambilDetailSPJ($tgl_berangkat,$kode_jadwal);
	$no_polisi		= ($row['NoPolisi']=="")?$no_polisi:$row['NoPolisi'];
	
	$list_field_diupdate="";
	
	//Mengambil jumlah penumpang dan omzet
	$data_total	=$Reservasi->hitungTotalOmzetdanJumlahPenumpangPerSPJ($tgl_berangkat,$list_kode_jadwal);
					
	$total_omzet			=($data_total['TotalOmzet']!='')?$data_total['TotalOmzet']:0;
	$jumlah_penumpang	=($data_total['JumlahPenumpang']!='')?$data_total['JumlahPenumpang']:0;	
	
	//Mengambil jumlah paket dan omzet
	$data_total_paket	=$Reservasi->hitungTotalOmzetdanJumlahPaketPerSPJ($tgl_berangkat,$list_kode_jadwal);
	
	$total_omzet_paket=($data_total_paket['TotalOmzet']!='')?$data_total_paket['TotalOmzet']:0;
	$jumlah_paket			=($data_total_paket['JumlahPaket']!='')?$data_total_paket['JumlahPaket']:0;	
	
	//Mengambil pembiayaan berdasarkan jurusan
	$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($kode_jadwal_utama);
	
	//mengupdate field utk SPJ di tbl posisi
	$no_spj	= $row['NoSPJ'];
	
	if($no_spj==""){
		//UPDATE 20 JANUARI 2014 BY BARTON
		//setiap jadwal pick up (sub jadwal) dibuatkan SPJ nya tersendiri
		
		/*$no_spj= "MNF".substr($kode_jadwal_utama,0,3).dateYMD().$nourut; 
		
		$Reservasi->tambahSPJ(
			$no_spj, $kode_jadwal_utama, $tgl_berangkat, 
			$jam_berangkat, $layout_kursi, $jumlah_penumpang, 
			$mobil_dipilih, $useraktif, $sopir_dipilih,
			$nama_sopir,$total_omzet,
			$jumlah_paket,$total_omzet_paket);*/
		//dinonaktifkan karena setiap jadwal pickup juga dibuatkan spj nya sendiri
		
		$no_spj= "MNF".substr($kode_jadwal,0,3).dateYMD().$nourut;
		
		$Reservasi->tambahSPJ(
			$no_spj, $kode_jadwal, $tgl_berangkat, 
			$jam_berangkat, $layout_kursi, $jumlah_penumpang, 
			$mobil_dipilih, $useraktif, $sopir_dipilih,
			$nama_sopir,$total_omzet,
			$jumlah_paket,$total_omzet_paket,$is_sub_jadwal);
		
		//jika spj belum pernah dicetak, maka akan menambahkan biaya ke database
		
		//biaya sopir
		if(!$is_sub_jadwal){
			//jika bukan sub jadwal, biaya akan diposting
			
			if($data_biaya['BiayaSopir']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaSopir'],$FLAG_BIAYA_SOPIR,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaSopir'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya tol
			if($data_biaya['BiayaTol']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaTol'],$FLAG_BIAYA_TOL,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaTol'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya parkir
			if($data_biaya['BiayaParkir']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaParkir'],$FLAG_BIAYA_PARKIR,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaParkir'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya bbm
			if($data_biaya['BiayaBBM']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaBBM'],$FLAG_BIAYA_BBM,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaBBM'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
		}
		
		$duplikat	= ""; 
		
	}
	else{
		$jumlah_kali_cetak	= $Reservasi->ubahSPJ(
			$row['NoSPJ'], $jumlah_penumpang, 
			$mobil_dipilih, $useraktif, $sopir_dipilih,
			$nama_sopir,$total_omzet,
			$jumlah_paket,$total_omzet_paket);
			
		
		$no_spj	= $row['NoSPJ'];
		
		$duplikat	= "<br>**** COPY $jumlah_kali_cetak ****";
		
			
		//CATAT LOG MANIFEST
		$Reservasi->tambahLogCetakManifest(
			$no_spj,$kode_jadwal,$tgl_berangkat,
			$jam_berangkat,$layout_kursi,$jumlah_penumpang,
			$jumlah_paket,$mobil_dipilih,$sopir_dipilih,
			$nama_sopir,$total_omzet,$total_omzet_paket,
			$is_sub_jadwal,$userdata['user_id'],$userdata['nama']);
	}
		
	//update data pada tbl posisi
	$Reservasi->ubahPosisiCetakSPJ(
		$kode_jadwal_utama, $tgl_berangkat,$list_field_diupdate, 
		$sopir_dipilih,$mobil_dipilih,$no_spj,$useraktif);
		
	//update tblReservasi
	$Reservasi->ubahDataReservasiCetakSPJ(
		$list_kode_jadwal, $tgl_berangkat,$sopir_dipilih,
		$mobil_dipilih,$no_spj);
		
	//update tblspj untuk insentif sopir
	$Reservasi->updateInsentifSopir(
		$no_spj,$layout_kursi, $INSENTIF_SOPIR_LAYOUT_MAKSIMUM, 
		$INSENTIF_SOPIR_JUMLAH_PNP_MINIMUM, $data_biaya['KomisiPenumpangSopir']);
	
	
	if($jumlah_paket>0){
		$result_paket = $Reservasi->ambilDataPaketUntukSPJ($tgl_berangkat,$list_kode_jadwal);
		$idx_no=0;
		while($row_paket=$db->sql_fetchrow($result_paket)){
			$idx_no++;
			
			$template ->assign_block_vars(
				'ROW_PAKET',
				array(
					'IDX_PAKET_NO'=>"(".$idx_no.")",
					'NO_TIKET_PAKET'=>$row_paket['NoTiket'],
					'TUJUAN'=>$row_paket['Tujuan'],
					'NAMA_PENGIRIM'=>$row_paket['NamaPengirim'],
					'TELP_PENGIRIM'=>$row_paket['TelpPengirim'],
					'NAMA_PENERIMA'=>$row_paket['NamaPenerima'],
					'TELP_PENERIMA'=>$row_paket['TelpPenerima']
					)
			);
		}
	}
	else{
		$tidak_ada_paket	= '<br>TIDAK ADA PAKET<br><br>';
	}
	
	//list penumpang
	
	$result_penumpang = $Reservasi->ambilDataPenumpangUntukSPJ($tgl_berangkat,$list_kode_jadwal);
	
	
	//TIKETUX
	$total_omzet_online	= 0;
	
	$total_jenis_penumpang	= array();
	$total_omzet_penumpang	= array();
	
	if($jumlah_penumpang>0){
		while($row_penumpang=$db->sql_fetchrow($result_penumpang)){
			
			if($row_penumpang['JenisPembayaran']!=3){
				//PEMBAYARAN MENGGUNAKAN BUKAN VOUCHER
				
				$nama_index		= $row_penumpang['JenisPenumpang'];
				$nilai_omzet	= $row_penumpang['Total'];	
				
				switch($row_penumpang['JenisPenumpang']){
					case "T":
						$nama_index		= "O";
						$nilai_omzet	= $row_penumpang['Total']/*-10000*/;
					break;
					
					/*case "R":
						$nama_index		= "U";
						$nilai_omzet	= $row_penumpang['HargaTiket'];
						$total_omzet_penumpang["r"]	+= $row_penumpang['HargaTiket']-$row_penumpang['Discount'];
					break;*/
				
				}
				
			}
			else{
				//PEMBAYARAN MENGGUNAKAN VOUCHER
				
				//Mengambil Nilai Voucher
				$nama_index		= "VR";
				$nilai_omzet	= $row_penumpang['Total'] - $Promo->getNilaiVoucherByNoTiketPulang($row_penumpang['NoTiket']);
			}
			
			
			
			$total_jenis_penumpang[$nama_index]++;
			$total_omzet_penumpang[$nama_index]	+= $nilai_omzet;
			
			if($row_penumpang['PetugasCetakTiket']!=0){
				$is_online	= "";
			}
			else{
				$is_online	= "<br/>**VIA ONLINE**";
				$total_omzet_online	+= $row_penumpang['Total'];
			}
			
			$template ->assign_block_vars(
				'ROW_PENUMPANG',
				array(
					'NOMOR_KURSI'=>substr("0".$row_penumpang['NomorKursi'],-2),
					'NAMA'=>"(".$nama_index.") ".$row_penumpang['Nama'],
					'NO_TIKET'=>$row_penumpang['NoTiket'].$is_online,
					'TELP'=>$row_penumpang['Telp'],
					'ASAL'=>$row_penumpang['Asal'],
					'TUJUAN'=>$row_penumpang['Tujuan']
					)
			);
			
		}
		
	}
	else{
		$tidak_ada_penumpang	= '<br>TIDAK ADA PENUMPANG<br>';
	}
	//DEBUG
	//exit;
	
	$list_total_by_jenis_penumpang = "";
	
	foreach($total_jenis_penumpang as $index=>$value){
		$list_total_by_jenis_penumpang .= $index."=".$value."|";
	}
	
	$total_omzet_show	= 0;
	$total_pendapatan_tunai	= 0;
	
	foreach($total_omzet_penumpang as $index=>$value){
		$total_omzet_show	+= $value;
		
		if($index=="O" || $index=="VR"){
			$value	= -$value;
			$list_total_omzet_penumpang .= $index."=".substr("***************Rp.".number_format($value,0,",","."),-18)."<br>";
			$total_pendapatan_tunai	+= $value;
		}
	}
	
	$total_pendapatan_tunai	+= $total_omzet_show;
	
	$list_total_by_jenis_penumpang	= substr($list_total_by_jenis_penumpang,0,-1);
	
	$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
	
	//UPDATE SIGNATURE LAYOUT
	$Reservasi->updateLayoutSignature($tgl_berangkat,$kode_jadwal_utama);
	
	
	$template->set_filenames(array('body' => 'SPJ_body.tpl')); 
	$template->assign_vars(array(
	   'DUPLIKAT'    				=>$duplikat,
	   'NAMA_PERUSAHAAN'    =>$data_perusahaan['NamaPerusahaan'],
	   'ALAMAT_PERUSAHAAN'  =>$data_perusahaan['AlamatPerusahaan'],
	   'TELP_PERUSAHAAN' 		=>$data_perusahaan['TelpPerusahaan'],
	   'NO_SPJ' 						=>$no_spj,
	   'TGL_BERANGKAT' 			=>dateparse(FormatMySQLDateToTgl($tgl_berangkat)),
	   'JURUSAN' 						=>$kode_jadwal." ".$jam_berangkat,
	   'TGL_CETAK' 					=>FormatMySQLDateToTglWithTime(dateNow(true)),
	   'NO_POLISI' 					=>$mobil_dipilih." (".$no_polisi.")",
	   'SOPIR' 							=>$nama_sopir." (".$sopir_dipilih.")",
	   'TIDAK_ADA_PAKET' 		=>$tidak_ada_paket,
	   'JUMLAH_PAKET' 			=>$jumlah_paket,
	   'OMZET_PAKET' 				=>number_format($total_omzet_paket,0,",","."),
	   'TIDAK_ADA_PENUMPANG'=>$tidak_ada_penumpang,
	   'JUMLAH_PENUMPANG' 	=>$jumlah_penumpang,
	   'OMZET_PENUMPANG' 		=>substr("***************Rp.".number_format($total_omzet_show,0,",","."),-12),
	   'SUB_TOTAL_OMZET' 		=>substr("***************Rp.".number_format($total_omzet+$total_omzet_paket,0,",","."),-11),
		 'OMZET_ONLINE'				=>substr("***************Rp.".number_format($total_omzet_online,0,",","."),-14),
		 'TOTAL_OMZET' 				=>substr("***************Rp.".number_format($total_omzet+$total_omzet_paket-$total_omzet_online,0,",","."),-15),
	   'JENIS_PENUMPANG'		=>$list_total_by_jenis_penumpang,
		 'LIST_OMZET_PENUMPANG'=>$list_total_omzet_penumpang,
		 'PENDAPATAN_TUNAI'		=>substr("***************Rp.".number_format($total_pendapatan_tunai,0,",","."),-14),
		 'CSO' 								=>$userdata['nama'],
	   'EMAIL_PERUSAHAAN' 	=>$data_perusahaan['EmailPerusahaan'],
	   'WEBSITE_PERUSAHAAN' =>$data_perusahaan['WebSitePerusahaan'],
		 'SID'								=>$userdata['session_id']
	   )
	);

	$template->pparse('body');	
		
	
}
?>