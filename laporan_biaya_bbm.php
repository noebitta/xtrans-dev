<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSpbu.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_KASIR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

function setComboSPBU($sbpu_dipilih){
	//SET COMBO SPBU
	global $db;
	$SPBU	= new SPBU();
			
	$result=$SPBU->ambilData("","Nama,Kota","ASC");
	$opt_sbpu="<option value=0>- Semua SPBU -</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($sbpu_dipilih!=$row['IdSPBU'])?"":"selected";
			$opt_sbpu .="<option value='$row[IdSPBU]' $selected>$row[Nama] $row[Kota] ($row[KodeSPBU])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_sbpu;
	//END SET COMBO SPBU
}

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$spbu  					= isset($HTTP_GET_VARS['opt_spbu'])? $HTTP_GET_VARS['opt_spbu'] : $HTTP_POST_VARS['opt_spbu'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

// LIST
$template->set_filenames(array('body' => 'laporan_biaya_bbm/laporan_biaya_bbm_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$kondisi	= "WHERE (TglVoucher BETWEEN CONVERT(datetime,'$tanggal_mulai',105) AND CONVERT(datetime,'$tanggal_akhir',105))";

$kondisi_spbu	=($spbu==0 || $spbu=="")?"":
	" AND IdSPBU = '$spbu' ";

$kondisi_cari	=($cari=="")?"":
	" AND (NoSPJ LIKE '%$cari'
		OR NoPolisi LIKE '%$cari%')";
	
$kondisi	= $kondisi.$kondisi_spbu.$kondisi_jb.$kondisi_cari;

			
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingDataWithJoin($idx_page,"IdVoucherBBM","TbVoucherBBM",
	"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&opt_spbu=$spbu",
	$kondisi,"laporan_biaya_bbm.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		IdVoucherBBM,KodeVoucher,NoSPJ,CONVERT(varchar(12),TglVoucher,106) as TanggalVoucher,NoPolisi,
		dbo.f_SopirGetNamaByKode(KodeSopir) AS NamaSopir,Kilometer,
		JenisBBM,JumlahLiter,JumlahBiaya,
		dbo.f_CabangGetNameByKode(KodeCabang) AS Cabang,
		dbo.f_SPBUGetNameByKode(IdSPBU) AS SPBU,
		dbo.f_UserGetNameById(IdPetugas) AS Kasir,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangAsalByIdJurusan(IdJurusan)) Asal,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangTujuanByIdJurusan(IdJurusan)) Tujuan
		
	FROM 
		TbVoucherBBM
	$kondisi
	ORDER BY TglVoucher,NoSPJ,IdVoucherBBM LIMIT $idx_awal_record,$VIEW_PER_PAGE";	
	//ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";	


if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
	
	$total_biaya_bbm	=	0;
	
  while ($row = $db->sql_fetchrow($result)){
		
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'tgl_voucher'=>$row['TanggalVoucher'],
					'kode'=>$row['KodeVoucher'],
					'no_spj'=>$row['NoSPJ'],
					'jurusan'=>$row['Asal']."-".$row['Tujuan'],
					'no_pol'=>$row['NoPolisi'],
					'sopir'=>$row['NamaSopir'],
					'jenis_bbm'=>$LIST_BBM[$row['JenisBBM']],
					'jumlah'=>number_format($row['JumlahBiaya'],0,",","."),
					'km'=>number_format($row['Kilometer'],0,",","."),
					'jumlah_liter'=>number_format($row['JumlahLiter'],0,",","."),
					'spbu'=>$row['SPBU'],
					'kasir'=>$row['Kasir']
				)
			);
		
		$total_biaya_bbm +=$row['JumlahBiaya'];
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Error :".__LINE__);exit;
} 

//SUMMARY

	$summary = "
		<table width=1210>
			<tr><td colspan=4><h2>SUMMARY</h2></td></tr>
			<tr><td width='240'>
				<table width='240' class='border'>
					<tr><th colspan=3><font size=3>BBM</font></th></tr>
					<tr>
						<td width=100>Total</td><td width=50>: Rp.</td><td width=150 align='right'>".number_format($total_biaya_bbm,0,",",".")."</td>
					</tr>
				</table>
			</td></tr>
		</table>
	";


$temp_var_jb		= "jb".$jenis_biaya;
$$temp_var_jb		= "selected";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$cari."&p4=".$spbu;
	
$script_cetak_pdf="Start('laporan_biaya_bbm_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_biaya_bbm_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'BCRUMP'    		=>  '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('menu_laporan.'.$phpEx).'">Laporan</a> | <a href="'.append_sid('laporan_biaya_bbm.'.$phpEx).'">Laporan Biaya BBM</a>',
	'ACTION_CARI'		=> append_sid('laporan_biaya_bbm.'.$phpEx),
	'OPT_SPBU'		=> setComboSPBU($spbu),
	'JENIS_BIAYA_'	=>$jb,
	'JENIS_BIAYA_0'	=>$jb0,
	'JENIS_BIAYA_1'	=>$jb1,
	'JENIS_BIAYA_2'	=>$jb2,
	'JENIS_BIAYA_3'	=>$jb3,
	'JENIS_BIAYA_4'	=>$jb4,
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>