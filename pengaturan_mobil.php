<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Mobil	= new Mobil();
$Sopir	= new Sopir();
$Cabang	= new Cabang();

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	global $Sopir;
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	$opt_sopir="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
}

function setComboCabang($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

	
	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'mobil/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'	=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_mobil.'.$phpEx).'">Mobil</a> | <a href="'.append_sid('pengaturan_mobil.'.$phpEx."?mode=add").'">Tambah Mobil</a> ',
		 'JUDUL'	=>'Tambah Data Mobil',
		 'MODE'   => 'save',
		 'SUB'    => '0',
		 'SOPIR1'  => setComboSopir(""),
		 'SOPIR2'  => setComboSopir(""),
		 'OPT_CABANG' 			=> setComboCabang(""),
		 'KURSI' 						=> setComboLayoutKursi(10),
		 'PESAN'						=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_MOBIL_ADD_ACT'	=> append_sid('pengaturan_mobil.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah mobil
		$kode_kendaraan  		= str_replace(" ","",$HTTP_POST_VARS['kode_kendaraan']);
		$kode_kendaraan_old	= str_replace(" ","",$HTTP_POST_VARS['kode_kendaraan_old']);
		$no_polisi  				= str_replace(" ","",$HTTP_POST_VARS['no_polisi']);
		$no_polisi_old			= str_replace(" ","",$HTTP_POST_VARS['no_polisi_old']);
		$jenis   						= $HTTP_POST_VARS['jenis'];
		$merek   						= $HTTP_POST_VARS['merek'];
		$tahun_pembuatan		= $HTTP_POST_VARS['tahun_pembuatan'];
		$warna   						= $HTTP_POST_VARS['warna'];
		$jumlah_kursi				= $HTTP_POST_VARS['jumlah_kursi'];
		$sopir1   					= $HTTP_POST_VARS['sopir1'];
		$sopir2   					= $HTTP_POST_VARS['sopir2'];
		$no_mesin   				= $HTTP_POST_VARS['no_mesin'];
		$no_stnk   					= $HTTP_POST_VARS['no_stnk'];
		$no_rangka 					= $HTTP_POST_VARS['no_rangka'];
		$no_bpkb   					= $HTTP_POST_VARS['no_bpkb'];
		$km   							= $HTTP_POST_VARS['km'];
		$cabang   					= $HTTP_POST_VARS['cabang'];
		$status_aktif   		= $HTTP_POST_VARS['aktif'];
		$tgl_serah_terima		= $HTTP_POST_VARS['tglserahterima'];
		$atpm								= $HTTP_POST_VARS['atpm'];
		$stnk_expired				= $HTTP_POST_VARS['stnkberlakuhingga'];
		$pajak_expired			= $HTTP_POST_VARS['pajakberlakuhingga'];
		$no_kir							= $HTTP_POST_VARS['nokir'];
		$kir_expired				= $HTTP_POST_VARS['kirberlakuhingga'];
		$no_kontrak					= $HTTP_POST_VARS['nokontrak'];
		$perusahaan_leasing	= $HTTP_POST_VARS['perusahaanleasing'];
		$harga_otr					= $HTTP_POST_VARS['hargaotr'];
		$angsuran_perbulan	= $HTTP_POST_VARS['angsuranperbulan'];
		$awal_angsuran			= $HTTP_POST_VARS['awalangsuran'];
		$tgl_jatuh_tempo		= $HTTP_POST_VARS['tgljatuhtempo'];
		$no_polis						= $HTTP_POST_VARS['nopolis'];
		$perusahaan_asuransi= $HTTP_POST_VARS['perusahaanasuransi'];
		$tjh								= $HTTP_POST_VARS['tjh'];
		$masa_berlaku_asuransi	= $HTTP_POST_VARS['asuransiberlakuhingga'];
		$tgl_bpkb_diterima	= $HTTP_POST_VARS['tglbpkbditerima'];
		$tgl_bpkb_dilepas		= $HTTP_POST_VARS['tglbpkbdilepas'];
		$nopol_hitam				= $HTTP_POST_VARS['nopolhitam'];
		
		
		$terjadi_error=false;
		
		if($Mobil->periksaDuplikasi($kode_kendaraan) && $kode_kendaraan!=$kode_kendaraan_old){
			$pesan="<font color='white' size=3>Kode Kendaraan yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else if($Mobil->periksaDuplikasiNoPol($no_polisi) && $no_polisi!=$no_polisi_old){
			$pesan="<font color='white' size=3>No Polisi yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Mobil";
				$path	='<a href="'.append_sid('pengaturan_mobil.'.$phpEx."?mode=add").'">Tambah Mobil</a> ';
				
				if($Mobil->tambah(
					$kode_kendaraan, $cabang, $no_polisi,
				  $jenis,$merek, $tahun_pembuatan, $warna,
				  $jumlah_kursi, $sopir1, $sopir2,
				  $no_stnk, $no_bpkb, $no_rangka,
				  $no_mesin, $km, $tgl_serah_terima,
					$atpm,$stnk_expired,$pajak_expired,
					$no_kir,$kir_expired,$no_kontrak,
					$perusahaan_leasing,$harga_otr,$angsuran_perbulan,
					$awal_angsuran,$tgl_jatuh_tempo,$no_polis,
					$perusahaan_asuransi,$tjh,$masa_berlaku_asuransi,
					$tgl_bpkb_diterima,$tgl_bpkb_dilepas,$nopol_hitam,
					$status_aktif)){
								
					redirect(append_sid('pengaturan_mobil.'.$phpEx.'?mode=add&pesan=1',true));
					
				}
			}
			else{
				
				$judul="Ubah Data Mobil";
				$path	='<a href="'.append_sid('pengaturan_mobil.'.$phpEx."?mode=edit&id=$no_polisi_old").'">Ubah Mobil</a> ';
				
				if($Mobil->ubah(
					$kode_kendaraan_old,
					$kode_kendaraan, $cabang, $no_polisi,
				  $jenis,$merek, $tahun_pembuatan, $warna,
				  $jumlah_kursi, $sopir1, $sopir2,
				  $no_stnk, $no_bpkb, $no_rangka,
				  $no_mesin, $km, $tgl_serah_terima,
					$atpm,$stnk_expired,$pajak_expired,
					$no_kir,$kir_expired,$no_kontrak,
					$perusahaan_leasing,$harga_otr,$angsuran_perbulan,
					$awal_angsuran,$tgl_jatuh_tempo,$no_polis,
					$perusahaan_asuransi,$tjh,$masa_berlaku_asuransi,
					$tgl_bpkb_diterima,$tgl_bpkb_dilepas,$nopol_hitam,
					$status_aktif)){
						
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}
			}
			
		}
		
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'mobil/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'	=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_mobil.'.$phpEx).'">Mobil</a> | '.$path,
			 'JUDUL'	=>$judul,
			 'MODE'   => 'save',
			 'SUB'    => $submode,
			 'KODE_KENDARAAN_OLD'=> $kode_kendaraan_old,
			 'KODE_KENDARAAN'    => $kode_kendaraan,
			 'NO_POLISI_OLD'    => $no_polisi_old,
			 'NO_POLISI'    		=> $no_polisi,
			 'JENIS'    				=> $jenis,
			 'MEREK'    				=> $merek,
			 'TAHUN_PEMBUATAN'	=> $tahun_pembuatan,
			 'WARNA'						=> $warna,
			 'KURSI'						=> setComboLayoutKursi($jumlah_kursi),
			 'SOPIR1'  					=> setComboSopir($sopir1),
			 'SOPIR2'  					=> setComboSopir($sopir2),
			 'NO_MESIN'  				=> $no_mesin,
			 'NO_RANGKA'  			=> $no_rangka,
			 'NO_STNK' 					=> $no_stnk,
			 'NO_BPKB' 					=> $no_bpkb,
			 'KM'								=> $km,
			 'TGL_SERAH_TERIMA'	=> $tgl_serah_terima,
			 'ATPM'							=> $atpm,
			 'STNK_BERLAKU_HINGGA'	=> $stnk_expired,
			 'PAJAK_BERLAKU_HINGGA'	=> $pajak_expired,
			 'NO_KIR'						=> $no_kir,
			 'KIR_BERLAKU_HINGGA'		=> $kir_expired,
			 'NO_KONTRAK'				=> $no_kontrak,
			 'PERUSAHAAN_LEASING'		=> $perusahaan_leasing,
			 'HARGA_OTR'				=> $harga_otr,
			 'ANGSURAN_PERBULAN'=> $angsuran_perbulan,
			 'AWAL_ANGSURAN'		=> $awal_angsuran,
			 'TGL_JATUH_TEMPO'	=> $tgl_jatuh_tempo,
			 'NO_POLIS'					=> $no_polis,
			 'PERUSAHAAN_ASURANSI'=> $perusahaan_asuransi,
			 'TJH'							=> $tjh,
			 'ASURANSI_BERLAKU_HINGGA'	=> $masa_berlaku_asuransi,
			 'TGL_BPKB_DITERIMA'=> $tgl_bpkb_diterima,
			 'TGL_BPKB_DILEPAS'	=> $tgl_bpkb_dilepas,
			 'NO_POL_HITAM'			=> $nopol_hitam,
			 'AKTIF_1'					=> $status_aktif_1,
			 'AKTIF_0'					=> $status_aktif_0,
			 'OPT_CABANG'				=> setComboCabang($cabang),
			 'PESAN'						=> $pesan,
			 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
			 'U_MOBIL_ADD_ACT'	=>append_sid('pengaturan_mobil.'.$phpEx)
			)
		);
		
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Mobil->ambilDataDetail($id);
	
		
		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'mobil/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'	=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_mobil.'.$phpEx).'">Mobil</a> | <a href="'.append_sid('pengaturan_mobil.'.$phpEx."?mode=edit&id=$id").'">Ubah Mobil</a> ',
			 'JUDUL'	=>'Ubah Data Mobil',
			 'MODE'   => 'save',
			 'SUB'    => '1',
			 'KODE_KENDARAAN_OLD'=> $row['KodeKendaraan'],
			 'KODE_KENDARAAN'   => $row['KodeKendaraan'],
			 'NO_POLISI_OLD'    => $row['NoPolisi'],
			 'NO_POLISI'    		=> $row['NoPolisi'],
			 'JENIS'    				=> $row['Jenis'],
			 'MEREK'    				=> $row['Merek'],
			 'TAHUN_PEMBUATAN'	=> $row['Tahun'],
			 'WARNA'						=> $row['Warna'],
			 'KURSI'						=> setComboLayoutKursi($row['JumlahKursi']),
			 'SOPIR1'  					=> setComboSopir($row['KodeSopir1']),
			 'SOPIR2'  					=> setComboSopir($row['KodeSopir2']),
			 'NO_MESIN'  				=> $row['NoMesin'],
			 'NO_RANGKA'  			=> $row['NoRangka'],
			 'NO_STNK' 					=> $row['NoSTNK'],
			 'NO_BPKB' 					=> $row['NoBPKB'],
			 'KM'								=> $row['KilometerAkhir'],
			 'TGL_SERAH_TERIMA'	=> FormatMySQLDateToTgl($row['TglSerahTerima']),
			 'ATPM'							=> $row['ATPM'],
			 'STNK_BERLAKU_HINGGA'	=> FormatMySQLDateToTgl($row['STNKExpired']),
			 'PAJAK_BERLAKU_HINGGA'	=> FormatMySQLDateToTgl($row['PajakExpired']),
			 'NO_KIR'						=> $row['NoKIR'],
			 'KIR_BERLAKU_HINGGA'		=> FormatMySQLDateToTgl($row['KIRExpired']),
			 'NO_KONTRAK'				=> $row['NoKontrak'],
			 'PERUSAHAAN_LEASING'		=> $row['NoKontrak'],
			 'HARGA_OTR'				=> $row['HargaOTR'],
			 'ANGSURAN_PERBULAN'=> $row['AngsuranPerBulan'],
			 'AWAL_ANGSURAN'		=> FormatMySQLDateToTgl($row['AwalAngsuran']),
			 'TGL_JATUH_TEMPO'	=> $row['TglJatuhTempo'],
			 'NO_POLIS'					=> $row['NoPolis'],
			 'PERUSAHAAN_ASURANSI'=> $row['PerusahaanAsuransi'],
			 'TJH'							=> $row['TJH'],
			 'ASURANSI_BERLAKU_HINGGA'	=> FormatMySQLDateToTgl($row['MasaBerlakuAsuransi']),
			 'TGL_BPKB_DITERIMA'=> FormatMySQLDateToTgl($row['TglBPKBDiterima']),
			 'TGL_BPKB_DILEPAS'	=> FormatMySQLDateToTgl($row['TglBPKBDilepas']),
			 'NO_POL_HITAM'			=> $row['NoPolHitam'],
			 'AKTIF_1'					=> $status_aktif_1,
			 'AKTIF_0'					=> $status_aktif_0,
			 'OPT_CABANG'				=> setComboCabang($row['KodeCabang']),
			 'PESAN'						=> $pesan,
			 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
			 'U_MOBIL_ADD_ACT'	=>append_sid('pengaturan_mobil.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus mobil
		$list_mobil = str_replace("\'","'",$HTTP_GET_VARS['list_mobil']);
		//echo($list_mobil. " asli :".$HTTP_GET_VARS['list_mobil']);
		$Mobil->hapus($list_mobil);
		
		exit;
	} 
	else if ($mode=='ubahstatus'){
		// aksi hapus jadwal
		$kode_kendaraan = str_replace("\'","'",$HTTP_GET_VARS['kode_kendaraan']);
	
		$Mobil->ubahStatusAktif($kode_kendaraan);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'mobil/mobil_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?""
			:" WHERE KodeKendaraan LIKE '%$cari%'
					OR NoPolisi LIKE '%$cari%'
					OR Merek LIKE '%$cari%'
					OR NoSTNK LIKE '%$cari%'
					OR PerusahaanAsuransi LIKE '%$cari%'
					OR ATPM LIKE '%$cari%'
					OR NoKIR LIKE '%$cari%'
					OR NoKontrak LIKE '%$cari%'
					OR PerusahaanLeasing LIKE '%$cari%'
					OR NoPolis LIKE '%$cari%'
					OR TJH LIKE '%$cari%'
					OR NoPolHitam LIKE '%$cari%'";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"KodeKendaraan","tbl_md_kendaraan","",$kondisi,"pengaturan_mobil.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *,(SELECT Nama FROM tbl_md_sopir WHERE KodeSopir=KodeSopir1) AS Sopir
			FROM tbl_md_kendaraan $kondisi 
			ORDER BY KodeKendaraan LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[KodeKendaraan]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_mobil.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[KodeKendaraan]\");'>Delete</a>";
				
				if($row['FlagAktif']){
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeKendaraan]\")'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeKendaraan]\")'>Tidak Aktif</a>";
				}
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'								=>$odd,
							'check'							=>$check,
							'no'								=>$i,
							'tglserahterima'		=>dateparse(FormatMySQLDateToTgl($row['TglSerahTerima'])),
							'norangka'					=>$row['NoRangka'],
							'nomesin'						=>$row['NoMesin'],
							'kode'							=>$row['KodeKendaraan'],
							'merek'							=>$row['Merek']." ".$row['Jenis'],
							'kapasitas'					=>substr($row['JumlahKursi'],0,2)."-".substr($row['JumlahKursi'],2,1),
							'atpm'							=>$row['ATPM'],
							'plat'							=>$row['NoPolisi'],
							'stnkexpired'				=>dateparse(FormatMySQLDateToTgl($row['STNKExpired'])),
							'pajakexpired'			=>dateparse(FormatMySQLDateToTgl($row['PajakExpired'])),
							'nokir'							=>$row['NoKIR'],
							'kirexpired'				=>dateparse(FormatMySQLDateToTgl($row['KIRExpired'])),
							'nokontrak'					=>$row['NoKontrak'],
							'perusahaanleasing'	=>$row['PerusahaanLeasing'],
							'hargaotr'					=>number_format($row['HargaOTR'],0,",","."),
							'angsuranperbulan'	=>number_format($row['AngsuranPerBulan'],0,",","."),
							'awalangsuran'			=>dateparse(FormatMySQLDateToTgl($row['AwalAngsuran'])),
							'tgljatuhtempo'			=>$row['TglJatuhTempo'],
							'nopolis'						=>$row['NoPolis'],
							'perusahaanasuransi'=>$row['PerusahaanAsuransi'],
							'tjh'								=>$row['TJH'],
							'masaberlakuasuransi'=>dateparse(FormatMySQLDateToTgl($row['MasaBerlakuAsuransi'])),
							'tglbpkbditerima'		=>dateparse(FormatMySQLDateToTgl($row['TglBPKBDiterima'])),
							'tglbpkbdilepas'		=>dateparse(FormatMySQLDateToTgl($row['TglBPKBDilepas'])),
							'nopolhitam'				=>$row['NoPolHitam'],
							'aktif'							=>$status,
							'action'						=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
			
		} 
		else{
			//die_error('Cannot Load mobil',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&cari=".$cari;														
		$script_cetak_excel="Start('pengaturan_mobil_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		//--END KOMPONEN UNTUK EXPORT
		
				
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_mobil.'.$phpEx).'">Mobil</a>',
			'U_MOBIL_ADD'		=> append_sid('pengaturan_mobil.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_mobil.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging,
			'CETAK_XL'			=> $script_cetak_excel
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>