<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari  					= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$cabang  				= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$tgl_awal  			= isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
$tgl_akhir 			= isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$sort_by				= isset($HTTP_GET_VARS['sortby'])? $HTTP_GET_VARS['sortby'] : $HTTP_POST_VARS['sortby'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'laporan_penjualan_cso_callcenter/index.tpl')); 

$tgl_awal		= ($tgl_awal!='')?$tgl_awal:dateD_M_Y();
$tgl_akhir	= ($tgl_akhir!='')?$tgl_akhir:dateD_M_Y();

$tgl_awal_mysql		= FormatTglToMySQLDate($tgl_awal);
$tgl_akhir_mysql	= FormatTglToMySQLDate($tgl_akhir);

//MENGAMBIL PENGATURAN CABANG CALL CENTER
$sql = 
	"SELECT *
	FROM tbl_pengaturan_parameter
	WHERE NamaParameter LIKE 'CALLCENTER_%'
	ORDER BY NamaParameter;";
				
if(!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}
		
$data_cc	= array();
		
while ($row=$db->sql_fetchrow($result)){
	$field_name	= $row['NilaiParameter'];
	$data_cc[$field_name]	= $row['Deskripsi'];
	$cabangs_callcenter	.= "'$row[NilaiParameter]',";
	
	$selected_cabang	= $cabang==$row['NilaiParameter']?"selected":"";
	
	$template->
		assign_block_vars(
			'CABANG',
			array(
				'value'			=> $row['NilaiParameter'],
				'nama'			=> $row['Deskripsi'],
				'selected'	=> $selected_cabang
			)
	);
}

$cabangs_callcenter	= substr($cabangs_callcenter,0,-1);

$kondisi_cabang	= $cabang==""?"":" AND CabangPesan='$cabang'";

$kondisi=
	"(DATE(WaktuPesan) BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql')
		AND CabangPesan IN($cabangs_callcenter)
		$kondisi_cabang
		AND (username='$cari'
	  OR tu.nama LIKE '%$cari%' 
		OR tu.telp LIKE '%$cari%'
		OR NRP LIKE '%$cari%')";

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"NamaCSO":$sort_by;
			
$sql	= 
	"SELECT 
		tu.nama AS NamaCSO,NRP,
		GROUP_CONCAT(DISTINCT CabangPesan ORDER BY CabangPesan SEPARATOR ',') AS CabangTugas,
		PetugasPenjual,
		IS_NULL(COUNT(NoTiket),0) AS Tiket,
		IS_NULL(COUNT(DISTINCT(KodeBooking)),0) AS Booking,
		MIN(WaktuPesan) AS BookTerawal,
		MAX(WaktuPesan) AS BookTerakhir
	FROM tbl_reservasi_olap tr INNER JOIN tbl_user tu ON tr.PetugasPenjual=tu.user_id
	WHERE $kondisi
	GROUP BY PetugasPenjual
	ORDER BY $sort_by $order";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//PLOT DATA
while($row=$db->sql_fetchrow($result)){
	
	$idx++;
	
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_penjualan_cso_callcenter_detail.php?cso='.$row['PetugasPenjual'].'&namacso='.$row['NamaCSO'].'&tglawal='.$tgl_awal_mysql.'&tglakhir='.$tgl_akhir_mysql)."\");return false'>Detail<a/>";
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx,
				'nama'=>$row['NamaCSO'],
				'nrp'=>$row['NRP'],
				'cabang'=>$row['CabangTugas'],
				'booking'=>number_format($row['Booking'],0,",","."),
				'tiket'=>number_format($row['Tiket'],0,",","."),
				'booking_terawal'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['BookTerawal'])),
				'booking_terakhir'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['BookTerakhir'])),
				'act'=>$act
			)
		);
		
} 

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tglawal=".$tgl_awal_mysql."&tglakhir=".$tgl_akhir_mysql."&cari=".$cari."&cabang=".$cabang."&sortby=".$sort_by."&order=".$order."";
											
$script_cetak_excel="Start('laporan_penjualan_cso_callcenter_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//BEGIN KOMPONEN-KOMPONEN SORTING================================
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';

$parameter_sorting	= 
	"&page=".$idx_page."&tglawal=".$tgl_awal."&tglakhir=".$tgl_akhir."&cari=".$cari."&cabang=".$cabang."&order=".$order_invert;

$array_sort	= 
	"'".append_sid('laporan_penjualan_cso_callcenter.php').'&sortby=NamaCSO'.$parameter_sorting."',".
	"'".append_sid('laporan_penjualan_cso_callcenter.php').'&sortby=NRP'.$parameter_sorting."',".
	"'".append_sid('laporan_penjualan_cso_callcenter.php').'&sortby=CabangTugas'.$parameter_sorting."',".
	"'".append_sid('laporan_penjualan_cso_callcenter.php').'&sortby=Booking'.$parameter_sorting."',".
	"'".append_sid('laporan_penjualan_cso_callcenter.php').'&sortby=Tiket'.$parameter_sorting."',".
	"'".append_sid('laporan_penjualan_cso_callcenter.php').'&sortby=BookTerawal'.$parameter_sorting."',".
	"'".append_sid('laporan_penjualan_cso_callcenter.php').'&sortby=BookTerakhir'.$parameter_sorting."'";

//END KOMPONEN-KOMPONEN SORTING================================
	
$template->assign_vars(array(
	'BCRUMP'    				=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_callcenter">Home</a> | <a href="'.append_sid('laporan_penjualan_cso_callcenter.'.$phpEx).'">Laporan Penjualan CSO Call Center</a>',
	'ACTION_CARI'				=> append_sid('laporan_penjualan_cso_callcenter.'.$phpEx),
	'CARI'							=> $cari,
	'SELECT_ALL_CABANG'	=> $cabang==""?"selected":"",
	'TGL_AWAL'					=> $tgl_awal,
	'TGL_AKHIR'					=> $tgl_akhir,
	'CETAK_XL'					=> $script_cetak_excel,
	'ARRAY_SORT'				=> $array_sort
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>