<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassVoucherDiskon.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$waktu_cetak= isset($HTTP_GET_VARS['waktucetak'])? $HTTP_GET_VARS['waktucetak'] : $HTTP_POST_VARS['waktucetak'];
$id_group 	= isset($HTTP_GET_VARS['idgroup'])? $HTTP_GET_VARS['idgroup'] : $HTTP_POST_VARS['idgroup'];
$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

if($id_group==""){
	echo("ILEGAL ACCESS!");
	exit;
}

$Voucher	= new Voucher();

$data_group	= $Voucher->getDetailGroup($id_group);
	
$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', "DAFTAR VOUCHER KORPORAT '".strtoupper($data_group['NamaGroup'])."' BATCH:".dateparseWithTime(FormatMySQLDateToTglWithTime($waktu_cetak)));
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', '#Voucher');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Expired');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Digunakan');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'CSO');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', '#Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', '#Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
//QUERY
$kondisi	=($cari=="")?"":
	" AND (Keterangan LIKE '%$cari%'
	OR KodeVoucher LIKE '%$cari'
	OR NamaPetugasPencetak LIKE '%$cari%'
	OR NamaPenumpang LIKE '%$cari%'
	OR Telp LIKE '%$cari%'
	OR NoTiketBerangkat LIKE '%$cari%')";
			
$sql = 
	"SELECT *
	FROM tbl_voucher
	WHERE IdGroup='$id_group' $kondisi AND WaktuCetak='$waktu_cetak'
	ORDER BY WaktuCetak";

if (!$result = $db->sql_query($sql)){
	echo("Err :".__LINE__);exit;
}

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+3;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['KodeVoucher']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['Keterangan']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['ExpiredDate'])));	
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuDigunakan'])));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NamaPetugasPengguna']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NoTiketBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['KodeJadwalBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['NamaPenumpang']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['Telp']);

}
$temp_idx=$idx_row;
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="DAFTAR VOUCHER KORPORAT '.strtoupper($data_group['NamaGroup']).' BATCH:'.dateparseWithTime(FormatMySQLDateToTglWithTime($waktu_cetak)).'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}


?>
