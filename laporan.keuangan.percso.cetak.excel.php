<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR))){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$cari           = isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan.keuangan.percso/index.tpl'));

//$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$id_sort				= isset($HTTP_GET_VARS['idsort'])? $HTTP_GET_VARS['idsort'] : $HTTP_POST_VARS['idsort'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

/*SELECT SORT*/
switch($id_sort){
  case "1":
    $sort_by  = "Nama";
    break;
  case "2":
    $sort_by  = "NRP";
    break;
  case "3":
    $sort_by  = "Cabang";
    break;
  case "4":
    $sort_by="TotalPenumpangU";
    break;
  case "5":
    $sort_by="TotalPenumpangM";
    break;
  case "6":
    $sort_by="TotalPenumpangK";
    break;
  case "7":
    $sort_by="TotalPenumpangKK";
    break;
  case "8":
    $sort_by="TotalPenumpangG";
    break;
  case "9":
    $sort_by="TotalPenumpangR";
    break;
  case "10":
    $sort_by="TotalPenumpangVR";
    break;
  case "11":
    $sort_by="TotalPenumpangV";
    break;
  case "12":
    $sort_by="TotalTiket";
    break;
  case "13":
    $sort_by="TotalTiketTunai";
    break;
  case "14":
    $sort_by="TotalTiketDebit";
    break;
  case "15":
    $sort_by="TotalTiketKredit";
    break;
  case "16":
    $sort_by="TotalDiscount";
    break;
  case "17":
    $sort_by="TotalUangTiket";
    break;
  case "18":
    $sort_by="TotalPaketP";
    break;
  case "19":
    $sort_by="TotalPaketGD";
    break;
  case "20":
    $sort_by="TotalPaketGA";
    break;
  case "21":
    $sort_by="TotalPaketS";
    break;
  case "22":
    $sort_by="TotalPaketCA";
    break;
  case "23":
    $sort_by="TotalPaketCD";
    break;
  case "24":
    $sort_by="TotalPaketI";
    break;
  case "25":
    $sort_by="TotalPaket";
    break;
  case "26":
    $sort_by="PaketOmzetTunai";
    break;
  case "27":
    $sort_by="PaketOmzetLangganan";
    break;
  case "28":
    $sort_by="PaketTotalDiskon";
    break;
  case "29":
    $sort_by="TotalUangPaket";
    break;
  case "30":
    $sort_by="TotalSetor";
    break;
}

$sort_by  = $id_sort==""?"Nama":$sort_by;

$kondisi	=($cari=="")?"":
  " AND (username='$cari'
	  OR nama LIKE '%$cari%'
		OR telp LIKE '%$cari%'
		OR NRP LIKE '%$cari%')";

$sql=
  "SELECT
	  user_id,nama,NRP,KodeCabang,f_cabang_get_name_by_kode(KodeCabang) AS cabang
	FROM
	  tbl_user
	WHERE user_active=1 $kondisi";

if (!$result_laporan = $db->sql_query($sql)){
  echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	=
  "SELECT
		PetugasCetakTiket,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(1),0) AS TotalTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=0,Total,0),IF(JenisPembayaran=0,HargaTiket,0))),0) AS TotalTiketTunai,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=1,Total,0),IF(JenisPembayaran=1,HargaTiket,0))),0) AS TotalTiketDebit,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=2,Total,0),IF(JenisPembayaran=2,HargaTiket,0))),0) AS TotalTiketKredit,
		IS_NULL(SUM(SubTotal),0) AS TotalUangTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,Total,0),HargaTiket)),0) AS TotalOmzet
	FROM tbl_reservasi_olap
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
		AND (FlagPesanan IS NULL OR FlagPesanan=0)
	GROUP BY PetugasCetakTiket ORDER BY PetugasCetakTiket";

if (!$result = $db->sql_query($sql)){
  echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
  $data_tiket[$row['PetugasCetakTiket']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	=
  "SELECT
		PetugasPenjual,
		IS_NULL(COUNT(IF(Layanan='P',1,NULL)),0) AS TotalPaketP,
		IS_NULL(COUNT(IF(Layanan='GD',1,NULL)),0) AS TotalPaketGD,
		IS_NULL(COUNT(IF(Layanan='GA',1,NULL)),0) AS TotalPaketGA,
		IS_NULL(COUNT(IF(Layanan='S',1,NULL)),0) AS TotalPaketS,
		IS_NULL(COUNT(IF(Layanan='CA',1,NULL)),0) AS TotalPaketCA,
		IS_NULL(COUNT(IF(Layanan='CD',1,NULL)),0) AS TotalPaketCD,
		IS_NULL(COUNT(IF(Layanan='I',1,NULL)),0) AS TotalPaketI,
		IS_NULL(COUNT(1),0) AS TotalPaket,
		IS_NULL(SUM(IF(JenisPembayaran=0,TotalBayar,0)),0) AS OmzetPaketTunai,
		IS_NULL(SUM(IF(JenisPembayaran!=0,TotalBayar,0)),0) AS OmzetPaketLangganan,
		IS_NULL(SUM(Diskon),0) AS OmzDiskon,
		IS_NULL(SUM(TotalBayar),0) AS TotalPenjualanPaket
	FROM tbl_paket
	WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY  PetugasPenjual ORDER BY PetugasPenjual";

if (!$result = $db->sql_query($sql)){
  echo("Err: ".__LINE__);exit;
}

//debug
//echo($sql);exit;

while ($row = $db->sql_fetchrow($result)){
  $data_paket[$row['PetugasPenjual']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){
  $temp_array[$idx]['user_id']						= $row['user_id'];
  $temp_array[$idx]['Nama']								= $row['nama'];
  $temp_array[$idx]['NRP']								= $row['NRP'];
  $temp_array[$idx]['UserId']							= $row['user_id'];
  $temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
  $temp_array[$idx]['Cabang']					    = $row['cabang'];

  $temp_array[$idx]['TotalPenumpangU']		= $data_tiket[$row['user_id']]['TotalPenumpangU'];
  $temp_array[$idx]['TotalPenumpangM']		= $data_tiket[$row['user_id']]['TotalPenumpangM'];
  $temp_array[$idx]['TotalPenumpangK']		= $data_tiket[$row['user_id']]['TotalPenumpangK'];
  $temp_array[$idx]['TotalPenumpangKK']		= $data_tiket[$row['user_id']]['TotalPenumpangKK'];
  $temp_array[$idx]['TotalPenumpangG']		= $data_tiket[$row['user_id']]['TotalPenumpangG'];
  $temp_array[$idx]['TotalPenumpangR']		= $data_tiket[$row['user_id']]['TotalPenumpangR'];
  $temp_array[$idx]['TotalPenumpangVR']		= $data_tiket[$row['user_id']]['TotalPenumpangVR'];
  $temp_array[$idx]['TotalPenumpangV']		= $data_tiket[$row['user_id']]['TotalPenumpangV'];
  $temp_array[$idx]['TotalTiket']					= $data_tiket[$row['user_id']]['TotalTiket'];
  $temp_array[$idx]['TotalTiketTunai']	  = $data_tiket[$row['user_id']]['TotalTiketTunai'];
  $temp_array[$idx]['TotalTiketDebit']	  = $data_tiket[$row['user_id']]['TotalTiketDebit'];
  $temp_array[$idx]['TotalTiketKredit']	  = $data_tiket[$row['user_id']]['TotalTiketKredit'];
  $temp_array[$idx]['TotalDiscount']		  = $data_tiket[$row['user_id']]['TotalDiscount'];
  $temp_array[$idx]['TotalUangTiket']		  = $data_tiket[$row['user_id']]['TotalOmzet'];

  $temp_array[$idx]['TotalPaketP']	      = $data_paket[$row['user_id']]['TotalPaketP'];
  $temp_array[$idx]['TotalPaketGD']	      = $data_paket[$row['user_id']]['TotalPaketGD'];
  $temp_array[$idx]['TotalPaketGA']	      = $data_paket[$row['user_id']]['TotalPaketGA'];
  $temp_array[$idx]['TotalPaketS']	      = $data_paket[$row['user_id']]['TotalPaketS'];
  $temp_array[$idx]['TotalPaketCA']	      = $data_paket[$row['user_id']]['TotalPaketCA'];
  $temp_array[$idx]['TotalPaketCD']	      = $data_paket[$row['user_id']]['TotalPaketCD'];
  $temp_array[$idx]['TotalPaketI']	      = $data_paket[$row['user_id']]['TotalPaketI'];
  $temp_array[$idx]['TotalPaket']	        = $data_paket[$row['user_id']]['TotalPaket'];
  $temp_array[$idx]['PaketOmzetTunai']		= $data_paket[$row['user_id']]['OmzetPaketTunai'];
  $temp_array[$idx]['PaketOmzetLangganan']= $data_paket[$row['user_id']]['OmzetPaketLangganan'];
  $temp_array[$idx]['PaketTotalDiskon']		= $data_paket[$row['user_id']]['OmzDiskon'];
  $temp_array[$idx]['TotalUangPaket']			= $data_paket[$row['user_id']]['TotalPenjualanPaket'];
  $temp_array[$idx]['TotalSetor']			    = $temp_array[$idx]['TotalUangPaket']+$temp_array[$idx]['TotalUangTiket']	;

  $idx++;
}

if($order!='DESC'){
  //$temp_array = multiSortArray($temp_array, array($sort_by=>1));
  $temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
  //$temp_array = multiSortArray($temp_array, array($sort_by=>0));
  $temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Keuangan Per CSO Tanggal '.$tanggal_mulai_mysql.' s/d '.$tanggal_akhir_mysql);
$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'NO.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'NAMA');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'NRP');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('D3:D4');
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'CABANG');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->mergeCells('E3:M3');
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'PAX TIKET');
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'U');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'M');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'K');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'KK');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I4', 'G');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J4', 'R');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K4', 'VR');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L4', 'V');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M4', 'T');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->mergeCells('N3:R3');
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'UANG TIKET (Rp.)');
$objPHPExcel->getActiveSheet()->setCellValue('N4', 'TUNAI');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O4', 'DEBIT');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P4', 'KARTU KREDIT');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Q4', 'DISCOUNT');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('R4', 'TOTAL');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->mergeCells('S3:Z3');
$objPHPExcel->getActiveSheet()->setCellValue('S4', 'P');
$objPHPExcel->getActiveSheet()->getColumnDimension('s')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('T4', 'GD');
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('U4', 'GA');
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('V4', 'S');
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('W4', 'CA');
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('X4', 'CD');
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Y4', 'I');
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Z4', 'T');
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->mergeCells('AA3:AD3');
$objPHPExcel->getActiveSheet()->setCellValue('AA3', 'UANG PAKET (Rp.)');
$objPHPExcel->getActiveSheet()->setCellValue('AA4', 'UMUM');
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('AB4', 'LANGGANAN');
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('AC4', 'DISCOUNT');
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('AD4', 'TOTAL');
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->mergeCells('AE3:AE4');
$objPHPExcel->getActiveSheet()->setCellValue('AE3', 'TOTAL SETOR');
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);

$idx=0;
$idx_row=5;

//PLOT DATA
while($idx<count($temp_array)){
  $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Nama']);
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['NRP']);
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['Cabang']);
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangU'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangK'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangKK'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangG'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangVR'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangV'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($temp_array[$idx]['TotalTiket']));
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($temp_array[$idx]['TotalTiketTunai']));
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($temp_array[$idx]['TotalTiketDebit']));
  $objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($temp_array[$idx]['TotalTiketKredit']));
  $objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($temp_array[$idx]['TotalDiscount']));
  $objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($temp_array[$idx]['TotalUangTiket']));
  $objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, number_format($temp_array[$idx]['TotalPaketP'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('T'.$idx_row, number_format($temp_array[$idx]['TotalPaketGD'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('U'.$idx_row, number_format($temp_array[$idx]['TotalPaketGA'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('V'.$idx_row, number_format($temp_array[$idx]['TotalPaketS'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('W'.$idx_row, number_format($temp_array[$idx]['TotalPaketCA'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('X'.$idx_row, number_format($temp_array[$idx]['TotalPaketCD'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('Y'.$idx_row, number_format($temp_array[$idx]['TotalPaketI'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('Z'.$idx_row, number_format($temp_array[$idx]['TotalPaket'],0,",","."));
  $objPHPExcel->getActiveSheet()->setCellValue('AA'.$idx_row, number_format($temp_array[$idx]['PaketOmzetTunai']));
  $objPHPExcel->getActiveSheet()->setCellValue('AB'.$idx_row, number_format($temp_array[$idx]['PaketOmzetLangganan']));
  $objPHPExcel->getActiveSheet()->setCellValue('AC'.$idx_row, number_format($temp_array[$idx]['PaketTotalDiskon']));
  $objPHPExcel->getActiveSheet()->setCellValue('AD'.$idx_row, number_format($temp_array[$idx]['TotalUangPaket']));
  $objPHPExcel->getActiveSheet()->setCellValue('AE'.$idx_row, number_format($temp_array[$idx]['TotalSetor']));

  $idx_row++;
  $idx++;

}
/*
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':B'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, "TOTAL");
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row,"=SUM(C5:C".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row,"=SUM(D5:D".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row,"=SUM(E5:E".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row,"=SUM(F5:F".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row,"=SUM(G5:G".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row,"=SUM(H5:H".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row,"=SUM(I5:I".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row,"=SUM(J5:J".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row,"=SUM(K5:K".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row,"=SUM(L5:L".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row,"=SUM(M5:M".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row,"=SUM(N5:N".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row,"=SUM(O5:O".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row,"=SUM(P5:P".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row,"=SUM(Q5:Q".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row,"=SUM(Q5:Q".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row,"=SUM(Q5:Q".($idx_row-1).")");
*/
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

if ($idx>0){
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Keuangan per CSO Tanggal '.$tanggal_mulai_mysql.' sd '.$tanggal_akhir_mysql.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');
}
else{
  echo("TIDAK ADA DATA YANG AKAN DIEKSPOR!");
}
?>
