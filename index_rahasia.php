<?php

//redirect ke server baru
//header('location:http://202.78.200.77');

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';

include($adp_root_path . 'common.php');
	
// SESSION 
$userdata = session_pagestart($user_ip,777);  // note .. kode 777 itu bisa diganti sama kode kamu sendiri (harus unik!!!)
init_userprefs($userdata);

if( $userdata['session_logged_in'] )
{  
  //redirect(append_sid('main.'.$phpEx),true);  // user aktif ga usa login, langsung redirect ke main
}

// HEADER
include($adp_root_path . 'includes/page_header.php');

$images_random	= rand(1,13);

// TEMPLATE
$template->set_filenames(array('body' => 'main_body.tpl')); 
$template->assign_vars(array(
	'U_LOGIN'						=>append_sid('auth.php'),
	'IMAGE_BACKGROUND'	=>"bg_img".$images_random.".gif"
	));

// PARSE
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
