<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################


class PenjadwalanKendaraan{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function PenjadwalanKendaraan(){
		$this->ID_FILE="C-PJK";
	}
	
	//BODY
	
	function tambah(
		$tgl_berangkat,$jam_berangkat,$kode_jadwal,$id_jurusan,$kode_kendaraan,$kode_driver,$status_aktif){
	  
		/*
		ID	: 002
		IS	: data jadwal belum ada dalam database
		FS	:Data jadwal baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		global $LAYOUT_KURSI_DEFAULT;
		
		$status_aktif	= ($status_aktif!='')?$status_aktif:1;
		
		//mengambil layout kursi dari kendaraan
		$sql	=
			"SELECT IF(JumlahKursi IS NOT NULL,JumlahKursi,0) AS LayoutKursi,NoPolisi FROM tbl_md_kendaraan WHERE KodeKendaraan='$kode_kendaraan'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		
		$layout_kursi	= $row['LayoutKursi'];
		$no_polisi		= $row['NoPolisi'];
		
		if($layout_kursi==''){
			$sql	=
			"SELECT IF(JumlahKursi IS NOT NULL,JumlahKursi,$LAYOUT_KURSI_DEFAULT) FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal'";
		
			if (!$result = $db->sql_query($sql)){
				die_error("Err".__LINE__);
			}
			
			$row = $db->sql_fetchrow($result);
			$layout_kursi	=	$row[0];
		}
		
		$sql	=
			"SELECT Nama FROM tbl_md_sopir WHERE KodeSopir='$kode_driver'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		$nama_driver	= $row['Nama'];

		//CEK SUB JADWAL
		$sql 	=
			"SELECT * FROM tbl_md_jadwal WHERE KodeJadwalUtama ='$kode_jadwal'";

		if (!$result = $db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}

		$row = $db->sql_fetchrow($result);

		$sub_jadwal = $row['KodeJadwal'];

		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_penjadwalan_kendaraan
				(TglBerangkat,JamBerangkat,
				KodeJadwal,IdJurusan,
				KodeKendaraan,NoPolisi,LayoutKursi,
				KodeDriver,NamaDriver,StatusAktif)
			VALUES(
				'$tgl_berangkat','$jam_berangkat',
				'$kode_jadwal',$id_jurusan,
				'$kode_kendaraan','$no_polisi',$layout_kursi,
				'$kode_driver','$nama_driver',$status_aktif);";
								
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}

		if($sub_jadwal != ""){
			//MENAMBAHKAN DATA KEDALAM DATABASE
			$sql =
				"INSERT INTO tbl_penjadwalan_kendaraan
				(TglBerangkat,JamBerangkat,
				KodeJadwal,IdJurusan,
				KodeKendaraan,NoPolisi,LayoutKursi,
				KodeDriver,NamaDriver,StatusAktif)
			VALUES(
				'$tgl_berangkat','$jam_berangkat',
				'$sub_jadwal',$id_jurusan,
				'$kode_kendaraan','$no_polisi',$layout_kursi,
				'$kode_driver','$nama_driver',$status_aktif);";

			if (!$db->sql_query($sql)){
				die_error("Err:".__LINE__);
			}
		}
		
		//MENCATAT PELAKU DARI PENGUBAHAN PENJADWALAN
		if($status_aktif==0){
			$id	= $db->sql_nextid();
			$this->mencatatPengubahData($id,1-$status_aktif);
		}
		
		return true;
	}
	
	function ubah(
		$id,$kode_kendaraan,$kode_driver){
	  
		/*
		ID	: 004
		IS	: data jadwal sudah ada dalam database
		FS	:Data jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql	=
			"SELECT IF(JumlahKursi IS NOT NULL,JumlahKursi,0) AS LayoutKursi,NoPolisi FROM tbl_md_kendaraan WHERE KodeKendaraan='$kode_kendaraan'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		
		$field_no_polisi	=($kode_kendaraan=="")?"":"KodeKendaraan='$kode_kendaraan',NoPolisi='$row[NoPolisi]',LayoutKursi=$row[LayoutKursi],";
		
		$sql	=
			"SELECT Nama FROM tbl_md_sopir WHERE KodeSopir='$kode_driver'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		
		$field_kode_sopir	=($kode_driver=="")?"":"KodeDriver='$kode_driver',NamaDriver='$row[Nama]',";
		
		//MENGUBAH DATA KEDALAM DATABASE
		$field_update	=$field_no_polisi.$field_kode_sopir."StatusAktif=1";
		
		$sql =
			"UPDATE tbl_penjadwalan_kendaraan 
					SET $field_update
					WHERE IdPenjadwalan=$id";
								
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}
		
		return true;
	}
	
	function ubahStatus($id){
	  
		/*
		ID	: 006
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//MENGAMBIL DATA PENJADWALAN
		$sql = 
			"SELECT KodeJadwal,TglBerangkat,StatusAktif,KodeKendaraan
			FROM tbl_penjadwalan_kendaraan
			WHERE IdPenjadwalan='$id'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$data_jadwal=$db->sql_fetchrow($result);
		
		//MEMERIKSA APAKAH JADWAL YANG AKAN DIUBAH MASIH ADA PENUMPANG YANG BELUM DIPINDAHKAN
		if($data_jadwal['StatusAktif']){
			$sql = 
				"SELECT COUNT(1)
				FROM tbl_reservasi
				WHERE KodeJadwal='$data_jadwal[KodeJadwal]'
					AND TglBerangkat='$data_jadwal[TglBerangkat]'
					AND (FlagBatal!=1 OR FlagBatal IS NULL)";
					
			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
			
			$data_penumpang=$db->sql_fetchrow($result);
			
			if($data_penumpang[0]>0){
				//MASIH ADA PENUMPANG DI JADWAL YANG AKAN DINONAKTIFKAN
				return false;
			}
		}

    $query_update_layout  = "";

    if($data_jadwal['KodeKendaraan']==""){
      //JIKA BELUM PERNAH DIPILIH KODE KENDARAAN, LAYOUT KURSI MENGAMBIL DARI MASTER JADWAL
      $sql =
        "SELECT JumlahKursi
				FROM tbl_md_jadwal
				WHERE KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal('$data_jadwal[KodeJadwal]')";

      if (!$result = $db->sql_query($sql)){
        die_error("Err: $this->ID_FILE".__LINE__);
      }

      $data_master=$db->sql_fetchrow($result);
      $query_update_layout  = ",LayoutKursi='$data_master[JumlahKursi]' ";
    }
		
		//MENGUBAH STATUS PENJADWALAN
		$sql =
      "UPDATE tbl_penjadwalan_kendaraan SET
         StatusAktif=1-StatusAktif $query_update_layout
       WHERE IdPenjadwalan='$id';";
		
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}
		
		//MENCATAT PELAKU DARI PENGUBAHAN PENJADWALAN
		$this->mencatatPengubahData($id,$data_jadwal['StatusAktif']);
		
		return true;
	}//end ubahStatus
	
	private function mencatatPengubahData($id,$is_aktif){
		global $db;
		global $userdata;
		
		//MENCATAT PELAKU DARI PENGUBAHAN PENJADWALAN
		$status_aktif	= $is_aktif==1?"Menonaktifkan":"Mengaktifkan";
		$sql ="UPDATE tbl_penjadwalan_kendaraan SET Remark=CONCAT(IF(Remark IS NOT NULL,CONCAT(Remark,'<br><br>'),''),'User: ".$userdata['nama']." ".$status_aktif." pada ".date("d-m-y H:i")."') WHERE IdPenjadwalan=$id;";

		
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}
	}
	
	function ambilDataDetail($tgl,$kode_jadwal,$kode_jadwal_utama){
		
		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT KodeKendaraan,NoPolisi,LayoutKursi,f_sopir_get_nama_by_id(KodeDriver) AS NamaSopir,KodeDriver,StatusAktif
			FROM tbl_penjadwalan_kendaraan
			WHERE ((KodeJadwal LIKE '$kode_jadwal' OR KodeJadwal LIKE '$kode_jadwal_utama') AND TglBerangkat='$tgl')";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData

  function ambilDataDetail2($tgl,$kode_jadwal){

    /*
    Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
    */

    //kamus
    global $db;

    $sql =
      "SELECT KodeKendaraan,NoPolisi,LayoutKursi,f_sopir_get_nama_by_id(KodeDriver) AS NamaSopir,KodeDriver,StatusAktif
			FROM tbl_penjadwalan_kendaraan
			WHERE KodeJadwal LIKE '$kode_jadwal' AND TglBerangkat='$tgl'";

    if ($result = $db->sql_query($sql)){
      $row=$db->sql_fetchrow($result);
      return $row;
    }
    else{
      die_error("Err: $this->ID_FILE".__LINE__);
    }

  }//  END ambilData
	
}
?>