<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN,$LEVEL_SUPERVISOR)))
{
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  		= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$username		= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_uang_cabang/laporan_uang_cabang_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!="")
{
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else
{
	$cari=$HTTP_GET_VARS["cari"];
}

$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai			= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir			= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

/*if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND KodeCabangAsal ='$userdata[KodeCabang]'";	
}	*/		

$kondisi_cari	.= $kondisi_cabang;
		
$sql=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari
	ORDER BY Kota,Nama";

if (!$result_laporan = $db->sql_query($sql))
{
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		KodeCabangAsal AS KodeCabang,
		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalTunaiTiket,

		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalTunaiPaket,

		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalDebitTiket,
		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalDebitPaket,
		

		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalKreditTiket,
		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalKreditPaket,

		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,

		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),tr.HargaTiket)),0) AS TotalSetor, 
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	
	FROM $tbl_reservasi tr LEFT JOIN tbl_md_jurusan tmj ON tr.IdJurusan=tmj.IdJurusan
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2 AND PetugasCetakTiket!=0
	GROUP BY KodeCabangAsal ORDER BY KodeCabangAsal "; 
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result))
{
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']			= $row['KodeCabang'];
	$temp_array[$idx]['Nama']				= $row['Nama'];
	$temp_array[$idx]['Alamat']				= $row['Alamat'];
	$temp_array[$idx]['Kota']				= $row['Kota'];
	$temp_array[$idx]['Telp']				= $row['Telp'];
	$temp_array[$idx]['Fax']				= $row['Fax'];

	$temp_array[$idx]['TotalTunaiTiket']	= $data_tiket_total[$row['KodeCabang']]['TotalTunaiTiket'];
	$temp_array[$idx]['TotalTunaiPaket']	= $data_tiket_total[$row['KodeCabang']]['TotalTunaiPaket'];

	$temp_array[$idx]['TotalDebitTiket']	= $data_tiket_total[$row['KodeCabang']]['TotalDebitTiket'];
	$temp_array[$idx]['TotalDebitPaket']	= $data_tiket_total[$row['KodeCabang']]['TotalDebitPaket'];

	$temp_array[$idx]['TotalKreditTiket']	= $data_tiket_total[$row['KodeCabang']]['TotalKreditTiket'];
	$temp_array[$idx]['TotalKreditPaket']	= $data_tiket_total[$row['KodeCabang']]['TotalKreditPaket'];
	
	$temp_array[$idx]['TotalTiket']			= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalSetor']			= $temp_array[$idx]['TotalTunai']+$temp_array[$idx]['TotalDebit']+$temp_array[$idx]['TotalKredit'];
	$idx++;
}

$idx=0;
$kota_terakhir			= "";
$gt_tunai_tiket			= 0;
$gt_tunai_paket			= 0;

$gt_debit_tiket			= 0;
$gt_debit_paket			= 0;

$gt_kredit_tiket		= 0;
$gt_kredit_paket		= 0;

$gt_setor				= 0;
$gt_pnp					= 0;

//PLOT DATA
while($idx<=count($temp_array))
{
	
	if($kota_terakhir!=$temp_array[$idx]['Kota'] || $idx==count($temp_array))
	{
		$nama_kota								= "<tr><th colspan='11' style='font-size:14px;'><b>".$temp_array[$idx]['Kota']."</b></th></tr>";
		$kota_terakhir						= $temp_array[$idx]['Kota'];
		$idx_kota									= 1;

		if($idx>0)
		{
		
			$total_per_kota		= 
				"<tr style='background:green;color:white;'>
					<td colspan='2' align='center'><b>Sub Total</b></td>
					<td align='right'>".number_format($total_pnp_t_perkota,0,",",".")."</td>

					<td align='right'>".number_format($total_tunaitiket_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_tunaipaket_perkota,0,",",".")."</td>
					
					<td align='right'>".number_format($total_debittiket_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_debitpaket_perkota,0,",",".")."</td>

					<td align='right'>".number_format($total_kredittiket_perkota,0,",",".")."</td>
					<td align='right'>".number_format($total_kreditpaket_perkota,0,",",".")."</td>

					<td align='right'>".number_format($total_setor_perkota,0,",",".")."</td>
					<td align='right'>&nbsp;</td>
				</tr>";
			
		}
		$total_tunaitiket_perkota	= $temp_array[$idx]['TotalTunaiTiket'];
		$total_tunaipaket_perkota	= $temp_array[$idx]['TotalTunaiPaket'];

		$total_debitTiket_perkota	= $temp_array[$idx]['TotalDebitTiket'];
		$total_debitPaket_perkota	= $temp_array[$idx]['TotalDebitPaket'];
		
		$total_kredittiket_perkota	= $temp_array[$idx]['TotalKreditTiket'];
		$total_kreditpaket_perkota	= $temp_array[$idx]['TotalKreditPaket'];
		
		$total_pnp_t_perkota		= $temp_array[$idx]['TotalTiket'];
		$total_setor_perkota		= $temp_array[$idx]['TotalSetor'];

		
	}
	else
	{
		$nama_kota					= "";
		$idx_kota++;
		$total_tunaitiket_perkota		+= $temp_array[$idx]['TotalTunaiTiket'];
		$total_tunaipaket_perkota		+= $temp_array[$idx]['TotalTunaiPaket'];

		$total_debittiket_perkota		+= $temp_array[$idx]['TotalDebitTiket'];
		$total_debitpaket_perkota		+= $temp_array[$idx]['TotalDebitPaket'];
		
		$total_kredittiket_perkota		+= $temp_array[$idx]['TotalKreditTiket'];
		$total_kreditpaket_perkota		+= $temp_array[$idx]['TotalKreditPaket'];
		
		$total_pnp_t_perkota			+= $temp_array[$idx]['TotalTiket'];
		$total_setor_perkota			+= $temp_array[$idx]['TotalSetor'];
		$total_per_kota					= "";
	}


	$odd ='odd';
	
	if (($idx % 2)==0)
	{
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_uang_cabang_detail_cso.php?tglmulai='.$tanggal_mulai.'&tglakhir='.$tanggal_akhir.'&kode='.$temp_array[$idx]['KodeCabang'].'&namacabang='.$temp_array[$idx]['Nama'].'&is_today='.$is_today)."\");return false'>CSO<a/>&nbsp;+&nbsp;";
	$act 	.="<a href='#' onClick='Start(\"".append_sid('laporan_uang_cabang_detail.php?tglmulai='.$tanggal_mulai.'&tglakhir='.$tanggal_akhir.'&kode='.$temp_array[$idx]['KodeCabang'].'&is_today='.$is_today)."\");return false'>Detail<a/>";
	
	//total tiket
	$total_penjualan_tiket			= $temp_array[$idx]['TotalSetor'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket					= $temp_array[$idx]['TotalTiket'];	

	if($idx<count($temp_array))
	{
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'						=>$odd,
					'no'						=>$idx_kota,
					'nama_kota'					=>$nama_kota,
					'kode_cabang'				=>$temp_array[$idx]['KodeCabang'],
					'cabang'					=>$temp_array[$idx]['Nama'],
					'alamat'					=>$temp_array[$idx]['Alamat'],
					
					'total_tunai_tiket'			=>number_format($temp_array[$idx]['TotalTunaiTiket'],0,",","."),
					'total_tunai_paket'			=>number_format($temp_array[$idx]['TotalTunaiPaket'],0,",","."),

					'total_debit_tiket'			=>number_format($temp_array[$idx]['TotalDebitTiket'],0,",","."),
					'total_debit_paket'			=>number_format($temp_array[$idx]['TotalDebitPaket'],0,",","."),
					
					'total_kredit_tiket'		=>number_format($temp_array[$idx]['TotalKreditTiket'],0,",","."),
					'total_kredit_paket'		=>number_format($temp_array[$idx]['TotalKreditPaket'],0,",","."),
					
					'total_penumpang'			=>number_format($total_tiket,0,",","."),
					'total_setoran'				=>number_format($total_penjualan_tiket,0,",","."),
					'act'						=>$act,
					'total_per_kota'			=>$total_per_kota,
				)
			);
	}
	else
	{
		$template->
			assign_block_vars(
				'ROW',
				array(
					'total_per_kota'		=>$total_per_kota
				)
			);
	}

	$gt_tunai_tiket		+= $temp_array[$idx]['TotalTunaiTiket'];
	$gt_tunai_paket		+= $temp_array[$idx]['TotalTunaiPaket'];

	$gt_debit_tiket		+= $temp_array[$idx]['TotalDebitTiket'];
	$gt_debit_paket		+= $temp_array[$idx]['TotalDebitPaket'];

	$gt_kredit_tiket	+= $temp_array[$idx]['TotalKreditTiket'];
	$gt_kredit_paket	+= $temp_array[$idx]['TotalKreditPaket'];
	
	$gt_pnp				+= $total_tiket;
	$gt_setor			+= $total_penjualan_tiket;
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&is_today=$is_today&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$temp_array[$idx]['KodeCabang'].
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
											
$script_cetak_excel="Start('laporan_uang_cabang_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "is_today".($is_today==""?"1":$is_today);
$$temp_var	= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_keuangan">Home</a> | <a href="'.append_sid('laporan_uang_cabang.'.$phpEx).'">Laporan Setoran Cabang</a>',
	'ACTION_CARI'		=> append_sid('laporan_uang_cabang.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'IS_TODAY1'			=> $is_today1,
	'IS_TODAY0'			=> $is_today0,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'				=> $userdata['Nama'],
	'SUMMARY'			=> $summary,
	'PAGING'			=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'GT_PENUMPANG'		=> number_format($gt_pnp,0,",","."),
	'GT_TUNAI_TIKET'	=> number_format($gt_tunai_tiket,0,",","."),
	'GT_TUNAI_PAKET'	=> number_format($gt_tunai_paket,0,",","."),


	'GT_DEBIT_TIKET'   	=> number_format($gt_debit_tiket,0,",","."),
	'GT_DEBIT_PAKET'   	=> number_format($gt_debit_paket,0,",","."),

	'GT_KREDIT_TIKET'   => number_format($gt_kredit_tiket,0,",","."),		
	'GT_KREDIT_PAKET'   => number_format($gt_kredit_paket,0,",","."),		
	

	'GT_SETORAN'    	=> number_format($gt_pnp_kk,0,",",".")
	)
);

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>