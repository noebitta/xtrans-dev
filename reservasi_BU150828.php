<?php
//
// RESERVASI
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_CSO,$LEVEL_CSO_PAKET))){  
  if(!isset($HTTP_GET_VARS['mode'])){
		redirect(append_sid('index.'.$phpEx),true); 
	}
	else{
		echo("loading...");exit;
	}
}

include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassPromo.php');
include($adp_root_path . 'ClassVoucherDiskon.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$user_id  	= $userdata['user_id']; 
$user_level	= $userdata['user_level'];

// membuat option Asal
function OptionAsal($kota){
	global $db;
	
	$Cabang	= new Cabang();
	
	$result	= $Cabang->setComboCabang($kota);
	
  $opt = "";
	
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
      $opt .= "<option value='$row[0]'>$row[1]</option>";
    }    
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
			
  return $opt;
}

// membuat option jurusan
function OptionJurusan($asal){
	
	global $db;
	
	$Jurusan	= new Jurusan();
	
	$result = $Jurusan->setComboJurusan($asal);
	
  $opt = "";
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
			$opt .= "<option value='$row[0]'>$row[1] ($row[2])</option>";
    }  
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
	
  return $opt;
}

// membuat option jam
function OptionJadwal($tgl,$id_jurusan){
  global $db;
	
  if ($id_jurusan!=''){
		
		$Jadwal	= new Jadwal();
		
		$result	= $Jadwal->setComboJadwal($tgl,$id_jurusan);
		
	  $opt = "";
		
	  if ($result){
			$opt = "<option>(none)</option>" . $opt;  
			while ($row = $db->sql_fetchrow($result)){
				$opt .= "<option value='$row[0]'>$row[0]-$row[1] $row[2]</option>";
			}    
	  } 
		else{
			$opt .="<option selected=selected>Error</option>";
	  }
		
		/*if ($result){
			while ($row = $db->sql_fetchrow($result)){
				$opt .= "<a href='#' id='$row[0]' onClick=\"p_jadwal.value='$row[0]';changeRuteJam();pilihJadwal(this);return false;\">$row[1] ($row[0])</a><hr>";
			}    
			
			$opt	= substr($opt,0,-4);
	  } 
		else{
			$opt .="Gagal mengambil data";
	  }*/
  }  
  
  return $opt;
}

function setListDiscount($kode_jadwal,$tgl_berangkat,$kode_discount=NULL){
		
		global $db;
		
		$sql =
			"SELECT FlagLuarKota,f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','$tgl_berangkat') AS HargaTiket,HargaTiket AS HargaTiketNormal,
				f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS CabangAsal,f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) AS CabangTujuan
      FROM  tbl_md_jurusan
			WHERE IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal')";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota			= $row['FlagLuarKota'];
		$harga_tiket				= $row['HargaTiket'];
		$harga_tiket_normal	= $row['HargaTiketNormal'];
		
		//$kondisi_promo_return = $kode_discount!=""?"":" OR '$row[CabangAsal]'=CabangPromo OR '$row[CabangTujuan]'=CabangPromo";
		
    $sql = "SELECT IdDiscount,NamaDiscount,JumlahDiscount,KodeDiscount
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota' AND (CabangPromo IS NULL OR '$row[CabangAsal]'=CabangPromo OR '$row[CabangTujuan]'=CabangPromo)
            ORDER BY NamaDiscount ASC";
	  
		$opt = "<option value=''>UMUM Rp. ".number_format($harga_tiket,0,",",".")."</option>";
		
	  if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				
				if($row['JumlahDiscount']>1){
					$besar_discount	= $row['JumlahDiscount'];
				}
				else{
					$besar_discount	= $harga_tiket*$row['JumlahDiscount'];
				}
				
				if($row['KodeDiscount']=="G"){
					$keterangan	= "GRATIS";
				}
				elseif($row['KodeDiscount']=="KK"){
					$keterangan	= " Rp. ".number_format($harga_tiket-$besar_discount,0,",",".");
				}
				elseif($row['KodeDiscount']=="M"){
					$keterangan	= " Rp. ".number_format($harga_tiket-$besar_discount,0,",",".");
				}
				elseif($row['KodeDiscount']=="V"){
					$keterangan	= " Rp. ".number_format($harga_tiket-$besar_discount,0,",",".");
				}
				elseif($row['KodeDiscount']=="R"){
					$keterangan	= " Rp. ".number_format($harga_tiket+$harga_tiket_normal-$besar_discount,0,",",".");
				}
				
				$selected	= ($kode_discount!=$row['KodeDiscount'])?"":"selected";
				
	      $opt .= "<option value=$row[IdDiscount] $selected>$row[NamaDiscount].$keterangan</option>";
	    } 
	  } 
		else{ 
			return "<option>Error</option>";
		}
		
		return $opt;
}

//PEMILIHAN PROSES
switch ($mode){
	
//HARGA TIKET ===========================================================================================================================================
case "hargatiket":
  $rute = $HTTP_GET_VARS['rute'];
  //$harga_tiket = getHargaTiket($rute);
      
  echo $harga_tiket;
  exit;

// PERIKSA NO TELP ===========================================================================================================================================================================================
case "periksa_no_telp":
	$no_telp 			= $HTTP_GET_VARS['no_telp'];
	$flag_paket 	= $HTTP_GET_VARS['paket'];
	$flag_pengirim= $HTTP_GET_VARS['flag'];
	
	include($adp_root_path . 'ClassPelanggan.php');
	
	$Pelanggan = new Pelanggan();
	
	$data_pelanggan=$Pelanggan->ambilDataDetail($no_telp);
  
	if(count($data_pelanggan)>1){	
		if($flag_paket!=1){
			echo("
				document.getElementById('hdn_no_telp_old').value='".trim($data_pelanggan['NoTelp'])."';
				document.getElementById('fnama').value='".trim($data_pelanggan['Nama'])."';
				document.getElementById('ftelp').value='".trim($data_pelanggan['NoTelp'])."';
			");
		}
		else{
			if($flag_pengirim==1){
				echo("
					document.getElementById('dlg_paket_nama_pengirim').value='".trim($data_pelanggan['Nama'])."';
					document.getElementById('dlg_paket_alamat_pengirim').value='".trim($data_pelanggan['Alamat'])."';
					document.getElementById('dlg_paket_telp_pengirim').value='".trim($data_pelanggan['NoTelp'])."';
				");
			}
			else{
				echo("
					document.getElementById('dlg_paket_nama_penerima').value='".trim($data_pelanggan['Nama'])."';
					document.getElementById('dlg_paket_alamat_penerima').value='".trim($data_pelanggan['Alamat'])."';
					document.getElementById('dlg_paket_telp_penerima').value='".trim($data_pelanggan['NoTelp'])."';
				");
			}
		}
	}
	else{
		echo(0);
	}
	
  exit;
  
 // PERIKSA ID Member ===========================================================================================================================================================================================
case "periksa_id_member":
	$id_member = $HTTP_GET_VARS['id_member'];
	
	$Member = new Member();
	
	$data_member=$Member->ambilData($id_member);
  
	if(count($data_member)>1){	
		
		/*if('".substr($data_member['TglLahir'],-5)."'=='".substr(dateNow(),-5)."'){
				alert('Ucapkan SELAMAT ULANG TAHUN pada member ini, karena member ini sedang berulang tahun hari ini!');
			}*/
		
		echo("
			if($data_member[MasaBerlaku]>$THRESHOLD_MEMBER_EXPIRED){
				if(!document.getElementById('hdn_ubah_id_member')){
					document.getElementById('hdn_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('fnama').value='".trim($data_member['Nama'])."';
					document.getElementById('ftelp').value='".trim($data_member['Handphone'])."';
				}
				else{
					document.getElementById('hdn_ubah_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('ubah_nama_penumpang').value='".trim($data_member['Nama'])."';
					document.getElementById('ubah_telp_penumpang').value='".trim($data_member['Handphone'])."';
				}
			}
			else if($data_member[MasaBerlaku]>0 && $data_member[MasaBerlaku]<=$THRESHOLD_MEMBER_EXPIRED){
				if(!document.getElementById('hdn_ubah_id_member')){
					document.getElementById('hdn_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('fnama').value='".trim($data_member['Nama'])."';
					document.getElementById('ftelp').value='".trim($data_member['Handphone'])."';
				}
				else{
					document.getElementById('hdn_ubah_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('ubah_nama_penumpang').value='".trim($data_member['Nama'])."';
					document.getElementById('ubah_telp_penumpang').value='".trim($data_member['Handphone'])."';
				}
				
				alert('Masa berlaku keanggotan sebentar lagi akan berakhir, mohon informasikan kepada pelanggan');
			}
			else{
				
				if(!document.getElementById('hdn_ubah_id_member')){
					document.getElementById('hdn_id_member').value='';
				}
				else{
					document.getElementById('hdn_ubah_id_member').value='';
				}
				
				alert('Maaf, masa keanggotaan member yang bersangkutan sudah berakhir');
			}
			
		");
	}
	else{
		echo(0);
	}
	
  exit;
	
// PERIKSA MEMBER ===========================================================================================================================================================================================
case "periksa_member_by_kartu":
	$no_seri_kartu = $HTTP_GET_VARS['no_kartu'];
	
	$Member = new Member();
	
	$data_member=$Member->ambilDataByNoSeriKartu($no_seri_kartu);
  
	if(count($data_member)>1){	
		echo("
			document.getElementById('hdn_id_member').value='".trim($data_member['id_member'])."';
			document.getElementById('fnama').value='".trim($data_member['nama'])."';
			document.getElementById('fadd').value='".trim($data_member['alamat'])." ".trim($data_member['kota'])."';
			document.getElementById('ftelp').value='".trim($data_member['telp_rumah'])."';
		");
	}
	else{
		echo("false");
	}
	
  exit;

//Asal  ===========================================================================================================================================================================================
case "asal":
    // membuat nilai awal..
	$kota	 = $HTTP_GET_VARS['kota_asal'];
	
	$oasal = OptionAsal($kota);
	 
	echo("
	  <table bgcolor='white' width='100%'> 
		  <tr><td>POINT KEBERANGKATAN<br></td></tr>
	    <tr>
				<td><select onchange='getUpdateTujuan(this.value);' id='rute_asal' name='rute_asal'>$oasal</select><span id='progress_asal' style='display:none;'><img src='./templates/images/loading.gif' /></span>
				</td>
			</tr>
		</table>");

	exit;

//TGL ===========================================================================================================================================================================================
case "tujuan":
    // membuat nilai awal..
	$asal=$HTTP_GET_VARS['asal'];
	
	$ojur = OptionJurusan($asal);
	  
	echo("
	    <table>
				<tr><td>POINT TUJUAN<br></td></tr>
	      <tr>
					<td><select onchange='getUpdateJadwal(this.value);' id='tujuan' name='tujuan'>$ojur</select><span id='progress_tujuan' style='display:none;'><img src='./templates/images/loading.gif' /></span>
					</td>
				</tr>
			</table>");
	exit;
	
// JAM ===========================================================================================================================================================================================
case "jam":
  // memilih jam yang tersedia sesuai dengan rute yang dimasukan
  $tgl 				= $HTTP_GET_VARS['tgl'];
	$id_jurusan = $HTTP_GET_VARS['id_jurusan'];
  $ojam 			= OptionJadwal($tgl,$id_jurusan);
  
	echo("
	<table bgcolor='white' width='100%'>
		<tr><td>JAM KEBERANGKATAN<br></td></tr>
	  <tr><td><select onchange='changeRuteJam();' name='p_jadwal' id='p_jadwal'>$ojam</select><span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span></td></tr>
  </table>");  
	
	
	/*echo("
	<table bgcolor='white' width='100%'>
		<tr><td>JAM KEBERANGKATAN<br></td></tr>
	  <tr><td>$ojam</td></tr>
  </table>");*/
  	
  exit;
	 
// CEK PERUBAHAN LAYOUT
case "ceklayout":
	$Reservasi						= new Reservasi();
	
	$tgl_mysql						= $HTTP_GET_VARS['tanggal'];
	$kode_jadwal 					= $HTTP_GET_VARS['jadwal'];
	
	$layout_signature	= $Reservasi->getLayoutSignature($tgl_mysql,$kode_jadwal);
	
	echo("
		if(document.getElementById('layoutsignature')){
			if('".$layout_signature."'!= document.getElementById('layoutsignature').value){
				getUpdateMobil();
			}
		}
	");
exit;

// MOBIL ===========================================================================================================================================================================================
case "mobil":
	//SOURCE BARU*******************************************************************************************
	// memilih mobil yang tersedia sesuai dengan tanggal, rute dan jam yang dimasukan
	//echo(date("H:i:s")); //speed test, ambil waktu awal
	
	if($userdata['user_level']==$LEVEL_CSO_PAKET){
		exit;
	}
	
	include($adp_root_path . 'ClassLayoutKendaraan.php');
  include($adp_root_path . 'ClassPenjadwalanKendaraan.php');
	
	//VARIABLE UNTUK CASE MOBIL
	$Jadwal								= new Jadwal();
	$PenjadwalanKendaraan	= new PenjadwalanKendaraan();
	$Reservasi						= new Reservasi();
	
	$LayoutKendaraan			= new LayoutKendaraan();
	$tgl_mysql						= $HTTP_GET_VARS['tanggal'];
  $tgl  								= FormatMySQLDateToTgl($tgl_mysql); // tanggal    
  $kode_jadwal 					= $HTTP_GET_VARS['jadwal'];    // jam
	$kode_booking_dipilih	= $HTTP_GET_VARS['kode_booking_dipilih'];    // jam
	
	if($kode_jadwal!="" && $kode_jadwal!="(none)"){
	
		//mengambil  data jadwal
			
		$row = $Jadwal->ambilDataDetail($kode_jadwal);
	  $jam_berangkat	= $row['JamBerangkat'];
	  $layout_kursi 	= $row['JumlahKursi'];
		$flag_sub_jadwal= $row['FlagSubJadwal'];
		$is_express			= $row['IsExpress'];
		$kode_jadwal_utama= ($flag_sub_jadwal==1)? $row['KodeJadwalUtama']:$kode_jadwal;
    $flag_aktif     = $row['FlagAktif'];

		//MEMERIKSA HAK AKSES, JIKA CSO BIASA TIDAK BOLEH MEMESAN PADA WAKTU YANG SUDAH LALU
		if(!$Reservasi->periksaHakAkses($tgl_mysql,$jam_berangkat) && $user_level>=$LEVEL_CSO){
			echo("<br><br><br><br><br><br>
				<img src='./templates/images/icon_warning.png' />
				<font color='red'><h3>Anda tidak boleh memilih waktu yang sudah lalu!</h3></font>");
			exit;
		}
		//--END PERIKSA HAK AKSES


		//MEMERIKSA AKTIF TIDAKNYA JADWAL utama
		if($flag_sub_jadwal){

      $penjadwalan_ini = $PenjadwalanKendaraan->ambilDataDetail2($tgl_mysql,$kode_jadwal);
      $flag_aktif =  $penjadwalan_ini['StatusAktif']==''?$flag_aktif:($penjadwalan_ini['StatusAktif']?true:false);

      $data_jadwal_utama	= $Jadwal->ambilDataDetail($kode_jadwal_utama);
	    $flag_aktif					= $data_jadwal_utama['FlagAktif']?$flag_aktif:false;
    }

		/*if(!$flag_aktif || $flag_aktif==''){
			echo("<br><br><br><br><br><br>
				<img src='./templates/images/icon_warning.png' />
				<font color='red'><h3>(1) Jadwal ini tidak dioperasikan!</h3></font>");
			exit;
		}*/

    $row						= $PenjadwalanKendaraan->ambilDataDetail2($tgl_mysql,$kode_jadwal_utama);
    $kode_kendaraan	= $row['KodeKendaraan'];
    $no_polisi 			= $row['NoPolisi'];
    $kode_sopir 		= $row['KodeDriver'];
    $nama_sopir 		= $row['NamaSopir'];
    $status_aktif 	= $row['StatusAktif'];
    $layout_kursi 	= ($row['LayoutKursi']=='')?$layout_kursi:$row['LayoutKursi'];
    $flag_aktif     = $row['StatusAktif']==''?$flag_aktif:($row['StatusAktif']?true:false);

		//mengambil harga tiket
		$harga_tiket=$Reservasi->getHargaTiket($kode_jadwal,$tgl_mysql);

		
		//MEMERIKSA AKTIF TIDAKNYA JADWAL
		
		//if(($status_aktif==0 && $status_aktif!='') || ($flag_aktif==0 && $status_aktif=='')){
		if(!$flag_aktif){
			if($userdata['user_level']!=$LEVEL_ADMIN){
				echo("<br><br><br><br><br><br>
					<img src='./templates/images/icon_warning.png' />
					<font color='red'><h3>Jadwal ini tidak dioperasikan!</h3></font>");
				exit;
			}
			else{
				echo("<img src='./templates/images/icon_warning.png' />
					<font color='red' style='text-decoration:blink;'><h3>Jadwal ini tidak dioperasikan!</h3></font>");
			}
		}
		
		//LAYOUT KENDARAAN
		
		//memeriksa flag sub jadwal, jika jadwal ini merupakan subjadwal dari jadwal yang lain , maka layoutnya akan mengambil jadwal induknya
		$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
		
		
		//mengambil header dari layout kursi
		$row				= $Reservasi->ambilDataHeaderLayout($tgl_mysql,$kode_jadwal_aktual);
		$id 				= $row['ID'];
		$no_spj			= $row['NoSPJ'];
		$waktu_spj	= $row['TglCetakSPJ'];
		
		/*
		//ambil memo
		$flag_memo				= $row['FlagMemo'];
		$memo							= $row['Memo'];
		$pembuat_memo			= $row['PembuatMemo'];
		$waktu_buat_memo	= $row['WaktuBuatMemo'];
		
		//$ada_memo_baru	= ($flag_memo==0)?"":"<blink><font size=2 color='red'><b><-Ada memo baru</b></font></blink>";
		
		$isi_memo="MEMO: \\n";
		
		include($adp_root_path . 'ClassUser.php');
		$User = new User();
		
		$pembuat_memo			= $User->ambilNamaById($pembuat_memo);
		$waktu_buat_memo	= dateparse(FormatMySQLDateToTglWithTime($waktu_buat_memo));
		$isi_memo				 .= "From :$pembuat_memo@$waktu_buat_memo\\n $memo \\n";
		
		if($flag_memo==0){
			$show_memo ="";
		}
		else{
			$show_memo ="&nbsp;&nbsp;<a href='javascript:onClick=alert(\"$isi_memo\")'><blink>Lihat Memo</blink></a>&nbsp;";
		}*/
		
		if($no_spj!=""){
			$kode_kendaraan= $row['KodeKendaraan'];
			$no_polisi		= $row['NoPolisi'];
			$kode_sopir 	= $row['KodeSopir'];
			$nama_sopir 	= $row['NamaSopir'];
			//$waktu_spj		= "";
		}
		
		if(is_null($id)){
			//jika belum ada transaksi untuk tanggal ini pada keberangkatan jam ini maka akan dibuat recordnya
			
			//MENAMBAHKAN LAYOUT KE TABEL POSISI
			
			if (!$result = $Reservasi->tambahPosisi(
				$kode_jadwal_aktual, $tgl_mysql, $jam_berangkat,
				$layout_kursi, $kode_kendaraan, $kode_sopir)){
				
				echo("Error ".__LINE__);
				exit;
			} 
		}
		$LayoutKendaraan->kode_booking_dipilih=$kode_booking_dipilih;
		//$error=$LayoutKendaraan->kode_booking_dipilih;
		//<input type='button' value='Cetak banyak tiket' onclick='PilihTiket();'></input>
		
		//LAYOUT MOBIL
		echo("
			<input type='hidden' value='$is_express' id='isexpress' />
			<input type='hidden' value='$jam_berangkat' id='jam_berangkat_aktif' />
			<table cellspacing='0' cellpadding='0' width='400'>
				<tbody>
				<tr>
					<td align='center'>
						<input type='hidden' name='layoutsignature' id='layoutsignature' value='".$Reservasi->getLayoutSignature($tgl_mysql,$kode_jadwal)."' />
						<table width='100%'>
							<tr>
								<td width='25%'><strong>No SPJ</strong></td>
								<td>:</td>
								<td>
									$no_spj
									<input type='hidden' readonly='yes' value='$no_spj' name='txt_spj' id='txt_spj' />
								</td>
							</tr>
							<tr>
								<td width='25%'><strong>Manifest</strong></td>
								<td>:</td>
								<td>
									".dateparse(FormatMySQLDateToTglWithTime($waktu_spj))."
									<input type='hidden' readonly='yes' value='$no_spj' name='txt_spj' id='txt_spj' />
								</td>
							</tr>
							<tr>
								<td><strong>Plat</strong></td>
								<td>:</td>
								<td>
									$no_polisi (Body: $kode_kendaraan)
									<input type='hidden' readonly='yes' value='$no_polisi' name='plat' id='plat' />
								</td>
							</tr>
							<tr>
								<td><strong>Harga Tiket</strong></td>
								<td>:</td>
								<td><strong>Rp. ".number_format($harga_tiket,0,",",".")."</strong></td>
							</tr>
							<tr>
								<td colspan='3' align='right'>
									<input type='button' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refresh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' onclick='getUpdateMobil();getPengumuman();setSisaKursi()' />
									<input type='hidden' value='$kode_kendaraan' name='hide_mobil_sekarang' id='hide_mobil_sekarang'></input>
									<input type='hidden' value='$layout_kursi' name='hide_layout_kursi' id='hide_layout_kursi'></input>
									<input type='hidden' name='hide_nama_sopir_sekarang' id='hide_nama_sopir_sekarang' value='$kode_sopir'>
								</td>
							</tr>
						</table>
					</td>	
				</tr>
				<tr>
					<td colspan='3' valign='middle' align='center' height='20' bgcolor='d0d0d0'>
						<table width='100%' cellspacing=0 cellpadding=0>
							<tr>
								<td width='85%'>$show_memo</td>
								<td width='15%' valign='middle' align='right'>
									<span id='progress_kursi' style='display:none;'><img src='./templates/images/loading.gif' /></span>&nbsp;
								</td>
							<tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='center'>".
						$LayoutKendaraan->setLayoutKursi($tgl_mysql,$kode_jadwal_aktual,$kode_jadwal,$layout_kursi,$nama_sopir)
					."</td>
				</tr>
				<tr>
					<td width='231'>$error</td>
				</tr>
				</tbody>
			</table>
		");
  }
	else{
		echo("<br><br><br><br><br><br><img src='./templates/images/icon_warning.png' /><font color='red'><h3>Anda belum memilih jadwal keberangkatan!</h3></font>");
	}
	//echo("<br>".date("H:i:s")); //speed test, ambil waktu awal
	exit;

// PENGUMUMAN LAYOUT===========================================================================================================================================================================================
case "pengumuman":
  // menampilkan pengumuman
	include_once($adp_root_path . 'ClassPengumuman.php');
	
	$Pengumuman	= new Pengumuman();
	
  if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
		//HANYA BISA DIAKSES OLEH SELAIN AGEN
		
	  $jumlah_pengumuman	= $Pengumuman->ambilJumlahPengumumanBaru($userdata['user_id']);
			
		if($jumlah_pengumuman>0){
			$jumlah_pengumuman	= "<blink>".$jumlah_pengumuman."</blink>";
			$blink_Pengumuman		= "<blink>Pesan</blink>";
		}
		else{
			$jumlah_pengumuman	= "";
			$blink_Pengumuman		= "Pesan";
		}
		
		//$isi_pengumuman
		
		$baca_pengumuman	= "Start('".append_sid('pengumuman_inbox.'.$phpEx)."');getPengumuman();return false;";
		
		echo("
			<a class='menu' href='#' onClick=$baca_pengumuman>&nbsp;&nbsp;&nbsp;&nbsp;<font color='red' size=5><b>$jumlah_pengumuman</b></font>&nbsp;&nbsp;&nbsp;&nbsp;
			<br><br><br>
			$blink_Pengumuman</a>
		");

	}
		
  exit;
break;

// SHOW CHAIR ===========================================================================================================================================================================================
case "showchair":
  // mendapatkan nilai kursi yang tersedia dan value nilai lain sesuai dengan tanggal, rute, jam dan kode
	
	  $kursi  		= trim($HTTP_GET_VARS['kursi']);
		$no_tiket		= trim($HTTP_GET_VARS['no_tiket']);
	  $no_spj			= trim($HTTP_GET_VARS['no_spj']);
		
		$Reservasi		= new Reservasi();
		$User					= new User();
		$Asuransi = new Asuransi();
		
		$data_kursi = $Reservasi->ambilDataKursi($no_tiket);
		
		if(count($data_kursi)==0){
			echo("
				<table border='0' class='border' width='100%' height='500'> 
					<tr>
						<td width='40%' colspan='3'><h3>Data tidak ditemukan</h3></td>
					</tr>
				</table>
			");
			exit;
		}
				
		$kode_jadwal	=$data_kursi['KodeJadwal'];
		$tgl_berangkat=$data_kursi['TglBerangkat'];
		$harga_tiket	=number_format($data_kursi['HargaTiket'],0,",",".");
		$discount			=number_format($data_kursi['Discount'],0,",",".");
		$sub_total_pertiket=number_format($data_kursi['SubTotal'],0,",",".");
		$total				=$data_kursi['Total'];
		$nama_cso			=$User->ambilNamaById($data_kursi['PetugasPenjual']);
		$cetak_tiket	=$data_kursi['CetakTiket'];
		$id_member		=trim($data_kursi['IdMember']);
		$kode_booking	=$data_kursi['KodeBooking'];
		$jenis_penumpang	= $data_kursi['JenisPenumpang'];
		$jenis_pembayaran	= $data_kursi['JenisPembayaran'];
		$no_spj				= $data_kursi['NoSPJ'];
		//Mengambil data total bayar
		$data_total = $Reservasi->ambilTotalPesananByKodeBooking($kode_booking);
				
		$sub_total			=($data_total['SubTotal']!='')?$data_total['SubTotal']:0;
		$jumlah_kursi		=($data_total['JumlahKursi']!='')?$data_total['JumlahKursi']:0;
		$total_discount	=($data_total['TotalDiscount']!='')?$data_total['TotalDiscount']:0;
		$total_bayar		=($data_total['TotalBayar']!='')?$data_total['TotalBayar']:0;
		$total_asuransi	= 0;
		
		if($jenis_pembayaran==4){
			$total							= 0;
			$discount						= 0;
			$sub_total_pertiket	= 0;
			$sub_total					= 0;
			$total_discount			= 0;
			$total_bayar				= 0;
		}
		
		$sub_total			= number_format($sub_total,0,",",".");
		$jumlah_kursi		= number_format($jumlah_kursi,0,",",".");
		$total_discount	= number_format($total_discount,0,",",".");
					
		if($cetak_tiket==0){
			//BELUM CETAK TIKET
			$status_pesanan="Booking";
			$waktu_cetak_tiket="";
		}
		else{
			//SUDAH CETAK TIKET
			$status_pesanan="Tiket telah dicetak";
			$waktu_cetak_tiket = 
				"<tr>
					<td>Waktu Cetak</td><td>:</td>
					<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuCetakTiket']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Dicetak oleh</td><td>:</td>
					<td><strong>".$User->ambilNamaById($data_kursi['PetugasCetakTiket'])."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</td>
				</tr>";
		}
		
		if($data_kursi['Pemutasi']==''){
			$status_mutasi="";
		}
		else{
			$status_mutasi = 
				"<tr>
					<td>Status Mutasi</td><td>:</td>
					<td><b>MUTASI</b></td>
				</tr>
				<tr>
					<td>Waktu Mutasi</td><td>:</td>
					<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuMutasi']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Dimutasi dari</td><td>:</td>
					<td><strong>".$data_kursi['MutasiDari']."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</td>
				</tr>
				<tr>
					<td>Dimutasi oleh</td><td>:</td>
					<td><strong>".$User->ambilNamaById($data_kursi['Pemutasi'])."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</td>
				</tr>";
		}
		
		if(!$cetak_tiket){
			//BELUM CETAK TIKET
			$text_id_member	= 
				"<tr><td>ID Member</td><td>:</td><td><input name='hdn_ubah_id_member' id='hdn_ubah_id_member' type='hidden' value='$id_member'/>
					<input name='ubah_id_member' id='ubah_id_member' type='text' value='$id_member' onBlur=\"if(this.value==''){Element.show('rewrite_ubah_list_discount');Element.hide('rewrite_ubah_keterangan_member');}else{cariDataMemberById(this.value);Element.hide('rewrite_ubah_list_discount');Element.show('rewrite_ubah_keterangan_member');}\" onFocus=\"Element.hide('progress_cari_member');\">
				</td></tr>
				<tr>
					<td colspan=3 align='center' height=30 >
						<span id='label_member_not_found' style='display:none;'>
							<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>ID Member belum pernah terdaftar</b></font></td></tr></table>
						</span>
						<span id='progress_cari_member' style='display:none;'>
							<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data member...</b></font></td></tr></table>
						</span>
					</td>
				</tr>";
			
			if($id_member==''){
				$display_pelanggan_setia	= "style='display:none'";
				$display_list_discount		= "";
			}
			else{
				$display_pelanggan_setia	= "";
				$display_list_discount		= "style='display:none'";
				
				$Member	= new Member();
				
				$data_member	= $Member->ambilData($id_member);
				
				$temp_tgl_lahir	= explode("-",$data_member['TglLahir']);
			}
			
			$tgl_lahir	= $temp_tgl_lahir[2];
			$bln_lahir	= $temp_tgl_lahir[1];
			$thn_lahir	= $temp_tgl_lahir[0];
			
			//MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU JIKA DITEMUKAN ADA PROMO, MAKA DISCOUNT YANG LAIN TIDAK BERLAKU
			$Promo	= new Promo();
			
			$data_discount_point	= $Promo->ambilDiscountPoint($data_kursi['IdJurusan'],$kode_jadwal,$tgl_berangkat);
			
			$target_promo	= $data_discount_point['FlagTargetPromo'];
			
			if($target_promo=='' || $target_promo==1){
				$opt_list_discount = 
					"<select id='id_discount'>".
						setListDiscount($kode_jadwal,$tgl_berangkat,$jenis_penumpang)
					."</select>";
			}
			else{
				$besar_discount			= ($data_discount_point['JumlahDiscount']>1)?"Rp.".number_format($data_discount_point['JumlahDiscount'],0,",","."):$data_discount_point['JumlahDiscount']."%";
				$opt_list_discount	="<b>Promo Discount $besar_discount<b><input type='hidden' id='id_discount' value=''/>";
			}
			//--END MEMERIKSA DISCOUNT
				
			$kolom_data_penumpang	="
				<tr>
					<td>Telepon</td><td>:</td>
					<td><input type='text' id='ubah_telp_penumpang' value='$data_kursi[Telp]' onkeypress='validasiNoTelp(event);' onFocus=\"Element.hide('ubah_telp_invalid');\" /><span id='ubah_telp_invalid' style='display:none;'><font color=red><b>(X)</b></font></span></td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td>
					<td><input type='text' id='ubah_nama_penumpang' value=\"$data_kursi[Nama]\" onFocus=\"Element.hide('ubah_nama_invalid');\" /><span id='ubah_nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
				</tr>
				<tr>
					<td>Penumpang</td><td>:</td>
					<td><span id='rewrite_ubah_keterangan_member' $display_pelanggan_setia><b>Pelanggan Setia</b></span>
						<span id='rewrite_ubah_list_discount' $display_list_discount>$opt_list_discount</span></td>
				</tr>
				<tr>
					<td valign='top'>Keterangan</td><td valign='top'>:</td>
					<td><textarea id='ubah_alamat_penumpang' cols='30' rows='2'>$data_kursi[Alamat]</textarea></td>
				</tr>";				
			
			$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
			
			$rec_asuransi	= $Asuransi->ambilData("NamaPlan","ASC");
				
			$opt_asuransi		= "<option value=''>-TANPA ASURANSI-</option>";
			
			while($row=$db->sql_fetchrow($rec_asuransi)){
				$selected	= $row["IdPlanAsuransi"]!="" && $data_asuransi["IdAsuransi"]!=$row["PlanAsuransi"]?"selected	":"";
				$opt_asuransi	.= "<option value='".$row["IdPlanAsuransi"]."' title=".$row["Keterangan"]." $selected>".$row["NamaPlan"]." | Premi: Rp.".number_format($row["BesarPremi"],0,",",".")."</option>";
			}
			
			$tgl_lahir_asuransi	= (substr($data_asuransi['TglLahir'],8,2))*1;
			$thn_lahir_asuransi	= (substr($data_asuransi['TglLahir'],0,4))*1;
			$temp_var		= "bln_asuransi".(substr($data_asuransi['TglLahir'],5,2))*1;
			$$temp_var	= "selected";
			
			$kolom_data_penumpang	.=
				"<tr>
					<td valign='top'>Asuransi</td><td valign='top'>:</td>
					<td>
						<input type='hidden' id='idasuransidipilih' value='$data_asuransi[IdAsuransi]'>
						<input type='hidden' id='tgllahirasuransidipilih' value='$tgl_lahir_asuransi'>
						<input type='hidden' id='thnlahirasuransidipilih' value='$thn_lahir_asuransi'>
						<select id='asuransipenumpang' onchange=\"asuransiManipulateTglLahir('showasuransitgllahirdetail',this.value);\">$opt_asuransi</select>
						<div id='showasuransitgllahirdetail' style='padding-top: 10px;'>
							Tgl Lahir
							<select id='asuransitgllahirdetail'></select>
							<select id='asuransiblnlahirdetail' onchange='setTglLahirAsuransi(\"asuransitgllahirdetail\",this.value,asuransithnlahir.value);'>
								<option value='1' $bln_asuransi1>Jan</option>
								<option value='2' $bln_asuransi2>Feb</option>
								<option value='3' $bln_asuransi3>Mar</option>
								<option value='4' $bln_asuransi4>Apr</option>
								<option value='5' $bln_asuransi5>Mei</option>
								<option value='6' $bln_asuransi6>Jun</option>
								<option value='7' $bln_asuransi7>Jul</option>
								<option value='8' $bln_asuransi8>Agu</option>
								<option value='9' $bln_asuransi9>Sep</option>
								<option value='10' $bln_asuransi10>Okt</option>
								<option value='11' $bln_asuransi11>Nov</option>
								<option value='12' $bln_asuransi12>Des</option>
							</select>
							<select id='asuransithnlahirdetail' onchange='setTglLahirAsuransi(\"asuransitgllahirdetail\",asuransiblnlahirdetail.value,this.value);'></select>
						</div>
					</td>
				</tr>";
				
			$kolom_data_penumpang .= $data_kursi['PetugasPenjual']!=0?"<tr><td colspan=3 align='right'><input type='button' value='  Simpan  ' onClick=\"ubahDataPenumpang('$no_tiket');\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>":"";
			
		}
		else{
			$arr_jenis_penumpang	= array("U"=>"UMUM","K"=>"Pelanggan Setia","G"=>"GRATIS","KK"=>"Keluarga Karyawan","M"=>"Mahasiswa dll","V"=>"Voucher","R"=>"Pergi-Pulang");
			
			$text_id_member	= ($id_member=='')?'':"<tr><td>ID Member</td><td>:</td><td>$id_member</td></tr>";
			
			$keterangan_penumpang	= $jenis_pembayaran!=3?$arr_jenis_penumpang[$jenis_penumpang]:"Pergi-pulang by Voucher";
			
			$kolom_data_penumpang	="
				<tr>
					<td>Telepon</td><td>:</td>
					<td>$data_kursi[Telp]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td>
					<td>$data_kursi[Nama]</td>
				</tr>
				<tr>
					<td>Penumpang</td><td>:</td>
					<td>".$keterangan_penumpang."</td>
				</tr>
				<tr>
					<td valign='top'>Keterangan</td><td valign='top'>:</td>
					<td>$data_kursi[Alamat]</td>
				</tr>
			";
			
			$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
		}
		
		
		//MEMERIKSA ASURANSI
		if($data_asuransi["PlanAsuransi"]!=""){
			
			$show_harga_asuransi	=
				"<tr>
					<td>Asuransi</td><td>:</td>
					<td align='right'>Rp. ".number_format($data_asuransi['BesarPremi'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>";
			
			$total	+= $data_asuransi['BesarPremi'];
		}
		
		$arr_no_tiket	= explode(",",$data_total['ListNoTiket']);
			
		for($i=0; $i<count($arr_no_tiket); $i++){
			$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($arr_no_tiket[$i]);
			
			$total_bayar	+= $data_asuransi['BesarPremi'];
			$total_asuransi+=$data_asuransi['BesarPremi'];
		}
		
		$show_total_harga_asuransi=
			"<tr>
				<td>Asuransi</td><td>:</td>
				<td align='right'>Rp. ".number_format($total_asuransi,0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>";
		
		$payment_code	= $data_kursi['PaymentCode']==''?"":"<tr><td>Payment Code</td><td>:</td><td><strong>".$data_kursi['PaymentCode']."</strong></td></tr>";
		
		echo("
			<table width='100%' height='500' > 
				<tr>
					<td align='center' colspan=3>
						<h1>Kursi: $kursi</h1>
						<h3>($kode_jadwal)</h3>
					</td>
				</tr>
				<tr>
					<td width='40%' colspan='3'><h3>PENUMPANG</h3>
						<input type='hidden' name='no_tiket' id='no_tiket' value='$no_tiket'>
						<input type='hidden' name='kode_booking' id='kode_booking' value='$kode_booking'>
						<input type='hidden' name='cetak_tiket' id='cetak_tiket' value='$cetak_tiket'>
						<input type='hidden' name='kursi' id='kursi' value='$kursi'>
					</td>
				</tr>
				<tr>
					<td>#Booking</td><td>:</td><td style='color:blue;'><b>$kode_booking</b></td>
				</tr>
				<tr>
					<td>#Tiket</td><td>:</td><td><b>$no_tiket</b></td>
				</tr>
					$text_id_member
					$kolom_data_penumpang
				<tr>
					<td colspan=3 height=1 bgcolor='D0D0D0'></td>
				</tr>
				<tr>
					<td>Harga tiket</td><td>:</td>
					<td align='right'>Rp. $sub_total_pertiket&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Discount</td><td>:</td>
					<td align='right'>Rp. $discount&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				$show_harga_asuransi
				<tr>
					<td colspan=3 height=1 bgcolor='D0D0D0'></td>
				</tr>
				<tr>
					<td>Total</td><td>:</td>
					<td align='right'><strong>Rp. ".number_format($total,0,",",".")."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3 height=1 bgcolor='red'></td>
				</tr>
				<tr>
					<td colspan=3><strong><h3>Keseluruhan pesanan</h3></strong></td>
				</tr>
				<tr>
					<td>Jumlah kursi dipesan</td><td>:</td>
					<td align='right'>$jumlah_kursi&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Sub Total</td><td>:</td>
					<td align='right'>Rp. $sub_total&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Total Discount</td><td>:</td>
					<td align='right'>Rp. $total_discount&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				$show_total_harga_asuransi
				<tr>
					<td colspan=3 height=1 bgcolor='red'></td>
				</tr>
				<tr>
					<td>Total Bayar</td><td>:</td>
					<td align='right'><strong>Rp. ".number_format($total_bayar,0,",",".")."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>CSO</td><td>:</td>
					<td><strong>$nama_cso</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Waktu Pesan</td><td>:</td>
					<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuPesan']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				$payment_code
				<tr>
					<td>Status Tiket</td><td>:</td>
					<td><strong>$status_pesanan</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				$waktu_cetak_tiket
				$status_mutasi");
				
		//$tombol_bayar_member="<input type='button' onclick=\"bayarByKartu('$row[kode0]');\" value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Autodebit Member&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'>";
				
		if($cetak_tiket==1) $tombol_bayar_member="";
					
		if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
			//inisialisasi tombol2
												
			//Demi keamanan, cetak tiket dibuat script tersendiri
					
			$script_cetak_tiket="
				layout_kursi	= document.getElementById('hide_layout_kursi').value;
				Start('tiket.php?sid=".$userdata['session_id']."&tiket_dipilih=\'".$row['NoTiket']."\'&mode=cetak_tiket&layout_kursi='+layout_kursi);
				getUpdateMobil();";
			
			if($cetak_tiket==1){
				$tombol_tiket="<input type='button' onclick=\"tampilkanDialogCetakUlangTiket()\" id='hider5' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cetak Ulang Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'><br><br>";
			}
			else{
				$tombol_tiket="<input type='button' onclick=\"dialog_pembayaran.show('$kode_booking');\" id='hider5' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cetak Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'><br><br>";
			}
				
			$tombol_koreksi_discount="&nbsp;<input type='button' onclick=\"tampilkanDialogDiscount('$no_tiket','$jenis_penumpang')\" id='hider6' value='Koreksi Discount'><br><br>";
			
			if($no_spj=="" || $userdata['user_level'] == $LEVEL_ADMIN){
				$tombol_mutasi="<input type='button' onclick=\"setFlagMutasi();\" id='btn_mutasi' value='      Mutasi Penumpang       '>";
			}
			else{
				$tombol_mutasi="";
			}
			
			if($cetak_tiket){
				if($userdata['user_level'] != $LEVEL_ADMIN){
					//level bukan admin
					$tombol_hapus="<input type='button' onclick=\"tampilkanDialogPembatalan('$no_tiket','$kursi');\" id='hider2' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;'>";
					$tombol_koreksi_discount	= "<br><br>";
				}
				else{
					$tombol_hapus="<input type='button' onclick=\"batal('$no_tiket','$kursi');\" id='hider2' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;'>";
				}
			}
			else{
				//$tombol_koreksi_discount="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";			
				$tombol_koreksi_discount	= "<br><br>";
				$tombol_hapus="<input type='button' onclick=\"batal('$no_tiket','$kursi');\" id='hider2' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;'>";
				$tombol_koreksi_discount	= $userdata['user_level'] != $LEVEL_ADMIN?"<br><br>":$tombol_koreksi_discount;
			}
			
			//#TIKETUX: MEMASANG PROCEDURE UNTUK MELARANG USER JIKA PEMESANAN DILAKUKAN OLEH TIKETUX
			
			if($data_kursi['PetugasPenjual']==0 && $userdata['user_level'] != $LEVEL_ADMIN){
				//PEMESAN ADALAH TIKETUX
				$tombol_hapus	= ""; //tidak boleh batal
				$tombol_koreksi_discount	= "";
								
				if(!$cetak_tiket || $data_kursi['otpUsed']==1){
					$tombol_tiket="<input type='button' onclick=\"tampilkanDialogCetakUlangTiket()\" id='hider5' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cetak Ulang Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'><br><br>";
					$tombol_mutasi	= "";
				}
				else{
					//MENGARAHKAN KE REQUEST PASSWORD OTP
					$tombol_tiket="<input type='button' onclick=\"tampilkanDialogCetakUlangTiketOTP('$no_tiket');\" id='hider5' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cetak Ulang Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'><br><br>";

					if($data_kursi['Pemutasi']=='' || $userdata['user_level'] <= $LEVEL_SUPERVISOR){
						$tombol_mutasi="<input type='button' onclick=\"setFlagMutasi();\" id='btn_mutasi' value='      Mutasi Penumpang       '>";
					}
					else{
						$tombol_mutasi	= "";
					}
				}
				
			}
			
			//DI OFF KAN DULU
			/*if($no_spj!="" && $userdata['user_level'] > $LEVEL_SUPERVISOR){
				$tombol_tiket="";
			}*/
			
			/*if($discount>0){
				$tombol_koreksi_discount="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}*/
			
			//TOMBOL HANYA DITAMPILKAN JIKA USER SELAIN MEMBER
			//update 4 mei 2011, tombol mutasi dihilangkan
			//update 16 mei 2011, tombol mutasi dimunculkan jika kendaraan belum berangkat
			
			echo(
				"<tr>
					<td colspan='3' align='center'>   
						$tombol_tiket
						$tombol_hapus $tombol_koreksi_discount
						$tombol_mutasi
					</td>
				</tr>");
		
		}//end if ($userdata['user_level'] != $LEVEL_SCHEDULER)
			
		echo("
			<tr><td colspan='3'><hr color='e0e0e0'></td></tr>
			</table>
		");	
		
	
	exit;
break;

// AMBIL STATUS KURSI SATUAN ==========================================================================================================================================================================================
case "ambil_status_kursi":
	include($adp_root_path . 'ClassLayoutKendaraan.php');
	
	$tgl 					= $HTTP_GET_VARS['tanggal'];
	$kode_jadwal 	= $HTTP_GET_VARS['jadwal'];
	$no_kursi 				= $HTTP_GET_VARS['no_kursi'];
	
	$LayoutKendaraan	= new LayoutKendaraan();
	$Jadwal						= new Jadwal();
	
	$row = $Jadwal->ambilDataDetail($kode_jadwal);
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	echo $LayoutKendaraan->getStatusKursi($tgl,$kode_jadwal_aktual,$no_kursi);
	
	exit;
	break;
	

// UBAH DATA PENUMPANG ===========================================================================================================================================================================================
case "ubahdatapenumpang":
  // mendapatkan nilai kursi yang tersedia dan value nilai lain sesuai dengan tanggal, rute, jam dan kode
		
	  $no_tiket			= trim($HTTP_GET_VARS['no_tiket']);
	  $kursi 				= trim($HTTP_GET_VARS['kursi']);
	  $nama  				= trim($HTTP_GET_VARS['nama']);
	  $telp  				= trim($HTTP_GET_VARS['telepon']);
	  $alamat				= trim($HTTP_GET_VARS['alamat']);
	  $id_discount	= trim($HTTP_GET_VARS['id_discount']);
	  $id_member		= trim($HTTP_GET_VARS['id_member']);
		$id_asuransi	= $HTTP_GET_VARS['idasuransi'];
	  $tgl_lahir		= $HTTP_GET_VARS['tgllahir'];
		
		$Reservasi		= new Reservasi();
		
		$Reservasi->ubahDataPenumpang($no_tiket, $nama , $alamat, $telp, $kursi, $id_discount, $id_member);
	
		$Asuransi	= new Asuransi();
		
		$Asuransi->hapusAsuransi($no_tiket);
		
		if($id_asuransi!=""){
			$data_kursi	= $Reservasi->ambilDataKursi($no_tiket);
			
			$data_asuransi = $Asuransi->ambilDataDetail($id_asuransi);
			
			$Asuransi->tambahAsuransi(
				$data_kursi["TglBerangkat"], $data_kursi["IdJurusan"], $data_kursi["KodeJadwal"], 
				$data_kursi["JamBerangkat"], $no_tiket, $data_kursi["Nama"], 
				$tgl_lahir, $data_kursi["Telp"], $data_kursi["Hp"], 
				$id_asuransi, $data_asuransi["BesarPremi"], $userdata["user_id"], 
				$userdata["KodeCabang"]);
		}
		
		echo(1);
		
	exit;
break;

// TEMP ==========================================================================================================================================================================================
case "temp":
	
	$tgl 					= $HTTP_GET_VARS['tanggal'];
	$kode_jadwal 	= $HTTP_GET_VARS['jam'];
	$kursi 				= $HTTP_GET_VARS['kursi'];
	$no_spj 			= $HTTP_GET_VARS['no_spj'];
	$layout_kursi = $HTTP_GET_VARS['layout_kursi'];
	
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	
	$row = $Jadwal->ambilDataDetail($kode_jadwal);
	$layout_kursi 	= $row['JumlahKursi'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	$session_id = $userdata['user_id'];
	
	$Reservasi->updateStatusKursi($kursi,$session_id,$kode_jadwal_aktual,$tgl,$userdata['user_level']);

	include($adp_root_path . 'ClassLayoutKendaraan.php');
	
	$LayoutKendaraan	= new LayoutKendaraan();
	
	echo $LayoutKendaraan->getStatusKursi($tgl,$kode_jadwal_aktual,$kursi);
	
	exit;
	break;
	
//BOOK ===========================================================================================================================================================================================
case "book":
	
	// UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
  $tgl     		= $HTTP_GET_VARS['tanggal'];   // tanggal
  $kode_jadwal= $HTTP_GET_VARS['kode_jadwal'];   // jam
  $id_member	= $HTTP_GET_VARS['id_member'];  // id member
  $nama    		= $HTTP_GET_VARS['nama'];  // nama
  $alamat 		= $HTTP_GET_VARS['alamat']; //alamat
	$no_telp_old= $HTTP_GET_VARS['no_telp_old']; // telepon
	$telepon 		= $HTTP_GET_VARS['telepon']; // telepon
	$no_spj			= $HTTP_GET_VARS['no_spj'];
	$layout_kursi= $HTTP_GET_VARS['layout_kursi'];
	$id_discount= $HTTP_GET_VARS['jenis_discount'];
	$id_asuransi= $HTTP_GET_VARS['idasuransi'];
	$tgl_lahir	= $HTTP_GET_VARS['tgllahir'];
	
	$kode_booking	= generateKodeBookingPenumpang();
	$payment_code	= "";
	
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	$Promo			= new Promo();
	$Jurusan		= new Jurusan();
	
	$list_no_telp	= array();
	
	$session_id	= $userdata['session_id'];
  $useraktif	= $userdata['user_id'];

	//memgambil data rute dan jadwal
	$row 					= $Jadwal->ambilDataDetail($kode_jadwal);
	$jam_berangkat= $row['JamBerangkat'];
	$id_jurusan 	= $row['IdJurusan'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	$is_express		= $row['IsExpress'];
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;

  //DEBUG TRAP LOG
  //$Reservasi->tambahLogTrap($kode_booking,"",$tgl,$kode_jadwal,0,"LINE:".__LINE__."| INISIASI | USER:".$userdata['nama']);

	//MEMERIKSA JIKA JADWAL EXPRESS NOMOR TELP BOLEH KOSONG, JIKA TIDAK, HARUS DIISI
	if($is_express==0){
		if($telepon==""){
			echo("status='ERROR';err='2';");
			exit;
		}
	}
	else{
		$telepon=$telepon==""?"0000":$telepon; //nomor telepon default untuk express
	}
	
	//mengambil 	data header tb posisi 
	$row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_aktual);
	$kode_kendaraan	= $row['KodeKendaraan'];
	$kode_sopir			= $row['KodeSopir'];
	$tgl_cetak_SPJ	= $row['TglCetakSPJ'];
	$petugas_cetak_spj= $row['PetugasCetakSPJ'];
	$cetak_spj			= ($no_spj=="")?0:1;
						
	//mengambil data-data kode account dan komisi
	$row														= $Jurusan->ambilDataDetail($id_jurusan);
	$komisi_penumpang_CSO						= $row['KomisiPenumpangCSO'];
	$kode_akun_pendapatan						= $row['KodeAkunPendapatanPenumpang'];
	$kode_akun_komisi_penumpang_CSO	= $row['KodeAkunKomisiPenumpangCSO'];
	
	$harga_tiket= $Reservasi->getHargaTiket($kode_jadwal,$tgl);
	$charge	=0;
	$PPN=0;
	$jenis_muatan=0;
	$flag_pesanan=0;
	
	//MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU
	$data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl);
	
	$discount		= ($data_discount_point['FlagDiscount']!=1)?$data_discount_point['JumlahDiscount']:($data_discount_point['JumlahDiscount']/100)*$harga_tiket;
	$point			= $data_discount_point['JumlahPoint'];
	$target_promo	= $data_discount_point['FlagTargetPromo'];
	$nama_promo	= $data_discount_point['NamaPromo'];
	
	$jenis_penumpang="U";
	
	if(trim($id_member)!=''){
		$Member = new Member();
		
		$data_member	= $Member->ambilData($id_member);
		$nama    			= $data_member['Nama'];  
	  //$alamat 			= $data_member['Alamat']; 
		//$telepon 			= $data_member['Handphone']; 
		
		//memeriksa discount 
		//if($nama!="" && $data_member['MasaBerlaku']>0){
		if($data_member['IdMember']!=""){
		
			if($data_member["KategoriMember"]!=2){
				$discount				= ($target_promo <=1 )?$discount:0;
				$point_member		= ($target_promo <=1 )?$point:0;
			}
			else{
				//MEMBER VIP
				$discount			= $harga_tiket;
				$point_member	= 0;
				$nama_promo		= "VIP";
			}
			$jenis_penumpang="K";
		}
	}
	else{
		//memeriksa discount 
		$discount	= ($target_promo ==2 || $target_promo ==0 )?$discount:0;
	}
	
	$jenis_discount	= ($discount==0)?"":$nama_promo;
	
	//MEMERIKSA APAKAH ADA PEMBERIAN DISCOUNT
	if($id_discount!='' && ($discount==0 || $discount=='')){
		//jika memang discount diberikan, maka akan diupdate pada database
		$data_discount	= $Reservasi->ambilDiscount($id_discount);
		$jenis_penumpang= $data_discount['KodeDiscount'];
		$jenis_discount	= $data_discount['NamaDiscount'];
		
		$besar_discount	= ($data_discount['JumlahDiscount']>1)? $data_discount['JumlahDiscount']:$harga_tiket*$data_discount['JumlahDiscount'];
		
		$discount	= ($besar_discount<=$harga_tiket)?$besar_discount:$harga_tiket;
		
		//$harga_tiket	= $data_discount['KodeDiscount']!="R"?$harga_tiket:$harga_tiket+$Reservasi->getHargaTiketNormal($id_jurusan);
	}
	
	//LAYOUT KURSI
	$result_layout_kursi = $Reservasi->ambilDataFlagLayout($tgl,$kode_jadwal_aktual,$userdata['user_id']);

  //DEBUG TRAP LOG
  //$jumlah_data  = $db->sql_numrows($result_layout_kursi);
  //$Reservasi->tambahLogTrap($kode_booking,"",$tgl,$kode_jadwal,0,"LINE:".__LINE__."| Ambil Kursi Dipilih | Kode Jadwal Aktual:$kode_jadwal_aktual | Kursi Dipilih:".$jumlah_data." | USER:".$userdata["nama"]);

	//mengisi  flag kursi dan session
	
	$jumlah_kursi_dipesan=0;
	
	$list_nomor_kursi="";
	
	while ($data_kursi = $db->sql_fetchrow($result_layout_kursi)){
    //Mengenerate no tiket
    $no_tiket = generateNoTiketPenumpang();

    $jumlah_kursi_dipesan++;

    $no_kursi	= $data_kursi['NomorKursi'];

    //DEBUG TRAP LOG
    //$Reservasi->tambahLogTrap($kode_booking,$no_tiket,$tgl,$kode_jadwal,$no_kursi,"LINE:".__LINE__."| Iterasi START Booking | Kode Jadwal Aktual:$kode_jadwal_aktual | Iterasi:".$jumlah_kursi_dipesan." | USER:".$userdata["nama"]);
		
		//$sub_total	= $harga_tiket;
		$sub_total	= $data_discount['KodeDiscount']!="R"?$harga_tiket:$harga_tiket+$Reservasi->getHargaTiketNormal($id_jurusan);
		$total			= $sub_total+$charge+$PPN-$discount;
		
		//MEMERIKSA APAKAH KURSI MASIH FREE (add 17 Oktober 2011)
		$sql = 
			"SELECT IF(StatusKursi='' OR StatusKursi IS NULL,0,StatusKursi) AS SudahTerisi
				FROM tbl_posisi_detail
				WHERE KodeJadwal LIKE '$kode_jadwal_aktual' AND TglBerangkat LIKE '$tgl' AND NomorKursi='$no_kursi' AND Session!='$userdata[user_id]'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		$status_kursi	= $row[0];

    //DEBUG TRAP LOG
    //$Reservasi->tambahLogTrap($kode_booking,$no_tiket,$tgl,$kode_jadwal,$no_kursi,"LINE:".__LINE__."| Iterasi Status Kursi | Kode Jadwal Aktual:$kode_jadwal_aktual | Iterasi:".$jumlah_kursi_dipesan." | Status Kursi: $status_kursi | USER:".$userdata["nama"]);

    if($status_kursi==0 || $status_kursi==''){
			$Reservasi->booking(
				$no_tiket, $userdata['KodeCabang'], $kode_jadwal,
				$id_jurusan, $kode_kendaraan , $kode_sopir ,
				$tgl, $jam_berangkat , $kode_booking,
				$id_member, $point_member, $nama ,
				$alamat, $telepon, $hp,
				$no_kursi, $harga_tiket,
				$charge, $sub_total, $discount,
				$PPN, $total, $userdata['user_id'],
				$flag_pesanan, $no_spj, $tgl_cetak_SPJ,
				$cetak_spj, $komisi_penumpang_CSO,
				$petugas_cetak_spj, $keterangan, $jenis_discount,
				$kode_akun_pendapatan,$jenis_penumpang, $kode_akun_komisi_penumpang_CSO,
				$payment_code);

      //DEBUG TRAP LOG
      //$Reservasi->tambahLogTrap($kode_booking,$no_tiket,$tgl,$kode_jadwal,$no_kursi,"LINE:".__LINE__."| Iterasi Insert Book | Kode Jadwal Aktual:$kode_jadwal_aktual | Iterasi:".$jumlah_kursi_dipesan." | PARAM: $no_tiket # $userdata[KodeCabang] # $kode_jadwal # $id_jurusan # $kode_kendaraan # $kode_sopir # $tgl # $jam_berangkat # $kode_booking # $id_member # $point_member # $nama # $alamat # $telepon # $hp # $no_kursi # $harga_tiket # $charge #  $sub_total # $discount # $PPN # $total # $userdata[user_id] # $flag_pesanan # $no_spj # $tgl_cetak_SPJ # $cetak_spj # $komisi_penumpang_CSO # $petugas_cetak_spj # $keterangan # $jenis_discount # $kode_akun_pendapatan # $jenis_penumpang # $kode_akun_komisi_penumpang_CSO # $payment_code | USER:".$userdata["nama"]);

      if(($id_discount=='' && $target_promo==1) || ($id_discount!='' && $data_discount['KodeDiscount']!="R")){
				//DIKARENAKAN DISCOUNT  HANYA DIBERIKAN KEPADA PELANGGAN PERTAMA,MAKA SETELAH DISCOUNT DIBERIKAN, BESAR DISCOUNT AKAN DI FREE KEMBALI ATAU KodeDiscount Bukan Tiket Return
				//ket: $target_promo=1 adalah promo yang hanya untuk member saja
				
				$id_member			= '';
				$discount				= 0; //discount diset kembali menjadi 0
				$jenis_discount	= ''; //jenis discount direset
				$jenis_penumpang= 'U'; //penumpang direset menjadi penumpang umum
			}
			
			//MEMERIKSA JIKA MEMBELI TRANSAKSI, AKAN DI SET
			if($id_asuransi!=""){
				$Asuransi = new Asuransi();
				
				$data_asuransi		= $Asuransi->ambilDataDetail($id_asuransi);
				
				$Asuransi->tambahAsuransi($tgl,$id_jurusan,$kode_jadwal,$jam_berangkat,$no_tiket,$nama,$tgl_lahir,$telepon,$hp,$id_asuransi,$data_asuransi["BesarPremi"],$userdata["user_id"],$userdata['KodeCabang']);
			}
			
			
			$list_nomor_kursi .= "\"".$no_kursi."\",";
			
			//set data array untuk sms
			if(!in_array($telepon,$list_no_telp)){
				$list_no_telp[]		= $telepon;		
			}

      //DEBUG TRAP LOG
      //$Reservasi->tambahLogTrap($kode_booking,$no_tiket,$tgl,$kode_jadwal,$no_kursi,"LINE:".__LINE__."| Iterasi END Book | Kode Jadwal Aktual:$kode_jadwal_aktual | Iterasi:".$jumlah_kursi_dipesan." | LIST NO KURSI:$list_nomor_kursi | USER:".$userdata["nama"]);

    }
	}
		
			
	//jika ada kursi yang dipesan
	if($jumlah_kursi_dipesan>0){
		//mengirim messange true ke body
		
		$Reservasi->ubahPosisi($kode_jadwal_aktual, $tgl, $jumlah_kursi_dipesan);
		
		//KIRIM SMS PROMO
			for($idx=0;$idx<count($list_no_telp);$idx++){
				$telp	= $list_no_telp[$idx];
				 
				if(in_array(substr($telp,0,2),$HEADER_NO_TELP)){
					
					$isi_pesan	= 
						"Terima Kasih telah memesan tiket XTrans. Kini Tiket XTrans dapat dipesan secara online di www.xtrans.co.id. Pilih sendiri kursi anda dan nikmati kemudahannya.";
					
					$telepon	= "62".substr($telp,1);
					$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
					//$response	= sendHttpPost($sms_config['url'],$parameter);	
				}
			}//end for
		
		$list_nomor_kursi	= substr($list_nomor_kursi,0,-1);
    //DEBUG TRAP LOG
    //$Reservasi->tambahLogTrap($kode_booking,$no_tiket,$tgl,$kode_jadwal,$no_kursi,"LINE:".__LINE__."| Book OK | Kode Jadwal Aktual:$kode_jadwal_aktual | Iterasi:".$jumlah_kursi_dipesan." | LIST NO KURSI:$list_nomor_kursi | USER:".$userdata["nama"]);

    echo("status=\"OK\";kodebooking=\"$kode_booking\";listkursi=new Array($list_nomor_kursi);");
	}
	else{
		//mengirim messange false ke body

    //DEBUG TRAP LOG
    //$Reservasi->tambahLogTrap($kode_booking,$no_tiket,$tgl,$kode_jadwal,$no_kursi,"LINE:".__LINE__."| Book GAGAL | Kode Jadwal Aktual:$kode_jadwal_aktual | Iterasi:".$jumlah_kursi_dipesan." | LIST NO KURSI:$list_nomor_kursi | USER:".$userdata["nama"]);

    echo("status='ERROR';err=1;");
	}
			
	
	echo($pesan);
exit;
	
//MEMBATALKAN PESANAN ====================================================================================================================================
case 'pembatalan':
	
	$no_tiket	= $HTTP_POST_VARS['no_tiket'];// nourut
	$kursi		= $HTTP_POST_VARS['no_kursi']; // kursi
	$Reservasi	= new Reservasi();
	
	$data_tiket	= $db->sql_fetchrow($Reservasi->ambilDataKursiByNoTiket($no_tiket));
	
	$valid=false;
	
	if($data_tiket['CetakTiket']==0){
		//BELUM CETAK TIKET
		$valid			 	= true;
		$user_pembatal= $userdata['user_id'];
	}
	else{
		//SUDAH CETAK TIKET
		if(in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_SUPERVISOR))){
			$valid= true;
			$user_pembatal= $userdata['user_id'];
		}
		else{
			$username				= $HTTP_POST_VARS['username'];
			$password				= $HTTP_POST_VARS['password'];
		
			$User = new User();
				
			$data_user	= $User->ambilDataDetailByUsername($username);
				
			$valid= $data_user['user_password']==md5($password) && $data_user['user_level']<=$LEVEL_SUPERVISOR?true:false;
			$user_pembatal= $data_user['user_id'];
		}
	
	}
	
	if($valid){
		if($Reservasi->pembatalan($no_tiket, $kursi, $user_pembatal)){
			$Asuransi = new Asuransi();
			$Asuransi->hapusAsuransi($no_tiket);
		}
		echo(1);
	}
	else{
		echo(0);
	}
	
	exit;
		
break;

//APPROVAL CETAK ULANG TIKET================================================================================================================================

case 'cetakulangtiket':
	
	$username				= $HTTP_POST_VARS['username'];
	$password				= $HTTP_POST_VARS['password'];
	
	$User = new User();
	
	$Reservasi= new Reservasi();
	
	$no_tiket				= $HTTP_POST_VARS['no_tiket'];
	$result					= $Reservasi->ambilDataKursiByNoTiket($no_tiket);
	$data_penumpang	= $db->sql_fetchrow($result);
	
	$data_user	= $User->ambilDataDetailByUsername($username);
	
	if($data_user['user_password']==md5($password) || $data_user['user_level']>$LEVEL_SUPERVISOR){
		
		$data_kursi	= $Reservasi->ambilDataKursi($no_tiket);
		
		$Reservasi->tambahLogCetakTiket(
			$no_tiket,$data_kursi['TglBerangkat'],$data_kursi['JamBerangkat'],
			$data_kursi['KodeJadwal'],$data_kursi['IdJurusan'],$data_kursi['NomorKursi'],
			$userdata['user_id'],$userdata['nama'],$data_user['user_id'],
			$data_user['nama']);
		
		echo("CetakTiket(0);");
	}
	else{
		//jika password tidak sesuai maka tidak diijinkan melakukna proses ini
		echo("alert('Anda tidak memiliki otoritas untuk melakukan aksi ini!');");
	}
	
	exit;
		
break;

//APPROVAL CETAK ULANG TIKET MENGGUNAKAN KODE OTP================================================================================================================================

case 'cetakulangtiketotp':
	
	$password				= $HTTP_POST_VARS['password'];
	$no_tiket				= $HTTP_POST_VARS['no_tiket'];
	
	$User = new User();
	
	//TIKETUX
	$Reservasi= new Reservasi();
	
	
	$result					= $Reservasi->ambilDataKursiByNoTiket($no_tiket);
	$data_penumpang	= $db->sql_fetchrow($result);
	

	if($userdata['user_level']==$LEVEL_ADMIN || $data_penumpang['otp']==$password){
		echo("CetakTiket(0);");
	}
	else{
		echo("alert('Kode OTP yang anda masukkan tidak benar!');");
	}
	
	exit;
		
break;

//KOREKSI DISCOUNT================================================================================================================================

case 'beridiscount':
	
	$no_tiket				= $HTTP_POST_VARS['no_tiket'];// nourut
	$jenis_discount	= $HTTP_POST_VARS['jenis_discount'];
	$username				= $HTTP_POST_VARS['username'];
	$password				= $HTTP_POST_VARS['password'];
	
	$Reservasi	= new Reservasi();
	
	//memeriksa apakah tiket sudah dicetak atau belum
	$data_tiket	= $Reservasi->ambilDataKursi($no_tiket);
	
	if($data_tiket['CetakTiket']==1){
		//jika tiket sudah dicetak, maka memerlukan otorisasi untuk meneruskan proses ini
		
		$User = new User();
		
		$data_user	= $User->ambilDataDetailByUsername($username);
		
		if($data_user['user_password']!=md5($password) || $data_user['user_level']>$LEVEL_ADMIN){
			//jika password tidak sesuai maka tidak diijinkan melakukna proses ini
			echo(0);
			exit;
		}
		
		$petugas_otorisasi	= $data_user['user_id'];
		
	}
	else{
		$petugas_otorisasi	= "NULL";
	}
	
	//mengupdate total bayar sesuai dengan jumlah discount
	
	if($jenis_discount!=''){
		$data_discount	= $Reservasi->ambilDiscount($jenis_discount);
		$kode_discount	= $data_discount['KodeDiscount'];
		$nama_discount	= $data_discount['NamaDiscount'];
		$jumlah_discount= $data_discount['JumlahDiscount'];
	}
	else{
		$nama_discount		= "";
		$jumlah_discount	= 0;
	}
	
	//mengambil harga tiket berlaku
	$harga_tiket= $Reservasi->getHargaTiket($data_tiket['KodeJadwal'],$data_tiket['TglBerangkat']);
	
	$sql	= 
		"UPDATE tbl_reservasi SET
			JenisPenumpang='$kode_discount',
			Discount=IF($jumlah_discount>1,IF($jumlah_discount<=$harga_tiket,$jumlah_discount,$harga_tiket),$jumlah_discount*$harga_tiket),
			JenisDiscount='$nama_discount',
			SubTotal=$harga_tiket,
			Total=SubTotal-Discount
		WHERE NoTiket IN('$no_tiket');";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL MENGUBAH DATA');//,__LINE__,__FILE__,$sql);
	}
	
	//INSERT KE LOG KOREKSI DISCOUNT
	$sql	= 
		"INSERT INTO tbl_log_koreksi_disc(
			NoTiket,KodeCabang,KodeJadwal,
			IdJurusan,TglBerangkat,JamBerangkat,
			KodeBooking,Nama,Telp,
			WaktuPesan,NomorKursi,HargaTiket,
			Charge,SubTotal,DiscountMula,
			DiscountBaru,Total,PetugasPenjual,
			CetakTiket,PetugasCetakTiket,WaktuCetakTiket,
			JenisDiscountMula,JenisDiscountBaru,JenisPembayaran,
			FlagBatal,JenisPenumpangMula,JenisPenumpangBaru,
			PetugasPengkoreksi,PetugasOtorisasi,WaktuKoreksi,
			WaktuCetakSPJ
		)VALUES(
			'$data_tiket[NoTiket]','$data_tiket[KodeCabang]','$data_tiket[KodeJadwal]',
			'$data_tiket[IdJurusan]','$data_tiket[TglBerangkat]','$data_tiket[JamBerangkat]',
			'$data_tiket[KodeBooking]','$data_tiket[Nama]','$data_tiket[Telp]',
			'$data_tiket[WaktuPesan]','$data_tiket[NomorKursi]','$data_tiket[HargaTiket]',
			'$data_tiket[Charge]','$data_tiket[SubTotal]','$data_tiket[Discount]',
			IF($jumlah_discount>1,IF($jumlah_discount<=$harga_tiket,$jumlah_discount,$harga_tiket),$jumlah_discount*$harga_tiket),SubTotal-DiscountBaru,'$data_tiket[PetugasPenjual]',
			'$data_tiket[CetakTiket]','$data_tiket[PetugasCetakTiket]','$data_tiket[WaktuCetakTiket]',
			'$data_tiket[JenisDiscount]','$nama_discount','$data_tiket[JenisPembayaran]',
			'$data_tiket[FlagBatal]','$data_tiket[JenisPenumpang]','$kode_discount',
			'$userdata[user_id]',$petugas_otorisasi,NOW(),
			'$$data_tiket[TglCetakSPJ]');";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL MENGUBAH DATA');//,__LINE__,__FILE__,$sql);
	} 
	
	echo(1);
	exit;
		
break;

//MUTASI PENUMPANG===============================================================================================================================================================================
case "mutasipenumpang":
	
	// UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
  $tgl     		= $HTTP_GET_VARS['tanggal'];   
  $kode_jadwal= $HTTP_GET_VARS['kode_jadwal'];   
  $no_tiket		= $HTTP_GET_VARS['no_tiket'];  
  $no_kursi		= $HTTP_GET_VARS['no_kursi']; 
	$no_spj			= $HTTP_GET_VARS['no_spj'];
	$layout_kursi= $HTTP_GET_VARS['layout_kursi'];
	
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	$Promo			= new Promo();
	$Jurusan		= new Jurusan();
	
	$session_id	= $userdata['session_id'];
  $useraktif	= $userdata['user_id'];
  
	//Mengambil data tiket yang lama
	$result	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);
	$data_tiket_lama 	= $db->sql_fetchrow($result);
	$id_member				= $data_tiket_lama['IdMember'];
	$tgl_lama					= $data_tiket_lama['TglBerangkat'];
	$kode_jadwal_lama	= $data_tiket_lama['KodeJadwal'];
	$no_kursi_lama		= $data_tiket_lama['NomorKursi'];
	$nama_lama				= $data_tiket_lama['Nama'];
	$cetak_tiket_lama	= $data_tiket_lama['CetakTiket'];
	$harga_tiket_lama	= $data_tiket_lama['HargaTiket'];
	$jenis_penumpang	= $data_tiket_lama['JenisPenumpang'];
	$discount					= $data_tiket_lama['Discount'];
	$total						= $data_tiket_lama['Total'];

	//--END data tiket lama
	
	//memgambil data rute dan jadwal
	$row 					= $Jadwal->ambilDataDetail($kode_jadwal);
	
	//memeriksa jika jenis penumpang adalah penumpang R maka tidak boleh mutasi pindah jurusan
	if(($jenis_penumpang=='R' || $jenis_penumpang=='VR') && $row['Asal']!='BDR' && $row['Tujuan']!='BDR'){
		echo(4);
		exit;
	}
	
	$jam_berangkat= $row['JamBerangkat'];
	$id_jurusan 	= $row['IdJurusan'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	//mengambil 	data header tb posisi 
	$row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_aktual);
	$kode_kendaraan	= $row['KodeKendaraan'];
	$kode_sopir			= $row['KodeSopir'];
	$tgl_cetak_SPJ	= $row['TglCetakSPJ'];
	$petugas_cetak_spj= $row['PetugasCetakSPJ'];
	$cetak_spj			= ($no_spj=="")?0:1;
						
	//mengambil data-data kode account dan komisi
	$row														= $Jurusan->ambilDataDetail($id_jurusan);
	$komisi_penumpang_CSO						= $row['KomisiPenumpangCSO'];
	$kode_akun_pendapatan						= $row['KodeAkunPendapatanPenumpang'];
	$kode_akun_komisi_penumpang_CSO	= $row['KodeAkunKomisiPenumpangCSO'];
	
	//Mengubah posisi TUJUAN
	if(!$Reservasi->ubahPosisi($kode_jadwal_aktual, $tgl, 1)){
		echo(0);
		exit;
	}
		
	//Mereset posisi SEBELUMNYA
	if(!$Reservasi->ubahPosisi($kode_jadwal_lama, $tgl_lama, -1)){
		echo(0);
		exit;
	}
	
	
	$harga_tiket= $Reservasi->getHargaTiket($kode_jadwal,$tgl);
	$charge	=0;
	$PPN=0;
	$jenis_muatan=0;
	$sub_total	= $harga_tiket;
				
	if(!$Reservasi->mutasiPenumpang(
		$no_tiket, $kode_jadwal,$id_jurusan, 
		$kode_kendaraan , $kode_sopir ,$tgl, 
		$jam_berangkat , $no_kursi, $harga_tiket,
		$charge, $sub_total, $discount,
		$PPN, $total,$no_spj, 
		$tgl_cetak_SPJ, $cetak_spj, $komisi_penumpang_CSO,
		$petugas_cetak_spj, $keterangan, $jenis_discount,
		$kode_akun_pendapatan, $kode_akun_komisi_penumpang_CSO, $payment_code,
		$nama_lama,$cetak_tiket_lama,$kode_jadwal_lama,
		$tgl_lama,$no_kursi_lama,$userdata['user_id'])){
		
		echo(3);
		exit;
	}
	
	if($harga_tiket==$harga_tiket_lama){
		echo(1);
	}
	else{
		echo(2);
	}
exit;

//==PAKET==
// PAKET LAYOUT===========================================================================================================================================================================================
case "paketlayout":
  // menampilkan paket yang tersedia sesuai dengan tanggal, rute dan jam yang dimasukan
  include($adp_root_path . 'ClassPaket.php');       			
  
	$Paket		= new Paket();
	$Jadwal		= new Jadwal();
	$Reservasi= new Reservasi();
	
	if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
		//HANYA BISA DIAKSES OLEH SELAIN MEMBER
		
		$tgl  				= $HTTP_GET_VARS['tanggal']; // tanggal    
		$kode_jadwal 	= $HTTP_GET_VARS['jadwal'];    // jam
		
		if($kode_jadwal!="" && $kode_jadwal!="(none)"){
			//LAYOUT PAKET
			
			$row = $Jadwal->ambilDataDetail($kode_jadwal);
		  $jam_berangkat	= $row['JamBerangkat'];
			
			if($row['FlagSubJadwal']!=1){
				$kode_jadwal_utama	= $kode_jadwal;
			}
			else{
				$kode_jadwal_utama	= $row['KodeJadwalUtama'];
			}
			
			//MEMERIKSA HAK AKSES, JIKA CSO BIASA TIDAK BOLEH MEMESAN PADA WAKTU YANG SUDAH LALU
			if(!$Reservasi->periksaHakAkses($tgl,$jam_berangkat) && $user_level>=$LEVEL_CSO){
				if($userdata['user_level']==$LEVEL_CSO_PAKET){
					echo("<br><br><br><br><br><br>
						<img src='./templates/images/icon_warning.png' />
						<font color='red'><h3>Anda tidak boleh memilih waktu yang sudah lalu!</h3></font>");
				}
				exit;
			}
			//--END PERIKSA HAK AKSES
			
			$tombol_mutasikan	= "";
			
			if($HTTP_GET_VARS['flag_mutasi']==1){
				$tombol_mutasikan= "<input type='button' value=' mutasikan paket ke jadwal ini ' onclick='mutasiPaket();' />";
			}
			
			echo(
				"<table cellspacing='1' cellpadding='0' width='400'>
					<tbody>
					<tr>
						<td align='right' colspan=2>
							$tombol_mutasikan
							<input type='button' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refresh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' onclick='getUpdatePaket();getPengumuman();' />
						</td>
					</tr>
					<tr>
						<td align='left' bgcolor='d0d0d0' height=20 colspan=2>
								&nbsp;<font size=2 color='505050'><strong>Daftar Paket</strong>&nbsp;&nbsp;
								<span id='progress_paket' style='display:none;'>
									<img src='./templates/images/loading.gif' />
								</span>&nbsp;
						</td>
					</tr>");
			
			// transaksi


      //LIST KODE JADWAL BERDASARKAN JADWAL UTAMA
      if($kodejadwal==$kode_jawal_utama){
        $res_subjadwal  = $Jadwal->ambilListSubKodeJadwalByKodeJadwal($kode_jadwal);
        $list_sub_jadwal  = "";

        while($data_kode_jadwal=$db->sql_fetchrow($res_subjadwal)){
          $list_sub_jadwal .= "'".$data_kode_jadwal["KodeJadwal"]."',";
        }

        $list_sub_jadwal  = substr($list_sub_jadwal,0,-1);

        $kondisi_sub_jadwal = $list_sub_jadwal!=""?" OR KodeJadwal IN($list_sub_jadwal) ":"";
      }


		  if ($result = $Paket->ambilData("TglBerangkat='$tgl' AND (KodeJadwal='$kode_jadwal' OR KodeJadwal='$kode_jadwal_utama' $kondisi_sub_jadwal) AND FlagBatal!=1 ORDER BY WaktuPesan ASC")){
				
				while ($row = $db->sql_fetchrow($result)){
					$i++;
					$odd ='odd';
				
					if (($i % 2)==0){
						$odd = 'even';
					}
				
					$nama_pengirim_disingkat=substr($row['NamaPengirim'],0,20);
					$nama_penerima_disingkat=substr($row['NamaPenerima'],0,20);
					$total_bayar=number_format($row['TotalBayar'],0,",",".");
					$cetak_tiket		=$row['CetakTiket'];
					$cara_bayar			=$row['CaraPembayaran'];
					$status_diambil	=$row['StatusDiambil'];
					
					
					
					if($cetak_tiket && ($cara_bayar!=$PAKET_CARA_BAYAR_DI_TUJUAN || $status_diambil==1)){
						$label_lunas="background='./templates/images/label_lunas.png' STYLE='background-repeat: no-repeat;background-position: left middle;'";
					}
					else{
						$label_lunas="background='./templates/images/icon_paket.png' STYLE='background-repeat: no-repeat;background-position: left middle;'";
					}

          $odd= $kode_jadwal==$row["KodeJadwal"]?"green":$odd;

		      echo("
					<tr class='$odd'>
						<td width='20'><h3>$i.</h3></td>
						<td onclick=\"showPaket('$row[NoTiket]')\"  valign='top' height='40' $label_lunas>
							<font color='008609'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pengirim: $nama_pengirim_disingkat Rp. $total_bayar</b></font><br>
							<font color='0000ff'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Penerima: $nama_penerima_disingkat</b></font>
						</td>
					</tr>
				");
				}	
		  } 
		  else{	  
		    //die_error('Cannot Load paket',__LINE__,__FILE__,$sql);
				echo("Error :".__LINE__);exit;
			}

			if($i<=0){
				echo("
					<tr align='center'>
						<td colspan=2>tidak ada paket</td>
					</tr>
				");
			}
			echo("</tbody></table>");
		}
	}
	else{
		echo("");
	}
	
  exit;
break;

//PAKET TAMBAH ===========================================================================================================================================================================================
case "pakettambah":
	include($adp_root_path . 'ClassPaket.php');
	$Reservasi	= new Reservasi();
	$Paket			= new Paket();
	$Jadwal			= new Jadwal();
	$Jurusan		= new Jurusan();
	
	// UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
  $tgl     				= $HTTP_GET_VARS['tanggal'];   // tanggal
  $kode_jadwal		= $HTTP_GET_VARS['kodejadwal'];   // kode jadwal
  $nama_pengirim	= $HTTP_GET_VARS['namapengirim'];  // nama
  $alamat_pengirim= $HTTP_GET_VARS['alamatpengirim']; //alamat
	$telepon_pengirim=$HTTP_GET_VARS['teleponpengirim']; // telepon  
	$nama_penerima	= $HTTP_GET_VARS['namapenerima'];  // nama
  $alamat_penerima= $HTTP_GET_VARS['alamatpenerima']; //alamat
	$telepon_penerima=$HTTP_GET_VARS['teleponpenerima'];   
	$keterangan			= $HTTP_GET_VARS['keterangan'];
	$instruksi_khusus= $HTTP_GET_VARS['instruksikhusus'];   
	$jumlah_koli		= $HTTP_GET_VARS['jumlahkoli']; 
	$jenis_barang		= $HTTP_GET_VARS['jenisbarang']; 
	$layanan				= $HTTP_GET_VARS['layanan']; 
	$cara_bayar			= $HTTP_GET_VARS['carabayar']; 
	$diskon					= $HTTP_GET_VARS['diskon'];
	$kode_pelanggan	= $HTTP_GET_VARS['kodepelanggan'];
	
	
	
	$arr_harga_paket= $Paket->ambilHargaPaketByKodeLayanan($kode_jadwal,$layanan);	
	
	$berat_minimum	=!in_array(substr($layanan,0,1), array("C","M1","M2"))?1:50;

	$berat					= $HTTP_GET_VARS['berat']>=$berat_minimum?$HTTP_GET_VARS['berat']:$berat_minimum;
	
	$harga_paket		= $arr_harga_paket[0]+$arr_harga_paket[1]*($berat-$berat_minimum);
	
	//menghitung diskon
	if($cara_bayar==0){
		//diskon untuk pelanggan biasa
		$diskon	= $harga_paket*$diskon;
	}
	else{
		$data_pelanggan	= $Paket->ambilDataPelanggan($kode_pelanggan);
		
		$diskon					= $data_pelanggan['BesarDiscount']>1?$data_pelanggan['BesarDiscount']:$data_pelanggan['BesarDiscount']*$harga_paket;
	}
	
	$total_bayar		= $harga_paket-$diskon;
	
	$kode_booking 	= generateNoTiketPaket($kode_jadwal);
	$no_spj					= $HTTP_GET_VARS['no_spj'];
	
  $useraktif=$userdata['user_id'];
  
	//memgambil data rute dan jadwal
	$row 					= $Jadwal->ambilDataDetail($kode_jadwal);
	$jam_berangkat= $row['JamBerangkat'];
	$id_jurusan 	= $row['IdJurusan'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	//mengambil 	data header tb posisi 
	$row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_aktual);
	$kode_kendaraan	= $row['KodeKendaraan'];
	$kode_sopir			= $row['KodeSopir'];
	$tgl_cetak_SPJ	= $row['TglCetakSPJ'];
	$petugas_cetak_spj= $row['PetugasCetakSPJ'];
	$cetak_spj			= ($no_spj=="")?0:1;
						
	//mengambil data-data kode account dan komisi
	$row												= $Jurusan->ambilDataDetail($id_jurusan);
	$kode_cabang								= $userdata['KodeCabang'];
	$komisi_paket_CSO						= $row['KomisiPaketCSO'];
	$komisi_paket_sopir					= $row['KomisiPaketSopir'];
	$kode_akun_pendapatan				= $row['KodeAkunPendapatanPaket'];
	$kode_akun_komisi_paket_CSO	= $row['KodeAkunKomisiPaketCSO'];
	$kode_akun_komisi_paket_sopir	= $row['KodeAkunKomisiPaketSopir'];
	
	$Paket->tambah($kode_booking, $kode_cabang, $kode_jadwal,
		$id_jurusan, $kode_kendaraan , $kode_sopir ,
		$tgl, $jam_berangkat , $nama_pengirim ,
		$alamat_pengirim, $telepon_pengirim,
		$nama_penerima, $alamat_penerima, $telepon_penerima,
		$harga_paket,$diskon,$total_bayar,$keterangan,$instruksi_khusus, $userdata['user_id'],
		$komisi_paket_CSO, $komisi_paket_sopir,
		$kode_akun_pendapatan, $kode_akun_komisi_paket_CSO, $kode_akun_komisi_paket_sopir,
		$jumlah_koli,$berat,$jenis_barang,
		$layanan,$cara_bayar,$kode_pelanggan);
	
	echo(1);

exit;

// UBAH PAKET==========================================================================================================================================================================================
case "paketubah":
	//mengubah data tiket yang sudah pernah diinput. operasi ini bersifat konfirmasi
	$no_tiket					= $HTTP_GET_VARS['no_tiket'];   
	$nama_pengirim    = $HTTP_GET_VARS['nama_pengirim'];  
  $alamat_pengirim 	= $HTTP_GET_VARS['alamat_pengirim'];
	$telp_pengirim		= $HTTP_GET_VARS['telp_pengirim'];
	$nama_penerima    = $HTTP_GET_VARS['nama_penerima'];  
  $alamat_penerima	= $HTTP_GET_VARS['alamat_penerima'];
	$telp_penerima		= $HTTP_GET_VARS['telp_penerima'];
	
	$sql	=
		"CALL sp_paket_ubah_data(
			'$no_tiket',
			'$nama_pengirim',
			'$alamat_pengirim',
			'$telp_pengirim',
			'$nama_penerima',
			'$alamat_penerima',
			'$telp_penerima')";	

	if (!$db->sql_query($sql)){
		//die_error('Cannot Save Transaksi',__LINE__,__FILE__,$sql);
		echo("Error :".__LINE__);exit;
	}
	
exit;

//PAKET BATAL ===========================================================================================================================================================================================
case "paketbatal":
	include($adp_root_path . 'ClassPaket.php');
	$Paket			= new Paket();
	
	$no_tiket	= $HTTP_POST_VARS['no_tiket'];
	$username	= $HTTP_POST_VARS['username'];
	$password	= $HTTP_POST_VARS['password'];
	
	//periksa wewenang
	if(in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR))){
		$Paket->pembatalan($no_tiket, $userdata['user_id']);
		echo(1);
	}
	else{
		$User = new User();
		
		$data_user	= $User->ambilDataDetailByUsername($username);
		
		if($data_user['user_password']==md5($password) && $data_user['user_level']<=$LEVEL_SUPERVISOR){
			$Paket->pembatalan($no_tiket, $userdata['user_id']);
			echo(1);
		}
		else{
			echo(0);
		}
	}

exit;

// PAKET DETAIL  ===========================================================================================================================================================================================
case "paketdetail":
  //operasi terhadap paket
  include($adp_root_path . 'ClassPaket.php');
	$Paket			= new Paket();
	
	$no_tiket = $HTTP_GET_VARS['no_tiket'];
	$submode  = $HTTP_GET_VARS['submode'];
	
	if ($userdata['user_level'] == $LEVEL_SCHEDULER){	
		exit;
	}
	    
	$row	= $Paket->ambilDataDetail($no_tiket);
	
	$harga_paket	= number_format($row['HargaPaket'],0,",",".");
	$harga_paket	= substr("________".$harga_paket,strlen($harga_paket)*-1);
	$diskon				= substr("________".number_format($row['Diskon'],0,",","."),strlen($harga_paket)*-1);
	$total_bayar	= substr("________".number_format($row['TotalBayar'],0,",","."),strlen($harga_paket)*-1);
	
	if($submode=='ambil'){
		$output =
			"<input type='hidden' id='dlg_ambil_paket_no_tiket' value='$row[NoTiket]'/>
			<input type='hidden' id='hdn_paket_cara_pembayaran' value='$row[CaraPembayaran]' />
			<table cellspacing=0 cellpadding=0 width='100%' height='100%'>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Pengirim</h2></td></tr>
						<tr><td>Telp Pengirim </td><td>:</td><td>$row[TelpPengirim]</td></tr>
						<tr><td width=200>Nama Pengirim</td><td width=5>:</td><td>$row[NamaPengirim]</td></tr>
						<tr><td>Alamat Pengirim</td><td>:</td><td>$row[AlamatPengirim]</td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Penerima</h2></td></tr>
						<tr><td>Telp Penerima </td><td>:</td><td>$row[TelpPenerima]</td></tr>
						<tr><td width=200>Nama Penerima </td><td width=5>:</td><td>$row[NamaPenerima]</td></tr>
						<tr><td>Alamat Penerima</td><td>:</td><td>$row[AlamatPenerima]</td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Paket</h2></td></tr>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td width='50%'>Jumlah Koli</td><td>:</td><td>".number_format($row['JumlahKoli'],0,",",".")."</td></tr>
						<tr><td>Berat (Kg)</td><td>:</td><td>".number_format($row['Berat'],0,",",".")."&nbsp;Kg.</td></tr>
						<tr><td>Jenis Barang</td><td>:</td><td>$row[JenisBarang]</td></tr>
						<tr><td>Layanan</td><td>:</td><td>".$LIST_JENIS_LAYANAN_PAKET[$row['Layanan']]."</td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td>Harga</td><td>:</td><td>Rp. $harga_paket</td></tr>
						<tr><td>Diskon</td><td>:</td><td>Rp. $diskon</td></tr>
						<tr><td><b>Total Bayar</b></td><td>:</td><td><b>Rp. $total_bayar</b></td></tr>
						<tr><td>Cara Pembayaran</td><td>:</td><td>".$LIST_JENIS_PEMBAYARAN_PAKET[$row['CaraPembayaran']]."</td></tr>
						<tr><td valign='top'>Isi Barang</td><td valign='top'>:</td><td>$row[KeteranganPaket]</td></tr>
						$show_instruksi_khusus
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Pengambil</h2></td></tr>
			<tr>
				<td width='50%' valign='top' colspan=2>
					<table>
						<tr>
							<td width='25%'>Nama Pengambil <span id='dlg_ambil_paket_nama_pengambil_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input type='text' id='dlg_ambil_paket_nama_pengambil' maxlength=20 onFocus='Element.hide(\"dlg_ambil_paket_nama_pengambil_invalid');\" /></td><td width=10></td>
							<td width='25%'>No. KTP <span id='dlg_ambil_paket_no_ktp_pengambil_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input type='text' id='dlg_ambil_paket_no_ktp_pengambil' maxlength=20 onFocus='Element.hide(\"dlg_ambil_paket_no_ktp_pengambil_invalid');\" /></td></tr>
					</table>
				</td>
			</tr>
		</table>
		";
		
		echo($output);
		exit;
	}
	
	$tombol_mutasi="<br><br><input type='button' id='btn_mutasi_paket' onclick='setFlagMutasiPaket();' value='      Mutasi Paket       '>";

	
	if(in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR))){
		$tombol_batal="<input type='button' onclick='batalPaket(\"".$row['NoTiket']."\");' value='&nbsp;&nbsp;Batalkan&nbsp;&nbsp;'>";
	}
	else{
		$tombol_batal="<input type='button' onclick='tampilkanDialogPembatalan();' value='&nbsp;&nbsp;Batalkan&nbsp;&nbsp;'>";
	}
	
	if($row['CetakTiket']==1){
		$keterangan_tombol_cetak_resi	= "    C e t a k    U l a n g   R e s i    ";
		$show_nama_pengirim 		=	$row['NamaPengirim'];
		$show_alamat_pengirim		=	$row['AlamatPengirim'];
		$show_telp_pengirim			=	$row['TelpPengirim'];
		$show_nama_penerima			=	$row['NamaPenerima'];
		$show_alamat_penerima		=	$row['AlamatPenerima'];
		$show_telp_penerima			=	$row['TelpPenerima'];
		$show_tombol_simpan_ubah= "";
	}
	else{
		$keterangan_tombol_cetak_resi	= "          C e t a k    R e s i          ";
		$show_nama_pengirim 		= "<input type='text' id='nama_pengirim' value='$row[NamaPengirim]' />";
		$show_alamat_pengirim		= "<textarea id='alamat_pengirim' rows='4' cols='30'>$row[AlamatPengirim]</textarea>";
		$show_telp_pengirim			= "<input type='text' id='telp_pengirim' value='$row[TelpPengirim]' onkeypress='validasiNoTelp(event);'/>";
		$show_nama_penerima			= "<input type='text' id='nama_penerima' value='$row[NamaPenerima]' />";;
		$show_alamat_penerima		= "<textarea id='alamat_penerima' rows='4' cols='30'>$row[AlamatPenerima]</textarea>";
		$show_telp_penerima			=	"<input type='text' id='telp_penerima' value='$row[TelpPenerima]' onkeypress='validasiNoTelp(event);' />";
		$show_tombol_simpan_ubah= "<tr><td colspan='3' align='center'><br><input type='button' value='Simpan Pengubahan' onClick='ubahPaket();' /></td></tr>";
	}
	
	$show_keterangan_paket	= $row['KeteranganPaket'];
	$show_instruksi_khusus	= $row['InstruksiKhusus']==""?"":"<tr><td valign='top'><font color='red'>Instruksi Khusus</font></td><td valign='top'>:</td><td><font color='red'>$row[InstruksiKhusus]</font></td></tr>";
	
	$tombol_tiket="<input type='button' onclick=\"CetakTiket(0);\" id='btn_cetak_ulang' value='$keterangan_tombol_cetak_resi'>";
	
	if($row['StatusDiambil']==1){
		$output_keterangan_pengambil="
			<tr>
				<td>Waktu Pengambilan</td><td>:</td>
				<td>".FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])."</td>
			</tr>
			<tr>
				<td>Nama Pengambil</td><td>:</td>
				<td>$row[NamaPengambil]</td>
			</tr>
			<tr>
				<td>Petugas Pemberi</td><td>:</td>
				<td>$row[PetugasPemberi]</td>
			</tr>
		";
	}
	
	
	if($row['CaraPembayaran']==1){
		$data_pelanggan	= $Paket->ambilDataPelanggan($row['KodePelanggan']);
		$output_data_pelanggan = "
			<tr><td>Kode Pelanggan</td><td>:</td><td>".$data_pelanggan['KodePelanggan']."</td></tr>
			<tr><td>Nama Pelanggan</td><td>:</td><td>".$data_pelanggan['NamaPelanggan']."</td></tr>
			<tr><td>Contact Person</td><td>:</td><td>".$data_pelanggan['ContactPerson']."</td></tr>
			";
	}
	else{
		$output_data_pelanggan = "";
	}
	
	$output ="
		<table border='0' width='100%'> 
			<tr>
				<td width='40%' colspan='3'><h2>Paket&nbsp;<input type='button' onClick='document.getElementById(\"dataPelanggan\").innerHTML=\"\"' value='&nbsp; Tambah Paket Baru &nbsp;'/></h2></td>
			</tr>
			<tr>
				<td width='40%'>Nomor Resi</td><td width='5%'>:</td>
				<td>
					<input type='hidden' name='flag_paket' id='flag_paket' value='1'>
					<input type='hidden' name='kode_booking' id='kode_booking' value='$row[NoTiket]'>$row[NoTiket]
				</td>
			</tr>
			<tr><td colspan=3><h3>Data Pengirim</h3></td></tr>
			<tr>
				<td>Nama</td><td>:</td>
				<td>$show_nama_pengirim</td>
			</tr>
			<tr>
				<td valign='top'>Alamat</td><td valign='top'>:</td>
				<td>$show_alamat_pengirim</td>
			</tr>
			<tr>
				<td>Telepon</td><td>:</td>
				<td>$show_telp_pengirim</td>
			</tr>
			<tr><td colspan=3><br><h3>Data Penerima</h3></td></tr>
			<tr>
				<td>Nama</td><td>:</td>
				<td>$show_nama_penerima</td>
			</tr>
			<tr>
				<td valign='top'>Alamat</td><td valign='top'>:</td>
				<td>$show_alamat_penerima</td>
			</tr>
			<tr>
				<td>Telepon</td><td>:</td>
				<td>$show_telp_penerima</td>
			</tr>
			$show_tombol_simpan_ubah
			<tr><td bgcolor='red' height=1 colspan=3></td></tr>
			<tr><td colspan=3><br><h3>Data Paket</h3></td></tr>
			<tr>
				<td>Jumlah Koli</td><td>:</td>
				<td>$row[JumlahKoli]</td>
			</tr>
			<tr>
				<td>Berat</td><td>:</td>
				<td>$row[Berat]&nbsp;Kg.</td>
			</tr>
			<tr>
				<td>Jenis Barang</td><td>:</td>
				<td>$row[JenisBarang]</td>
			</tr>
			<tr>
				<td>Layanan</td><td>:</td>
				<td>".$LIST_JENIS_LAYANAN_PAKET[$row['Layanan']]."</td>
			</tr>
			<tr>
				<td>Harga paket</td><td>:</td>
				<td>Rp. $harga_paket</td>
			</tr>
			<tr>
				<td>Diskon</td><td>:</td>
				<td>Rp. $diskon</td>
			</tr>
			<tr>
				<td><b>Total Bayar</b></td><td>:</td>
				<td><b>Rp. $total_bayar</b></td>
			</tr>
			<tr>
				<td>Cara Pembayaran</td><td>:</td>
				<td>".$LIST_JENIS_PEMBAYARAN_PAKET[$row['CaraPembayaran']]."</td>
			</tr>
			$output_data_pelanggan
			<tr>
				<td>Isi Barang</td><td>:</td>
				<td>$show_keterangan_paket</td>
			</tr>
			$show_instruksi_khusus
			<tr>
				<td>CSO</td><td>:</td>
				<td><strong>$row[NamaCSO]</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan='3' align='center'>   
					<br>
					$tombol_tiket
					$tombol_mutasi
					$tombol_batal
				</td>
			</tr>
			<tr><td colspan='3' align='center'><hr color='e0e0e0'></td></tr>
			$output_keterangan_pengambil
		</table>";
		
		echo($output);
	
	exit;
	
break;

//PAKET AMBIL ===========================================================================================================================================================================================
case "paketambil":
	include($adp_root_path . 'ClassPaket.php');
	$Paket			= new Paket();
	
	$no_tiket					= $HTTP_GET_VARS['no_tiket'];
	$nama_pengambil		= $HTTP_GET_VARS['nama_pengambil'];
	$no_ktp_pengambil	= $HTTP_GET_VARS['no_ktp_pengambil'];
	
	if($Paket->updatePaketDiambil($no_tiket,$nama_pengambil,$no_ktp_pengambil,$userdata['user_id'],$userdata['KodeCabang'])){
		echo(1);
	}
	else{
		echo(0);
	}
	
exit;

//PAKET MUTASI ===========================================================================================================================================================================================

case "mutasipaket":
	include($adp_root_path . 'ClassPaket.php');
	
  $tgl     		= $HTTP_GET_VARS['tanggal'];   
  $kode_jadwal= $HTTP_GET_VARS['kode_jadwal'];   
  $no_tiket		= $HTTP_GET_VARS['no_tiket']; 
	$no_spj			= $HTTP_GET_VARS['no_spj'];
	
	$Paket			= new Paket();
	$Jadwal			= new Jadwal();
	$Jurusan		= new Jurusan();
	
	$session_id	= $userdata['session_id'];
  $username		= $userdata['username'];
	
	//memgambil data rute dan jadwal
	$row 					= $Jadwal->ambilDataDetail($kode_jadwal);
	$jam_berangkat= $row['JamBerangkat'];
			
	if(!$Paket->mutasiPaket($no_tiket, $kode_jadwal,$tgl,$jam_berangkat,$username)){
		echo(2);
		exit;
	}
	
	echo(1);
exit;

//WAITING LIST ===========================================================================================================================================================================================
case "waiting_list":
	
  $aksi    		= $HTTP_GET_VARS['aksi'];  // tipe aksi (pesan,book,hapus)     
	$kode_jadwal= $HTTP_GET_VARS['jam'];   // jam  
	
  $useraktif=$userdata['username'];
	
  if(($kode_jadwal!="" && $kode_jadwal!="(none)") || $aksi=='hapus'){
		if($aksi=='tambah'){
				$tgl     		= FormatMySQLDateToTgl($HTTP_GET_VARS['tanggal']);   // tanggal
			  
			  $nama    		= $HTTP_GET_VARS['nama'];  // nama
			  $alamat 				= $HTTP_GET_VARS['alamat']; //alamat
				$telepon 		= $HTTP_GET_VARS['telepon']; // telepon  
			  $hp      		= $HTTP_GET_VARS['hp']; // hp
				$jum_kursi	= $HTTP_GET_VARS['jum_kursi']; // discount		
				
				$sql = "INSERT INTO tbl_waiting_list 
									(tgl_berangkat,kode_rute,
									nama,alamat,telepon,hp,jum_kursi,cso) 
						  	VALUES(
									CONVERT(datetime,'$tgl',104),'$kode_jadwal',
									'$nama','$alamat','$telepon','$hp','$jum_kursi','$useraktif');";
				
				if (!$db->sql_query($sql)){
					//die_error('Cannot Save waiting list',__LINE__,__FILE__,$sql);
					echo("Error :".__LINE__);exit;
				}
		}
		elseif($aksi=='tampilkan'){
			// menampilkan waiting list yang ada sesuai dengan tanggal, rute dan jam yang dimasukan
	          			
		  if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
				//HANYA BISA DIAKSES OLEH SELAIN MEMBER
				
				$tgl  = FormatMySQLDateToTgl($HTTP_GET_VARS['tanggal']); // tanggal    
				$kode_jadwal = $HTTP_GET_VARS['jam'];    // jam
				$ada_data=false;
				
				//LAYOUT WAITING LIST
				echo(
					"<table cellspacing='1' cellpadding='0' border='0' class='whiter' width='100%'>
						<tbody>
						<tr>
							<td bgcolor='D0D0D0' align='center'>
									<strong>WAITING LIST</strong>
							</td>
						</tr>
						<tr>
							<td bgcolor='RED' align='center'>
								<span id='progress_waiting_list' style='display:none;'>
									<font color='WHITE' >Memproses waiting list</font>
								</span>
							</td>
						</tr>");
				
				// transaksi
			  $sql = 
					"SELECT 
						id_waiting_list,nama,alamat,telepon,hp,jum_kursi,cso
					FROM tbl_waiting_list 
					WHERE (CONVERT(CHAR(20), tgl_berangkat, 105)) = '$tgl' 
						AND kode_rute='$kode_jadwal' order by id_waiting_list";
			  
				$idx=1;
			  
				if ($result = $db->sql_query($sql)){
					while ($row = $db->sql_fetchrow($result)){
						$ada_data=true;
						$nama_disingkat=substr($row[1],0,15);
						$no_telepon="$row[3]/$row[4]";
						$id_waiting_list=$row['id_waiting_list'];
			      echo("
							<tr align='left'>
								<td>
									<strong>($idx)</strong> <a href='#' onClick=\"PilihPelanggan('$row[nama]','$row[alamat]','$row[telepon]','$row[hp]');deleteWaitingList('$id_waiting_list');\">N:$nama_disingkat T:$no_telepon<br>Jum.Kursi:$row[jum_kursi]</a>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href='#' OnClick='deleteWaitingList(\"$id_waiting_list\");'><font color='red'>[BATAL]</font></a>
								</td>
							</tr>
							<tr>
								<td height=2 bgcolor='D0D0D0'></td>
							</tr>
						");
						$idx++;
					}	
			  } 
			  else{	  
			    //die_error('Cannot Load data waiting list',__LINE__,__FILE__,$sql);
					echo("Error :".__LINE__);exit;
				}

				if(!$ada_data){
					echo("
						<tr align='center'>
							<td>tidak ada waiting list</td>
						</tr>
					");
				}
				echo("</tbody></table>");
			}
			
		}
		elseif($aksi=='hapus'){
				
				$id_waiting_list= $HTTP_GET_VARS['id_waiting_list'];   // nourut	  									
							
				$sql = "DELETE FROM tbl_waiting_list WHERE id_waiting_list='$id_waiting_list';";
				
				if (!$db->sql_query($sql)){
					//die_error('Cannot delete waiting list',__LINE__,__FILE__,$sql);
					echo("Error :".__LINE__);exit;
				}
		}	
	}


exit;

	
// UBAH TIKET==========================================================================================================================================================================================
case "ubahTiket":
	//mengubah data tiket yang sudah pernah diinput. operasi ini bersifat konfirmasi
	$no_tiket		= $HTTP_GET_VARS['no_tiket'];   // no tiket
	$nama    		= $HTTP_GET_VARS['nama'];  // nama
  $alamat 		= $HTTP_GET_VARS['alamat']; //alamat
	$telepon 		= $HTTP_GET_VARS['telepon']; // telepon  
  $hp      		= $HTTP_GET_VARS['hp']; // hp
	$discount		= $HTTP_GET_VARS['discount']; // discount
	
	$sql	="UPDATE TbReservasi
					SET 
						nama0='$nama',
						alamat0='$alamat',
						telp0='$telepon',
						hp0='$hp',
						discount=$discount,
						total=(pesanKursi*(HargaTiket-$discount))
					WHERE noUrut='$no_tiket'";	

	if (!$db->sql_query($sql)){
		//die_error('Cannot Save Transaksi',__LINE__,__FILE__,$sql);
		echo("Error :".__LINE__);exit;
	}
	
exit;

// KODE PEMBATALAN==========================================================================================================================================================================================
case "kode_pembatalan":
	
	$sandi_pembatalan=new Enkripsi();

	//mengambil jam server
	$sql ="SELECT
		DAY(GETDATE()),
		MONTH(GETDATE()),
		YEAR(GETDATE());";

	if ($result = $db->sql_query($sql)){
		while ($row = $db->sql_fetchrow($result)){
			$kode_batal=$sandi_pembatalan->Encrypt(($row[2]%$row[0])+($row[1]*$row[2]).BulanString($row[0]).(($row[1]*$row[2])%$row[0]));
		}
	}
	else{
		//die_error('gagal');
		echo("Error :".__LINE__);exit;
	}
	
	echo($kode_batal);
	
exit;

// LIST JENIS DISCOUNT=========================================================================================================================	
case 'list_jenis_discount':
		// membuat list jenis discount
		$Promo	= new Promo();
		
		$kode_jadwal 		= $HTTP_GET_VARS['kode_jadwal'];   
		$tgl_berangkat 	= $HTTP_GET_VARS['tgl_berangkat'];   
		$flag_koreksi 	= $HTTP_GET_VARS['flag_koreksi'];   
		$jenis_penumpang= $HTTP_GET_VARS['jenis_penumpang'];   
		
		//MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU JIKA DITEMUKAN ADA PROMO, MAKA DISCOUNT YANG LAIN TIDAK BERLAKU
		$data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl_berangkat);
		
		$target_promo	= $data_discount_point['FlagTargetPromo'];
		
		if($target_promo=='' || $target_promo==1){
			if($flag_koreksi=='' || $flag_koreksi==0){
				$opt = "<SELECT id='opt_jenis_discount'>".setListDiscount($kode_jadwal,$tgl_berangkat)."</select>";
			}
			else{
				$opt = "<SELECT id='opt_jenis_discount_koreksi'>".setListDiscount($kode_jadwal,$tgl_berangkat,$jenis_penumpang)."</select>";
			}
		}
		else{
			$besar_discount		= ($data_discount_point['JumlahDiscount']>1)?"Rp.".number_format($data_discount_point['JumlahDiscount'],0,",","."):$data_discount_point['JumlahDiscount']."%";
			$opt	="<b>Promo Discount $besar_discount<b>";
		}
		
		echo($opt);

	exit;

case 'cari_jadwal_pelanggan':
		// membuat list jenis discount
		$Reservasi	= new Reservasi();
		
		$no_telp	= $HTTP_GET_VARS['no_telp'];
		
		$result	= $Reservasi->cariJadwalKeberangkatan($no_telp);
		
	  if ($result){
		
			$return	= "
				<table>
					<tr>
						<th width=100>Nama</th>
						<th width=100>Tgl.Pergi</th>
						<th width=100>Jam</th>
						<th width=200>Asal</th>
						<th width=200>Tujuan</th>
						<th width=100>Kode Booking</th>
						<th width=100>No.Kursi</th>
						<th width=100>Tiket</th>
						<th width=100>Status</th>
					</tr>";
			
			$idx	= 0;
			while ($row = $db->sql_fetchrow($result)){
	      $idx++;
				$nomor_kursi	="";
				
				$result_nomor_kursi	= $Reservasi->ambilNomorKursi($row['KodeBooking']);
				
				while($data_no_kursi = $db->sql_fetchrow($result_nomor_kursi)){
					$nomor_kursi	.=$data_no_kursi[0].",";
				}
				
				$nomor_kursi	= substr($nomor_kursi,0,-1);
				
				$status_tiket			= ($row['CetakTiket'])?"Dibayar":"Book";
				$status_berangkat	= ($row['CetakSPJ'])?"Berangkat<BR>".dateparse(FormatMySQLDateToTglWithTime($row['TglCetakSPJ'])):"Belum Berangkat";
				
				$return	.="
					<tr bgcolor='dfdfdf'>
						<td>$row[Nama]</td>
						<td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
						<td align='center'>$row[JamBerangkat]</td>
						<td>$row[Asal]</td>
						<td>$row[Tujuan]</td>
						<td>$row[KodeBooking]</td>
						<td align='center'>$nomor_kursi</td>
						<td align='center'>$status_tiket</td>
						<td align='center'>$status_berangkat</td>
					</tr>";
	    } 
	  } 
		else{ 
			echo("Err :".__LINE__);exit;
		}
		
		if($idx==0){
			$return .="<tr><td colspan=9 class='banner' align='center'><h2>Tidak ada keberangkatan ditemukan!</h2></td></tr>";
		}
		
		echo($return."</table>");

	exit;
	
	case 'cari_paket':
		// membuat list jenis discount
		$Reservasi	= new Reservasi();
		
		$no_resi	= $HTTP_GET_VARS['no_resi'];
		
		$result	= $Reservasi->cariPaket($no_resi);
		
	  if ($result){
		
			$return	= "
				<table>
					<tr>
						<th width=100>Tgl.Pergi</th>
						<th width=150>Asal</th>
						<th width=150>Tujuan</th>
						<th width=70>Jam</th>
						<th width=100>Id Kendaraan</th>
						<th width=100>Sopir</th>
						<th width=100>Kode Booking</th>
						<th width=150>Pengirim</th>
						<th width=150>Penerima</th>
						<th width=150>Pengambil</th>
					</tr>";
			
			$jum_data	= 0;
			
			while ($row = $db->sql_fetchrow($result)){
				
				if($row['StatusDiambil']==1){
					$output_pengambilan	="
					<div valign='top'>
						Nama	: $row[NamaPengambil]<br>
						KTP		: $row[NoKTPPengambil]<br>
						Waktu	: ".FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])."
						CSO		: $row[NamaPetugasPemberi]
					</div>
					";
				}
				else{
					$output_pengambilan	="
						<div align='center'><a href='' onClick='ambilDataPaket(\"$row[NoTiket]\");return false;'>Ambil Paket</a></div>
					";
				}
				
				$return	.="
					<tr bgcolor='dfdfdf'>
						<td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
						<td>$row[Asal]</td>
						<td>$row[Tujuan]</td>
						<td align='center'>$row[JamBerangkat]</td>
						<td align='left'>$row[KodeKendaraan] ($row[NoPolisi])</td>
						<td align='left'>$row[NamaSopir]</td>
						<td>$row[NoTiket]</td>
						<td valign='top'>
							Nama: $row[NamaPengirim]<br>
							Alamat: $row[AlamatPengirim]<br>
							Telp: $row[TelpPengirim]
						</td>
						<td valign='top'>
							Nama: $row[NamaPenerima]<br>
							Alamat: $row[AlamatPenerima]<br>
							Telp: $row[TelpPenerima]
						</td>
						<td>
							$output_pengambilan
						</td>
					</tr>";
				
				$jum_data++;
	    } 
	  } 
		else{ 
			echo("Err :".__LINE__);exit;
		}
		
		if($jum_data==0){
			$return .="<tr><td colspan=11 class='banner' align='center'><h2>Data paket tidak ditemukan!</h2></td></tr>";
		}
		
		echo($return."</table>");

	exit;
	
	// UBAH FLAG BACA MEMO ==========================================================================================================================================================================================
case "updateflagmemo":
	
	$tgl 					= $HTTP_GET_VARS['tanggal'];
	$kode_jadwal 	= $HTTP_GET_VARS['kode_jadwal'];
	
	$sql = 
		"UPDATE tbl_posisi SET FlagMemo=0
		WHERE TglBerangkat = '$tgl' AND 
			KodeJadwal='$kode_jadwal';";
				
	if (!$db->sql_query($sql)){
		die_error("Err $this->ID_FILE".__LINE__);
		echo(0);
		exit;
	}
	
	echo(1);
	
	exit;
	break;
	
case "updatestatuscetaktiket":
	
	$Reservasi	= new Reservasi();
	
	$cetak_tiket				= $HTTP_GET_VARS['cetak_tiket'];
	$list_no_tiket			= str_replace("\'","'",$HTTP_GET_VARS['list_no_tiket']);
	$kode_jadwal				= $HTTP_GET_VARS['kode_jadwal'];
	$tanggal						= $HTTP_GET_VARS['tanggal'];
	$jenis_pembayaran		= $HTTP_GET_VARS['jenis_pembayaran'];
	$list_kode_booking	= str_replace("\'","'",$HTTP_GET_VARS['list_kode_booking']);
	
	if($cetak_tiket!=1){
		//UPDATE FLAG TIKET DI LAYOUT KURSI
		//mengambil jadwal utama
		$sql	=
			"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal';";
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
			
		$row = $db->sql_fetchrow($result);
		$kode_jadwal_utama	= $row[0];
		
		$sql = 
			"UPDATE tbl_posisi_detail SET StatusBayar=1
			WHERE 
			  KodeJadwal='$kode_jadwal_utama' 
				AND TGLBerangkat='$tanggal'
				AND NoTiket IN ($list_no_tiket);";
		
		if(!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
		
		//mengupate flag cetak tiket
	
		$list_kode_booking	= substr($list_kode_booking,0,-1);
		$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
	}

	exit;
//=====================================================================================================================
	case 'sisa_kursi_next':
		//MENGAMBIL DAN MENAMPILKAN SISA KURSI UNTUK BEBERAPA JAM KEDEPAN
		$id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];
		$tgl 						= $HTTP_GET_VARS['tgl_berangkat'];
		$jam_berangkat 	= $HTTP_GET_VARS['jam_berangkat'];
		
		$Jadwal	= new Jadwal();
		
		$result	= $Jadwal->setSisaKursiJadwalNext($tgl,$id_jurusan,$jam_berangkat);
		
	  echo $result;

	exit;
	
//======================================================================================================================	
	case 'ambil_list_harga_paket':
		//Mengambil daftar harga paket
		include($adp_root_path .'/ClassPaket.php');
		$id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];
		
		$Paket= new Paket();
		
		$result= $Paket->ambilDaftarHarga($id_jurusan);
		
		if ($result){
		  $row = $db->sql_fetchrow($result);
			
			//return eval
			echo("
				document.getElementById('dlg_paket_harga_kg_pertama_p').value = '$row[HargaPaketPKiloPertama]';
				document.getElementById('dlg_paket_harga_kg_pertama_ga').value = '$row[HargaPaketGAKiloPertama]';
				document.getElementById('dlg_paket_harga_kg_pertama_gd').value = '$row[HargaPaketGDKiloPertama]';
				document.getElementById('dlg_paket_harga_kg_pertama_s').value = '$row[HargaPaketSKiloPertama]';
				document.getElementById('dlg_paket_harga_kg_pertama_ca').value = '$row[HargaPaketCAKiloPertama]';
				document.getElementById('dlg_paket_harga_kg_pertama_cd').value = '$row[HargaPaketCDKiloPertama]';
				document.getElementById('dlg_paket_harga_kg_berikutnya_p').value = '$row[HargaPaketPKiloBerikut]';
				document.getElementById('dlg_paket_harga_kg_berikutnya_ga').value = '$row[HargaPaketGAKiloBerikut]';
				document.getElementById('dlg_paket_harga_kg_berikutnya_gd').value = '$row[HargaPaketGDKiloBerikut]';
				document.getElementById('dlg_paket_harga_kg_berikutnya_s').value = '$row[HargaPaketSKiloBerikut]';
				document.getElementById('dlg_paket_harga_kg_berikutnya_ca').value = '$row[HargaPaketCAKiloBerikut]';
				document.getElementById('dlg_paket_harga_kg_berikutnya_cd').value = '$row[HargaPaketCDKiloBerikut]';
				document.getElementById('dlg_paket_harga_motor1').value = '$row[HargaPaketM1]';
				document.getElementById('dlg_paket_harga_motor2').value = '$row[HargaPaketM2]';
			");
		 
		} 
		else{      
			echo("Error ".__LINE__);
		}
		
	exit;
	
//======================================================================================================================
	case 'bayar_by_voucher':
		
		$Promo			= new Promo();
		$Reservasi	= new Reservasi();
		
		$kode_voucher	= $HTTP_POST_VARS['kode_voucher'];
		$tgl_berangkat= $HTTP_POST_VARS['tglberangkat'];
		$id_jurusan		= $HTTP_POST_VARS['id_jurusan'];
		$no_tiket			= $HTTP_POST_VARS['no_tiket'];
		
		if(strtoupper(substr($kode_voucher,0,2))!="VC"){
			//VOUCHER RETURN
			$return_val	= $Promo->verifyVoucher($kode_voucher,$id_jurusan,$no_tiket);
				
			if(!$return_val){
				echo("alert('Kode Voucher yang anda masukkan TIDAK VALID, atau masa berlakunya sudah habis.');");
				
				exit;
			}

      if($return_val['status']){
        echo("CetakTiket(3);dialog_voucher.hide();");
      }
      else{
        switch($return_val['error']){
          case "INVALID_VOUCHER":
            echo("alert('Kode voucher yang dimasukkan SALAH!');");
            break;

          case "EXPIRED":
            echo("alert('Voucher sudah EXPIRED');");
            break;

          case "TERPAKAI":
            echo("alert('Voucher anda sudah DIGUNAKAN penumpang lain!');");
            break;

          case "INVALID_JURUSAN":
            echo("alert('Voucher tidak dapat digunakan untuk jurusan ini!');");
            break;

          case "RETURN":
            echo("alert('voucher ini tidak dapat digunakan untuk membayar penumpang return!');");
            break;
        }
      }
		}
		else{
			//VOUCHER BIASA
			$Voucher	= new Voucher();
			
			$return_val	= $Voucher->verifyVoucher($kode_voucher,$tgl_berangkat);
			
			if($return_val['status']){
				$data_voucher	= $Voucher->getDataVoucher($kode_voucher);
				
				$jenis_pembayaran = $data_voucher["IsSettlement"]==1?4:5;
				
				echo("CetakTiket($jenis_pembayaran);dialog_voucher.hide();");
			}
			else{
				switch($return_val['error']){
					case "GROUP_BLOCKED":
						echo("alert('Voucher tidak dapat digunakan dikarenakan korporat anda sedang di blokir!');");
						break;
					
					case "INVALID":
						echo("alert('Kode Voucher yang anda masukkan TIDAK VALID!');");
						break;
					
					case "EXPIRED":
						echo("alert('Voucher anda sudah EXPIRED!');");
						break;
					
					case "INVALID_DAY":
						echo("alert('Voucher tidak dapat digunakan untuk hari keberangkatan yang anda inginkan!');");
						break;

          case "SUSPEND":
            echo("alert('Voucher pada BATCH ini sedang DI-SUSPEND, hubungi Supervisor anda!');");
            break;
				}
			}
		}
		
	exit;
		
}



/*if($user_level!=$LEVEL_SCHEDULER){
	//jika  bukan member, dapat memilih tipe (penumpang/paket)
	
	$tombol_cetak_spj=
		"<a href='#' onclick='setDialogSPJ();'><img src='templates/images/icon_cetak_spj.gif'></a>
		 </br>
		 <a href='#' onclick='setDialogSPJ();'><span class='genmed'>Cetak Manifest</span></a>";
	
}
else{
	//jika   member, tidak dapat memilih tipe (penumpang/paket)
		$select_tipe	=	"<input name='ftipe' id='ftipe' type='hidden' value='penumpang'>Penumpang";
		$kolom_discount="<td colspan='2'></td>";
		$tombol_cetak_spj="";
}*/
	
include($adp_root_path .'/ClassPengaturanUmum.php');

$PengaturanUmum	= new PengaturanUmum();      

$template->assign_vars (
	array(
		'BCRUMP'    					=> '<a href="'.append_sid('main.'.$phpEx) .'">Home',
		'TGL_SEKARANG'				=> dateY_M_D(),
		'OPT_KOTA'						=> setComboKota("BANDUNG"),
		'U_PAKET'							=> append_sid('reservasi.'.$phpEx.'?expd=1'),
		'U_RESERVASI'					=> append_sid('reservasi.'.$phpEx.''),
		'U_CHECKIN_PAKET'			=> "Start('".append_sid('paket.checkin.'.$phpEx.'')."');",
		'U_LAPORAN_UANG'			=> "Start('".append_sid('laporan_rekap_uang_user.'.$phpEx.'')."');",
		'U_LAPORAN_PENJUALAN'	=> "Start('".append_sid('laporan_penjualan_user.'.$phpEx.'')."');",
		'U_UBAH_PASSWORD'			=> append_sid('ubah_password.'.$phpEx.''),
		'ASURANSI_TGL_LAHIR'	=> date("d")."-".date("m")."-".(date("Y")-20),
		)
	);
	

if($userdata['user_level']!=$LEVEL_CSO_PAKET && $HTTP_GET_VARS['expd']!='1'){	
	
	//LOAD ASURANSI
	$Asuransi	= new Asuransi();
	
	$rec_asuransi	= $Asuransi->ambilData("NamaPlan","ASC");
	
	while($row=$db->sql_fetchrow($rec_asuransi)){
		$template->assign_block_vars(
			"OPT_ASURANSI",array(
				"value" 			=> $row["IdPlanAsuransi"],
				"keterangan" 	=> $row["NamaPlan"]." | Premi: Rp.".number_format($row["BesarPremi"],0,",","."),
				"title" 			=> $row["Keterangan"]));
	}
		
	$template->set_filenames(array('body' => 'reservasi_body.tpl')); 
}
else{
	$template->set_filenames(array('body' => 'reservasi_paket_body.tpl')); 
}

$template->assign_vars(array('DEPOSIT' => $deposit));

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>