<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

// LIST
$template->set_filenames(array('body' => 'paket.pelanggan/index.tpl')); 

$cari		= $HTTP_POST_VARS["txt_cari"]!=""?$HTTP_POST_VARS["txt_cari"]:$HTTP_GET_VARS["cari"];
$status	= $HTTP_POST_VARS["status"]!=""?$HTTP_POST_VARS["status"]:$HTTP_GET_VARS["status"];

$kondisi = 
		"(KodePelanggan LIKE '%$cari' 
		OR NamaPelanggan LIKE '%$cari%' 
		OR AlamatPelanggan LIKE '%$cari%'
		OR ContactPerson LIKE '%$cari%'
		OR TelpCP LIKE '%$cari%'
		OR tu.nama LIKE '%$cari%')";

$kondisi	.= $status==""?"":($status=="0"?" AND DATEDIFF(TglHabisKontrak,NOW())>0":" AND DATEDIFF(TglHabisKontrak,NOW())<=0");
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,
	"KodePelanggan","tbl_paket_pelanggan tpp LEFT JOIN tbl_user tu ON tpp.DidaftarkanOleh=tu.user_id",
	"cari=$cari&status=$status&sort_by=$sort_by&order=$order","WHERE 1 AND $kondisi","paket.pelanggan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"NamaPelanggan":$sort_by;

$sql = 
	"SELECT tpp.*,IF(DATEDIFF(TglHabisKontrak,NOW())>0,'OK','HABIS KONTRAK') AS StatusKontrak,tu.nama AS NamaUser
	FROM tbl_paket_pelanggan tpp LEFT JOIN tbl_user tu ON tpp.DidaftarkanOleh=tu.user_id
	WHERE 1 AND $kondisi 
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if(!$result = $db->sql_query($sql)){
	//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['StatusKontrak']=="OK"){
			$show_kode_pelanggan	= "<a href='#' onClick=\"pilihData('".$row['KodePelanggan']."');\" />". $row['KodePelanggan']."</a>";
	}
	else{
		$odd	= "red";
		$show_kode_pelanggan	= $row['KodePelanggan'];
	}
	
	$act 	="<a href='#' onClick=\"showDetailData('$row[0]');\">Edit</a> + ";
	$act .="<a  href='#' onclick='hapusData(\"$row[0]\");'>Delete</a>";
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$row['IsAktif']==1?$odd:"red",
				'no'=>$i,
				'kodepelanggan'=>$row['KodePelanggan'],
				'namapelanggan'=>$row['NamaPelanggan'],
				'alamat'=>$row['AlamatPelanggan'],
				'contactperson'=>$row['ContactPerson'],
				'telpcp'=>$row['TelpCP'],
				'discount'=>($row['BesarDiscount']>1)?"Rp.".number_format($row['BesarDiscount'],0,",","."):(($row['BesarDiscount']*100)."%"),
				'habiskontrak'=>FormatMySQLDateToTgl($row['TglHabisKontrak']),
				'waktudaftar'=>FormatMySQLDateToTglWithTime($row['WaktuCatatDaftar']),
				'didaftarkanoleh'=>$row['NamaUser'],
				'aktif'=>"<a href='#' onClick=\"ubahStatus('".$row['KodePelanggan']."');\">".($row['IsAktif']==1?"Aktif":"Nonaktif")."</a>",
				'status'=>$row['StatusKontrak'],
				'action'=>$act
			)
		);
	
	$i++;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&cari=$cari&status=$status&order=$order_invert";

$array_sort	= 
	"'".append_sid('paket.pelanggan.php?sort_by=KodePelanggan'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=NamaPelanggan'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=AlamatPelanggan'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=ContactPerson'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=TelpCP'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=BesarDiscount'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=TglHabisKontrak'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=WaktuCatatDaftar'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=NamaUser'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=StatusKontrak'.$parameter_sorting)."',".
	"'".append_sid('paket.pelanggan.php?sort_by=IsAktif'.$parameter_sorting)."'";
	
$temp_var	= "opt_status_sel".$status;
$$temp_var= "selected";

$template->assign_vars(array(
	'BCRUMP'    				=> '<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('paket.pelanggan.'.$phpEx).'">Pelanggan Paket</a>',
	'ACTION_CARI'				=> append_sid('paket.pelanggan.'.$phpEx),
	'DISPLAY_NO_DATA'		=> $i-1>0?"none":"block",
	'OPT_STATUS_SEL'		=> $opt_status_sel,
	'OPT_STATUS_SEL0'		=> $opt_status_sel0,
	'OPT_STATUS_SEL1'		=> $opt_status_sel1,
	'TGL_HABIS_KONTRAK'	=> date('d-m-Y',strtotime(date("Y-m-d") . "+365 days")),
	'TXT_CARI'					=> $cari,
	'ARRAY_SORT'				=> $array_sort,
	'PAGING'						=> $paging
	)
);
		    
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>