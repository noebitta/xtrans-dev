CREATE TABLE `NewTable` (
  `id_biaya`  int(10) NOT NULL AUTO_INCREMENT ,
  `id_user`  int(10) NOT NULL ,
  `KodeCabang`  varchar(25) NOT NULL ,
  `Jumlah`  int(255) NOT NULL ,
  `TglBiaya`  datetime NOT NULL ,
  `Keterangan`  text NULL ,
  `JenisPengeluaran`  varchar(255) NOT NULL ,
  PRIMARY KEY (`id_biaya`)
)
;
