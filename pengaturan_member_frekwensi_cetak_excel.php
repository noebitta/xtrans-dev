<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_CCARE))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//memberikan akses memory yang tak terbatas mengingat proses ini membutuhkan memory yang cukup besar
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari  					= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$username				= $userdata['username'];
//set_time_limit(300); 

//INISIALISASI
	$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
	$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
	
	$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";
	
	$kondisi	=($cari=="")?"WHERE 1 ":
			" WHERE (IdMember LIKE '%$cari%' 
				OR Nama LIKE '%$cari%' 
				OR Alamat LIKE '%$cari%' 
				OR Handphone LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR Email LIKE '%$cari%'
				OR Pekerjaan LIKE '%$cari%')";
	
	$sql = 
			"SELECT IdMember,TglLahir,Nama,Alamat,Telp,Handphone,Email,Pekerjaan,FlagAktif,f_member_hitung_frekwensi_by_tanggal(IdMember,'$tanggal_mulai_mysql','$tanggal_akhir_mysql') AS Frekwensi
			FROM tbl_md_member $kondisi 
			$kondisi_sort";
						
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();        
		
		$objPHPExcel->setActiveSheetIndex(0);  
		$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		
		$sheet++;
		$idx_row=4;
		
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Frekwensi Keberangkatan Member dari Tanggal '.dateparse($tanggal_mulai)." s/d ".dateparse($tanggal_akhir));
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'ID Member');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Alamat');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Telp');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Email');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Pekerjaan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Frekwensi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Tgl.Lahir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Tgl.Expired');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Cabang Daftar');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Status');
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		
		$idx=0;
		
		while ($row = $db->sql_fetchrow($result)){
			
			$idx++;
			
			//echo($row['Nama']." ->".$data_frekwensi[$row['IdMember']]."<br>");
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['IdMember']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Alamat']." ".$row['Kota']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['Handphone']."/".$row['Telp']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Email']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Pekerjaan']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['Frekwensi']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, dateparse(FormatTglToMySQLDate($row['TglLahir'])));
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, dateparse(FormatTglToMySQLDate($row['ExpiredDate'])));
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['CabangDaftar']);
			
			if($row['FlagAktif']){
				$status="Aktif";
			}
			else{
				$status="Tidak Aktif";
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $status);
			
			$idx_row++;
		
		}
		$temp_idx=$idx_row;
		
		$idx_row++;		
		
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Frekwensi Keberangkatan Member dari Tanggal '.dateparse($tanggal_mulai)." s/d ".dateparse($tanggal_akhir).'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
?>
