<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_KASIR,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$jenis_biaya  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$cabang					= isset($HTTP_GET_VARS['p7'])? $HTTP_GET_VARS['p7'] : $HTTP_POST_VARS['p7'];

//INISIALISASI
$kondisi	= "WHERE (TglTransaksi BETWEEN CONVERT(datetime,'$tanggal_mulai',105) AND CONVERT(datetime,'$tanggal_akhir',105))";

$kondisi_cabang	=($cabang==0 || $cabang=="")?"":
	" AND KodeCabang = '$cabang' ";

$kondisi_jb	=($jenis_biaya=="")?"":
	" AND FlagJenisBiaya = $jenis_biaya ";

$kondisi_cari	=($cari=="")?"":
	" AND (NoSPJ LIKE '%$cari'
		OR NoPolisi LIKE '%$cari%')";
	
$kondisi	= $kondisi.$kondisi_cabang.$kondisi_jb.$kondisi_cari;

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"TglPesan,JamPesan":$sort_by;

		
//QUERY
$sql=
	"SELECT 
		IdBiayaOP,NoSPJ,FlagJenisBiaya,NoPolisi,dbo.f_SopirGetNamaByKode(KodeSopir) AS NamaSopir,Jumlah,
		dbo.f_KodeVoucherGetByNoSPJ(NoSPJ) AS KodeVoucher,
		CONVERT(varchar(12),TglTransaksi,106) as TglTransaksi,
		dbo.f_CabangGetNameByKode(KodeCabang) AS Cabang,
		dbo.f_UserGetNameById(IdPetugas) AS Kasir,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangAsalByIdJurusan(IdJurusan)) Asal,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangTujuanByIdJurusan(IdJurusan)) Tujuan
		
	FROM 
		TbBiayaOP
	$kondisi
	ORDER BY TglTransaksi,NoSPJ,IdBiayaOP";	
	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,80);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Biaya','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(30,5,'Tgl.Trx','B',0,'C',1);
$pdf->Cell(30,5,'No.SPJ','B',0,'C',1);
$pdf->Cell(30,5,'No.Pol','B',0,'C',1);
$pdf->Cell(30,5,'Sopir.','B',0,'C',1);
$pdf->Cell(50,5,'Jurusan','B',0,'C',1);
$pdf->Cell(30,5,'Jenis Biaya.','B',0,'C',1);
$pdf->Cell(20,5,'Jumlah','B',0,'C',1);
$pdf->Cell(30,5,'Cabang','B',0,'C',1);
$pdf->Cell(30,5,'Kasir','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
	
	$total_biaya	=array(0,0,0,0,0);
	
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$kode_voucher	=($row['FlagJenisBiaya']!=$LIST_KODE_BIAYA['bbm'])?"":"(".$row['KodeVoucher'].")";
		
		$pdf->Cell(5,5,$i,'',0,'C');
		$pdf->Cell(30,5,$row['TglTransaksi'],'',0,'L');
		$pdf->MultiCell2(30,5,$row['NoSPJ'],'','L');
		$pdf->Cell(30,5,$row['NoPolisi'],'',0,'L');
		$pdf->MultiCell2(30,5,$row['NamaSopir'],'','L');
		$pdf->MultiCell2(50,5,$row['Asal']."-".$row['Tujuan'],'','L');
		$pdf->MultiCell2(30,5,$LIST_BIAYA[$row['FlagJenisBiaya']].$kode_voucher,'','L');
		$pdf->Cell(20,5,number_format($row['Jumlah'],0,",",".")." ",'',0,'R');
		$pdf->MultiCell2(30,5,$row['Cabang'],'','L');
		$pdf->MultiCell2(30,5,$row['Kasir'],'','L');
		$pdf->Ln(0);
		$pdf->Cell(285,1,'','B',0,'');
		$pdf->Ln();
		
		$total_biaya[$row['FlagJenisBiaya']] +=$row['Jumlah'];
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
} 

$pdf->Ln();	
$pdf->Cell(50,5,"Total Biaya Jasa Sopir",'',0,'L');$pdf->Cell(5,5,":",'',0,'C');
$pdf->Cell(20,5,number_format($total_biaya[0],0,",",".")." ",'',0,'R');
$pdf->Ln();				
$pdf->Cell(50,5,"Total Biaya Tol",'',0,'L');$pdf->Cell(5,5,":",'',0,'C');
$pdf->Cell(20,5,number_format($total_biaya[1],0,",",".")." ",'',0,'R');
$pdf->Ln();
$pdf->Cell(50,5,"Total Biaya Parkir",'',0,'L');$pdf->Cell(5,5,":",'',0,'C');
$pdf->Cell(20,5,number_format($total_biaya[4],0,",",".")." ",'',0,'R');
$pdf->Ln();								
$pdf->Cell(50,5,"Total Fee Driver",'',0,'L');$pdf->Cell(5,5,":",'',0,'C');
$pdf->Cell(20,5,number_format($total_biaya[2],0,",",".")." ",'',0,'R');
$pdf->Ln();									
$pdf->Cell(50,5,"Total BBM",'',0,'L');$pdf->Cell(5,5,":",'',0,'C');
$pdf->Cell(20,5,number_format($total_biaya[3],0,",",".")." ",'',0,'R');										
$pdf->Ln();
										
$pdf->Output();
						
?>