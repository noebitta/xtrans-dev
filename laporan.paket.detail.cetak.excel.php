<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN_PAKET,$LEVEL_SUPERVISOR,$LEVEL_SUPERVISOR_PAKET))){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$cari           = isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$param_filter  	= isset($HTTP_GET_VARS['paramfilter'])? $HTTP_GET_VARS['paramfilter'] : $HTTP_POST_VARS['paramfilter'];

// LIST
$template->set_filenames(array('body' => 'laporan.paket/paket.detail.tpl'));

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= "AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$param_filter'";

$order	=($order=='')?"ASC":$order;

$sort_by =($sort_by=='')?"KodeCabangAsal,KodeCabangTujuan,TglBerangkat,JamBerangkat":$sort_by;

$sql	=
    "SELECT *,
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabangAsal,
		f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) AS KodeCabangTujuan
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi
	ORDER BY $sort_by $order";

if ($result = $db->sql_query($sql)){

}else{
    echo("Err:".__LINE__);exit;
}

//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:P2');
$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
$objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
$objPHPExcel->getActiveSheet()->mergeCells('D3:D4');
$objPHPExcel->getActiveSheet()->mergeCells('E3:G3');
$objPHPExcel->getActiveSheet()->mergeCells('H3:J3');
$objPHPExcel->getActiveSheet()->mergeCells('K3:K4');
$objPHPExcel->getActiveSheet()->mergeCells('L3:L4');
$objPHPExcel->getActiveSheet()->mergeCells('M3:M4');
$objPHPExcel->getActiveSheet()->mergeCells('N3:N4');
$objPHPExcel->getActiveSheet()->mergeCells('O3:O4');
$objPHPExcel->getActiveSheet()->mergeCells('P3:P4');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Detail Paket Cabang '.$param_filter.' Periode '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'No Resi');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Waktu Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Pengirim');
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Nama');
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Alamat');
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Penerima');
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Nama');
$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Alamat');
$objPHPExcel->getActiveSheet()->setCellValue('J4', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Berat (Kg)');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Harga');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Diskon');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Bayar');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Layanan');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'Jenis Bayar');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

$idx=0;
$idx_row=5;

while ($row = $db->sql_fetchrow($result)){

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoTiket']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeJadwal']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NamaPengirim']);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['AlamatPengirim']);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['TelpPengirim']);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NamaPenerima']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['AlamatPenerima']);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['TelpPenerima']);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Berat']);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($row['HargaPaket']));
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($row['Diskon']));
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($row['TotalBayar']));
    $objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $row['Layanan']);
    $objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $row['JenisPembayaran']==0?"TUNAI":"LANGGANAN");

    $idx_row++;
    $idx++;
}

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

if ($idx>0){

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Detail Paket Cabang '.$param_filter.' per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

}
else{
    echo("Tidak ada data yang akan diexport!");
}
