<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassStatistik.php');


// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern

if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_SCHEDULER)))
{ 
  redirect('index.'.$phpEx,true); 
}

$mode 		= isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode 	= isset($HTTP_GET_VARS['submode'])?$HTTP_GET_VARS['submode']:$HTTP_POST_VARS['submode'];


		$sql= "SELECT KodeJadwal, COUNT( NoTiket ) As JumlahKeberangkatan
		FROM tbl_reservasi
		where CetakTiket = 1
		GROUP BY KodeJadwal
		ORDER BY COUNT( NoTiket ) DESC ";

		$template->set_filenames(array('body' => 'statistik/statistik_body.tpl'));
				
		if($result=$db->sql_query($sql))
		{
			$no=0;
			while($row = $db->sql_fetchrow($result))
			{$no++;
				$template->assign_block_vars(
					'ROW',array(
						'no'					=> $no,
						'jumlahkeberangkatan'	=> $row['JumlahKeberangkatan'],
						'kodejadwal'			=> $row['KodeJadwal'],
						
				));
			}
		}
		
	
	


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>