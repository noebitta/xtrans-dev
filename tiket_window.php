<?php
//
// CETAK TIKET UNTUK OS WINDOWS
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$layout_kursi		= $HTTP_GET_VARS['layout_kursi'];

switch ($mode){
	case 'pilih_tiket':
		
		$kode_jadwal    	= $HTTP_GET_VARS['kode_jadwal'];
		$tgl_berangkat    = FormatMySQLDateToTgl($HTTP_GET_VARS['tgl_berangkat']);
		
		//mengambil data tiket
		$sql = 
			"SELECT 
				NoUrut,Nama0,Alamat0,Telp0,HP0,HargaTiket,TOTAL,NomorKursi
			FROM TbReservasi
			WHERE (CONVERT(CHAR(20), TGLBerangkat, 105)) = '$tgl_berangkat' 
			AND kodejadwal='$kode_jadwal' 
			AND CetakTiket=0 
			ORDER BY Nama0";
		
		if (!$result = $db->sql_query($sql)){
			die_error('Cannot Load data',__LINE__,__FILE__,$sql);
		} 
		
		$list_tiket=
			"<table>
				<tr bgcolor='D0D0D0'>
					<td width='2%' align='center'>&nbsp;</td>
					<td width='3%' align='center'>#</td>
					<td width='10%' align='center'>No.Tiket</td>
					<td width='15%' align='center'>Nama</td>
					<td width='15%' align='center'>Alamat</td>
					<td width='10%' align='center'>Telp.</td>
					<td width='10%' align='center'>HP</td>
					<td width='10%' align='center'>Harga Tiket</td>
					<td width='10%' align='center'>Total</td>
					<td width='10%' align='center'>No.Kursi</td>
				</tr>
			";
		$i=1;
		while ($row = $db->sql_fetchrow($result)){
			
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$list_tiket	.=
					"<tr class='$odd'>
						<td>
							<input type='hidden' value='$layout_kursi' name='hide_layout_kursi' id='hide_layout_kursi'></input>
							<input type='checkbox' checked id='chk_tiket_$i' value=\"'$row[NoUrut]'\"	></input>
						</td>
						<td>
							$i
						</td>
						<td>
							$row[NoUrut]
						</td>
						<td>
							$row[Nama0]
						</td>
						<td>
							$row[Alamat0]
						</td>
						<td>
							$row[Telp0]
						</td>
						<td>
							$row[HP0]
						</td>
						<td align='right'>".
							number_format($row[HargaTiket],0,",",".")
						."</td>
						<td align='right'>".
							number_format($row[TOTAL],0,",",".")
						."</td>
						<td>
							$row[NomorKursi]
						</td>
					</tr>";
			$i++;
		}
		
		$list_tiket .="</table>";
		include($adp_root_path . 'includes/page_header.php');
		$template->set_filenames(array('body' => 'tiket_body.tpl')); 
		$template->assign_vars (
			array(
				'RUTE'						=> $kode_jadwal,
				'TGL_BERANGKAT'		=> $tgl_berangkat,
				'LIST_TIKET'			=> $list_tiket
			)
		);
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
	break;
	
	case 'cetak_tiket':
		$list_tiket			= $HTTP_GET_VARS['tiket_dipilih'];		
		
		$list_tiket=str_replace("\'","'",$list_tiket=$HTTP_GET_VARS['tiket_dipilih']);
		
		//NoUrut='$no_tiket'
		if ($list_tiket!=''){
				
				//mengambil data tiket
				$sql = "SELECT 
									NoUrut,Nama0,Alamat0,Telp0,HP0,TGLBerangkat,JamBerangkat,Asal,Tujuan,
									HargaTiket,TOTAL,NomorKursi,KodeJadwal,operator,discount,idMember
								FROM TbReservasi
								WHERE NoUrut IN($list_tiket)";
				
				if (!$result = $db->sql_query($sql)){
					die_error('GAGAL MENGAMBIL DATA');//,__LINE__,__FILE__,$sql);
			  } 
				else{
					$cso=$userdata['nama'];
					
					//EXPORT KE PDF
					class PDF extends FPDF {
						function Footer() {
							$this->SetY(-1.5);
			        $this->SetFont('Arial','I',8);
			        $this->Cell(0,1,'',0,0,'R');
			        }
			     }
					
					//set kertas & file
					$pdf=new PDF('P','cm','tiket');
					$pdf->Open();
					$pdf->AliasNbPages();
					//$pdf->AddPage();
					$pdf->Setmargins(1,0,0,0);
					$pdf->SetFont('courier','',10);
					$i=0;
					
					$list_field_bayar="";
					
					while ($row = $db->sql_fetchrow($result)){
				
						$no_tiket=$row['NoUrut'];
						$nama=$row['Nama0'];
						$alamat=$row['Alamat0'];
						$telp=$row['Telp0']."/".$row['HP0'];
						$temp_tanggal=$row['TGLBerangkat'];
						$tanggal=substr($temp_tanggal,0,11);
						$jam=substr($row['JamBerangkat'],11,10);
						$asal=$row['Asal'];
						$tujuan=$row['Tujuan'];
						$harga_tiket=number_format($row['HargaTiket'],0,",",".");
						$discount=number_format($row['discount'],0,",",".");
						$bayar=number_format($row['TOTAL'],0,",",".");
						$nomor_kursi=$row['NomorKursi'];
						$operator=$row['operator'];
						$kode_jadwal=$row['KodeJadwal'];
						$id_member=($row['idMember']=='')?'':" ***MEMBER*** $row[idMember]";
						
						$list_field_bayar .="bayar$nomor_kursi=1,";
																		
						// Header
						$pdf->AddPage();
						
						if($i==0){
							//header utk tiket pertama  (maka headernya harus sedikit diperpendek)
							$pdf->Ln();
							$pdf->Cell(0,2.3,'','',0,'');$pdf->Ln();
							$margin_bawah=1;
						}
						else{
							//header utk tiket  kelipatan 4 (karena ganti kertas,maka headernya harus diperpendek)
							$pdf->Ln();
							$pdf->Cell(0,1.4,'','',0,'');$pdf->Ln();
							$margin_bawah=1;
						}
						
				    $pdf->Cell(0,0.2,$no_tiket.'/'.$operator.$id_member,'',0,'');
						$pdf->Ln();
						
						//content
						$pdf->Cell(1.5,1,'',0,0,'');$pdf->Cell(15,1.3,$nama,0,0,'');$pdf->Cell(3.5,0.8,$tanggal,0,0,'');
						$pdf->Ln();
						$pdf->Cell(1.5,1,'',0,0,'');$pdf->Cell(14,1,$alamat,0,0,'');$pdf->Cell(3.5,0.7,$harga_tiket,0,0,'R');
						$pdf->Ln();
						$pdf->Cell(15.5,0.1,'',0,0,'');$pdf->Cell(3.5,0.4,$discount,0,0,'R');
						$pdf->Ln();
						$pdf->Cell(1.5,1,'',0,0,'');$pdf->Cell(14,0.1,$telp,0,0,'');$pdf->Cell(3.5,0.6,$bayar,0,0,'R');
						$pdf->Ln();
						$pdf->Cell(16.5,0.1,'',0,0,'');$pdf->Cell(3.5,1.2,$nomor_kursi,0,0,'R');
						
						//footer
						$pdf->Ln();
						$pdf->Cell(5,0.7,'',0,0,'C');
						$pdf->Ln();
						$pdf->Cell(5,0.1,$asal,0,0,'C');$pdf->Cell(7.3,0.1,$tujuan,0,0,'C');
						$pdf->Ln();
						$pdf->Cell(5,0.7,$jam,0,0,'C');
						$pdf->Ln();
						$pdf->Cell(14,0.4,'',0,0,'C');
						$pdf->Ln();
						$pdf->Cell(14,0.1,'','',0,'C');$pdf->Cell(5,0.1,$cso,'',0,'C');
						//$pdf->Ln();
						//$pdf->Cell(5,$margin_bawah,'','B',0,'C');
						
						$i++;
					}
					
					if($layout_kursi<=10){
						//jika bukan bis
						//mengupate flag cetak tiket
						$list_field_bayar=substr($list_field_bayar,0,strlen($list_field_bayar)-1);
						
						if($nomor_kursi!=''){
							$sql = 
								"UPDATE TbPosisi SET $list_field_bayar
								WHERE KodeJadwal='$kode_jadwal' AND TGLBerangkat=CONVERT(DATETIME, '$temp_tanggal', 105);";
						}
					}
					else{
						//jika bis
						$sql = 
							"UPDATE tbl_layout_detail SET status_bayar=1
							WHERE no_tiket IN($list_tiket);";
					}
					
					
					//mengupate flag cetak tiket
					$sql .= 
						"UPDATE tbReservasi SET CetakTiket=1,CSO='$userdata[username]',waktu_cetak_tiket=CONVERT(VARCHAR(20), GETDATE(), 101)
						WHERE NoUrut IN($list_tiket);";						
						
					if (!$result = $db->sql_query($sql)){
						die_error('GAGAL MENGUBAH DATA',__LINE__,__FILE__,$sql);
					}
					
					$pdf->Output();
						
				}
		}
	break;
}
?>