<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_tgl_digunakan	=isset($HTTP_GET_VARS['istgldigunakan'])? $HTTP_GET_VARS['istgldigunakan'] : $HTTP_POST_VARS['istgldigunakan'];
$fil_status = isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_keuangan_voucher_return/laporan_keuangan_voucher_return_body.tpl')); 

$Cabang	= new Cabang();
	
$cari	= $HTTP_POST_VARS["txt_cari"];

$kondisi_cari	= $cari==""?"":" AND (KodeVoucher LIKE '%$cari%'
														OR tr.Nama LIKE '%$cari%'
														OR tr.telp LIKE '%$cari%'
														OR NoTiketBerangkat LIKE '%$cari%'
														OR NoTiketPulang LIKE '%$cari%'
														OR KodeJadwalBerangkat LIKE '%$cari%'
														OR KodeJadwalPulang LIKE '%$cari%'
														OR tu.Nama LIKE '%$cari%')";

if($kota!=""){
	$kondisi_cabang	.= " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusanBerangkat))='$kota'";
}

if($fil_status!=""){
	switch($fil_status){
		case "0"	:
			//belum terpakai
			$kondisi_status	= " AND (NoTiketPulang IS NULL AND ExpiredDate>DATE(NOW())) ";
			break;
		case "1"	:
			//terpakai
			$kondisi_status	= " AND NoTiketPulang IS NOT NULL ";
			break;
		case "2"	:
			//expired
			$kondisi_status	= " AND ExpiredDate<=DATE(NOW()) ";
			break;
	}
}


$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_tanggal	= $kondisi_cari==""?" AND (".($is_tgl_digunakan==0?"DATE(WaktuCetak)":"DATE(WaktuDigunakan)")." BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')":"";

$kondisi	= $kondisi_cari.$kondisi_cabang.$kondisi_status.$kondisi_tanggal;
	
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?($is_tgl_digunakan==0?"WaktuCetak":"WaktuDigunakan"):$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeVoucher",
		"(tbl_voucher_return tv
		LEFT JOIN tbl_reservasi tr ON tv.NoTiketPulang=tr.NoTiket)
		LEFT JOIN tbl_user tu ON tv.PetugasPengguna=tu.user_id",
		"&kota=$kota&status=$fil_status&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
		"WHERE 1 $kondisi","laporan_keuangan_voucher_return.php",
		$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================


//DATA PENJUALAN TIKET
$sql	= 
	"SELECT tv.*,tr.Nama,tr.Telp,tu.nama as Petugas,IF(NoTiketPulang IS NULL,IF(ExpiredDate>DATE(NOW()),0,2),1) AS Status
	FROM (tbl_voucher_return tv
		LEFT JOIN tbl_reservasi_olap tr ON tv.NoTiketPulang=tr.NoTiket)
		LEFT JOIN tbl_user tu ON tv.PetugasPengguna=tu.user_id
	WHERE 1 $kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=0;

//PLOT DATA
while($row = $db->sql_fetchrow($result)){
	
	$i++;
	
	switch($row['Status']){
		case 0:
			//belum terpakai
			$odd = $i % 2==0?'odd':'even';
			$status	= "Open";
			break;
		
		case 1:
			//terpakai
			$odd	= "green";
			$status	= "TERPAKAI";
			break;
		
		case 2:
			$odd	= "red";
			$status	= "EXPIRED";
			break;
		
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'kode_voucher'=>$row['KodeVoucher'],
				'waktu_buat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetak'])),
				'waktu_pakai'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuDigunakan'])),
				'cabang_cetak'=>$row['CabangBerangkat'],
				'no_tiket'=>$row['NoTiketPulang'],
				'kode_jadwal'=>$row['KodeJadwalPulang'],
				'nama'=>$row['Nama'],
				'telp'=>$row['Telp'],
				'nilai'=>number_format($row['NilaiVoucher'],0,",","."),
				'petugas'=>$row['Petugas'],
				'expired'=>dateparse(FormatMySQLDateToTgl($row['ExpiredDate'])),
				'keterangan'=>"",
				'status'=>$status
			)
		);
		
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota."&status=".$fil_status.
										"&sort_by=".$sort_by."&order=".$order."";
													
$script_cetak_excel="Start('laporan_keuangan_voucher_return_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&status=$fil_status&asal=$asal&tujuan=$tujuan&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "opt_status".$fil_status;
$$temp_var	= "selected";

$temp_var		= "is_tgl_digunakan".$is_tgl_digunakan;
$$temp_var	= "selected";

$sql	= 
	"SELECT COUNT(KodeVoucher) AS TotalVoucher,SUM(IF(NoTiketPulang IS NULL AND ExpiredDate>DATE(NOW()),1,0)) AS TotalBelumTerpakai,
		SUM(IF(ExpiredDate <=DATE(NOW()),1,0)) AS TotalExpired,
		SUM(NilaiVoucher) AS TotalRupiahVoucher,SUM(IF(NoTiketPulang IS NULL AND ExpiredDate>DATE(NOW()),NilaiVoucher,0)) AS TotalRupiahBelumTerpakai,
		SUM(IF(ExpiredDate <=DATE(NOW()),NIlaiVoucher,0)) AS TotalRupiahExpired
	FROM (tbl_voucher_return tv
		LEFT JOIN tbl_reservasi tr ON tv.NoTiketPulang=tr.NoTiket)
		LEFT JOIN tbl_user tu ON tv.PetugasPengguna=tu.user_id
		WHERE 1 $kondisi";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_total = $db->sql_fetchrow($result);

//$kota	= $kota!=""?$kota:"JAKARTA";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_keuangan">Home</a> | <a href="'.append_sid('laporan_keuangan_voucher_return.'.$phpEx).'">Laporan Rekap Penjualan Tiketux</a>',
	'ACTION_CARI'		=> append_sid('laporan_keuangan_voucher_return.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'ISTGLDIGUNAKAN0'=>$is_tgl_digunakan0,
	'ISTGLDIGUNAKAN1'=>$is_tgl_digunakan1,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_KOTA'			=> setComboKota($kota),
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'STATUS'				=> $opt_status,
	'STATUS0'				=> $opt_status0,
	'STATUS1'				=> $opt_status1,
	'STATUS2'				=> $opt_status2,
	'NAMA'					=> $userdata['Nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=KodeVoucher'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan kode voucher($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=WaktuCetak'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Waktu cetak($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=WaktuDigunakan'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan waktu penggunaan ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=CabangTujuan'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan cabang ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=NoTiket'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan nomor tiket($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=KodeJadwal'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan kode jadwal($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=Nama'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan nama penumpang($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=Telp'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan nomor telepon($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=NilaiVoucher'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan nilai voucher($order_invert)",
	'A_SORT_10'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=Petugas'.$parameter_sorting),
	'TIPS_SORT_10'	=> "Urutkan petugas ($order_invert)",
	'A_SORT_11'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=ExpiredDate'.$parameter_sorting),
	'TIPS_SORT_11'	=> "Urutkan expired date ($order_invert)",
	'A_SORT_12'			=> append_sid('laporan_keuangan_voucher_return.'.$phpEx.'?sort_by=Status'.$parameter_sorting),
	'TIPS_SORT_12'	=> "Urutkan status ($order_invert)",
	'TOTAL_VOUCHER'	=> number_format($data_total['TotalVoucher'],0,",","."),
	'TOTAL_BELUM_DIGUNAKAN'=> number_format($data_total['TotalBelumTerpakai'],0,",","."),
	'TOTAL_DIGUNAKAN'=> number_format($data_total['TotalVoucher']-$data_total['TotalBelumTerpakai']-$data_total['TotalExpired'],0,",","."),
	'TOTAL_EXPIRED'	=> number_format($data_total['TotalExpired'],0,",","."),
	'TOTAL_RUPIAH_VOUCHER'	=> "Rp. ".number_format($data_total['TotalRupiahVoucher'],0,",","."),
	'TOTAL_RUPIAH_BELUM_DIGUNAKAN'=> "Rp. ".number_format($data_total['TotalRupiahBelumTerpakai'],0,",","."),
	'TOTAL_RUPIAH_DIGUNAKAN'=> "Rp. ".number_format($data_total['TotalRupiahVoucher']-$data_total['TotalRupiahBelumTerpakai']-$data_total['TotalRupiahExpired'],0,",","."),
	'TOTAL_RUPIAH_EXPIRED'	=> "Rp. ".number_format($data_total['TotalRupiahExpired'],0,",",".")
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>