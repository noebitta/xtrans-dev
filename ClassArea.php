<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Area{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Area(){
		$this->ID_FILE="C-ARE";
	}
	
	//BODY
	
	function periksaDuplikasi($kode){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) AS jumlah_data FROM tbl_md_area WHERE KodeArea='$kode'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		$ditemukan = ($row['jumlah_data']<=0)?false:true;


		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($kode,$nama){
	  
		/*
		ID	: 002
		IS	: data Area belum ada dalam database
		FS	:Data Area baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "INSERT INTO tbl_md_area VALUES('$kode','$nama');";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_md_area
			WHERE 
				KodeArea LIKE '$pencari' 
				OR NamaArea LIKE '%$pencari%'
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err:". __LINE__);
		}
		
	}//  END ambilData
	
	function ubah($kode_old,$kode,$nama){
	  
		/*
		ID	: 004
		IS	: data cabang sudah ada dalam database
		FS	:Data cabang diubah 
		*/
		
		//kamus
		global $db;
		
		//MENGUBAH DATA DI DATABASE
		$sql ="UPDATE tbl_md_area SET KodeArea='$kode',NamaArea='$nama' WHERE KodeArea='$kode_old';";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list){
	  
		/*
		ID	: 005
		IS	: data Cabang sudah ada dalam database
		FS	:Data Cabang dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_area
			WHERE KodeArea IN($list);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($kode=""){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$kondisi = $kode==""?"":"WHERE KodeArea='$kode'";

		$sql = 
			"SELECT *
			FROM tbl_md_area $kondisi;";
		
		if ($result = $db->sql_query($sql,TRUE)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$error	= $db->sql_error();
			die_error("Err:$this->ID_FILE ".__LINE__);
		}
		
	}//  END ambilDataDetail
	
	function setComboArea(){
		
		/*
		Desc	:Mengembalikan data Cabang sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_area
			ORDER BY KodeArea;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err: $this->ID_FILE". __LINE__);
		}
		
	}//  END setComboArea
	
}
?>