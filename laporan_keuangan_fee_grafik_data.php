<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; 
$submode 		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 

//DATA GRAFIK

$tanggal  	= isset($HTTP_GET_VARS['tgl'])? $HTTP_GET_VARS['tgl'] : $HTTP_POST_VARS['tgl'];

$arr_tanggal	= explode("-",$tanggal);
$bulan	= $arr_tanggal[1];
$tahun	= $arr_tanggal[2];

//INISIALISASI
include_once( 'chart/php-ofc-library/open-flash-chart.php' );
$g = new graph();

$data	= array();
$axis	= array();

$PengaturanUmum	= new PengaturanUmum();
$fee_transaksi	= $PengaturanUmum->ambilFeeTransaksi();
$fee_tiket			= $fee_transaksi['FeeTiket']; 
$fee_paket			= $fee_transaksi['FeePaket'];

//GRAFIK OMZET==============================================================================================
if($mode=='bulanan' && $submode=='omzet'){
	//AMBIL HARI
	$sql=
		"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		$temp_hari	= $row['Hari'];
	}
	
	//FEE PENUMPANG
	$sql=
		"SELECT 
			WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
			IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket
		FROM tbl_reservasi
		WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1
		GROUP BY DATE(WaktuCetakTiket)
		ORDER BY DATE(WaktuCetakTiket) ";

	if ($result_penumpang = $db->sql_query($sql)){
		$data_penumpang = $db->sql_fetchrow($result_penumpang);
	} 
	else{
		//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
		echo("Error:".__LINE__);exit;
	}
		
	//FEE PAKET
	$sql=
		"SELECT 
			WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
			IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
			IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
		FROM tbl_paket
		WHERE MONTH(WaktuPesan)=$bulan AND YEAR(WaktuPesan)=$tahun AND CetakTiket=1 AND FlagBatal!=1
		GROUP BY DATE(WaktuPesan)
		ORDER BY DATE(WaktuPesan)";

	if ($result_paket = $db->sql_query($sql)){
		$data_paket = $db->sql_fetchrow($result_paket);
	} 
	else{
		//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
		echo("Error:".__LINE__);exit;
	}
	
	for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
		
		$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

		$axis[$idx_tgl]		= $idx_tgl+1 ."(".HariStringShort($idx_str_hari).")";
		
		
		//FEE PENUMPANG
		if($data_penumpang['Tanggal']==$idx_tgl+1){
			$data[$idx_tgl]	= number_format($data_penumpang['TotalTiket']*$fee_tiket,0,"",""); 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_tgl]		= 0;
		}
		
		//FEE PAKET
		if($data_paket['Tanggal']==$idx_tgl+1){
			$data1[$idx_tgl]	= number_format(($fee_paket<=1)?$data_paket['TotalOmzetPaket']*$fee_paket:$total_paket*$fee_paket,0,"",""); 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_tgl]		= 0;
		}
		
		$temp_hari++;
	}
	
	$judul_grafik	="Grafik Fee Bulan: ".BulanString($bulan)." $tahun ";
	$legend	="Tanggal";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Omzet Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Omzet Paket', 12 );
	$g->set_y_legend( 'Jumlah (Rp.)', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);	

	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
} 
elseif($mode=='tahunan' && $submode=='omzet'){

	$pembagi	= 1000;
	
	//FEE PENUMPANG
	$sql=
		"SELECT 
			MONTH(WaktuCetakTiket) bulan,
			IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket
		FROM tbl_reservasi
		WHERE YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1 AND  FlagBatal!=1
		GROUP BY MONTH(WaktuCetakTiket),YEAR(WaktuCetakTiket)
		ORDER BY bulan";

	if(!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	
	//FEE PAKET
	$sql=
		"SELECT 
			MONTH(WaktuPesan) bulan,
			IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
			IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
		FROM tbl_paket
		WHERE YEAR(WaktuPesan)=$tahun AND CetakTiket=1 AND  FlagBatal!=1
		GROUP BY MONTH(WaktuPesan),YEAR(WaktuPesan)
		ORDER BY bulan";

	if(!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_paket = $db->sql_fetchrow($result_paket);
		
	for($idx_bln=0;$idx_bln<12;$idx_bln++){
		$axis[$idx_bln]	= BulanString($idx_bln+1);
		
		//FEE PENUMPANG
		if($data_penumpang['bulan']==$idx_bln+1){
			$data[$idx_bln]	= $data_penumpang['TotalTiket']*$fee_tiket/$pembagi; 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_bln]=0;
		}
		
		//FEE PAKET
		if($data_paket['bulan']==$idx_bln+1){
			$data1[$idx_bln]	= (($fee_paket<=1)?$data_paket['TotalOmzetPaket']*$fee_paket:$total_paket*$fee_paket)/$pembagi; 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_bln]=0;
		}
	}	
	
	
	$judul_grafik	="Grafik Fee Tahun: $tahun";
	$legend	="Bulan";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Omzet Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Omzet Paket', 12 );
	$g->set_y_legend( 'Jumlah (Rp.) - x '.$pembagi, 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);

	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
} //GRAFIK PRODUKTIFITAS============================================================================================= 
elseif($mode=='bulanan' && $submode=='produktifitas'){
	//AMBIL HARI
	$sql=
		"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		$temp_hari	= $row['Hari'];
	}
		
	//LOAD PENUMPANG
	$sql=
		"SELECT 
			WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
			IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket
		FROM tbl_reservasi
		WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1 AND  FlagBatal!=1
		GROUP BY DATE(WaktuCetakTiket)
		ORDER BY DATE(WaktuCetakTiket)";
	
	
	if (!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	//LOAD PAKET
	$sql=
		"SELECT 
			WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
			IS_NULL(COUNT(NoTiket),0) AS TotalPaket
		FROM tbl_paket
		WHERE MONTH(WaktuPesan)=$bulan AND YEAR(WaktuPesan)=$tahun AND CetakTiket=1 AND  FlagBatal!=1
		GROUP BY DATE(WaktuPesan)
		ORDER BY DATE(WaktuPesan)";
	
	if (!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	$data_paket 		= $db->sql_fetchrow($result_paket);
		
	for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
		
		$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

		$axis[$idx_tgl]		= $idx_tgl+1 ."(".HariStringShort($idx_str_hari).")";
		
		//penumpang
		if($data_penumpang['Tanggal']==$idx_tgl+1){
			$data[$idx_tgl]	= number_format($data_penumpang['TotalTiket'],0,"",""); 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_tgl]		= 0;
		}
		
		//PAKET
		if($data_paket['Tanggal']==$idx_tgl+1){
			$data1[$idx_tgl]	= number_format($data_paket['TotalPaket'],0,"",""); 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_tgl]		= 0;
		}
		
		$temp_hari++;
	}
	
	$judul_grafik	="Grafik Load Penumpang & Paket Cabang: ".$cabang." Bulan: ".BulanString($bulan)." $tahun";
	$legend	="Tanggal";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Load Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Load Paket', 12 );
	$g->set_y_legend( 'Jumlah', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);
	
	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
} 
elseif($mode=='tahunan' && $submode=='produktifitas'){
	
	//LOAD PENUMPANG
	$sql=
		"SELECT 
			MONTH(WaktuCetakTiket) bulan,
			IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket
		FROM tbl_reservasi
		WHERE YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1 AND  FlagBatal!=1
		GROUP BY MONTH(WaktuCetakTiket),YEAR(WaktuCetakTiket)
		ORDER BY bulan";

	if(!$result_penumpang = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
	
	//LOAD PAKET
	$sql=
		"SELECT 
			MONTH(WaktuPesan) bulan,
			IS_NULL(COUNT(NoTiket),0) AS TotalPaket
		FROM tbl_paket
		WHERE YEAR(WaktuPesan)=$tahun AND CetakTiket=1 AND  FlagBatal!=1
		GROUP BY MONTH(WaktuPesan),YEAR(WaktuPesan)
		ORDER BY bulan";

	if(!$result_paket = $db->sql_query($sql)){
		echo("Error:".__LINE__);exit;
	}
	
	$data_paket = $db->sql_fetchrow($result_paket);
		
	for($idx_bln=0;$idx_bln<12;$idx_bln++){
		$axis[$idx_bln]	= BulanString($idx_bln+1);
		
		//DATA PENUMPANG
		if($data_penumpang['bulan']==$idx_bln+1){
			$data[$idx_bln]	= $data_penumpang['TotalTiket']; 
			$data_penumpang = $db->sql_fetchrow($result_penumpang);
		}
		else{
			$data[$idx_bln]=0;
		}
		
		//DATA PAKET
		if($data_paket['bulan']==$idx_bln+1){
			$data1[$idx_bln]	= $data_paket['TotalPaket']; 
			$data_paket = $db->sql_fetchrow($result_paket);
		}
		else{
			$data1[$idx_bln]=0;
		}
	}	
	
	$judul_grafik	="Grafik Load Penumpang & Paket Cabang: $cabang Tahun: $tahun";
	$legend	="Bulan";
	
	// we add 3 sets of data:
	$g->set_data($data);
	$g->set_data($data1);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Load Penumpang', 12 );
	$g->line_hollow( 2, 4, '0x80a033', 'Load Paket', 12 );
	$g->set_y_legend( 'Jumlah', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data),max($data1));
	
	$max_value_y	= max($temp_max_value);
	
	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;
	
} 

$g->title($judul_grafik, '{font-size: 20px; color: #736AFF}' );

$g->set_x_labels($axis);
$g->set_x_label_style( 12, '0x000000', 0, 2 );
$g->set_x_legend($legend, 13, '#736AFF' );

$g->set_y_max($max_value);

$g->y_label_steps(5);
echo $g->render();
?>