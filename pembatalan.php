<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Reservasi	= new Reservasi();
$Jadwal			= new Jadwal();

	if ($mode=='show'){
		// membatalkan reservasi
		$no_tiket = $HTTP_GET_VARS['no_tiket'];
		
		$data_kursi	= $Reservasi->ambilDataKursi($no_tiket);
		
		if($data_kursi['NoTiket']==""){
			
			echo("
				<table border='0' width='100%' height='100%' bgcolor='ffffff'> 
					<tr>
						<td width='40%' colspan='3'><h3>Data tidak ditemukan</h3></td>
					</tr>
				</table>
			");
			exit;
		}
				
		$harga_tiket	=number_format($data_kursi['HargaTiket'],0,",",".");
		$discount			=number_format($data_kursi['Discount'],0,",",".");
		$sub_total		=number_format($data_kursi['SubTotal'],0,",",".");
		$total				=number_format($data_kursi['Total'],0,",",".");
		$nama_cso			=$data_kursi['NamaCSO'];
		
		$data_jadwal	= $Jadwal->ambilDataDetail($data_kursi['KodeJadwal']);
		
		echo("
			<table border='0' class='border' width='100%' height='100%'> 
				<tr>
					<input type='hidden' id='no_tiket' value='$no_tiket' />
					<input type='hidden' id='no_kursi' value='$data_kursi[NomorKursi]' />
					<td align='center' colspan=3><h1>Kursi: $data_kursi[NomorKursi]</h1></td>
				</tr>
				<tr>
					<td>No Tiket</td><td>:</td>
					<td>$no_tiket</td>
				</tr>
				<tr>
					<td>Tgl. Berangkat</td><td>:</td>
					<td>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['TglBerangkat']))."</td>
				</tr>
				<tr>
					<td>Jurusan</td><td>:</td>
					<td>$data_jadwal[NamaAsal] - $data_jadwal[NamaTujuan] $data_jadwal[JamBerangkat]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td>
					<td>$data_kursi[Nama]</td>
				</tr>
				<tr>
					<td valign='top'>Alamat</td><td valign='top'>:</td>
					<td>$data_kursi[Alamat]</td>
				</tr>
				<tr>
					<td>Telepon</td><td>:</td>
					<td>$data_kursi[Telp]</td>
				</tr>
				<tr>
					<td colspan=3 heigh=1 bgcolor='D0D0D0'></td>
				</tr>
				<tr>
					<td>Harga tiket</td><td>:</td>
					<td align='right'>Rp. $harga_tiket&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Discount</td><td>:</td>
					<td align='right'>Rp. $discount&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3 heigh=1 bgcolor='D0D0D0'></td>
				</tr>
				<tr>
					<td>Total</td><td>:</td>
					<td align='right'><strong>Rp. $total</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3 height=1 bgcolor='red'></td>
				</tr>
				<tr>
					<td>CSO</td><td>:</td>
					<td><strong>$nama_cso</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Waktu Pesan</td><td>:</td>
					<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuPesan']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>");
			
		echo("</table>");	
		
		exit;
		
	}
	elseif($mode=='pembatalan'){
		
		$no_tiket	= $HTTP_GET_VARS['no_tiket'];// nourut
		$kursi		= $HTTP_GET_VARS['no_kursi']; // kursi
		$Reservasi	= new Reservasi();
		
		//periksa wewenang
		$Reservasi->pembatalan($no_tiket, $kursi, $userdata['user_id']);
		echo(1);
		exit;
	}
	else {
		// LIST
		$template->set_filenames(array('body' => 'pembatalan/pembatalan_body.tpl')); 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pembatalan.'.$phpEx).'">Pembatalan</a>'
		
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>