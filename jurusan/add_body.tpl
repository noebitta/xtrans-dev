<script type="text/javascript" src="{TPL}js/main.js"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;

	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){

	valid=true;

	kode									= document.getElementById('kode');
	harga_tiket						= document.getElementById('harga_tiket');
	harga_tiket_tuslah		= document.getElementById('harga_tiket_tuslah');
	biaya_sopir						= document.getElementById('biaya_sopir');
	biaya_tol							= document.getElementById('biaya_tol');
	biaya_parkir					= document.getElementById('biaya_parkir');
	biaya_bbm							= document.getElementById('biaya_bbm');
	komisi_penumpang_sopir= document.getElementById('komisi_penumpang_sopir');

	kode.style.background="white";
	harga_tiket.style.background="white";
	harga_tiket_tuslah.style.background="white";


	if(kode.value==''){valid=false;	kode.style.background="red"};

	if(harga_tiket.value==''){valid=false; harga_tiket.style.background="red"};

	if(harga_tiket_tuslah.value==''){valid=false; harga_tiket_tuslah.style.background="red"};

	return valid;

}

</script>

<form name="frm_data_mobil" action="{U_JURUSAN_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Jurusan</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='1000'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='500'>
				<table width='500'>
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <input type="hidden" name="id_jurusan" value="{ID_JURUSAN}">
			      <input type="hidden" name="kode_jurusan_old" value="{KODE_JURUSAN_OLD}">
						<td width='200'><u>Kode Jurusan</u></td><td width='5'>:</td>
						<td width='300'>
							<input type="text" id="kode" name="kode" value="{KODE_JURUSAN}" maxlength=10 onFocus="this.style.background='white';">
						</td>
			    </tr>
					<tr>
						<td>Asal</td><td>:</td>
						<td>
							<select id='asal' name='asal'>
								{OPT_ASAL}
							</select>
						</td>
					</tr>
					<tr>
						<td>Tujuan</td><td>:</td>
						<td>
							<select id='tujuan' name='tujuan'>
								{OPT_TUJUAN}
							</select>
						</td>
					</tr>
					<tr>
			      <td><u>Harga Tiket</u></td><td>:</td>
						<td>
							<input type="text" id="harga_tiket" name="harga_tiket" value="{HARGA_TIKET}" maxlength=8 onkeypress="validasiAngka(event);" onFocus="this.style.background='white';">
						</td>
			    </tr>
					<tr>
			      <td><u>Harga Tiket Tuslah</u></td><td>:</td>
						<td>
							<input type="text" id="harga_tiket_tuslah" name="harga_tiket_tuslah" value="{HARGA_TIKET_TUSLAH}" maxlength=8 onkeypress="validasiAngka(event);" onFocus="this.style.background='white';">
						</td>
			    </tr>
					<tr>
			      <td>Status Aktif</td><td>:</td>
						<td>
							<select id="flag_aktif" name="flag_aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
			    </tr>
					<tr>
			      <td>Jenis Jurusan</td><td>:</td>
						<td>
							<select id="flag_jenis" name="flag_jenis">
								<option value=1 {JENIS_1}>LUAR KOTA</option>
								<option value=0 {JENIS_0}>DALAM KOTA</option>
							</select>
						</td>
			    </tr>
					<tr>
						<td>Operasional untuk</td><td>:</td>
						<td>
							<select id="flag_op_jurusan" name="flag_op_jurusan">
								<option value=0 {OP_0}>Travel & Paket</option>
								<option value=1 {OP_1}>Paket</option>
							</select>
						</td>
					</tr>
			    <tr>
						<td>Area</td><td>:</td>
						<td>
							<select id='kode_area' name='kode_area'>
								{OPT_AREA}
							</select>
						</td>
					</tr>
					<tr><td colspan='3'><hr></td></tr>
					<tr><td colspan=3><h3>Biaya-biaya:</h3></td></tr>
					<tr>
			      <td>Biaya Sopir</td><td>:</td>
						<td>
							Rp.<input type="text" id="biaya_sopir" name="biaya_sopir" value="{BIAYA_SOPIR}" maxlength=8 onkeypress="validasiAngka(event);">
						</td>
			    </tr>
			    <tr>
			      <td>Insentif Sopir/Penumpang</td><td>:</td>
						<td>
							Rp.<input type="text" id="komisi_penumpang_sopir" name="komisi_penumpang_sopir" value="{KOMISI_PENUMPANG_SOPIR}" maxlength=8 onkeypress="validasiAngka(event);">
						</td>
			    </tr>
					<tr>
			      <td>Biaya Tol</td><td>:</td>
						<td>
							Rp.<input type="text" id="biaya_tol" name="biaya_tol" value="{BIAYA_TOL}" maxlength=8 onkeypress="validasiAngka(event);">
						</td>
			    </tr>
					<tr>
			      <td>Biaya Parkir</td><td>:</td>
						<td>
							Rp.<input type="text" id="biaya_parkir" name="biaya_parkir" value="{BIAYA_PARKIR}" maxlength=8 onkeypress="validasiAngka(event);">
						</td>
			    </tr>
					<tr>
			      <td>Biaya BBM</td><td>:</td>
						<td>
							Rp.<input type="text" id="biaya_bbm" name="biaya_bbm" value="{BIAYA_BBM}" maxlength=8 onkeypress="validasiAngka(event);">
						</td>
			    </tr>
				</table>
			</td>
			<td width=1 bgcolor='D0D0D0'></td>
			<td align='center' valign='top' width='500'>
				<table width='500'>
					<tr><td colspan=3><h3>Harga Kirim Paket:</h3></td></tr>
					<tr>
			      <td valign='top'>Paket Platinum</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_p_kilo_pertama" name="harga_paket_p_kilo_pertama" value="{HARGA_PAKET_P_KILO_PERTAMA}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg pertama<br><br>
							Rp.&nbsp;<input type="text" id="harga_paket_p_kilo_berikut" name="harga_paket_p_kilo_berikut" value="{HARGA_PAKET_P_KILO_BERIKUT}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Gold Antar</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_ga_kilo_pertama" name="harga_paket_ga_kilo_pertama" value="{HARGA_PAKET_GA_KILO_PERTAMA}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg pertama<br><br>
							Rp.&nbsp;<input type="text" id="harga_paket_ga_kilo_berikut" name="harga_paket_ga_kilo_berikut" value="{HARGA_PAKET_GA_KILO_BERIKUT}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Gold Diambil</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_gd_kilo_pertama" name="harga_paket_gd_kilo_pertama" value="{HARGA_PAKET_GD_KILO_PERTAMA}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg pertama<br><br>
							Rp.&nbsp;<input type="text" id="harga_paket_gd_kilo_berikut" name="harga_paket_gd_kilo_berikut" value="{HARGA_PAKET_GD_KILO_BERIKUT}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Silver</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_s_kilo_pertama" name="harga_paket_s_kilo_pertama" value="{HARGA_PAKET_S_KILO_PERTAMA}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg pertama<br><br>
							Rp.&nbsp;<input type="text" id="harga_paket_s_kilo_berikut" name="harga_paket_s_kilo_berikut" value="{HARGA_PAKET_S_KILO_BERIKUT}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg berikutnya<hr>
						</td>
			    </tr>
			    <tr>
			      <td valign='top'>Paket Cargo Antar</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_ca_kilo_pertama" name="harga_paket_ca_kilo_pertama" value="{HARGA_PAKET_CA_KILO_PERTAMA}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/<b>50 Kg pertama</b><br><br>
							Rp.&nbsp;<input type="text" id="harga_paket_ca_kilo_berikut" name="harga_paket_ca_kilo_berikut" value="{HARGA_PAKET_CA_KILO_BERIKUT}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Cargo Diambil</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_cd_kilo_pertama" name="harga_paket_cd_kilo_pertama" value="{HARGA_PAKET_CD_KILO_PERTAMA}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/<b>50 Kg pertama</b><br><br>
							Rp.&nbsp;<input type="text" id="harga_paket_cd_kilo_berikut" name="harga_paket_cd_kilo_berikut" value="{HARGA_PAKET_CD_KILO_BERIKUT}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Kg berikutnya<hr>
						</td>
			    </tr>
			    <tr>
			      <td valign='top'>Paket Motor Kecil</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_motor1" name="harga_paket_motor1" value="{HARGA_PAKET_MOTOR1}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Unit<br><br>
						</td>
			    </tr>
			    <tr>
			      <td valign='top'>Paket Motor Besar</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_motor2" name="harga_paket_motor2" value="{HARGA_PAKET_MOTOR2}" maxlength='8' size='12' onkeypress="validasiAngka(event);">/Unit<br><br>
						</td>
			    </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
				<input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
</form>