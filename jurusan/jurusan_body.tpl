<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}

		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_jurusan="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatusAktif(id){
	
		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatusaktif&id="+id,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function ubahStatusTuslah(id,tuslah){
		
		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatustuslah&id="+id+"&tuslah="+tuslah,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}


</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul" width='550'>&nbsp;Master Jurusan</td>
				<td colspan=2 align='left' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />&nbsp;<input type="submit" value="cari" />&nbsp;
					</form>
				</td>
			</tr>
			<tr>
				<td align='left'>
					<a href="{U_JURUSAN_ADD}">[+]Tambah Jurusan</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Jurusan</a></td>
				<td align='left'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
		<table width='100%' class="border">
    <tr>
       <th rowspan='2' width=30></th>
       <th rowspan='2' width=30>No</th>
			 <th rowspan='2' width=100>Kode Jurusan</th>
			 <th rowspan='2' width=100>Asal</th>
			 <th rowspan='2' width=100>Tujuan</th>
			 <th rowspan='2' width=100>Jenis</th>
			 <th rowspan='2' width=70>Tiket (Rp.)</th>
			 <th rowspan='2' width=70>Tuslah (Rp.)</th>
			 <th colspan='8'>Harga Kirim Paket</th>
			 <th rowspan='2' width=50>Area</th>
			 <th rowspan='2' width=70>Aktif</th>
			 <th rowspan='2' width=120>Action</th>
     </tr>
		 <tr>
			<th width='60'>Platinum</th>
			<th width='60'>Gold Antar</th>
			<th width='60'>Gold Diambil</th>
			<th width='60'>Silver</th>
			<th width='60'>Cargo Antar</th>
			<th width='60'>Cargo Diambil</th>
		 	<th width='60'>Motor Kecil</th>
		 	<th width='60'>Motor Besar</th>
		 </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.check}</td>
       <td align="right">{ROW.no}</td>
       <td align="left">{ROW.kode}</td>
			 <td align="left">{ROW.asal}</td>
       <td align="left">{ROW.tujuan}</td>
       <td align="left">{ROW.jenis}</td>
			 <td class="{ROW.normal}" align="right">{ROW.harga_tiket}</td>
			 <td class="{ROW.tuslah}" align="right">{ROW.harga_tiket_tuslah}</td>
			 <td align="right">{ROW.harga_paket_p}</td>
			 <td align="right">{ROW.harga_paket_ga}</td>
			 <td align="right">{ROW.harga_paket_gd}</td>
			 <td align="right">{ROW.harga_paket_s}</td>
			 <td align="right">{ROW.harga_paket_ca}</td>
			 <td align="right">{ROW.harga_paket_cd}</td>
			 <td align="right">{ROW.harga_paket_motor1}</td>
			 <td align="right">{ROW.harga_paket_motor2}</td>
			 <td align="center">{ROW.area}</td>
			 <td align="center">{ROW.status_aktif}</td>
       <td align="center">{ROW.action}</td>
     </tr>  
     <!-- END ROW -->
		{NO_DATA}
    </table>
    <table width='100%'>
			<tr>
				<td width='550' align='left'>
					<a href="{U_JURUSAN_ADD}">[+]Tambah Jurusan</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Jurusan</a></td>
				<td align='left'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>