<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberPromo.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act   	 = $HTTP_GET_VARS['act'];
$pesan   = $HTTP_GET_VARS['pesan'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Promo	= new Promo();

switch($mode){

//TAMPILKAN MEMBER BARU ==========================================================================================================
case 'tampilkan_promo':
	
	$sortby    	= $HTTP_GET_VARS['sortby'];
	$ascending	= $HTTP_GET_VARS['ascending'];
	$ascending	= ($ascending==0)?"asc":"desc";
	
	$cari 			= $HTTP_GET_VARS['cari'];
	
	//HEADER TABEL
	$hasil ="
		<table width='100%' class='border'>
    <tr>
      <th>#</th>
			<th><a href='#' onClick='setOrder(\"id_promo\")'>
				<font color='white'>ID</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"layer_promo\")'>
				<font color='white'>Layer Promo</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"kode_jadwal\")'>
				<font color='white'>Rute</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"masa_berlaku_mula\")'>
				<font color='white'>Berlaku mulai</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"masa_berlaku_akhir\")'>
				<font color='white'>Berakhir pada</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"point\")'>
				<font color='white'>point</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"discount_jumlah\")'>
				<font color='white'>Discount (Rp.)</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"discount_persentase\")'>
				<font color='white'>Discount (%)</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"dibuat_oleh\")'>
				<font color='white'>Creator</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"waktu_dibuat\")'>
				<font color='white'>Waktu dibuat</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"status_aktif\")'>
				<font color='white'>Status</font></a></th>
			
			<th width='90'>Aksi</th>
    </tr>";
    
	$result	= $Promo->ambilData($cari,$sortby,$ascending);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		if($row['status_aktif']){
			$status	= "Aktif";
		}
		else
		{
			$status	= "Nonaktif";
			$odd='red';
		}
		
		$action = 
			"<input type='button' value='&nbsp;&nbsp;Ubah&nbsp;&nbsp;&nbsp;' onClick='document.location=\"".append_sid('member_promo_detail.'.$phpEx)."&id_promo=$row[id_promo]&mode=ambil_data_promo"."\"' />
			<input type='button' value='&nbsp;&nbsp;Hapus&nbsp;&nbsp;' onclick=\"TanyaHapus('$row[id_promo]');\"  />";
		
		//option layer promo
	$temp_var="selected_layer_promo".$row['layer_promo'];
	$$temp_var="selected";
	
	$opt_layer_promo="
		<option $selected_layer_promo1 value='1'>1</option>
		<option $selected_layer_promo2 value='2'>2</option>
		<option $selected_layer_promo3 value='3'>3</option>
		<option $selected_layer_promo4 value='4'>4</option>
		<option $selected_layer_promo5 value='5'>5</option>
		<option $selected_layer_promo6 value='6'>6</option>
		<option $selected_layer_promo7 value='7'>7</option>
		<option $selected_layer_promo8 value='8'>8</option>
		<option $selected_layer_promo9 value='9'>9</option>
		<option $selected_layer_promo10 value='10'>10</option>";
		
		$$temp_var="";
		
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[id_promo]</td>
      <td class='$odd' align='center'><select onChange='ubahLayerPromo($row[id_promo],this.value);'>$opt_layer_promo</select><span id='progress_$row[id_promo]' style='display:none;'><img src='./images/progress.gif' /></span></td>
      <td class='$odd'>$row[kode_jadwal]</td>
      <td class='$odd'>$row[masa_berlaku_mula]</td>
      <td class='$odd'>$row[masa_berlaku_akhir]</td>
      <td class='$odd' align='right'>".number_format($row['point'],0,",",".")."</td>
      <td class='$odd' align='right'>".number_format($row['discount_jumlah'],0,",",".")."</td>
			<td class='$odd' align='right'>".number_format($row['discount_persentase'],0,",",".")."</td>
			<td class='$odd'>$row[dibuat_oleh]</td>
			<td class='$odd'>$row[waktu_dibuat]</td>
			<td class='$odd' align='center'><a href='#' onClick='ubahStatus(\"$row[id_promo]\");'>$status</a></td>
			<td class='$odd' align='center'>$action</td>
    </tr>
		<tr>
			<td colspan=14 height=3 bgcolor='D0D0D0'></td>
		</tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=14 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

//UBAH STATUS PROMO ==========================================================================================================
case 'ubah_status':
	$id_promo    = $HTTP_GET_VARS['id_promo'];  
	
	if(!$Promo->ubahStatus($id_promo)) echo("false");else echo("true");
	
exit;
//switch mode

//UBAH LAYER PROMO ==========================================================================================================
case 'ubah_layer_promo':
	$id_promo    = $HTTP_GET_VARS['id_promo'];  
	$layer_promo = $HTTP_GET_VARS['layer_promo'];  
	
	if(!$Promo->ubahLayerPromo($id_promo,$layer_promo)) echo("false");else echo("true");
	
exit;
//switch mode

//HAPUS MEMBER ==========================================================================================================
case 'hapus_promo':
	
	//ambil isi dari inputan
	$id_promo	= $HTTP_GET_VARS['id_promo'];	
	
	if($Promo->hapus($id_promo)){
		echo("berhasil");
	}
	else{
		echo("gagal");
	}
exit;
}//switch mode

$template->set_filenames(array('body' => 'member_promo.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  	=>$userdata['username'],
   	'BCRUMP'    	=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_promo.'.$phpEx).'">Promo</a>',
   	'U_ADD' 			=>'<a href="'.append_sid('member_promo_detail.'.$phpEx) .'">Tambah Promo</a>',
   	'U_USER_SHOW'	=>append_sid('promo.'.$phpEx.'?mode=tampilkan_promo'),
		'PESAN'				=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>