<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$area				= $HTTP_GET_VARS['area'];
$ket_area		= $area!=""?"Area ".$area:"Semua Area";
$sort_by		= $HTTP_GET_VARS['sort_by'];
$order			= $HTTP_GET_VARS['order'];

$tanggal_mulai	= $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir	= $HTTP_GET_VARS['tanggal_akhir'];
$kode_driver		= $HTTP_GET_VARS['kode_driver']; 

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"TglSPJ":$sort_by;

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Insentif Sopir Detail Manifest '.$ket_area.' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);


//DATA SOPIR
$sql	=
	"SELECT 
		Nama,KodeSopir
	FROM tbl_md_sopir
	WHERE KodeSopir='$kode_driver'";

if(!$result = $db->sql_query($sql)){
	echo("Err: $sql".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$keterangan_data_sopir	=
	"Nama Sopir:$row[Nama] | NIS:$row[KodeSopir] | Area:$area | Periode Laporan: $tanggal_mulai s/d $tanggal_akhir";
$objPHPExcel->getActiveSheet()->setCellValue('A2', $keterangan_data_sopir);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', '#Manifest');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Kode Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Area');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Layout Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jumlah Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Jumlah Penumpang Insentif');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Total Penerimaan Insentif');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

$kondisi_area	= $area!="" ? " AND KodeArea='$area'":"";

$sql	=
	"SELECT 
		NoSPJ,TglSPJ,KodeJadwal,
		TglBerangkat,JamBerangkat,JumlahKursiDisediakan,JumlahPenumpang,
		JumPnpInsentif,JumPnpInsentif*InsentifSopir AS TotalInsentif,
		NoPolisi,KodeArea,IF(IsSubJadwal!=1,'UTAMA','TRANSIT') AS Keterangan
	FROM tbl_spj ts
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND KodeDriver='$kode_driver' $kondisi_area
	ORDER BY $sort_by $order";

	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+3;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoSPJ']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['TglSPJ']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, FormatMySQLDateToTgl($row['TglBerangkat'])." ".$row['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['KodeArea']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['JumlahKursiDisediakan']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['JumlahPenumpang']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['JumPnpInsentif']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['TotalInsentif']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Keterangan']);	
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':H'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Insentif Sopir Detail Manifest '.$ket_area.' per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
