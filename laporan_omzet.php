<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassArea.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$kode_area	= isset($HTTP_GET_VARS['kodearea'])? $HTTP_GET_VARS['kodearea'] : $HTTP_POST_VARS['kodearea'];
$asal				= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan			= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$bulan			= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

$bulan	=($bulan!='')?$bulan:date("m");
$tahun	= ($tahun!='')?$tahun:date("Y");

$Cabang = new Cabang();

//=METHOD=========================================================
function setComboArea($area_dipilih){
	//SET COMBO AREA
	global $db;
	$Area = new Area();
			
	$result=$Area->ambilData("","KodeArea","ASC");
	$opt="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($area_dipilih!=$row['KodeArea'])?"":"selected";
			$opt .="<option value='$row[KodeArea]' $selected>$row[KodeArea] ($row[NamaArea])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt;
	//END SET AREA
}

function setOptAsal($area="",$cabang_dipilih=""){
		//SET COMBO cabang
		global $db;
		global $Cabang;
		
		$opt_cabang="<option value='' >-semua asal-</option>";
		
		$sql	=
			"SELECT DISTINCT(KodeCabangAsal) AS CabangAsal FROM tbl_md_jurusan WHERE 1 ".($area==""?"":" AND KodeArea='$area'")." ORDER BY CabangAsal";
		
		if (!$result= $db->sql_query($sql)){
			echo("Err:".__LINE__);exit;
		}
		
		$data_cabang	= array();
		
		while ($row = $db->sql_fetchrow($result)){
			$data_cabang[$row['CabangAsal']] = true;
		}
		
		$result_cabang = $Cabang->ambilData("","Kota,Nama","ASC");
		
		$kota="";
		
		while($row=$db->sql_fetchrow($result_cabang)){
			if($data_cabang[$row['KodeCabang']]){
				$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
				
				if($kota!=$row['Kota']){
						
					if($kota!=""){
						$opt_cabang .= "</optgroup>";
					}
						
					$kota=$row['Kota'];
					$opt_cabang .="<optgroup label='$kota'>";
				}
				
				$opt_cabang .="<option value='$row[KodeCabang]' $selected>".$row['Nama'] ."($row[KodeCabang])</option>";
			}
		}
		
		return $opt_cabang;
	}
	
	function setOptTujuan($area="",$cabang_asal,$cabang_dipilih=""){
		//SET COMBO cabang
		global $db;
		global $Cabang;
		
		$opt_cabang="<option value='' >-semua tujuan-</option>";
		
		$sql	=
			"SELECT DISTINCT(KodeCabangTujuan) AS CabangTujuan FROM tbl_md_jurusan WHERE KodeCabangAsal='$cabang_asal' ".($area==""?"":" AND KodeArea='$area'")." ORDER BY CabangTujuan";
		
		if (!$result= $db->sql_query($sql)){
			echo("Err:".__LINE__);exit;
		}
		
		$data_cabang	= array();
		
		while ($row = $db->sql_fetchrow($result)){
			$data_cabang[$row['CabangTujuan']] = true;
		}
		
		$result_cabang = $Cabang->ambilData("","Kota,Nama","ASC");
		
		$kota="";
		
		while($row=$db->sql_fetchrow($result_cabang)){
			if($data_cabang[$row['KodeCabang']]){
				$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
				
				if($kota!=$row['Kota']){
						
					if($kota!=""){
						$opt_cabang .= "</optgroup>";
					}
						
					$kota=$row['Kota'];
					$opt_cabang .="<optgroup label='$kota'>";
				}
				
				$opt_cabang .="<option value='$row[KodeCabang]' $selected>".$row['Nama'] ."($row[KodeCabang])</option>";
			}
		}
		
		return $opt_cabang;
	}
//=END METHOD====================

//=OPERASI===========================
switch($mode){
	case 'getasal':
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(\"$kode_area\",this.value);'>
				".setOptAsal($kode_area,$asal)."
			</select>";
	exit;
		
	case 'gettujuan':
		echo "
			<select name='tujuan' id='tujuan' >
				".setOptTujuan($kode_area,$asal,$tujuan)."
			</select>";
	exit;
}
//=END OPERASI==============================

//BODY
$kondisi_area			= $kode_area==""?"":" AND (SELECT KodeArea FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=tr.IdJurusan)='$kode_area'";

if($asal!=""){
	$kondisi_jurusan	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tr.IdJurusan)='$asal'";
	$kondisi_jurusan	.= $tujuan==""?"" : " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tr.IdJurusan)='$tujuan'";
}

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	//$list_bulan	.="<a href='".append_sid('laporan_omzet_cabang_grafik.'.$phpEx).$parameter."'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
	$list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}

// LIST
$template->set_filenames(array('body' => 'laporan_omzet/laporan_omzet_body.tpl')); 


//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}

//QUERY PENUMPANG TANGGAL HARI INI
if(date("Y-m")==$tahun."-".$bulan){
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
			IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
			IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
			IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
			IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
			IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,

			IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB, 

			IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
			IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
			IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
			IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
			IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
			IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),HargaTiket)),0) AS TotalPenjualanTiket, 
			IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
		FROM tbl_reservasi tr
		WHERE DAY(TglBerangkat)=DAY(NOW()) AND MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_area $kondisi_jurusan
		GROUP BY TglBerangkat";
	
	if ($result_penumpang = $db->sql_query($sql)){
		$data_penumpang_hari_ini = $db->sql_fetchrow($result_penumpang);
	} 
	else{
		//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
		echo("Error:".__LINE__);exit;
	}
}

//QUERY PENUMPANG OLAP
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,
		IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB, 
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),HargaTiket)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM tbl_reservasi_olap tr
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	$kondisi_area $kondisi_jurusan
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$sum_trip			= 0;
$sum_pnp_u			= 0;
$sum_pnp_m			= 0;
$sum_pnp_k			= 0;
$sum_pnp_kk			= 0;
$sum_pnp_g			= 0;
$sum_pnp_o			= 0;
$sum_pnp_rb			= 0;
$sum_pnp_r			= 0;
$sum_pnp_vr			= 0;
$sum_pnp_t			= 0;
$sum_omzet_net	= 0;


for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
	
	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	$odd ='odd';
	
	if (($idx_tgl % 2)==0){
		$odd = 'even';
	}
	
	//OMZET PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_trip			= $data_penumpang['TotalBerangkat'];
		$total_pnp_u		= $data_penumpang['TotalPenumpangU'];
		$total_pnp_m		= $data_penumpang['TotalPenumpangM'];
		$total_pnp_k		= $data_penumpang['TotalPenumpangK'];
		$total_pnp_kk		= $data_penumpang['TotalPenumpangKK'];
		$total_pnp_g		= $data_penumpang['TotalPenumpangG'];
		$total_pnp_o		= $data_penumpang['TotalPenumpangT'];
		$total_pnp_rb		= $data_penumpang['TotalPenumpangRB'];
		$total_pnp_r		= $data_penumpang['TotalPenumpangR'];
		$total_pnp_vr		= $data_penumpang['TotalPenumpangVR'];
		$total_pnp_t		= $data_penumpang['TotalTiket'];
		$total_pnp_trip	= ($data_penumpang['TotalBerangkat']>0)?$data_penumpang['TotalTiket']	/$data_penumpang['TotalBerangkat']:0;
		$total_omz_net	= $data_penumpang['TotalPenjualanTiket']-$data_penumpang['TotalDiscount']-$data_penumpang['TotalKomisiOnline'];
		
		$data_penumpang = $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_trip			= 0;
		$total_pnp_u		= 0;
		$total_pnp_m		= 0;
		$total_pnp_k		= 0;
		$total_pnp_kk		= 0;
		$total_pnp_g		= 0;
		$total_pnp_o		= 0;
		$total_pnp_rb		= 0;
		$total_pnp_r		= 0;
		$total_pnp_vr		= 0;
		$total_pnp_t		= 0;
		$total_pnp_trip	= 0;
		$total_omz_net	= 0;
		
	}
	
	if(date("Y-m-d")==$tahun."-".$bulan."-".($idx_tgl+1)){
		$total_trip			= $data_penumpang_hari_ini['TotalBerangkat'];
		$total_pnp_u		= $data_penumpang_hari_ini['TotalPenumpangU'];
		$total_pnp_m		= $data_penumpang_hari_ini['TotalPenumpangM'];
		$total_pnp_k		= $data_penumpang_hari_ini['TotalPenumpangK'];
		$total_pnp_kk		= $data_penumpang_hari_ini['TotalPenumpangKK'];
		$total_pnp_g		= $data_penumpang_hari_ini['TotalPenumpangG'];
		$total_pnp_o		= $data_penumpang_hari_ini['TotalPenumpangT'];
		$total_pnp_rb		= $data_penumpang_hari_ini['TotalPenumpangRB'];
		$total_pnp_r		= $data_penumpang_hari_ini['TotalPenumpangR'];
		$total_pnp_vr		= $data_penumpang_hari_ini['TotalPenumpangVR'];
		$total_pnp_t		= $data_penumpang_hari_ini['TotalTiket'];
		$total_pnp_trip	= ($data_penumpang_hari_ini['TotalBerangkat']>0)?$data_penumpang_hari_ini['TotalTiket']	/$data_penumpang_hari_ini['TotalBerangkat']:0;
		$total_omz_net	= $data_penumpang_hari_ini['TotalPenjualanTiket']-$data_penumpang_hari_ini['TotalDiscount']-$data_penumpang_hari_ini['TotalKomisiOnline'];
	}
	
	$sum_trip				+= $total_trip;
	$sum_pnp_u			+= $total_pnp_u;
	$sum_pnp_m			+= $total_pnp_m;
	$sum_pnp_k			+= $total_pnp_k;
	$sum_pnp_kk			+= $total_pnp_kk;
	$sum_pnp_g			+= $total_pnp_g;
	$sum_pnp_o			+= $total_pnp_o;
	$sum_pnp_rb			+= $total_pnp_rb;
	$sum_pnp_r			+= $total_pnp_r;
	$sum_pnp_vr			+= $total_pnp_vr;
	$sum_pnp_t			+= $total_pnp_t;
	$sum_omzet_net	+= $total_omz_net;
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'tgl'=>$tgl_transaksi,
				'total_keberangkatan'=>number_format($total_trip,0,",","."),
				'total_penumpang_u'=>number_format($total_pnp_u,0,",","."),
				'total_penumpang_m'=>number_format($total_pnp_m,0,",","."),
				'total_penumpang_k'=>number_format($total_pnp_k,0,",","."),
				'total_penumpang_kk'=>number_format($total_pnp_kk,0,",","."),
				'total_penumpang_g'=>number_format($total_pnp_g,0,",","."),
				'total_penumpang_o'=>number_format($total_pnp_o,0,",","."),
				'total_penumpang_rb'=>number_format($total_pnp_rb,0,",","."),
				'total_penumpang_r'=>number_format($total_pnp_r,0,",","."),
				'total_penumpang_vr'=>number_format($total_pnp_vr,0,",","."),
				'total_penumpang'=>number_format($total_pnp_t,0,",","."),
				'rata_pnp_per_trip'=>number_format($total_pnp_trip,0,",","."),
				'omzet_net'=>number_format($total_omz_net,0,",",".")
			)
		);
		
	$temp_hari++;
}
			

//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&kodearea=".$kode_area."&asal=".$asal."&tujuan=".$tujuan."&bulan=".$bulan."&tahun=".$tahun;

$script_cetak_excel="Start('laporan_omzet_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_omzet">Home</a> | <a href="'.append_sid('laporan_omzet.'.$phpEx).'">Laporan Omzet</a>',
	'URL'						=> append_sid('laporan_omzet.'.$phpEx).$parameter,
	'OPT_AREA'			=> setComboArea($kode_area),
	'KODE_AREA'			=> $kode_area,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'LIST_BULAN'		=> "| ".$list_bulan,
	'BULAN'					=> $bulan,
	'TAHUN'					=> $tahun,
	'GT_TRIP'				=>number_format($sum_trip,0,",","."),
	'GT_PNP_U'			=>number_format($sum_pnp_u,0,",","."),
	'GT_PNP_M'			=>number_format($sum_pnp_m,0,",","."),
	'GT_PNP_K'			=>number_format($sum_pnp_k,0,",","."),
	'GT_PNP_KK'			=>number_format($sum_pnp_kk,0,",","."),
	'GT_PNP_G'			=>number_format($sum_pnp_g,0,",","."),
	'GT_PNP_O'			=>number_format($sum_pnp_o,0,",","."),
	'GT_PNP_RB'			=>number_format($sum_pnp_rb,0,",","."),
	'GT_PNP_R'			=>number_format($sum_pnp_r,0,",","."),
	'GT_PNP_VR'			=>number_format($sum_pnp_vr,0,",","."),
	'GT_PNP_T'			=>number_format($sum_pnp_t,0,",","."),
	'GT_OMZ_NET'		=>number_format($sum_omzet_net,0,",","."),
	'U_LAPORAN_OMZET_GRAFIK'	=>append_sid('laporan_omzet_grafik.'.$phpEx).'&bulan='.$bulan.'&tahun='.$tahun,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>