<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_CSO,$LEVEL_CSO_PAKET))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$bulan  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tahun  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];


//INISIALISASI

		
//QUERY
//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}
	
//QUERY PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE MONTH(TglBerangkat)=$bulan 
		AND YEAR(TglBerangkat)=$tahun 
		AND CetakTiket=1 
		AND FlagBatal!=1
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("$sql Error:".__LINE__);exit;
}
	
//QUERY PAKET
$sql=
	"SELECT 
		WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(TotalBayar),0) AS TotalOmzet
	FROM tbl_paket
	WHERE MONTH(WaktuPesan)=$bulan 
		AND YEAR(WaktuPesan)=$tahun 
		AND CetakTiket=1 
		AND FlagBatal!=1
		AND IF(CaraPembayaran!=$PAKET_CARA_BAYAR_DI_TUJUAN,PetugasPenjual=$userdata[user_id],PetugasPemberi=$userdata[user_id])
	GROUP BY DATE(WaktuPesan)
	ORDER BY DATE(WaktuPesan)";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//QUERY BIAYA
$sql=
	"SELECT 
		WEEKDAY(TglTransaksi)+1 AS Hari,DAY(TglTransaksi) AS Tanggal,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE MONTH(TglTransaksi)=$bulan 
		AND YEAR(TglTransaksi)=$tahun
		AND IdPetugas=$userdata[user_id]
	GROUP BY TglTransaksi
	ORDER BY TglTransaksi ";

if ($result_biaya = $db->sql_query($sql)){
	$data_biaya = $db->sql_fetchrow($result_biaya);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

/*$sql=
	"SELECT 
		WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount,
		$sql_total_biaya AS TotalBiaya,
		IS_NULL(SUM(SubTotal),0)-$sql_total_biaya AS TotalProfit
	FROM tbl_reservasi
	WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY DATE(WaktuCetakTiket)
	ORDER BY DATE(WaktuCetakTiket)";*/

	
$sum_penumpang						= 0;
$sum_paket								= 0;
$sum_omzet_penumpang			= 0;
$sum_omzet_paket					= 0;
$sum_discount							= 0;
$sum_pendapatan_penumpang	= 0;
$sum_biaya								= 0;
$sum_profit								= 0;

	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,80);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Omzet','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,BulanString($bulan).' '.$tahun,'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
#$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(30,5,'Tanggal','B',0,'C',1);
$pdf->Cell(30,5,'Jum.Pnp','B',0,'C',1);
$pdf->Cell(30,5,'Omzet pnp.','B',0,'C',1);
$pdf->Cell(30,5,'Disc.','B',0,'C',1);
$pdf->Cell(30,5,'Pend.pnp.','B',0,'C',1);
$pdf->Cell(30,5,'Jum.pkt.','B',0,'C',1);
$pdf->Cell(30,5,'Omzet pkt.','B',0,'C',1);
$pdf->Cell(30,5,'OP Langsung','B',0,'C',1);
$pdf->Cell(30,5,'L/B Kotor','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

  
for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$odd ='odd';
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
	
	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	//OMZET PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_penumpang				= $data_penumpang['TotalPenumpang']; 
		$total_omzet_penumpang	= $data_penumpang['TotalOmzet']; 
		$total_discount					= $data_penumpang['TotalDiscount'];
		$pendapatan_penumpang		= $total_omzet_penumpang-$total_discount;
		$total_profit						= $pendapatan_penumpang;
		$data_penumpang 				= $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_penumpang				= 0;
		$total_omzet_penumpang	= 0;
		$total_discount					= 0;
		$pendapatan_penumpang		= 0;
		$total_profit						= 0;
	}
	
	//OMZET PAKET
	if($data_paket['Tanggal']==$idx_tgl+1){
		$total_paket				= $data_paket['TotalPaket']; 
		$total_omzet_paket	= $data_paket['TotalOmzet'];
		$total_profit				+= $total_omzet_paket;
		$data_paket 				= $db->sql_fetchrow($result_paket);
	}
	else{
		$total_paket				= 0;
		$total_omzet_paket	= 0;
	}
	
	//OMZET BIAYA
	if($data_biaya['Tanggal']==$idx_tgl+1){
		$total_biaya		= $data_biaya['TotalBiaya']; 
		$total_profit		-= $total_biaya;
		$data_biaya 		= $db->sql_fetchrow($result_biaya);
	}
	else{
		$total_biaya		= 0;
	}
	
	$sum_penumpang				+= $total_penumpang;
	$sum_paket						+= $total_paket;
	$sum_omzet_penumpang	+= $total_omzet_penumpang;
	$sum_omzet_paket			+= $total_omzet_paket;
	$sum_biaya						+= $total_biaya;
	$sum_discount					+= $total_discount;
	$sum_pendapatan_penumpang	+= $pendapatan_penumpang ;
	$sum_profit						+= $total_profit;
	
	#$pdf->Cell(5,5,$i,'',0,'C');
	$pdf->Cell(30,5,$tgl_transaksi.'  ','',0,'L');
	$pdf->Cell(30,5,number_format($total_penumpang,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_omzet_penumpang,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_discount,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($pendapatan_penumpang,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_paket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_omzet_paket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_biaya,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_profit,0,",","."),'',0,'R');
	$pdf->Ln();
	$pdf->Cell(270,1,'','B',0,'');
	$pdf->Ln();
	
	$i++;
	$temp_hari++;
}


//FOOTER
$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
#$pdf->Cell(5,5,'','B',0,'C',1);
$pdf->Cell(30,5,'TOTAL','B',0,'C',1);
$pdf->Cell(30,5,number_format($sum_penumpang,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_omzet_penumpang,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_discount,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_pendapatan_penumpang,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_paket,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_omzet_paket,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_biaya,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_profit,0,",","."),'',0,'R',1);
$pdf->Ln();
$pdf->Ln();
									
$pdf->Output();
						
?>