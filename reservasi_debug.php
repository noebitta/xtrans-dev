<?php
//
// RESERVASI
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_CSO,$LEVEL_CSO_PAKET))){  
  if(!isset($HTTP_GET_VARS['mode'])){
		redirect(append_sid('index.'.$phpEx),true); 
	}
	else{
		echo("loading...");exit;
	}
}

include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassUser.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$user_id  	= $userdata['user_id']; 
$user_level	= $userdata['user_level'];

// membuat option Asal
function OptionAsal($kota){
	global $db;
	
	$Cabang	= new Cabang();
	
	$result	= $Cabang->setComboCabang($kota);
	
  $opt = "";
	
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
      $opt .= "<option value='$row[0]'>$row[1]</option>";
    }    
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
			
  return $opt;
}

// membuat option jurusan
function OptionJurusan($asal){
	
	global $db;
	
	$Jurusan	= new Jurusan();
	
	$result = $Jurusan->setComboJurusan($asal);
	
  $opt = "";
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
			$opt .= "<option value='$row[0]'>$row[1] ($row[2])</option>";
    }  
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
	
  return $opt;
}

// membuat option jam
function OptionJadwal($tgl,$id_jurusan){
  global $db;
	
  if ($id_jurusan!=''){
		
		$Jadwal	= new Jadwal();
		
		$result	= $Jadwal->setComboJadwal($tgl,$id_jurusan);
		
	  $opt = "";
		
	  if ($result){
			$opt = "<option>(none)</option>" . $opt;  
			while ($row = $db->sql_fetchrow($result)){
				$opt .= "<option value='$row[0]'>$row[0]-$row[1]</option>";
			}    
	  } 
		else{
			$opt .="<option selected=selected>Error</option>";
	  }
		
		/*if ($result){
			while ($row = $db->sql_fetchrow($result)){
				$opt .= "<a href='#' id='$row[0]' onClick=\"p_jadwal.value='$row[0]';changeRuteJam();pilihJadwal(this);return false;\">$row[1] ($row[0])</a><hr>";
			}    
			
			$opt	= substr($opt,0,-4);
	  } 
		else{
			$opt .="Gagal mengambil data";
	  }*/
  }  
  
  return $opt;
}

function setListDiscount($kode_jadwal,$tgl_berangkat,$kode_discount=NULL){
		
		global $db;
		
		$sql = "SELECT FlagLuarKota,f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','$tgl_berangkat') AS HargaTiket
            FROM   tbl_md_jurusan
						WHERE IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal')";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota	= $row['FlagLuarKota'];
		$harga_tiket		= $row['HargaTiket'];
		
    $sql = "SELECT IdDiscount,NamaDiscount,JumlahDiscount,KodeDiscount
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota'
            ORDER BY NamaDiscount ASC";
	  
		$opt = "<option value=''>UMUM Rp. ".number_format($harga_tiket,0,",",".")."</option>";
		
	  if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				
				if($row['JumlahDiscount']>1){
					$besar_discount	= $row['JumlahDiscount'];
				}
				else{
					$besar_discount	= $harga_tiket*$row['JumlahDiscount'];
				}
				
				if($row['KodeDiscount']=="G"){
					$keterangan	= "GRATIS";
				}
				elseif($row['KodeDiscount']=="KK"){
					$keterangan	= "Keluarga Karyawan Rp. ".number_format($harga_tiket-$besar_discount,0,",",".");
				}
				elseif($row['KodeDiscount']=="M"){
					$keterangan	= "MAHASISWA/GRUP. Rp. ".number_format($harga_tiket-$besar_discount,0,",",".");
				}
				elseif($row['KodeDiscount']=="V"){
					$keterangan	= "VOUCHER Rp. ".number_format($harga_tiket-$besar_discount,0,",",".");
				}
				
				$selected	= ($kode_discount!=$row['KodeDiscount'])?"":"selected";
				
	      $opt .= "<option value=$row[IdDiscount] $selected>$keterangan</option>";
	    } 
	  } 
		else{ 
			return "Error";
		}
		
		return $opt;
}

//PEMILIHAN PROSES
switch ($mode){
	
//HARGA TIKET ===========================================================================================================================================
case "hargatiket":
  $rute = $HTTP_GET_VARS['rute'];
  $harga_tiket = getHargaTiket($rute);
      
  echo $harga_tiket;
  exit;

// PERIKSA NO TELP ===========================================================================================================================================================================================
case "periksa_no_telp":
	$no_telp 			= $HTTP_GET_VARS['no_telp'];
	$flag_paket 	= $HTTP_GET_VARS['paket'];
	$flag_pengirim= $HTTP_GET_VARS['flag'];
	
	include($adp_root_path . 'ClassPelanggan.php');
	
	$Pelanggan = new Pelanggan();
	
	$data_pelanggan=$Pelanggan->ambilDataDetail($no_telp);
  
	if(count($data_pelanggan)>1){	
		if($flag_paket!=1){
			echo("
				document.getElementById('hdn_no_telp_old').value='".trim($data_pelanggan['NoTelp'])."';
				document.getElementById('fnama').value='".trim($data_pelanggan['Nama'])."';
				document.getElementById('ftelp').value='".trim($data_pelanggan['NoTelp'])."';
			");
		}
		else{
			if($flag_pengirim==1){
				echo("
					document.getElementById('dlg_paket_nama_pengirim').value='".trim($data_pelanggan['Nama'])."';
					document.getElementById('dlg_paket_alamat_pengirim').value='".trim($data_pelanggan['Alamat'])."';
					document.getElementById('dlg_paket_telp_pengirim').value='".trim($data_pelanggan['NoTelp'])."';
				");
			}
			else{
				echo("
					document.getElementById('dlg_paket_nama_penerima').value='".trim($data_pelanggan['Nama'])."';
					document.getElementById('dlg_paket_alamat_penerima').value='".trim($data_pelanggan['Alamat'])."';
					document.getElementById('dlg_paket_telp_penerima').value='".trim($data_pelanggan['NoTelp'])."';
				");
			}
		}
	}
	else{
		echo(0);
	}
	
  exit;
  
 // PERIKSA ID Member ===========================================================================================================================================================================================
case "periksa_id_member":
	$id_member = $HTTP_GET_VARS['id_member'];
	
	$Member = new Member();
	
	$data_member=$Member->ambilData($id_member);
  
	if(count($data_member)>1){	
		
		echo("
			if('".substr($data_member['TglLahir'],-5)."'=='".substr(dateNow(),-5)."'){
				alert('Ucapkan SELAMAT ULANG TAHUN pada member ini, karena member ini sedang berulang tahun hari ini!');
			}
			
			if($data_member[MasaBerlaku]>$THRESHOLD_MEMBER_EXPIRED){
				if(!document.getElementById('hdn_ubah_id_member')){
					document.getElementById('hdn_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('fnama').value='".trim($data_member['Nama'])."';
					document.getElementById('ftelp').value='".trim($data_member['Handphone'])."';
				}
				else{
					document.getElementById('hdn_ubah_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('ubah_nama_penumpang').value='".trim($data_member['Nama'])."';
					document.getElementById('ubah_telp_penumpang').value='".trim($data_member['Handphone'])."';
				}
			}
			else if($data_member[MasaBerlaku]>0 && $data_member[MasaBerlaku]<=$THRESHOLD_MEMBER_EXPIRED){
				if(!document.getElementById('hdn_ubah_id_member')){
					document.getElementById('hdn_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('fnama').value='".trim($data_member['Nama'])."';
					document.getElementById('ftelp').value='".trim($data_member['Handphone'])."';
				}
				else{
					document.getElementById('hdn_ubah_id_member').value='".trim($data_member['IdMember'])."';
					document.getElementById('ubah_nama_penumpang').value='".trim($data_member['Nama'])."';
					document.getElementById('ubah_telp_penumpang').value='".trim($data_member['Handphone'])."';
				}
				
				alert('Masa berlaku keanggotan sebentar lagi akan berakhir, mohon informasikan kepada pelanggan');
			}
			else{
				
				if(!document.getElementById('hdn_ubah_id_member')){
					document.getElementById('hdn_id_member').value='';
				}
				else{
					document.getElementById('hdn_ubah_id_member').value='';
				}
				
				alert('Maaf, masa keanggotaan member yang bersangkutan sudah berakhir');
			}
			
		");
	}
	else{
		echo(0);
	}
	
  exit;
	
// PERIKSA MEMBER ===========================================================================================================================================================================================
case "periksa_member_by_kartu":
	$no_seri_kartu = $HTTP_GET_VARS['no_kartu'];
	
	$Member = new Member();
	
	$data_member=$Member->ambilDataByNoSeriKartu($no_seri_kartu);
  
	if(count($data_member)>1){	
		echo("
			document.getElementById('hdn_id_member').value='".trim($data_member['id_member'])."';
			document.getElementById('fnama').value='".trim($data_member['nama'])."';
			document.getElementById('fadd').value='".trim($data_member['alamat'])." ".trim($data_member['kota'])."';
			document.getElementById('ftelp').value='".trim($data_member['telp_rumah'])."';
		");
	}
	else{
		echo("false");
	}
	
  exit;

//Asal  ===========================================================================================================================================================================================
case "asal":
    // membuat nilai awal..
	$kota	 = $HTTP_GET_VARS['kota_asal'];
	
	$oasal = OptionAsal($kota);
	 
	echo("
	  <table bgcolor='white' width='100%'> 
		  <tr><td>POINT KEBERANGKATAN<br></td></tr>
	    <tr>
				<td><select onchange='getUpdateTujuan(this.value);' id='rute_asal' name='rute_asal'>$oasal</select><span id='progress_asal' style='display:none;'><img src='./templates/images/loading.gif' /></span>
				</td>
			</tr>
		</table>");

	exit;

//TGL ===========================================================================================================================================================================================
case "tujuan":
    // membuat nilai awal..
	$asal=$HTTP_GET_VARS['asal'];
	
	$ojur = OptionJurusan($asal);
	  
	echo("
	    <table>
				<tr><td>POINT TUJUAN<br></td></tr>
	      <tr>
					<td><select onchange='getUpdateJadwal(this.value);' id='tujuan' name='tujuan'>$ojur</select><span id='progress_tujuan' style='display:none;'><img src='./templates/images/loading.gif' /></span>
					</td>
				</tr>
			</table>");
	exit;
	
// JAM ===========================================================================================================================================================================================
case "jam":
  // memilih jam yang tersedia sesuai dengan rute yang dimasukan
  $tgl 				= $HTTP_GET_VARS['tgl'];
	$id_jurusan = $HTTP_GET_VARS['id_jurusan'];
  $ojam 			= OptionJadwal($tgl,$id_jurusan);
  echo("
	<table bgcolor='white' width='100%'>
		<tr><td>JAM KEBERANGKATAN<br></td></tr>
	  <tr><td><select onchange='changeRuteJam();' name='p_jadwal' id='p_jadwal'>$ojam</select><span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span></td></tr>
  </table>");  
	
	/*echo("
	<table bgcolor='white' width='100%'>
		<tr><td>JAM KEBERANGKATAN<br></td></tr>
	  <tr><td>$ojam</td></tr>
  </table>");*/
  	
  exit;
	 
// MOBIL ===========================================================================================================================================================================================
case "mobil":
	//SOURCE BARU*******************************************************************************************
	// memilih mobil yang tersedia sesuai dengan tanggal, rute dan jam yang dimasukan
	//echo(date("H:i:s")); //speed test, ambil waktu awal
	
	if($userdata['user_level']==$LEVEL_CSO_PAKET){
		exit;
	}
	
	include($adp_root_path . 'ClassLayoutKendaraanDebug.php');
  include($adp_root_path . 'ClassPenjadwalanKendaraan.php');
	
	//VARIABLE UNTUK CASE MOBIL
	$Jadwal								= new Jadwal();
	$PenjadwalanKendaraan	= new PenjadwalanKendaraan();
	$Reservasi						= new Reservasi();
	
	$LayoutKendaraan			= new LayoutKendaraan();
	$tgl_mysql						= $HTTP_GET_VARS['tanggal'];
  $tgl  								= FormatMySQLDateToTgl($tgl_mysql); // tanggal    
  $kode_jadwal 					= $HTTP_GET_VARS['jadwal'];    // jam
	$kode_booking_dipilih	= $HTTP_GET_VARS['kode_booking_dipilih'];    // jam
	
	if($kode_jadwal!="" && $kode_jadwal!="(none)"){
	
		//mengambil  data jadwal
			
		$row = $Jadwal->ambilDataDetail($kode_jadwal);
	  $jam_berangkat	= $row['JamBerangkat'];
	  $layout_kursi 	= $row['JumlahKursi'];
		$flag_sub_jadwal= $row['FlagSubJadwal'];
		$kode_jadwal_utama= ($flag_sub_jadwal)? $row['KodeJadwalUtama']:$kode_jadwal;
		
		//MEMERIKSA HAK AKSES, JIKA CSO BIASA TIDAK BOLEH MEMESAN PADA WAKTU YANG SUDAH LALU
		if(!$Reservasi->periksaHakAkses($tgl_mysql,$jam_berangkat) && $user_level>=$LEVEL_CSO){
			echo("<br><br><br><br><br><br>
				<img src='./templates/images/icon_warning.png' />
				<font color='red'><h3>Anda tidak boleh memilih waktu yang sudah lalu!</h3></font>");
			exit;
		}
		//--END PERIKSA HAK AKSES
		
		//MEMERIKSA AKTIF TIDAKNYA JADWAL
		$data_jadwal_utama	= $Jadwal->ambilDataDetail($kode_jadwal_utama);
	  $flag_aktif					= $data_jadwal_utama['FlagAktif'];
		
		/*if(!$flag_aktif || $flag_aktif==''){
			echo("<br><br><br><br><br><br>
				<img src='./templates/images/icon_warning.png' />
				<font color='red'><h3>(1) Jadwal ini tidak dioperasikan!</h3></font>");
			exit;
		}*/
		
		//mengambil harga tiket
		$harga_tiket=$Reservasi->getHargaTiket($kode_jadwal,$tgl_mysql);
		
		//mengambil  data kendaraan dan sopir
			
		$row						= $PenjadwalanKendaraan->ambilDataDetail($tgl_mysql,$kode_jadwal_utama);
		$kode_kendaraan	= $row['KodeKendaraan'];
		$no_polisi 			= $row['NoPolisi'];
	  $kode_sopir 		= $row['KodeDriver'];
	  $nama_sopir 		= $row['NamaSopir'];
		$status_aktif 	= $row['StatusAktif'];
	  $layout_kursi 	= ($row['LayoutKursi']=='')?$layout_kursi:$row['LayoutKursi'];
		
		//MEMERIKSA AKTIF TIDAKNYA JADWAL
		
		if(($status_aktif==0 && $status_aktif!='') || ($flag_aktif==0 && $status_aktif=='')){
			echo("<br><br><br><br><br><br>
				<img src='./templates/images/icon_warning.png' />
				<font color='red'><h3>Jadwal ini tidak dioperasikan!</h3></font>");
			exit;
		}
		
		//LAYOUT KENDARAAN
		
		//memeriksa flag sub jadwal, jika jadwal ini merupakan subjadwal dari jadwal yang lain , maka layoutnya akan mengambil jadwal induknya
		$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
		
		
		//mengambil header dari layout kursi
		$row				= $Reservasi->ambilDataHeaderLayout($tgl_mysql,$kode_jadwal_aktual);
		$id 				= $row['ID'];
		$no_spj			= $row['NoSPJ'];
		
		/*
		//ambil memo
		$flag_memo				= $row['FlagMemo'];
		$memo							= $row['Memo'];
		$pembuat_memo			= $row['PembuatMemo'];
		$waktu_buat_memo	= $row['WaktuBuatMemo'];
		
		//$ada_memo_baru	= ($flag_memo==0)?"":"<blink><font size=2 color='red'><b><-Ada memo baru</b></font></blink>";
		
		$isi_memo="MEMO: \\n";
		
		include($adp_root_path . 'ClassUser.php');
		$User = new User();
		
		$pembuat_memo			= $User->ambilNamaById($pembuat_memo);
		$waktu_buat_memo	= dateparse(FormatMySQLDateToTglWithTime($waktu_buat_memo));
		$isi_memo				 .= "From :$pembuat_memo@$waktu_buat_memo\\n $memo \\n";
		
		if($flag_memo==0){
			$show_memo ="";
		}
		else{
			$show_memo ="&nbsp;&nbsp;<a href='javascript:onClick=alert(\"$isi_memo\")'><blink>Lihat Memo</blink></a>&nbsp;";
		}*/
		
		if($no_spj!=""){
			$kode_kendaraan= $row['KodeKendaraan'];
			$no_polisi		= $row['NoPolisi'];
			$kode_sopir 	= $row['KodeSopir'];
			$nama_sopir 	= $row['NamaSopir'];
		}
		
		if(is_null($id)){
			//jika belum ada transaksi untuk tanggal ini pada keberangkatan jam ini maka akan dibuat recordnya
			
			//MENAMBAHKAN LAYOUT KE TABEL POSISI
			
			if (!$result = $Reservasi->tambahPosisi(
				$kode_jadwal_aktual, $tgl_mysql, $jam_berangkat,
				$layout_kursi, $kode_kendaraan, $kode_sopir)){
				
				echo("Error ".__LINE__);
				exit;
			} 
		}

		$LayoutKendaraan->kode_booking_dipilih=$kode_booking_dipilih;
		//$error=$LayoutKendaraan->kode_booking_dipilih;
		//<input type='button' value='Cetak banyak tiket' onclick='PilihTiket();'></input>
		
		//LAYOUT MOBIL
		echo("
			<input type='hidden' value='$jam_berangkat' id='jam_berangkat_aktif' />
			<table cellspacing='0' cellpadding='0' width='400'>
				<tbody>
				<tr>
					<td align='center'>
						<table width='100%'>
							<tr>
								<td width='25%'><strong>No SPJ</strong></td>
								<td>:</td>
								<td>
									$no_spj
									<input type='hidden' readonly='yes' value='$no_spj' name='txt_spj' id='txt_spj' />
								</td>
							</tr>
							<tr>
								<td><strong>Plat</strong></td>
								<td>:</td>
								<td>
									$no_polisi (Body: $kode_kendaraan)
									<input type='hidden' readonly='yes' value='$no_polisi' name='plat' id='plat' />
								</td>
							</tr>
							<tr>
								<td><strong>Harga Tiket</strong></td>
								<td>:</td>
								<td><strong>Rp. ".number_format($harga_tiket,0,",",".")."</strong></td>
							</tr>
							<tr>
								<td colspan='3' align='right'>
									<input type='button' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refresh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' onclick='getUpdateMobil();getPengumuman();setSisaKursi()' />
									<input type='hidden' value='$kode_kendaraan' name='hide_mobil_sekarang' id='hide_mobil_sekarang'></input>
									<input type='hidden' value='$layout_kursi' name='hide_layout_kursi' id='hide_layout_kursi'></input>
									<input type='hidden' name='hide_nama_sopir_sekarang' id='hide_nama_sopir_sekarang' value='$kode_sopir'>
								</td>
							</tr>
						</table>
					</td>	
				</tr>
				<tr>
					<td colspan='3' valign='middle' align='center' height='20' bgcolor='d0d0d0'>
						<table width='100%' cellspacing=0 cellpadding=0>
							<tr>
								<td width='85%'>$show_memo</td>
								<td width='15%' valign='middle' align='right'>
									<span id='progress_kursi' style='display:none;'><img src='./templates/images/loading.gif' /></span>&nbsp;
								</td>
							<tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='center'>".
						$LayoutKendaraan->setLayoutKursi($tgl_mysql,$kode_jadwal_aktual,$kode_jadwal,$layout_kursi,$nama_sopir)
					."</td>
				</tr>
				<tr>
					<td width='231'>$error</td>
				</tr>
				</tbody>
			</table>
		");
  }
	else{
		echo("<br><br><br><br><br><br><img src='./templates/images/icon_warning.png' /><font color='red'><h3>Anda belum memilih jadwal keberangkatan!</h3></font>");
	}
	//echo("<br>".date("H:i:s")); //speed test, ambil waktu awal
	exit;

// PENGUMUMAN LAYOUT===========================================================================================================================================================================================
case "pengumuman":
  // menampilkan pengumuman
	include_once($adp_root_path . 'ClassPengumuman.php');
	
	$Pengumuman	= new Pengumuman();
	
  if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
		//HANYA BISA DIAKSES OLEH SELAIN AGEN
		
	  $jumlah_pengumuman	= $Pengumuman->ambilJumlahPengumumanBaru($userdata['user_id']);
			
		if($jumlah_pengumuman>0){
			$jumlah_pengumuman	= "<blink>".$jumlah_pengumuman."</blink>";
			$blink_Pengumuman		= "<blink>Pesan</blink>";
		}
		else{
			$jumlah_pengumuman	= "";
			$blink_Pengumuman		= "Pesan";
		}
		
		//$isi_pengumuman
		
		$baca_pengumuman	= "Start('".append_sid('pengumuman_inbox.'.$phpEx)."');getPengumuman();return false;";
		
		echo("
			<a class='menu' href='#' onClick=$baca_pengumuman>&nbsp;&nbsp;&nbsp;&nbsp;<font color='red' size=5><b>$jumlah_pengumuman</b></font>&nbsp;&nbsp;&nbsp;&nbsp;
			<br><br><br>
			$blink_Pengumuman</a>
		");

	}
		
  exit;
break;

// SHOW CHAIR ===========================================================================================================================================================================================
case "showchair":
  // mendapatkan nilai kursi yang tersedia dan value nilai lain sesuai dengan tanggal, rute, jam dan kode
	include($adp_root_path . 'ClassAsuransi.php');
			
	$Asuransi	= new Asuransi();
	
	  $kursi  		= trim($HTTP_GET_VARS['kursi']);
		$no_tiket		= trim($HTTP_GET_VARS['no_tiket']);
	  $no_spj			= trim($HTTP_GET_VARS['no_spj']);
		
		$Reservasi		= new Reservasi();
		$User					= new User();
		
		$data_kursi = $Reservasi->ambilDataKursi($no_tiket);
		
		if(count($data_kursi)==0){
			echo("
				<table border='0' class='border' width='100%' height='500'> 
					<tr>
						<td width='40%' colspan='3'><h3>Data tidak ditemukan</h3></td>
					</tr>
				</table>
			");
			exit;
		}
				
		$kode_jadwal	=$data_kursi['KodeJadwal'];
		$tgl_berangkat=$data_kursi['TglBerangkat'];
		$harga_tiket	=number_format($data_kursi['HargaTiket'],0,",",".");
		$discount			=number_format($data_kursi['Discount'],0,",",".");
		$sub_total		=number_format($data_kursi['SubTotal'],0,",",".");
		$total				=number_format($data_kursi['Total'],0,",",".");
		$nama_cso			=$User->ambilNamaById($data_kursi['PetugasPenjual']);
		$cetak_tiket	=$data_kursi['CetakTiket'];
		$id_member		=trim($data_kursi['IdMember']);
		$kode_booking	=$data_kursi['KodeBooking'];
		$jenis_penumpang	= $data_kursi['JenisPenumpang'];
						
		//Mengambil data total bayar
		$data_total = $Reservasi->ambilTotalPesananByKodeBooking($kode_booking);
				
		$sub_total			=($data_total['SubTotal']!='')?$data_total['SubTotal']:0;
		$jumlah_kursi		=($data_total['JumlahKursi']!='')?$data_total['JumlahKursi']:0;
		$total_discount	=($data_total['TotalDiscount']!='')?$data_total['TotalDiscount']:0;
		$total_bayar		=($data_total['TotalBayar']!='')?$data_total['TotalBayar']:0;
					
		$sub_total			= number_format($sub_total,0,",",".");
		$jumlah_kursi		= number_format($jumlah_kursi,0,",",".");
		$total_discount	= number_format($total_discount,0,",",".");
		$total_bayar		= number_format($total_bayar,0,",",".");
					
		if($cetak_tiket==0){
			$status_pesanan="Booking";
			$waktu_cetak_tiket="";
		}
		else{
			$status_pesanan="Tiket telah dicetak";
			$waktu_cetak_tiket = 
				"<tr>
					<td>Waktu Cetak</td><td>:</td>
					<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuCetakTiket']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Dicetak oleh</td><td>:</td>
					<td><strong>".$User->ambilNamaById($data_kursi['PetugasCetakTiket'])."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</td>
				</tr>";
		}
		
		if($data_kursi['Pemutasi']==''){
			$status_mutasi="";
		}
		else{
			$status_mutasi = 
				"<tr>
					<td>Status Mutasi</td><td>:</td>
					<td><b>MUTASI</b></td>
				</tr>
				<tr>
					<td>Waktu Mutasi</td><td>:</td>
					<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuMutasi']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Dimutasi oleh</td><td>:</td>
					<td><strong>".$User->ambilNamaById($data_kursi['Pemutasi'])."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</td>
				</tr>";
		}
		
		if(!$cetak_tiket){
		
			$text_id_member	= 
				"<tr><td>ID Member</td><td>:</td><td><input name='hdn_ubah_id_member' id='hdn_ubah_id_member' type='hidden' value='$id_member'/>
					<input name='ubah_id_member' id='ubah_id_member' type='text' value='$id_member' onBlur=\"if(this.value==''){Element.show('rewrite_ubah_list_discount');Element.hide('rewrite_ubah_keterangan_member');}else{cariDataMemberById(this.value);Element.hide('rewrite_ubah_list_discount');Element.show('rewrite_ubah_keterangan_member');}\" onFocus=\"Element.hide('progress_cari_member');\">
				</td></tr>
				<tr>
					<td colspan=3 align='center' height=30 >
						<span id='label_member_not_found' style='display:none;'>
							<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>ID Member belum pernah terdaftar</b></font></td></tr></table>
						</span>
						<span id='progress_cari_member' style='display:none;'>
							<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data member...</b></font></td></tr></table>
						</span>
					</td>
				</tr>";
			
			if($id_member==''){
				$display_pelanggan_setia	= "style='display:none'";
				$display_list_discount		= "";
				
				$temp_tgl_lahir	= explode("-",$Asuransi->ambilTglLahirTerakhir($data_kursi['Telp']));
			}
			else{
				$display_pelanggan_setia	= "";
				$display_list_discount		= "style='display:none'";
				
				$Member	= new Member();
				
				$data_member	= $Member->ambilData($id_member);
				
				$temp_tgl_lahir	= explode("-",$data_member['TglLahir']);
			}
			
			$tgl_lahir	= $temp_tgl_lahir[2];
			$bln_lahir	= $temp_tgl_lahir[1];
			$thn_lahir	= $temp_tgl_lahir[0];
			
			//MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU JIKA DITEMUKAN ADA PROMO, MAKA DISCOUNT YANG LAIN TIDAK BERLAKU
			include($adp_root_path . 'ClassPromo.php');
			$Promo	= new Promo();
			
			$data_discount_point	= $Promo->ambilDiscountPoint($data_kursi['IdJurusan'],$kode_jadwal,$tgl_berangkat);
			
			$target_promo	= $data_discount_point['FlagTargetPromo'];
			
			if($target_promo=='' || $target_promo==1){
				$opt_list_discount = 
					"<select id='id_discount'>".
						setListDiscount($kode_jadwal,$tgl_berangkat,$jenis_penumpang)
					."</select>";
			}
			else{
				$besar_discount			= ($data_discount_point['JumlahDiscount']>1)?"Rp.".number_format($data_discount_point['JumlahDiscount'],0,",","."):$data_discount_point['JumlahDiscount']."%";
				$opt_list_discount	="<b>Promo Discount $besar_discount<b><input type='hidden' id='id_discount' value=''/>";
			}
			//--END MEMERIKSA DISCOUNT
			
			//SET COMBO ASURANSI
			
			$data_asuransi_penumpang	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
			
			if (!$result = $Asuransi->ambilData("NamaPlan","ASC")){
				$opt_plan_asuransi="Error";
			}
			
			$opt_plan_asuransi = "<input type='hidden' id='id_asuransi' value='$data_asuransi_penumpang[IdAsuransi]' />
				<select id='plan_asuransi' onChange='if(this.value==0){Element.hide(\"show_tgl_lahir\");}else{Element.show(\"show_tgl_lahir\");}'>";
		  
			$opt_plan_asuransi.="<option value=0>NO PLAN</option>";
			
			while ($row = $db->sql_fetchrow($result)){
				$selected	= ($data_asuransi_penumpang['PlanAsuransi']!=$row[0])?"":"selected";
				$opt_plan_asuransi.="<option value=$row[0] $selected>$row[NamaPlan] (Premi=Rp. ".number_format($row['BesarPremi'],0,",",".").")</option>";
			}
			
			$opt_plan_asuransi.="</select>";
			
			//SET combo tanggal lahir 
			
			$temp_tgl_lahir_asuransi	= explode("-",$data_asuransi_penumpang['TglLahir']);
			
			//tanggal
			$opt_tanggal_lahir	= "<span id='span_tgl_lahir'><select id='opt_tgl_lahir'>";
			
			$tgl_lahir	= ($temp_tgl_lahir_asuransi[2]!="")?$temp_tgl_lahir_asuransi[2]:$tgl_lahir;
			
			for($tgl=1;$tgl<=31;$tgl++){
				$selected	=(substr("0".$tgl,-2)!=$tgl_lahir)?"":"selected";
				$opt_tanggal_lahir	.= "<option value='$tgl' $selected>$tgl</option>";
			}
			
			$opt_tanggal_lahir	.= "</select></span>";
			
			//bulan
			$opt_bulan_lahir	= "<select id='opt_bln_lahir' onChange='setComboTglLahir(document.getElementById(\"opt_tgl_lahir\").value,this.value,document.getElementById(\"opt_thn_lahir\").value);'>";
			
			$bln_lahir	= ($temp_tgl_lahir_asuransi[1]!="")?$temp_tgl_lahir_asuransi[1]:$bln_lahir;
			
			for($bln=1;$bln<=12;$bln++){
				$selected	=(substr("0".$bln,-2)!=$bln_lahir)?"":"selected";
				$opt_bulan_lahir	.= "<option value='$bln' $selected>".BulanString($bln)."</option>";
			}
			
			$opt_bulan_lahir	.= "</select>";
			
			//tahun
			$opt_tahun_lahir	= "<select id='opt_thn_lahir' onChange='setComboTglLahir(document.getElementById(\"opt_tgl_lahir\").value,document.getElementById(\"opt_bln_lahir\").value,this.value);'>";
			
			$thn_min	= date("Y")-90;
			
			$thn_lahir	= ($temp_tgl_lahir_asuransi[0]!="")?$temp_tgl_lahir_asuransi[0]:$thn_lahir;
			$thn_lahir	= ($thn_lahir=="")?date("Y")-20:$thn_lahir;
			
			for($thn=$thn_min;$thn<=date("Y");$thn++){
				$selected	=($thn!=$thn_lahir)?"":"selected";
				$opt_tahun_lahir	.= "<option value='$thn' $selected>$thn</option>";
			}
			
			$opt_tahun_lahir	.= "</select>";
			
			//-END set combo tanggal lahir
			
			$display_tgl_lahir	= ($data_asuransi_penumpang['IdAsuransi']=="")?"none":"yes";
			
			//--END SET COMBO ASURANSI
			
			$kolom_data_penumpang	="
				<tr>
					<td>Telepon</td><td>:</td>
					<td><input type='text' id='ubah_telp_penumpang' value='$data_kursi[Telp]' onkeypress='validasiNoTelp(event);' onFocus=\"Element.hide('ubah_telp_invalid');\" /><span id='ubah_telp_invalid' style='display:none;'><font color=red><b>(X)</b></font></span></td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td>
					<td><input type='text' id='ubah_nama_penumpang' value='$data_kursi[Nama]' onFocus=\"Element.hide('ubah_nama_invalid');\" /><span id='ubah_nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
				</tr>
				<tr>
					<td>Penumpang</td><td>:</td>
					<td><span id='rewrite_ubah_keterangan_member' $display_pelanggan_setia><b>Pelanggan Setia</b></span>
						<span id='rewrite_ubah_list_discount' $display_list_discount>$opt_list_discount</span></td>
				</tr>
				<tr>
					<td valign='top'>Keterangan</td><td valign='top'>:</td>
					<td><textarea id='ubah_alamat_penumpang' cols='30' rows='2'>$data_kursi[Alamat]</textarea></td>
				</tr>
				<tr>
					<td valign='top'>Plan Asuransi</td><td valign='top'>:</td>
					<td>$opt_plan_asuransi</td>
				</tr>
				<tr height=30>
					<td valign='top' colspan=3 align='center'>
						<span id='show_tgl_lahir' style='display:$display_tgl_lahir;'>
							<font size=1 color='808080'>
								Untuk mengikuti asuransi, pelanggan harus memasukkan tanggal lahirnya.
							</font>
							Tanggal lahir &nbsp; $opt_tanggal_lahir $opt_bulan_lahir $opt_tahun_lahir
						</span>
					</td>
				</tr>
				<tr><td colspan=3 align='right'><input type='button' value='  Simpan  ' onClick=\"ubahDataPenumpang('$no_tiket');\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
			";
		}
		else{
			$arr_jenis_penumpang	= array("U"=>"UMUM","K"=>"Pelanggan Setia","G"=>"GRATIS","KK"=>"Keluarga Karyawan","M"=>"Mahasiswa dll","V"=>"Voucher");
			
			$text_id_member	= ($id_member=='')?'':"<tr><td>ID Member</td><td>:</td><td>$id_member</td></tr>";
			
			//mengambil data asuransi
			$data_asuransi_penumpang	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
			
			$show_plan_asuransi	= ($data_asuransi_penumpang['NamaPlanAsuransi']=="")?"":
				"<tr>
					<td valign='top'>Plan Asuransi</td><td valign='top'>:</td>
					<td><b>$data_asuransi_penumpang[NamaPlanAsuransi] besar premi Rp. ".number_format($data_asuransi_penumpang['BesarPremi'],0,",",".")."<b></td>
				</tr>
				<tr>
					<td valign='top'>Tgl. Lahir</td><td valign='top'>:</td>
					<td>".dateparse(FormatMySQLDateToTgl($data_asuransi_penumpang['TglLahir']))."</td>
				</tr>";
			
			//END mengambil data asuransi
			
			$kolom_data_penumpang	="
				<tr>
					<td>Telepon</td><td>:</td>
					<td>$data_kursi[Telp]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td>
					<td>$data_kursi[Nama]</td>
				</tr>
				<tr>
					<td>Penumpang</td><td>:</td>
					<td>".$arr_jenis_penumpang[$jenis_penumpang]."</td>
				</tr>
				<tr>
					<td valign='top'>Keterangan</td><td valign='top'>:</td>
					<td>$data_kursi[Alamat]</td>
				</tr>
				$show_plan_asuransi
			";
		}
		
		echo("
			<table width='100%' height='500' > 
				<tr>
					<td align='center' colspan=3>
						<h1>Kursi: $kursi</h1>
						<h3>($kode_jadwal)</h3>
					</td>
				</tr>
				<tr>
					<td width='40%' colspan='3'><h3>PENUMPANG</h3>
						<input type='hidden' name='no_tiket' id='no_tiket' value='$no_tiket'>
						<input type='hidden' name='kode_booking' id='kode_booking' value='$kode_booking'>
						<input type='hidden' name='cetak_tiket' id='cetak_tiket' value='$cetak_tiket'>
						<input type='hidden' name='kursi' id='kursi' value='$kursi'>
					</td>
				</tr>
					$text_id_member
					$kolom_data_penumpang
				<tr>
					<td colspan=3 height=1 bgcolor='D0D0D0'></td>
				</tr>
				<tr>
					<td>Harga tiket</td><td>:</td>
					<td align='right'>Rp. $harga_tiket&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Discount</td><td>:</td>
					<td align='right'>Rp. $discount&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3 height=1 bgcolor='D0D0D0'></td>
				</tr>
				<tr>
					<td>Total</td><td>:</td>
					<td align='right'><strong>Rp. $total</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3 height=1 bgcolor='red'></td>
				</tr>
				<tr>
					<td colspan=3><strong><h3>Keseluruhan pesanan</h3></strong></td>
				</tr>
				<tr>
					<td>Jumlah kursi dipesan</td><td>:</td>
					<td align='right'>$jumlah_kursi&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Sub Total</td><td>:</td>
					<td align='right'>Rp. $sub_total&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Total Discount</td><td>:</td>
					<td align='right'>Rp. $total_discount&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3 height=1 bgcolor='red'></td>
				</tr>
				<tr>
					<td>Total Bayar</td><td>:</td>
					<td align='right'><strong>Rp. $total_bayar</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>CSO</td><td>:</td>
					<td><strong>$nama_cso</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Waktu Pesan</td><td>:</td>
					<td><strong>".dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuPesan']))."</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>Status Tiket</td><td>:</td>
					<td><strong>$status_pesanan</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				$waktu_cetak_tiket
				$status_mutasi");
				
		//$tombol_bayar_member="<input type='button' onclick=\"bayarByKartu('$row[kode0]');\" value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Autodebit Member&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'>";
				
		if($cetak_tiket==1) $tombol_bayar_member="";
					
		if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
			//inisialisasi tombol2
												
			//Demi keamanan, cetak tiket dibuat script tersendiri
					
			$script_cetak_tiket="
				layout_kursi	= document.getElementById('hide_layout_kursi').value;
				Start('tiket.php?sid=".$userdata['session_id']."&tiket_dipilih=\'".$row['NoTiket']."\'&mode=cetak_tiket&layout_kursi='+layout_kursi);
				getUpdateMobil();";
				
			$tombol_hapus="<input type='button' onclick=\"batal('$no_tiket','$kursi');\" id='hider2' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;'>";
			
			if($cetak_tiket==1){
				$tombol_tiket="<input type='button' onclick=\"CetakTiket(0);\" id='hider5' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cetak Ulang Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'>";
				$tombol_koreksi_asuransi="<input type='button' onclick=\"tampilkanDialogKoreksiAsuransi('$no_tiket')\" id='hider6' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Koreksi Asuransi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'><br><br>";
			
			}
			else{
				$tombol_tiket="<input type='button' onclick=\"dialog_pembayaran.show('$kode_booking');\" id='hider5' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cetak Tiket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'>";
			}
				
			$tombol_koreksi_discount="<input type='button' onclick=\"tampilkanDialogDiscount('$no_tiket','$jenis_penumpang')\" id='hider6' value='Koreksi Discount'>";
			
			if($no_spj=="" || $userdata['user_level'] == $LEVEL_ADMIN){
				$tombol_mutasi="<br><br><input type='button' onclick=\"setFlagMutasi();\" id='btn_mutasi' value='       Mutasi Penumpang        '>";
			}
			else{
				$tombol_mutasi="";
			}
			
			if(in_array($userdata['user_level'],array($LEVEL_MANAJER,$LEVEL_SUPERVISOR))){
				//level manajerial
				if($cetak_tiket){
					$tombol_koreksi_discount="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					//$tombol_hapus="";
				}
			}
				
			if($cetak_tiket){
				if($userdata['user_level'] != $LEVEL_ADMIN){
					//level bukan admin
					$tombol_hapus="";
					//$tombol_koreksi_discount="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					$tombol_koreksi_discount=$tombol_koreksi_discount."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				}
			}
			else{
				$tombol_koreksi_discount="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";			
			}
			
			//DI OFF KAN DULU
			/*if($no_spj!="" && $userdata['user_level'] > $LEVEL_SUPERVISOR){
				$tombol_tiket="";
			}*/
			
			/*if($discount>0){
				$tombol_koreksi_discount="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}*/
			
			//TOMBOL HANYA DITAMPILKAN JIKA USER SELAIN MEMBER
			//update 4 mei 2011, tombol mutasi dihilangkan
			//update 16 mei 2011, tombol mutasi dimunculkan jika kendaraan belum berangkat
			echo(
				"<tr>
					<td colspan='3' align='center'>   
						$tombol_tiket<br><br>
						$tombol_koreksi_asuransi
						$tombol_hapus &nbsp;$tombol_koreksi_discount
						$tombol_mutasi
					</td>
				</tr>");
		
		}//end if ($userdata['user_level'] != $LEVEL_SCHEDULER)
			
		echo("
			<tr><td colspan='3'><hr color='e0e0e0'></td></tr>
			</table>
		");	
		
	
	exit;
break;

// AMBIL STATUS KURSI SATUAN ==========================================================================================================================================================================================
case "ambil_status_kursi":
	include($adp_root_path . 'ClassLayoutKendaraanDebug.php');
	
	$tgl 					= $HTTP_GET_VARS['tanggal'];
	$kode_jadwal 	= $HTTP_GET_VARS['jadwal'];
	$no_kursi 				= $HTTP_GET_VARS['no_kursi'];
	
	$LayoutKendaraan	= new LayoutKendaraan();
	$Jadwal						= new Jadwal();
	
	$row = $Jadwal->ambilDataDetail($kode_jadwal);
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	echo $LayoutKendaraan->getStatusKursi($tgl,$kode_jadwal_aktual,$no_kursi);

	exit;
	break;
	

// UBAH DATA PENUMPANG ===========================================================================================================================================================================================
case "ubahdatapenumpang":
  // mendapatkan nilai kursi yang tersedia dan value nilai lain sesuai dengan tanggal, rute, jam dan kode
		
	  $no_tiket			= trim($HTTP_GET_VARS['no_tiket']);
	  $kursi 				= trim($HTTP_GET_VARS['kursi']);
	  $nama  				= trim($HTTP_GET_VARS['nama']);
	  $telp  				= trim($HTTP_GET_VARS['telepon']);
	  $alamat				= trim($HTTP_GET_VARS['alamat']);
	  $id_discount	= trim($HTTP_GET_VARS['id_discount']);
	  $id_member		= trim($HTTP_GET_VARS['id_member']);
	  $id_asuransi	= $HTTP_GET_VARS['id_asuransi'];
	  $plan_asuransi= $HTTP_GET_VARS['plan_asuransi'];
	  $tgl_lahir		= $HTTP_GET_VARS['tgl_lahir'];
		
		$Reservasi		= new Reservasi();
		
		$Reservasi->ubahDataPenumpang($no_tiket, $nama , $alamat, $telp, $kursi, $id_discount, $id_member);
		
		include($adp_root_path . 'ClassAsuransi.php');
			
		$Asuransi	= new Asuransi();
		
		if($plan_asuransi!=""){
			
			$data_asuransi	= $Asuransi->ambilDataDetail($plan_asuransi);
			
			$data_tiket	= $Reservasi->ambilDataKursi($no_tiket);
			
			if($id_asuransi==""){
				$Asuransi->tambahAsuransi(
				$data_tiket['TglBerangkat'], $data_tiket['IdJurusan'], $data_tiket['KodeJadwal'], 
				$data_tiket['JamBerangkat'], $no_tiket, $nama, 
				$tgl_lahir, $telp, $telp, 
				$plan_asuransi, $data_asuransi['BesarPremi'], $userdata['user_id'], 
				dateNow(), $userdata['KodeCabang'],1);
			}
			else{
				$Asuransi->ubahAsuransi(
					$id_asuransi,$data_tiket['TglBerangkat'], $data_tiket['IdJurusan'],
					$data_tiket['KodeJadwal'],  $data_tiket['JamBerangkat'], $no_tiket, 
					$nama, $tgl_lahir, $telp, 
					$telp, $plan_asuransi,  $data_asuransi['BesarPremi'],
					$userdata['user_id'],  dateNow(), $userdata['KodeCabang'], 
					1);
			}
		}
		else{
			$Asuransi->hapusAsuransi($no_tiket);	
		}
		
		echo(1);
		
	exit;
break;

// TEMP ==========================================================================================================================================================================================
case "temp":
	
	$tgl 					= $HTTP_GET_VARS['tanggal'];
	$kode_jadwal 	= $HTTP_GET_VARS['jam'];
	$kursi 				= $HTTP_GET_VARS['kursi'];
	$no_spj 			= $HTTP_GET_VARS['no_spj'];
	$layout_kursi = $HTTP_GET_VARS['layout_kursi'];
	
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	
	$row = $Jadwal->ambilDataDetail($kode_jadwal);
	$layout_kursi 	= $row['JumlahKursi'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	$session_id = $userdata['user_id'];
	
	$Reservasi->updateStatusKursi($kursi,$session_id,$kode_jadwal_aktual,$tgl,$userdata['user_level']);

	include($adp_root_path . 'ClassLayoutKendaraanDebug.php');
	
	$LayoutKendaraan	= new LayoutKendaraan();
	
	echo $LayoutKendaraan->getStatusKursi($tgl,$kode_jadwal_aktual,$kursi);
	
	exit;
	break;
	
//BOOK ===========================================================================================================================================================================================
case "book":
	
	include($adp_root_path . 'ClassPromo.php');
	
	// UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
  $tgl     		= $HTTP_GET_VARS['tanggal'];   // tanggal
  $kode_jadwal= $HTTP_GET_VARS['kode_jadwal'];   // jam
  $id_member	= $HTTP_GET_VARS['id_member'];  // id member
  $nama    		= $HTTP_GET_VARS['nama'];  // nama
  $alamat 		= $HTTP_GET_VARS['alamat']; //alamat
	$no_telp_old= $HTTP_GET_VARS['no_telp_old']; // telepon
	$telepon 		= $HTTP_GET_VARS['telepon']; // telepon
	$no_spj			= $HTTP_GET_VARS['no_spj'];
	$layout_kursi= $HTTP_GET_VARS['layout_kursi'];
	$id_discount= $HTTP_GET_VARS['jenis_discount'];
	
	$kode_booking	= generateKodeBookingPenumpang();
	$payment_code	= "";
	
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	$Promo			= new Promo();
	$Jurusan		= new Jurusan();
	
	$session_id	= $userdata['session_id'];
  $useraktif	= $userdata['user_id'];
  
	//memgambil data rute dan jadwal
	$row 					= $Jadwal->ambilDataDetail($kode_jadwal);
	$jam_berangkat= $row['JamBerangkat'];
	$id_jurusan 	= $row['IdJurusan'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	//mengambil 	data header tb posisi 
	$row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_aktual);
	$kode_kendaraan	= $row['KodeKendaraan'];
	$kode_sopir			= $row['KodeSopir'];
	$tgl_cetak_SPJ	= $row['TglCetakSPJ'];
	$petugas_cetak_spj= $row['PetugasCetakSPJ'];
	$cetak_spj			= ($no_spj=="")?0:1;
						
	//mengambil data-data kode account dan komisi
	$row														= $Jurusan->ambilDataDetail($id_jurusan);
	$komisi_penumpang_CSO						= $row['KomisiPenumpangCSO'];
	$kode_akun_pendapatan						= $row['KodeAkunPendapatanPenumpang'];
	$kode_akun_komisi_penumpang_CSO	= $row['KodeAkunKomisiPenumpangCSO'];
	
	$harga_tiket= $Reservasi->getHargaTiket($kode_jadwal,$tgl);
	$charge	=0;
	$PPN=0;
	$jenis_muatan=0;
	$flag_pesanan=0;
	
	//MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU
	$data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl);
	
	$discount		= ($data_discount_point['FlagDiscount']!=1)?$data_discount_point['JumlahDiscount']:($data_discount_point['JumlahDiscount']/100)*$harga_tiket;
	$point			= $data_discount_point['JumlahPoint'];
	$target_promo	= $data_discount_point['FlagTargetPromo'];
	
	$jenis_penumpang="U";
	
	if(trim($id_member)!=''){
		$Member = new Member();
		
		$data_member	= $Member->ambilData($id_member);
		$nama    			= $data_member['Nama'];  
	  //$alamat 			= $data_member['Alamat']; 
		//$telepon 			= $data_member['Handphone']; 
		
		//memeriksa discount 
		if($nama!="" && $data_member['MasaBerlaku']>0){
			$discount				= ($target_promo <=1 )?$discount:0;
			$point_member		= ($target_promo <=1 )?$point:0;
			$jenis_penumpang="K";
		}
	}
	else{
		//memeriksa discount 
		$discount	= ($target_promo ==2 || $target_promo ==0 )?$discount:0;
	}
	
	$jenis_discount	= ($discount==0)?"":$data_discount_point['NamaPromo'];
	
	//MEMERIKSA APAKAH ADA PEMBERIAN DISCOUNT
	if($id_discount!='' && ($discount==0 || $discount=='')){
		//jika memang discount diberikan, maka akan diupdate pada database
		$data_discount	= $Reservasi->ambilDiscount($id_discount);
		$jenis_penumpang= $data_discount['KodeDiscount'];
		$jenis_discount	= $data_discount['NamaDiscount'];
		
		if($data_discount['JumlahDiscount']>1){
			$besar_discount	= $data_discount['JumlahDiscount'];
		}
		else{
			$besar_discount	= $harga_tiket*$data_discount['JumlahDiscount'];
		}
		
		$discount	= ($besar_discount<=$harga_tiket)?$besar_discount:$harga_tiket;
	}
	
	//LAYOUT KURSI
	$result_layout_kursi = $Reservasi->ambilDataFlagLayout($tgl,$kode_jadwal_aktual,$userdata['user_id']);
	
	//mengisi  flag kursi dan session
	
	$jumlah_kursi_dipesan=0;
	
	$list_nomor_kursi="";
	
	while ($data_kursi = $db->sql_fetchrow($result_layout_kursi)){
		
		//Mengenerate no tiket
		$no_tiket = generateNoTiketPenumpang();
		
		$sub_total	= $harga_tiket;
		$total			= $sub_total+$charge+$PPN-$discount;
		
		$no_kursi	= $data_kursi['NomorKursi'];
		
		//MEMERIKSA APAKAH KURSI MASIH FREE (add 17 Oktober 2011)
		$sql = 
			"SELECT IF(StatusKursi IS NULL,0,StatusKursi) AS SudahTerisi
				FROM tbl_posisi_detail
				WHERE KodeJadwal LIKE '$kode_jadwal_aktual' AND TglBerangkat LIKE '$tgl' AND NomorKursi='$no_kursi' AND Session!='$userdata[user_id]'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		$status_kursi	= $row[0];
		
		if($status_kursi==0 || $status_kursi==''){
			$Reservasi->booking(
				$no_tiket, $userdata['KodeCabang'], $kode_jadwal,
				$id_jurusan, $kode_kendaraan , $kode_sopir ,
				$tgl, $jam_berangkat , $kode_booking,
				$id_member, $point_member, $nama ,
				$alamat, $telepon, $hp,
				$no_kursi, $harga_tiket,
				$charge, $sub_total, $discount,
				$PPN, $total, $userdata['user_id'],
				$flag_pesanan, $no_spj, $tgl_cetak_SPJ,
				$cetak_spj, $komisi_penumpang_CSO,
				$petugas_cetak_spj, $keterangan, $jenis_discount,
				$kode_akun_pendapatan,$jenis_penumpang, $kode_akun_komisi_penumpang_CSO,
				$payment_code);
			
			if($target_promo==1 || $id_discount!=''){
				//DIKARENAKAN DISCOUNT  HANYA DIBERIKAN KEPADA PELANGGAN PERTAMA,MAKA SETELAH DISCOUNT DIBERIKAN, BESAR DISCOUNT AKAN DI FREE KEMBALI
				//ket: $target_promo=1 adalah promo yang hanya untuk member saja
				
				$id_member			= '';
				$discount				= 0; //discount diset kembali menjadi 0
				$jenis_discount	= ''; //jenis discount direset
				$jenis_penumpang= 'U'; //penumpang direset menjadi penumpang umum
			}
			
			$list_nomor_kursi .= $no_kursi."/";
			
			$jumlah_kursi_dipesan++;
		}
	}
		
			
	//jika ada kursi yang dipesan
	if($jumlah_kursi_dipesan>0){
		//mengirim messange true ke body
		
		$Reservasi->ubahPosisi($kode_jadwal_aktual, $tgl, $jumlah_kursi_dipesan);
		
		echo("1/$kode_booking|$list_nomor_kursi|");
	}
	else{
		//mengirim messange false ke body
		echo("0/||");
	}
			
	
	echo($pesan);
exit;
	
//MEMBATALKAN PESANAN ====================================================================================================================================
case 'pembatalan':
			
	$no_tiket	= $HTTP_GET_VARS['no_tiket'];// nourut
	$kursi		= $HTTP_GET_VARS['no_kursi']; // kursi
	$Reservasi	= new Reservasi();
	
	$data_tiket	= $db->sql_fetchrow($Reservasi->ambilDataKursiByNoTiket($no_tiket));
	
	//periksa wewenang
	if(($userdata['user_level']==$LEVEL_CSO && $data_tiket['CetakTiket'])!="1" || in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_SUPERVISOR))){
		
		include($adp_root_path . 'ClassAsuransi.php');
		
		$Asuransi	= new Asuransi();
		
		if($Asuransi->hapusAsuransi($no_tiket)){
			$Reservasi->pembatalan($no_tiket, $kursi);
			echo(1);
		}
		else{
			echo(0);
		}
		
		
	}
	else{
		echo(0);
	}
	
	exit;
		
break;

//KOREKSI DISCOUNT================================================================================================================================

case 'beridiscount':
	
	$no_tiket				= $HTTP_POST_VARS['no_tiket'];// nourut
	$jenis_discount	= $HTTP_POST_VARS['jenis_discount'];
	$username				= $HTTP_POST_VARS['username'];
	$password				= $HTTP_POST_VARS['password'];
	
	$Reservasi	= new Reservasi();
	
	//memeriksa apakah tiket sudah dicetak atau belum
	$data_tiket	= $Reservasi->ambilDataKursi($no_tiket);
	
	if($data_tiket['CetakTiket']==1){
		//jika tiket sudah dicetak, maka memerlukan otorisasi untuk meneruskan proses ini
		
		$User = new User();
		
		$data_user	= $User->ambilDataDetailByUsername($username);
		
		if($data_user['user_password']!=md5($password)){
			//jika password tidak sesuai maka tidak diijinkan melakukna proses ini
			echo(0);
			exit;
		}
	}
	
	//mengupdate total bayar sesuai dengan jumlah discount
	
	if($jenis_discount!=''){
		$data_discount	= $Reservasi->ambilDiscount($jenis_discount);
		$kode_discount	= $data_discount['KodeDiscount'];
		$nama_discount	= $data_discount['NamaDiscount'];
		$jumlah_discount= $data_discount['JumlahDiscount'];
	}
	else{
		$nama_discount		= "";
		$jumlah_discount	= 0;
	}
	
	//mengambil harga tiket berlaku
	$harga_tiket= $Reservasi->getHargaTiket($data_tiket['KodeJadwal'],$data_tiket['TglBerangkat']);
	
	$sql	= 
		"UPDATE tbl_reservasi SET
			JenisPenumpang='$kode_discount',
			Discount=IF($jumlah_discount>1,IF($jumlah_discount<=$harga_tiket,$jumlah_discount,$harga_tiket),$jumlah_discount*$harga_tiket),
			JenisDiscount='$nama_discount',
			SubTotal=$harga_tiket,
			Total=SubTotal-Discount
		WHERE NoTiket IN('$no_tiket');";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL MENGUBAH DATA');//,__LINE__,__FILE__,$sql);
	} 
	
	echo(1);
	exit;
		
break;

//KOREKSI ASURANSI================================================================================================================================

case 'koreksiasuransi':
	include($adp_root_path . 'ClassAsuransi.php');
	
	$no_tiket				= $HTTP_POST_VARS['no_tiket'];// nourut
	$plan_asuransi	= $HTTP_POST_VARS['plan_asuransi'];
	$username				= $HTTP_POST_VARS['username'];
	$password				= $HTTP_POST_VARS['password'];
	$tgl_lahir			= $HTTP_POST_VARS['tgl_lahir'];
	
	include($adp_root_path . 'ClassUser.php');
	$User = new User();
		
	$data_user	= $User->ambilDataDetailByUsername($username);
		
	if($data_user['user_password']!=md5($password)){
		//jika password tidak sesuai maka tidak diijinkan melakukna proses ini
		echo(0);
		exit;
	}
	
	//mengupdate data asuransi
	
	$Asuransi		= new Asuransi();
	$Reservasi	= new Reservasi();
	
	$data_asuransi			= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
	$data_plan_asuransi	= $Asuransi->ambilDataDetail($plan_asuransi);
	$data_tiket					= $Reservasi->ambilDataKursi($no_tiket);
	
	if($data_asuransi['IdAsuransi']==""){
		$Asuransi->tambahAsuransi(
			$data_tiket['TglBerangkat'], $data_tiket['IdJurusan'], $data_tiket['KodeJadwal'], 
			$data_tiket['JamBerangkat'], $no_tiket, $data_tiket['Nama'], 
			$tgl_lahir, $data_tiket['Telp'], $data_tiket['HP'], 
			$plan_asuransi, $data_plan_asuransi['BesarPremi'], $userdata['user_id'], 
			dateNow(), $userdata['KodeCabang'],0);
	}
	else{
		if($plan_asuransi>0){
			$Asuransi->ubahAsuransi(
				$data_asuransi['IdAsuransi'],$data_tiket['TglBerangkat'], $data_tiket['IdJurusan'],
				$data_tiket['KodeJadwal'],  $data_tiket['JamBerangkat'], $no_tiket, 
				$data_tiket['Nama'], $tgl_lahir, $data_tiket['Telp'], 
				$data_tiket['HP'], $plan_asuransi,  $data_plan_asuransi['BesarPremi'],
				$data_asuransi['PetugasTransaksi'],  $data_asuransi['WaktuTransaksi'], $data_asuransi['CabangTransaksi'], 
				1);
		}
		else{
			$Asuransi->hapusAsuransi($no_tiket);
		}
	}
	
	
	
	echo(1);
	exit;
		
break;


//MUTASI PENUMPANG===============================================================================================================================================================================
case "mutasipenumpang":
	
	include($adp_root_path . 'ClassPromo.php');
	
	// UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
  $tgl     		= $HTTP_GET_VARS['tanggal'];   
  $kode_jadwal= $HTTP_GET_VARS['kode_jadwal'];   
  $no_tiket		= $HTTP_GET_VARS['no_tiket'];  
  $no_kursi		= $HTTP_GET_VARS['no_kursi']; 
	$no_spj			= $HTTP_GET_VARS['no_spj'];
	$layout_kursi= $HTTP_GET_VARS['layout_kursi'];
	
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	$Promo			= new Promo();
	$Jurusan		= new Jurusan();
	
	$session_id	= $userdata['session_id'];
  $useraktif	= $userdata['user_id'];
  
	//Mengambil data tiket yang lama
	$result	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);
	$data_tiket_lama 	= $db->sql_fetchrow($result);
	$id_member				= $data_tiket_lama['IdMember'];
	$tgl_lama					= $data_tiket_lama['TglBerangkat'];
	$kode_jadwal_lama	= $data_tiket_lama['KodeJadwal'];
	$no_kursi_lama		= $data_tiket_lama['NomorKursi'];
	$nama_lama				= $data_tiket_lama['Nama'];
	$cetak_tiket_lama	= $data_tiket_lama['CetakTiket'];
	//--END data tiket lama
	
	//memgambil data rute dan jadwal
	$row 					= $Jadwal->ambilDataDetail($kode_jadwal);
	$jam_berangkat= $row['JamBerangkat'];
	$id_jurusan 	= $row['IdJurusan'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	//mengambil 	data header tb posisi 
	$row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_aktual);
	$kode_kendaraan	= $row['KodeKendaraan'];
	$kode_sopir			= $row['KodeSopir'];
	$tgl_cetak_SPJ	= $row['TglCetakSPJ'];
	$petugas_cetak_spj= $row['PetugasCetakSPJ'];
	$cetak_spj			= ($no_spj=="")?0:1;
						
	//mengambil data-data kode account dan komisi
	$row														= $Jurusan->ambilDataDetail($id_jurusan);
	$komisi_penumpang_CSO						= $row['KomisiPenumpangCSO'];
	$kode_akun_pendapatan						= $row['KodeAkunPendapatanPenumpang'];
	$kode_akun_komisi_penumpang_CSO	= $row['KodeAkunKomisiPenumpangCSO'];
	
	$harga_tiket= $Reservasi->getHargaTiket($kode_jadwal,$tgl);
	$charge	=0;
	$PPN=0;
	$jenis_muatan=0;
	
	$data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl);
	
	$discount		= ($data_discount_point['FlagDiscount']!=1)?$data_discount_point['JumlahDiscount']:($data_discount_point['JumlahDiscount']/100)*$harga_tiket;
	$point			= $data_discount_point['JumlahPoint'];
	$target_promo	= $data_discount_point['FlagTargetPromo'];
	
	$jenis_discount	= ($discount==0)?"":"PROMO";
	
	//Mengubah posisi TUJUAN
	if(!$Reservasi->ubahPosisi($kode_jadwal_aktual, $tgl, 1)){
		echo(0);
		exit;
	}
		
	//Mereset posisi SEBELUMNYA
	if(!$Reservasi->ubahPosisi($kode_jadwal_lama, $tgl_lama, -1)){
		echo(0);
		exit;
	}
			
	$sub_total	= $harga_tiket;
	$total			= $sub_total+$charge+$PPN-$discount;
				
			
	if(!$Reservasi->mutasiPenumpang(
		$no_tiket, $kode_jadwal,$id_jurusan, 
		$kode_kendaraan , $kode_sopir ,$tgl, 
		$jam_berangkat , $no_kursi, $harga_tiket,
		$charge, $sub_total, $discount,
		$PPN, $total,$no_spj, 
		$tgl_cetak_SPJ, $cetak_spj, $komisi_penumpang_CSO,
		$petugas_cetak_spj, $keterangan, $jenis_discount,
		$kode_akun_pendapatan, $kode_akun_komisi_penumpang_CSO, $payment_code,
		$nama_lama,$cetak_tiket_lama,$kode_jadwal_lama,
		$tgl_lama,$no_kursi_lama,$userdata['user_id'])){
		
		echo(2);
		exit;
	}
	
	include($adp_root_path . 'ClassAsuransi.php');
		
	$Asuransi	= new Asuransi();
	
	$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
	
	if($data_asuransi['IdAsuransi']!=""){
		
		$Asuransi->ubahAsuransi(
			$data_asuransi['IdAsuransi'],$tgl, $id_jurusan,
			$kode_jadwal,  $jam_berangkat, $no_tiket, 
			$data_asuransi['Nama'], $data_asuransi['TglLahir'], $data_asuransi['Telp'], 
			$data_asuransi['HP'], $data_asuransi['PlanAsuransi'],  $data_asuransi['BesarPremi'],
			$userdata['user_id'],  $data_asuransi['WaktuTransaksi'], $userdata['KodeCabang'], 
			$data_asuransi['FlagBatal']);
	}
	
	echo(1);
exit;

//==PAKET==
// PAKET LAYOUT===========================================================================================================================================================================================
case "paketlayout":
  // menampilkan paket yang tersedia sesuai dengan tanggal, rute dan jam yang dimasukan
  include($adp_root_path . 'ClassPaket.php');       			
  
	$Paket		= new Paket();
	$Jadwal		= new Jadwal();
	$Reservasi= new Reservasi();
	
	if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
		//HANYA BISA DIAKSES OLEH SELAIN MEMBER
		
		$tgl  				= $HTTP_GET_VARS['tanggal']; // tanggal    
		$kode_jadwal 	= $HTTP_GET_VARS['jadwal'];    // jam
		
		if($kode_jadwal!="" && $kode_jadwal!="(none)"){
			//LAYOUT PAKET
			
			$row = $Jadwal->ambilDataDetail($kode_jadwal);
		  $jam_berangkat	= $row['JamBerangkat'];
			
			if($row['FlagSubJadwal']!=1){
				$kode_jadwal_utama	= $kode_jadwal;
			}
			else{
				$kode_jadwal_utama	= $row['KodeJadwalUtama'];
			}
			
			//MEMERIKSA HAK AKSES, JIKA CSO BIASA TIDAK BOLEH MEMESAN PADA WAKTU YANG SUDAH LALU
			if(!$Reservasi->periksaHakAkses($tgl,$jam_berangkat) && $user_level>=$LEVEL_CSO){
				if($userdata['user_level']==$LEVEL_CSO_PAKET){
					echo("<br><br><br><br><br><br>
						<img src='./templates/images/icon_warning.png' />
						<font color='red'><h3>Anda tidak boleh memilih waktu yang sudah lalu!</h3></font>");
				}
				exit;
			}
			//--END PERIKSA HAK AKSES
			
			echo(
				"<table cellspacing='1' cellpadding='0' width='400'>
					<tbody>
					<tr>
						<td align='right' colspan=2>
							<input type='button' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refresh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' onclick='getUpdatePaket();getPengumuman();' />
						</td>
					</tr>
					<tr>
						<td align='left' bgcolor='d0d0d0' height=20 colspan=2>
								&nbsp;<font size=2 color='505050'><strong>Daftar Paket</strong>&nbsp;&nbsp;
								<span id='progress_paket' style='display:none;'>
									<img src='./templates/images/loading.gif' />
								</span>&nbsp;
						</td>
					</tr>");
			
			// transaksi
		  
		  if ($result = $Paket->ambilData("TglBerangkat='$tgl' AND (KodeJadwal='$kode_jadwal' OR KodeJadwal='$kode_jadwal_utama') AND FlagBatal!=1 ORDER BY WaktuPesan ASC")){
				
				while ($row = $db->sql_fetchrow($result)){
					$i++;
					$odd ='odd';
				
					if (($i % 2)==0){
						$odd = 'even';
					}
				
					$nama_pengirim_disingkat=substr($row['NamaPengirim'],0,20);
					$nama_penerima_disingkat=substr($row['NamaPenerima'],0,20);
					$harga_paket=number_format($row['HargaPaket'],0,",",".");
					$cetak_tiket		=$row['CetakTiket'];
					$cara_bayar			=$row['CaraPembayaran'];
					$status_diambil	=$row['StatusDiambil'];
					
					
					
					if($cetak_tiket && ($cara_bayar!=$PAKET_CARA_BAYAR_DI_TUJUAN || $status_diambil==1)){
						$label_lunas="background='./templates/images/label_lunas.png' STYLE='background-repeat: no-repeat;background-position: left middle;'";
					}
					else{
						$label_lunas="background='./templates/images/icon_paket.png' STYLE='background-repeat: no-repeat;background-position: left middle;'";
					}
					
		      echo("
					<tr class='$odd'>
						<td width='20'><h3>$i.</h3></td>
						<td onclick=\"showPaket('$row[NoTiket]')\"  valign='top' height='40' $label_lunas>
							<font color='008609'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pengirim: $nama_pengirim_disingkat Rp. $harga_paket</b></font><br>
							<font color='0000ff'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Penerima: $nama_penerima_disingkat</b></font>
						</td>
					</tr>
				");
				}	
		  } 
		  else{	  
		    //die_error('Cannot Load paket',__LINE__,__FILE__,$sql);
				echo("Error :".__LINE__);exit;
			}

			if($i<=0){
				echo("
					<tr align='center'>
						<td colspan=2>tidak ada paket</td>
					</tr>
				");
			}
			echo("</tbody></table>");
		}
	}
	else{
		echo("");
	}
	
  exit;
break;

//PAKET TAMBAH ===========================================================================================================================================================================================
case "pakettambah":
	include($adp_root_path . 'ClassPaket.php');
	$Reservasi	= new Reservasi();
	$Paket			= new Paket();
	$Jadwal			= new Jadwal();
	$Jurusan		= new Jurusan();
	
	// UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
  $tgl     				= $HTTP_GET_VARS['tanggal'];   // tanggal
  $kode_jadwal		= $HTTP_GET_VARS['kode_jadwal'];   // kode jadwal
  $nama_pengirim	= $HTTP_GET_VARS['nama_pengirim'];  // nama
  $alamat_pengirim= $HTTP_GET_VARS['alamat_pengirim']; //alamat
	$telepon_pengirim=$HTTP_GET_VARS['telepon_pengirim']; // telepon  
	$nama_penerima	= $HTTP_GET_VARS['nama_penerima'];  // nama
  $alamat_penerima= $HTTP_GET_VARS['alamat_penerima']; //alamat
	$telepon_penerima=$HTTP_GET_VARS['telepon_penerima']; // telepon  
	$keterangan			= $HTTP_GET_VARS['keterangan']; // telepon  
	$jumlah_koli		= $HTTP_GET_VARS['jumlah_koli']; 
	$berat					= $HTTP_GET_VARS['berat'];
	$jenis_barang		= $HTTP_GET_VARS['jenis_barang']; 
	$layanan				= $HTTP_GET_VARS['layanan']; 
	$cara_bayar			= $HTTP_GET_VARS['cara_bayar']; 
	$arr_harga_paket= $Paket->ambilHargaPaketByKodeLayanan($layanan);	
	$harga_paket		= $arr_harga_paket[0]+$arr_harga_paket[1]*($berat-1);
	
	$kode_booking 	= generateNoTiketPaket();
	$no_spj					= $HTTP_GET_VARS['no_spj'];
	
  $useraktif=$userdata['user_id'];
  
	//memgambil data rute dan jadwal
	$row 					= $Jadwal->ambilDataDetail($kode_jadwal);
	$jam_berangkat= $row['JamBerangkat'];
	$id_jurusan 	= $row['IdJurusan'];
	$flag_sub_jadwal= $row['FlagSubJadwal'];
	$kode_jadwal_utama= $row['KodeJadwalUtama'];
	$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
	
	//mengambil 	data header tb posisi 
	$row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_aktual);
	$kode_kendaraan	= $row['KodeKendaraan'];
	$kode_sopir			= $row['KodeSopir'];
	$tgl_cetak_SPJ	= $row['TglCetakSPJ'];
	$petugas_cetak_spj= $row['PetugasCetakSPJ'];
	$cetak_spj			= ($no_spj=="")?0:1;
						
	//mengambil data-data kode account dan komisi
	$row												= $Jurusan->ambilDataDetail($id_jurusan);
	$kode_cabang								= $userdata['KodeCabang'];
	$komisi_paket_CSO						= $row['KomisiPaketCSO'];
	$komisi_paket_sopir					= $row['KomisiPaketSopir'];
	$kode_akun_pendapatan				= $row['KodeAkunPendapatanPaket'];
	$kode_akun_komisi_paket_CSO	= $row['KodeAkunKomisiPaketCSO'];
	$kode_akun_komisi_paket_sopir	= $row['KodeAkunKomisiPaketSopir'];
	
	$Paket->tambah(
		$kode_booking, $kode_cabang, $kode_jadwal_aktual,
		$id_jurusan, $kode_kendaraan , $kode_sopir ,
		$tgl, $jam_berangkat , $nama_pengirim ,
		$alamat_pengirim, $telepon_pengirim,
		$nama_penerima, $alamat_penerima, $telepon_penerima,
		$harga_paket,$keterangan, $userdata['user_id'],
		$komisi_paket_CSO, $komisi_paket_sopir,
		$kode_akun_pendapatan, $kode_akun_komisi_paket_CSO, $kode_akun_komisi_paket_sopir,
		$jumlah_koli,$berat,$jenis_barang,
		$layanan,$cara_bayar);
	
	echo(1);

exit;

//PAKET BATAL ===========================================================================================================================================================================================
case "paketbatal":
	include($adp_root_path . 'ClassPaket.php');
	$Paket			= new Paket();
	
	$no_tiket	= $HTTP_GET_VARS['no_tiket'];// nourut
	
	//periksa wewenang
	if(in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR))){
		$Paket->pembatalan($no_tiket, $userdata['user_id']);
		echo(1);
	}
	else{
		echo(0);
	}
	
	exit;

exit;

// PAKET DETAIL  ===========================================================================================================================================================================================
case "paketdetail":
  //operasi terhadap paket
  include($adp_root_path . 'ClassPaket.php');
	$Paket			= new Paket();
	
	$no_tiket = $HTTP_GET_VARS['no_tiket'];
	$submode  = $HTTP_GET_VARS['submode'];
	
	if ($userdata['user_level'] == $LEVEL_SCHEDULER){	
		exit;
	}
	    
	$row	= $Paket->ambilDataDetail($no_tiket);
			
	$harga_paket	=number_format($row['HargaPaket'],0,",",".");
	
	if($submode=='ambil'){
		echo("
			<input type='hidden' id='dlg_ambil_paket_no_tiket' value='$row[NoTiket]'/>
			<input type='hidden' id='hdn_paket_cara_pembayaran' value='$row[CaraPembayaran]' />
			<table cellspacing=0 cellpadding=0 width='100%' height='100%'>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Pengirim</h2></td></tr>
						<tr><td>Telp Pengirim </td><td>:</td><td>$row[TelpPengirim]</td></tr>
						<tr><td width=200>Nama Pengirim</td><td width=5>:</td><td>$row[NamaPengirim]</td></tr>
						<tr><td>Alamat Pengirim</td><td>:</td><td>$row[AlamatPengirim]</td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Penerima</h2></td></tr>
						<tr><td>Telp Penerima </td><td>:</td><td>$row[TelpPenerima]</td></tr>
						<tr><td width=200>Nama Penerima </td><td width=5>:</td><td>$row[NamaPenerima]</td></tr>
						<tr><td>Alamat Penerima</td><td>:</td><td>$row[AlamatPenerima]</td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Paket</h2></td></tr>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td width='50%'>Jumlah Koli</td><td>:</td><td>".number_format($row['JumlahKoli'],0,",",".")."</td></tr>
						<tr><td>Berat (Kg)</td><td>:</td><td>".number_format($row['Berat'],0,",",".")."&nbsp;Kg.</td></tr>
						<tr><td>Jenis Barang</td><td>:</td><td>$row[JenisBarang]</td></tr>
						<tr><td>Layanan</td><td>:</td><td>".$LIST_JENIS_LAYANAN_PAKET[$row['Layanan']]."</td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td>Harga</td><td>:</td><td>Rp. $harga_paket</td></tr>
						<tr><td>Cara Pembayaran</td><td>:</td><td>".$LIST_JENIS_PEMBAYARAN_PAKET[$row['CaraPembayaran']]."</td></tr>
						<tr><td valign='top'>Keterangan</td><td valign='top'>:</td><td>$row[KeteranganPaket]</td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Pengambil</h2></td></tr>
			<tr>
				<td width='50%' valign='top' colspan=2>
					<table>
						<tr>
							<td width='25%'>Nama Pengambil <span id='dlg_ambil_paket_nama_pengambil_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input type='text' id='dlg_ambil_paket_nama_pengambil' maxlength=20 onFocus='Element.hide(\"dlg_ambil_paket_nama_pengambil_invalid');\" /></td><td width=10></td>
							<td width='25%'>No. KTP <span id='dlg_ambil_paket_no_ktp_pengambil_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input type='text' id='dlg_ambil_paket_no_ktp_pengambil' maxlength=20 onFocus='Element.hide(\"dlg_ambil_paket_no_ktp_pengambil_invalid');\" /></td></tr>
					</table>
				</td>
			</tr>
		</table>
		");
		exit;
	}
	
	if(in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN))){
		$tombol_batal="<br><br><input type='button' onclick='batalPaket(\"$no_tiket\");' value='&nbsp;&nbsp;Batalkan&nbsp;&nbsp;'>";
	}
	
	if($row['CetakTiket']==1){
		$tombol_tiket="<input type='button' onclick=\"CetakTiket(0);\" id='btn_cetak_ulang' value='    C e t a k    U l a n g   R e s i    '>";
	}
	else{
		$tombol_tiket="<input type='button' onclick=\"dialog_pembayaran.show('$kode_booking');\" id='btn_cetak_ulang' value='          C e t a k    R e s i          '>";
	}
	
	if($row['StatusDiambil']==1){
		$output_keterangan_pengambil="
			<tr>
				<td>Waktu Pengambilan</td><td>:</td>
				<td>".FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])."</td>
			</tr>
			<tr>
				<td>Nama Pengambil</td><td>:</td>
				<td>$row[NamaPengambil]</td>
			</tr>
			<tr>
				<td>Petugas Pemberi</td><td>:</td>
				<td>$row[PetugasPemberi]</td>
			</tr>
		";
	}
	
	echo("
		<table border='0' width='100%'> 
			<tr>
				<td width='40%' colspan='3'><h2>Paket&nbsp;<input type='button' onClick='document.getElementById(\"dataPelanggan\").innerHTML=\"\"' value='&nbsp; Tambah Paket Baru &nbsp;'/></h2></td>
			</tr>
			<tr>
				<td width='40%'>Nomor Resi</td><td width='5%'>:</td>
				<td>
					<input type='hidden' name='flag_paket' id='flag_paket' value='1'>
					<input type='hidden' name='kode_booking' id='kode_booking' value='$row[NoTiket]'>$row[NoTiket]
				</td>
			</tr>
			<tr><td colspan=3><h3>Data Pengirim</h3></td></tr>
			<tr>
				<td>Nama</td><td>:</td>
				<td>$row[NamaPengirim]</td>
			</tr>
			<tr>
				<td valign='top'>Alamat</td><td valign='top'>:</td>
				<td>$row[AlamatPengirim]</td>
			</tr>
			<tr>
				<td>Telepon</td><td>:</td>
				<td>$row[TelpPengirim]</td>
			</tr>
			<tr><td colspan=3><br><h3>Data Penerima</h3></td></tr>
			<tr>
				<td>Nama</td><td>:</td>
				<td>$row[NamaPenerima]</td>
			</tr>
			<tr>
				<td valign='top'>Alamat</td><td valign='top'>:</td>
				<td>$row[AlamatPenerima]</td>
			</tr>
			<tr>
				<td>Telepon</td><td>:</td>
				<td>$row[TelpPenerima]</td>
			</tr>
			<tr><td bgcolor='red' height=1 colspan=3></td></tr>
			<tr><td colspan=3><br><h3>Data Paket</h3></td></tr>
			<tr>
				<td>Jumlah Koli</td><td>:</td>
				<td>$row[JumlahKoli]</td>
			</tr>
			<tr>
				<td>Berat</td><td>:</td>
				<td>$row[Berat]&nbsp;Kg.</td>
			</tr>
			<tr>
				<td>Jenis Barang</td><td>:</td>
				<td>$row[JenisBarang]</td>
			</tr>
			<tr>
				<td>Layanan</td><td>:</td>
				<td>".$LIST_JENIS_LAYANAN_PAKET[$row['Layanan']]."</td>
			</tr>
			<tr>
				<td>Harga paket</td><td>:</td>
				<td>Rp. $harga_paket</td>
			</tr>
			<tr>
				<td>Cara Pembayaran</td><td>:</td>
				<td>".$LIST_JENIS_PEMBAYARAN_PAKET[$row['CaraPembayaran']]."</td>
			</tr>
			<tr>
				<td>Keterangan paket</td><td>:</td>
				<td>$row[KeteranganPaket]</td>
			</tr>
			<tr>
				<td>CSO</td><td>:</td>
				<td><strong>$row[NamaCSO]</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan='3' align='center'>   
					<br>
					$tombol_tiket
					$tombol_batal
				</td>
			</tr>
			<tr><td colspan='3' align='center'><hr color='e0e0e0'></td></tr>
			$output_keterangan_pengambil
		</table>");
	
	exit;
	
break;

//PAKET AMBIL ===========================================================================================================================================================================================
case "paketambil":
	include($adp_root_path . 'ClassPaket.php');
	$Paket			= new Paket();
	
	$no_tiket					= $HTTP_GET_VARS['no_tiket'];
	$nama_pengambil		= $HTTP_GET_VARS['nama_pengambil'];
	$no_ktp_pengambil	= $HTTP_GET_VARS['no_ktp_pengambil'];
	
	if($Paket->updatePaketDiambil($no_tiket,$nama_pengambil,$no_ktp_pengambil,$userdata['user_id'],$userdata['KodeCabang'])){
		echo(1);
	}
	else{
		echo(0);
	}
	
exit;

//WAITING LIST ===========================================================================================================================================================================================
case "waiting_list":
	
  $aksi    		= $HTTP_GET_VARS['aksi'];  // tipe aksi (pesan,book,hapus)     
	$kode_jadwal= $HTTP_GET_VARS['jam'];   // jam  
	
  $useraktif=$userdata['username'];
	
  if(($kode_jadwal!="" && $kode_jadwal!="(none)") || $aksi=='hapus'){
		if($aksi=='tambah'){
				$tgl     		= FormatMySQLDateToTgl($HTTP_GET_VARS['tanggal']);   // tanggal
			  
			  $nama    		= $HTTP_GET_VARS['nama'];  // nama
			  $alamat 				= $HTTP_GET_VARS['alamat']; //alamat
				$telepon 		= $HTTP_GET_VARS['telepon']; // telepon  
			  $hp      		= $HTTP_GET_VARS['hp']; // hp
				$jum_kursi	= $HTTP_GET_VARS['jum_kursi']; // discount		
				
				$sql = "INSERT INTO tbl_waiting_list 
									(tgl_berangkat,kode_rute,
									nama,alamat,telepon,hp,jum_kursi,cso) 
						  	VALUES(
									CONVERT(datetime,'$tgl',104),'$kode_jadwal',
									'$nama','$alamat','$telepon','$hp','$jum_kursi','$useraktif');";
				
				if (!$db->sql_query($sql)){
					//die_error('Cannot Save waiting list',__LINE__,__FILE__,$sql);
					echo("Error :".__LINE__);exit;
				}
		}
		elseif($aksi=='tampilkan'){
			// menampilkan waiting list yang ada sesuai dengan tanggal, rute dan jam yang dimasukan
	          			
		  if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
				//HANYA BISA DIAKSES OLEH SELAIN MEMBER
				
				$tgl  = FormatMySQLDateToTgl($HTTP_GET_VARS['tanggal']); // tanggal    
				$kode_jadwal = $HTTP_GET_VARS['jam'];    // jam
				$ada_data=false;
				
				//LAYOUT WAITING LIST
				echo(
					"<table cellspacing='1' cellpadding='0' border='0' class='whiter' width='100%'>
						<tbody>
						<tr>
							<td bgcolor='D0D0D0' align='center'>
									<strong>WAITING LIST</strong>
							</td>
						</tr>
						<tr>
							<td bgcolor='RED' align='center'>
								<span id='progress_waiting_list' style='display:none;'>
									<font color='WHITE' >Memproses waiting list</font>
								</span>
							</td>
						</tr>");
				
				// transaksi
			  $sql = 
					"SELECT 
						id_waiting_list,nama,alamat,telepon,hp,jum_kursi,cso
					FROM tbl_waiting_list 
					WHERE (CONVERT(CHAR(20), tgl_berangkat, 105)) = '$tgl' 
						AND kode_rute='$kode_jadwal' order by id_waiting_list";
			  
				$idx=1;
			  
				if ($result = $db->sql_query($sql)){
					while ($row = $db->sql_fetchrow($result)){
						$ada_data=true;
						$nama_disingkat=substr($row[1],0,15);
						$no_telepon="$row[3]/$row[4]";
						$id_waiting_list=$row['id_waiting_list'];
			      echo("
							<tr align='left'>
								<td>
									<strong>($idx)</strong> <a href='#' onClick=\"PilihPelanggan('$row[nama]','$row[alamat]','$row[telepon]','$row[hp]');deleteWaitingList('$id_waiting_list');\">N:$nama_disingkat T:$no_telepon<br>Jum.Kursi:$row[jum_kursi]</a>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href='#' OnClick='deleteWaitingList(\"$id_waiting_list\");'><font color='red'>[BATAL]</font></a>
								</td>
							</tr>
							<tr>
								<td height=2 bgcolor='D0D0D0'></td>
							</tr>
						");
						$idx++;
					}	
			  } 
			  else{	  
			    //die_error('Cannot Load data waiting list',__LINE__,__FILE__,$sql);
					echo("Error :".__LINE__);exit;
				}

				if(!$ada_data){
					echo("
						<tr align='center'>
							<td>tidak ada waiting list</td>
						</tr>
					");
				}
				echo("</tbody></table>");
			}
			
		}
		elseif($aksi=='hapus'){
				
				$id_waiting_list= $HTTP_GET_VARS['id_waiting_list'];   // nourut	  									
							
				$sql = "DELETE FROM tbl_waiting_list WHERE id_waiting_list='$id_waiting_list';";
				
				if (!$db->sql_query($sql)){
					//die_error('Cannot delete waiting list',__LINE__,__FILE__,$sql);
					echo("Error :".__LINE__);exit;
				}
		}	
	}


exit;

	
// UBAH TIKET==========================================================================================================================================================================================
case "ubahTiket":
	//mengubah data tiket yang sudah pernah diinput. operasi ini bersifat konfirmasi
	$no_tiket		= $HTTP_GET_VARS['no_tiket'];   // no tiket
	$nama    		= $HTTP_GET_VARS['nama'];  // nama
  $alamat 				= $HTTP_GET_VARS['alamat']; //alamat
	$telepon 		= $HTTP_GET_VARS['telepon']; // telepon  
  $hp      		= $HTTP_GET_VARS['hp']; // hp
	$discount		= $HTTP_GET_VARS['discount']; // discount
	
	$sql	="UPDATE TbReservasi
					SET 
						nama0='$nama',
						alamat0='$alamat',
						telp0='$telepon',
						hp0='$hp',
						discount=$discount,
						total=(pesanKursi*(HargaTiket-$discount))
					WHERE noUrut='$no_tiket'";	

	if (!$db->sql_query($sql)){
		//die_error('Cannot Save Transaksi',__LINE__,__FILE__,$sql);
		echo("Error :".__LINE__);exit;
	}
	
exit;

// KODE PEMBATALAN==========================================================================================================================================================================================
case "kode_pembatalan":
	
	$sandi_pembatalan=new Enkripsi();

	//mengambil jam server
	$sql ="SELECT
		DAY(GETDATE()),
		MONTH(GETDATE()),
		YEAR(GETDATE());";

	if ($result = $db->sql_query($sql)){
		while ($row = $db->sql_fetchrow($result)){
			$kode_batal=$sandi_pembatalan->Encrypt(($row[2]%$row[0])+($row[1]*$row[2]).BulanString($row[0]).(($row[1]*$row[2])%$row[0]));
		}
	}
	else{
		//die_error('gagal');
		echo("Error :".__LINE__);exit;
	}
	
	echo($kode_batal);
	
exit;

// LIST JENIS DISCOUNT=========================================================================================================================	
case 'list_jenis_discount':
		// membuat list jenis discount
		include($adp_root_path . 'ClassPromo.php');
		$Promo	= new Promo();
		
		$kode_jadwal 		= $HTTP_GET_VARS['kode_jadwal'];   
		$tgl_berangkat 	= $HTTP_GET_VARS['tgl_berangkat'];   
		$flag_koreksi 	= $HTTP_GET_VARS['flag_koreksi'];   
		$jenis_penumpang= $HTTP_GET_VARS['jenis_penumpang'];   
		
		//MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU JIKA DITEMUKAN ADA PROMO, MAKA DISCOUNT YANG LAIN TIDAK BERLAKU
		$data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl_berangkat);
		
		$target_promo	= $data_discount_point['FlagTargetPromo'];
		
		if($target_promo=='' || $target_promo==1){
			if($flag_koreksi=='' || $flag_koreksi==0){
				$opt = "<SELECT id='opt_jenis_discount'>".setListDiscount($kode_jadwal,$tgl_berangkat)."</select>";
			}
			else{
				$opt = "<SELECT id='opt_jenis_discount_koreksi'>".setListDiscount($kode_jadwal,$tgl_berangkat,$jenis_penumpang)."</select>";
			}
		}
		else{
			$besar_discount		= ($data_discount_point['JumlahDiscount']>1)?"Rp.".number_format($data_discount_point['JumlahDiscount'],0,",","."):$data_discount_point['JumlahDiscount']."%";
			$opt	="<b>Promo Discount $besar_discount<b>";
		}
		
		echo($opt);

	exit;

// LIST PLAN ASURANSI=========================================================================================================================	
case 'listplanasuransi':
		// membuat list plan asuransi
		include($adp_root_path . 'ClassAsuransi.php');
		
		$no_tiket 		= $HTTP_GET_VARS['no_tiket'];   
		
		$Asuransi	= new Asuransi();
		
		//SET COMBO ASURANSI
			
		$data_asuransi_penumpang	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
		
		if (!$result = $Asuransi->ambilData("NamaPlan","ASC")){
			$opt_plan_asuransi="Error";
		}
		
		$opt_plan_asuransi = "<input type='hidden' id='id_asuransi' value='$data_asuransi_penumpang[IdAsuransi]' />
			Silahkan pilih plan asuransi: 
			<select id='plan_asuransi' onChange='if(this.value==0){Element.hide(\"show_tgl_lahir\");}else{Element.show(\"show_tgl_lahir\");}'>";
		
		$opt_plan_asuransi.="<option value=0>NO PLAN</option>";
		
		while ($row = $db->sql_fetchrow($result)){
			$selected	= ($data_asuransi_penumpang['PlanAsuransi']!=$row[0])?"":"selected";
			$opt_plan_asuransi.="<option value=$row[0] $selected>$row[NamaPlan] (Premi=Rp. ".number_format($row['BesarPremi'],0,",",".").")</option>";
		}
		
		$opt_plan_asuransi.="</select>";
		
		//SET combo tanggal lahir 
		
		$temp_tgl_lahir_asuransi	= explode("-",$data_asuransi_penumpang['TglLahir']);
		
		//tanggal
		$opt_tanggal_lahir	= "<span id='span_tgl_lahir'><select id='opt_tgl_lahir'>";
		
		$tgl_lahir	= ($tgl_lahir=="")?$temp_tgl_lahir_asuransi[2]:$tgl_lahir;
		
		for($tgl=1;$tgl<=31;$tgl++){
			$selected	=(substr("0".$tgl,-2)!=$tgl_lahir)?"":"selected";
			$opt_tanggal_lahir	.= "<option value='$tgl' $selected>$tgl</option>";
		}
		
		$opt_tanggal_lahir	.= "</select></span>";
		
		//bulan
		$opt_bulan_lahir	= "<select id='opt_bln_lahir' onChange='setComboTglLahir(document.getElementById(\"opt_tgl_lahir\").value,this.value,document.getElementById(\"opt_thn_lahir\").value);'>";
		
		$bln_lahir	= ($bln_lahir=="")?$temp_tgl_lahir_asuransi[1]:$bln_lahir;
		
		for($bln=1;$bln<=12;$bln++){
			$selected	=(substr("0".$bln,-2)!=$bln_lahir)?"":"selected";
			$opt_bulan_lahir	.= "<option value='$bln' $selected>".BulanString($bln)."</option>";
		}
		
		$opt_bulan_lahir	.= "</select>";
		
		//tahun
		$opt_tahun_lahir	= "<select id='opt_thn_lahir' onChange='setComboTglLahir(document.getElementById(\"opt_tgl_lahir\").value,document.getElementById(\"opt_bln_lahir\").value,this.value);'>";
		
		$thn_min	= date("Y")-90;
		
		$thn_lahir	= ($thn_lahir=="")?$temp_tgl_lahir_asuransi[0]:$thn_lahir;
		$thn_lahir	= ($thn_lahir=="")?date("Y")-20:$thn_lahir;
		
		for($thn=$thn_min;$thn<=date("Y");$thn++){
			$selected	=($thn!=$thn_lahir)?"":"selected";
			$opt_tahun_lahir	.= "<option value='$thn' $selected>$thn</option>";
		}
		
		$opt_tahun_lahir	.= "</select>";
		
		//-END set combo tanggal lahir
		
		$display_tgl_lahir	= ($data_asuransi_penumpang['IdAsuransi']=="")?"none":"yes";
		
		//--END SET COMBO ASURANSI
		
		$opt_plan_asuransi	.=
			"<br><br><span id='show_tgl_lahir' style='display:$display_tgl_lahir;'>
				<font size=1 color='808080'>
					Untuk mengikuti asuransi, pelanggan harus memasukkan tanggal lahirnya.
				</font><br><br>
				Tanggal lahir &nbsp; $opt_tanggal_lahir $opt_bulan_lahir $opt_tahun_lahir
			</span><br><br>";
		
		echo($opt_plan_asuransi);

	exit;

	
case 'cari_jadwal_pelanggan':
		// membuat list jenis discount
		$Reservasi	= new Reservasi();
		
		$no_telp	= $HTTP_GET_VARS['no_telp'];
		
		$result	= $Reservasi->cariJadwalKeberangkatan($no_telp);
		
	  if ($result){
		
			$return	= "
				<table>
					<tr>
						<th width=100>Nama</th>
						<th width=100>Tgl.Pergi</th>
						<th width=100>Jam</th>
						<th width=200>Asal</th>
						<th width=200>Tujuan</th>
						<th width=100>Kode Booking</th>
						<th width=100>No.Kursi</th>
						<th width=100>Tiket</th>
						<th width=100>Status</th>
					</tr>";
			
			$idx	= 0;
			while ($row = $db->sql_fetchrow($result)){
	      $idx++;
				$nomor_kursi	="";
				
				$result_nomor_kursi	= $Reservasi->ambilNomorKursi($row['KodeBooking']);
				
				while($data_no_kursi = $db->sql_fetchrow($result_nomor_kursi)){
					$nomor_kursi	.=$data_no_kursi[0].",";
				}
				
				$nomor_kursi	= substr($nomor_kursi,0,-1);
				
				$status_tiket			= ($row['CetakTiket'])?"Dibayar":"Book";
				$status_berangkat	= ($row['CetakSPJ'])?"Berangkat<BR>".dateparse(FormatMySQLDateToTglWithTime($row['TglCetakSPJ'])):"Belum Berangkat";
				
				$return	.="
					<tr bgcolor='dfdfdf'>
						<td>$row[Nama]</td>
						<td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
						<td align='center'>$row[JamBerangkat]</td>
						<td>$row[Asal]</td>
						<td>$row[Tujuan]</td>
						<td>$row[KodeBooking]</td>
						<td align='center'>$nomor_kursi</td>
						<td align='center'>$status_tiket</td>
						<td align='center'>$status_berangkat</td>
					</tr>";
	    } 
	  } 
		else{ 
			echo("Err :".__LINE__);exit;
		}
		
		if($idx==0){
			$return .="<tr><td colspan=9 class='banner' align='center'><h2>Tidak ada keberangkatan ditemukan!</h2></td></tr>";
		}
		
		echo($return."</table>");

	exit;
	
	case 'cari_paket':
		// membuat list jenis discount
		$Reservasi	= new Reservasi();
		
		$no_resi	= $HTTP_GET_VARS['no_resi'];
		
		$result	= $Reservasi->cariPaket($no_resi);
		
	  if ($result){
		
			$return	= "
				<table>
					<tr>
						<th width=100>Tgl.Pergi</th>
						<th width=150>Asal</th>
						<th width=150>Tujuan</th>
						<th width=70>Jam</th>
						<th width=100>Id Kendaraan</th>
						<th width=100>Sopir</th>
						<th width=100>Kode Booking</th>
						<th width=150>Pengirim</th>
						<th width=150>Penerima</th>
						<th width=150>Pengambil</th>
					</tr>";
			
			$jum_data	= 0;
			
			while ($row = $db->sql_fetchrow($result)){
				
				if($row['StatusDiambil']==1){
					$output_pengambilan	="
					<div valign='top'>
						Nama	: $row[NamaPengambil]<br>
						KTP		: $row[NoKTPPengambil]<br>
						Waktu	: ".FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])."
						CSO		: $row[NamaPetugasPemberi]
					</div>
					";
				}
				else{
					$output_pengambilan	="
						<div align='center'><a href='' onClick='ambilDataPaket(\"$row[NoTiket]\");return false;'>Ambil Paket</a></div>
					";
				}
				
				$return	.="
					<tr bgcolor='dfdfdf'>
						<td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
						<td>$row[Asal]</td>
						<td>$row[Tujuan]</td>
						<td align='center'>$row[JamBerangkat]</td>
						<td align='left'>$row[KodeKendaraan] ($row[NoPolisi])</td>
						<td align='left'>$row[NamaSopir]</td>
						<td>$row[NoTiket]</td>
						<td valign='top'>
							Nama: $row[NamaPengirim]<br>
							Alamat: $row[AlamatPengirim]<br>
							Telp: $row[TelpPengirim]
						</td>
						<td valign='top'>
							Nama: $row[NamaPenerima]<br>
							Alamat: $row[AlamatPenerima]<br>
							Telp: $row[TelpPenerima]
						</td>
						<td>
							$output_pengambilan
						</td>
					</tr>";
				
				$jum_data++;
	    } 
	  } 
		else{ 
			echo("Err :".__LINE__);exit;
		}
		
		if($jum_data==0){
			$return .="<tr><td colspan=11 class='banner' align='center'><h2>Data paket tidak ditemukan!</h2></td></tr>";
		}
		
		echo($return."</table>");

	exit;
	
	// UBAH FLAG BACA MEMO ==========================================================================================================================================================================================
case "updateflagmemo":
	
	$tgl 					= $HTTP_GET_VARS['tanggal'];
	$kode_jadwal 	= $HTTP_GET_VARS['kode_jadwal'];
	
	$sql = 
		"UPDATE tbl_posisi SET FlagMemo=0
		WHERE TglBerangkat = '$tgl' AND 
			KodeJadwal='$kode_jadwal';";
				
	if (!$db->sql_query($sql)){
		die_error("Err $this->ID_FILE".__LINE__);
		echo(0);
		exit;
	}
	
	echo(1);
	
	exit;
	break;
	
	case "updatestatuscetaktiket":
	
	include($adp_root_path . 'ClassAsuransi.php');
	
	$Asuransi		= new Asuransi();
	$Reservasi	= new Reservasi();
	
	$cetak_tiket				= $HTTP_GET_VARS['cetak_tiket'];
	$list_no_tiket			= str_replace("\'","'",$HTTP_GET_VARS['list_no_tiket']);
	$kode_jadwal				= $HTTP_GET_VARS['kode_jadwal'];
	$tanggal						= $HTTP_GET_VARS['tanggal'];
	$jenis_pembayaran		= $HTTP_GET_VARS['jenis_pembayaran'];
	$list_kode_booking	= str_replace("\'","'",$HTTP_GET_VARS['list_kode_booking']);
	
	if($cetak_tiket!=1){
		//UPDATE FLAG TIKET DI LAYOUT KURSI
		//mengambil jadwal utama
		$sql	=
			"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal';";
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
			
		$row = $db->sql_fetchrow($result);
		$kode_jadwal_utama	= $row[0];
		
		$sql = 
			"UPDATE tbl_posisi_detail SET StatusBayar=1
			WHERE 
			  KodeJadwal='$kode_jadwal_utama' 
				AND TGLBerangkat='$tanggal'
				AND NoTiket IN ($list_no_tiket);";
		
		if(!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
		
		//mengupate flag cetak tiket
	
		$list_kode_booking	= substr($list_kode_booking,0,-1);
		$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
	}
	
	//mengupdate status asuransi menjadi OK
	$Asuransi->ubahFlagBatalAsuransi($list_no_tiket, 0, $userdata['user_id']);

	exit;
//=====================================================================================================================
	case 'sisa_kursi_next':
		//MENGAMBIL DAN MENAMPILKAN SISA KURSI UNTUK BEBERAPA JAM KEDEPAN
		$id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];
		$tgl 						= $HTTP_GET_VARS['tgl_berangkat'];
		$jam_berangkat 	= $HTTP_GET_VARS['jam_berangkat'];
		
		$Jadwal	= new Jadwal();
		
		$result	= $Jadwal->setSisaKursiJadwalNext($tgl,$id_jurusan,$jam_berangkat);
		
	  echo $result;

	exit;
	
//======================================================================================================================	
	case 'ambil_list_harga_paket':
		//Mengambil daftar harga paket
		include($adp_root_path .'/ClassPaket.php');
		$id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];
		
		$Paket= new Paket();
		
		$result= $Paket->ambilDaftarHarga($id_jurusan);
		
		if ($result){
		  $row = $db->sql_fetchrow($result);
			
			//return eval
			echo("
				document.getElementById('dlg_paket_harga_kg_pertama_p').value = $row[HargaPaketPKiloPertama];
				document.getElementById('dlg_paket_harga_kg_pertama_ga').value = $row[HargaPaketGAKiloPertama];
				document.getElementById('dlg_paket_harga_kg_pertama_gd').value = $row[HargaPaketGDKiloPertama];
				document.getElementById('dlg_paket_harga_kg_pertama_s').value = $row[HargaPaketSKiloPertama];
				document.getElementById('dlg_paket_harga_kg_berikutnya_p').value = $row[HargaPaketPKiloBerikut];
				document.getElementById('dlg_paket_harga_kg_berikutnya_ga').value = $row[HargaPaketGAKiloBerikut];
				document.getElementById('dlg_paket_harga_kg_berikutnya_gd').value = $row[HargaPaketGDKiloBerikut];
				document.getElementById('dlg_paket_harga_kg_berikutnya_s').value = $row[HargaPaketSKiloBerikut];
			");
		 
		} 
		else{      
			echo("Error ".__LINE__);
		}
		
	exit;
}



if($user_level!=$LEVEL_SCHEDULER){
	//jika  bukan member, dapat memilih tipe (penumpang/paket)
	
	$tombol_cetak_spj=
		"<a href='#' onclick='setDialogSPJ();'><img src='templates/images/icon_cetak_spj.gif'></a>
		 </br>
		 <a href='#' onclick='setDialogSPJ();'><span class='genmed'>Cetak Manifest</span></a>";
	
}
else{
	//jika   member, tidak dapat memilih tipe (penumpang/paket)
		$select_tipe	=	"<input name='ftipe' id='ftipe' type='hidden' value='penumpang'>Penumpang";
		$kolom_discount="<td colspan='2'></td>";
		$tombol_cetak_spj="";
}
	
include($adp_root_path .'/ClassPengaturanUmum.php');

$PengaturanUmum	= new PengaturanUmum();      

$template->assign_vars (
	array(
		'BCRUMP'    					=> '<a href="'.append_sid('main.'.$phpEx) .'">Home',
		'TOMBOL_CETAK_SPJ'		=> $tombol_cetak_spj,
		'TGL_SEKARANG'				=> dateY_M_D(),
		'OPT_KOTA'						=> setComboKota("BANDUNG"),
		'U_LAPORAN_UANG'			=> "Start('".append_sid('laporan_rekap_uang_user.'.$phpEx.'')."');",
		'U_LAPORAN_PENJUALAN'	=> "Start('".append_sid('laporan_penjualan_user.'.$phpEx.'')."');",
		'U_UBAH_PASSWORD'			=> append_sid('ubah_password.'.$phpEx.''),
		'HARGA_MINIMUM_PAKET'	=> $PengaturanUmum->ambilHargaPaketMinimum()
		)
	);
	

if($userdata['user_level']!=$LEVEL_CSO_PAKET){	
	$template->set_filenames(array('body' => 'reservasi_body.tpl')); 
}
else{
	$template->set_filenames(array('body' => 'reservasi_paket_body.tpl')); 
}

$template->assign_vars(array('DEPOSIT' => $deposit));

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>