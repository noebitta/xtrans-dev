<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$cari  					= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai  = $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir  = $HTTP_GET_VARS['tanggal_akhir'];

$kota		          = isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$filter_by_tglreg	= $HTTP_GET_VARS['filterbytglreg'];
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";

if($userdata["user_level"]==$LEVEL_SUPERVISOR) {
  $kondisi_kota = " AND f_cabang_get_kota_by_kode_cabang('$userdata[KodeCabang]')='$kota'";
}
else{
  $kondisi_kota = $kota == "" ? "" : " AND (f_cabang_get_kota_by_kode_cabang(f_jurusan_get_kode_cabang_asal_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal(KodeJurusanTerakhir)))='$kota')";
}

$kondisi_tglreg	= $filter_by_tglreg==""?"":" AND (TglTerakhirTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') ";

$kondisi =($cari=="")?"":
  " AND (Nama LIKE '%$cari%'
				OR NoHP LIKE '%$cari'
				OR KodeJurusanTerakhir LIKE '%$cari%')";
$sql =
  "SELECT *
			FROM tbl_pelanggan
			WHERE  1 $kondisi $kondisi_tglreg $kondisi_kota
			$kondisi_sort";
	
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

    $show_kota  = $kota==""?"semua kota":$kota;

		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1','Data Pelanggan Per Tanggal '.dateNowD_MMM_Y());
		$objPHPExcel->getActiveSheet()->setCellValue('A2','Kota:'.$show_kota);
	  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Ponsel');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Trx.Pertama');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Trx.Terakhir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jurusan Terakhir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Frekwensi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		
		$idx=0;
		
		while ($row = $db->sql_fetchrow($result)){
			$idx++;
			$idx_row=$idx+3;
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, "'".$row['NoHP']);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparse(FormatTglToMySQLDate($row['TglPertamaTransaksi'])));
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, dateparse(FormatTglToMySQLDate($row['TglTerakhirTransaksi'])));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['KodeJurusanTerakhir']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['FrekwensiPergi']);
			
		}
		$temp_idx=$idx_row;
		
		$idx_row++;		
		
		set_time_limit(60);
		
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Data Pelanggan Per Tanggal '.dateNowD_MMM_Y().' '.$show_kota.'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
		
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
  
?>
