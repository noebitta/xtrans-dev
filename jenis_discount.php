<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 	= $config['perpage'];
$mode    	= $HTTP_GET_VARS['mode'];
$act    	= $HTTP_GET_VARS['act'];
$submode 	= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   	= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$pesan    = $HTTP_GET_VARS['pesan'];

$mode 	= ($mode != '') ? $mode : 'set_awal';
$pesan	= ($pesan =='') ? "<td colspan='9'></td>":"<td colspan='9' bgcolor='yellow'><h2>".$pesan."</h2></td>";

switch($mode){

//mengatur tampilan awal pada halaman laporan rute
case 'set_awal':
		
	$template->set_filenames(array('body' => 'jenis_discount.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  =>$userdata['username'],
	   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('jenis_discount.'.$phpEx).'">Jenis discount</a>',
			/*'U_ADD' =>"<a href='#' OnClick='Tambah();'>Tambah Jenis Discount</a>",*/
			'SID'=>$userdata['sid'],
	  ));
	include($adp_root_path . 'includes/page_header.php');
	$template->pparse('body');
	include($adp_root_path . 'includes/page_tail.php');
exit;

//TAMPILKAN DATA LAPORAN RUTE ==========================================================================================================
case 'tampilkan_data':
	
	//mengambil data transaksi
	
	$sql = "SELECT *
					FROM	tbl_jenis_discount
					ORDER BY FlagLuarKota DESC,NamaDiscount ASC ";
	
	if (!$result = $db->sql_query($sql)){
		die_error('Err:',__LINE__);
		//die_error('GAGAL mengambil data');
	}
	else {
		
		$hasil ="
			<table width='100%' class='border'>
		    <tr>
		      <th width='20'>#</th>
					<th width='100'><font color='white'>Kode Discount</font></th>
					<th width='200'><font color='white'>Nama Discount</font></th>
					<th width='100'><font color='white'>Besar discount</font></th>
					<th width='100'><font color='white'>Jenis</font></th>
					<th width='200'><font color='white'>Cabang Promo</font></th>
					<th width='100'><font color='white'>Status</font></th>
					<th width='100'><font color='white'>Aksi</font></th>
		    </tr>";
			
		while ($row=$db->sql_fetchrow($result)){   
			$i++;
			$odd ="bgcolor='E0E0E0'";
			if (($i % 2)==0){
				$odd = "bgcolor='white'";
			}						
			
			if($row['FlagAktif']){
				$status="<a href='#' onClick='ubahStatus($row[IdDiscount])'>Aktif</a>";
			}
			else{
				$status="<a href='#' onClick='ubahStatus($row[IdDiscount])'>Nonaktif</a>";
				$odd="bgcolor='red'";
			}
			
			$jenis	= ($row['FlagLuarKota'])?"Luar Kota":"Dalam Kota";
			
			$action = "<a href='#' onClick='simpan($row[IdDiscount])'>Simpan</a>";
			
			$hasil .=
			"<tr>
	      <td $odd >$i</td>
				<td $odd >$row[KodeDiscount]</td>
				<td $odd >$row[NamaDiscount]</td>
	      <td $odd align='right'><input type='text' id='txt".$row['IdDiscount']."' value=$row[JumlahDiscount] /></td>
				<td $odd align='center' >$jenis</td>
				<td $odd align='left' >$row[CabangPromo]</td>
				<td $odd align='center' >$status</td>
				<td $odd align='center' >$action</td>
			</tr>";
			
		}
		
		//jika tidak ditemukan data pada database
		if($i==0){
			$hasil=
				"<table width='100%' class='border'>
					<tr><td align='center' bgcolor='EFEFEF'>
						<font color='red'><strong>Data tidak ditemukan!</strong></font>
					</td></tr>
				</table><br><br>";
		}
	}
	
	echo($hasil);
	
exit;

//UBAH ACCOUNT==========================================================================================================
case 'ubah':  
	$id_discount 				= $HTTP_GET_VARS['id_discount'];  
	$besar_discount    	= $HTTP_GET_VARS['besar_discount'];  
	
	$sql =
		"UPDATE tbl_jenis_discount 
		SET JumlahDiscount='$besar_discount'
		WHERE (IdDiscount ='$id_discount')";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL menghapus data anggota',__FILE__,__LINE__,$sql);
		die_error('Err:'.__LINE__);
	}
	
	echo(1);
	
exit;

//UBAH STATUS==========================================================================================================
case 'ubah_status':  
	$id_discount 			= $HTTP_GET_VARS['id_discount'];  
	
	$sql ="CALL sp_jenis_discount_ubah_status_aktif('$id_discount')";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL menghapus data anggota',__FILE__,__LINE__,$sql);
		die_error('GAGAL mengubah data');
	}
	
exit;

//HAPUS ACCOUNT==========================================================================================================
case 'hapus':
	$id_discount    = $HTTP_GET_VARS['id_discount'];  
	
	$sql =
		"DELETE FROM tbl_jenis_discount 
		WHERE (IdDiscount ='$id_discount')";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL menghapus data anggota',__FILE__,__LINE__,$sql);
		die_error('GAGAL menghapus data');
	}
	
	echo(1);
	
exit;
}//switch mode
?>
