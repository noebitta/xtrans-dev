<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassVoucherDiskon.php');


// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_KASIR,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$id_group  = isset($HTTP_GET_VARS['idgroup'])? $HTTP_GET_VARS['idgroup'] : $HTTP_POST_VARS['idgroup'];
$tgl_awal  = isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
$tgl_akhir = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];


//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
$pdf=new PDF('P','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(20,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,50);
$pdf->Ln(5);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(50,4,'','',0,'L');$pdf->Cell(40,8,'PT. BATARA TITIAN KENCANA','',0,'L');$pdf->Ln(5);
$pdf->SetFont('Arial','',10);
$pdf->Cell(50,4,'','',0,'L');$pdf->Cell(40,8,'Jln.Wirosari No. 1, Blora - Menteng','',0,'L');$pdf->Ln(5);
$pdf->Cell(50,4,'','',0,'L');$pdf->Cell(40,8,'Jakarta Pusat-Jakarta, Indonesia','',0,'L');$pdf->Ln(5);
$pdf->Cell(50,4,'','',0,'L');$pdf->Cell(40,8,'Telp: 022-3150555','',0,'L');$pdf->Ln(15);

$pdf->SetFont('Arial','',11);
$pdf->Cell(20,7,'Perihal','',0,'L');$pdf->Cell(5,7,': INVOICE','',0,'');$pdf->Cell(150,7,"Jakarta, ".dateparse(dateD_M_Y()),'',0,'R');$pdf->Ln();
$pdf->Cell(20,7,'Lampiran','',0,'L');$pdf->Cell(5,7,': 1 Lampiran','',0,'');$pdf->Ln();
$pdf->Ln(10);

//MENGAMBIL DATA KORPORAT
$Voucher	= new Voucher();

$data_korporat	= $Voucher->getDetailGroup($id_group);

$pdf->Cell(20,7,'Kepada','',0,'L');$pdf->Ln();
$pdf->Cell(20,7,'Divisi Keuangan '.$data_korporat['NamaGroup'],'',0,'L');$pdf->Ln();
$pdf->Cell(20,7,'Ditempat','',0,'L');$pdf->Ln(15);

//QUERY AMBIL TOTAL TAGIHAN
$sql = 
	"SELECT
		COUNT(1) AS VoucherDipakai,
		SUM(TotalBayar) AS TotalSettlement
	FROM tbl_voucher
	WHERE IdGroup='$id_group' AND (WaktuDigunakan BETWEEN '$tgl_awal 00:00:00' AND '$tgl_akhir 23:59:59')
	GROUP BY IdGroup";

if(!$result=$db->sql_query($sql)){
	die_error("ERROR QUERY",__LINE__,basename(__FILE__),"");
}

$data_total	= $db->sql_fetchrow($result);

$pdf->Cell(20,7,'Dengan Hormat,','',0,'L');$pdf->Ln();
$pdf->MultiCell2(175,7,'Berdasarkan perjanjian kerjasama antara '.$data_korporat['NamaGroup'].' dengan XTrans mengenai penggunaan voucher dengan sistem settlement, dengan ini '.
								 'kami menyampaikan perihal penagihan pembayaran yang harus dilakukan '.$data_korporat['NamaGroup'].' kepada XTrans untuk periode transaksi mulai tanggal '.
								dateparse(FormatMySQLDateToTgl($tgl_awal)).' sampai dengan tanggal '.dateparse(FormatMySQLDateToTgl($tgl_akhir)).
								'yaitu dengan jumlah penagihan sebesar Rp. '.number_format($data_total[1],0,",",".").' dengan jumlah pemakain voucher sebanyak '.$data_total[0].' voucher.'.
								' Mohon tagihan ini dibayar selambat-lambatnya 1 minggu dari tanggal surat ini. Pembayaran dapat dilakukan dengan melalui transfer ke rekening:
								
								Bank Central Asia (BCA) KCP Wisma Indocement
								
								No. Rekening: 459-3022377, Atas Nama BATARA TITIAN KENCANA, PT
								
								Demikian Invoice ini dibuat, untuk perhatian dan kerjasamanya, kami ucapkan terima kasih.',0,'J',0);$pdf->Ln(15);


$pdf->Cell(130,7,'','',0,'L');$pdf->Cell(45,7,'Hormat Kami,','',0,'C');$pdf->Ln(30);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(130,7,'','',0,'L');$pdf->Cell(45,7,'Sutanti Budiarti,','',0,'C');$pdf->Ln();

$pdf->AddPage('L');

$pdf->SetFont('arial','B',10);

$pdf->Setmargins(10,10,10,10);
$pdf->Cell(250,5,"DAFTAR VOUCHER TERPAKAI",0,0,'C',0);$pdf->Ln();
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(40,5,'#Voucher','B',0,'C',1);
$pdf->Cell(40,5,'Digunakan','B',0,'C',1);
$pdf->Cell(40,5,'#Tiket','B',0,'C',1);
$pdf->Cell(30,5,'#Jadwal.','B',0,'C',1);
$pdf->Cell(40,5,'Penumpang','B',0,'C',1);
$pdf->Cell(40,5,'Telp','B',0,'C',1);
$pdf->Cell(40,5,'Bayar','B',0,'C',1);
$pdf->Ln(7);

$pdf->SetFont('arial','',10);
$pdf->SetTextColor(0);


//CONTENT
$sql = 
	"SELECT *
	FROM tbl_voucher
	WHERE IdGroup='$id_group' AND (WaktuDigunakan BETWEEN '$tgl_awal 00:00:00' AND '$tgl_akhir 23:59:59')
	ORDER BY WaktuDigunakan";
	
if(!$result=$db->sql_query($sql)){
	die_error("ERROR QUERY",__LINE__,basename(__FILE__),"");
}

$i=0;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
		$i++;
	
	if (($i % 2)==0){
		$pdf->SetFillColor(220,220,220);
	}
	else{
		$pdf->SetFillColor(255,255,255);
	}
	
	
	$pdf->Cell(5,5,$i,0,0,'C',1);
	$pdf->Cell(40,5,$row['KodeVoucher'],0,0,'C',1);
	$pdf->Cell(40,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuDigunakan'])),0,0,'C',1);
	$pdf->Cell(40,5,$row['NoTiketBerangkat'],0,0,'C',1);
	$pdf->Cell(30,5,$row['KodeJadwalBerangkat'],0,0,'C',1);
	$pdf->Cell(40,5,substr($row['NamaPenumpang'],0,16),0,0,'C',1);
	$pdf->Cell(40,5,$row['Telp'],0,0,'C',1);
	$pdf->Cell(40,5,"Rp.".number_format($row['TotalBayar'],0,",","."),0,0,'R',1);
	$pdf->Ln();

}

$pdf->Ln();												
$pdf->Output();
						
?>