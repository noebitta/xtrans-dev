<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$template->set_filenames(array('body' => 'laporan_omzet_jadwal/laporan_omzet_jadwal_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Cabang								= new Cabang();
$Jurusan 							= new Jurusan();

if(in_array($userdata['user_level'],array($LEVEL_ADMIN))){
	$kondisi_cabang	= ($kode_cabang=="")?"":" AND KodeCabang='$kode_cabang'";
	$cabang_default	= "";
}
else{
	$kondisi_cabang	= " AND KodeCabang='$userdata[KodeCabang]'";
	$cabang_default	= $userdata['KodeCabang'];
}

switch($mode){
	case 'gettujuan':
		$result = $Jurusan->setComboJurusan($asal);

		$opt = "";
		if ($result){
			$opt = "<option value=''>(none)</option>" . $opt;

			while ($row = $db->sql_fetchrow($result)){
				$selected = ($tujuan!=$row[0])?"":"selected";
				$opt .= "<option value='$row[0]' $selected>$row[1] ($row[2])</option>";
			}
		}
		else{
			$opt .="<option selected=selected>Error</option>";
		}

		echo "
			<select name='tujuan' id='tujuan' >
				".$opt."
			</select>";
	exit;
}

if($asal!="" && $tujuan!=""){
	$kondisi_cabang.= " AND IdJurusan='$tujuan'";
}
	
$sql=
	"SELECT 
		IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
		JamBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(IF(JenisPenumpang='U' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangU,
		IS_NULL(SUM(IF(JenisPenumpang='M' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangM,
		IS_NULL(SUM(IF(JenisPenumpang='K' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangK,
		IS_NULL(SUM(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangKK,
		IS_NULL(SUM(IF(JenisPenumpang='G' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangG,
		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangT,

		IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB, 

		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,0),Total)),0) AS TotalOmzet,
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM tbl_reservasi_olap
	WHERE  (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_cabang
	GROUP BY JamBerangkat
	ORDER BY JamBerangkat ";

	
if (!$result = $db->sql_query($sql))
{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);
}

//memasukkan ke array
$data_laporan	= array();
while($row = $db->sql_fetchrow($result)){
	$data_laporan[$row['JamBerangkat']]['JamBerangkat']			= $row['JamBerangkat'];
	$data_laporan[$row['JamBerangkat']]['TotalBerangkat']		= $row['TotalBerangkat'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangU']		= $row['TotalPenumpangU'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangM']		= $row['TotalPenumpangM'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangK']		= $row['TotalPenumpangK'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangKK']		= $row['TotalPenumpangKK'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangG']		= $row['TotalPenumpangG'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangT']		= $row['TotalPenumpangT'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangRB']		= $row['TotalPenumpangRB'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangR']		= $row['TotalPenumpangR'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangVR']		= $row['TotalPenumpangVR'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangV']		= $row['TotalPenumpangV'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpang']		= $row['TotalPenumpang'];
	$data_laporan[$row['JamBerangkat']]['TotalOmzet']			= $row['TotalOmzet'];
	$data_laporan[$row['JamBerangkat']]['TotalDiscount']		= $row['TotalDiscount'];
}

//jika ada dalam filter tanggal harus mengambil dari tbl_reservasi
//note: tiap hari transaksi di tbl_reservasi di backup ke tbl_reservasi_olap
if($tanggal_akhir_mysql>=date("Y-m-d")){
	$sql=
		"SELECT 
			IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
			JamBerangkat,
			IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
			IS_NULL(SUM(IF(JenisPenumpang='U' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangU,
			IS_NULL(SUM(IF(JenisPenumpang='M' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangM,
			IS_NULL(SUM(IF(JenisPenumpang='K' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangK,
			IS_NULL(SUM(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangKK,
			IS_NULL(SUM(IF(JenisPenumpang='G' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangG,
			IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,1,0)),0) AS TotalPenumpangT,
			IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangRB, 
			IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
			IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
			IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
			IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
			IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,0),Total)),0) AS TotalOmzet,
			IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
		FROM tbl_reservasi
		WHERE  (TglBerangkat BETWEEN '".date("Y-m-d")."' AND '$tanggal_akhir_mysql') 
			AND CetakTiket=1 AND FlagBatal!=1
			$kondisi_cabang
		GROUP BY JamBerangkat
		ORDER BY JamBerangkat ";
	
		
	if (!$result = $db->sql_query($sql)){
		//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
		echo("Err:".__LINE__);exit;
	}
	
	//memasukkan ke array
	while($row = $db->sql_fetchrow($result)){
		$data_laporan[$row['JamBerangkat']]['JamBerangkat']			= $row['JamBerangkat'];
		$data_laporan[$row['JamBerangkat']]['TotalBerangkat']		+= $row['TotalBerangkat'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangU']		+= $row['TotalPenumpangU'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangM']		+= $row['TotalPenumpangM'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangK']		+= $row['TotalPenumpangK'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangKK']		+= $row['TotalPenumpangKK'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangG']		+= $row['TotalPenumpangG'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangT']		+= $row['TotalPenumpangT'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangRB']		+= $row['TotalPenumpangRB'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangR']		+= $row['TotalPenumpangR'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangVR']		+= $row['TotalPenumpangVR'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangV']		+= $row['TotalPenumpangV'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpang']		+= $row['TotalPenumpang'];
		$data_laporan[$row['JamBerangkat']]['TotalOmzet']			+= $row['TotalOmzet'];
		$data_laporan[$row['JamBerangkat']]['TotalDiscount']		+= $row['TotalDiscount'];
	}
}

$sum_berangkat			= 0;
$sum_penumpang_u		= 0;
$sum_penumpang_m		= 0;
$sum_penumpang_k		= 0;
$sum_penumpang_kk		= 0;
$sum_penumpang_g		= 0;
$sum_penumpang_t		= 0;
$sum_penumpang_r		= 0;
$sum_penumpang_rb		= 0;
$sum_penumpang_vr		= 0;
$sum_penumpang_v		= 0;
$sum_penumpang			= 0;
$sum_omzet				= 0;
$sum_discount			= 0;

$idx=0;

foreach($data_laporan as $row_laporan){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$idx++;
	
	
	$total_berangkat	= $row_laporan['TotalBerangkat']; 
	$total_penumpang_u	= $row_laporan['TotalPenumpangU']; 
	$total_penumpang_m	= $row_laporan['TotalPenumpangM']; 
	$total_penumpang_k	= $row_laporan['TotalPenumpangK']; 
	$total_penumpang_kk	= $row_laporan['TotalPenumpangKK']; 
	$total_penumpang_g	= $row_laporan['TotalPenumpangG']; 
	$total_penumpang_t	= $row_laporan['TotalPenumpangT'];
	$total_penumpang_rb	= $row_laporan['TotalPenumpangRB'];
	$total_penumpang_r	= $row_laporan['TotalPenumpangR'];
	$total_penumpang_vr	= $row_laporan['TotalPenumpangVR'];
	$total_penumpang_v	= $row_laporan['TotalPenumpangV'];
	$total_penumpang	= $row_laporan['TotalPenumpang'];
	$total_omzet		= $row_laporan['TotalOmzet']; 
	$total_discount		= $row_laporan['TotalDiscount']; 
		
	
	$sum_berangkat			+= $total_berangkat;
	$sum_penumpang_u		+= $total_penumpang_u;
	$sum_penumpang_m		+= $total_penumpang_m;
	$sum_penumpang_k		+= $total_penumpang_k;
	$sum_penumpang_kk		+= $total_penumpang_kk;
	$sum_penumpang_g		+= $total_penumpang_g;
	$sum_penumpang_t		+= $total_penumpang_t;
	$sum_penumpang_rb		+= $total_penumpang_rb;
	$sum_penumpang_r		+= $total_penumpang_r;
	$sum_penumpang_vr		+= $total_penumpang_vr;
	$sum_penumpang_v		+= $total_penumpang_v;
	$sum_penumpang			+= $total_penumpang;
	$sum_omzet					+= $total_omzet;
	$sum_discount				+= $total_discount;
	
	$rata_penumpang_per_trip	=($total_berangkat>0)?$total_penumpang/$total_berangkat:0;
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'jam'=>$row_laporan['JamBerangkat'],
				'total_keberangkatan'=>number_format($total_berangkat,0,",","."),
				'total_penumpang_u'=>number_format($total_penumpang_u,0,",","."),
				'total_penumpang_m'=>number_format($total_penumpang_m,0,",","."),
				'total_penumpang_k'=>number_format($total_penumpang_k,0,",","."),
				'total_penumpang_kk'=>number_format($total_penumpang_kk,0,",","."),
				'total_penumpang_g'=>number_format($total_penumpang_g,0,",","."),
				'total_penumpang_t'=>number_format($total_penumpang_t,0,",","."),
				'total_penumpang_rb'=>number_format($total_penumpang_rb,0,",","."),
				'total_penumpang_r'=>number_format($total_penumpang_r,0,",","."),
				'total_penumpang_vr'=>number_format($total_penumpang_vr,0,",","."),
				'total_penumpang_v'=>number_format($total_penumpang_v,0,",","."),
				'total_penumpang'=>number_format($total_penumpang,0,",","."),
				'rata_pnp_per_trip'=>number_format($rata_penumpang_per_trip,0,",","."),
				'total_omzet'=>number_format($total_omzet,0,",","."),
				'total_discount'=>number_format($total_discount,0,",","."),
			)
	);
}

//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&asal=".$asal."&tujuan=".$tujuan;
	
$script_cetak_pdf="Start('laporan_omzet_jadwal_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_omzet_jadwal_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$rata_penumpang_per_trip	=($sum_berangkat>0)?$sum_penumpang/$sum_berangkat:0;

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_omzet">Home</a> | <a href="'.append_sid('laporan_omzet_jadwal.'.$phpEx).'">Laporan Omzet Jadwal</a>',
	'URL'						=> append_sid('laporan_omzet.'.$phpEx).$parameter,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_ASAL'			=> $Cabang->setInterfaceComboCabang($asal,$cabang_default),
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'SUM_KEBERANGKATAN'		=>number_format($sum_berangkat,0,",","."),
	'SUM_PENUMPANG_U'		=>number_format($sum_penumpang_u,0,",","."),
	'SUM_PENUMPANG_M'		=>number_format($sum_penumpang_m,0,",","."),
	'SUM_PENUMPANG_K'		=>number_format($sum_penumpang_k,0,",","."),
	'SUM_PENUMPANG_KK'		=>number_format($sum_penumpang_kk,0,",","."),
	'SUM_PENUMPANG_G'		=>number_format($sum_penumpang_g,0,",","."),
	'SUM_PENUMPANG_T'		=>number_format($sum_penumpang_t,0,",","."),
	'SUM_PENUMPANG_RB'		=>number_format($sum_penumpang_rb,0,",","."),
	'SUM_PENUMPANG_R'		=>number_format($sum_penumpang_r,0,",","."),
	'SUM_PENUMPANG_VR'		=>number_format($sum_penumpang_vr,0,",","."),
	'SUM_PENUMPANG_V'		=>number_format($sum_penumpang_v,0,",","."),
	'SUM_PENUMPANG'			=>number_format($sum_penumpang,0,",","."),
	'RATA_PNP_PER_TRIP'		=>number_format($rata_penumpang_per_trip,0,",","."),
	'SUM_OMZET'				=> number_format($sum_omzet,0,",","."),
	'SUM_DISCOUNT'			=> number_format($sum_discount,0,",","."),
	'SUM_BIAYA'				=> number_format($sum_biaya,0,",","."),
	'SUM_PROFIT'			=> number_format($sum_profit,0,",","."),
	'U_GRAFIK'				=> append_sid('laporan_omzet_jadwal_grafik.'.$phpEx).$parameter_cetak,
	'CETAK_PDF'				=> $script_cetak_pdf,
	'CETAK_XL'				=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>