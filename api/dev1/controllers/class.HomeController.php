<?php
/**
 * Home controller.
 *
 * Last update: Feb 09, 2012 22:44
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */
class HomeController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Halaman default
	 */
	public function index()
	{
		$data = 'TIKETUX XTRANS API';

		$this->sendResponse($data);
	}
}
?>