<?php
/**
 * Jadwal model.
 *
 * Last update: Jan 10, 2013 08:42 AM
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */
class JadwalModel extends Model
{
	public function __construct()
	{
		parent::__construct();

		$this->_table = 'tbl_md_jadwal';
	}

	/**
	 * Mendapatkan daftar jadwal aktif
	 *
	 * @return array Daftar jadwal
	 */
	public function getList()
	{
		return $this->findAll(array('filter' => array('IsOnline=1',
													  "KodeJadwal NOT LIKE '%-E1%'")));
	}

	/**
	 * Mendapatkan detail jadwal.
	 *
	 * @param string $kode Kode jadwal
	 *
	 * @return object Detail jadwal
	 */
	public function getDetail($kode)
	{
		global $cfg;

		$sql = "SELECT
						*
				FROM
						tbl_md_jadwal
				JOIN
						tbl_md_jurusan
				USING(IdJurusan)
				WHERE
						KodeJadwal = '$kode'";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan daftar jadwal untuk suatu jurusan.
	 *
	 * @param int $jurusan Id Jurusan
	 * @param string $jam Batas bawah jam berangkat
	 *
	 * @return array Daftar jadwal
	 */
	public function getListByJurusan($jurusan, $jam)
	{
		global $cfg;

		$jam = (empty($jam)) ? '00:00' : $jam;

		$sql = "SELECT
						*
				FROM
						tbl_md_jadwal
				WHERE
						IdJurusan = '$jurusan'
						AND
						IsOnline = 1
						AND
						FlagAktif = 1
						AND
						KodeJadwal NOT LIKE '%-E1%'
						AND
						JamBerangkat >= '$jam'
				ORDER BY
						JamBerangkat ASC";

		$res = array();

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan data penjadwalan untuk jurusan, jadwal dan tanggal tertentu
	 *
	 * @param int $jurusan Id Jurusan
	 * @param string $jadwal Kode jadwal
	 * @param string $tanggal Tanggal keberangkatan
	 *
	 * @return array Data penjadwalan
	 */
	public function getPenjadwalan($jurusan, $jadwal, $tanggal)
	{
		global $cfg;

		$sql = "SELECT
						StatusAktif
				FROM
						tbl_penjadwalan_kendaraan
				WHERE
						IdJurusan = '$jurusan'
						AND
						KodeJadwal = '$jadwal'
						AND
						TglBerangkat = '$tanggal'
				ORDER BY
						JamBerangkat ASC";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan data reservasi untuk jurusan dan tanggal tertentu
	 *
	 * @param int $jurusan Id jurusan
	 * @param string $tanggal Tanggal keberangkatan
	 *
	 * @return array Daftar reservasi
	 */
	public function getReservasiList($jurusan, $tanggal)
	{
		global $cfg;

		$sql = "SELECT
						COUNT(j.KodeJadwal) AS JumlahBooking,
						j.KodeJadwal
				FROM
						tbl_md_jadwal j
				JOIN
						tbl_posisi_detail r
				ON IF(j.FlagSubJadwal=0,j.KodeJadwal,j.KodeJadwalUtama)=r.kodejadwal
				WHERE
						j.IdJurusan = '$jurusan'
						AND
						r.TglBerangkat ='$tanggal'
						AND
						StatusKursi != 0
				GROUP BY
						j.KodeJadwal";

		$res = array();

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan nomor kursi yang telah dibooking
	 *
	 * @param string $kode Kode jadwal
	 * @param string $tanggal Tanggal berangkat (yyyy-mm-dd)
	 *
	 * @return array Nomor kursi
	 */
	public function getPosisiKursi($kode, $tanggal)
	{
		global $cfg;

		$sql = "SELECT
						GROUP_CONCAT(CONVERT(NomorKursi, CHAR(1)) ORDER BY NomorKursi)  AS NomorKursi
				FROM
						tbl_reservasi
				WHERE
						KodeJadwal = '$kode'
						AND
						TglBerangkat ='$tanggal'
				GROUP BY
						KodeJadwal";

		$res = array();

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();

		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}

	/**
	 * Mendapatkan jumlah kursi dari suatu kode jadwal.
	 *
	 * @param string $kode Kode jadwal
	 *
	 * @return int Jumlah kursi
	 */
	public function getJumlahKursi($kode)
	{
		global $cfg;

		$sql = "SELECT
						JumlahKursi
				FROM
						tbl_md_jadwal
				WHERE
						KodeJadwal = '$kode'";

		$res = 0;

		try {
			$this->_dbObj->query($sql);

			$data = $this->_dbObj->fetch();
			$res  = $data->JumlahKursi;
		} catch (DbException $e) {
			Error::store('Jadwal', $e->getMessage());
		}

		return $res;
	}
}

?>