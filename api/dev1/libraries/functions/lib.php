<?php
/**
 * All global functions defined here
 */
function __p($array)
{
    print_r($array);
}

function __vd($vd)
{
    var_dump($vd);
}

function app_shutdown($message) {
    die("Kelimutu: $message");
}

function formatDate($date, $format='m/d/Y')
{
    return ($date == null || $date == '0000-00-00 00:00:00' || $date == '0000-00-00') ? '' : date($format, strtotime($date));
}

function zeroExtend($s, $n)
{
    for ($i = 0; $i < $n; $i++) {
        if (strlen($s) == $n) return $s;
        $s = "0".$s;
    }

    return $s;
}

function firstCapital($str)
{
	$str    = strtolower($str);
    $arrstr = explode(' ', $str);
	$result = array();

    for ($i = 0; $i < sizeof($arrstr); $i++)
    {
        if (strlen($arrstr[$i]) > 0) {
			$result[$i] = ucfirst($arrstr[$i]);
		}
    }

    return implode(' ', $result);
}

function getSQLLimit($page, $itemPerPage)
{
	global $cfg;

	$page			= (empty($page)) ? 1 : $page;
	$itemPerPage	= (empty($itemPerPage)) ? $cfg['sys']['itemPerPage'] : $itemPerPage;
	$limit			= ($page-1) * $itemPerPage;

	$limit 			= "LIMIT $itemPerPage OFFSET $limit";

	return $limit;
}
?>