<?php
/**
 * Database config file.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

$cfg['db']['host']     = 'localhost';
$cfg['db']['name']     = 'tiketux_xtrans_dummy';
$cfg['db']['user']     = 'root';
$cfg['db']['password'] = '4sy3333mK3cut';
$cfg['db']['port']     = '3306';
?>
