<?php
/**
 * System config
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

$cfg['sys']['dbDriver']       = 'MySQL';
$cfg['sys']['authType']       = 'db';
$cfg['sys']['tblPrefix']      = 'tj';
$cfg['sys']['debug']          = true;
$cfg['sys']['theme']          = 'default';
$cfg['sys']['itemPerPage']    = 30;
$cfg['sys']['responsePrefix'] = 'xtrans';
$cfg['sys']['siteName']       = 'XTRANS API';
$cfg['sys']['enableAuth']     = true;
$cfg['sys']['authLevel']      = 1;
$cfg['sys']['timestampSkew']  = 600;
$cfg['sys']['baseUrl']        = 'http://localhost/api/xtrans';
?>