<?php
/**
 * index.php
 *
 * Main access point, instantiate dispatcher and let it handle all incoming requests.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

include 'init.php';

$dispatcher = new Dispatcher();

$dispatcher->enableDebug(true);
$dispatcher->dispatch();
?>