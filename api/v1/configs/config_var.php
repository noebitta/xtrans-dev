<?php
/**
 * Global varible.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */


/**
 * rows = Jumlah baris
 * cols = Jumlah kolom
 * seat = layout kursi
 * type = P = Penumpang, K = Kosong, S = Sopir
 *
 */

$cfg['seat']['9'] =  array('rows'  => 4,
                           'cols'  => 3,
                           'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                            array('row' => 1, 'col' => 2, 'no' => '9', 'type' => 'P'),
                                            array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                            array('row' => 2, 'col' => 1, 'no' => '2', 'type' => 'P'),
                                            array('row' => 2, 'col' => 2, 'no' => '',  'type' => 'K'),
                                            array('row' => 2, 'col' => 3, 'no' => '3', 'type' => 'P'),

                                            array('row' => 3, 'col' => 1, 'no' => '4', 'type' => 'P'),
                                            array('row' => 3, 'col' => 2, 'no' => '',  'type' => 'K'),
                                            array('row' => 3, 'col' => 3, 'no' => '5', 'type' => 'P'),

                                            array('row' => 4, 'col' => 1, 'no' => '6', 'type' => 'P'),
                                            array('row' => 4, 'col' => 2, 'no' => '7', 'type' => 'P'),
                                            array('row' => 4, 'col' => 3, 'no' => '8', 'type' => 'P')
                                            )
                           );

$cfg['seat']['10']  = array('rows'  => 4,
                            'cols'  => 3,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '11','type' => 'P'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '2', 'type' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '3', 'type' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '4', 'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '',  'type' => 'K'),
                                             array('row' => 3, 'col' => 2, 'no' => '6', 'type' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '7', 'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '8', 'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '9', 'type' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '10','type' => 'P')
                                             )
                            );

$cfg['seat']['11']  = array('rows'  => 4,
                            'cols'  => 3,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '2', 'type' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '3', 'type' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '4', 'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '5', 'type' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '6', 'type' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '7', 'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '8', 'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '9', 'type' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '10','type' => 'P')
                                             )
                            );

$cfg['seat']['131']  = array('rows'  => 5,
                             'cols'  => 3,
                             'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                              array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                              array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                              array('row' => 2, 'col' => 1, 'no' => '2', 'type' => 'P'),
                                              array('row' => 2, 'col' => 2, 'no' => '3', 'type' => 'P'),
                                              array('row' => 2, 'col' => 3, 'no' => '4', 'type' => 'P'),

                                              array('row' => 3, 'col' => 1, 'no' => '5', 'type' => 'P'),
                                              array('row' => 3, 'col' => 2, 'no' => '6', 'type' => 'P'),
                                              array('row' => 3, 'col' => 3, 'no' => '7', 'type' => 'P'),

                                              array('row' => 4, 'col' => 1, 'no' => '8', 'type' => 'P'),
                                              array('row' => 4, 'col' => 2, 'no' => '9', 'type' => 'P'),
                                              array('row' => 4, 'col' => 3, 'no' => '10','type' => 'P'),

                                              array('row' => 5, 'col' => 1, 'no' => '11','type' => 'P'),
                                              array('row' => 5, 'col' => 2, 'no' => '12','type' => 'P'),
                                              array('row' => 5, 'col' => 3, 'no' =>  '', 'type' => 'K')
                                             )
                            );

$cfg['seat']['132']  = array('rows'  => 4,
                             'cols'  => 4,
                             'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                              array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                              array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'K'),
                                              array('row' => 1, 'col' => 4, 'no' => '',  'type' => 'S'),

                                              array('row' => 2, 'col' => 1, 'no' => '',  'type' => 'K'),
                                              array('row' => 2, 'col' => 2, 'no' => '',  'type' => 'K'),
                                              array('row' => 2, 'col' => 3, 'no' => '2', 'type' => 'P'),
                                              array('row' => 2, 'col' => 4, 'no' => '3', 'type' => 'P'),

                                              array('row' => 3, 'col' => 1, 'no' => '4', 'type' => 'P'),
                                              array('row' => 3, 'col' => 2, 'no' => '',  'type' => 'K'),
                                              array('row' => 3, 'col' => 3, 'no' => '5', 'type' => 'P'),
                                              array('row' => 3, 'col' => 4, 'no' => '6', 'type' => 'P'),

                                              array('row' => 4, 'col' => 1, 'no' => '',  'type' => 'K'),
                                              array('row' => 4, 'col' => 2, 'no' => '',  'type' => 'K'),
                                              array('row' => 4, 'col' => 3, 'no' => '7', 'type' => 'P'),
                                              array('row' => 4, 'col' => 4, 'no' => '8', 'type' => 'P'),

                                              array('row' => 5, 'col' => 1, 'no' => '9',  'type' => 'P'),
                                              array('row' => 5, 'col' => 2, 'no' => '10', 'type' => 'P'),
                                              array('row' => 5, 'col' => 3, 'no' => '11', 'type' => 'P'),
                                              array('row' => 5, 'col' => 4, 'no' => '12', 'type' => 'P')
                                             )
                             );

$cfg['seat']['133']  = array('rows'  => 5,
                             'cols'  => 3,
                             'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                              array('row' => 1, 'col' => 2, 'no' => '13',  'type' => 'P'),
                                              array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                              array('row' => 2, 'col' => 1, 'no' => '2', 'type' => 'P'),
                                              array('row' => 2, 'col' => 2, 'no' => '3', 'type' => 'P'),
                                              array('row' => 2, 'col' => 3, 'no' => '4', 'type' => 'P'),

                                              array('row' => 3, 'col' => 1, 'no' => '5', 'type' => 'P'),
                                              array('row' => 3, 'col' => 2, 'no' => '6', 'type' => 'P'),
                                              array('row' => 3, 'col' => 3, 'no' => '7', 'type' => 'P'),

                                              array('row' => 4, 'col' => 1, 'no' => '', 'type' => 'K'),
                                              array('row' => 4, 'col' => 2, 'no' => '8', 'type' => 'P'),
                                              array('row' => 4, 'col' => 3, 'no' => '9','type' => 'P'),

                                              array('row' => 5, 'col' => 1, 'no' => '10','type' => 'P'),
                                              array('row' => 5, 'col' => 2, 'no' => '11','type' => 'P'),
                                              array('row' => 5, 'col' => 3, 'no' =>  '12', 'type' => 'P')
                                             )
                            );

$cfg['seat']['14']  = array('rows'  => 5,
                            'cols'  => 3,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '2',  'type' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '3',  'type' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '4',  'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '5',  'type' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '6',  'type' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '7',  'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '8',  'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '9',  'type' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '10', 'type' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '11',   'type' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '12',   'type' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '13',   'type' => 'P'),
                                            )
                            );

$cfg['seat']['19']  = array('rows'  => 6,
                            'cols'  => 5,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 4, 'no' => '2', 'type' => 'P'),
                                             array('row' => 1, 'col' => 5, 'no' => '3', 'type' => 'P'),

                                             array('row' => 2, 'col' => 1, 'no' => '4', 'type' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 2, 'col' => 3, 'no' => '',  'type' => 'K'),
                                             array('row' => 2, 'col' => 4, 'no' => '5', 'type' => 'P'),
                                             array('row' => 2, 'col' => 5, 'no' => '6', 'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '',  'type' => 'K'),
                                             array('row' => 3, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 3, 'col' => 3, 'no' => '',  'type' => 'K'),
                                             array('row' => 3, 'col' => 4, 'no' => '7', 'type' => 'P'),
                                             array('row' => 3, 'col' => 5, 'no' => '8', 'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '9',  'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '',   'type' => 'K'),
                                             array('row' => 4, 'col' => 3, 'no' => '',   'type' => 'K'),
                                             array('row' => 4, 'col' => 4, 'no' => '10', 'type' => 'P'),
                                             array('row' => 4, 'col' => 5, 'no' => '11', 'type' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '12', 'type' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '',   'type' => 'K'),
                                             array('row' => 5, 'col' => 3, 'no' => '',   'type' => 'K'),
                                             array('row' => 5, 'col' => 4, 'no' => '13', 'type' => 'P'),
                                             array('row' => 5, 'col' => 5, 'no' => '14', 'type' => 'P'),

                                             array('row' => 6, 'col' => 1, 'no' => '15', 'type' => 'P'),
                                             array('row' => 6, 'col' => 2, 'no' => '16', 'type' => 'P'),
                                             array('row' => 6, 'col' => 3, 'no' => '17', 'type' => 'P'),
                                             array('row' => 6, 'col' => 4, 'no' => '18', 'type' => 'P'),
                                             array('row' => 6, 'col' => 5, 'no' => '19', 'type' => 'P'),
                                            )
                            );

$cfg['seat']['29']  = array('rows'  => 7,
                            'cols'  => 5,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '2', 'type' => 'P'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 4, 'no' => '3', 'type' => 'P'),
                                             array('row' => 1, 'col' => 5, 'no' => '4', 'type' => 'P'),

                                             array('row' => 2, 'col' => 1, 'no' => '5', 'type' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '6', 'type' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '',  'type' => 'K'),
                                             array('row' => 2, 'col' => 4, 'no' => '7', 'type' => 'P'),
                                             array('row' => 2, 'col' => 5, 'no' => '8', 'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '9',  'type' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '10', 'type' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '',   'type' => 'K'),
                                             array('row' => 3, 'col' => 4, 'no' => '11', 'type' => 'P'),
                                             array('row' => 3, 'col' => 5, 'no' => '12', 'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '13', 'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '14', 'type' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '',   'type' => 'K'),
                                             array('row' => 4, 'col' => 4, 'no' => '15', 'type' => 'P'),
                                             array('row' => 4, 'col' => 5, 'no' => '16', 'type' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '17', 'type' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '18', 'type' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '',   'type' => 'K'),
                                             array('row' => 5, 'col' => 4, 'no' => '19', 'type' => 'P'),
                                             array('row' => 5, 'col' => 5, 'no' => '20', 'type' => 'P'),

                                             array('row' => 6, 'col' => 1, 'no' => '21', 'type' => 'P'),
                                             array('row' => 6, 'col' => 2, 'no' => '22', 'type' => 'P'),
                                             array('row' => 6, 'col' => 3, 'no' => '',   'type' => 'K'),
                                             array('row' => 6, 'col' => 4, 'no' => '23', 'type' => 'P'),
                                             array('row' => 6, 'col' => 5, 'no' => '24', 'type' => 'P'),

                                             array('row' => 7, 'col' => 1, 'no' => '25', 'type' => 'P'),
                                             array('row' => 7, 'col' => 2, 'no' => '26', 'type' => 'P'),
                                             array('row' => 7, 'col' => 3, 'no' => '27', 'type' => 'P'),
                                             array('row' => 7, 'col' => 4, 'no' => '28', 'type' => 'P'),
                                             array('row' => 7, 'col' => 5, 'no' => '29', 'type' => 'P'),
                                            )
                            );

$cfg['seat']['101']  = array('rows'  => 5,
                            'cols'  => 3,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '2', 'type' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 2, 'col' => 3, 'no' => '3', 'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '4',  'type' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '',   'type' => 'K'),
                                             array('row' => 3, 'col' => 3, 'no' => '5',  'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '6', 'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 4, 'col' => 3, 'no' => '7', 'type' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '8',  'type' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '9',  'type' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '10', 'type' => 'P'),
                                             )
                            );
?>