<?php
/**
 * Cabang controller.
 *
 * Last update: Feb 09, 2012 22:44
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class CabangController extends Controller
{
	private $_cabangModel;

	public function __construct()
	{
		parent::__construct();

		$this->_cabangModel = $this->loadModel('Cabang');
	}

	/**
	 * Mendapatkan daftar cabang.
	 *
	 * URL: GET /cabang
	 *
	 * Paramaters: -
	 *
	 * Requires authentication: YES
	 *
	 * HTTP method: GET
	 */
	public function index()
	{
		$this->setRequestMethod('GET');
		//$this->authenticate(1);

		$data		= array();
		$cabangs	= array();
		$results	= array();
		$status		= 'ZERO_RESULTS';

		$cabangList = $this->_cabangModel->getList();

		if (is_array($cabangList) && sizeof($cabangList)) {
			$status	 = 'OK';

			for ($i = 0; $i < sizeof($cabangList); $i++) {
				$cabangs[$i]['kode'] 	  = $cabangList[$i]->KodeCabang;
				$cabangs[$i]['nama'] 	  = firstCapital($cabangList[$i]->Nama);
				$cabangs[$i]['alamat'] 	  = firstCapital($cabangList[$i]->Alamat);
				$cabangs[$i]['kota'] 	  = firstCapital($cabangList[$i]->Kota);
				$cabangs[$i]['telpon'] 	  = $cabangList[$i]->Telp;
				$cabangs[$i]['fax'] 	  = $cabangList[$i]->Fax;
				$cabangs[$i]['flag_agen'] = (int) $cabangList[$i]->FlagAgen;
			}

			$results = array('cabang' => $cabangs);
		}

		$data['results'] = $results;
		$data['status']  = $status;

		$this->sendResponse($data);
	}

	public function sync()
	{
		$this->index();
	}
}
?>