<?php
/**
 * Kursi controller.
 *
 * Last update: June 29, 2012 16:26
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class KursiController extends Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Mendapatkan layout kursi.
	 *
	 * URL: GET /kursi/layout
	 *
	 * Paramaters:
	 * - kode = Kode layout, kode layout adalah nilai jumlah kursi yang diambil dari field 'JumlahKursi' dari tabel
	 * tbl_md_jadwal.
	 *
	 * Requires authentication: YES
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: GET
	 */
	public function layout()
	{
		global $cfg;

		$this->setRequestMethod('GET');
		//$this->authenticate(1);

		$kode		= $_GET['kode'];

		$data		= array();
		$results    = array();
		$status		= 'ZERO_RESULTS';

		if (!empty($cfg['seat'][$kode])) { //didefinisikan di config/config_var.php
			$status	 = 'OK';
			$results = $cfg['seat'][$kode];
		}

		$data['results']	= $results;
		$data['status'] 	= $status;

		$this->sendResponse($data);
	}
}
?>