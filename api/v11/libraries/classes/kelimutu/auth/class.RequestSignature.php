<?php
/**
 * Kelimutu API Framework
 *
 * Last updated: Feb 10, 2012, 12:21
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2012 Lorensius W. L. T
 */

/**
 * RequestSignature class
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2012 Lorensius W. L. T
 *
 */
class RequestSignature
{
    /**
     * Encode string
     *
     * @param string $s String to be encoded
     *
     * @return string Encoded string
     */
    public function urlencode($s)
    {
        if ($s === false) {
            return $s;
        } else {
            return str_replace('%7E', '~', rawurlencode($s));
        }
    }

    /**
     * Decode string
     *
     * @param string $s String to be decoded
     *
     * @return string Decoded string
     */
    public function urldecode ($s)
    {
        if ($s === false) {
            return $s;
        } else {
            return rawurldecode($s);
        }
    }

    /**
     * Create nonce string
     *
     * @param boolean $unique Unique
     *
     * @return string Nonce string
     */
    public function createNonce($unique = false)
    {
        $key = md5(uniqid(rand(), true));
        if ($unique)
        {
            list($usec,$sec) = explode(' ',microtime());
            $key .= dechex($usec).dechex($sec);
        }

        return $key;
    }

    /**
     * Normalize parameters, sort ascending by key name. If value is an array, it will be sorted by it's value.
     *
     * @param array $params Paramters
     *
     * @return array Normalized parameters
     */
    public function normalizeParams($params)
    {
        if (empty($params)) return;

        $normalized = array();

        ksort($params);
        foreach ($params as $name => $value) {
            if (is_array($value) && sizeof($value)) {
                sort($value);

                for ($i = 0; $i < sizeof($value); $i++) {
                    $normalized[] = "$name=" . $this->urlencode($value[$i]);
                }
            } else {
                $normalized[] = "$name=" . $this->urlencode($value);
            }
        }

        return implode("&", $normalized);
    }

    /**
     * Create signature base string
     *
     * @param string $method Http method (GET/POST)
     * @param string $url URL
     * @param array $params Request parameters
     *
     * @return string Signature base string
     */
    public function createSignatureBase($method, $url, $params)
    {
        $params  = (empty($params)) ? '' : $this->normalizeParams($params);
        $sigbase = array($method,
                         $this->urlencode($url),
                         $this->urlencode($params));

        return implode('&', $sigbase);
    }

    /**
     * Create signature string
     *
     * @param string $signatureBase Signature base string
     * @param string $clientId Client id
     * @param string $clientSecret Client secret
     *
     * @return string Signature string
     */
    public function createSignature($signatureBase, $clientId, $clientSecret )
    {
        $key = $this->urlencode($clientId) . '&' . $this->urlencode($clientSecret);

        if (function_exists('hash_hmac')) {
            $signature = base64_encode(hash_hmac("sha1", $signatureBase, $key, true));
        } else {
            $blocksize	= 64;
            $hashfunc	= 'sha1';

            if (strlen($key) > $blocksize) {
                $key = pack('H*', $hashfunc($key));
            }

            $key	= str_pad($key,$blocksize,chr(0x00));
            $ipad	= str_repeat(chr(0x36),$blocksize);
            $opad	= str_repeat(chr(0x5c),$blocksize);
            $hmac 	= pack(
                        'H*',$hashfunc(
                            ($key^$opad).pack(
                                'H*',$hashfunc(
                                    ($key^$ipad).$signatureBase
                                )
                            )
                        )
                    );

            $signature = base64_encode($hmac);
        }

        return $this->urlencode($signature);
    }
}
?>