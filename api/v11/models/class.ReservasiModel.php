<?php
/**
 * Reservasi model.
 *
 * Last updated: Feb 09, 2012 22:38
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class ReservasiModel extends Model
{

	public function __construct()
	{
		parent::__construct();

		$this->_table = 'tbl_reservasi';
	}

	/**
	 * Mendapatkan kursi yang telah dibooking.
	 *
	 * @param string $kode Kode jadwal
	 * @param string $tanggal Tanggal keberangkatan (yyyy-mm-dd)
	 *
	 * @return string Daftar kursi (comma separated, ex: 5,6,7)
	 */
	public function getPosisiKursi($kode, $tanggal)
	{
		global $cfg;

		$sql = "SELECT
						GROUP_CONCAT(CONVERT(NomorKursi, CHAR(2)) ORDER BY NomorKursi)  AS NomorKursi
				FROM
						tbl_posisi_detail
				WHERE
						KodeJadwal = f_jadwal_ambil_kodeutama_by_kodejadwal('$kode')
						AND
						TglBerangkat = '$tanggal'
						AND
						StatusKursi != 0
				GROUP BY
						KodeJadwal";

		$res = '';

		try {
			$this->_dbObj->query($sql);

			$row = $this->_dbObj->fetch();

			$res = $row->NomorKursi;
		} catch (DbException $e) {
			Error::store('Reservasi', $e->getMessage());
		}

		return $res;
	}

	public function getDetail($kode)
	{
		global $cfg;

		$sql = "SELECT
						*
				FROM
						tbl_reservasi
				WHERE
						KodeBooking = '$kode'";

		$res = array();

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) {
			Error::store('Reservasi', $e->getMessage());
		}

		return $res;
	}
}
?>