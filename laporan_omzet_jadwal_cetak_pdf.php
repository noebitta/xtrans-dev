<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$template->set_filenames(array('body' => 'laporan_omzet_jadwal/laporan_omzet_jadwal_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Cabang								= new Cabang();

if(in_array($userdata['user_level'],array($LEVEL_ADMIN))){
	$kondisi_cabang	= ($kode_cabang=="")?"":" AND KodeCabang='$kode_cabang'";
	$cabang_default	= "";
}
else{
	$kondisi_cabang	= " AND KodeCabang='$userdata[KodeCabang]'";
	$cabang_default	= $userdata['KodeCabang'];
}

//AMBIL DATA CABANG
//Cabang Asal
$data_cabang_asal		= $Cabang->ambilDataDetail($asal);
$data_cabang_tujuan	= $Cabang->ambilDataDetail($tujuan);

$keterangan_asal	= ($data_cabang_asal['Nama']=="")?"Semua Asal":"$data_cabang_asal[Nama] ($data_cabang_asal[KodeCabang]) $data_cabang_asal[Kota]"; 
$keterangan_tujuan= ($data_cabang_tujuan['Nama']=="")?"Semua Tujuan":"$data_cabang_tujuan[Nama] ($data_cabang_tujuan[KodeCabang]) $data_cabang_tujuan[Kota]"; 

if($asal!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'";
}

if($asal!="" && $tujuan!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'";
}



$sql=
	"SELECT 
		IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
		JamBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(IF(JenisPenumpang='U',1,0)),0) AS TotalPenumpangU,
		IS_NULL(SUM(IF(JenisPenumpang='M',1,0)),0) AS TotalPenumpangM,
		IS_NULL(SUM(IF(JenisPenumpang='K',1,0)),0) AS TotalPenumpangK,
		IS_NULL(SUM(IF(JenisPenumpang='KK',1,0)),0) AS TotalPenumpangKK,
		IS_NULL(SUM(IF(JenisPenumpang='G',1,0)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi_olap
	WHERE  (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_cabang
	GROUP BY JamBerangkat
	ORDER BY JamBerangkat ";

	
if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

//memasukkan ke array
$data_laporan	= array();

while($row = $db->sql_fetchrow($result)){
	$data_laporan[$row['JamBerangkat']]['JamBerangkat']			= $row['JamBerangkat'];
	$data_laporan[$row['JamBerangkat']]['TotalBerangkat']		= $row['TotalBerangkat'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangU']	= $row['TotalPenumpangU'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangM']	= $row['TotalPenumpangM'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangK']	= $row['TotalPenumpangK'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangKK']	= $row['TotalPenumpangKK'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpangG']	= $row['TotalPenumpangG'];
	$data_laporan[$row['JamBerangkat']]['TotalPenumpang']		= $row['TotalPenumpang'];
	$data_laporan[$row['JamBerangkat']]['TotalOmzet']				= $row['TotalOmzet'];
	$data_laporan[$row['JamBerangkat']]['TotalDiscount']		= $row['TotalDiscount'];
}

//jika ada dalam filter tanggal harus mengambil dari tbl_reservasi
//note: tiap hari transaksi di tbl_reservasi di backup ke tbl_reservasi_olap
if($tanggal_akhir_mysql>=date("Y-m-d")){
	$sql=
		"SELECT 
			IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
			JamBerangkat,
			IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
			IS_NULL(SUM(IF(JenisPenumpang='U',1,0)),0) AS TotalPenumpangU,
			IS_NULL(SUM(IF(JenisPenumpang='M',1,0)),0) AS TotalPenumpangM,
			IS_NULL(SUM(IF(JenisPenumpang='K',1,0)),0) AS TotalPenumpangK,
			IS_NULL(SUM(IF(JenisPenumpang='KK',1,0)),0) AS TotalPenumpangKK,
			IS_NULL(SUM(IF(JenisPenumpang='G',1,0)),0) AS TotalPenumpangG,
			IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
			IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
			IS_NULL(SUM(Discount),0) AS TotalDiscount
		FROM tbl_reservasi
		WHERE  (TglBerangkat BETWEEN '".date("Y-m-d")."' AND '$tanggal_akhir_mysql') 
			AND CetakTiket=1 AND FlagBatal!=1
			$kondisi_cabang
		GROUP BY JamBerangkat
		ORDER BY JamBerangkat ";
	
		
	if (!$result = $db->sql_query($sql)){
		//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
		echo("Err:".__LINE__);exit;
	}
	
	//memasukkan ke array
	while($row = $db->sql_fetchrow($result)){
		$data_laporan[$row['JamBerangkat']]['JamBerangkat']			= $row['JamBerangkat'];
		$data_laporan[$row['JamBerangkat']]['TotalBerangkat']		+= $row['TotalBerangkat'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangU']	+= $row['TotalPenumpangU'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangM']	+= $row['TotalPenumpangM'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangK']	+= $row['TotalPenumpangK'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangKK']	+= $row['TotalPenumpangKK'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpangG']	+= $row['TotalPenumpangG'];
		$data_laporan[$row['JamBerangkat']]['TotalPenumpang']		+= $row['TotalPenumpang'];
		$data_laporan[$row['JamBerangkat']]['TotalOmzet']				+= $row['TotalOmzet'];
		$data_laporan[$row['JamBerangkat']]['TotalDiscount']		+= $row['TotalDiscount'];
	}
}

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,30);
$pdf->Ln(20);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Per Jam periode '.$tanggal_mulai.' s/d '.$tanggal_akhir,'',0,'L');$pdf->Ln();
$pdf->SetFont('courier','B',12);
$pdf->Cell(40,8,'Asal:'.$keterangan_asal.' Tujuan:'.$keterangan_tujuan,'',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
#$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(20,5,'Jam','B',0,'C',1);
$pdf->Cell(20,5,'Tot.Brgkt','B',0,'C',1);
$pdf->Cell(20,5,'Pnp.U','B',0,'C',1);
$pdf->Cell(20,5,'Pnp.M','B',0,'C',1);
$pdf->Cell(20,5,'Pnp.K','B',0,'C',1);
$pdf->Cell(20,5,'Pnp.KK','B',0,'C',1);
$pdf->Cell(20,5,'Pnp.G','B',0,'C',1);
$pdf->Cell(30,5,'Tot.Pnp.','B',0,'C',1);
$pdf->Cell(20,5,'Rt2 Pnp/Brgkt.','B',0,'C',1);
$pdf->Cell(30,5,'Omzet.','B',0,'C',1);
$pdf->Cell(30,5,'Disc.','B',0,'C',1);
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

$sum_berangkat			= 0;
$sum_penumpang_u		= 0;
$sum_penumpang_m		= 0;
$sum_penumpang_k		= 0;
$sum_penumpang_kk		= 0;
$sum_penumpang_g		= 0;
$sum_penumpang			= 0;
$sum_omzet					= 0;
$sum_discount				= 0;

foreach($data_laporan as $row_laporan){
	
	$total_berangkat		= $row_laporan['TotalBerangkat']; 
	$total_penumpang_u	= $row_laporan['TotalPenumpangU']; 
	$total_penumpang_m	= $row_laporan['TotalPenumpangM']; 
	$total_penumpang_k	= $row_laporan['TotalPenumpangK']; 
	$total_penumpang_kk	= $row_laporan['TotalPenumpangKK']; 
	$total_penumpang_g	= $row_laporan['TotalPenumpangG']; 
	$total_penumpang		= $row_laporan['TotalPenumpang']; 
	$total_omzet				= $row_laporan['TotalOmzet']; 
	$total_discount			= $row_laporan['TotalDiscount']; 
	
	$rata_penumpang_per_trip	=($total_berangkat>0)?$total_penumpang/$total_berangkat:0;
	
	$pdf->Cell(20,5,$row_laporan['JamBerangkat'].'  ','',0,'L');
	$pdf->Cell(20,5,number_format($total_berangkat,0,",","."),'',0,'R');
	$pdf->Cell(20,5,number_format($total_penumpang_u,0,",","."),'',0,'R');
	$pdf->Cell(20,5,number_format($total_penumpang_m,0,",","."),'',0,'R');
	$pdf->Cell(20,5,number_format($total_penumpang_k,0,",","."),'',0,'R');
	$pdf->Cell(20,5,number_format($total_penumpang_kk,0,",","."),'',0,'R');
	$pdf->Cell(20,5,number_format($total_penumpang_g,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_penumpang,0,",","."),'',0,'R');
	$pdf->Cell(20,5,number_format($rata_penumpang_per_trip,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_omzet,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_discount,0,",","."),'',0,'R');
	$pdf->Ln();
	$pdf->Cell(250,1,'','B',0,'');
	$pdf->Ln();
	
	$sum_berangkat			+= $total_berangkat;
	$sum_penumpang_u		+= $total_penumpang_u;
	$sum_penumpang_m		+= $total_penumpang_m;
	$sum_penumpang_k		+= $total_penumpang_k;
	$sum_penumpang_kk		+= $total_penumpang_kk;
	$sum_penumpang_g		+= $total_penumpang_g;
	$sum_penumpang			+= $total_penumpang;
	$sum_omzet					+= $total_omzet;
	$sum_discount				+= $total_discount;

}

$rata_penumpang_per_trip	=($sum_penumpang>0)?$sum_penumpang/$sum_berangkat:0;

//FOOTER
$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
#$pdf->Cell(5,5,'','B',0,'C',1);
$pdf->Cell(20,5,'TOTAL','B',0,'C',1);
$pdf->Cell(20,5,number_format($sum_berangkat,0,",","."),'',0,'R',1);
$pdf->Cell(20,5,number_format($sum_penumpang_u,0,",","."),'',0,'R',1);
$pdf->Cell(20,5,number_format($sum_penumpang_m,0,",","."),'',0,'R',1);
$pdf->Cell(20,5,number_format($sum_penumpang_k,0,",","."),'',0,'R',1);
$pdf->Cell(20,5,number_format($sum_penumpang_kk,0,",","."),'',0,'R',1);
$pdf->Cell(20,5,number_format($sum_penumpang_g,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_penumpang,0,",","."),'',0,'R',1);
$pdf->Cell(20,5,number_format($rata_penumpang_per_trip,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_omzet,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_discount,0,",","."),'',0,'R',1);
$pdf->Ln();
$pdf->Ln();
									
$pdf->Output();
						
?>