<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


	if($mode=="ubah_status_online"){
		$list_user = str_replace("\'","'",$HTTP_GET_VARS['list_user']);
		
		$sqla = 
			"UPDATE tbl_user SET 
				status_online =1-status_online
			WHERE user_id IN($list_user);";
		
		if (!$resulta = $db->sql_query($sqla)){
			die_error('Err:'.__LINE__);//,__FILE__,__LINE__,$sqla);
		}
		
		$sqla = 
			"DELETE FROM tbl_sessions 
			WHERE session_user_id IN ($list_user)";
		
		if (!$resulta = $db->sql_query($sqla)){
			die_error('Err:'.__LINE__);//,__FILE__,__LINE__,$sqla);
		}
		
		exit;
	}
	else{
		// LIST
		$template->set_filenames(array('body' => 'user/user_list_login_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$temp_cari=str_replace("cabang=","",$cari);
		if($temp_cari==$cari){
			$kondisi_cabang = 
				"nrp LIKE '%$cari%' 
				OR nama LIKE '%$cari%' 
				OR username LIKE '%$cari%'
				OR telp LIKE '%$cari%'
				OR hp LIKE '%$cari%'
				OR email LIKE '%$cari%'
				OR address LIKE '%$cari%'";
		}
		else{
			$kondisi_cabang = "dbo.f_cabang_get_name_by_kode(KodeCabang) LIKE '%$temp_cari%' ";
			$cari=$temp_cari;
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE  $kondisi_cabang";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"user_id","tbl_user","",$kondisi,"pengaturan_user_list_login.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT 
				user_id, username, user_level, nama,f_cabang_get_name_by_kode(KodeCabang) as NamaCabang,telp,hp,
				status_online,waktu_login,waktu_logout,NRP
			FROM tbl_user $kondisi 
			ORDER BY status_online DESC,username LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['status_online']){
					$status="<a href='' onClick='return ubahStatus(\"$row[0]\")'>On Line</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[0]\")'>OFF</a>";
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama'=>$row['nama'],
							'nrp'=>$row['NRP'],
							'username'=>$row['username'],
							'cabang'=>$row['NamaCabang'],
							'user_level'=>$USER_LEVEL[$row['user_level']],
							'telp_hp'=>$row['telp']."/".$row['hp'],
							'login'=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['waktu_login'])),
							'logout'=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['waktu_logout'])),
							'status'=>$status
						)
					);
				
				$i++;
		  }
		} 
		else{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Err:".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_user_list_login.'.$phpEx).'">List User</a>',
			'U_USER_ADD'		=> append_sid('pengaturan_user.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_user_list_login.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'PAGING'				=> $paging
			)
		);
	}

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>