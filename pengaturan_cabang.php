<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Cabang	= new Cabang();

function setComboTipeCabang($dipilih){
	
	$temp_var		= "select_".$dipilih;
	
	$$temp_var	= "selected";
	
	$opt_tipe_cabang	= "<option $select_0 value=0>CABANG</option>
											 <option $select_1 value=1>AGEN</option>";
											 
	return $opt_tipe_cabang;
}

	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'cabang/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=add").'">Tambah Cabang</a> ',
		 'JUDUL'		=>'Tambah Data Cabang',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '0',
		 'OPT_KOTA' => setComboKota("BANDUNG"),
		 'OPT_TIPE_CABANG' => setComboTipeCabang(0),
		 'PESAN'						=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_CABANG_ADD_ACT'	=> append_sid('pengaturan_cabang.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah cabang
		$kode  		= str_replace(" ","",$HTTP_POST_VARS['kode']);
		$kode_old	= str_replace(" ","",$HTTP_POST_VARS['kode_old']);
		$nama   	= $HTTP_POST_VARS['nama'];
		$alamat     = $HTTP_POST_VARS['alamat'];
		$kota		= $HTTP_POST_VARS['kota'];
		$telp   	= $HTTP_POST_VARS['telp'];
		$fax		= $HTTP_POST_VARS['fax'];
		$flag_agen  = $HTTP_POST_VARS['tipe_cabang'];
		$saldo      = $HTTP_POST_VARS['saldo'];
		$terjadi_error=false;
		
		if($Cabang->periksaDuplikasi($kode) && $kode!=$kode_old){
			$pesan="<font color='white' size=3>Kode cabang yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Cabang";
				$path	='<a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=add").'">Tambah Cabang</a> ';
				
				if($Cabang->tambah($kode,$nama,$alamat,$kota,$telp,$fax,$flag_agen,$saldo)){


					redirect(append_sid('pengaturan_cabang.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data cabang Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_cabang.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
				}
			}
			else{
				
				$judul="Ubah Data Cabang";
				$path	='<a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=edit&id=$kode_old").'">Ubah Cabang</a> ';
				
				if($Cabang->ubah($kode_old,$kode,$nama,$alamat,$kota,$telp,$fax,$flag_agen,$saldo)){
						
						//redirect(append_sid('pengaturan_cabang.'.$phpEx.'?mode=add',true));
						//die_message('<h2>Data cabang Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_cabang.'.$phpEx.'?mode=edit&id='.$kode).'">Sini</a> Untuk Melanjutkan','');
						$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
						$bgcolor_pesan="98e46f";
				}
			}
			
			//exit;
			
		}
		
		$template->set_filenames(array('body' => 'cabang/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		    => '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a> | '.$path,
		 'JUDUL'		    => $judul,
		 'MODE'   	        => 'save',
		 'SUB'    	        => $submode,
		 'KODE_OLD'         => $kode_old,
		 'KODE'    	        => $kode,
		 'NAMA'    	        => $nama,
		 'ALAMAT'           => $alamat,
		 'OPT_KOTA'	        => setComboKota($kota),
		 'TELP'			    => $telp,
		 'FAX'			    => $fax,
         'SALDO'            => $saldo,
		 'OPT_TIPE_CABANG'  => setComboTipeCabang($flag_agen),
		 'PESAN'		    => $pesan,
		 'BGCOLOR_PESAN'    => $bgcolor_pesan,
		 'U_CABANG_ADD_ACT' => append_sid('pengaturan_cabang.'.$phpEx)
		 )
		);
		
		
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Cabang->ambilDataDetail($id);
		
		$template->set_filenames(array('body' => 'cabang/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		    => '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=edit&id=$id").'">Ubah Cabang</a> ',
			 'JUDUL'		    => 'Ubah Data Cabang',
			 'MODE'   	        => 'save',
			 'SUB'    	        => '1',
			 'KODE_OLD'	        => $row['KodeCabang'],
			 'KODE'    	        => $row['KodeCabang'],
			 'NAMA'    	        => $row['Nama'],
			 'ALAMAT'           => $row['Alamat'],
			 'OPT_KOTA'	        => setComboKota($row['Kota']),
			 'TELP'			    => $row['Telp'],
			 'FAX'			    => $row['Fax'],
             'SALDO'            => $row['SaldoDropCash'],
			 'OPT_TIPE_CABANG'  => setComboTipeCabang($row['FlagAgen']),
			 'BGCOLOR_PESAN'    => $bgcolor_pesan,
			 'U_CABANG_ADD_ACT' => append_sid('pengaturan_cabang.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus cabang
		$list_cabang = str_replace("\'","'",$HTTP_GET_VARS['list_cabang']);

		$Cabang->hapus($list_cabang);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'cabang/cabang_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE KodeCabang LIKE '%$cari%' 
				OR Nama LIKE '%$cari%' 
				OR Alamat LIKE '%$cari%' 
				OR Kota LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR Fax LIKE '%$cari%'";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"KodeCabang","tbl_md_cabang","",$kondisi,"pengaturan_cabang.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT KodeCabang,Nama,Alamat,Kota,Telp,Fax,FlagAgen
			FROM tbl_md_cabang $kondisi 
			ORDER BY KodeCabang,Nama LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[KodeCabang]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_cabang.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'		=>$odd,
							'check'	=>$check,
							'no'		=>$i,
							'kode'	=>$row['KodeCabang'],
							'nama'	=>$row['Nama'],
							'alamat'=>$row['Alamat'],
							'kota'	=>$row['Kota'],
							'telp'	=>$row['Telp'],
							'fax'		=>$row['Fax'],
							'tipe_cabang'=>($row['FlagAgen']==0)?"CABANG":"AGEN",
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load cabang',__FILE__,__LINE__,$sql);
			echo("Err :".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a>',
			'U_CABANG_ADD'		=> append_sid('pengaturan_cabang.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_cabang.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'			=> $no_data,
			'PAGING'			=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>