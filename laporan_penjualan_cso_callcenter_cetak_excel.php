<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$cari  					= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$cabang  				= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$tgl_awal  			= isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
$tgl_akhir 			= isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$sort_by				= isset($HTTP_GET_VARS['sortby'])? $HTTP_GET_VARS['sortby'] : $HTTP_POST_VARS['sortby'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//MENGAMBIL PENGATURAN CABANG CALL CENTER
$sql = 
	"SELECT *
	FROM tbl_pengaturan_parameter
	WHERE NamaParameter LIKE 'CALLCENTER_%'
	ORDER BY NamaParameter;";
				
if(!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}
		
$data_cc	= array();
		
while ($row=$db->sql_fetchrow($result)){
	$field_name	= $row['NilaiParameter'];
	$data_cc[$field_name]	= $row['Deskripsi'];
	$cabangs_callcenter	.= "'$row[NilaiParameter]',";
}

$cabangs_callcenter	= substr($cabangs_callcenter,0,-1);

$kondisi_cabang	= $cabang==""?"":" AND CabangPesan='$cabang'";

$kondisi=
	"(DATE(WaktuPesan) BETWEEN '$tgl_awal' AND '$tgl_akhir')
		AND CabangPesan IN($cabangs_callcenter)
		$kondisi_cabang
		AND (username='$cari'
	  OR tu.nama LIKE '%$cari%' 
		OR tu.telp LIKE '%$cari%'
		OR NRP LIKE '%$cari%')";

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"NamaCSO":$sort_by;
			
$sql	= 
	"SELECT 
		tu.nama AS NamaCSO,NRP,
		GROUP_CONCAT(DISTINCT CabangPesan ORDER BY CabangPesan SEPARATOR ',') AS CabangTugas,
		PetugasPenjual,
		IS_NULL(COUNT(NoTiket),0) AS Tiket,
		IS_NULL(COUNT(DISTINCT(KodeBooking)),0) AS Booking,
		MIN(WaktuPesan) AS BookTerawal,
		MAX(WaktuPesan) AS BookTerakhir
	FROM tbl_reservasi_olap tr INNER JOIN tbl_user tu ON tr.PetugasPenjual=tu.user_id
	WHERE $kondisi
	GROUP BY PetugasPenjual
	ORDER BY $sort_by $order";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}


//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Penjualan CSO Call Center Periode '.dateparse(FormatMySQLDateToTgl($tgl_awal)).' s/d '.dateparse(FormatMySQLDateToTgl($tgl_akhir)));
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'NRP');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Call Center');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Booking');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Booking Terawal');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Booking Terakhir');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);


$idx=0;
$idx_row=4;
while($row=$db->sql_fetchrow($result)){
			
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NamaCSO']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, "'".$row['NRP']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['CabangTugas']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['Booking']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Tiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['BookTerawal'])));
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['BookTerakhir'])));
	
	$idx_row++;
	$idx++;
}
$temp_idx=$idx_row-1;

//$idx_row++;		
//debug

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':D'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
//echo("YES". __LINE__);exit;
if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Penjualan CSO Call Center Periode '.dateparse(FormatMySQLDateToTgl($tgl_awal)).' s/d '.dateparse(FormatMySQLDateToTgl($tgl_akhir)).'.xls"');
  header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');
	$phpExcel->disconnectWorksheets();
  unset($phpExcel);
}
  
  
?>
