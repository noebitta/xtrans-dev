<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN,$LEVEL_SUPERVISOR))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= $HTTP_GET_VARS['is_today'];
$tanggal_mulai  = $HTTP_GET_VARS['tglmulai'];
$tanggal_akhir  = $HTTP_GET_VARS['tglakhir'];
$kode_cabang  	= $HTTP_GET_VARS['kode'];
$nama_cabang  	= $HTTP_GET_VARS['namacabang'];

// LIST
$template->set_filenames(array('body' => 'laporan_uang_cabang/detail_cso.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";
		
$sql	= 
	"SELECT 
		f_user_get_nama_by_userid(PetugasCetakTiket) AS NamaCSO,
		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalTunai,
		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalDebit,
		IS_NULL(SUM(IF(JenisPembayaran=2,Total,0)),0) AS TotalKredit,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),HargaTiket)),0) AS TotalSetor
	FROM $tbl_reservasi
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang'
	GROUP BY PetugasCetakTiket ORDER BY NamaCSO";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	
	$odd = $idx%2==0?"even":"odd";
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'							=>$odd,
				'no'							=>$idx,
				'nama'						=>$row['NamaCSO'],
				'kode_cabang'			=>$row['KodeCabang'],
				'total_tunai'			=>number_format($row['TotalTunai'],0,",","."),
				'total_debit'			=>number_format($row['TotalDebit'],0,",","."),
				'total_kredit'		=>number_format($row['TotalKredit'],0,",","."),
				'total_penumpang'	=>number_format($row['TotalTiket'],0,",","."),
				'total_setoran'		=>number_format($row['TotalTunai']+$row['TotalDebit']+$row['TotalKredit'],0,",","."),
			)
		);
}


//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&is_today=$is_today&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$temp_array[$idx]['KodeCabang'].
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
											
$script_cetak_excel="Start('laporan_uang_cabang_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "is_today".($is_today==""?"1":$is_today);
$$temp_var	= "selected";

$template->assign_vars(array(
	'CABANG'				=> strtoupper($nama_cabang),
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	)
);

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>