<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Sopir{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Sopir(){
		$this->ID_FILE="C-SPR";
	}
	
	//BODY
	
	function periksaDuplikasi($kode){
		
		/*
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT f_sopir_periksa_duplikasi('$kode') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($kode_sopir,$nama,$hp,$alamat,$no_sim,$flag_aktif){
	  
		/*
		IS	: data sopir belum ada dalam database
		FS	:Data sopir baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_sopir_tambah('$kode_sopir','$nama','$hp','$alamat','$no_sim',$flag_aktif)";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah($kode_sopir_old,$kode_sopir,$nama,$hp,$alamat,$no_sim,$flag_aktif){
	  
		/*
		IS	: data kendaraan sudah ada dalam database
		FS	:Data kendaraan diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_sopir_ubah('$kode_sopir_old','$kode_sopir','$nama','$hp','$alamat','$no_sim',$flag_aktif)";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

    //MENGUBAH KODE SOPIR JIKA BERUBAH KE TABEL-TABEL LAIN
    if($kode_sopir_old!=$kode_sopir){
      $dependen_tables = array(
        "tbl_biaya_op"              =>"KodeSopir",
        "tbl_log_cetak_manifest"    =>"KodeDriver",
        "tbl_md_kendaraan"          =>"KodeSopir1",
        "tbl_md_kendaraan"          =>"KodeSopir2",
        "tbl_paket"                 =>"KodeSopir",
        "tbl_penjadwalan_kendaraan" =>"KodeDriver",
        "tbl_posisi"                =>"KodeSopir",
        "tbl_posisi_backup"         =>"KodeSopir",
        "tbl_reservasi"             =>"KodeSopir",
        "tbl_reservasi_olap"        =>"KodeSopir",
        "tbl_spj"                   =>"KodeDriver"
        );

      foreach($dependen_tables as $table => $field){
        $sql= "UPDATE $table SET $field='$kode_sopir' WHERE $field='$kode_sopir_old'";

        if (!$db->sql_query($sql)){
          die_error("Err: $this->ID_FILE".__LINE__);
        }
      }

    }

		return true;
	}
	
	function hapus($list_sopir){
	  
		/*
		IS	: data sopir sudah ada dalam database
		FS	:Data sopir dihapus
		*/
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA
		$sql =
			"DELETE FROM tbl_md_sopir
			WHERE KodeSopir IN($list_sopir);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ubahStatusAktif($kode_sopir){
	  
		/*
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql ="CALL sp_sopir_ubah_status_aktif('$kode_sopir');";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
	
	function ambilDataDetail($kode_sopir){
		
		/*
		Desc	:Mengembalikan data sopir sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_sopir
			WHERE KodeSopir='$kode_sopir';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		Desc	:Mengembalikan data sopir sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_md_sopir
			WHERE 
				(KodeSopir LIKE '$pencari' 
				OR Nama LIKE '$pencari' 
				OR HP LIKE '%$pencari%' 
				OR Alamat LIKE '%$pencari%'
				OR NoSIM LIKE '$pencari') 
				AND FlagAktif=1
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
}
?>