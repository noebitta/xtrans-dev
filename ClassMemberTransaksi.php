<?php
class TransaksiMember{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1,$TABEL2,$TABEL3,$TABEL4,$TBEL5;
	
	//CONSTRUCTOR
	function TransaksiMember(){
		$this->ID_FILE="C-003";
		$this->TABEL1="tbl_member_master";
		$this->TABEL2="tbl_member_topup";
		$this->TABEL3="tbl_member_mutasi_account";
		$this->TABEL4="tbl_member_mutasi_point";
		$this->TABEL5="TBMDJadwal";
	}
	
	//BODY
	
	function kodeValidasi($id_member,$no_ktp,$deposit,$point){
		return md5($deposit.$point.$id_member.$no_ktp);
	}
	
	function ambilDeposit($id_member){
		
		/*
		ID	: 001
		Desc	:Mengembalikan nilai deposit
		*/
		
		//kamus
		global $db;
		
		$id_member	=trim($id_member);
		
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 001");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)==$kode_validasi){
			return $deposit;
		}
		else{
			return "false";
		}
		
	}//  END ambilDeposit
	
	function ambilPoint($id_member){
		
		/*
		ID	: 002
		Desc	:Mengembalikan nilai point 
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 002");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)==$kode_validasi){
			return $point;
		}
		else{
			return "false";
		}
		
	}//  END ambilPoint
	
	function ambilSaldoMutasiTerakhir($id_member){
		
		/*
		ID	: 003
		Desc	:Mengembalikan nilai point 
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT TOP 1 saldo
			FROM $this->TABEL3
			WHERE 
				id_member='$id_member'
			ORDER BY waktu_transaksi DESC";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$saldo 	= $row['saldo'];
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 003");
		}
		
		$saldo	= ($saldo!='')?$saldo:0;
		
		return $saldo;
				
	}//  END ambilSaldoMutasiTerakhir
	
	function topupDeposit($id_member,$jumlah_topup){
	  
		$id_member=trim($id_member);
		
		/*
		ID	: 004
		IS	: data member sudah ada dalam database
		FS	: deposit member bertambah
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		$useraktif	= $userdata['username'];
		
		//MEMERIKSA VALIDASI DATA
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 004-01");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)!=$kode_validasi){
			//jika kode validasi tidak sama, berarti data tidak valid
			return "false";
		}
		
		//mengenerate kode validasi baru setelah ditambahkan nilai top up
		$deposit	=	$deposit+$jumlah_topup;
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$deposit,$point);
		
		//MENAMBAHKAN TOP UP MEMBER
		$sql =
			"UPDATE $this->TABEL1
			SET saldo=$deposit,kode_validasi='$kode_validasi'
			WHERE id_member='$id_member';";
		
		//MENYMPAN TRANSAKSI KEDALAM HISTORY TOP UP
		//mengenerate id top up
		$nourut = rand(1000,9999);
		$id_topup = "TU".dateYMD().$nourut;
		
		$sql .=
			"INSERT INTO $this->TABEL2
				(id_topup,id_member,waktu_topup,jumlah_topup, 
				masa_aktif,operator,kode_validasi)
			VALUES(
				'$id_topup','$id_member',{fn NOW()},$jumlah_topup,
				NULL,'$useraktif','$kode_validasi');";
		
		//MENYMPAN TRANSAKSI KEDALAM MUTASI ACCOUNT
		//mengenerate id top up
		$nourut 				= rand(10000,99999);
		$id_mutasi 			= "MTTU".dateYMD().$nourut;
		$saldo_terakhir	= $this->ambilSaldoMutasiTerakhir($id_member);
		$saldo					= $saldo_terakhir+$jumlah_topup;
		$sql .=
			"INSERT INTO $this->TABEL3
				(id_mutasi,waktu_transaksi,id_member,referensi,
				keterangan,jenis_mutasi,jumlah_mutasi,saldo,
				operator,kode_validasi)
			VALUES(
				'$id_mutasi',{fn NOW()},'$id_member','$id_topup',
				'TOP UP Member ID Member $id_member',0,$jumlah_topup,$saldo,
				'$useraktif','$kode_validasi');";
		
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 004-02 $sql");
		}
		
		return $id_topup;
	}
	
	function ambilDataDetailTopup($id_topup){
		
		/*
		ID	:005
		Desc	:Mengembalikan data topup
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				id_topup,id_member,CONVERT(CHAR(25),waktu_topup,103) as waktu_topup,jumlah_topup,
				masa_aktif,operator
			FROM $this->TABEL2
			WHERE id_topup='$id_topup';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Gagal $this->ID_FILE 005");
		}
		
	}//  END ambilDataDetailTopup
	
	function mutasiPembelianTiket($id_member,$kode_booking,$keterangan,$jumlah_transaksi,$jumlah_point){
	  
		$id_member=trim($id_member);
		
		/*
		ID	: 006
		IS	: data member sudah ada dalam database
		FS	: deposit member berkurang
		*/
		
		//kamus
		global $userdata;
		global $db;
		
		$useraktif	= $userdata['username'];
		
		//MEMERIKSA VALIDASI DATA
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 006-01");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)!=$kode_validasi){
			//jika kode validasi tidak sama, berarti data tidak valid
			return "false";
		}
		
		//mengenerate kode validasi baru setelah melakukan transaksi
		$deposit	=	$deposit-$jumlah_transaksi;
		$point		= $point+$jumlah_point;
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$deposit,$point);
		
		//MENGUPDATE SALDO DAN POINT
		$sql =
			"UPDATE $this->TABEL1
			SET saldo=$deposit,point=$point,kode_validasi='$kode_validasi'
			WHERE id_member='$id_member';";
				
		//MENYMPAN TRANSAKSI KEDALAM MUTASI ACCOUNT
		//mengenerate id top up
		$nourut 				= rand(10000,99999);
		$id_mutasi 			= "MTTX".dateYMD().$nourut;
		$id_mutasi_point= "TUPT".dateYMD().$nourut;
		$saldo_terakhir	= $this->ambilSaldoMutasiTerakhir($id_member);
		$saldo					= $saldo_terakhir-$jumlah_transaksi;
		
		$sql .=
			"INSERT INTO $this->TABEL3
				(id_mutasi,waktu_transaksi,id_member,referensi,
				keterangan,jenis_mutasi,jumlah_mutasi,saldo,
				operator,kode_validasi)
			VALUES(
				'$id_mutasi',{fn NOW()},'$id_member','$kode_booking',
				'$keterangan',1,$jumlah_transaksi,$saldo,
				'$useraktif','$kode_validasi');";
				
		if($jumlah_point>0){
			$sql .=
				"INSERT INTO $this->TABEL4
					(id_mutasi_point,waktu_transaksi,id_member,referensi,
					keterangan,jenis_mutasi,jumlah_mutasi_point,
					operator,kode_validasi)
				VALUES(
					'$id_mutasi_point',{fn NOW()},'$id_member','$kode_booking',
					'$keterangan',0,$jumlah_point,
					'$useraktif','$kode_validasi');";
		}
		
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 006-02");
		}
	}
	
	function ambilGroupMutasiByMember($tgl_mula,$tgl_akhir,$kategori_member,$cari,$order_by,$asc){
		
		/*
		ID	: 007
		Desc	:Mengembalikan data mutasi
		*/
		
		//kamus
		global $db;
		
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":"ORDER BY T1.id_member";
		
		$sql = 
			"SELECT
				T1.id_member, T1.nama, T1.alamat, T1.kota, 
				ISNULL(SUM(T3.jumlah_mutasi * (1 - T3.jenis_mutasi)), 0) AS jum_mutasi_topup, 
				ISNULL(SUM(T3.jumlah_mutasi * T3.jenis_mutasi), 0) AS jum_mutasi_transaksi,
				T1.saldo,T1.point,T1.kategori_member
			FROM $this->TABEL1 T1 LEFT OUTER JOIN
					$this->TABEL3 T3 ON T1.id_member = T3.id_member 
					AND (T3.waktu_transaksi BETWEEN CONVERT(DATETIME,'$tgl_mula 00:00:00',105) AND CONVERT(DATETIME,'$tgl_akhir 23:59:59',105))
			WHERE 
				(T1.id_member LIKE '$cari%' OR T1.nama LIKE '$cari%' 
				OR T1.alamat LIKE '$cari%' OR T1.kota LIKE '$cari%')
				AND status_member=1 AND kategori_member LIKE '$kategori_member%'
			GROUP BY T1.id_member, T1.nama, T1.alamat, T1.kota, T1.saldo, T1.point, T1.kategori_member
			$order";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
				
	}//  END ambilGroupMutasiByMember
	
	function ambilMutasiByMember($tgl_mula,$tgl_akhir,$id_member){
		
		/*
		ID	: 008
		Desc	:Mengembalikan data mutasi
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT
				id_mutasi,CONVERT(CHAR(17),waktu_transaksi,113)as waktu_transaksi_con,id_member,referensi,keterangan,
				(jumlah_mutasi * (1 - jenis_mutasi)) AS jum_mutasi_topup, 
				(jumlah_mutasi * jenis_mutasi) AS jum_mutasi_transaksi,flag_batal,
				saldo,operator
			FROM $this->TABEL3
			WHERE 
				id_member LIKE '$id_member%' AND (waktu_transaksi >= CONVERT(DATETIME,'$tgl_mula 00:00:00',105) AND waktu_transaksi <= CONVERT(DATETIME,'$tgl_akhir 23:59:59',105))
			ORDER BY waktu_transaksi;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 008 ".$sql);
		}
				
	}//  END ambilMutasiByMember
	
	function pendaftaranMember(
		$id_member, $nama, $jenis_kelamin, $tempat_lahir, 
		$tgl_lahir, $no_ktp, $tgl_registrasi, $alamat, 
		$kota, $kode_pos, $telp_rumah, $handphone,$email,$kategori_member,$password){
	  
		/*
		ID	: 009
		IS	: data member sudah ada dalam database
		FS	:Data member diubah 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		global $TOP_UP_AWAL;
		global $BIAYA_PENDAFTARAN;
		
		$id_member	=	trim($id_member);
		$no_ktp			=trim($no_ktp);
		
		$useraktif	= $userdata['username'];
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,0,0);
		$password				= md5($password);
		$sql =
			"UPDATE $this->TABEL1
			SET 
				nama='$nama',jenis_kelamin='$jenis_kelamin',tempat_lahir='$tempat_lahir', 
				tgl_lahir=CONVERT(DATETIME,'$tgl_lahir',104),no_ktp='$no_ktp',alamat='$alamat',
				tgl_registrasi=CONVERT(DATETIME,'$tgl_registrasi',104),
				kota='$kota',kode_pos='$kode_pos',telp_rumah='$telp_rumah',handphone='$handphone',
				email='$email',diubah_oleh='$useraktif',tgl_diubah={fn NOW()},status_member=1,
				saldo=0,point=0,kode_validasi='$kode_validasi',kategori_member='$kategori_member',
				kata_sandi='$password'
			WHERE id_member='$id_member';";
						
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 009");
		}
		
		//TOP UP DEPOSIT AWAL
		//$this->topupDeposit($id_member,$TOP_UP_AWAL);
		
		//MEMOTONG BIAYA PENDAFTARAN
		//$this->mutasiPembelianTiket($id_member,'-','Biaya pendaftaran',$BIAYA_PENDAFTARAN,0);
		
		return true;
	}
	
	function koreksiDeposit($id_member,$jumlah_saldo_baru){
	  
		$id_member=trim($id_member);
		
		/*
		ID	: 010
		IS	: data transaksi sudah ada dalam database
		FS	: deposit member diubah
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		$useraktif	= $userdata['username'];
		
		//MEMERIKSA VALIDASI DATA
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi,nama,telp_rumah,handphone
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$nama		 				= trim($row['nama']);
				$telp_rumah 		= trim($row['telp_rumah']);
				$handphone 			= trim($row['handphone']);
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 010-01");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)!=$kode_validasi){
			//jika kode validasi tidak sama, berarti data tidak valid
			return "false";
		}
		
		$deposit_sekarang			= $this->ambilDeposit($id_member);
		$deposit_penyesuaian	= $jumlah_saldo_baru;
		$selisih_deposit			= $deposit_penyesuaian-$deposit_sekarang;
		
		//mengenerate kode validasi baru setelah ditambahkan nilai koreksi
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$deposit_penyesuaian,$point);
		
		//Mengubah deposit
		$sql =
			"UPDATE $this->TABEL1
			SET saldo=$deposit_penyesuaian,kode_validasi='$kode_validasi'
			WHERE id_member='$id_member';";
		
		//MENYMPAN TRANSAKSI KEDALAM HISTORY koreksi saldo
		//mengenerate id top up
		$nourut = rand(1000,9999);
		$id_koreksi = "KOR".dateYMD().$nourut;
		
		$sql .=
			"INSERT INTO tbl_member_log_koreksi_saldo
				(id_koreksi,waktu_koreksi,id_member,nama_member, 
				telp_member,hp_member,keterangan,jumlah_koreksi,
				operator)
			VALUES(
				'$id_koreksi',{fn NOW()},'$id_member','$nama',
				'$telp_rumah','$handphone','Koreksi by admin',$deposit_penyesuaian,
				'$useraktif');";
		
		if($selisih_deposit>=0){
			$jenis_mutasi=0;
		}
		else{
			$jenis_mutasi=1;
		}
		
		//MENYMPAN TRANSAKSI KEDALAM MUTASI ACCOUNT
		//mengenerate id top up
		$nourut 				= rand(10000,99999);
		$id_mutasi 			= "MTKR".dateYMD().$nourut;
		$saldo					= $deposit_penyesuaian;
		$sql .=
			"INSERT INTO $this->TABEL3
				(id_mutasi,waktu_transaksi,id_member,referensi,
				keterangan,jenis_mutasi,jumlah_mutasi,saldo,
				operator,kode_validasi)
			VALUES(
				'$id_mutasi',{fn NOW()},'$id_member','$id_koreksi',
				'Koreksi saldo by admin id koreksi $id_koreksi',$jenis_mutasi,".abs($selisih_deposit).",$saldo,
				'$useraktif','$kode_validasi');";
		
		
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 010-02");
		}
		
		return $sql;
	}
	
	function koreksiPoint($id_member,$jumlah_poin_baru){
	  
		$id_member=trim($id_member);
		
		/*
		ID	: 011
		IS	: data transaksi sudah ada dalam database
		FS	: poin member diubah
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		$useraktif	= $userdata['username'];
		
		//MEMERIKSA VALIDASI DATA
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi,nama,telp_rumah,handphone
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$nama		 				= trim($row['nama']);
				$telp_rumah 		= trim($row['telp_rumah']);
				$handphone 			= trim($row['handphone']);
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 011-01");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)!=$kode_validasi){
			//jika kode validasi tidak sama, berarti data tidak valid
			return "false";
		}
		
		$point_sekarang			= $this->ambilPoint($id_member);
		$point_penyesuaian	= $jumlah_poin_baru;
		$selisih_point			= $point_penyesuaian-$point_sekarang;
		
		//mengenerate kode validasi baru setelah ditambahkan nilai koreksi
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$deposit,$point_penyesuaian);
		
		//Mengubah deposit
		$sql =
			"UPDATE $this->TABEL1
			SET point=$point_penyesuaian,kode_validasi='$kode_validasi'
			WHERE id_member='$id_member';";
		
		//MENYMPAN TRANSAKSI KEDALAM HISTORY koreksi saldo
		//mengenerate id top up
		$nourut = rand(1000,9999);
		$id_koreksi = "KORP".dateYMD().$nourut;
		
		$sql .=
			"INSERT INTO tbl_member_log_koreksi_point
				(id_koreksi,waktu_koreksi,id_member,nama_member, 
				telp_member,hp_member,keterangan,jumlah_koreksi,
				operator)
			VALUES(
				'$id_koreksi',{fn NOW()},'$id_member','$nama',
				'$telp_rumah','$handphone','Koreksi by admin',$point_penyesuaian,
				'$useraktif');";
		
		if($selisih_point>=0){
			$jenis_mutasi=0;
		}
		else{
			$jenis_mutasi=1;
		}
		
		//MENYMPAN TRANSAKSI KEDALAM MUTASI ACCOUNT
		//mengenerate id top up
		$nourut 				= rand(10000,99999);
		$id_mutasi 			= "MTKORP".dateYMD().$nourut;
		$point					= $point_penyesuaian;
		$sql .=
			"INSERT INTO $this->TABEL4
				(id_mutasi_point,waktu_transaksi,id_member,referensi,
				keterangan,jenis_mutasi,jumlah_mutasi_point,
				operator,kode_validasi)
			VALUES(
				'$id_mutasi',{fn NOW()},'$id_member','$id_koreksi',
				'Koreksi saldo by admin id koreksi $id_koreksi',$jenis_mutasi,".abs($selisih_point).",
				'$useraktif','$kode_validasi');";
		
		
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 011-02");
		}
		
		return $sql;
	}
	
	function ambilLogKoreksiSaldo($tgl_mula,$tgl_akhir,$cari,$order_by,$asc){
		
		/*
		ID	: 007
		Desc	:Mengembalikan data mutasi
		*/
		
		//kamus
		global $db;
		
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":"ORDER BY id_mutasi";
		
		$sql = 
			"SELECT	id_mutasi,CONVERT(varchar(20),waktu_transaksi,106) as waktu_transaksi_con,CONVERT(varchar(20),waktu_koreksi,106) as waktu_koreksi_con,
				id_member,keterangan,jenis_mutasi,jumlah_mutasi,pengkoreksi,operator
			FROM tbl_member_log_koreksi_mutasi_account
			WHERE 
				waktu_koreksi BETWEEN CONVERT(DATETIME,'$tgl_mula 00:00:00',105) AND CONVERT(DATETIME,'$tgl_akhir 23:59:59',105)
				AND pengkoreksi LIKE '$cari%'
			$order";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 012");//.$sql);
		}
				
	}//  END ambilLogKoreksiSaldo
	
	function ambilLogKoreksiPoint($tgl_mula,$tgl_akhir,$cari,$order_by,$asc){
		
		/*
		ID	: 007
		Desc	:Mengembalikan data mutasi
		*/
		
		//kamus
		global $db;
		
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":"ORDER BY id_koreksi";
		
		$sql = 
			"SELECT	id_koreksi,id_member,CONVERT(varchar(20),waktu_koreksi,106) as waktu_koreksi,nama_member,
			telp_member,hp_member,keterangan,jumlah_koreksi,operator
			FROM tbl_member_log_koreksi_point
			WHERE 
				waktu_koreksi BETWEEN CONVERT(DATETIME,'$tgl_mula 00:00:00',105) AND CONVERT(DATETIME,'$tgl_akhir 23:59:59',105)
				AND operator LIKE '$cari%'
			$order";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 013");
		}
				
	}//  END ambilLogKoreksiPoint
	
	function ambilDetailMutasi($id_mutasi){
		
		/*
		ID	: 014
		Desc	:Mengembalikan data mutasi
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT
				id_mutasi,CONVERT(CHAR(17),waktu_transaksi,113)as waktu_transaksi,id_member,referensi,keterangan,
				jenis_mutasi,jumlah_mutasi,saldo,operator
			FROM $this->TABEL3
			WHERE 
				id_mutasi LIKE '$id_mutasi';";
				
		if ($result = $db->sql_query($sql)){
			return $db->sql_fetchrow($result);
		} 
		else{
			die_error("Gagal $this->ID_FILE 014 ");//.$sql);
		}
				
	}//  END ambilDetailMutasi
	
	function koreksiMutasiAccount($id_member,$id_mutasi,$jumlah_mutasi){
		
		/*
		ID	: 015
		IS	: data transaksi sudah ada dalam database
		FS	: jumlah mutasi account dirubah diubah
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		$useraktif	= $userdata['username'];
		$id_member	= trim($id_member);
		
		//MEMERIKSA VALIDASI DATA
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi,nama,telp_rumah,handphone
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$nama		 				= trim($row['nama']);
				$telp_rumah 		= trim($row['telp_rumah']);
				$handphone 			= trim($row['handphone']);
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 010-01");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)!=$kode_validasi){
			//jika kode validasi tidak sama, berarti data tidak valid
			return "false";
		}
		
		$data_transaksi=$this->ambilDetailMutasi($id_mutasi);
		
		//membatalkan transaksi mutasi
		$sql =
			"UPDATE $this->TABEL3
			SET flag_batal=1
			WHERE id_mutasi='$id_mutasi';";
		
		//MENYMPAN TRANSAKSI KEDALAM HISTORY koreksi saldo
		
		$sql .=
			"INSERT INTO tbl_member_log_koreksi_mutasi_account
				(id_mutasi,waktu_transaksi,id_member,referensi, 
				keterangan,jenis_mutasi,jumlah_mutasi,
				operator,waktu_koreksi,pengkoreksi,keterangan_koreksi)
			VALUES(
				'$id_mutasi','$data_transaksi[waktu_transaksi]','$id_member','$data_transaksi[referensi]',
				'$data_transaksi[keterangan]','$data_transaksi[jenis_mutasi]',$jumlah_mutasi,
				'$data_transaksi[operator]',{fn NOW()},'$useraktif','Koreksi $data_transaksi[keterangan]');";
				
		//MENYMPAN TRANSAKSI KEDALAM MUTASI ACCOUNT
		$id_batal							= "BT-".$id_mutasi;
		$id_koreksi						= "KR-".$id_mutasi;
		
		$tanda	=($data_transaksi['jenis_mutasi']==0)?1:-1;
		
		$saldo_sekarang			= $this->ambilDeposit($id_member);
		$saldo							= $saldo_sekarang+(-$tanda*$data_transaksi['jumlah_mutasi']);
		
		//mengenerate kode validasi baru setelah ditambahkan nilai koreksi
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$saldo,$point);
		
		//membatalkan transaksi yang akan dikoreksi
		$sql .=
			"INSERT INTO $this->TABEL3
				(id_mutasi,waktu_transaksi,id_member,referensi,
				keterangan,jenis_mutasi,jumlah_mutasi,saldo,
				operator,kode_validasi)
			VALUES(
				'$id_batal',{fn NOW()},'$id_member','$id_mutasi',
				'Pembatalan $data_transaksi[keterangan]',1-$data_transaksi[jenis_mutasi],$data_transaksi[jumlah_mutasi],$saldo,
				'$useraktif','$kode_validasi');";

		//menyimpan transaksi yang sudah dikoreksi
		
		if (abs($jumlah_mutasi)>0){
			$saldo_sekarang			= $saldo;
			$saldo							= $saldo_sekarang+($tanda*abs($jumlah_mutasi));
			
			//mengenerate kode validasi baru setelah ditambahkan nilai koreksi
			$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$saldo,$point);
			
			$sql .=
				"INSERT INTO $this->TABEL3
					(id_mutasi,waktu_transaksi,id_member,referensi,
					keterangan,jenis_mutasi,jumlah_mutasi,saldo,
					operator,kode_validasi)
				VALUES(
					'$id_koreksi',{fn NOW()},'$id_member','$id_mutasi',
					'Koreksi $data_transaksi[keterangan]',$data_transaksi[jenis_mutasi],$jumlah_mutasi,$saldo,
					'$useraktif','$kode_validasi');";
		}
	
		//mengenerate kode validasi baru setelah ditambahkan nilai top up
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$saldo,$point);
		
		//Me ngupdate saldo
		$sql .=
			"UPDATE $this->TABEL1
			SET saldo=$saldo,kode_validasi='$kode_validasi'
			WHERE id_member='$id_member';";
	
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 015-02");
		}
		
		//return $sql;
	}//koreksiMutasiAccount
	
	function mutasiTukarPoint($id_member,$id_promo_poin){
	  
		$id_member=trim($id_member);
		
		/*
		ID	: 016
		IS	: data member sudah ada dalam database
		FS	: poin member berkurang
		*/
		
		//kamus
		
		global $userdata;
		global $db;
		
		$useraktif	= $userdata['username'];
		
		//MEMERIKSA VALIDASI DATA
		$sql = 
			"SELECT 
				no_ktp,saldo,point,kode_validasi
			FROM $this->TABEL1
			WHERE 
				id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$no_ktp 				= trim($row['no_ktp']);
				$deposit				= trim($row['saldo']);
				$point					= trim($row['point']);
				$kode_validasi	= trim($row['kode_validasi']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 016-01");
		}
		
		if($this->kodeValidasi($id_member,$no_ktp,$deposit,$point)!=$kode_validasi){
			//jika kode validasi tidak sama, berarti data tidak valid
			return "false";
		}
		
		//mengambil data promo
		//MEMERIKSA VALIDASI DATA
		$sql = 
			"SELECT 
				id_promo_poin,kode_promo,nama_penukaran_poin,poin
			FROM tbl_member_promo_tukar_point
			WHERE 
				id_promo_poin='$id_promo_poin'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$id_promo_poin			= trim($row['id_promo_poin']);
				$kode_promo					= trim($row['kdoe_promo']);
				$nama_penukaran_poin= trim($row['nama_penukaran_poin']);
				$mutasi_point				= trim($row['poin']);
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 016-02".$sql);
		}
		
		if($point<$mutasi_point){
			return "poin kurang";
		}
		
		//mengenerate kode validasi baru setelah melakukan transaksi
		$point				=	$point-$mutasi_point;
		$kode_validasi=$this->kodeValidasi($id_member,$no_ktp,$deposit,$point);
		
		
		//MENGUPDATE POINT
		$sql =
			"UPDATE $this->TABEL1
			SET point=$point,kode_validasi='$kode_validasi'
			WHERE id_member='$id_member';";
				
		//MENYMPAN TRANSAKSI KEDALAM MUTASI ACCOUNT
		//mengenerate id top up
		$nourut 				= rand(10000,99999);
		$id_mutasi_point= "TKPT".dateYMD().$nourut;
				
		$sql .=
			"INSERT INTO $this->TABEL4
				(id_mutasi_point,waktu_transaksi,id_member,referensi,
				keterangan,jenis_mutasi,jumlah_mutasi_point,
				operator,kode_validasi)
			VALUES(
				'$id_mutasi_point',{fn NOW()},'$id_member','$kode_promo',
				'Tukar point $kode_promo $nama_penukaran_poin',1,$mutasi_point,
				'$useraktif','$kode_validasi');";
		
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 016-03");//.$sql);
		}
		
		return "true";
	}
}
?>