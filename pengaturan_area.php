<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassArea.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Area	= new Area();

if ($mode=='add'){
	// add 
	
	$pesan = $HTTP_GET_VARS['pesan'];
	
	if($pesan==1){
		$pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
		$bgcolor_pesan="98e46f";
	}
	
	$template->set_filenames(array('body' => 'pengaturan.area/edit.tpl')); 
	$template->assign_vars(array(
	 'BCRUMP'				=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_area.'.$phpEx).'">Area</a> | <a href="'.append_sid('pengaturan_area.'.$phpEx."?mode=add").'">Tambah Area</a> ',
	 'JUDUL'				=>'Tambah Data Area',
	 'MODE'   			=> 'save',
	 'SUB'    			=> '0',
	 'PESAN'				=> $pesan,
	 'BGCOLOR_PESAN'=> $bgcolor_pesan,
	 'U_ADD_ACT'		=> append_sid('pengaturan_area.'.$phpEx)
	 )
	);
} 
else if ($mode=='save'){
	// aksi menyimpan
	$kode  		= str_replace(" ","",$HTTP_POST_VARS['kode']);
	$kode_old	= str_replace(" ","",$HTTP_POST_VARS['kode_old']);
	$nama   	= $HTTP_POST_VARS['nama'];

	$terjadi_error=false;
	
	if($Area->periksaDuplikasi($kode) && $kode!=$kode_old){
		$pesan="<font color='white' size=3>Kode yang dimasukkan sudah terdaftar dalam sistem!</font>";
		$bgcolor_pesan="red";
	}
	else{
		
		if($submode==0){
			$judul="Tambah Data Area";
			$path	='<a href="'.append_sid('pengaturan_area.'.$phpEx."?mode=add").'">Tambah Area</a> ';
			
			if($Area->tambah($kode,$nama)){					
				redirect(append_sid('pengaturan_area.'.$phpEx.'?mode=add&pesan=1',true));
			}
		}
		else{
			
			$judul="Ubah Data Area";
			$path	='<a href="'.append_sid('pengaturan_area.'.$phpEx."?mode=edit&id=$kode_old").'">Ubah Area</a> ';
			
			if($Area->ubah($kode_old,$kode,$nama)){
				$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
				$bgcolor_pesan="98e46f";
			}
		}
		
		//exit;
		
	}
	
	$template->set_filenames(array('body' => 'pengaturan.area/edit.tpl')); 
	$template->assign_vars(array(
	 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_area.'.$phpEx).'">Area</a> | '.$path,
	 'JUDUL'		=>$judul,
	 'MODE'   	=> 'save',
	 'SUB'    	=> $submode,
	 'KODE_OLD' => $kode_old,
	 'KODE'    	=> $kode,
	 'NAMA'    	=> $nama,
	 'PESAN'		=> $pesan,
	 'BGCOLOR_PESAN'=> $bgcolor_pesan,
	 'U_ADD_ACT'=>append_sid('pengaturan_area.'.$phpEx)
	 )
	);
	
	
} 
else if ($mode=='edit'){
	// edit
	
	$id = $HTTP_GET_VARS['id'];
	
	$row=$Area->ambilDataDetail($id);
	
	$template->set_filenames(array('body' => 'pengaturan.area/edit.tpl')); 
	$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_area.'.$phpEx).'">Area</a> | <a href="'.append_sid('pengaturan_area.'.$phpEx."?mode=edit&id=$id").'">Ubah Area</a> ',
		 'JUDUL'		=>'Ubah Data Area',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '1',
		 'KODE_OLD'	=> $row['KodeArea'],
		 'KODE'    	=> $row['KodeArea'],
		 'NAMA'    	=> $row['NamaArea'],
		 'BGCOLOR_PESAN'=> $bgcolor_pesan,
		 'U_ADD_ACT'=>append_sid('pengaturan_area.'.$phpEx)
		 )
	);
} 
else if ($mode=='delete'){
	// aksi hapus
	$list = str_replace("\'","'",$HTTP_GET_VARS['list']);
	$Area->hapus($list);
	
	exit;
} 
else {
	// LIST
	$template->set_filenames(array('body' => 'pengaturan.area/index.tpl')); 
	
	if($HTTP_POST_VARS["txt_cari"]!=""){
		$cari=$HTTP_POST_VARS["txt_cari"];
	}
	else{
		$cari=$HTTP_GET_VARS["cari"];
	}
	
	$kondisi	=($cari=="")?"":
		" WHERE KodeArea LIKE '%$cari%' 
			OR KodeArea LIKE '%$cari%' 
			OR NamaArea LIKE '%$cari%'";
	
	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging=pagingData($idx_page,"KodeArea","tbl_md_area","",$kondisi,"pengaturan_area.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING======================================================
	
	$sql = 
		"SELECT *
		FROM tbl_md_area $kondisi 
		ORDER BY KodeArea,NamaArea LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
	$idx_check=0;
	
	
	if ($result = $db->sql_query($sql)){
		$i = $idx_page*$VIEW_PER_PAGE+1;
	  while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
			
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$idx_check++;
			
			$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[KodeArea]'\"/>";
			
			$act 	="<a href='".append_sid('pengaturan_area.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
			$act .="<a  href='' onclick='return hapusData(\"$row[KodeArea]\");'>Delete</a>";
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'		=>$odd,
						'check'	=>$check,
						'no'		=>$i,
						'kode'	=>$row['KodeArea'],
						'nama'	=>$row['NamaArea'],
						'action'=>$act
					)
				);
			
			$i++;
	  }
		
		if($i-1<=0){
			$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
		}
	} 
	else{
		echo("Err :".__LINE__);exit;
	} 
	
	$template->assign_vars(array(
		'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_area.'.$phpEx).'">Area</a>',
		'U_ADD'					=> append_sid('pengaturan_area.'.$phpEx.'?mode=add'),
		'ACTION_CARI'		=> append_sid('pengaturan_area.'.$phpEx),
		'TXT_CARI'			=> $cari,
		'NO_DATA'				=> $no_data,
		'PAGING'				=> $paging
		)
	);
	
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>