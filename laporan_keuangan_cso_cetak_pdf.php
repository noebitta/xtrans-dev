<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$cari  					= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$sort_by				= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$order					= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"":
	" WHERE (username='$cari'
	  OR nama LIKE '%$cari%' 
		OR telp LIKE '%$cari%'
		OR NRP LIKE '%$cari%')";

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"nama":$sort_by;		

//QUERY
$sql=
	"SELECT 
	  user_id,nama,username,nrp,telp,hp,KodeCabang,f_cabang_get_name_by_kode(KodeCabang) AS cabang
	FROM 
	  tbl_user
	$kondisi_cari";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		PetugasCetakTiket,
		IS_NULL(SUM(IF(JenisPembayaran=0,SubTotal,0)),0) AS TotalTiketTunai, 
		IS_NULL(SUM(IF(JenisPembayaran=1,SubTotal,0)),0) AS TotalTiketDebit, 
		IS_NULL(SUM(IF(JenisPembayaran=2,SubTotal,0)),0) AS TotalTiketKredit, 
		IS_NULL(SUM(SubTotal),0) AS TotalUangTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount, 
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket 
	FROM tbl_reservasi_olap
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		AND (FlagPesanan IS NULL OR FlagPesanan=0) 
	GROUP BY PetugasCetakTiket ORDER BY PetugasCetakTiket";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_total[$row['PetugasCetakTiket']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		PetugasPenjual,
		IS_NULL(SUM(IF(JenisPembayaran=0,HargaPaket,0)),0) AS TotalPaketTunai, 
		IS_NULL(SUM(IF(JenisPembayaran=1,HargaPaket,0)),0) AS TotalPaketDebit, 
		IS_NULL(SUM(IF(JenisPembayaran=2,HargaPaket,0)),0) AS TotalPaketKredit, 
		IS_NULL(SUM(HargaPaket),0) AS TotalUangPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY PetugasPenjual ORDER BY PetugasPenjual";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['PetugasPenjual']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){
	$temp_array[$idx]['user_id']					= $row['user_id'];
	$temp_array[$idx]['nama']							= $row['nama'];
	$temp_array[$idx]['username']					= $row['username'];
	$temp_array[$idx]['nrp']							= $row['nrp'];
	$temp_array[$idx]['telp']							= $row['telp'];
	$temp_array[$idx]['telp']							= $row['telp'];
	$temp_array[$idx]['hp']								= $row['hp'];
	$temp_array[$idx]['KodeCabang']				= $row['KodeCabang'];
	$temp_array[$idx]['cabang']						= $row['cabang'];
	$temp_array[$idx]['TotalTiketTunai']	= $data_total[$row['user_id']]['TotalTiketTunai'];
	$temp_array[$idx]['TotalTiketDebit']	= $data_total[$row['user_id']]['TotalTiketDebit'];
	$temp_array[$idx]['TotalTiketKredit']	= $data_total[$row['user_id']]['TotalTiketKredit'];
	$temp_array[$idx]['TotalUangTiket']		= $data_total[$row['user_id']]['TotalUangTiket'];
	$temp_array[$idx]['TotalDiscount']		= $data_total[$row['user_id']]['TotalDiscount'];
	$temp_array[$idx]['TotalTiket']				= $data_total[$row['user_id']]['TotalTiket'];
	$temp_array[$idx]['TotalPaketTunai']	= $data_paket_total[$row['user_id']]['TotalPaketTunai'];
	$temp_array[$idx]['TotalPaketDebit']	= $data_paket_total[$row['user_id']]['TotalPaketDebit'];
	$temp_array[$idx]['TotalPaketKredit']	= $data_paket_total[$row['user_id']]['TotalPaketKredit'];
	$temp_array[$idx]['TotalUangPaket']		= $data_paket_total[$row['user_id']]['TotalUangPaket'];
	$temp_array[$idx]['TotalPaket']				= $data_paket_total[$row['user_id']]['TotalPaket'];
	$temp_array[$idx]['Total']						= $temp_array[$idx]['TotalUangTiket'] + $temp_array[$idx]['TotalUangPaket'] - $temp_array[$idx]['TotalDiscount'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}
	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,60);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Keuangan Penjualan CSO','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(50,5,'Nama','B',0,'C',1);
$pdf->Cell(20,5,'NRP','B',0,'C',1);
$pdf->Cell(30,5,'Cabang','B',0,'C',1);
$pdf->Cell(30,5,'Tiket','B',0,'C',1);
$pdf->Cell(30,5,'Uang Tiket','B',0,'C',1);
$pdf->Cell(30,5,'Paket','B',0,'C',1);
$pdf->Cell(30,5,'Uang Paket','B',0,'C',1);
$pdf->Cell(30,5,'Tot.Disc.','B',0,'C',1);
$pdf->Cell(30,5,'TOTAL','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

$idx=0;

while($idx<count($temp_array)){
	
	//total tiket
	$total_uang_tiket	= $temp_array[$idx]['TotalUangTiket'];
	$total_discount		= $temp_array[$idx]['TotalDiscount'];
	$total_tiket			= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_uang_paket		= $temp_array[$idx]['TotalUangPaket'];
	$total_paket				= $temp_array[$idx]['TotalPaket'];
	
	//total
	$total							= $temp_array[$idx]['Total'];
	
	$sum_tiket			+= $total_tiket;
	$sum_uang_tiket	+= $total_uang_tiket;
	$sum_paket			+= $total_paket;
	$sum_uang_paket	+= $total_uang_paket;
	$sum_discount		+= $total_discount;
	$sum_total			+= $total;
	
	$pdf->Cell(5,5,$idx+1,'',0,'C');
	$pdf->MultiCell2(50,5,$temp_array[$idx]['nama'],'','L');
	$pdf->Cell(20,5,$temp_array[$idx]['nrp'],'',0,'L');
	$pdf->Cell(20,5,$temp_array[$idx]['cabang'],'',0,'L');
	$pdf->Cell(30,5,number_format($total_tiket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_uang_tiket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_paket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_uang_paket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_discount,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total,0,",","."),'',0,'R');
	$pdf->Ln(0);
	$pdf->Cell(285,1,'','B',0,'');
	$pdf->Ln();
	
	$idx++;
}

$pdf->Cell(5,5,'','',0,'C');
$pdf->Cell(90,5,'TOTAL','',0,'L');
$pdf->Cell(30,5,number_format($sum_tiket,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($sum_uang_tiket,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($sum_paket,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($sum_uang_paket,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($sum_discount,0,",","."),'',0,'R');
$pdf->Cell(30,5,number_format($sum_total,0,",","."),'',0,'R');

										
$pdf->Output();
						
?>