<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassUser.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Asuransi	= new Asuransi();
$Jurusan	= new Jurusan();
$User			= new User();
		// LIST
		$template->set_filenames(array('body' => 'laporan_asuransi/laporan_asuransi_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi_sort	= ($sort_by=='') ?"ORDER BY WaktuTransaksi $order" : "ORDER BY $sort_by $order";
		
		$temp_cari_jurusan	= explode('-',$cari);

		$cari_asal		= $temp_cari_jurusan[0];
		$cari_tujuan	= $temp_cari_jurusan[1];

    $kondisi="(WaktuBayar BETWEEN '$tanggal_mulai_mysql 00:00:00' AND '$tanggal_akhir_mysql 23:59:59') AND IsDibayar=1";
		$kondisi	.=($cari=="")?"":
			" AND (Nama LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR HP LIKE '%$cari%'
				OR (f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) LIKE '%$cari_asal%' AND 
					 (f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) LIKE '%$cari_tujuan%'))
				OR f_user_get_nama_by_userid(PetugasTransaksi) LIKE '%$cari%')";
		

		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdAsuransi","tbl_asuransi","&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order","WHERE ".$kondisi,"laporan_asuransi.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================

		$sql = 
			"SELECT *
			FROM tbl_asuransi
			WHERE 1 AND $kondisi
			$kondisi_sort LIMIT $idx_awal_record,$VIEW_PER_PAGE";
				
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				//mengambil jurusan
				$data_jurusan	= $Jurusan->ambilDataDetail($row['IdJurusan']);
				
				//mengambil data user
				$data_user		= $User->ambilDataDetail($row['PetugasTransaksi']);
				
				//mengambil data plan asuransi
				$data_plan_asuransi	= $Asuransi->ambilDataDetail($row['PlanAsuransi']);
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'no'=>$i,
							'waktu_transaksi'=>dateparse(FormatMySQLDateToTgl($row['WaktuTransaksi'])),
							'plan'=>$data_plan_asuransi['NamaPlan'],
							'besar_premi'=>number_format($row['BesarPremi'],0,",","."),
							'no_tiket'=>$row['NoTiket'],
							'tgl_berangkat'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])),
							'jurusan'=>$data_jurusan['NamaCabangAsal']."-".$data_jurusan['NamaCabangTujuan'],
							'nama'=>$row['Nama'],
							'telp'=>$row['Telp']."/".$row['HP'],
							'tgl_lahir'=>dateparse(FormatMySQLDateToTgl($row['TglLahir'])),
							'cso'=>$data_user['nama']
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=13 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			echo("Error :".__LINE__);exit;
		} 
		
		$order_invert	= ($order=='asc' || $order=='')?'desc':'asc';
		
		$parameter_sorting	= 'laporan_asuransi.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir;
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&cari=".$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir."&sort_by=".$sort_by."&order=".$order;
			
		//$script_cetak_pdf="Start('pengaturan_member_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
														
		$script_cetak_excel="Start('laporan_asuransi_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		//--END KOMPONEN UNTUK EXPORT
		
		//mengambil total pendapatan dari asuransi
		$total_pendapatan_asuransi	= $Asuransi->ambilTotalPendapatanAsuransi($tanggal_mulai_mysql,$tanggal_akhir_mysql,$kondisi);
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_keuangan">Home</a> | <a href="'.append_sid('laporan_asuransi.'.$phpEx).'">Laporan Peserta Asuransi</a>',
			'ACTION_CARI'		=> append_sid('laporan_asuransi.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'TGL_AWAL'			=> $tanggal_mulai,
			'TGL_AKHIR'			=> $tanggal_akhir,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging,
			'TOTAL_PREMI_ASURANSI'=> number_format($total_pendapatan_asuransi,0,",","."),
			'A_SORT_BY_WAKTU_TRX'			=>  append_sid($parameter_sorting.'&sort_by=WaktuTransaksi&order='.$order_invert),
			'A_SORT_BY_PLAN'			=>  append_sid($parameter_sorting.'&sort_by=PlanAsuransi&order='.$order_invert),
			'A_SORT_BY_PREMI'			=>  append_sid($parameter_sorting.'&sort_by=BesarPremi&order='.$order_invert),
			'A_SORT_BY_NO_TIKET'			=>  append_sid($parameter_sorting.'&sort_by=NoTiket&order='.$order_invert),
			'A_SORT_BY_TGL_BRGKT'			=>  append_sid($parameter_sorting.'&sort_by=TglBerangkat&order='.$order_invert),
			'A_SORT_BY_JURUSAN'		=>  append_sid($parameter_sorting.'&sort_by=KodeJadwal&order='.$order_invert),
			'A_SORT_BY_NAMA'				=>  append_sid($parameter_sorting.'&sort_by=Nama&order='.$order_invert),
			'A_SORT_BY_TELP'			=>  append_sid($parameter_sorting.'&sort_by=Telp&order='.$order_invert),
			'A_SORT_BY_TGL_LAHIR'	=>  append_sid($parameter_sorting.'&sort_by=TglLahir&order='.$order_invert),
			'A_SORT_BY_PENUTUP'	=>  append_sid($parameter_sorting.'&sort_by=PetugasTransaksi&order='.$order_invert),
			'CETAK_XL'						=> $script_cetak_excel
			)
		);
		
	  

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>