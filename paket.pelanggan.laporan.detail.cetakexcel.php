<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPaket.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN_PAKET,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$tanggal_mulai  = $HTTP_GET_VARS['tglmulai'];
$tanggal_akhir  = $HTTP_GET_VARS['tglakhir'];
$kode_pelanggan	= $HTTP_GET_VARS['kodepelanggan'];

$sql	= 
	"SELECT *,
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabangAsal,
		f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) AS KodeCabangTujuan
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai' AND '$tanggal_akhir') 
		AND CetakTiket=1 AND FlagBatal!=1 AND KodePelanggan='$kode_pelanggan'
	ORDER BY TglBerangkat,JamBerangkat ASC";
	
//debug
//echo($sql);exit;

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

//Mengambil data pelanggan
$Paket	= new Paket();
$data_pelanggan	= $Paket->ambilDataPelanggan($kode_pelanggan);

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Paket Pelanggan '.FormatMySQLDateToTgl($tanggal_mulai).' s/d '.FormatMySQLDateToTgl($tanggal_akhir));
$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Pelanggan '.$data_pelanggan['NamaPelanggan'].'('.$data_pelanggan['KodePelanggan'].' CP:'.$data_pelanggan['ContactPerson'].' ('.$data_pelanggan['TelpCP'].') ');
$objPHPExcel->getActiveSheet()->mergeCells('A4:A5');
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('B4:B5');
$objPHPExcel->getActiveSheet()->setCellValue('B4', '#Resi');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('C4:C5');
$objPHPExcel->getActiveSheet()->setCellValue('C4', '#Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('D4:D5');
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Waktu Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('E4:G4');
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Pengirim');
$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Alamat');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('H4:J4');
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Pengirim');
$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Alamat');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('K4:K5');
$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Berat (Kg)');
$objPHPExcel->getActiveSheet()->mergeCells('L4:L5');
$objPHPExcel->getActiveSheet()->setCellValue('L4', 'Harga (Rp.)');
$objPHPExcel->getActiveSheet()->mergeCells('M4:M5');
$objPHPExcel->getActiveSheet()->setCellValue('M4', 'Diskon (Rp.)');
$objPHPExcel->getActiveSheet()->mergeCells('N4:N5');
$objPHPExcel->getActiveSheet()->setCellValue('N4', 'Bayar (Rp.)');
$objPHPExcel->getActiveSheet()->mergeCells('O4:O5');
$objPHPExcel->getActiveSheet()->setCellValue('O4', 'Layanan');
$objPHPExcel->getActiveSheet()->mergeCells('P4:P5');
$objPHPExcel->getActiveSheet()->setCellValue('P4', 'Jenis Bayar');

$idx=0;
$idx_row=6;

while($row = $db->sql_fetchrow($result)){
	
		
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NamaPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['AlamatPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['TelpPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NamaPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['AlamatPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['TelpPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Berat']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['HargaPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $row['Diskon']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['TotalBayar']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $row['Layanan']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $row['JenisPembayaran']==0?"TUNAI":"LANGGANAN");
	
	$idx_row++;
	$idx++;
}

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':K'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, "TOTAL");
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row,"=SUM(L6:L".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row,"=SUM(M6:M".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row,"=SUM(N6:N".($idx_row-1).")");
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Paket Pelanggan '.$data_pelanggan['NamaPelanggan'].' ('.$data_pelanggan['KodePelanggan'].' '.$tanggal_mulai_mysql.' sd '.$tanggal_akhir_mysql.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
