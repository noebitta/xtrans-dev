<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']>=$LEVEL_MANAJEMEN){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo submode kosong, defaultnya EXplorer Mode
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$template->set_filenames(array('body' => 'pengaturan_body.tpl')); 
$template->assign_vars(array(
   'BCRUMP'    				=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan.'.$phpEx).'">Pengaturan</a>',
   'U_JADWAL' 				=> append_sid('pengaturan_jadwal.'.$phpEx),
   'U_MOBIL' 					=> append_sid('pengaturan_mobil.'.$phpEx),
   'U_SOPIR' 					=> append_sid('pengaturan_sopir.'.$phpEx),
   'U_USER' 					=> append_sid('pengaturan_user.'.$phpEx),
	 'U_CABANG' 				=> append_sid('pengaturan_cabang.'.$phpEx),
	 'U_USER_LOGIN'			=> append_sid('pengaturan_user_list_login.'.$phpEx),
   'U_JURUSAN' 				=> append_sid('pengaturan_jurusan.'.$phpEx)
   )
);

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>