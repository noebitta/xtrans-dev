<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$cari           = isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan.keuangan.percso/index.tpl'));

//$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$id_sort				= isset($HTTP_GET_VARS['idsort'])? $HTTP_GET_VARS['idsort'] : $HTTP_POST_VARS['idsort'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

/*SELECT SORT*/
switch($id_sort){
  case "1":
    $sort_by  = "Nama";
    break;
  case "2":
    $sort_by  = "NRP";
    break;
  case "3":
    $sort_by  = "Cabang";
    break;
  case "4":
    $sort_by="TotalPenumpangU";
    break;
  case "5":
    $sort_by="TotalPenumpangM";
    break;
  case "6":
    $sort_by="TotalPenumpangK";
    break;
  case "7":
    $sort_by="TotalPenumpangKK";
    break;
  case "8":
    $sort_by="TotalPenumpangG";
    break;
  case "9":
    $sort_by="TotalPenumpangR";
    break;
  case "10":
    $sort_by="TotalPenumpangVR";
    break;
  case "11":
    $sort_by="TotalPenumpangV";
    break;
  case "12":
    $sort_by="TotalTiket";
    break;
  case "13":
    $sort_by="TotalTiketTunai";
    break;
  case "14":
    $sort_by="TotalTiketDebit";
    break;
  case "15":
    $sort_by="TotalTiketKredit";
    break;
  case "16":
    $sort_by="TotalDiscount";
    break;
  case "17":
    $sort_by="TotalUangTiket";
    break;
  case "18":
    $sort_by="TotalPaketP";
    break;
  case "19":
    $sort_by="TotalPaketGD";
    break;
  case "20":
    $sort_by="TotalPaketGA";
    break;
  case "21":
    $sort_by="TotalPaketS";
    break;
  case "22":
    $sort_by="TotalPaketCA";
    break;
  case "23":
    $sort_by="TotalPaketCD";
    break;
  case "24":
    $sort_by="TotalPaketI";
    break;
  case "25":
    $sort_by="TotalPaket";
    break;
  case "26":
    $sort_by="PaketOmzetTunai";
    break;
  case "27":
    $sort_by="PaketOmzetLangganan";
    break;
  case "28":
    $sort_by="PaketTotalDiskon";
    break;
  case "29":
    $sort_by="TotalUangPaket";
    break;
  case "30":
    $sort_by="TotalSetor";
    break;
}

$sort_by  = $id_sort==""?"Nama":$sort_by;

$kondisi	=($cari=="")?"":
	" AND (username='$cari'
	  OR nama LIKE '%$cari%'
		OR telp LIKE '%$cari%'
		OR NRP LIKE '%$cari%')";

		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"","tbl_user",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
"WHERE user_active=1 $kondisi","laporan.keuangan.percso.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
  "SELECT
	  user_id,nama,NRP,KodeCabang,f_cabang_get_name_by_kode(KodeCabang) AS cabang
	FROM
	  tbl_user
	WHERE user_active=1 $kondisi";

if (!$result_laporan = $db->sql_query($sql)){
  echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	=
  "SELECT
		PetugasCetakTiket,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(1),0) AS TotalTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=0,Total,0),IF(JenisPembayaran=0,HargaTiket,0))),0) AS TotalTiketTunai,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=1,Total,0),IF(JenisPembayaran=1,HargaTiket,0))),0) AS TotalTiketDebit,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=2,Total,0),IF(JenisPembayaran=2,HargaTiket,0))),0) AS TotalTiketKredit,
		IS_NULL(SUM(SubTotal),0) AS TotalUangTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,Total,0),HargaTiket)),0) AS TotalOmzet
	FROM tbl_reservasi_olap
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
		AND (FlagPesanan IS NULL OR FlagPesanan=0)
	GROUP BY PetugasCetakTiket ORDER BY PetugasCetakTiket";

if (!$result = $db->sql_query($sql)){
  echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
  $data_tiket[$row['PetugasCetakTiket']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		PetugasPenjual,
		IS_NULL(COUNT(IF(Layanan='P',1,NULL)),0) AS TotalPaketP,
		IS_NULL(COUNT(IF(Layanan='GD',1,NULL)),0) AS TotalPaketGD,
		IS_NULL(COUNT(IF(Layanan='GA',1,NULL)),0) AS TotalPaketGA,
		IS_NULL(COUNT(IF(Layanan='S',1,NULL)),0) AS TotalPaketS,
		IS_NULL(COUNT(IF(Layanan='CA',1,NULL)),0) AS TotalPaketCA,
		IS_NULL(COUNT(IF(Layanan='CD',1,NULL)),0) AS TotalPaketCD,
		IS_NULL(COUNT(IF(Layanan='I',1,NULL)),0) AS TotalPaketI,
		IS_NULL(COUNT(1),0) AS TotalPaket,
		IS_NULL(SUM(IF(JenisPembayaran=0,TotalBayar,0)),0) AS OmzetPaketTunai,
		IS_NULL(SUM(IF(JenisPembayaran!=0,TotalBayar,0)),0) AS OmzetPaketLangganan,
		IS_NULL(SUM(Diskon),0) AS OmzDiskon,
		IS_NULL(SUM(TotalBayar),0) AS TotalPenjualanPaket
	FROM tbl_paket
	WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY  PetugasPenjual ORDER BY PetugasPenjual";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//debug
//echo($sql);exit;

while ($row = $db->sql_fetchrow($result)){
	$data_paket[$row['PetugasPenjual']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){
  $temp_array[$idx]['user_id']						= $row['user_id'];
  $temp_array[$idx]['Nama']								= $row['nama'];
  $temp_array[$idx]['NRP']								= $row['NRP'];
  $temp_array[$idx]['UserId']							= $row['user_id'];
	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Cabang']					    = $row['cabang'];

  $temp_array[$idx]['TotalPenumpangU']		= $data_tiket[$row['user_id']]['TotalPenumpangU'];
  $temp_array[$idx]['TotalPenumpangM']		= $data_tiket[$row['user_id']]['TotalPenumpangM'];
  $temp_array[$idx]['TotalPenumpangK']		= $data_tiket[$row['user_id']]['TotalPenumpangK'];
  $temp_array[$idx]['TotalPenumpangKK']		= $data_tiket[$row['user_id']]['TotalPenumpangKK'];
  $temp_array[$idx]['TotalPenumpangG']		= $data_tiket[$row['user_id']]['TotalPenumpangG'];
  $temp_array[$idx]['TotalPenumpangR']		= $data_tiket[$row['user_id']]['TotalPenumpangR'];
  $temp_array[$idx]['TotalPenumpangVR']		= $data_tiket[$row['user_id']]['TotalPenumpangVR'];
  $temp_array[$idx]['TotalPenumpangV']		= $data_tiket[$row['user_id']]['TotalPenumpangV'];
  $temp_array[$idx]['TotalTiket']					= $data_tiket[$row['user_id']]['TotalTiket'];
  $temp_array[$idx]['TotalTiketTunai']	  = $data_tiket[$row['user_id']]['TotalTiketTunai'];
  $temp_array[$idx]['TotalTiketDebit']	  = $data_tiket[$row['user_id']]['TotalTiketDebit'];
  $temp_array[$idx]['TotalTiketKredit']	  = $data_tiket[$row['user_id']]['TotalTiketKredit'];
  $temp_array[$idx]['TotalDiscount']		  = $data_tiket[$row['user_id']]['TotalDiscount'];
  $temp_array[$idx]['TotalUangTiket']		  = $data_tiket[$row['user_id']]['TotalOmzet'];

	$temp_array[$idx]['TotalPaketP']	      = $data_paket[$row['user_id']]['TotalPaketP'];
	$temp_array[$idx]['TotalPaketGD']	      = $data_paket[$row['user_id']]['TotalPaketGD'];
	$temp_array[$idx]['TotalPaketGA']	      = $data_paket[$row['user_id']]['TotalPaketGA'];
	$temp_array[$idx]['TotalPaketS']	      = $data_paket[$row['user_id']]['TotalPaketS'];
	$temp_array[$idx]['TotalPaketCA']	      = $data_paket[$row['user_id']]['TotalPaketCA'];
	$temp_array[$idx]['TotalPaketCD']	      = $data_paket[$row['user_id']]['TotalPaketCD'];
	$temp_array[$idx]['TotalPaketI']	      = $data_paket[$row['user_id']]['TotalPaketI'];
	$temp_array[$idx]['TotalPaket']	        = $data_paket[$row['user_id']]['TotalPaket'];
	$temp_array[$idx]['PaketOmzetTunai']		= $data_paket[$row['user_id']]['OmzetPaketTunai'];
	$temp_array[$idx]['PaketOmzetLangganan']= $data_paket[$row['user_id']]['OmzetPaketLangganan'];
  $temp_array[$idx]['PaketTotalDiskon']		= $data_paket[$row['user_id']]['OmzDiskon'];
	$temp_array[$idx]['TotalUangPaket']			= $data_paket[$row['user_id']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalSetor']			    = $temp_array[$idx]['TotalUangPaket']+$temp_array[$idx]['TotalUangTiket']	;

	$idx++;
}

if($order!='DESC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}

  $act 	="<a href='#' onClick='Start(\"".append_sid('laporan.keuangan.percso.detail.php?tanggal_mulai='.$tanggal_mulai.'&ptanggal_akhir1='.$tanggal_akhir.'&cari='.$temp_array[$idx]['user_id'])."\");return false'>Detail<a/>";

	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total
	$total									= $temp_array[$idx]['Total'];
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'                 =>$odd,
				'no'                  =>$idx+1,
				'nama'                =>$temp_array[$idx]['Nama'],
				'nrp'                 =>$temp_array[$idx]['NRP'],
				'cabang'              =>$temp_array[$idx]['Cabang'],
				'tiket_pax_u'         =>number_format($temp_array[$idx]['TotalPenumpangU'],0,",","."),
				'tiket_pax_m'         =>number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."),
				'tiket_pax_k'         =>number_format($temp_array[$idx]['TotalPenumpangK'],0,",","."),
				'tiket_pax_kk'        =>number_format($temp_array[$idx]['TotalPenumpangKK'],0,",","."),
				'tiket_pax_g'         =>number_format($temp_array[$idx]['TotalPenumpangG'],0,",","."),
				'tiket_pax_r'         =>number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."),
				'tiket_pax_vr'        =>number_format($temp_array[$idx]['TotalPenumpangVR'],0,",","."),
				'tiket_pax_v'         =>number_format($temp_array[$idx]['TotalPenumpangV'],0,",","."),
				'tiket_pax_total'     =>number_format($temp_array[$idx]['TotalTiket'],0,",","."),
				'tiket_omz_tunai'     =>number_format($temp_array[$idx]['TotalTiketTunai'],0,",","."),
				'tiket_omz_debit'     =>number_format($temp_array[$idx]['TotalTiketDebit'],0,",","."),
				'tiket_omz_credit'    =>number_format($temp_array[$idx]['TotalTiketKredit'],0,",","."),
				'tiket_omz_discount'  =>number_format($temp_array[$idx]['TotalDiscount'],0,",","."),
				'tiket_omz_total'     =>number_format($temp_array[$idx]['TotalUangTiket'],0,",","."),
				'paket_pax_p'         =>number_format($temp_array[$idx]['TotalPaketP'],0,",","."),
				'paket_pax_gd'        =>number_format($temp_array[$idx]['TotalPaketGD'],0,",","."),
				'paket_pax_ga'        =>number_format($temp_array[$idx]['TotalPaketGA'],0,",","."),
				'paket_pax_s'         =>number_format($temp_array[$idx]['TotalPaketS'],0,",","."),
				'paket_pax_ca'        =>number_format($temp_array[$idx]['TotalPaketCA'],0,",","."),
				'paket_pax_cd'        =>number_format($temp_array[$idx]['TotalPaketCD'],0,",","."),
				'paket_pax_i'         =>number_format($temp_array[$idx]['TotalPaketI'],0,",","."),
				'paket_pax_total'     =>number_format($temp_array[$idx]['TotalPaket'],0,",","."),
				'paket_omz_umum'      =>number_format($temp_array[$idx]['PaketOmzetTunai'],0,",","."),
				'paket_omz_langganan' =>number_format($temp_array[$idx]['PaketOmzetLangganan'],0,",","."),
        'paket_omz_discount'  =>number_format($temp_array[$idx]['PaketTotalDiskon'],0,",","."),
				'paket_omz_total'     =>number_format($temp_array[$idx]['TotalUangPaket'],0,",","."),
				'total_setor'         =>number_format($temp_array[$idx]['TotalSetor'],0,",","."),
				'act'                 =>$act
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&is_today=$is_today&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir.
										"&cari=".$cari."&idsort=".$id_sort."&order=".$order."";
													
$script_cetak_excel="Start('laporan.keuangan.percso.cetak.excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'BCRUMP'    		  => '<a href="'.append_sid('main.'.$phpEx) .'#laporan_keuangan">Home</a> | <a href="'.append_sid('laporan.keuangan.percso.'.$phpEx).'">Laporan Keuangan Per CSO</a>',
	'ACTION_CARI'		  => append_sid('laporan.keuangan.percso.'.$phpEx),
	'CARI'			      => $cari,
	'TGL_AWAL'			  => $tanggal_mulai,
	'TGL_AKHIR'			  => $tanggal_akhir,
	'NAMA'					  => $userdata['Nama'],
	'PAGING'				  => $paging,
	'CETAK_XL'			  => $script_cetak_excel,
  'SORT_BY'         => $id_sort,
  'ORDER_BY_'.$order=> "selected",
  'A_SORT'	        => append_sid('laporan.keuangan.percso.php?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&order='.$order)
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>