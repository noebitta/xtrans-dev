<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Asuransi	= new Asuransi();


	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Ditambahkan!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'asuransi/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#menu_lain_lain">Home</a> | <a href="'.append_sid('pengaturan_asuransi.'.$phpEx).'">Asuransi</a> | <a href="'.append_sid('pengaturan_asuransi.'.$phpEx."?mode=add").'">Tambah Asuransi</a> ',
		 'JUDUL'		=>'Tambah Data Asuransi',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '0',
		 'PESAN'		=> $pesan,
		 'BGCOLOR_PESAN'			=> $bgcolor_pesan,
		 'U_ASURANSI_ADD_ACT'	=> append_sid('pengaturan_asuransi.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah asuransi
		$id_plan_asuransi	= str_replace(" ","",$HTTP_POST_VARS['id_plan_asuransi']);
		$nama_plan   			= $HTTP_POST_VARS['nama_plan'];
		$besar_premi 			= $HTTP_POST_VARS['besar_premi'];
		$keterangan				= $HTTP_POST_VARS['keterangan'];
		
		$terjadi_error=false;
		
		if($submode==0){
			$judul="Tambah Data Asuransi";
			$path	='<a href="'.append_sid('pengaturan_asuransi.'.$phpEx."?mode=add").'">Tambah Asuransi</a> ';
			
			if($Asuransi->tambah($nama_plan,$besar_premi,$keterangan)){
				
				redirect(append_sid('pengaturan_asuransi.'.$phpEx.'?mode=add&pesan=1',true));
				
			}
		}
		else{
			
			$judul="Ubah Data Asuransi";
			$path	='<a href="'.append_sid('pengaturan_asuransi.'.$phpEx."?mode=edit&id=$kode_asuransi_old").'">Ubah Asuransi</a> ';
			
			if($Asuransi->ubah($id_plan_asuransi,$nama_plan,$besar_premi,$keterangan)){
				
				$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
				$bgcolor_pesan="98e46f";
				
			}
		}
		
		$template->set_filenames(array('body' => 'asuransi/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_asuransi.'.$phpEx).'">Asuransi</a> | '.$path,
			 'JUDUL'		=>$judul,
			 'MODE'   	=> 'save',
			 'SUB'    	=> $submode,
			 'ID_PLAN_ASURANSI' => $id_plan_asuransi,
			 'NAMA_PLAN'  => $nama_plan,
			 'BESAR_PREMI'=> $besar_premi,
			 'KETERANGAN'	=> $keterangan,
			 'PESAN'			=> $pesan,
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_ASURANSI_ADD_ACT'=>append_sid('pengaturan_asuransi.'.$phpEx)
			)
		);
	
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Asuransi->ambilDataDetail($id);
		
		$template->set_filenames(array('body' => 'asuransi/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_asuransi.'.$phpEx).'">Asuransi</a> | <a href="'.append_sid('pengaturan_asuransi.'.$phpEx."?mode=edit&id=$id").'">Ubah Asuransi</a> ',
			 'JUDUL'		=>'Ubah Data Asuransi',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'ID_PLAN_ASURANSI' => $row['IdPlanAsuransi'],
			 'NAMA_PLAN'  => $row['NamaPlan'],
			 'BESAR_PREMI'=> $row['BesarPremi'],
			 'KETERANGAN'	=> $row['Keterangan'],
			 'U_ASURANSI_ADD_ACT'=>append_sid('pengaturan_asuransi.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus asuransi
		$list_asuransi = str_replace("\'","'",$HTTP_GET_VARS['list_asuransi']);
		//echo($list_asuransi. " asli :".$HTTP_GET_VARS['list_asuransi']);
		$Asuransi->hapus($list_asuransi);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'asuransi/asuransi_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE NamaPlan LIKE '%$cari%' 
				OR Keterangan LIKE '%$cari%' 
				OR BesarPremi LIKE '%$cari%'";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdPlanAsuransi","tbl_md_plan_asuransi","&cari=$cari",$kondisi,"pengaturan_asuransi.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *
			FROM tbl_md_plan_asuransi $kondisi 
			ORDER BY NamaPlan LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_asuransi.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama_plan'=>$row['NamaPlan'],
							'besar_premi'=>number_format($row['BesarPremi'],0,",","."),
							'keterangan'=>$row['Keterangan'],
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load asuransi',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_asuransi.'.$phpEx).'">Asuransi</a>',
			'U_ASURANSI_ADD'		=> append_sid('pengaturan_asuransi.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_asuransi.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>