<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

//PAGING
$idx_page 	= ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:$HTTP_POST_VARS['page'];
$idx_page 	= ($idx_page!='')?$idx_page:0;
$cari				= $HTTP_POST_VARS["txt_cari"]!=""?$HTTP_POST_VARS["txt_cari"]:$HTTP_GET_VARS["cari"];

$Jurusan= new Jurusan();
$Cabang	= new Cabang();
$Jadwal	= new Jadwal();

function setComboCabangAsal($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] ($row[Kota]) ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;
			
	$result=$Jurusan->ambilDataByKodeCabangAsal($cabang_asal);
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboJadwal($id_jurusan,$jadwal_dipilih){
	//SET COMBO jurusan
	global $db;
	global $Jadwal;
			
	$result=$Jadwal->setComboJadwal("",$id_jurusan);
	
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($jadwal_dipilih!=$row['KodeJadwal'])?"":"selected";
			$opt_jadwal .="<option value='$row[KodeJadwal]' $selected>$row[KodeJadwal] $row[JamBerangkat]</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}
	
	return $opt_jadwal;
	//END SET COMBO JURUSAN
}

function setComboJam($jam_dipilih){
	$opt_jam="";
		
		for($jam=0;$jam<24;$jam++){
			$str_jam=substr("0".$jam,-2);
			
			$selected=($jam_dipilih!=$str_jam)?"":"selected";
			$opt_jam .="<option $selected value=$str_jam>$str_jam</option>";
		}
		
		return $opt_jam;
}

function setComboMenit($menit_dipilih){
	$opt_menit="";
	
	for($menit=0;$menit<60;$menit +=5){
		$str_menit	= substr("0".$menit,-2);
		$selected=($menit_dipilih!=$str_menit)?"":"selected";
		$opt_menit .="<option $selected value=$str_menit>$str_menit</option>";
	}
	
	return $opt_menit;
}

function setHariAktif($obj_template, $hari_dipilih){
  //LOOP HARI AKTIF

  $nama_hari  = array("Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
	if($hari_dipilih != ""){
		$list_hari_dipilih = explode(",",$hari_dipilih);
	}else{
		$list_hari_dipilih = array();
	}

  for($idx_hari=0;$idx_hari<=6;$idx_hari++) {
    $obj_template->
    assign_block_vars(
      'HARI_AKTIF',
      array(
        'id'      => $idx_hari,
        'nama'    => $nama_hari[$idx_hari],
        'checked' => (in_array($idx_hari,$list_hari_dipilih)?"checked":"")
      )
    );
  }
}

$mode	= $mode==""?"explore":$mode;

//BODY 
	switch($mode){
		case 'explore':
      $is_aktif	  = $HTTP_POST_VARS["isaktif"]!=""?$HTTP_POST_VARS["isaktif"]:$HTTP_GET_VARS["isaktif"];
      $is_online	= $HTTP_POST_VARS["isonline"]!=""?$HTTP_POST_VARS["isonline"]:$HTTP_GET_VARS["isonline"];

      // LIST
      $template->set_filenames(array('body' => 'jadwal/jadwal_body.tpl'));

      $temp_cari=str_replace("asal=","",$cari);
      if($temp_cari==$cari){
        $kondisi_asal = "";
      }
      else{
        $kondisi_asal = "AND tmc1.Nama LIKE '%$temp_cari%' ";
        $cari=$temp_cari;
      }

      $temp_cari=str_replace("tujuan=","",$cari);
      if($temp_cari==$cari){
        $kondisi_tujuan = "";
      }
      else{
        $kondisi_tujuan = "AND tmc2.Nama LIKE '%$temp_cari%' ";
        $cari=$temp_cari;
      }

      $kondisi	=($cari=="")?"":
        " AND (KodeJadwal LIKE '%$cari%'
          OR tmc1.Nama LIKE '%$cari%'
          OR tmc2.Nama LIKE '%$cari%')
          $kondisi_asal
          $kondisi_tujuan ";

      $kondisi_aktif  = $is_aktif==""?"":" AND tmj.FlagAktif='$is_aktif'";
      $kondisi_online = $is_online==""?"":" AND tmj.IsOnline='$is_online'";

      //PAGING======================================================
      $paging		= pagingData($idx_page,"KodeJadwal","((tbl_md_jadwal tmj LEFT JOIN tbl_md_jurusan tmjr ON tmj.IdJurusan=tmjr.IdJurusan)
          LEFT JOIN tbl_md_cabang tmc1 ON tmc1.KodeCabang=tmjr.KodeCabangAsal)
          LEFT JOIN tbl_md_cabang tmc2 ON tmc2.KodeCabang=tmjr.KodeCabangTujuan","&cari=$cari&isaktif=$is_aktif&isonline=$is_online","WHERE 1 $kondisi $kondisi_aktif $kondisi_online","pengaturan_jadwal.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
      //END PAGING======================================================

      $sql =
        "SELECT KodeJadwal,tmj.IdJurusan,
          tmc1.Nama as asal,
          tmc2.Nama as tujuan,
          JamBerangkat,JumlahKursi,FlagSubJadwal,KodeJadwalUtama,
          IsExpress,tmj.FlagAktif,tmj.IsOnline
        FROM ((tbl_md_jadwal tmj LEFT JOIN tbl_md_jurusan tmjr ON tmj.IdJurusan=tmjr.IdJurusan)
          LEFT JOIN tbl_md_cabang tmc1 ON tmc1.KodeCabang=tmjr.KodeCabangAsal)
          LEFT JOIN tbl_md_cabang tmc2 ON tmc2.KodeCabang=tmjr.KodeCabangTujuan
        WHERE 1 $kondisi $kondisi_aktif $kondisi_online
        ORDER BY KodeJadwal LIMIT $idx_awal_record,$VIEW_PER_PAGE";

      $idx_check=0;


      if (!$result = $db->sql_query($sql)){
         //die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
         echo("Error :".__LINE__);exit;
      }

      $i = $idx_page*$VIEW_PER_PAGE+1;
      while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
          $odd = 'even';
        }

        if($row['FlagAktif']){
          $status="<a href='' onClick='return ubahStatus(\"$row[KodeJadwal]\")'>Aktif</a>";
        }
        else{
          $odd	= "red";
          $status="<a href='' onClick='return ubahStatus(\"$row[KodeJadwal]\")'>Tidak Aktif</a>";
        }

        $idx_check++;

        $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[KodeJadwal]'\"/>";

        $kode_jadwal_utama="";
        if($row['FlagSubJadwal']==1){
          $kode_jadwal_utama	= " <font color='".($odd!="red"?"red":"white")."'>($row[KodeJadwalUtama])</font>";
        }

        //'kursi'=>substr($row['JumlahKursi'],0,2)."-".substr($row['JumlahKursi'],2,1),
        $index_layout	= substr($row['JumlahKursi'],2,1);
        $layout_kursi	= $index_layout==""?substr($row['JumlahKursi'],0,2):substr($row['JumlahKursi'],0,2).chr(64+substr($row['JumlahKursi'],2,1));

        $act 	="<a href='".append_sid('pengaturan_jadwal.'.$phpEx.'?mode=edit&cari='.$cari.'&page='.$idx_page.'&id='.$row[0])."'>Edit</a> + ";
        $act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";

        if($userdata["user_level"]!=$LEVEL_ADMIN){
          $act_online = $row["IsOnline"]==1?"Online":"Offline";
        }
        else{
          $act_online = "<a href='#' onclick='ubahOnline(\"$row[KodeJadwal]\");'>".($row["IsOnline"]==1?"Online":"Offline")."</a>";
        }


        $template->
          assign_block_vars(
            'ROW',
            array(
              'odd'=>$odd,
              'check'=>$check,
              'no'=>$i,
              'kode'=>"<b>".$row['KodeJadwal']."</b>".$kode_jadwal_utama,
              'jurusan'=>$row['asal']."-".$row['tujuan'].($row['IsExpress']==0?"":" <b>[JADWAL EXPRESS]</b>"),
              'jam'=>$row['JamBerangkat'],
              'kursi'=>$layout_kursi,
              'aktif'=>$status,
              'online'=>$act_online,
              'classonline'=>($row["IsOnline"]==1?"green":""),
              'action'=>$act
            )
          );

        $i++;
      }

      if($i-1<=0){
        $no_data	=	"<tr><td colspan=8 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
      }


      //CONTROL SHOW SET ONLINE/OFFLINE
      if($userdata["user_level"]==$LEVEL_ADMIN){
        $template->assign_block_vars('SET_ONLINE',array());
      }

      $template->assign_vars(array(
        'BCRUMP'    		              => '<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx).'">Jadwal</a>',
        'U_JADWAL_ADD'		            => append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add&cari='.$cari.'&page='.$idx_page),
        'ACTION_CARI'		              => append_sid('pengaturan_jadwal.'.$phpEx),
        'TXT_CARI'			              => $cari,
        'NO_DATA'				              => $no_data,
        'PAGING'				              => $paging,
        'IS_ONLINE'.$is_online        => "selected",
        'IS_AKTIF'.$is_aktif          => "selected"
        )
      );
		break;
	
		case 'add':
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];

		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}

    setHariAktif($template,"0,1,2,3,4,5,6");
		
		$template->set_filenames(array('body' => 'jadwal/add_body.tpl')); 
		$template->assign_vars(array(
			'BCRUMP'			=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?page='.$idx_page.'&cari='.$cari).'">Jadwal</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add&page='.$idx_page.'&cari='.$cari).'">Ubah Jadwal</a> ',
			'JUDUL'				=>'Tambah Data Jadwal',
			'MODE'   			=> 'save',
			'SUB'    			=> '0',
			'OPT_ASAL'		=> setComboCabangAsal(""),
			'OPT_JAM' 		=> setComboJam("00"),
			'OPT_MENIT'		=> setComboMenit("00"),
			'OPT_KURSI'		=> setComboLayoutKursi(10),
			'OPT_SUB_ASAL'=> setComboCabangAsal(""),
			'IS_EXPRESS'	=> "",
			'SUB_JADWAL'	=> 'none',
			'PESAN'				=> $pesan,
			'BGCOLOR_PESAN'=> $bgcolor_pesan,
			'U_JADWAL_ADD_ACT'=> append_sid('pengaturan_jadwal.'.$phpEx),
			'CARI'				=> $cari,
			'PAGE'				=> $idx_page
		 )
		);
		break;
	
		case 'save':
		// aksi menambah jadwal
		$kode_jadwal  			= str_replace(" ","",$HTTP_POST_VARS['kode_jadwal']);
		$kode_jadwal_old		= str_replace(" ","",$HTTP_POST_VARS['kode_jadwal_old']);
		$id_jurusan					= $HTTP_POST_VARS['opt_tujuan'];
		$asal								= $HTTP_POST_VARS['opt_asal'];
		$jam_berangkat			= $HTTP_POST_VARS['opt_jam'];
		$menit_berangkat  	= $HTTP_POST_VARS['opt_menit'];
		$kursi   						= $HTTP_POST_VARS['opt_kursi'];
		$flag_sub_jadwal		= $HTTP_POST_VARS['flag_sub_jadwal'];
		$sub_id_jurusan			= $HTTP_POST_VARS['opt_tujuan_asal'];
		$sub_asal						= $HTTP_POST_VARS['opt_sub_asal'];
		$kode_jadwal_utama	= $HTTP_POST_VARS['kode_jadwal_utama'];
		$is_express					= $HTTP_POST_VARS['isexpress']==""?0:1;
		$status_aktif   		= $HTTP_POST_VARS['aktif'];
    $list_hari_aktif    = $HTTP_POST_VARS['listhariaktif'];

    for($i=0;$i<count($list_hari_aktif);$i++){
      $hari_aktif .= $list_hari_aktif[$i].",";
    }

    $hari_aktif = substr($hari_aktif,0,-1);

		$terjadi_error=false;
		
		$waktu_berangkat=$jam_berangkat.":".$menit_berangkat;
		
		if($Jadwal->periksaDuplikasi($kode_jadwal) && $kode_jadwal!=$kode_jadwal_old){
			$pesan="<font color='white' size=3>Kode jadwal yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else if($flag_sub_jadwal==1 && $kode_jadwal_utama==""){
			$pesan="<font color='white' size=3>Anda belum memilih jadwal utama!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Jadwal";
				$path	='<a href="'.append_sid('pengaturan_jadwal.'.$phpEx."?mode=add&cari=".$cari."&page=".$idx_page).'">Tambah Jadwal</a> ';
				
				if($Jadwal->tambah($kode_jadwal,$id_jurusan,$waktu_berangkat,$kursi,$flag_sub_jadwal,$kode_jadwal_utama,$is_express,$status_aktif,$hari_aktif)){
						
					redirect(append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data jadwal Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
				}
			}
			else{
				
				$judul="Ubah Data Jadwal";
				$path	='<a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=edit&id='.$kode_jadwal_old.'&cari='.$cari.'&page='.$idx_page).'">Ubah Jadwal</a> ';
				
				if($Jadwal->ubah($kode_jadwal_old, $kode_jadwal, $id_jurusan, $waktu_berangkat, $kursi,$flag_sub_jadwal,$kode_jadwal_utama,$is_express,$status_aktif,$hari_aktif)){
						
					//redirect(append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data jadwal Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=edit&id='.$kode_jadwal).'">Sini</a> Untuk Melanjutkan','');
					
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}
			}
			
			//exit;
			
		}

    setHariAktif($template,$hari_aktif);

		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";
		
		$temp_var_sub_jadwal	="sub_jadwal_".$flag_sub_jadwal;
		$$temp_var_sub_jadwal	="selected";
		
		$template->set_filenames(array('body' => 'jadwal/add_body.tpl')); 
		$template->assign_vars(array(
				'BCRUMP'	=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?cari='.$cari.'&page='.$idx_page).'">Jadwal</a> | '.$path,
				'JUDUL'	=>$judul,
				'MODE'   => 'save',
				'SUB'    => $submode,
				'KODE_JADWAL_OLD'	=> $kode_jadwal_old,
				'KODE_JADWAL'   	=> $kode_jadwal,
				'ASAL'						=> $asal,
				'OPT_ASAL'				=> setComboCabangAsal($asal),
				'ID_JURUSAN'			=> $id_jurusan,
				'OPT_JAM' 				=> setComboJam($jam_berangkat),
				'OPT_MENIT'				=> setComboMenit($menit_berangkat),
				'OPT_KURSI'				=> setComboLayoutKursi($kursi),
				'SUB_JADWAL_0'		=> $sub_jadwal_0,
				'SUB_JADWAL_1'		=> $sub_jadwal_1,
				'SUB_JADWAL'			=> ($flag_sub_jadwal!=1)?'none':'yes',
				'OPT_SUB_ASAL'		=> setComboCabangAsal($sub_asal),
				'SUB_ASAL'				=> $sub_asal,
				'SUB_ID_JURUSAN'	=> $sub_id_jurusan,
				'KODE_SUB_JADWAL'	=> $kode_jadwal_utama,
				'OPT_SUB_JADWAL'	=> setComboJadwal($id_jurusan,$kode_jadwal_utama),
				'AKTIF_1'					=> $status_aktif_1,
				'AKTIF_0'					=> $status_aktif_0,
				'IS_EXPRESS'			=> $is_express==0?"":"checked",
				'PESAN'						=> $pesan,
				'BGCOLOR_PESAN'		=> $bgcolor_pesan,
				'U_JADWAL_ADD_ACT'=>append_sid('pengaturan_jadwal.'.$phpEx),
				'CARI'						=> $cari,
				'PAGE'						=> $idx_page
			 )
		);
		
	break;

	case 'edit':
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row	= $Jadwal->ambilDataDetail($id);
		
		if($row['FlagSubJadwal']==1){
			$row_sub				= $Jadwal->ambilDataDetail($row['KodeJadwalUtama']);
			$opt_sub_asal		= setComboCabangAsal($row_sub['Asal']);
			$opt_sub_jadwal	= setComboJadwal($row_sub['IdJurusan'],$row['KodeJadwalUtama']);
		}

    setHariAktif($template,$row["HariAktif"]);

		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";
		
		$temp_var_sub_jadwal	="sub_jadwal_".$row['FlagSubJadwal'];
		$$temp_var_sub_jadwal	="selected";
		
		$template->set_filenames(array('body' => 'jadwal/add_body.tpl')); 
		$template->assign_vars(array(
				'BCRUMP'	=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?page='.$idx_page.'&cari='.$cari).'">Jadwal</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=edit&id='.$id.'&page='.$idx_page.'&cari='.$cari).'">Ubah Jadwal</a> ',
				'JUDUL'	=>'Ubah Data Jadwal',
				'MODE'   => 'save',
				'SUB'    => '1',
				'KODE_JADWAL_OLD'	=> $row['KodeJadwal'],
				'KODE_JADWAL'   	=> $row['KodeJadwal'],
				'ASAL'						=> $row['Asal'],
				'OPT_ASAL'				=> setComboCabangAsal($row['Asal']),
				'ID_JURUSAN'			=> $row['IdJurusan'],
				'OPT_JAM' 				=> setComboJam(substr($row['JamBerangkat'],0,2)),
				'OPT_MENIT'				=> setComboMenit(substr($row['JamBerangkat'],-2)),
				'OPT_KURSI'				=> setComboLayoutKursi($row['JumlahKursi']),
				'SUB_JADWAL_0'		=> $sub_jadwal_0,
				'SUB_JADWAL_1'		=> $sub_jadwal_1,
				'SUB_JADWAL'			=> ($row['FlagSubJadwal']!=1)?'none':'yes',
				'OPT_SUB_ASAL'		=> setComboCabangAsal($row_sub['Asal']),
				'SUB_ASAL'				=> $row_sub['Asal'],
				'SUB_ID_JURUSAN'	=> $row_sub['IdJurusan'],
				'KODE_SUB_JADWAL'	=> $row['KodeJadwalUtama'],
				'OPT_SUB_JADWAL'	=> $opt_sub_jadwal,
				'AKTIF_1'					=> $status_aktif_1,
				'AKTIF_0'					=> $status_aktif_0,
				'IS_EXPRESS'			=> $row['IsExpress']==0?"":"checked",
				'U_JADWAL_ADD_ACT'=>append_sid('pengaturan_jadwal.'.$phpEx),
				'CARI'						=> $cari,
				'PAGE'						=> $idx_page
			 )
		);
	break;

	case 'delete':
		// aksi hapus jadwal
		$list_jadwal = str_replace("\'","'",$HTTP_GET_VARS['list_jadwal']);
		//echo($list_jadwal. " asli :".$HTTP_GET_VARS['list_jadwal']);
		$Jadwal->hapus($list_jadwal);
		
	exit;
	
	case 'setaktifjadwal':
		// aksi hapus jadwal
		$list_jadwal 	= str_replace("\'","'",$HTTP_GET_VARS['list_jadwal']);
		$is_aktif			= $HTTP_GET_VARS['isaktif'];
		
		//echo($list_jadwal. " asli :".$HTTP_GET_VARS['list_jadwal']);
		$Jadwal->setStatusAktif($is_aktif,$list_jadwal);
	exit;
	
	case 'ubahstatus':
		$kode_jadwal = str_replace("\'","'",$HTTP_GET_VARS['kode_jadwal']);
	
		$Jadwal->ubahStatusAktif($kode_jadwal);
		
	exit;

  case 'ubahonline':
    $kode_jadwal = str_replace("\'","'",$HTTP_GET_VARS['kode_jadwal']);
    $Jadwal->ubahStatusOnline($kode_jadwal);
  exit;

  case 'setonlinejadwal':
    // aksi hapus jadwal
    $list_jadwal 	= str_replace("\'","'",$HTTP_GET_VARS['list_jadwal']);
    $is_aktif			= $HTTP_GET_VARS['isaktif'];

    //echo($list_jadwal. " asli :".$HTTP_GET_VARS['list_jadwal']);
    $Jadwal->setStatusOnline($is_aktif,$list_jadwal);
  exit;

	case 'get_tujuan':
		$cabang_asal		= $HTTP_GET_VARS['asal'];
		$id_jurusan			= $HTTP_GET_VARS['jurusan'];
		
		if($cabang_asal==''){
			$result	= $Cabang->ambilData("","Nama,Kota","ASC LIMIT 0,1");
							
			if($result){
				while ($row = $db->sql_fetchrow($result)){
					$cabang_asal = $row['KodeCabang'];
				}
			}
			else{
				echo("Error :".__LINE__);exit;
			}		
		}
		
		$opt_cabang_tujuan=
			"<select id='opt_tujuan' name='opt_tujuan'>".
			setComboCabangTujuan($cabang_asal,$id_jurusan)
			."</select>";
		
		echo($opt_cabang_tujuan);
		
	exit;
	
	case 'get_tujuan_utama':
		$cabang_asal		= $HTTP_GET_VARS['asal'];
		$id_jurusan			= $HTTP_GET_VARS['jurusan'];
		
		if($cabang_asal==''){
			$result	= $Cabang->ambilData("","Nama,Kota","ASC LIMIT 0,1");
							
			if($result){
				while ($row = $db->sql_fetchrow($result)){
					$cabang_asal = $row['KodeCabang'];
				}
			}
			else{
				echo("Error :".__LINE__);exit;
			}		
		}
		
		$opt_cabang_tujuan=
			"<select id='opt_tujuan_asal' name='opt_tujuan_asal' onClick='getUpdateJadwal(this.value)'>".
			setComboCabangTujuan($cabang_asal,$id_jurusan)
			."</select>";
		
		echo($opt_cabang_tujuan);
		
	exit;
	
	case 'get_jadwal':
		$kode_jadwal		= $HTTP_GET_VARS['kode_jadwal'];
		$id_jurusan			= $HTTP_GET_VARS['jurusan'];
		
		$opt_jadwal=
			"<select id='kode_jadwal_utama' name='kode_jadwal_utama'>".
			setComboJadwal($id_jurusan,$kode_jadwal)
			."</select>";
		
		echo($opt_jadwal);
		
	exit;

}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>