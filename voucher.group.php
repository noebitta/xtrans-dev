<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassVoucherDiskon.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$Voucher	= new Voucher();

$mode	= $mode==""?"exp":$mode;

$bcrump		= "<a href=\"".append_sid('main.'.$phpEx) ."#voucherdiskon\">Home</a> | <a href=\"".append_sid(basename(__FILE__))."\">Voucher Diskon Korporat</a>";

switch($mode){
	
	case "exp":
		// LIST
		
		$cari = isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
		
		$kondisi	=($cari=="")?"":
			" AND(NamaGroup LIKE '%$cari%' 
				OR Deskripsi LIKE '%$cari%')";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"Id","tbl_voucher_group","&cari=$cari","WHERE 1 $kondisi",basename(__FILE__),$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *
			FROM tbl_voucher_group
			WHERE 1 $kondisi 
			ORDER BY NamaGroup LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['IsAktif']){
					$status="<a href='' onClick='return ubahStatus(\"$row[Id]\")'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[Id]\")'>Tidak Aktif</a>";
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act  ="<a href='".append_sid('voucher.group.batchvoucher.php?idgroup='.$row[0])."'>Voucher</a> | ";
				$act .="<a href='".append_sid(basename(__FILE__).'?mode=edit&id='.$row[0])."'>Edit</a> | ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'namagrup'=>$row['NamaGroup'],
							'deskripsi'=>$row['Deskripsi'],
							'settlement'=>($row['IsSettlement']==1?"YA":"TIDAK"),
							'aktif'	=>$status,
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$template->assign_block_vars("NO_DATA",array());
			}
		} 
		else{
			echo("Error :".__LINE__);exit;
		}
		
		$template->set_filenames(array('body' => 'voucher.group/index.tpl')); 
		$template->assign_vars(array(
			'BCRUMP'    		=> $bcrump,
			'U_ADD'					=> append_sid(basename(__FILE__)."?mode=add"),
			'ACTION_CARI'		=> append_sid(basename(__FILE__)),
			'CARI'					=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
	break;
	
	case "add":
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'voucher.group/detail.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		=>$bcrump." | <a href=\"".append_sid(basename(__FILE__)."?mode=add")."\">Tambah Korporat</a>",
		 'JUDUL'		=>'Tambah Data Grup',
		 'MODE'   	=> 'save',
		 'PESAN'		=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_ADD'		=> append_sid(basename(__FILE__))
		 )
		);
	break;

	case "save":
		// aksi menambah
		$id  					= $HTTP_POST_VARS['id'];
		$nama_group   = $HTTP_POST_VARS['namagroup'];
		$deskripsi   	= $HTTP_POST_VARS['deskripsi'];
		$is_settlement= $HTTP_POST_VARS['issettlement'];
		$is_aktif			= $HTTP_POST_VARS['isaktif'];
		
		$terjadi_error=false;
		
		if($id==""){
			$judul="Tambah Data Grup";
			$path	='<a href="'.append_sid(basename(__FILE__)."?mode=add").'">Tambah Grup</a> ';
			
			if($Voucher->tambahGroup($nama_group,$deskripsi,$is_settlement,$is_aktif)>0){
				
				redirect(append_sid(basename(__FILE__).'?mode=add&pesan=1',true));
				
			}
		}
		else{
			$judul="Ubah Data Grup";
			$path	='<a href="'.append_sid(basename(__FILE__)."?mode=edit&id=$id").'">Ubah Grup</a> ';
			
			if($Voucher->ubahGroup($nama_group,$deskripsi,$is_settlement,$is_aktif,$id)){
				
				$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
				$bgcolor_pesan="98e46f";
				
			}
		}
				
		$template->set_filenames(array('body' => 'voucher.group/detail.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'									=> $bcrump." | ".$path,
			 'JUDUL'									=> $judul,
			 'MODE'   								=> 'save',
			 'ID' 										=> $id,
			 'NAMA_GRUP'    					=> $nama_group,
			 'DESKRIPSI'   						=> $deskripsi,
			 'SETSEL_'.$is_settlement	=> "selected",
			 'AKTIF_'.$is_aktif				=> "selected",
			 'PESAN'									=> $pesan,
			 'BGCOLOR_PESAN'					=> $bgcolor_pesan,
			 'U_ADD'									=> append_sid(basename(__FILE__))
			)
		);
	
	break;

	case "edit":
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Voucher->getDetailGroup($id);
		
		$template->set_filenames(array('body' => 'voucher.group/detail.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'												=> $bcrump." | <a href=\"".append_sid(basename(__FILE__)."?mode=edit&id=$id")."\">Ubah Korporat</a>",
			 'JUDUL'												=>'Ubah Data Group',
			 'MODE'   											=> 'save',
			 'ID' 													=> $row['Id'],
			 'NAMA_GRUP'    								=> $row['NamaGroup'],
			 'DESKRIPSI'   									=> $row['Deskripsi'],
			 'SETSEL_'.$row['IsSettlement']	=> "selected",
			 'AKTIF_'.$row['IsAktif']				=> "selected",
			 'U_ADD'												=>append_sid(basename(__FILE__))
			 )
		);
	break;

	case "delete":
		// aksi hapus
		$list_id = str_replace("\'","'",$HTTP_GET_VARS['listid']);
		$Voucher->hapusGroup($list_id);
		
	exit;
	
	case "ubahstatus":
		// aksi hapus jadwal
		$id = $HTTP_GET_VARS['id'];
	
		$Voucher->ubahStatusAktifGroup($id);
		
	exit;
		
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>