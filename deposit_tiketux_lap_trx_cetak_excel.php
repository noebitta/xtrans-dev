<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 


// PARAMETER
$perpage 		= $config['perpage'];

$fil_status 		= $HTTP_GET_VARS['status'];
$tanggal_mulai  = $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir  = $HTTP_GET_VARS['tanggal_akhir'];
$cari						= $HTTP_GET_VARS["cari"];

$kondisi_cari	= $cari==""?"":" AND (Keterangan LIKE '%$cari%')";

if($filstatus!=""){
	switch($filstatus){
		case "0"	:
			//krebit
			$kondisi_status	= " AND IsDebit=0 ";
			break;
		case "1"	:
			//Debit
			$kondisi_status	= " AND IsDebit=1 ";
			break;
	}
}

$kondisi_tanggal	= " AND (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai' AND '$tanggal_akhir')";

$total_debit	= 0;
$total_kredit	= 0; 

//QUERY
$sql	= 
	"SELECT *
	FROM tbl_deposit_log_trx
	WHERE 1 $kondisi_cari $kondisi_tanggal $kondisi_status
	ORDER BY ID";
	
if ($result = $db->sql_query($sql)){
		
	$i=1;
	
	$objPHPExcel = new PHPExcel();          
  $objPHPExcel->setActiveSheetIndex(0);  
  $objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
  $objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
  
	//HEADER
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Transaksi Deposit Tiketux per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir));
	$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Filter Jenis Transaksi: '.$status);
	$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Waktu Transaksi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Keterangan');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Jumlah');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Jenis Trx');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Saldo');
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$idx=0;
	
	while ($row = $db->sql_fetchrow($result)){
		$idx++;
		$idx_row=$idx+4;
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['Keterangan']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Jumlah']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['IsDebit']==1?"DB":"CR");
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Saldo']);
		
		$total_debit	+= $row['IsDebit']==1?$row['Jumlah']:0;
		$total_kredit += $row['IsDebit']==0?$row['Jumlah']:0;
		
	}
	$temp_idx=$idx_row;
	$idx_row++;		
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Trx Debit');
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $total_debit);
	$idx_row++;		
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Trx Kredit');
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $total_kredit);
	
		
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
  
	if ($idx>0){
		header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Transaksi Deposit Tiketux per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output'); 
	}
}
else{
	die_error('Err:',__LINE__);
}   


?>
