<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_SUPERVISOR_PAKET,$LEVEL_STAFF_KEUANGAN_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Cabang	= new Cabang();

//BODY 
if ($mode=='get_asal'){

	$kota				= $HTTP_GET_VARS['kota'];
	$cabang_asal= $HTTP_GET_VARS['asal'];
	
	$opt_cabang_tujuan=
		"<select id='opt_cabang_asal' name='opt_cabang_asal' onChange='getUpdateTujuan(this.value)'>".
		setComboCabangAsal($kota,$cabang_asal)
		."</select>";
	
	echo($opt_cabang_tujuan);
	
	exit;
}
else if ($mode=='get_tujuan'){
	$cabang_asal		= $HTTP_GET_VARS['asal'];
	$id_jurusan			= $HTTP_GET_VARS['jurusan'];
	
	$opt_cabang_tujuan=
		"<select id='opt_tujuan' name='opt_tujuan' onChange='getUpdateJam(this.value)'>".
		setComboCabangTujuan($cabang_asal,$id_jurusan)
		."</select>";
	
	echo($opt_cabang_tujuan);
	
	exit;
}
else if ($mode=='get_jam'){
	$id_jurusan	= $HTTP_GET_VARS['jurusan'];
	$jam_dipilih= $HTTP_GET_VARS['jam'];
	
	$opt_cabang_jam=
		"<select id='opt_jam' name='opt_jam'>".
		setComboJam($id_jurusan,$jam_dipilih)
		."</select>";
	
	echo($opt_cabang_jam);
	
	exit;
}
else {
	// LIST
	$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
	$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
	$tujuan  				= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
	$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 
	
	$tgl_awal					= ($tgl_awal!='')?$tgl_awal:dateD_M_Y();
	$tgl_akhir				= ($tgl_akhir!='')?$tgl_akhir:dateD_M_Y();
	$tgl_awal_mysql		= FormatTglToMySQLDate($tgl_awal);
	$tgl_akhir_mysql	= FormatTglToMySQLDate($tgl_akhir);
	//end pemgaturan periode

	//pengaturan pemilihan tabel pencarian
	$tbl_pencarian	= "tbl_paket";
	//end pengatran pemilihan tabel pencarian
	
	$template->set_filenames(array('body' => 'laporan.paket/paket.belumdiambil.tpl')); 
	
	$kondisi	= " WHERE (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND FlagBatal!=1 AND IdJurusan AND CetakTiket=1  AND StatusDiambil=0";
	
	$kondisi	.= ($cari=="")?"":" AND (NoTiket LIKE '%$cari%' OR NamaPengirim LIKE '%$cari%' OR AlamatPengirim LIKE '%$cari%' OR TelpPengirim LIKE '%$cari%' OR NamaPenerima LIKE '%$cari%' OR AlamatPenerima LIKE '%$cari%' OR TelpPenerima LIKE '%$cari%' OR KodeJadwal LIKE '$cari%')";
	
	if($tujuan!=""){
		$kondisi	.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) LIKE '$tujuan' ";
	}	
	
	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging		= pagingData($idx_page,"NoTiket",$tbl_pencarian,
							"&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&asal=$asal&tujuan=$tujuan&cari=$cari",
							$kondisi,"laporan.paket.data.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING======================================================

	$sql = 
		"SELECT *
		FROM $tbl_pencarian
		$kondisi 
		ORDER BY TglBerangkat,JamBerangkat,KodeJadwal LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
	$idx_check=0;
	
	
	if (!$result = $db->sql_query($sql)){
		//die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
		echo("Err: ".__LINE__);exit;
	} 
	
	$i = $idx_page*$VIEW_PER_PAGE+1;
		
	while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
						
		if (($i % 2)==0){
			$odd = 'even';
		}

		if($row['NoSPJ']==""){
			$status	= "Belum Dikirim";
		}
		else{
			if($row['IsSampai']==0){
				$status="DIKIRIM";
			}
			else{
				if($row['StatusDiambil']==0){
					$status="SAMPAI";
				}
				else{
					$tatus="DIAMBIL/DITERIMA";
				}
			}
		}

		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'							=>$odd,
					'no'							=>$i,
					'no_tiket'				=>$row['NoTiket'],
					'kode_jadwal'			=>$row['KodeJadwal'],
					'waktu_berangkat'	=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
					'nama_pengirim'		=>$row['NamaPengirim'],
					'alamat_pengirim'	=>$row['AlamatPengirim'],
					'telp_pengirim'		=>$row['TelpPengirim'],
					'nama_penerima'		=>$row['NamaPenerima'],
					'alamat_penerima'	=>$row['AlamatPenerima'],
					'telp_penerima'		=>$row['TelpPenerima'],
					'berat'						=>$row['Berat'],
					'harga'						=>number_format($row['HargaPaket'],0,",","."),
					'diskon'					=>number_format($row['Diskon'],0,",","."),
					'bayar'						=>number_format($row['TotalBayar'],0,",","."),
					'layanan'					=>$row['Layanan'],
					'jenis_bayar'			=>$row['JenisPembayaran']==0?"TUNAI":"LANGGANAN",
					'status'					=>$status
				)
			);
		
		$i++;
	}
	
	
	//KOMPONEN UNTUK EXPORT
	$parameter_cetak	= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir."&opt_bulan=".$bulan."&tahun=".$tahun."&kota_asal=".$kota_asal."&opt_cabang_asal=".$cabang_asal."&opt_tujuan=".$cabang_tujuan."&opt_jam=".$kode_jadwal."&status=".$status."&cari=".$cari;
	$script_cetak_pdf="Start('laporan_pembatalan_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
	$script_cetak_excel="Start('laporan_pembatalan_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
	//END KOMPONEN UNTUK EXPORT	
	
	$template->assign_vars(array(
		'BCRUMP'    		=> '<a href="'.append_sid('main.php').'#laporan_paket">Home</a> | <a href="'.append_sid('laporan.paket.belumdiambil.'.$phpEx).'">Laporan Paket Belum Diambil</a>',
		'CETAK_XL'    	=> $script_cetak_excel,
		'ACTION_CARI'		=> append_sid('laporan.paket.belumdiambil.'.$phpEx),
		'OPT_TUJUAN'		=> $Cabang->setInterfaceComboCabang($asal,$cabang_default),
		'TUJUAN'				=> $tujuan,
		'TGL_AWAL'			=> $tgl_awal,
		'TGL_AKHIR'			=> $tgl_akhir,
		'CARI'					=> $cari,
		'PAGING'				=> $paging
		)
	);
	
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>