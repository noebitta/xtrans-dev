<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act   	 = $HTTP_GET_VARS['act'];
$pesan   = $HTTP_GET_VARS['pesan'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Member		= new Member();

switch($mode){

//TAMPILKAN DATA MUTASI SEMUA MEMBER ==========================================================================================================
case 'tampilkan':
	
	$sortby    	= $HTTP_GET_VARS['sortby'];
	$ascending	= $HTTP_GET_VARS['ascending'];
	$ascending	= ($ascending==0)?"asc":"desc";
	
	//HEADER TABEL
	$hasil ="
		<table width='100%' class='border'>
    <tr>
      <th>#</th>
			
			<th><a href='#' onClick='setOrder(\"no_seri_kartu\")'>
				<font color='white'>No Seri Kartu</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"id_member\")'>
				<font color='white'>ID Member</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"tgl_diubah\")'>
				<font color='white'>Tgl registrasi</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"diubah_oleh\")'>
				<font color='white'>Diregistrasi oleh</font></a></th>
			
			<th width='90'>Aksi</th>
    </tr>";
    
	$result	= $Member->ambilDataKartuBelumAktif($sortby,$ascending);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$action = 
			"<a href='#' onClick='tanyaHapus(\"$row[id_member]\")'>Hapus</a>";
		
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[no_seri_kartu]</td>
      <td class='$odd'>$row[id_member]</td>
      <td class='$odd'>$row[tgl_diubah]</td>
      <td class='$odd'>$row[diubah_oleh]</td>
			<td class='$odd' align='center'>$action</td>
    </tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=6 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

//MENGHAPUS KARTU TERDAFTAR==========================================================================================================
case 'hapus':
	
	$id_member	=	$HTTP_GET_VARS['id_member'];
	
	$Member->hapus($id_member);
	
	echo('berhasil');
	
exit;

}//switch mode

$template->set_filenames(array('body' => 'member_laporan_kartu_belum_aktif.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  	=>$userdata['username'],
   	'BCRUMP'    	=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_laporan_kartu_belum_aktif.'.$phpEx).'">Daftar Kartu Belum Aktif</a>',
		'PESAN'				=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>