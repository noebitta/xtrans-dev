<?php
//
// CETAK TIKET UNTUK LINUX
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassPelanggan.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$layout_kursi		= $HTTP_GET_VARS['layout_kursi'];
$kode_booking		= str_replace("\'","",$HTTP_GET_VARS['kode_booking']);		
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);		
$cetak_tiket		= $HTTP_GET_VARS['cetak_tiket'];		
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];		
$cso						= $userdata['nama'];

function getDiscountGroup($kode_jadwal,$tgl_berangkat){
		
		global $db;
	
	
		
		$sql = "SELECT FlagLuarKota,f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','".FormatMySQLDateToTgl($tgl_berangkat)."') AS HargaTiket
            FROM   tbl_md_jurusan
						WHERE IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal')";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota	= $row['FlagLuarKota'];
		$harga_tiket		= $row['HargaTiket'];
		
    $sql = "SELECT IdDiscount,NamaDiscount,IS_NULL(JumlahDiscount,0) AS JumlahDiscount
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota' AND KodeDiscount='M'";
	  
	  if (!$result = $db->sql_query($sql)){
			return 0;
		}
		
		$row = $db->sql_fetchrow($result);
		
		return $row;
}

$Reservasi= new Reservasi();
$Asuransi	= new Asuransi();

	//MEMERIKSA JIKA KODE TIKET KOSONG, MAKA AKAN DICARI DATA BERDASARKAN KODE BOOKING
	if($cetak_tiket!=1){
		//mengambil data berdasarkan kode booking dan dapat mencetak tiket lebih dari 1 tiket
		$result	= $Reservasi->ambilDataKursiByKodeBooking($kode_booking);
	}
	else{
		//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
		$result	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);
	}
	
	if ($result){
		$template->set_filenames(array('body' => 'tiket.tpl')); 
		
		$list_no_tiket		="";
		$list_kode_booking="";
		
		$sudah_diperiksa	= false;
		
		//MENGAMBIL JUMLAH TIKET YANG DIPESAN
		$jum_pesanan	= @mysql_num_rows($result);
		
		while ($row = $db->sql_fetchrow($result)){
			
			$no_tiket=$row['NoTiket'];
			$kode_booking=$row['KodeBooking'];
			$id_member=$row['IdMember'];
			$nama=$row['Nama'];
			$alamat=$row['Alamat'];
			$telp=$row['Telp'];
			$tanggal=$row['TglBerangkat'];
			$jam=$row['JamBerangkat'];
			$asal=explode("(",$row['NamaAsal']);
			$tujuan=explode("(",$row['NamaTujuan']);
			$harga_tiket=number_format($row['HargaTiket'],0,",",".");
			$discount=number_format($row['Discount'],0,",",".");
			$total_bayar=$row['Total'];
			$bayar=number_format($row['Total'],0,",",".");
			$nomor_kursi=$row['NomorKursi'];
			$operator=$row['NamaCSO'];
			$cetak_tiket=$row['CetakTiket'];
			$jenis_pembayaran=($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
			$kode_jadwal=$row['KodeJadwal'];
			$cso	=($cetak_tiket!=1)?$cso:$row['NamaCSOTiket'];
			
			//MEMBERIKAN DISKON JIKA KURSI YANG DIPESAN LEBIH DARI JUMLAH MINIMUM DISCOUNT GRUP
			if($jum_pesanan>=$JUMLAH_MINIMUM_DISCOUNT_GROUP && ($row['Discount']=='' || $row['Discount']==0)){
				$data_discount_group	= getDiscountGroup($kode_jadwal,$tanggal);
				
				//MENGUBAH DATA DISCOUNT
				$sql = "CALL sp_reservasi_ubah_discount('$no_tiket', 'M', '$data_discount_group[NamaDiscount]','$data_discount_group[JumlahDiscount]');";
				
				if (!$db->sql_query($sql)){
					die_error("Err $this->ID_FILE".__LINE__);
				}
				
				if($data_discount_group['JumlahDiscount']!=''){
					$discount			= number_format($data_discount_group['JumlahDiscount'],0,",",".");
					$total_bayar	= $row['HargaTiket']-$data_discount_group['JumlahDiscount'];
					$bayar				= number_format($total_bayar,0,",",".");
				}
			}
			
			//MENGAMBIL DATA ASURANSI
			$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
			
			if(!$sudah_diperiksa && $cetak_tiket!=1){
				if($id_member==''){
					//MEMERIKSA DATA PENUMPANG, JIKA BELUM PERNAH NAIK, AKAN DIMASUKKAN DALAM TBL PELANGGAN
					$Pelanggan	= new Pelanggan();
					
					if($Pelanggan->periksaDuplikasi($telp)){
						//JIKA SUDAH PERNAH NAIK, FREKWENSI KEBERANGKATAN AKAN DITAMBAHKAN
						
						$Pelanggan->ubah(
							$telp,$telp,
							$telp, $telp ,$nama,
						  "", $tanggal,
						  $userdata['user_id'], $kode_jadwal);
					}
					else{
						//JIKA BELUM PERNAH NAIK , AKAN DITAMBAHKAN
						$Pelanggan->tambah(
							$telp, $telp ,$nama,
						  $alamat, $tanggal, $tanggal,
						  $userdata['user_id'], $kode_jadwal,1,
						  0);
					}
					
					$sudah_diperiksa	= true;
				}
				else{
					$Reservasi->updateFrekwensiMember($id_member,$tanggal);
				}
			}
			
			//$pesanan=(trim($row['pesanan'])==1)?"Pesanan":"Go Show";
			
				
			switch($jenis_pembayaran){
				case 0:
					$ket_jenis_pembayaran="TUNAI";
				break;
				case 1:
					$ket_jenis_pembayaran="DEBIT CARD";
				break;
				case 2:
					$ket_jenis_pembayaran="KREDIT CARD";
				break;
				case 3:
					$ket_jenis_pembayaran="VOUCHER";
				break;
			}
			
			//$no_seri_kartu=(trim($row['no_seri_kartu'])=='')?'':" ***MEMBER*** ".trim($row['no_seri_kartu']);
		
			$list_no_tiket			.="'$no_tiket',";
			$list_kode_booking	.="'$kode_booking',";

			
			$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
			
			//SET PARAMETER TIKET UTK HTML
			$output	 = 
			"<table border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td colspan='3'>
						<div class='tiket_header'>".$data_perusahaan['NamaPerusahaan']."</div>
						<div class='tiket_subheader'>
							Pelopor On Time Shuttle<br>".
							$data_perusahaan['AlamatPerusahaan']."<br>".
							$data_perusahaan['TelpPerusahaan']."
						</div>
					</td>
				</tr>
				<tr><td colspan='3 height='10'>&nbsp;</td></tr>
				<tr class='tiket_header'>
					<td colspan='3'>".
						$no_tiket.'/'.$operator."<br>".
						$ket_jenis_pembayaran."
					</td>
				</tr>
				<tr><td colspan='3 height='10'>&nbsp;</td></tr>
				<tr class='tiket_body'>
					<td>Tgl.Pergi</td><td>:</td><td>&nbsp;".dateparse(FormatMySQLDateToTgl($tanggal))."</td>
				</tr>
				<tr class='tiket_body'>
					<td colspan='3'>".substr($asal[0],0,20)."-".substr($tujuan[0],0,20)."</td>
				</tr>
				<tr class='tiket_body'>
					<td>Jam</td><td>:</td><td>".$jam."</td>
				</tr>
				<tr><td colspan='3 height='10'>&nbsp;</td></tr>
				<tr class='tiket_body'>
					<td>No.Kursi</td><td>:</td><td>".$nomor_kursi."</td>
				</tr>
				<tr><td colspan='3 height='10'>&nbsp;</td></tr>";
							
			if($data_asuransi['IdAsuransi']!=""){
				$output	.=
					"<tr class='tiket_body'>
						<td>Tiket</td><td>:</td><td align='right'>Rp. ".$bayar."</td>
					</tr>
					<tr class='tiket_body'>
						<td>Premi</td><td>:</td><td align='right'>Rp. ".number_format($data_asuransi['BesarPremi'],0,",",".")."</td>
					</tr>
					<tr class='tiket_body'>
						<td>Asuransi</td><td>:</td><td>".$data_asuransi['NamaPlanAsuransi']."</td>
					</tr>";
				$bayar	= number_format($total_bayar + $data_asuransi['BesarPremi'] ,0,",",".");
			}
			
			$output	.=
				"<tr class='tiket_body'>
					<td>Bayar</td><td>:</td><td align='right'>Rp. ".$bayar."</td>
				</tr>
				<tr class='tiket_body'>
					<td>CSO</td><td>:</td><td>".substr($cso,0,24)."</td>
				</tr>
				<tr><td colspan='3 height='10'>&nbsp;</td></tr>";
			
			//PESAN SPONSOR
			
			$pesan_sponsor	= $Reservasi->ambilPesanUntukDiTiket();
			
			$output	.=
				"<tr class='tiket_footer'>
					<td colspan='3'>".
						$pesan_sponsor."<br>
						Terima Kasih
					</td>
				</tr>";
						
			$i++;
		}
		
		//UPDATE FLAG TIKET DI LAYOUT KURSI
		$list_no_tiket	= substr($list_no_tiket,0,-1);
		
		$sql = 
			"UPDATE tbl_posisi_detail SET StatusBayar=1
			WHERE 
			  KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal('$kode_jadwal') 
				AND TGLBerangkat='$tanggal'
				AND NoTiket IN ($list_no_tiket);";
		
		if(!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
		
		//mengupate flag cetak tiket
		if($cetak_tiket!=1){
			$list_kode_booking	= substr($list_kode_booking,0,-1);
			$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
		}
		
		//mengupdate status asuransi menjadi OK
		$Asuransi->ubahFlagBatalAsuransi($list_no_tiket, 0, $userdata['user_id']);
		
		$template->assign_vars(array(
		 'OUTPUT_TIKET'		=>$output
		 )
		);
		$template->pparse('body');
		
	} 
	else{
		//die_error('GAGAL MENGAMBIL DATA',__LINE__,__FILE__,$sql);
		echo("Error :".__LINE__);
		exit;
	}
	
?>