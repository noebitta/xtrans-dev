<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassDeposit.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Deposit	=  new Deposit();

if($mode==""){
	
	//mengambil parameter filter
	$filstatus 			= isset($HTTP_GET_VARS['filterstatus'])? $HTTP_GET_VARS['filterstatus'] : $HTTP_POST_VARS['filterstatus'];
	$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
	$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
	$cari						= $HTTP_POST_VARS["txt_cari"];
	
	// BODY
	$template->set_filenames(array('body' => 'deposit_tiketux/deposit_redbus_body.tpl'));
	
	$kondisi_cari	= $cari==""?"":" AND (KodeReferensi LIKE '%$cari%')";
	
	if($filstatus!=""){
		switch($filstatus){
			case "0"	:
				//Belum diverifikasi
				$kondisi_status	= " AND IsVerified=0 ";
				break;
			case "1"	:
				//sudah diverifikasi
				$kondisi_status	= " AND IsVerified=1 ";
				break;
		}
	}
	
	
	$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
	$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
	$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
	$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
	
	$kondisi_tanggal	= " AND (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')";
	
	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging		= pagingData($idx_page,"ID","tbl_deposit_log_topup_redbus",
	"&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir","","deposit_redbus.php",
	$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING======================================================
	
	//QUERY
	$sql	= 
		"SELECT *,
			f_user_get_nama_by_userid(PetugasTopUp) AS NamaPetugasTopUp,
			IF(IsVerified=0,'',f_user_get_nama_by_userid(PetugasVerifikasi)) AS NamaPetugasVerifikasi
		FROM tbl_deposit_log_topup_redbus
		WHERE 1 $kondisi_cari $kondisi_tanggal $kondisi_status
		ORDER BY ID LIMIT $idx_awal_record,$VIEW_PER_PAGE";

	if (!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	
	$i=0;
	
	//PLOT DATA
	while($row = $db->sql_fetchrow($result)){
		
		$i++;
		
		if($row['IsVerified']==1){
			$odd		= "green";
			$status	= "VERIFIED";
			$action	= "";
		}
		else{
			$odd 		= "yellow";
			$status	= "Need Verify";
			$action	= "<a href='#' onClick='kodereferensi.value=\"".$row['KodeReferensi']."\";kodeverifikasi.value=\"\";dlg_verifikasi.show();return false;'>Verify</a>";
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i+$idx_page*$VIEW_PER_PAGE,
					'waktu_trx'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])),
					'kode_referensi'=>$row['KodeReferensi'],
					'jumlah'=>number_format($row['Jumlah'],0,",","."),
					'user'=>$row['NamaPetugasTopUp'],
					'status'=>$status,
					'verifikator'=>$row['NamaPetugasVerifikasi'],
					'waktu_verifikasi'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuVerifikasi'])),
					'action'=>$action
				)
			);
			
	}
	
	//KOMPONEN UNTUK EXPORT
	$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai_mysql."&tanggal_akhir=".$tanggal_akhir_mysql."&cari=".$cari."&status=".$filstatus."";
														
	$script_cetak_excel="Start('deposit_tiketux_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
	//--END KOMPONEN UNTUK EXPORT
	
	$temp_var		= "opt_status".$filstatus;
	$$temp_var	= "selected";
		
	$template->assign_vars(array(
		'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#tiketux">Home</a> | <a href="'.append_sid('deposit_redbus.'.$phpEx).'">Top Up Deposit</a>',
		'ACTION_CARI'		=> append_sid('deposit_redbus.'.$phpEx),
		'ACT_ADD'				=> append_sid('deposit_redbus.'.$phpEx.'?mode=addtopup'),
		'TXT_CARI'			=> $cari,
		'TGL_AWAL'			=> $tanggal_mulai,
		'TGL_AKHIR'			=> $tanggal_akhir,
		'OPT_STATUS'		=> $opt_status,
		'OPT_STATUS0'		=> $opt_status0,
		'OPT_STATUS1'		=> $opt_status1,
		'PAGING'				=> $paging,
		'CETAK_XL'			=> $script_cetak_excel,
		'SALDO_DEPOSIT'	=> number_format($Deposit->getSaldoDepositRedbus(),0,",",".")
		)
	);
}
elseif($mode=="addtopup"){
	// BODY
	$template->set_filenames(array('body' => 'deposit_tiketux/add_body_redbus.tpl'));
	
	$template->assign_vars(array(
		'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#redbus">Home</a> | <a href="'.append_sid('deposit_redbus.'.$phpEx).'">Top Up Deposit</a>',
		'U_ACT_FORM'    => append_sid('deposit_redbus.'.$phpEx)
		)
	);
	
}
elseif($mode=="addprocess"){
	// BODY
	
	$kode_referensi	=	$Deposit->generateKodeReferensi();
	$kode_otp			 	=  $Deposit->generateKodeOTP();
	$jumlah 				= $HTTP_POST_VARS['jumlah'];

	if($Deposit->tambahRedbus($kode_referensi,$kode_otp,$jumlah,$userdata['user_id'])){
		//berhasil menambahkan data
		
		//KIRIM SMS KE KEUANGAN
		/*$isi_pesan	= "Kode Referensi Top Up Deposit: ".$kode_referensi." dgn Kode Verifikasi: ".$kode_otp;
		$telepon	= "02193275555";
		$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
		$response	= sendHttpPost($sms_config['url'],$parameter);
		
		//KIRIM SMS KE BU TANTI
		$telepon	= "08129200966";
		$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
		$response	= sendHttpPost($sms_config['url'],$parameter);

		//KIRIM SMS TIKETUX
		$telepon= "08170525609";
		$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
		$response	= sendHttpPost($sms_config['url'],$parameter);
		header("location: ".append_sid('deposit_tiketux.php')."&mode=showverify&kodereferensi=$kode_referensi&jumlah=$jumlah");
		*/
		
		if($Deposit->verifikasiRedbus($kode_referensi,$kode_otp)){
			$succeed	= 1;
		}
		else{
			$succeed	= 0;
		}
		
		header("location: ".append_sid('deposit_redbus.php')."&mode=showverify&kodereferensi=$kode_referensi&jumlah=$jumlah&succ=$succeed");
		
		
	}
	else{
		echo("TERJADI KEGAGALAN");
	}
	
}
elseif($mode=="showverify"){

	$kode_referensi	= $HTTP_GET_VARS['kodereferensi'];
	$jumlah					= $HTTP_GET_VARS['jumlah'];
	
	$succeed				= $HTTP_GET_VARS['succ'];
	
	if($succeed==""){
		$display_ver	= "";
		$display_ok		= "none";
		$act_button_ok= "";
		$pesan				= "";
	}
	elseif($succeed=="0"){
		$display_ver	= "";
		$display_ok		= "none";
		$act_button_ok= "";
		$pesan				= "<tr><td colspan=3 align='center'><font size='2' color='red'><b>KODE VERIFIKASI TIDAK BENAR</b></font></td></tr>";
	}
	elseif($succeed=="1"){
		$display_ver	= "none";
		$display_ok		= "";
		$act_button_ok= "window.location='".append_sid("deposit_redbus.php")."';";
		$pesan				= "<tr><td colspan=3 align='center'><font size='2' color='green'><b>TOP UP BERHASI DILAKUKAN</b></font></td></tr>";
	}

	$template->set_filenames(array('body' => 'deposit_tiketux/verify_body_redbus.tpl'));

	$template->assign_vars(array(
		'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#tiketux">Home</a> | <a href="'.append_sid('deposit_redbus.'.$phpEx).'">Top Up Deposit</a>',
		'KODE_REFERENSI'=> $kode_referensi,
		'JUMLAH'				=> number_format($jumlah,0,",","."),
		'JUMLAH_NUM'		=> $jumlah,
		'U_ACT_FORM'    => append_sid('deposit_redbus.php'),
		'DISPLAY_VER'		=> $display_ver,
		'DISPLAY_OK'		=> $display_ok,
		'PESAN'					=> $pesan,
		'ACT_BUTTON_OK'	=> $act_button_ok
		)
	);
}
elseif($mode=="verify"){
	$kode_referensi		= $HTTP_POST_VARS['kodereferensi'];
	$kode_otp					= $HTTP_POST_VARS['kodeverifikasi'];
	$jumlah						= $HTTP_POST_VARS['jumlah'];
	
	if($Deposit->verifikasiRedbus($kode_referensi,$kode_otp)){
		$succeed	= 1;
	}
	else{
		$succeed	= 0;
	}
	
	header("location: ".append_sid('deposit_redbus.php')."&mode=showverify&kodereferensi=$kode_referensi&jumlah=$jumlah&succ=$succeed");
}
elseif($mode=="verifybydialog"){
	$kode_referensi		= $HTTP_POST_VARS['kodereferensi'];
	$kode_otp					= $HTTP_POST_VARS['kodeverifikasi'];
	
	if($Deposit->verifikasiRedbus($kode_referensi,$kode_otp)){
		$succeed	= 1;
	}
	else{
		$succeed	= 0;
	}
	
	echo($succeed);
	exit;
	
}

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>