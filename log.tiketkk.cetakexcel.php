<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= $HTTP_GET_VARS['is_today'];
$tanggal_mulai  = $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir  = $HTTP_GET_VARS['tanggal_akhir'];
$cari						= $HTTP_GET_VARS['cari']; 
$sort_by				= $HTTP_GET_VARS['sort_by'];
$order					= $HTTP_GET_VARS['order'];

$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"TglBerangkat":$sort_by;

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi	=
	"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND PetugasCetakTiket!=0 AND JenisPenumpang='KK'";

$kondisi	.=($cari=="")?"":
	" AND (tr.Nama LIKE '%$cari%'
		OR tr.Telp LIKE '$cari%'
		OR tu.Nama LIKE '%$cari%'
		OR KodeJadwal LIKE '%$cari%')";

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT NoTiket,JamBerangkat,NomorKursi,tr.TglBerangkat,tr.Nama,tr.Telp,tr.KodeJadwal,tu.nama AS NamaPetugasCetak,WaktuCetakTiket
	FROM $tbl_reservasi tr LEFT JOIN tbl_user tu ON tr.PetugasCetakTiket=tu.user_id
	$kondisi
	ORDER BY $sort_by $order";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Tiket Keluarga Karyawan Periode Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Tgl.Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', '#Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Jam');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'KodeJadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', '#Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Cetak Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Tiket Oleh');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

$idx=0;
$idx_row=4;

while ($row = $db->sql_fetchrow($result_laporan)){
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NomorKursi']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, "'".$row['Telp']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])));
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['NamaPetugasCetak']);
	
	$idx_row++;
	$idx++;
}
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Tiket Keluarga Karyawan Periode Tanggal '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
