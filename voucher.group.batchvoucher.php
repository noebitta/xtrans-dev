<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassVoucherDiskon.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$id_group 	= isset($HTTP_GET_VARS['idgroup'])? $HTTP_GET_VARS['idgroup'] : $HTTP_POST_VARS['idgroup'];

$Voucher	= new Voucher();

$mode	= $mode==""?"exp":$mode;

$bcrump	= "<a href=\"".append_sid("main.".$phpEx) ."#voucherdiskon\">Home</a> | <a href=\"".append_sid("voucher.group.php") ."\">Korporat</a> | <a href=\"".append_sid(basename(__FILE__)."?idgroup=$id_group")."\">Batch Voucher</a>";

function setComboCabangTujuan($cabang_asal){
	//SET COMBO cabang
	global $db;
	
	$Jurusan = new Jurusan();
			
	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal);
	$opt_cabang="<option value=''>silahkan pilih...</otpion>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}


switch($mode){
	
	case "exp":
		// LIST
		
		if($id_group==""){
			echo("ILEGAL ACCESS!");
			exit;
		}
		
		$tgl_awal  	= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal'];
		$tgl_akhir  = isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir'];
		$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
				
		$tgl_awal					= ($tgl_awal!='')?$tgl_awal:dateD_M_Y();
		$tgl_akhir				= ($tgl_akhir!='')?$tgl_akhir:dateD_M_Y();
		$tgl_awal_mysql		= FormatTglToMySQLDate($tgl_awal);
		$tgl_akhir_mysql	= FormatTglToMySQLDate($tgl_akhir);
		
		$Cabang	= new Cabang();
		
		$kondisi	=($cari=="")?"":
			" AND (Keterangan LIKE '%$cari%'
			OR NamaPetugasPencetak LIKE '%$cari%')";
			
		$sql = 
			"SELECT BatchId,GROUP_CONCAT(DISTINCT WaktuCetak SEPARATOR ',') AS WaktuCetakBatch,
				GROUP_CONCAT(DISTINCT NamaPetugasPencetak SEPARATOR ',') AS NamaPetugasPencetakBatch,
				GROUP_CONCAT(DISTINCT IsHargaTetap SEPARATOR ',') AS IsHargaTetapBatch,
				GROUP_CONCAT(DISTINCT NilaiVoucher SEPARATOR ',') AS NilaiVoucherBatch,
				GROUP_CONCAT(DISTINCT IsSettlement SEPARATOR ',') AS IsSettlementBatch,
				GROUP_CONCAT(DISTINCT IsBolehWeekEnd SEPARATOR ',') AS IsBolehWeekEndBatch,
				GROUP_CONCAT(DISTINCT ExpiredDate SEPARATOR ',') AS ExpiredDateBatch,
				GROUP_CONCAT(DISTINCT Keterangan SEPARATOR ',') AS KeteranganBatch,
				COUNT(1) AS JumlahVoucher,
				IF(SUM(IsSuspend)>0,1,0) IsSuspend,
				COUNT(IF(WaktuDigunakan IS NULL,NULL,1)) AS JumlahVoucherDigunakan
			FROM tbl_voucher
			WHERE IdGroup='$id_group' $kondisi
			GROUP BY BatchId
			ORDER BY WaktuCetakBatch";
		
		$idx_check=0;
		
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);exit;
		}
		
		$i = $idx_page*$VIEW_PER_PAGE+1;
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
			
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$idx_check++;
			
			$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
			
			$act  ="<a href='".append_sid("voucher.group.listvoucher.exportexcel.php?idgroup=$id_group&waktucetak=".$row['WaktuCetakBatch'])."' class='b_exp_excel' title='Export Daftar Voucher ke MS Excel'>&nbsp;</a> | ";
			$act .="<a href='".append_sid("voucher.group.listvoucher.php?idgroup=$id_group&waktucetak=".$row['WaktuCetakBatch'])."' class='b_browse' title='browse voucher'>&nbsp;</a> | &nbsp;</a>";
			$act .="<a href='#' onClick='suspendVoucher(\"$id_group\",\"$row[BatchId]\",\"$row[IsSuspend]\")' class='b_warning' title='suspend voucher'>&nbsp;</a> | &nbsp;</a>";
			$act .="<a href='#' onClick='hapusVoucher(\"$id_group\",\"$row[BatchId]\");' class='b_delete' title='hapus batch voucher'>&nbsp;</a>";

			$jenis_arr				= explode(",",$row["IsHargaTetapBatch"]);
			$nilai_arr				= explode(",",$row["NilaiVoucherBatch"]);
			$settlement_arr		= explode(",",$row["IsSettlementBatch"]);
			$hari_berlaku_arr	= explode(",",$row["IsBolehWeekEndBatch"]);

			if($nilai_arr[0] < 1 && $nilai_arr[0] != 0){
				$nilai_voucher = ($nilai_arr[0]*100).' %';
			}else{
				$nilai_voucher = 'Rp. '.number_format($nilai_arr[0],0,",",".");
			}

      $odd  = $row['IsSuspend']?"pink":$odd;

			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'check'=>$check,
						'no'=>$i,
						'waktubuat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakBatch'])),
						'dibuatoleh'=>$row['NamaPetugasPencetakBatch'],
						'jenis'=>($jenis_arr[0]==0?"DISKON":"FIXED PRICE"),
						'nilai'=>$nilai_voucher,
						'settlement'=>($settlement_arr[0]==1?"YA":"TIDAK"),
						'hariberlaku'=>($hari_berlaku_arr[0]==1?"ALL DAY":"WEEK DAY"),
						'jumlah'=>number_format($row['JumlahVoucherDigunakan'],0,",",".")."/".number_format($row['JumlahVoucher'],0,",","."),
						'expired'=>dateparse(FormatMySQLDateToTgl($row["ExpiredDateBatch"])),
						'keterangan'=>$row['KeteranganBatch'],
						'action'=>$act
					)
				);
			
			$i++;
		}
		
		if($i-1<=0){
			$template->assign_block_vars("NO_DATA",array());
		}
		
		$data_group	= $Voucher->getDetailGroup($id_group);
		
		$template->set_filenames(array('body' => 'voucher.group/batchvoucher.tpl')); 
		$template->assign_vars(array(
			'BCRUMP'    		=> $bcrump,
			'NAMA_GROUP'		=> $data_group["NamaGroup"],
			'ACTION_CARI'		=> append_sid(basename(__FILE__."?idgroup=$id_group")),
			'ID_GROUP'			=> $id_group,
			'OPT_ASAL'			=> $Cabang->setInterfaceComboCabangByKota(),
			'TGL_AWAL'			=> $tgl_awal,
			'TGL_AKHIR'			=> $tgl_akhir,
			'CARI'					=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
	break;
	
	case 'buatvoucher':
		
		$id_group	= $HTTP_GET_VARS['idgroup'];
		$is_persentase = $HTTP_GET_VARS['ispersentase'];
		$batch_id	= $Voucher->generateBatchId();

		if($is_persentase == 1){
			$nilai_voucher = $HTTP_GET_VARS['nilaivoucher'] / 100;
		}else{
			$nilai_voucher = $HTTP_GET_VARS['nilaivoucher'];
		}

		$data_group	= $Voucher->getDetailGroup($id_group);
		
		$Voucher->setVoucher(
			$batch_id,$HTTP_GET_VARS['idgroup'],$HTTP_GET_VARS['jurusan'],
			$userdata['user_id'],$userdata['nama'],FormatTglToMySQLDate($HTTP_GET_VARS['expireddate']),
			$nilai_voucher,$HTTP_GET_VARS['ishargatetap'],$HTTP_GET_VARS['isbolehweekend'],
			$HTTP_GET_VARS['keterangan'],$data_group['IsSettlement'],$HTTP_GET_VARS['jumlahcetak']);
		
		echo("alert('Voucher telah berhasil dibuat!');window.location.reload();");
		
		exit;

  case 'suspendvoucher':

    $id_group	  = $HTTP_GET_VARS['idgroup'];
    $batch_id	  = $HTTP_GET_VARS['batchid'];
    $is_suspend	= $HTTP_GET_VARS['issuspend'];

    $Voucher->suspendBatchVoucher($id_group,$batch_id,$is_suspend);

    $keterangan = $is_suspend?"Suspend Voucher telah BERHASIL dibuka!":"Voucher telah di SUSPEND!";

    echo("alert('$keterangan');window.location.reload();");

    exit;

  case 'hapusvoucher':

    $id_group	  = $HTTP_GET_VARS['idgroup'];
    $batch_id	  = $HTTP_GET_VARS['batchid'];

    $Voucher->hapusBatchVoucher($id_group,$batch_id);

    echo("alert('Voucher berhasil dihapus!');window.location.reload();");

    exit;
	
	case 'gettujuan':
		$cabang_asal	= $HTTP_GET_VARS['asal'];
		echo("<select id='jurusan' name='jurusan'>".setComboCabangTujuan($cabang_asal)."</select>");
	exit;
			
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>