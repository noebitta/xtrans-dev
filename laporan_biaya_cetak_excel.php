<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_KASIR,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$jenis_biaya  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$cabang					= isset($HTTP_GET_VARS['p7'])? $HTTP_GET_VARS['p7'] : $HTTP_POST_VARS['p7'];

//INISIALISASI
$kondisi	= "WHERE (TglTransaksi BETWEEN CONVERT(datetime,'$tanggal_mulai',105) AND CONVERT(datetime,'$tanggal_akhir',105))";

$kondisi_cabang	=($cabang==0 || $cabang=="")?"":
	" AND KodeCabang = '$cabang' ";

$kondisi_jb	=($jenis_biaya=="")?"":
	" AND FlagJenisBiaya = $jenis_biaya ";

$kondisi_cari	=($cari=="")?"":
	" AND (NoSPJ LIKE '%$cari'
		OR NoPolisi LIKE '%$cari%')";
	
$kondisi	= $kondisi.$kondisi_cabang.$kondisi_jb.$kondisi_cari;

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"TglPesan,JamPesan":$sort_by;
		
//QUERY
$sql=
	"SELECT 
		IdBiayaOP,NoSPJ,FlagJenisBiaya,NoPolisi,dbo.f_SopirGetNamaByKode(KodeSopir) AS NamaSopir,
		CONVERT(DECIMAL,Jumlah) AS Jumlah,
		dbo.f_KodeVoucherGetByNoSPJ(NoSPJ) AS KodeVoucher,
		CONVERT(varchar(12),TglTransaksi,106) as TglTransaksi,
		dbo.f_CabangGetNameByKode(KodeCabang) AS Cabang,
		dbo.f_UserGetNameById(IdPetugas) AS Kasir,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangAsalByIdJurusan(IdJurusan)) Asal,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangTujuanByIdJurusan(IdJurusan)) Tujuan
		
	FROM 
		TbBiayaOP
	$kondisi
	ORDER BY TglTransaksi,NoSPJ,IdBiayaOP";	
	
//EXPORT KE MS-EXCEL
	
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
	  
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Biaya per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
	  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Tgl. Trx');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'No.SPJ');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'No. Polisi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Sopir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jurusan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jenis Biaya');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jumlah');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Cabang');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Kasir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Reff');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		
		$idx=0;
		
		while ($row = $db->sql_fetchrow($result)){
			$idx++;
			$idx_row=$idx+3;
			
			$kode_voucher	=($row['FlagJenisBiaya']!=$LIST_KODE_BIAYA['bbm'])?"":$row['KodeVoucher'];
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['TglTransaksi']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoSPJ']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['NoPolisi']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NamaSopir']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Asal']."-".$row['Tujuan']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $LIST_BIAYA[$row['FlagJenisBiaya']]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['Jumlah']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Cabang']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['Kasir']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $kode_voucher);
			
			
		}
		$temp_idx=$idx_row;
		
		$idx_row++;		
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':G'.$idx_row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
			
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Laporan Biaya per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
  
?>
