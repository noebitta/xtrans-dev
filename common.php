<?php
//
// Common PREPARER 
//
date_default_timezone_set('Asia/Jakarta');
if (!defined('FRAMEWORK'))
{ 
  die("Hacking attempt"); 
}

// #region KEEP OUT
	error_reporting  (E_ERROR | E_WARNING | E_PARSE); 
	set_magic_quotes_runtime(0); 
	if (!isset($HTTP_POST_VARS) && isset($_POST))
	{
		$HTTP_POST_VARS = $_POST;
		$HTTP_GET_VARS = $_GET;
		$HTTP_SERVER_VARS = $_SERVER;
		$HTTP_COOKIE_VARS = $_COOKIE;
		$HTTP_ENV_VARS = $_ENV;
		$HTTP_POST_FILES = $_FILES;
		if (isset($_SESSION))
		{
			$HTTP_SESSION_VARS = $_SESSION;
		}
	}
	if (@phpversion() < '4.0.0')
	{
		$test = array('HTTP_GET_VARS' => NULL, 'HTTP_POST_VARS' => NULL, 'HTTP_COOKIE_VARS' => NULL, 'HTTP_SERVER_VARS' => NULL, 'HTTP_ENV_VARS' => NULL, 'HTTP_POST_FILES' => NULL, 'phpEx' => NULL, 'phpbb_root_path' => NULL,'www_root_path'=>NULL);
		@reset($test);
		while (list($input,) = @each($test))
		{
			while (list($var,) = @each($$input))
			{
				if (!isset($test[$var]) && $var != 'test' && $var != 'input')
				{
					unset($$var);
				}
			}
		}
	}
	else if (@ini_get('register_globals') == '1' || strtolower(@ini_get('register_globals')) == 'on')
	{
		$not_unset = array('HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_COOKIE_VARS', 'HTTP_SERVER_VARS', 'HTTP_SESSION_VARS', 'HTTP_ENV_VARS', 'HTTP_POST_FILES', 'phpEx', 'phpbb_root_path','www_root_path');
		if (!isset($HTTP_SESSION_VARS))
		{
			$HTTP_SESSION_VARS = array();
		}
		$input = array_merge($HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_COOKIE_VARS, $HTTP_SERVER_VARS, $HTTP_SESSION_VARS, $HTTP_ENV_VARS, $HTTP_POST_FILES);
		unset($input['input']);
		unset($input['not_unset']);
		while (list($var,) = @each($input))
		{
			if (!in_array($var, $not_unset))
			{
				unset($$var);
			}
		} 
		unset($input);
	}
	if( !get_magic_quotes_gpc() )
	{
		if( is_array($HTTP_GET_VARS) )
		{
			while( list($k, $v) = each($HTTP_GET_VARS) )
			{
				if( is_array($HTTP_GET_VARS[$k]) )
				{
					while( list($k2, $v2) = each($HTTP_GET_VARS[$k]) )
					{
						$HTTP_GET_VARS[$k][$k2] = addslashes($v2);
					}
					@reset($HTTP_GET_VARS[$k]);
				}
				else
				{
					$HTTP_GET_VARS[$k] = addslashes($v);
				}
			}
			@reset($HTTP_GET_VARS);
		}	
		if( is_array($HTTP_POST_VARS) )
		{
			while( list($k, $v) = each($HTTP_POST_VARS) )
			{
				if( is_array($HTTP_POST_VARS[$k]) )
				{
					while( list($k2, $v2) = each($HTTP_POST_VARS[$k]) )
					{
						$HTTP_POST_VARS[$k][$k2] = addslashes($v2);
					}
					@reset($HTTP_POST_VARS[$k]);
				}
				else
				{
					$HTTP_POST_VARS[$k] = addslashes($v);
				}
			}
			@reset($HTTP_POST_VARS);
		}	
		if( is_array($HTTP_COOKIE_VARS) )
		{
			while( list($k, $v) = each($HTTP_COOKIE_VARS) )
			{
				if( is_array($HTTP_COOKIE_VARS[$k]) )
				{
					while( list($k2, $v2) = each($HTTP_COOKIE_VARS[$k]) )
					{
						$HTTP_COOKIE_VARS[$k][$k2] = addslashes($v2);
					}
					@reset($HTTP_COOKIE_VARS[$k]);
				}
				else
				{
					$HTTP_COOKIE_VARS[$k] = addslashes($v);
				}
			}
			@reset($HTTP_COOKIE_VARS);
		}
	}
	
// Persiapan Variabel-Variabel Global
	$config             = array();
	$lang              = array();
	$nav_links         = array();
	$gen_simple_header = FALSE;
	
	
	
// Persiapan Loading Modul-Modul Standard
	include($adp_root_path . 'config.inc.php');         // load file config
	include($adp_root_path . 'includes/template.php');  // template
	include($adp_root_path . 'includes/sessions.php');  // session
	include($adp_root_path . 'includes/functions.php'); // functions
	include($adp_root_path . 'includes/db.php');        // database

// Decoding	
	$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : getenv('REMOTE_ADDR') );
	$user_ip = encode_ip($client_ip);	

// SESSION 
$userdata = session_pagestart($user_ip,201); 
init_userprefs($userdata);

?>
