<?php
//
// PELANGGAN PAKET
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPaket.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR_PAKET))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Paket	= new Paket();

switch($mode){
	case 'save':
		$kode_pelanggan	  = $HTTP_POST_VARS['kodepelanggan'];
		$nama_pelanggan	  = $HTTP_POST_VARS['namapelanggan'];
		$alamat_pelanggan = $HTTP_POST_VARS['alamatpelanggan'];
		$contact_person	  = $HTTP_POST_VARS['contactperson'];
		$telp_cp	  			= $HTTP_POST_VARS['telpcontactperson'];
		$discount	  			= $HTTP_POST_VARS['discount'];
		$tgl_habis_kontrak= $HTTP_POST_VARS['tglhabiskontrak'];
		$is_aktif	  			= $HTTP_POST_VARS['isaktif'];
		
		if($kode_pelanggan==''){
			//Tambah Data Baru
			$Paket->tambahPelanggan(
				$nama_pelanggan,$alamat_pelanggan,$contact_person,
				$telp_cp,$discount,FormatTglToMySQLDate($tgl_habis_kontrak),
				$userdata['user_id']);
		}
		else{
			$Paket->ubahPelanggan(
				$kode_pelanggan, $nama_pelanggan, $alamat_pelanggan, $contact_person,
				$telp_cp,$discount, FormatTglToMySQLDate($tgl_habis_kontrak),
				$is_aktif);
		}
		
		exit;
	
	case 'showdatadetail':
		$kode_pelanggan = $HTTP_POST_VARS['kodepelanggan'];
		
		$row=$Paket->ambilDataPelanggan($kode_pelanggan);
				
		$return_eval	= "
			document.getElementById('showkodepelanggan').innerHTML='".$row['KodePelanggan']."';
			hdnkodepelanggan.value	= '".$row['KodePelanggan']."';
			namapelanggan.value	= '".$row['NamaPelanggan']."';
			alamatpelanggan.value	= '".$row['AlamatPelanggan']."';
			contactperson.value	= '".$row['ContactPerson']."';
			telpcontactperson.value	= '".$row['TelpCP']."';
			discount.value	= '".$row['BesarDiscount']."';
			tglhabiskontrak.value	= '".FormatMySQLDateToTgl($row['TglHabisKontrak'])."';
			isaktif.checked				= ".($row['IsAktif']==1?"true":"false").";";
			
		echo($return_eval);
		exit;
	
	case 'delete':
		// aksi hapus
		$kode_pelanggan = $HTTP_POST_VARS['kodepelanggan'];
		
		$Paket->hapusPelanggan($kode_pelanggan);
		
		exit;
	
	case 'ubahstatus':
		// aksi ubah status aktif
		
		$kode_pelanggan = $HTTP_POST_VARS['kodepelanggan'];
		
		$row=$Paket->ubahAktifPelanggan($kode_pelanggan);
		
		exit;
		
}
?>