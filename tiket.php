<?php
//
// CETAK TIKET UNTUK LINUX
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassPelanggan.php');
include($adp_root_path . 'ClassAsuransi.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassPromo.php');
include($adp_root_path . 'ClassVoucherDiskon.php');
include($adp_root_path . 'ClassMember.php');


// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$layout_kursi		= $HTTP_GET_VARS['layout_kursi'];
$kode_booking		= str_replace("\'","",$HTTP_GET_VARS['kode_booking']);		
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);		
$cetak_tiket		= $HTTP_GET_VARS['cetak_tiket'];		
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];		
$cso						= $userdata['nama'];
$kode_voucher		= $HTTP_GET_VARS['kode_voucher'];
$is_express			= $HTTP_GET_VARS['isexpress'];

$PengaturanUmum = new PengaturanUmum();

function getDiscountGroup($kode_jadwal,$tgl_berangkat){
		
		global $db;
		global $PengaturanUmum;
		
		//mengambil tanggal tuslah
		$data_pengaturan = $PengaturanUmum->ambilTglTuslah();
		
		//mengambil idjurusan dari kodejadwal
		$sql	= 
			"SELECT  IdJurusan
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal'";
		
		if(!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$id_jurusan	= ($row['IdJurusan']!="")?$row['IdJurusan']:0;
		
		//mengambil harga tiket
		$sql	= "SELECT f_jurusan_get_harga_tiket_by_id_jurusan('$id_jurusan','$tgl_berangkat') AS HargaTiket";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$harga_tiket	= ($row['HargaTiket']!="")?$row['HargaTiket']:0;
		
		$sql = "SELECT FlagLuarKota
            FROM   tbl_md_jurusan
						WHERE IdJurusan='$id_jurusan'";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota	= $row['FlagLuarKota'];
		
    $sql = "SELECT IdDiscount,NamaDiscount,IS_NULL(JumlahDiscount,0) AS JumlahDiscount
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota' AND KodeDiscount='M'";
	  
	  if (!$result = $db->sql_query($sql)){
			return 0;
		}
		
		$row = $db->sql_fetchrow($result);
		
		return $row;
}

$Reservasi= new Reservasi();
$Asuransi	= new Asuransi();
$User			= new User();
$Jurusan	= new Jurusan();
$Promo		= new Promo();
$Voucher 	= new Voucher();
$Member		= new Member();

$template->set_filenames(array('body' => 'tiket.tpl'));

//MEMERIKSA JIKA KODE TIKET KOSONG, MAKA AKAN DICARI DATA BERDASARKAN KODE BOOKING
if($cetak_tiket!=1){
	//mengambil data berdasarkan kode booking dan dapat mencetak tiket lebih dari 1 tiket
	if($jenis_pembayaran<3){
		//PEMBAYARAN MENGGUNAKAN VOUCHER
		$result	= $Reservasi->ambilDataKursiByKodeBooking4Tiket($kode_booking);
	}
	else{
		$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
	}
	$keterangan_duplikat	= "";
}
else{
	//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
	$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
	$keterangan_duplikat	= "**DUPLIKAT**<br>";
}

if (!$result){
	echo("Error :".__LINE__);
	exit;
}

$i=0;

$list_no_tiket		="";
$list_tiket_berasuransi	= "";
$list_kode_booking="";

$sudah_diperiksa			= false;
$dapat_discount_group	= false;

//MENGAMBIL JUMLAH TIKET YANG DIPESAN
$jum_pesanan	= @mysql_num_rows($result);

while ($row = $db->sql_fetchrow($result)){
	
	$array_jurusan	= $Jurusan->ambilNamaJurusanByIdJurusan($row['IdJurusan']);
	
	$no_tiket=$row['NoTiket'];
	$kode_booking=$row['KodeBooking'];
	$id_member=$row['IdMember'];
	$nama=$row['Nama'];
	$alamat=$row['Alamat'];
	$telp=$row['Telp'];
	$tanggal=$row['TglBerangkat'];
	$jam=$row['JamBerangkat'];
	$asal=$array_jurusan['Asal'];
	$tujuan=$array_jurusan['Tujuan'];
	$harga_tiket=$row['HargaTiket'];
	$discount=$row['Discount'];
	$total_bayar=$row['Total'];
	$bayar=$row['Total'];
	$nomor_kursi=$is_express==0?("Kursi<br>".$row['NomorKursi'].($row['PetugasPenjual']!=0?"":"<br/> **BELI ONLINE**")):"";
	//$cetak_tiket=$row['CetakTiket'];
	$jenis_pembayaran=($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
	$kode_jadwal=$row['KodeJadwal'];
	$jenis_penumpang	= $row['JenisPenumpang'];
	$id_jurusan	= $row['IdJurusan'];
	$waktu_cetak= $row['WaktuCetakTiket'];
	$petugas_cetak	= $row['PetugasCetakTiket'];
	
	if(in_array($jenis_pembayaran,array(3,4))){
		$discount			= 0;
		
		if($PengaturanUmum->isTuslah($tanggal)){
			$harga_tiket	= 10000;
			$bayar				= 10000;
		}
		else{
			$harga_tiket	= 0;
			$bayar				= 0;
		}
	}
	
	if($cetak_tiket!=1){
		$nama_cso	= $cso;
		
		//memeriksa jenis penumpang, jika return, akan mendaftarkan voucher
		if($jenis_penumpang=="R"){
			//penumpang return
			$kode_voucher = $Promo->setVoucher($no_tiket,$id_jurusan,$kode_jadwal,$discount,$userdata['user_id']);
		}
	}
	else{
		$nama_cso = $User->ambilNamaUser($row['PetugasCetakTiket']);
	}

	$cso	= $nama_cso;
	
	//MEMBERIKAN DISKON JIKA KURSI YANG DIPESAN LEBIH DARI JUMLAH MINIMUM DISCOUNT GRUP
	if($jum_pesanan>=$JUMLAH_MINIMUM_DISCOUNT_GROUP && ($row['Discount']=='' || $row['Discount']==0) /*&& !$dapat_discount_group*/){
		$data_discount_group	= getDiscountGroup($kode_jadwal,$tanggal);
		
		if($data_discount_group['JumlahDiscount']!=''){
			$discount			= $data_discount_group['JumlahDiscount'];
			$bayar				= $row['HargaTiket']-$data_discount_group['JumlahDiscount'];
			
			$dapat_discount_group	= true;
		}
	}
	
	//MENGAMBIL DATA ASURANSI
	//$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);

	if(!$sudah_diperiksa && $cetak_tiket!=1){
		if($id_member==''){
			//MEMERIKSA DATA PENUMPANG, JIKA BELUM PERNAH NAIK, AKAN DIMASUKKAN DALAM TBL PELANGGAN
			$Pelanggan	= new Pelanggan();
			
			if($Pelanggan->periksaDuplikasi($telp)){
				//JIKA SUDAH PERNAH NAIK, FREKWENSI KEBERANGKATAN AKAN DITAMBAHKAN
				
				$Pelanggan->ubah(
					$telp,$telp,
					$telp, $telp ,$nama,
				  "", $tanggal,
				  $userdata['user_id'], $kode_jadwal);
			}
			else{
				//JIKA BELUM PERNAH NAIK , AKAN DITAMBAHKAN
				$Pelanggan->tambah(
					$telp, $telp ,$nama,
				  $alamat, $tanggal, $tanggal,
				  $userdata['user_id'], $kode_jadwal,1,
				  0);
			}
			
		}
		else{
			$Reservasi->updateFrekwensiMember($id_member,$tanggal);
		}

		$sudah_diperiksa	= true;
	}
	
	$pesanan=(trim($row['pesanan'])==1)?"Pesanan":"Go Show";
	
		
	switch($jenis_pembayaran){
		case 0:
			$ket_jenis_pembayaran="TUNAI";

			if($kode_voucher != "" && (strtoupper(substr($kode_voucher,0,2))=="VC")){
				//VOUCHER DISKON
				if($cetak_tiket!=1){
					$data_voucher	= $Voucher->getDataVoucher($kode_voucher);
					$nilai_voucher= $data_voucher['NilaiVoucher']>1?$data_voucher['NilaiVoucher']:$harga_tiket*$data_voucher['NilaiVoucher'];

					$discount	= ($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher)>=$harga_tiket?$harga_tiket:($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher);

					$bayar 		= $harga_tiket-($discount>=$harga_tiket?$harga_tiket:$discount);
				}
				else{
					$data_voucher	= $Voucher->getDataVoucherByNoTiketBerangkat($no_tiket);
					$kode_voucher	= $data_voucher['KodeVoucher'];
				}

				$data_korporat= $Voucher->getDetailGroup($data_voucher['IdGroup']);
				$ket_jenis_pembayaran="VOUCHER DISKON<br>".$data_korporat["NamaGroup"]."<br><b>".$kode_voucher."</b>";
			}
		break;
		case 1:
			$ket_jenis_pembayaran="DEBIT CARD";

			if($kode_voucher != "" && (strtoupper(substr($kode_voucher,0,2))=="VC")){
				//VOUCHER DISKON
				if($cetak_tiket!=1){
					$data_voucher	= $Voucher->getDataVoucher($kode_voucher);
					$nilai_voucher= $data_voucher['NilaiVoucher']>1?$data_voucher['NilaiVoucher']:$harga_tiket*$data_voucher['NilaiVoucher'];

					$discount	= ($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher)>=$harga_tiket?$harga_tiket:($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher);

					$bayar 		= $harga_tiket-($discount>=$harga_tiket?$harga_tiket:$discount);
				}
				else{
					$data_voucher	= $Voucher->getDataVoucherByNoTiketBerangkat($no_tiket);
					$kode_voucher	= $data_voucher['KodeVoucher'];
				}

				$data_korporat= $Voucher->getDetailGroup($data_voucher['IdGroup']);
				$ket_jenis_pembayaran="VOUCHER DISKON<br>".$data_korporat["NamaGroup"]."<br><b>".$kode_voucher."</b>";
			}
		break;
		case 2:
			$ket_jenis_pembayaran="KREDIT CARD";

			if($kode_voucher != "" && (strtoupper(substr($kode_voucher,0,2))=="VC")){
				//VOUCHER DISKON
				if($cetak_tiket!=1){
					$data_voucher	= $Voucher->getDataVoucher($kode_voucher);
					$nilai_voucher= $data_voucher['NilaiVoucher']>1?$data_voucher['NilaiVoucher']:$harga_tiket*$data_voucher['NilaiVoucher'];

					$discount	= ($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher)>=$harga_tiket?$harga_tiket:($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher);

					$bayar 		= $harga_tiket-($discount>=$harga_tiket?$harga_tiket:$discount);
				}
				else{
					$data_voucher	= $Voucher->getDataVoucherByNoTiketBerangkat($no_tiket);
					$kode_voucher	= $data_voucher['KodeVoucher'];
				}

				$data_korporat= $Voucher->getDetailGroup($data_voucher['IdGroup']);
				$ket_jenis_pembayaran="VOUCHER DISKON<br>".$data_korporat["NamaGroup"]."<br><b>".$kode_voucher."</b>";
			}
		break;
		case 3:
			if($cetak_tiket==1){
				$data_voucher	= $Promo->getDataVoucherByNoTiket($no_tiket);
				$kode_voucher	= $data_voucher['KodeVoucher'];
			}
			
			$ket_jenis_pembayaran="VOUCHER<br><b>".$kode_voucher."</b>";
		break;
	
		case 4:
			//VOUCHER DISKON
			if($cetak_tiket!=1){
				$data_voucher	= $Voucher->getDataVoucher($kode_voucher);
			}
			else{
				$data_voucher	= $Voucher->getDataVoucherByNoTiketBerangkat($no_tiket);
				$kode_voucher	= $data_voucher['KodeVoucher'];
			}
			
			$harga_tiket	= 0;
			$bayar				= 0;
			
			$data_korporat= $Voucher->getDetailGroup($data_voucher['IdGroup']);
			$ket_jenis_pembayaran="VOUCHER DISKON<br>".$data_korporat["NamaGroup"]."<br><b>".$kode_voucher."</b>";
		break;
	
		case 5:
			//VOUCHER DISKON
			if($cetak_tiket!=1){
				$data_voucher	= $Voucher->getDataVoucher($kode_voucher);
				$nilai_voucher= $data_voucher['NilaiVoucher']>1?$data_voucher['NilaiVoucher']:$harga_tiket*$data_voucher['NilaiVoucher'];

				$discount	= ($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher)>=$harga_tiket?$harga_tiket:($data_voucher['IsHargaTetap']!=1?$nilai_voucher:$harga_tiket-$nilai_voucher);

				$bayar 		= $harga_tiket-($discount>=$harga_tiket?$harga_tiket:$discount);
			}
			else{
				$data_voucher	= $Voucher->getDataVoucherByNoTiketBerangkat($no_tiket);
				$kode_voucher	= $data_voucher['KodeVoucher'];
			}
			
			$data_korporat= $Voucher->getDetailGroup($data_voucher['IdGroup']);
			$ket_jenis_pembayaran="VOUCHER DISKON<br>".$data_korporat["NamaGroup"]."<br><b>".$kode_voucher."</b>";
		break;
	}
	
	$list_no_tiket			.="'$no_tiket',";
	$list_kode_booking	.="'$kode_booking',";
	
	$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
		
	if($jenis_penumpang!="R"){
		$show_harga_tiket	= "Tiket:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>";
	}
	else{
		//CETAK VOUCHER
		$show_harga_tiket	=
			"Tk.Pr:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>
			 Tk.Pl:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>";
		
		$data_voucher	= $Promo->getDataVoucherByNoTiket($no_tiket);
		
		$data_tgl_tuslah = $PengaturanUmum->ambilTglTuslah();
		
		/*$pesan_tuslah	=
			"<font size=3>Jika anda menggunakan voucher ini antara
			tgl ".FormatMySQLDateToTgl($data_tgl_tuslah['TGL_MULAI_TUSLAH1'])." s/d tgl ".FormatMySQLDateToTgl($data_tgl_tuslah['TGL_AKHIR_TUSLAH1'])."
			Anda diwajibkan membayar Rp.10.000
			sebagai biaya tuslah</font><br><br><br>";*/
		
		$template->
			assign_block_vars(
				'VOUCHER',
				array(
					'NAMA_PENUMPANG'		=>substr($nama,0,20),
					'JURUSAN_VOUCHER'		=>substr($tujuan,0,20)."-".substr($asal,0,20),
					'KODE_VOUCHER'			=>$data_voucher['KodeVoucher'],
					'EXPIRED_VOUCHER'		=>FormatMySQLDateToTglWithTime($data_voucher['ExpiredDate']),
					'PESAN_TUSLAH_VOUCHER'=>$pesan_tuslah
				)
			);
	}
	
	//CETAK KUPON UNDIAN MEMBER
	/*
	if($id_member!=""){
		include($adp_root_path . 'ClassMember.php');
		$Member	= new Member();
		
		$data_member=$Member->ambilData($id_member);
		
		//MEMERIKSA APAKAH MEMBER BERULANG TAHUN ATAU TIDAK
		//JIKA BERULANG TAHUN AKAN DIBERI DISKON DAN DIBERI UCAPAN ULANG TAHUN
		/*if(substr($data_member['TglLahir'],-5)==date("m-d")){
			$tahun_lahir		= substr($data_member['TglLahir'],0,4);
			$tahun_sekarang	= date("Y");
			$umur	= $tahun_sekarang-$tahun_lahir;
			
			$Promo->beriDiscountUltahMember($id_member,$no_tiket);
			
			$discount	= 0.5*$harga_tiket;
			$bayar		= $harga_tiket-$discount;
			
			$output_ultah	= "
				<tr><td class='tiket_body' colspan=3 align='center'>
					<script type='text/javascript'>alert('Ucapkan SELAMAT ULANG TAHUN pada member ini, karena member ini sedang berulang tahun yang ke $umur tahun pada hari ini!');</script>
					<br><br><br>
					<img src='./templates/images/ultah.png' />
					<br>---HAPPY BIRTHDAY--<br><br>
					Anda mendapatkan DISCOUNT<br>
					<font size=7>50%</font><br><br>
				</td></tr>
			";
			
		}*/
		//END MODUL ULTAH MEMBER
		/*
		if($cetak_tiket!=1){
			
			//MENGAMBIL POINT MEMBER
			$point_member	= $Member->ambilPoint($id_member);
			$point_kelipatan = 6; //setiap berapa kupon mendapatkan 1 kupon
			
			if(($point_member%$point_kelipatan)==0){
				$template->assign_block_vars('kupon_undian',array());
				//MEMBUAT VOUCHER BARU
				$kode_kupon	= $Promo->setKupon($no_tiket,$id_member,$nama,$userdata['user_id']);
				$waktu_cetak_kupon	= FormatMySQLDateToTglWithTime(date("Y-m-d H:i"));
				$pencetak_kupon			= $userdata['nama'];
			}
			
		}
		else{
			//MENGAMBIL DATA VOUCHER LAMA
			$data_kupon	= $Promo->getDataKuponByNoTiket($no_tiket);
			
			if($data_kupon['KodeKupon']!=""){
				$template->assign_block_vars('kupon_undian',array());
				$kode_kupon	= $data_kupon['KodeKupon'];
				$waktu_cetak_kupon	= FormatMySQLDateToTglWithTime($data_kupon['WaktuCetak']);
				$pencetak_kupon			= $data_kupon['NamaPetugasCetak'];
			}
		}
		
		$show_point_member	= "POINT ".number_format($Member->ambilPoint($id_member),0,",",".")."<br>";
		
	}*/
	
	//PESAN SPONSOR
	
	$pesan_sponsor	= $PengaturanUmum->ambilPesanUntukDiTiket();
	
	$ket	= "";
	
	if($id_member!=""){
		//JIKA MEMBER, PERIKSA APAKAH INI MEMBER VIP ATAU BUKAN
		$data_member	= $Member->ambilData($id_member);
		
		if($data_member["KategoriMember"]==2){
			
			$ket	= "<br><b>[---MEMBER VIP---]</b>";
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						'NO_TIKET'					=>$no_tiket,
						'DUPLIKAT'					=>$keterangan_duplikat,
						'JENIS_BAYAR'				=>$ket_jenis_pembayaran,
						'TGL_BERANGKAT'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
						'JURUSAN'						=>substr($asal,0,20)."-".substr($tujuan,0,20),
						'JAM_BERANGKAT'			=>$jam,
						'NAMA_PENUMPANG'		=>substr($nama,0,20).$ket,
						'NOMOR_KURSI'				=>$nomor_kursi,
						'HARGA_TIKET'				=>$show_harga_tiket,
						'DISKON'						=>substr("------------Rp.".number_format($discount,0,",","."),-12),
						'BAYAR'							=>substr("------------Rp.".number_format($bayar,0,",","."),-12),
						'POINT_MEMBER'			=>$show_point_member,
						'CSO'								=>substr($cso,0,20),
						'PESAN'							=>$pesan_sponsor,
						'TTD_CUSTOMER'			=>"TTD Member<br><br><br><br><br><b>".substr($nama,0,20)."</b><br>",
						'IS_VIP'						=> true
					)
				);
		}
	}
	
	$template->
			assign_block_vars(
				'ROW',
				array(
					'NO_TIKET'					=>$no_tiket,
					'DUPLIKAT'					=>$keterangan_duplikat,
					'JENIS_BAYAR'				=>$ket_jenis_pembayaran,
					'TGL_BERANGKAT'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
					'JURUSAN'						=>substr($asal,0,20)."-".substr($tujuan,0,20),
					'JAM_BERANGKAT'			=>$jam,
					'NAMA_PENUMPANG'		=>substr($nama,0,20).$ket,
					'NOMOR_KURSI'				=>$nomor_kursi,
					'HARGA_TIKET'				=>$show_harga_tiket,
					'DISKON'						=>substr("------------Rp.".number_format($discount,0,",","."),-12),
					'BAYAR'							=>substr("------------Rp.".number_format($bayar,0,",","."),-12),
					'POINT_MEMBER'			=>$show_point_member,
					'CSO'								=>substr($cso,0,20),
					'PESAN'							=>$pesan_sponsor,
					'IS_VIP'						=> false
				)
			);
	
	//UNTUK MENCETAK KUPON MERCHANDISE UNTUK PEMBELIAN ONLINE
	/*$tgl_awal_promo		= "2014-12-15";
	$tgl_akhir_promo	= "2014-12-31";
	$tgl_cetak				= substr($waktu_cetak,0,-9);
	$jumlah_kupon			= 360;
	$sql	= "SELECT COUNT(1) AS JumlahKupon FROM tbl_reservasi WHERE (WaktuCetakTiket BETWEEN '$tgl_awal_promo' AND '$tgl_akhir_promo') AND PetugasCetakTiket=0 AND KodeBooking!='$kode_booking'";

	if (!$result_merch = $db->sql_query($sql)){
		die_error("Err: $this->ID_FILE".__LINE__);
	}
	
	$data_kupon	= $db->sql_fetchrow($result_merch);
	
	if(($waktu_cetak>=$tgl_awal_promo && $waktu_cetak<=$tgl_akhir_promo) && $data_kupon['JumlahKupon']<$jumlah_kupon && $petugas_cetak==0){
		
		$template->
			assign_block_vars(
				'KUPON_MERCH',
				array(
					'notiket'	=>$no_tiket,
					'tgl'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
					'jadwal'	=>$kode_jadwal,
					'nama'		=>substr($nama,0,20),
					'telp'		=>substr($telp,0,3)."****".substr($telp,7)
				)
		);
	}
	*/

	//ASURANSI
	$data_asuransi_penumpang	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
	
	if($data_asuransi_penumpang["IdAsuransi"]!=""){
		
		$data_asuransi	= $Asuransi->ambilDataDetail($data_asuransi_penumpang["PlanAsuransi"]);
		
		$template->
			assign_block_vars(
				'ASURANSI',
				array(
					'NO_TIKET'				=>$no_tiket,
					'NAMA_PENUMPANG'	=>substr($nama,0,20).$ket,
					'PLAN_ASURANSI'		=>$data_asuransi["NamaPlan"],
					'KETERANGAN'			=>$data_asuransi["Keterangan"],
					'BESAR_PREMI'			=>"Rp.".number_format($data_asuransi["BesarPremi"],0,",",".")
				)
			);
		
		$list_tiket_berasuransi	.="'$no_tiket',";
	}
	
	$i++;
}

$temp_array_date	= explode("-",$tanggal);
$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] ,(int) $temp_array_date[2]+2,(int) $temp_array_date[0]);

$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) < date('Y-m-d');

if($tanggal_lampau!=1){
	$tbl_posisi_detail= "tbl_posisi_detail";
	$tbl_reservasi		= "tbl_reservasi";
}
else{
	$tbl_posisi_detail= "tbl_posisi_detail_backup";
	$tbl_reservasi		= "tbl_reservasi_olap";
}


//UPDATE DISCOUNT GROUP
$list_no_tiket	= substr($list_no_tiket,0,-1);

if($dapat_discount_group){

	$sql	=
		"UPDATE $tbl_reservasi SET
			JenisPenumpang='M',
		  Discount=IF($data_discount_group[JumlahDiscount]>1,IF($data_discount_group[JumlahDiscount]<=HargaTiket,$data_discount_group[JumlahDiscount],HargaTiket),$data_discount_group[JumlahDiscount]*HargaTiket),
		  JenisDiscount='$data_discount_group[NamaDiscount]',
		  Total=SubTotal-Discount
		 WHERE NoTiket IN($list_no_tiket);";
			
	if (!$db->sql_query($sql)){
		die_error("Err:".__LINE__);
	}
}


//mengambil jadwal utama
$sql	=
	"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
	FROM tbl_md_jadwal
	WHERE KodeJadwal='$kode_jadwal';";

if (!$result = $db->sql_query($sql)){
	echo("Err :".__LINE__);
	exit;
}
	
$row = $db->sql_fetchrow($result);
$kode_jadwal_utama	= $row[0];

$sql = 
	"UPDATE $tbl_posisi_detail SET StatusBayar=1
	WHERE 
	  KodeJadwal='$kode_jadwal_utama' 
		AND TGLBerangkat='$tanggal'
		AND NoTiket IN ($list_no_tiket);";

if(!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);
	exit;
}
// log trap 5
$parameter = "5|LINE: ".__LINE__."|Update Posisi Detail 1";
$Reservasi->tambahLogTrap('',$no_tiket,$tanggal,$kode_jadwal,'',$parameter);

//mengupate flag cetak tiket
if($cetak_tiket!=1){
	$list_kode_booking	= substr($list_kode_booking,0,-1);
	
	if($jenis_pembayaran<3){
		if($kode_voucher != "" && (strtoupper(substr($kode_voucher,0,2))=="VC")){
			//VOUCHER DISKON
			$signature	= $Voucher->setSignature(
				$kode_voucher,$no_tiket,$id_jurusan,
				$kode_jadwal,$userdata['user_id']);

			if($Voucher->pakaiVoucher(
				$kode_voucher,$tanggal,$no_tiket,$id_jurusan,
				$kode_jadwal,$userdata['user_id'],$userdata['nama'],
				$nama,$telp,$signature)){
				//trap 1
				$parameter = "1|LINE: ".__LINE__."|Update Cetak Tiket : Penumpang U Pakai Voucher Diskon";
				$Reservasi->tambahLogTrap('',$no_tiket,$tanggal,$kode_jadwal,'',$parameter);
				$Reservasi->updateStatusCetakTiketByNoTiket($userdata['user_id'],$jenis_pembayaran,$list_no_tiket,$userdata['KodeCabang']);
			}
		}else{
			//trap 2
			$parameter = "2|LINE: ".__LINE__."|Update Cetak Tiket : Penumpang U Tanpa Voucher Diskon";
			$Reservasi->tambahLogTrap('',$no_tiket,$tanggal,$kode_jadwal,'',$parameter);
			$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
		}
	}
	else{
		//UPDATE PAKAI VOUCHER
		
		if(strtoupper(substr($kode_voucher,0,2))!="VC"){
			//VOUCHER RETURN
			//trap 3
			$parameter = "3|LINE: ".__LINE__."|Update Cetak Tiket : Penumpang R Pakai Voucher";
			$Reservasi->tambahLogTrap('',$no_tiket,$tanggal,$kode_jadwal,'',$parameter);
			$Reservasi->updateStatusCetakTiketByNoTiket($userdata['user_id'],$jenis_pembayaran,$list_no_tiket,$userdata['KodeCabang']);
			$Promo->pakaiVoucher($kode_voucher,$no_tiket,$id_jurusan,$kode_jadwal,$userdata['user_id']);
		}
		else{
			//VOUCHER DISKON
			
			$signature	= $Voucher->setSignature(
				$kode_voucher,$no_tiket,$id_jurusan,
				$kode_jadwal,$userdata['user_id']);
			
			if($Voucher->pakaiVoucher(
				$kode_voucher,$tanggal,$no_tiket,$id_jurusan,
				$kode_jadwal,$userdata['user_id'],$userdata['nama'],
				$nama,$telp,$signature)){
				//trap 4
				$parameter = "4|LINE: ".__LINE__."|Update Cetak Tiket : Penumpang U Pakai Voucher Diskon / Pembayaran Voucher";
				$Reservasi->tambahLogTrap('',$no_tiket,$tanggal,$kode_jadwal,'',$parameter);
				$Reservasi->updateStatusCetakTiketByNoTiket($userdata['user_id'],$jenis_pembayaran,$list_no_tiket,$userdata['KodeCabang']);
			}
		}
	}

	//UPDATE SIGNATURE LAYOUT
	$Reservasi->updateLayoutSignature($tanggal,$kode_jadwal_utama);

	//UDPATE ASURANSI
	if($list_tiket_berasuransi!=""){
		$list_tiket_berasuransi	= substr($list_tiket_berasuransi,0,-1);
		$Asuransi->bayarPremi($list_tiket_berasuransi,$userdata["user_id"]);
	}
}

//UPDATE FLAG OTP USED
if($no_tiket!="" && $jenis_penumpang=="T") {
  $Reservasi->updateOTPUsed($no_tiket);
}

//mengupdate status asuransi menjadi OK
//$Asuransi->ubahFlagBatalAsuransi($list_no_tiket, 0, $userdata['user_id']);

$template->pparse('body');	
?>