<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class MacAddress{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function MacAddress(){
		$this->ID_FILE="C-MAD";
	}
	
	//BODY
	
	function periksaDuplikasi($kode){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) AS jumlah_data FROM tbl_mac_address WHERE MacAddress='$kode'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
		$ditemukan = ($row[0]<=0)?false:true;
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($mac_address,$kode_cabang,$nama_komputer){
	  
		/*
		ID	: 002
		IS	: data cabang belum ada dalam database
		FS	:Data cabang baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
				"INSERT INTO tbl_mac_address(MacAddress,KodeCabang,NamaKomputer,AddBy,AddByName,WaktuTambah) 
				VALUES('$mac_address','$kode_cabang','$nama_komputer',$userdata[user_id],'$userdata[username]',NOW());";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah($id,$mac_address,$kode_cabang,$nama_komputer){
	  
		/*
		ID	: 004
		IS	: data cabang sudah ada dalam database
		FS	:Data cabang diubah 
		*/
		
		//kamus
		global $db;
		
		//MENGUBAH DATA DI DATABASE
		$sql =
			"UPDATE tbl_mac_address 
			SET MacAddress='$mac_address',KodeCabang='$kode_cabang',NamaKomputer='$nama_komputer'
			WHERE Id=$id;";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list){
	  
		/*
		ID	: 005
		IS	: data Cabang sudah ada dalam database
		FS	:Data Cabang dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_mac_address
			WHERE Id IN($list);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($id){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_mac_address
			WHERE Id=$id;";
		
		if (!$result = $db->sql_query($sql,TRUE)){
			die_error("Err:$this->ID_FILE ".__LINE__);
		}

		$row=$db->sql_fetchrow($result);
		return $row;
		
	}//  END ambilDataDetail

}
?>