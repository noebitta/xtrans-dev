<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$area  					= isset($HTTP_GET_VARS['area'])? $HTTP_GET_VARS['area'] : $HTTP_POST_VARS['area'];	
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_driver		= isset($HTTP_GET_VARS['kode_driver'])? $HTTP_GET_VARS['kode_driver'] : $HTTP_POST_VARS['kode_driver'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'laporan_insentif_sopir/laporan_insentif_sopir_detail_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"TglSPJ":$sort_by;

$kondisi_area	= $area!="" ? " AND KodeArea='$area'":"";


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoSPJ","tbl_spj ts",
"&area=$area&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&kode_driver=$kode_driver&sort_by=$sort_by&order=$order",
"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND KodeDriver='$kode_driver' $kondisi_area" ,"laporan_insentif_sopir_detail.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	=
	"SELECT 
		NoSPJ,TglSPJ,KodeJadwal,
		TglBerangkat,JamBerangkat,JumlahKursiDisediakan,JumlahPenumpang,
		JumPnpInsentif,JumPnpInsentif*InsentifSopir AS TotalInsentif,
		NoPolisi,KodeArea,IF(IsSubJadwal!=1,'UTAMA','TRANSIT') AS Keterangan
	FROM tbl_spj ts
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND KodeDriver='$kode_driver'
	$kondisi_area
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
		
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'no_spj'=>$row['NoSPJ'],
				'waktu_cetak'=>FormatMySQLDateToTglWithTime($row['TglSPJ']),
				'kode_jadwal'=>$row['KodeJadwal'],
				'waktu_berangkat'=>FormatMySQLDateToTgl($row['TglBerangkat'])." ".$row['JamBerangkat'],
				'area'=>$row['KodeArea'],
				'layout'=>substr($row['JumlahKursiDisediakan'],0,2),
				'jum_pnp'=>number_format($row['JumlahPenumpang'],0,",","."),
				'jum_pnp_insentif'=>number_format($row['JumPnpInsentif'],0,",","."),
				'total_insentif'=>number_format($row['TotalInsentif'],0,",","."),
				'keterangan'=>$row['Keterangan']
			)
		);
	
	$i++;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&area=$area&kode_driver=$kode_driver&order=$order_invert";

//DATA SOPIR
$sql	=
	"SELECT 
		Nama,KodeSopir
	FROM tbl_md_sopir
	WHERE KodeSopir='$kode_driver'";

	
if(!$result = $db->sql_query($sql)){
	echo("Err: $sql".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&area=".$area."sort_by=$sort_by&order=$order&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kode_driver=".$kode_driver."&area=".$area;								
$script_cetak_excel="Start('laporan_insentif_sopir_detail_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$keterangan_area	= $area!=""?$area:"Semua area";

$keterangan_data_sopir	=
	"<b>Nama:</b>$row[Nama] | <b>NIS:</b>$row[KodeSopir] | <b>Area:</b>$keterangan_area | <b>Periode Laporan:</b>$tanggal_mulai s/d $tanggal_akhir";

//MENGAMBIL DETAIL TOTAL
$sql	= 
	"SELECT COUNT(IF(IsSubJadwal!=1,1,NULL)) AS TotalManifestUtama,COUNT(IF(IsSubJadwal=1,1,NULL)) AS TotalManifestTransit,SUM(InsentifSopir*JumPnpInsentif) AS TotalInsentif  
	FROM tbl_spj 
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND KodeDriver='$kode_driver'
	$kondisi_area;";

if(!$result = $db->sql_query($sql)){
	echo("Err: $sql".__LINE__);exit;
}

$data_sopir	= $db->sql_fetchrow($result);

$template->assign_vars(array(
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'DATA_SOPIR'		=> $keterangan_data_sopir,
	'TOTAL_MANIFEST'=> $data_sopir['TotalManifestUtama'],
	'TOTAL_MANIFEST_TRANSIT'=> $data_sopir['TotalManifestTransit'],
	'TOTAL_INSENTIF'=> number_format($data_sopir['TotalInsentif'],0,",","."),
	'A_SORT_1'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=NoSPJ'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan berdasarkan Nomor Manifest ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=TglSPJ'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan berdasarkan tanggal cetak ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=KodeJadwal'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan berdasarkan kode jadwal($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=TglBerangkat'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan berdasarkan tanggal berangkat ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=JumlahKursiDisediakan'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan berdasarkan layout kursi($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=JumlahPenumpang'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan berdasarkan jumlah penumpang ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=JumPnpInsentif'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan berdasarkan jumlah penumpang insentif ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=TotalInsentif'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan berdasarkan total penerimaan insentif ($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_insentif_sopir_detail.'.$phpEx.'?sort_by=Keterangan'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan berdasarkan Keterangan ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>