<?php
//
// CETAK TIKET UNTUK LINUX
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassPelanggan.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$layout_kursi		= $HTTP_GET_VARS['layout_kursi'];
$kode_booking		= str_replace("\'","",$HTTP_GET_VARS['kode_booking']);		
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);		
$cetak_tiket		= $HTTP_GET_VARS['cetak_tiket'];		
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];		
$cso						= $userdata['nama'];

function getDiscountGroup($kode_jadwal,$tgl_berangkat){
		
		global $db;
		
		//mengambil tanggal tuslah
		$sql	= 
			"SELECT TglMulaiTuslah,TglAkhirTuslah
				FROM tbl_pengaturan_umum LIMIT 0,1";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$tgl_mulai_tuslah	= $row['TglMulaiTuslah'];
		$tgl_akhir_tuslah	= $row['TglAkhirTuslah'];
		
		//mengambil harga tiket
		$sql	= 
			"SELECT IF(p_tgl_berangkat NOT BETWEEN $tgl_mulai_tuslah AND $tgl_akhir_tuslah,HargaTiket,HargaTiketTuslah) AS HargaTiket
			FROM tbl_md_jurusan AS tmjr INNER JOIN tbl_md_jadwal tmj ON tmjr.IdJurusan=tmj.IdJurusan
			WHERE tmj.KodeJadwal='$kode_jadwal'";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$harga_tiket	= ($row['HargaTiket']!="")?$row['HargaTiket']:0;
		
		//mengambil idjurusan dari kodejadwal
		$sql	= 
			"SELECT  IdJurusan
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal'";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$id_jurusan	= ($row['IdJurusan']!="")?$row['IdJurusan']:0;
		
		$sql = "SELECT FlagLuarKota
            FROM   tbl_md_jurusan
						WHERE IdJurusan='$id_jurusan'";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota	= $row['FlagLuarKota'];
		
    $sql = "SELECT IdDiscount,NamaDiscount,IS_NULL(JumlahDiscount,0) AS JumlahDiscount
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota' AND KodeDiscount='M'";
	  
	  if (!$result = $db->sql_query($sql)){
			return 0;
		}
		
		$row = $db->sql_fetchrow($result);
		
		return $row;
}

$Reservasi= new Reservasi();
$Asuransi	= new Asuransi();

	//MEMERIKSA JIKA KODE TIKET KOSONG, MAKA AKAN DICARI DATA BERDASARKAN KODE BOOKING
	if($cetak_tiket!=1){
		//mengambil data berdasarkan kode booking dan dapat mencetak tiket lebih dari 1 tiket
		$result	= $Reservasi->ambilDataKursiByKodeBooking($kode_booking);
	}
	else{
		//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
		$result	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);
	}
	
	if ($result){
		
		//EXPORT KE PDF
		class PDF extends FPDF {
			function Footer() {
				$this->SetY(-1.5);
			  $this->SetFont('Arial','I',8);
			  $this->Cell(0,1,'',0,0,'R');
			}
			
			var $javascript;
	    var $n_js;

	    function IncludeJS($script) {
	        $this->javascript=$script;
	    }

	    function _putjavascript() {
	        $this->_newobj();
	        $this->n_js=$this->n;
	        $this->_out('<<');
	        $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
	        $this->_out('>>');
	        $this->_out('endobj');
	        $this->_newobj();
	        $this->_out('<<');
	        $this->_out('/S /JavaScript');
	        $this->_out('/JS '.$this->_textstring($this->javascript));
	        $this->_out('>>');
	        $this->_out('endobj');
	    }

	    function _putresources() {
	        parent::_putresources();
	        if (!empty($this->javascript)) {
	            $this->_putjavascript();
	        }
	    }

	    function _putcatalog() {
	        parent::_putcatalog();
	        if (isset($this->javascript)) {
	            $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
	        }
	    }
			
			function AutoPrint($dialog=false)
			{
			    //Embed some JavaScript to show the print dialog or start printing immediately
			    $param=($dialog ? 'true' : 'false');
			    $script="print($param);";
			    $this->IncludeJS($script);
			}
		}
					
		//set kertas & file
		$pdf=new PDF('P','cm','tiketkecil');
		$pdf->Open();
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->Setmargins(0.6,0,0,0);
		$pdf->SetFont('courier','',9);
		$i=0;
		$line_space	=0.4;
		
		$list_no_tiket		="";
		$list_kode_booking="";
		
		$sudah_diperiksa	= false;
		
		//MENGAMBIL JUMLAH TIKET YANG DIPESAN
		$jum_pesanan	= @mysql_num_rows($result);
		
		while ($row = $db->sql_fetchrow($result)){
			
			$no_tiket=$row['NoTiket'];
			$kode_booking=$row['KodeBooking'];
			$id_member=$row['IdMember'];
			$nama=$row['Nama'];
			$alamat=$row['Alamat'];
			$telp=$row['Telp'];
			$tanggal=$row['TglBerangkat'];
			$jam=$row['JamBerangkat'];
			$asal=explode("(",$row['NamaAsal']);
			$tujuan=explode("(",$row['NamaTujuan']);
			$harga_tiket=number_format($row['HargaTiket'],0,",",".");
			$discount=number_format($row['Discount'],0,",",".");
			$total_bayar=$row['Total'];
			$bayar=number_format($row['Total'],0,",",".");
			$nomor_kursi=$row['NomorKursi'];
			$operator=$row['NamaCSO'];
			$cetak_tiket=$row['CetakTiket'];
			$jenis_pembayaran=($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
			$kode_jadwal=$row['KodeJadwal'];
			$cso	=($cetak_tiket!=1)?$cso:$row['NamaCSOTiket'];
			
			//MEMBERIKAN DISKON JIKA KURSI YANG DIPESAN LEBIH DARI JUMLAH MINIMUM DISCOUNT GRUP
			if($jum_pesanan>=$JUMLAH_MINIMUM_DISCOUNT_GROUP && ($row['Discount']=='' || $row['Discount']==0)){
				$data_discount_group	= getDiscountGroup($kode_jadwal,$tanggal);
				
				//MENGUBAH DATA DISCOUNT
				$sql = "CALL sp_reservasi_ubah_discount('$no_tiket', 'M', '$data_discount_group[NamaDiscount]','$data_discount_group[JumlahDiscount]');";
				
				if (!$db->sql_query($sql)){
					die_error("Err $this->ID_FILE".__LINE__);
				}
				
				if($data_discount_group['JumlahDiscount']!=''){
					$discount			= number_format($data_discount_group['JumlahDiscount'],0,",",".");
					$total_bayar	= $row['HargaTiket']-$data_discount_group['JumlahDiscount'];
					$bayar				= number_format($total_bayar,0,",",".");
				}
			}
			
			//MENGAMBIL DATA ASURANSI
			$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
			
			if(!$sudah_diperiksa && $cetak_tiket!=1){
				if($id_member==''){
					//MEMERIKSA DATA PENUMPANG, JIKA BELUM PERNAH NAIK, AKAN DIMASUKKAN DALAM TBL PELANGGAN
					$Pelanggan	= new Pelanggan();
					
					if($Pelanggan->periksaDuplikasi($telp)){
						//JIKA SUDAH PERNAH NAIK, FREKWENSI KEBERANGKATAN AKAN DITAMBAHKAN
						
						$Pelanggan->ubah(
							$telp,$telp,
							$telp, $telp ,$nama,
						  "", $tanggal,
						  $userdata['user_id'], $kode_jadwal);
					}
					else{
						//JIKA BELUM PERNAH NAIK , AKAN DITAMBAHKAN
						$Pelanggan->tambah(
							$telp, $telp ,$nama,
						  $alamat, $tanggal, $tanggal,
						  $userdata['user_id'], $kode_jadwal,1,
						  0);
					}
					
					$sudah_diperiksa	= true;
				}
				else{
					$Reservasi->updateFrekwensiMember($id_member,$tanggal);
				}
			}
			
			//$pesanan=(trim($row['pesanan'])==1)?"Pesanan":"Go Show";
			
				
			switch($jenis_pembayaran){
				case 0:
					$ket_jenis_pembayaran="TUNAI";
				break;
				case 1:
					$ket_jenis_pembayaran="DEBIT CARD";
				break;
				case 2:
					$ket_jenis_pembayaran="KREDIT CARD";
				break;
				case 3:
					$ket_jenis_pembayaran="VOUCHER";
				break;
			}
			
			//$no_seri_kartu=(trim($row['no_seri_kartu'])=='')?'':" ***MEMBER*** ".trim($row['no_seri_kartu']);
		
			$list_no_tiket			.="'$no_tiket',";
			$list_kode_booking	.="'$kode_booking',";

			if($i>0){
				//header utk tiket pertama  (maka headernya harus sedikit diperpendek)
				$pdf->Cell(0,0.5,'','',0,'');$pdf->Ln();
			}
			else{
				$pdf->Ln();
				$pdf->Cell(0,0,'','',0,'');$pdf->Ln();
			}			
			
			
			$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
			$pdf->Ln();
			$pdf->SetFont('courier','',11);
		  $pdf->Cell(6.4,$line_space,$data_perusahaan['NamaPerusahaan'],'',0,'');$pdf->Ln();
		  $pdf->SetFont('courier','',10);
			$pdf->Cell(6.4,$line_space,"Pelopor On Time Shuttle",'',0,'');$pdf->Ln();
			$pdf->Cell(6.4,$line_space,$data_perusahaan['AlamatPerusahaan'],'',0,'');$pdf->Ln();
		  $pdf->Cell(6.4,$line_space,$data_perusahaan['TelpPerusahaan'],'',0,'');$pdf->Ln();
		  
			$pdf->SetFont('courier','',10);
			//$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(6.4,$line_space,$no_tiket,'',0,'');$pdf->Ln();
		  $pdf->Cell(6.4,$line_space,$ket_jenis_pembayaran,'',0,'');$pdf->Ln();
			
			//content
			$pdf->Cell(2,$line_space,'Tgl.Pergi',0,0,'');$pdf->Cell(4.4,$line_space,":".dateparse(FormatMySQLDateToTgl($tanggal)),0,0,'');$pdf->Ln();
			$pdf->Cell(6.4,$line_space,substr($asal[0],0,20)."-".substr($tujuan[0],0,20),0,0,'');$pdf->Ln();
			$pdf->Cell(2,$line_space,'Jam',0,0,'');$pdf->Cell(4.4,$line_space,":".$jam,0,0,'');$pdf->Ln();
			//$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(2,$line_space,'No.Kursi',0,0,'');$pdf->Cell(4.4,$line_space,":".$nomor_kursi,0,0,'');$pdf->Ln();
			//$pdf->Cell(2,$line_space,'Nama',0,0,'');$pdf->Cell(4.4,$line_space,":".substr($nama,0,24),0,0,'');$pdf->Ln();
			//$pdf->Cell(2,$line_space,'Telp',0,0,'');$pdf->Cell(4.4,$line_space,":".substr($telp,0,24),0,0,'');$pdf->Ln();
			//$pdf->Cell(2,$line_space,'Ket',0,0,'');$pdf->Cell(4.4,$line_space,":",0,0,'');$pdf->Ln();
			//$pdf->MultiCell2(6.4,$line_space,$alamat,'','L');$pdf->Ln();
			//$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
			//$pdf->Cell(2,$line_space,'Fee Tiket',0,0,'');$pdf->Cell(4.4,$line_space,":".$harga_tiket." (Rp.)",0,0,'');$pdf->Ln();
			//$pdf->Cell(2,$line_space,'Discount',0,0,'');$pdf->Cell(4.4,$line_space,":".$discount." (Rp.)",0,0,'');$pdf->Ln();
			//$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
			$pdf->Ln();
			
			if($data_asuransi['IdAsuransi']!=""){
				$pdf->Cell(2,$line_space,'Tiket',0,0,'');$pdf->Cell(2.9,$line_space,": Rp. ".$bayar,0,0,'R');$pdf->Ln();
				$pdf->Cell(2,$line_space,'Premi',0,0,'');$pdf->Cell(2.9,$line_space,": Rp. ".number_format($data_asuransi['BesarPremi'],0,",","."),0,0,'R');$pdf->Ln();
				$pdf->Cell(4.4,$line_space,'Asuransi '.$data_asuransi['NamaPlanAsuransi'],0,0,'');$pdf->Ln();
				$bayar	= number_format($total_bayar + $data_asuransi['BesarPremi'] ,0,",",".");
			}
			
			$pdf->Cell(2,$line_space,'Bayar',0,0,'');$pdf->Cell(2.9,$line_space,": Rp. ".$bayar,0,0,'R');$pdf->Ln();
			$pdf->Cell(2,$line_space,'CSO',0,0,'');$pdf->Cell(4.4,$line_space,":".substr($cso,0,24),0,0,'');$pdf->Ln();
			$pdf->Ln();
			
			//PESAN SPONSOR
			
			$pesan_sponsor	= $Reservasi->ambilPesanUntukDiTiket();
			
			if(strlen($pesan_sponsor)>30){
				$arr_kata	= explode(" ",$pesan_sponsor);
				
				$temp_pesan_sponsor="";
				$jumlah_kata	= count($arr_kata);
				
				$idx	= 0;
				
				while($idx<$jumlah_kata){
					
					if(strlen($temp_pesan_sponsor." ".$arr_kata[$idx])<30){
						$temp_pesan_sponsor	= $temp_pesan_sponsor." ".$arr_kata[$idx];
						$idx++;
					}
					else{
						$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
						$temp_pesan_sponsor	= "";
						
					}
				}
				
				$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
			}
			else{
				$pdf->Cell(6.4,$line_space,$pesan_sponsor,0,0,'C');$pdf->Ln();
			}
			
			$pdf->Cell(6,$line_space,"-- Terima Kasih --",0,0,'C');$pdf->Ln();
			$pdf->SetFont('courier','',8);
			//$pdf->Cell(6.4,$line_space,$data_perusahaan['EmailPerusahaan'],0,0,'C');$pdf->Ln();
			//$pdf->SetFont('courier','',8);
			//$pdf->Cell(6.4,$line_space,$data_perusahaan['WebSitePerusahaan'],0,0,'C');$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			//$pdf->Ln();
			//$pdf->Ln();
			$i++;
		}
		
		//UPDATE FLAG TIKET DI LAYOUT KURSI
		$list_no_tiket	= substr($list_no_tiket,0,-1);
		
		$sql = 
			"UPDATE tbl_posisi_detail SET StatusBayar=1
			WHERE 
			  KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal('$kode_jadwal') 
				AND TGLBerangkat='$tanggal'
				AND NoTiket IN ($list_no_tiket);";
		
		if(!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
		
		//mengupate flag cetak tiket
		if($cetak_tiket!=1){
			$list_kode_booking	= substr($list_kode_booking,0,-1);
			$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
		}
		
		//mengupdate status asuransi menjadi OK
		$Asuransi->ubahFlagBatalAsuransi($list_no_tiket, 0, $userdata['user_id']);
		
		$pdf->AutoPrint(true);
		$pdf->Output();
		
	} 
	else{
		//die_error('GAGAL MENGAMBIL DATA',__LINE__,__FILE__,$sql);
		echo("Error :".__LINE__);
		exit;
	}
	
?>