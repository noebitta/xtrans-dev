<?php

// desc  : Footer
// code  : eR
// lupd  : juni 22 05

if (!defined('FRAMEWORK')) {  die("Hacking attempt"); }

// benchmark
$benchmark_time_end = getmicrotime();
$time = round($benchmark_time_end - $benchmark_time_start,5);

// footer page
$template->set_filenames(array('overall_footer' => 'overall_footer.tpl'));
$template->assign_vars(
array(
    'PAGE_GENERATION_TIME'=>'This Page Was Generated In : '.$time.' sec',
	'TRANSLATION_INFO' => ( isset($lang['TRANSLATION_INFO']) ) ? $lang['TRANSLATION_INFO'] : '',
	'ADMIN_LINK' => $admin_link)
);
$template->pparse('overall_footer');
$db->sql_close();

// g-zip compression
if ( $do_gzip_compress )
{
	$gzip_contents = ob_get_contents();
	ob_end_clean();
	$gzip_size = strlen($gzip_contents);
	$gzip_crc = crc32($gzip_contents);
	$gzip_contents = gzcompress($gzip_contents, 9);
	$gzip_contents = substr($gzip_contents, 0, strlen($gzip_contents) - 4);
	echo "\x1f\x8b\x08\x00\x00\x00\x00\x00";
	echo $gzip_contents;
	echo pack('V', $gzip_crc);
	echo pack('V', $gzip_size);
}
exit;
?>