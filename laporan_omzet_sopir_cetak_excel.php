<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND CetakSPJ=1 AND tr.KodeSopir=tms.KodeSopir";
		
$kondisi_cari	=($cari=="")?
	" WHERE tms.KodeSopir LIKE '%' ":
	" WHERE (tms.KodeSopir LIKE '$cari%' OR tms.Nama LIKE '%$cari%')";
	

	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tms.KodeSopir":$sort_by;

$sql_total_jalan			= "(SELECT COUNT(DISTINCT(NoSPJ)) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_hari_kerja = "(SELECT COUNT(DISTINCT(TglBerangkat)) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_penumpang	= "(SELECT IS_NULL(COUNT(NoTiket),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_omzet			= "(SELECT IS_NULL(SUM(SubTotal),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_kursi			= "(SELECT SUM(IS_NULL(JumlahKursiDisediakan,0)) FROM tbl_spj 
													WHERE KodeDriver=tms.KodeSopir)";
													
$sql	=
	"SELECT tms.KodeSopir,tms.Nama, 
		$sql_total_jalan AS Jalan, 
		$sql_total_hari_kerja AS TotalHariKerja,
		$sql_total_penumpang AS TotalPenumpang, 
		$sql_total_omzet AS TotalOmzet, 
		$sql_total_kursi AS JumlahKursi, 
		f_sopir_get_insentif(KodeSopir,'$tanggal_mulai_mysql','$tanggal_akhir_mysql') AS TotalInsentif,
		100*$sql_total_penumpang/$sql_total_kursi AS Produktifitas
	FROM tbl_md_sopir tms 
	$kondisi_cari
	ORDER BY $sort_by $order";
	
//EXPORT KE MS-EXCEL
	
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
	  
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Sopir per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
	  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'NRP');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Total Hari Kerja');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total Jalan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jum.Pnp');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Omzet');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Insentif');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Load Faktor');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		
		$idx=0;
		
		while ($row = $db->sql_fetchrow($result)){
			$idx++;
			$idx_row=$idx+3;
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['Kode']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['TotalHariKerja']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['Jalan']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['TotalPenumpang']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['TotalOmzet']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['TotalInsentif']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Produktifitas']."%");
			
			
		}
		$temp_idx=$idx_row;
		
		$idx_row++;		
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D4:D'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
			
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Laporan Omzet Sopir per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
  
?>
