<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];


$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"TglBerangkat":$sort_by;

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$template->set_filenames(array('body' => 'log.tiketkk/index.tpl')); 

$kondisi	=
	"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND PetugasCetakTiket!=0 AND JenisPenumpang='KK'";

$kondisi	.=($cari=="")?"":
	" AND (tr.Nama LIKE '%$cari%'
		OR tr.Telp LIKE '$cari%'
		OR tu.Nama LIKE '%$cari%'
		OR KodeJadwal LIKE '%$cari%')";


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoTiket","$tbl_reservasi tr LEFT JOIN tbl_user tu ON tr.PetugasCetakTiket=tu.user_id",
						"&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&is_today=$is_today&cari=$cari&sort_by=$sort_by&order=$order",
						$kondisi,"log.tiketkk.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================
		
//DATA PENJUALAN TIKET
$sql	= 
	"SELECT NoTiket,JamBerangkat,NomorKursi,tr.TglBerangkat,tr.Nama,tr.Telp,tr.KodeJadwal,tu.nama AS NamaPetugasCetak,WaktuCetakTiket
	FROM $tbl_reservasi tr LEFT JOIN tbl_user tu ON tr.PetugasCetakTiket=tu.user_id
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
					
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'						=>$odd,
				'no'						=>$i,
				'tgl_berangkat'	=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])),
				'no_tiket'			=>$row['NoTiket'],
				'jam'						=>$row['JamBerangkat'],
				'kode_jadwal'		=>$row['KodeJadwal'],
				'no_kursi'			=>$row['NomorKursi'],
				'nama'					=>$row['Nama'],
				'telp'					=>$row['Telp'],
				'waktu_cetak_tiket'	=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
				'pencetak_tiket'=>$row['NamaPetugasCetak']
			)
		);
	
	$i++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&is_today=$is_today&cari=$cari&sort_by=$sort_by&order=$order";
$script_cetak_excel="Start('log.tiketkk.cetakexcel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//END KOMPONEN UNTUK EXPORT	

$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert&is_today=$is_today";

$array_sort	= 
	"'".append_sid('log.tiketkk.php?sort_by=TglBerangkat'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=NoTiket'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=JamBerangkat'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=KodeJadwal'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=NomorKursi'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=tr.Nama'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=tr.Telp'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=WaktuCetakTiket'.$parameter_sorting)."',".
	"'".append_sid('log.tiketkk.php?sort_by=NamaPetugasCetak'.$parameter_sorting)."'";
	
$temp_var		= "is_today".($is_today==""?"1":$is_today);
$$temp_var	= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.php') .'#log_reservasi">Home</a> | <a href="'.append_sid('log.tiketkk.php').'">Laporan Tiket Keluarga Karyawan</a>',
	'CETAK_XL'    	=> $script_cetak_excel,
	'ACTION_CARI'		=> append_sid('log.tiketkk.'.$phpEx),
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'CARI'					=> $cari,
	'IS_TODAY1'			=> $is_today1,
	'IS_TODAY0'			=> $is_today0,
	'PAGING'				=> $paging,
	'ARRAY_SORT'		=> $array_sort
	)
);
	    

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>