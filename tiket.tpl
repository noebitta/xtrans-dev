<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript" src="./browser/bowser.min.js"></script>
<script type="text/javascript">
	nama_browser	= bowser.name;
	
	if(nama_browser.toUpperCase()!='FIREFOX'){
		alert("Anda harus menggunakan browser Mozilla Firefox.");
		window.close();
	}
	
  function print_doc()
  {
		//set printer default
		var printers = jsPrintSetup.getPrintersList().split(',');
		jsPrintSetup.setPrinter(printers[0]);
		jsPrintSetup.setOption('orientation', jsPrintSetup.kPortraitOrientation);
		// set top margins in millimeters
		jsPrintSetup.setOption('marginTop', 5);
		jsPrintSetup.setOption('marginBottom', 5);
		jsPrintSetup.setOption('marginLeft', 5);
		jsPrintSetup.setOption('marginRight', 5);
		// set page header
		jsPrintSetup.setOption('headerStrLeft', '');
		jsPrintSetup.setOption('headerStrCenter', '');
		jsPrintSetup.setOption('headerStrRight', '');
		// set empty page footer
		jsPrintSetup.setOption('footerStrLeft', '');
		jsPrintSetup.setOption('footerStrCenter', '');
		jsPrintSetup.setOption('footerStrRight', '');
		// Suppress print dialog
		jsPrintSetup.setSilentPrint(false);/** Set silent printing */
		// Do Print
		//jsPrintSetup.print();
		jsPrintSetup.printWindow(window);
		// Restore print dialog
		jsPrintSetup.setSilentPrint(false); /** Set silent printing back to false */
		
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
		window.opener.getUpdateMobil();
		
		window.close();
		
  }
	
 </script>	 

<body class='tiket'>

<div style="font-size:14px;width:225px;">
	<!-- BEGIN ROW -->
	XTrans<br>
	Pelopor On Time Shuttle<br>
	<br>
	{ROW.NO_TIKET}<br>
	{ROW.DUPLIKAT}
	By:{ROW.JENIS_BAYAR}<br>
	Pergi:<font style="font-size:18px;">{ROW.TGL_BERANGKAT}</font><br>
	{ROW.JURUSAN}<br>
	
	<div style="text-align: center;width:225px;">
		{ROW.JAM_BERANGKAT}<br>
		<font style="text-transform: uppercase;">{ROW.NAMA_PENUMPANG}</font><br>
		{ROW.NOMOR_KURSI}<br>
	</div>
	{ROW.HARGA_TIKET}
	Disc.:{ROW.DISKON}<br>
	Bayar:{ROW.BAYAR}<br>
	{ROW.POINT_MEMBER}
	CSO:{ROW.CSO}<br>
	<br>
	{ROW.TTD_CUSTOMER}
	{ROW.PESAN}<br>
	<br>
	<script type="text/javascript">
		if ({ROW.IS_VIP}) {
			alert("Pelanggan ini adalah MEMBER VIP, tiket akan tercetak 2 lembar, jangan lupa meminta tanda tangan MEMBER di tiket kedua, dan simpan tiket kedua sebagai bukti rekap!");
		}
	</script>
	<!-- END ROW -->
	<div style="text-align: center;width:225px;"">-- Terima Kasih --</div><br>
	<!-- BEGIN KUPON_MERCH -->
	<div style="text-align: center;width:225px;">
		<br>
		------KUPON MERCHANDISE-----<br>
		Nama:{KUPON_MERCH.nama}<br>
		Telp:{KUPON_MERCH.telp}<br>
		#Tiket:{KUPON_MERCH.notiket}<br>
		Tgl:{KUPON_MERCH.tgl}<br>
		#Jadwal:{KUPON_MERCH.jadwal}<br>
		Tukarkan kupon ini dengan merchandise selama persediaan masih ada
	</div>
	<!-- END KUPON_MERCH -->
	<!-- BEGIN kupon_undian -->
	<div style="text-align: center;width:225px;">
		<br><br><br>
		---KUPON UNDIAN--<br>
		<br>
		<span style="font-size:20px;">{KODE_KUPON}</span><br><br>
		<b>{ID_MEMBER}</b><br>
		Nama:{NAMA_PENUMPANG}<br><br>
		Telp:{TELP_PENUMPANG}<br><br>
		#Tiket:{NO_TIKET}<br>
		WaktuCetak<br>
		{WAKTU_CETAK}<br>
		CSO:{PENCETAK_KUPON}<br><br><br>
	</div>
	<!-- END kupon_undian -->
	<!-- BEGIN VOUCHER -->
	<div style="text-align: center;width:225px;">
		<br><br><br>
		---------VOUCHER--------<br>
		<br>
		Nama:{VOUCHER.NAMA_PENUMPANG}<br><br>
		{VOUCHER.JURUSAN_VOUCHER}<br><br>
		TIKET BALIK<br><br>
		FREE<br><br>
		<span style="font-size:20px;">{VOUCHER.KODE_VOUCHER}</span><br><br>
		berlaku hingga<br>
		{VOUCHER.EXPIRED_VOUCHER}<br><br>
		{VOUCHER.PESAN_TUSLAH_VOUCHER}
	</div>
	<!-- END VOUCHER -->
	<!-- BEGIN ASURANSI -->
	<div style="text-align: center;width:225px;">
		<br><br><br>
		---------ASURANSI--------<br>
		<br>
		Nama:{ASURANSI.NAMA_PENUMPANG}<br><br>
		<span style="font-size:20px;">{ASURANSI.NO_TIKET}</span><br><br>
		{ASURANSI.PLAN_ASURANSI}<br><br>
		{ASURANSI.BESAR_PREMI}<br><br>
		{ASURANSI.KETERANGAN}
	</div>
	<!-- END ASURANSI -->
	<br><br><br>
</div>
</body>

<script language="javascript">
	print_doc();
</script>
</html>
