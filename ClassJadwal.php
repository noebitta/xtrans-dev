<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################


class Jadwal{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Jadwal(){
		$this->ID_FILE="C-JDW";
	}
	
	//BODY
	
	function periksaDuplikasi($kode_jadwal){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika kode_jadwal tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT f_jadwal_periksa_duplikasi('$kode_jadwal') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti kode_jadwal sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($kode_jadwal, $id_jurusan, $jam, $jumlah_kursi,$flag_sub_jadwal,$kode_jadwal_utama, $is_express, $flag_aktif, $hari_aktif){
	  
		/*
		ID	: 002
		IS	: data jadwal belum ada dalam database
		FS	:Data jadwal baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE

    $sql=
      "INSERT INTO tbl_md_jadwal SET
        KodeJadwal='$kode_jadwal',
        IdJurusan='$id_jurusan',
        JamBerangkat='$jam',
        JumlahKursi='$jumlah_kursi',
        FlagSubJadwal='$flag_sub_jadwal',
        KodeJadwalUtama='$kode_jadwal_utama',
        IsExpress='$is_express',
        FlagAktif='$flag_aktif',
        HariAktif='$hari_aktif'";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}

	function ubah($kode_jadwal_old, $kode_jadwal, $id_jurusan, $jam, $jumlah_kursi,$flag_sub_jadwal,$kode_jadwal_utama,$is_express, $flag_aktif,$hari_aktif){
	  
		/*
		ID	: 004
		IS	: data jadwal sudah ada dalam database
		FS	:Data jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		
		//MENGUBAH DATA KEDALAM DATABASE
		$sql ="CALL sp_jadwal_ubah(
			'$kode_jadwal_old','$kode_jadwal', $id_jurusan, 
			'$jam', '$jumlah_kursi',$flag_sub_jadwal,'$kode_jadwal_utama',
			'$is_express',$flag_aktif,'$hari_aktif');";

		if (!$db->sql_query($sql)){
			die_error("Err: $sql $this->ID_FILE".__LINE__);
		}

    //memeriksa tabel penjadwalan untuk memeriksa layout kursi

    $sql =
      "UPDATE tbl_penjadwalan_kendaraan SET LayoutKursi='$jumlah_kursi' WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(KodeJadwal)='$kode_jadwal' AND (KodeKendaraan IS NULL OR KodeKendaraan='')";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }
    

		
		return true;
	}
	
	function hapus($kode_jadwal){
	  
		/*
		ID	: 005
		IS	: data jadwal sudah ada dalam database
		FS	:Data jadwal dihapus
		*/
		
		//kamus
	
		global $db;
		
	
		//MENGHAPUS DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_jadwal
			WHERE KodeJadwal IN($kode_jadwal);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}

    //MENGUPDATE DATA YANG TERKONEKSI DENGAN DATA INI
    $sql =
      "UPDATE  tbl_md_jadwal SET KodeJadwalUtama=NULL,IsOnline=0,FlagSubJadwal=0,FlagAktif=0
			WHERE KodeJadwalUtama IN($kode_jadwal);";

    if (!$db->sql_query($sql)){
      return false;
      die_error("Err: $this->ID_FILE".__LINE__);
    }
		
		return true;
	}//end hapus
	
	function ubahStatusAktif($kode_jadwal){
	  
		/*
		ID	: 006
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql ="CALL sp_jadwal_ubah_status_aktif('$kode_jadwal');";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
	
	function setStatusAktif($status,$kode_jadwals){
	  
		/*
		ID	: 006
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql =
			"UPDATE tbl_md_jadwal SET
				FlagAktif='$status'
			WHERE KodeJadwal IN ($kode_jadwals);";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus

  function ubahStatusOnline($kode_jadwal){

    //kamus
    global $db;

    $sql ="UPDATE tbl_md_jadwal SET IsOnline=1-IsOnline WHERE KodeJadwal='$kode_jadwal';";

    if (!$db->sql_query($sql)){
      return false;
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }//end ubahStatusOnline

  function setStatusOnline($status,$kode_jadwals){

    //kamus
    global $db;

    $sql =
      "UPDATE tbl_md_jadwal SET
				IsOnline='$status'
			WHERE KodeJadwal IN ($kode_jadwals);";

    if (!$db->sql_query($sql)){
      return false;
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }//end setStatusOnline

	function ambilDataDetail($kode_jadwal){
		
		/*
		ID	:007
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				KodeJadwal,JamBerangkat,IdJurusan,
				JumlahKursi,FlagSubJadwal,KodeJadwalUtama,
				IsExpress,FlagAktif,HariAktif
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal';";		
		
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			
			$data_jadwal['KodeJadwal']			= $row['KodeJadwal'];
			$data_jadwal['JamBerangkat']		= $row['JamBerangkat'];
			$data_jadwal['IdJurusan']				= $row['IdJurusan'];
			$data_jadwal['JumlahKursi']			= $row['JumlahKursi'];
			$data_jadwal['FlagSubJadwal']		= $row['FlagSubJadwal'];
			$data_jadwal['KodeJadwalUtama']	= $row['KodeJadwalUtama'];
			$data_jadwal['IsExpress']				= $row['IsExpress'];
			$data_jadwal['FlagAktif']				= $row['FlagAktif'];
			$data_jadwal['HariAktif']				= $row['HariAktif'];

			//mengambil kodecabang asal dan tujuan
			$sql	=
			"SELECT KodeCabangAsal,KodeCabangTujuan 
			FROM tbl_md_jurusan 
			WHERE IdJurusan='$row[IdJurusan]'";
				
			if ($result = $db->sql_query($sql)){
				$row=$db->sql_fetchrow($result);
				$data_jadwal['Asal']		= $row['KodeCabangAsal'];
				$data_jadwal['Tujuan']	= $row['KodeCabangTujuan'];
			}                                   
			else{
				die_error("Err: $this->ID_FILE".__LINE__);
			}
			
			//mengambil nama cabang asal
			$sql	=
				"SELECT Nama
			  FROM tbl_md_cabang
			  WHERE KodeCabang='$data_jadwal[Asal]'";
			
			if ($result = $db->sql_query($sql)){
				$row=$db->sql_fetchrow($result);
				
				$data_jadwal['NamaAsal']	= $row['Nama'];
				
			} 
			else{
				die_error("Err: $this->ID_FILE".__LINE__);
			}
			
			//mengambil nama cabang tujuan
			$sql	=
				"SELECT Nama
			  FROM tbl_md_cabang
			  WHERE KodeCabang='$data_jadwal[Tujuan]'";
			
			if ($result = $db->sql_query($sql)){
				$row=$db->sql_fetchrow($result);
				
				$data_jadwal['NamaTujuan']	= $row['Nama'];
				
			} 
			else{
				die_error("Err: $this->ID_FILE".__LINE__);
			}
			
			return $data_jadwal;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function setComboJadwal($tgl,$id_jurusan){
		
		/*
		Desc	:Mengembalikan data Cabang sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $LEVEL_SUPERVISOR;
		global $userdata;
		
		if($id_jurusan=="") exit;
		
		if($userdata['user_level']>$LEVEL_SUPERVISOR){
			if($tgl==""){
				$sql = 
					"SELECT KodeJadwal,JamBerangkat,HariAktif
					FROM tbl_md_jadwal tmj
					WHERE IdJurusan=$id_jurusan 
					ORDER BY KodeJadwal,JamBerangkat;";
			}
			else{
				$sql = 
					"SELECT tmj.KodeJadwal,tmj.JamBerangkat,tmj.HariAktif
					FROM tbl_md_jadwal tmj 
						LEFT JOIN tbl_penjadwalan_kendaraan tpk 
						ON tpk.KodeJadwal=tmj.KodeJadwal AND tpk.TglBerangkat='$tgl'
					WHERE tmj.IdJurusan=$id_jurusan AND (tpk.StatusAktif=1 OR (tpk.StatusAktif IS NULL AND tmj.FlagAktif=1 AND tmj.HariAktif LIKE CONCAT('%',WEEKDAY('$tgl'),'%')))
					ORDER BY tmj.KodeJadwal,tmj.JamBerangkat;";
			}
		}
		else{
			$sql = 
				"SELECT KodeJadwal,JamBerangkat,HariAktif
			  FROM tbl_md_jadwal tmj
			  WHERE IdJurusan=$id_jurusan 
			  ORDER BY KodeJadwal,JamBerangkat;";
		}
		
		
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			#echo("Err: $this->ID_FILE $sql". __LINE__);
		}
		
	}//  END ambilData
	
	function setListJadwal($tgl,$id_jurusan){
		
		/*
		Desc	:Mengembalikan data Cabang sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $userdata;
		global $TOLERANSI_KEBERANGKATAN;
		global $LEVEL_CSO;
		
		if($id_jurusan=="") exit;
		
		if($tgl==""){
			$kondisi_status_aktif	= " AND FlagAktif=1 ";
		}
		else{
			$sql_penjadwalan	=
				"(SELECT StatusAktif
				FROM tbl_penjadwalan_kendaraan tpk
				WHERE tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat LIKE '$tgl')";
			
			$kondisi_status_aktif	= " AND ($sql_penjadwalan=1 OR ($sql_penjadwalan IS NULL AND FlagAktif=1)) ";
		}
		
		//CEK USERLEVEL
		if($userdata['user_level']>=$LEVEL_CSO){
			$kondisi_user_level	= " AND  TIME_TO_SEC(TIMEDIFF(CONCAT('$tgl',' ',JamBerangkat,':00'),NOW()))>=-($TOLERANSI_KEBERANGKATAN*60) ";
		}
		
		
		$sql = 
			"SELECT KodeJadwal,JamBerangkat
			  FROM tbl_md_jadwal tmj
			  WHERE IdJurusan=$id_jurusan 
					$kondisi_status_aktif $kondisi_user_level
			  ORDER BY JamBerangkat;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			#echo("Err: $this->ID_FILE $sql". __LINE__);
		}
		
	}//  END ambilData
	
	function ambilListSubKodeJadwalByKodeJadwal($kode_jadwal){
		
		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT KodeJadwal
			FROM tbl_md_jadwal
			WHERE KodeJadwalUtama='$kode_jadwal' AND FlagSubJadwal=1;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilListSubKodeJadwalByKodeJadwal
	
	function setSisaKursiJadwalNext($tgl,$id_jurusan,$jam_berangkat){
		
		/*
		Desc	:Mengembalikan data jadwal
		*/
		
		//kamus
		global $db;
		global $config;
		global $SESSION_TIME_EXPIRED;
		
		$sql = 
			"SELECT KodeJadwal,StatusAktif,LayoutKursi
				FROM tbl_penjadwalan_kendaraan
				WHERE 
					IdJurusan='$id_jurusan' 
					AND TglBerangkat LIKE '$tgl' 
					AND JamBerangkat>'$jam_berangkat' 
					AND StatusAktif=1";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err".__LINE__);
		}
		
		while ($row = $db->sql_fetchrow($result)){
			$data_penjadwalan[$row['KodeJadwal']]['StatusAktif'] 	=  $row['StatusAktif'];
			$data_penjadwalan[$row['KodeJadwal']]['LayoutKursi'] 	=  $row['LayoutKursi'];
		}
		
		$sql = 
			"SELECT KodeJadwal,JamBerangkat,JumlahKursi
			  FROM tbl_md_jadwal
			  WHERE 
					IdJurusan='$id_jurusan'
					AND JamBerangkat>'$jam_berangkat'
					AND FlagAktif=1
					AND RIGHT(JamBerangkat,2)=RIGHT('$jam_berangkat',2)
			  ORDER BY JamBerangkat
				LIMIT 0,".$config['range_show_sisa_kursi'].";";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err".__LINE__);
		}
		
		$idx_jadwal	= 0;
		while ($row = $db->sql_fetchrow($result)){
			$list_kode_jadwal	.= "'".$row['KodeJadwal']."',"; 
			$data_jadwal[$idx_jadwal]['KodeJadwal']		= $row['KodeJadwal'];
			$data_jadwal[$idx_jadwal]['JamBerangkat']	= $row['JamBerangkat'];
			$data_jadwal[$idx_jadwal]['JumlahKursi']		= $data_penjadwalan[$row['KodeJadwal']]['LayoutKursi']==''?$row['JumlahKursi']-1:$data_penjadwalan[$row['KodeJadwal']]['LayoutKursi']-1;
			$idx_jadwal++;
		}
	 
		//mengambil jumlah kursi kosong
		
		$list_kode_jadwal	= substr($list_kode_jadwal,0,-1);
		
		if($list_kode_jadwal!=''){
			$sql = 
				"SELECT KodeJadwal,COUNT(1) AS JumlahDipesan
				FROM tbl_posisi_detail
				WHERE 
					KodeJadwal IN ($list_kode_jadwal) 
					AND TglBerangkat='$tgl'
					AND ((HOUR(TIMEDIFF(SessionTime,NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW())))<=$SESSION_TIME_EXPIRED 
					OR StatusKursi!=0)
				GROUP BY KodeJadwal";

			if (!$result = $db->sql_query($sql)){
				die_error("Err".__LINE__);
			}
			
			while ($row = $db->sql_fetchrow($result)){
				$data_jum_kursi[$row['KodeJadwal']]['JumlahDipesan']		= $row['JumlahDipesan'];
				$test++;
			}
		}
		
		$return	= "";
		
		for($idx=0; $idx<$idx_jadwal; $idx++){
			//list status kursi
			$row = $db->sql_fetchrow($result);
			
			if($data_penjadwalan[$row['KodeJadwal']]['StatusAktif']==1 || $data_penjadwalan[$row['KodeJadwal']]['StatusAktif']==''){
				$jumlah_sisa_kursi	= $data_jadwal[$idx]['JumlahKursi']-$data_jum_kursi[$data_jadwal[$idx]['KodeJadwal']]['JumlahDipesan'];
				$jumlah_sisa_kursi	= $jumlah_sisa_kursi>=0?$jumlah_sisa_kursi:0;
				$jumlah_sisa_kursi	= $jumlah_sisa_kursi>0?substr("0".$jumlah_sisa_kursi,-2)." Kursi":"<font color='red'>HABIS</font>";
				$return	.="<b>".$data_jadwal[$idx]['KodeJadwal']."</b> | ".$data_jadwal[$idx]['JamBerangkat'].": <b>".$jumlah_sisa_kursi."</b> <br>";
			}                                                                                                                                                                                                                                      
		}	 

		
		
		
		return $return;
		
	}//  END setSisaKursiJadwalNext
}
?>