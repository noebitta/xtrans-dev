<?php
//
// CETAK TIKET UNTUK LINUX
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPaket.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassLogSMS.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){
	redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];
$cso							= $userdata['nama'];

$Reservasi	= new Reservasi();
$Paket			= new Paket();

//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
$row	= $Paket->ambilDataDetail($no_tiket);

$no_tiket=$row['NoTiket'];
$nama_pengirim=$row['NamaPengirim'];
$telp_pengirim=$row['TelpPengirim'];
$alamat_pengirim=$row['AlamatPengirim'];
$nama_penerima=$row['NamaPenerima'];
$telp_penerima=$row['TelpPenerima'];
$alamat_penerima=$row['AlamatPenerima'];
$tanggal=$row['TglBerangkat'];
$jam=$row['JamBerangkat'];
$asal=explode("(",$row['NamaAsal']);
$tujuan=explode("(",$row['NamaTujuan']);
$harga_paket=number_format($row['TotalBayar'],0,",",".");
$diskon_paket=number_format($row['Diskon'],0,",",".");
$total_bayar=number_format($row['TotalBayar'],0,",",".");
$operator=$row['NamaCSO'];
$cetak_tiket=$row['CetakTiket'];
$jenis_pembayaran=($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
$kode_jadwal=$row['KodeJadwal'];
$cara_bayar	= $row['CaraBayar'];
$id_jurusan	= $row['IdJurusan'];

$jenis_pembayaran	= $row['CaraPembayaran']==0?"TUNAI":"LANGGANAN";

$layanan	= $LIST_JENIS_LAYANAN_PAKET[$row['Layanan']];

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}

	var $javascript;
	var $n_js;

	function IncludeJS($script) {
		$this->javascript=$script;
	}

	function _putjavascript() {
		$this->_newobj();
		$this->n_js=$this->n;
		$this->_out('<<');
		$this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
		$this->_out('>>');
		$this->_out('endobj');
		$this->_newobj();
		$this->_out('<<');
		$this->_out('/S /JavaScript');
		$this->_out('/JS '.$this->_textstring($this->javascript));
		$this->_out('>>');
		$this->_out('endobj');
	}

	function _putresources() {
		parent::_putresources();
		if (!empty($this->javascript)) {
			$this->_putjavascript();
		}
	}

	function _putcatalog() {
		parent::_putcatalog();
		if (isset($this->javascript)) {
			$this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
		}
	}

	function AutoPrint($dialog=false)
	{
		//Embed some JavaScript to show the print dialog or start printing immediately
		$param=($dialog ? 'true' : 'false');
		$script="print($param);";
		$this->IncludeJS($script);
	}
}

//set kertas & file
$pdf=new PDF('P','cm','resipaket');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(0.5,0,0,0);
$pdf->SetFont('courier','',9);
$i=0;
$line_space	=0.3;

//TIKET==========

//content
$pdf->Ln(1.8);
$pdf->Cell(3.5,$line_space,strtoupper(substr($tujuan[0],0,20)).' ['.$layanan.']',0,0,'C');
$pdf->Cell(11.6,$line_space,"",0,0,'C');
$pdf->Cell(5.7,$line_space,$row['NoTiket'],0,0,'C');
//$pdf->Cell(0.2,$line_space,"",0,0,'C');
$pdf->Ln(0.4);
$pdf->Cell(20.8,$line_space,"Tanggal:".dateparse(FormatMySQLDateToTgl($tanggal))."|Jurusan: ".substr($asal[0],0,20)."-".substr($tujuan[0],0,20)."|Jadwal Kirim: ".$jam,0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(20.8,$line_space,"Jenis Barang:".$row['JenisBarang']."|Koli: ".$row['JumlahKoli']."|Berat: ".$row['Berat']." KG",0,0,'L');
$pdf->Ln(0.8);
$pdf->Cell(10.4,$line_space,"PENGIRIM",0,0,'L');
$pdf->Cell(10.4,$line_space,"PENERIMA",0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(10.4,$line_space,"Nama : ".$nama_pengirim,0,0,'L');
$pdf->Cell(10.4,$line_space,"Nama : ".$nama_penerima,0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(10.4,$line_space,"Alamat : ".$alamat_pengirim,0,0,'L');
$pdf->Cell(10.4,$line_space,"Alamat : ".$alamat_penerima,0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(10.4,$line_space,"Telp : ".$telp_pengirim,0,0,'L');
$pdf->Cell(10.4,$line_space,"Telp : ".$telp_penerima,0,0,'L');
$pdf->Ln(0.8);
$pdf->Cell(2,$line_space,"Harga :Rp. ",0,0,'L');
$pdf->Cell(2.2,$line_space,$harga_paket,0,0,'R');
$pdf->Cell(0.2,$line_space,"",0,0,'R');
$pdf->Cell(5,$line_space,"Isi : ".$row['KeteranganPaket'],0,0,'L');
$pdf->Cell(11.4,0.1,"Dengan menyetujui resi ini saya selaku pengirim telah",0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(2,$line_space,"Discount :Rp. ",0,0,'L');
$pdf->Cell(2.2,$line_space,$diskon_paket,0,0,'R');
$pdf->Cell(0.2,$line_space,"",0,0,'R');
$pdf->Cell(5,$line_space,"Ins.Khusus : ".$row['InstruksiKhusus'],0,0,'L');
$pdf->Cell(11.4,0.1,"membaca, memahami dan menyetujuui seluruh syarat",0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(4.2,$line_space,"----------------------",0,0,'L');
$pdf->Cell(0.2,$line_space,"",0,0,'R');
$pdf->Cell(5,$line_space,"",0,0,'L');
$pdf->Cell(11.4,0.1,"pengiriman yang tertera pada bagian belakang resi ini",0,0,'L');
$pdf->Ln(0.2);
$pdf->Cell(2,$line_space,"Total :Rp. ",0,0,'L');
$pdf->Cell(2.2,$line_space,$total_bayar,0,0,'R');
$pdf->Cell(1,$line_space,"",0,0,'R');
$pdf->Ln(0.6);
$pdf->Cell(10.4,$line_space,"Jenis Pembayaran:".$jenis_pembayaran." | Waktu Cetak:".dateparseWithTime(date("d-m-Y H:i:s")),0,0,'L');
$pdf->Ln(0.6);
$pdf->Cell(4.16,$line_space,"CSO Asal",0,0,'C');
$pdf->Cell(4.16,$line_space,"Pengirim",0,0,'C');
$pdf->Cell(4.16,$line_space,"CSO Tujuan",0,0,'C');
$pdf->Cell(4.16,$line_space,"Penerima",0,0,'C');
$pdf->Cell(4.16,$line_space,"KTP:",0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"Telp:",0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"Tgl:",0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"Jam:",0,0,'L');
$pdf->Ln(0.3);
$pdf->Cell(4.16,$line_space,$userdata['nama'],0,0,'C');
$pdf->Cell(4.16,$line_space,$nama_pengirim,0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'C');
$pdf->Cell(4.16,$line_space,"",0,0,'L');

//mengupate flag cetak tiket
if($cetak_tiket!=1){

	$cabang_transaksi	=($cara_bayar<$PAKET_CARA_BAYAR_DI_TUJUAN)?$userdata['KodeCabang']:"";
	$Paket->updateCetakTiket($no_tiket,$jenis_pembayaran,$cabang_transaksi);

	//SMS REMINDER
	$kirim_sms=false;
	if($kirim_sms){
		$LogSMS	= new LogSMS();

		//mengambil asal dan tujuan

		$asal		= substr($asal[0],0,10);
		$tujuan	= substr($tujuan[0],0,10);

		//Mengirim kepada penerima
		if(in_array(substr($telp_penerima,0,2),$HEADER_NO_TELP)){

			$isi_pesan	= "SDR/I ".strtoupper(substr($nama_pengirim,0,10))." MENGIRIMKAN PAKET UNTUK ANDA PADA ".dateparse(FormatMySQLDateToTgl($tanggal))." $jam RESI $no_tiket.PAKET DAPAT DIAMBIL DI XTRANS $tujuan. INFO 0213149777";

			$telepon	= "62".substr($telp_penerima,1);
			$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";

			$response	= sendHttpPost($sms_config['url'],$parameter);

			if($response=="00"){
				$LogSMS->tambah(
					$telp_penerima, $nama_penerima, 2,
					$no_tiket, $isi_pesan);
			}
		}
	}

	//Mengirim kepada pengirim
	/*if(in_array(substr($telp_pengirim,0,2),$HEADER_NO_TELP)){

        $isi_pesan	= "TERIMA KASIH SDR/I ".strtoupper(substr($nama_pengirim,0,10))." SUDAH MENGIRIMKAN PAKET PADA ".dateparse(FormatMySQLDateToTgl($tanggal))." $jam RESI:$no_tiket.PAKET DIKIRIM KE BIMOTRANS $tujuan. $keterangan_bayar,INFO 02168636880/02270366326";

        $telepon	= "62".substr($telp_pengirim,1);
        $parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";
        //$response	= sendHttpPost($sms_config['url'],$parameter);

        /*if($response=="00"){
            $LogSMS->tambah(
                $telp_pengirim, $nama_pengirim, 2,
                $no_tiket, $isi_pesan);
        }*/


	//}

}

$pdf->AutoPrint(true);
$pdf->Output();

?>