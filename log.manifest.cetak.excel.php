<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){
	redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$kota_asal			= isset($HTTP_GET_VARS['kota_asal'])? $HTTP_GET_VARS['kota_asal'] : $HTTP_POST_VARS['kota_asal'];
$cabang_asal		= isset($HTTP_GET_VARS['cabang_asal'])? $HTTP_GET_VARS['cabang_asal'] : $HTTP_POST_VARS['cabang_asal'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir'];
$id_jurusan  	= isset($HTTP_GET_VARS['id_jurusan'])? $HTTP_GET_VARS['id_jurusan'] : $HTTP_POST_VARS['id_jurusan'];
$jam  					= isset($HTTP_GET_VARS['jam'])? $HTTP_GET_VARS['jam'] : $HTTP_POST_VARS['jam'];
$sort_by				= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

$kota_asal	= ($kota_asal!="")?$kota_asal:"BANDUNG";

//INISIALISASI

$kondisi	= " WHERE (TglBerangkat BETWEEN '$tanggal_mulai' AND '$tanggal_akhir') ";
$kondisi	.=" AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang=(f_jurusan_get_kode_cabang_asal_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal(KodeJadwal))))='$kota_asal'";
//$kondisi	.= ($jam =="") ? "" : "AND KodeJadwal = '$jam'";

if($cabang_asal=='0'){
	$kondisi	.= "";
}
else{

	if($id_jurusan=='0'){
		$kondisi	.= " AND (SELECT KodeCabangAsal FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal(t1.KodeJadwal)) LIKE '$cabang_asal' ";
	}
	else{
		if($jam=='0'){
			$kondisi	.= " AND f_jadwal_ambil_id_jurusan_by_kode_jadwal(t1.KodeJadwal) LIKE '$id_jurusan' ";
		}
		else{
			$kondisi	.= " AND t1.KodeJadwal LIKE '$jam' ";
		}
	}
}

$sql=
	"SELECT 
		*
	FROM tbl_log_cetak_manifest t1
	$kondisi
	ORDER BY TglBerangkat DESC, KodeJadwal ASC, JamBerangkat DESC, NoSPJ ASC, CetakanKe DESC";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__.$sql);exit;
}
$idx = 0;
while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['NoSPJ']									= $row['NoSPJ'];
	$temp_array[$idx]['KodeJadwal']								= $row['KodeJadwal'];
	$temp_array[$idx]['TglBerangkat']							= $row['TglBerangkat'];
	$temp_array[$idx]['JamBerangkat']							= $row['JamBerangkat'];
	$temp_array[$idx]['Layout']									= $row['JumlahKursiDisediakan'];
	$temp_array[$idx]['Kendaraan']								= $row['NoPolisi'];
	$temp_array[$idx]['Sopir']									= $row['Driver'];
	$temp_array[$idx]['Penumpang']								= $row['JumlahPenumpang'];
	$temp_array[$idx]['Paket']									= $row['JumlahPaket'];
	$temp_array[$idx]['OmzetPenumpang']							= $row['TotalOmzet'];
	$temp_array[$idx]['OmzetPaket']								= $row['TotalOmzetPaket'];
	$temp_array[$idx]['Cetakan']								= $row['CetakanKe'];
	$temp_array[$idx]['Pencetak']								= $row['NamaUserPencetak'];
	$temp_array[$idx]['WaktuCetak']								= $row['WaktuCetak'];
	$idx++;
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:P2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Log Cetak Ulang Manifest Tanggal'.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'NoSPJ');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'KodeJadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Tgl Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jam Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Layout');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Kendaraan');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Sopir');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Omzet Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Omzet Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Cetakan');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Jenis');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Pencetak');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'WaktuCetak');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Keterlambatan');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);

$idx=0;
$idx_row=4;

while ($idx<count($temp_array)){

	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx_kota);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['NoSPJ']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['TglBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$idx]['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$idx]['Layout']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $temp_array[$idx]['Kendaraan']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $temp_array[$idx]['Sopir']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($temp_array[$idx]['Penumpang'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($temp_array[$idx]['Paket'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($temp_array[$idx]['OmzetPenumpang'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($temp_array[$idx]['OmzetPaket'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $temp_array[$idx]['Cetakan']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $temp_array[$idx]['Jenis']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $temp_array[$idx]['Pencetak']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $temp_array[$idx]['WaktuCetak']);


	$idx_row++;
	$idx++;
}

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Laporan Log Cetak Manifest Per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

?>
