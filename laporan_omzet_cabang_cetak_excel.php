<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_MANAJER,$LEVEL_STAFF_KEUANGAN,$LEVEL_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tbl_reservasi	= $is_today=="1" || $is_today==""?"tbl_reservasi":"tbl_reservasi_olap";

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

/*if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND KodeCabangAsal='$userdata[KodeCabang]'";	
}*/			

$kondisi_cari	.= $kondisi_cabang;
		
$sql=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari
	ORDER BY Kota,Nama";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		tmj.KodeCabangAsal AS KodeCabang,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,

		IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB,
		
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		
		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),tr.HargaTiket)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM $tbl_reservasi tr LEFT JOIN tbl_md_jurusan tmj ON tr.IdJurusan=tmj.IdJurusan
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY KodeCabangAsal ORDER BY KodeCabangAsal ";
		
	
if (!$result = $db->sql_query($sql))
{
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Nama']								= $row['Nama'];
	$temp_array[$idx]['Alamat']							= $row['Alamat'];
	$temp_array[$idx]['Kota']								= $row['Kota'];
	$temp_array[$idx]['Telp']								= $row['Telp'];
	$temp_array[$idx]['Fax']								= $row['Fax'];
	$temp_array[$idx]['hp']									= $row['hp'];
	$temp_array[$idx]['TotalPenumpangU']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangG']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangT']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangT'];
	$temp_array[$idx]['TotalPenumpangRB']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangRB'];
	$temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangVR']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangVR'];
	$temp_array[$idx]['TotalBerangkat']			= $data_tiket_total[$row['KodeCabang']]['TotalBerangkat'];
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalKomisiOnline']					= $data_tiket_total[$row['KodeCabang']]['TotalKomisiOnline'];
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['OmzetNet']						= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket']-$data_tiket_total[$row['KodeCabang']]['TotalDiscount']-$data_tiket_total[$row['KodeCabang']]['TotalKomisiOnline'];
	$temp_array[$idx]['TotalPenumpangPerTrip']= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	
	$idx++;
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Penumpang Cabang per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Kode');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Alamat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Total Umum');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Total Mahasiswa');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Total Khusus');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Total Keluarga Karyawan');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Total Gratis');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Total Tiketux Online');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Total Red Bus');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Total Return');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Total Voucher');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Total Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'Total Penumpang/Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Total Omzet');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('R3', 'Disc');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('S3', 'Omzet Net');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);

$idx=0;
$idx_row=4;
$kota_terakhir		= "";
$gt_trip			= 0;
$gt_pnp_u			= 0;
$gt_pnp_m			= 0;
$gt_pnp_k			= 0;
$gt_pnp_kk			= 0;
$gt_pnp_g			= 0;
$gt_pnp_o			= 0;
$gt_pnp_r			= 0;
$gt_pnp_rb			= 0;
$gt_pnp_vr			= 0;
$gt_pnp_t			= 0;
$gt_omz_pnp			= 0;
$gt_disc			= 0;
$gt_omzet_net		= 0;

while ($idx<count($temp_array)){
	
	if($kota_terakhir!=$temp_array[$idx]['Kota'] || $idx==count($temp_array)){
		$kota_terakhir						= $temp_array[$idx]['Kota'];
		$idx_kota									= 1;
		
		if($idx>0){
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':D'.$idx_row);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, "SUB TOTAL");
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($total_trip_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($total_pnp_u_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($total_pnp_m_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($total_pnp_k_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($total_pnp_kk_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($total_pnp_g_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($total_pnp_o_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($total_pnp_rb_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($total_pnp_r_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($total_pnp_vr_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($total_pnp_t_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($total_omz_pnp_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($total_disc_perkota,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($total_omzet_net_perkota,0,"",""));
			
			$idx_row++;
	
		}
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':R'.$idx_row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $temp_array[$idx]['Kota']);
		$idx_row++;
		
		$total_trip_perkota				= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perkota			= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perkota			= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perkota			= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perkota			= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perkota			= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perkota			= $temp_array[$idx]['TotalPenumpangT'];
		$total_pnp_rb_perkota			= $temp_array[$idx]['TotalPenumpangRB'];
		$total_pnp_r_perkota			= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perkota			= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_t_perkota			= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perkota		= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perkota				= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perkota	= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];

		
	}
	else{
		$nama_kota								= "";
		$idx_kota++;
		$total_trip_perkota				+= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perkota			+= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perkota			+= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perkota			+= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perkota			+= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perkota			+= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perkota			+= $temp_array[$idx]['TotalPenumpangT'];
		$total_pnp_rb_perkota			+= $temp_array[$idx]['TotalPenumpangRB'];
		$total_pnp_r_perkota			+= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perkota			+= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_t_perkota			+= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perkota		+= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perkota				+= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perkota	+= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
	}
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx_kota);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Area']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($temp_array[$idx]['TotalBerangkat'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangU'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangM'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangK'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangKK'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangG'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangT'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangRB'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangR'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangVR'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($temp_array[$idx]['TotalTiket'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($temp_array[$idx]['TotalPenumpangPerTrip'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($temp_array[$idx]['TotalPenjualanTiket'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($temp_array[$idx]['TotalDiscount'],0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, number_format($temp_array[$idx]['OmzetNet'],0,"",""));
	
	
	$gt_trip			+= $temp_array[$idx]['TotalBerangkat'];
	$gt_pnp_u			+= $temp_array[$idx]['TotalPenumpangU'];
	$gt_pnp_m			+= $temp_array[$idx]['TotalPenumpangM'];
	$gt_pnp_k			+= $temp_array[$idx]['TotalPenumpangK'];
	$gt_pnp_kk			+= $temp_array[$idx]['TotalPenumpangKK'];
	$gt_pnp_g			+= $temp_array[$idx]['TotalPenumpangG'];
	$gt_pnp_o			+= $temp_array[$idx]['TotalPenumpangT'];
	$gt_pnp_rb			+= $temp_array[$idx]['TotalPenumpangRB'];
	$gt_pnp_r			+= $temp_array[$idx]['TotalPenumpangR'];
	$gt_pnp_vr			+= $temp_array[$idx]['TotalPenumpangVR'];
	$gt_pnp_t			+= $total_tiket;
	$gt_omz_pnp			+= $total_penjualan_tiket;
	$gt_disc			+= $total_discount;
	$gt_omzet_net		+= $temp_array[$idx]['OmzetNet'];
	
	$idx_row++;
	$idx++;
}

$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':D'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, "SUB TOTAL");
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($total_trip_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($total_pnp_u_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($total_pnp_m_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($total_pnp_k_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($total_pnp_kk_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($total_pnp_g_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($total_pnp_o_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($total_pnp_rb_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($total_pnp_r_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($total_pnp_vr_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($total_pnp_t_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($total_omz_pnp_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($total_disc_perkota,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($total_omzet_net_perkota,0,"",""));

$idx_row++;	
$temp_idx=$idx_row;

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':D'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'GRAND TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $gt_trip);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $gt_pnp_u);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $gt_pnp_m);
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $gt_pnp_k);
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $gt_pnp_kk);
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $gt_pnp_g);
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $gt_pnp_o);
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $gt_pnp_rb);
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $gt_pnp_r);
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $gt_pnp_vr);
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $gt_pnp_t);
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $gt_omz_pnp);
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $gt_disc);
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, $gt_omzet_net);
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet Penumpang Cabang per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
