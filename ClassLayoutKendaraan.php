<?php

class LayoutKendaraan{
	
	//BODY
	
	var $kode_booking_dipilih; //variable untuk menampung kode booking yang dipilih. digunakan untuk membuat group ketika dipilih
	
	function MakeImageByStatus($num,$status,$status_bayar,$session_id_old,$nama,$t,$user_level,$kode_booking,$sub_jadwal=0,$petugas_penjual){
	  global $userdata;
		global $LEVEL_ADMIN;
		global $LEVEL_SUPERVISOR;
		global $LEVEL_KOORDINATOR;
		global $LEVEL_AGEN;
		global $db;
		
		$user_level=$userdata['user_level'];
		$session_id=$userdata['user_id'];
		
		$nama	=substr($nama,0,10);
		
	  if ($status==0) {
			//kursi masih kosong
			
			$layout_kursi ="<div class='kursi_kosong' onclick='updateTemp($num)'>$num</div>";
			
	  } else
		if ($status==1 && (trim($t)=="")){
	    // Jika kursi sudah di flag
			
			if($session_id==$session_id_old){
			  //jika select memang benar dilakukan oleh user ini
					
					$layout_kursi ="<div class='kursi_dipilih' onclick='updateTemp($num)'>$num</div>";
			}
			else{
			  //jika select dilakukan bukan oleh user ini, maka user ini tidak berhak membatalkannya
		
				//if(in_array($user_level,array($LEVEL_ADMIN,$LEVEL_SUPERVISOR,$LEVEL_KOORDINATOR))){
				//update tanggal 5 mei 2011, siapapun tidak diperbolehkan membatalkan kursi yang sedang dipilih kecuali admin
				if($user_level==$LEVEL_ADMIN){
					//jika  admin/supervisor, berhak membatalkan transaksi orang lain
						
					$layout_kursi =
						"<div class='kursi_block' onclick='updateTemp($num)'>
							$num
							<div class='kursi_warning'>Kursi tidak dapat diakses!</div>
						</div>";
				}
				else{
					//jika bukan admin/ supervisor, tidak dapat membatalkan transaksi orang lain
					$layout_kursi =
						"<div class='kursi_block'>
							$num<br>
							<div class='kursi_warning'>Kursi tidak dapat diakses!</div>
						</div>";
					
				}//endif ($user_level==$LEVEL_ADMIN || $user_level==$LEVEL_SUPERVISOR)

			}//endif $array_kursi[$num]==$userdata['session_id']
		
	  } else
	  if ($status==1 && (trim($t)!="")){
	    // Jika kursi sudah di booking
			
			if($status_bayar!=1){
				//jika tiket belum dibayar
									
				if($petugas_penjual!=0){
					$tombol_cetak_tiket	= "<input type='button' onclick=\"cetakTiketCepat('$kode_booking');\" value='Tiket'> $num<br>";
					$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_booking".$sub_jadwal:"kursi_booking_dipilih".$sub_jadwal;
				}
				else{
					$tombol_cetak_tiket	= "<br>";
					$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_booking".$sub_jadwal."_ttx":"kursi_booking_dipilih".$sub_jadwal."_ttx";
				}
				
				$layout_kursi =
					"<div onclick='showChair($num,\"$t\");' class='$class_kursi'>
					$tombol_cetak_tiket
					<div class='kursi_nama'>$nama</div>
						
					</div>";
				
			}
			else{
				//jika kursi sudah dibayar
				
				if($user_level!=$LEVEL_AGEN){
					//jika  bukan member, dapat melihat detail transaksi
					
					if($petugas_penjual!=0){
						$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_bayar".$sub_jadwal:"kursi_bayar_dipilih".$sub_jadwal;
					}
					else{
						$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_bayar".$sub_jadwal."_ttx":"kursi_bayar_dipilih".$sub_jadwal."_ttx";
					}
					
					$layout_kursi =
						"<div onclick='showChair($num,\"$t\");' class='$class_kursi'>
							$num<br>
							<div class='kursi_nama'>$nama</div>
						</div>";
				}
				else{
					exit;
				}
			}
		
		}
		return $layout_kursi;
	}
	
	function setLayoutKursi($tgl_berangkat,$kode_jadwal,$kode_sub_jadwal,$layout_kursi,$nama_sopir){
		global $userdata;
		global $db;
		global $LEVEL_ADMIN;
		global $LEVEL_SUPERVISOR;
		global $LEVEL_KOORDINATOR;
		global $LEVEL_AGEN;
		global $SESSION_TIME_EXPIRED;
		
		//INISIALISASI
		$layout_kendaraan =
			"<br><br>
			<img src='./templates/images/icon_warning.png' />
			<font color='red'><h3>Layout Kendaraan $layout_kursi kursi tidak ditemukan!</h3>
			</font>
			<br><br>";
		
		//mengambil data user dan levelnya
		$user_id  				= $userdata['user_id']; 
		$user_level				= $userdata['user_level'];
		$nama_sopir				=	substr($nama_sopir,0,7);
		
		// Mengambil data dari tbPosisidetail
		/*$sql = 
			"SELECT *
			FROM tbl_posisi_detail
			WHERE 
				KodeJadwal LIKE '$kode_jadwal' 
				AND TglBerangkat='$tgl_berangkat'
				AND (f_reservasi_session_time_selisih(SessionTime)<=$SESSION_TIME_EXPIRED 
				OR NoTiket!='' 
				OR NoTiket IS NOT NULL)";*/
		
		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] ,(int) $temp_array_date[2]+2,(int) $temp_array_date[0]);
		
		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) < date('Y-m-d');
		
		if($tanggal_lampau!=1){
			$nama_tbl 					= "tbl_posisi_detail";
			$nama_tbl_reservasi = "tbl_reservasi";
		}
		else{
			$nama_tbl						= "tbl_posisi_detail_backup";
			$nama_tbl_reservasi				= "tbl_reservasi_olap";
		}
		
		$sql = 
			"SELECT *
			FROM $nama_tbl
			WHERE 
				KodeJadwal LIKE '$kode_jadwal' 
				AND TglBerangkat='$tgl_berangkat'
				AND ((HOUR(TIMEDIFF(SessionTime,NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW())))<=$SESSION_TIME_EXPIRED 
				OR NoTiket!='' 
				OR NoTiket IS NOT NULL)";
		
		$list_no_tiket	= "";
		
		if ($result = $db->sql_query($sql)){
		
			while ($row = $db->sql_fetchrow($result)){
					//list status kursi
					$nomor_kursi	= $row['NomorKursi'];
					
					$status_kursi[$nomor_kursi]	= $row['StatusKursi'];	
					$no_tiket[$nomor_kursi]			= $row['NoTiket'];	
					$nama[$nomor_kursi]					= $row['Nama'];		
					$session[$nomor_kursi]			= $row['Session'];	
					$status_bayar[$nomor_kursi]	= $row['StatusBayar'];		                                                                                                                                                                                                                                                
					$kode_booking[$nomor_kursi]	= $row['KodeBooking'];	

					$list_no_tiket	.= "'".$row['NoTiket']."',";
			}	 
		} 
		else{	  
			//die_error('Cannot Load Transaksi');//,__LINE__,__FILE__,$sql);
			echo("Err:".__LINE__);
			exit;
		}
		
		$list_no_tiket	= substr($list_no_tiket,0,-1);
		
		if($list_no_tiket!=""){
			$sql = 
				"SELECT NomorKursi,KodeJadwal,PetugasPenjual
				FROM $nama_tbl_reservasi
				WHERE NoTiket IN ($list_no_tiket)";
			
			if (!$result = $db->sql_query($sql)){
				//die_error('Cannot Load Transaksi');//,__LINE__,__FILE__,$sql);
				echo("Err:".__LINE__);
				exit;
			}
			
			while ($row = $db->sql_fetchrow($result)){
				//list status kursi
				$nomor_kursi	= $row['NomorKursi'];
				
				$sub_jadwal[$nomor_kursi]				= ($kode_sub_jadwal==$row['KodeJadwal'])?0:1;	
				$petugas_penjual[$nomor_kursi]	= $row['PetugasPenjual'];
			}	
		}
		
		//PEMILIHAN LAYOUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		switch($layout_kursi){
			
			// Layout mobil dengan 9 kursi ==========================================================================================
			case 9:
				/*layout kursi
					[1][9][SOP]
					[2] 	[3] 
					[4] 	[5] 
					[6][7][8]
				*/
				
				//BARIS [1][9][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi9'>".
								$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2] 	[3] 
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".     
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [4] 	[5] 
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
				</tr>";          

				//BARIS [6][7][8]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
				</tr>";          				
				
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 11 kursi ==========================================================================================
			case 10:
				/*layout kursi
					[1][11][SOP]
					[2] [3] [4]
					[  ] [6] [7]
					[8][9][10]
				*/
					  
				//BARIS [1][11][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi11'>".
								$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2][3][4]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [5][6][7]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>&nbsp;</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
				</tr>";          

				//BARIS [8][9][10]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
				</tr>";   
			
				$layout_kendaraan.=
				"</table>";
				
		  break;
		
		// Layout mobil dengan 11 kursi ==========================================================================================
			case 101:
				/*layout kursi
					[1][11][SOP]
					[2] 		[3]
					[4]			[5]
					[6] 		[7]
					[8][9][10]
				*/
					  
				//BARIS [1][11][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi11'>".
								$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2]	[3]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>&nbsp;</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [4]		[5]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
					<td class='layout_kursi'>&nbsp;</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
				</tr>";          

				//BARIS [6]		[7]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
					<td class='layout_kursi'>&nbsp;</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
				</tr>";   
				
				//BARIS [8][9][10]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
				</tr>";   
				
				$layout_kendaraan.=
				"</table>";
				
		  break;
			
			// Layout mobil dengan 11 kursi ==========================================================================================
			case 11:
				/*layout kursi
					[1][11][SOP]
					[2] [3] [4]
					[5] [6] [7]
					[8][9][10]
				*/

				//BARIS [1][11][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi11'>".
								$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2][3][4]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [5][6][7]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
				</tr>";          

				//BARIS [8][9][10]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
				</tr>";   
			
				$layout_kendaraan.=
				"</table>";
				
		  break;

      // Layout mobil dengan 11 premium kursi ==========================================================================================
      case 111:
        /*layout kursi
          [1][11][SOP]
          [2] [3] [4]
          [5] [6] [7]
          [8][9][10]
        */

        //BARIS [1][SOP]
        $layout_kendaraan =
          "
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan='4' bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
              $this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
              "</div>
						</td>
						<td class='layout_kursi' colspan='2'>
							&nbsp;
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";

        //BARIS  X[2][3][4]
        $layout_kendaraan.=
          "<tr valign='center'>
          <td class='layout_kursi'>
            &nbsp;
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
          $this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
          "</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
          $this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
          "</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
          $this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
          "</div>
					</td>
				</tr>";

        //BARIS [5]X[6][7]
        $layout_kendaraan.=
          "<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
          $this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
          "</div>
					</td>
					<td class='layout_kursi'>
            &nbsp;
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
          $this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
          "</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
          $this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
          "</div>
					</td>
				</tr>";

        //BARIS [8][9][10][11]
        $layout_kendaraan.=
          "<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
          $this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
          "</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
          $this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
          "</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
          $this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
          "</div>
					</td>
					<td>
					  <div id='rewrite_kursi11'>".
              $this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
            "</div>
					</td>
				</tr>";

        $layout_kendaraan.=
          "</table>";

        break;

			// Layout mobil dengan 12 kursi ==========================================================================================
			case 12:
				/*layout kursi
					[1][12][SOP]
					    [2] [3] 
					[4] [5] [6]
					    [7][8]
					[9][10][11]
				*/
				
				//BARIS [1][12][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi12'>".
								$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2][3]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>&nbsp;</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [4][5][6]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
				</tr>";          

				//BARIS [7][8]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>&nbsp;</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
				</tr>";   

				//BARIS [9][10][11]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi11'>".
							$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
						"</div>
					</td>
				</tr>";    				
				
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 13 kursi ==========================================================================================
			case 131:
				/*layout kursi
					[1][13][SOP]
					[2] [3] [4]
					[5] [6] [7]
					[8][9][10]
					[11][12]
				*/
				
				//BARIS [1][13][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi13'>".
								$this->MakeImageByStatus(13,$status_kursi[13],$status_bayar[13],$session[13],$nama[13],$no_tiket[13],$user_level,$kode_booking[13],$sub_jadwal[13],$petugas_penjual[13]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2][3][4]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [5][6][7]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
				</tr>";          

				//BARIS [8][9][10]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
				</tr>";   

				//BARIS [11][12]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi11'>".
							$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi12'>".
							$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
						"</div>
					</td>
					<td class='layout_kursi'>&nbsp;</td>
				</tr>";    				
				
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 12B kursi ==========================================================================================
			case 132:
				/*layout kursi
					[1][13][SOP]
						[2] [3] 	
					[4] [5] [6] 
						[7][8]
					[9][10][11][12]
				*/
				
				//BARIS [1][13][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi13'>".
								$this->MakeImageByStatus(13,$status_kursi[13],$status_bayar[13],$session[13],$nama[13],$no_tiket[13],$user_level,$kode_booking[13],$sub_jadwal[13],$petugas_penjual[13]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2][3]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [4][5][6]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
				</tr>";                                                                                       

				//BARIS [7][8]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
				</tr>";   

				//BARIS [9][10][11][12]
				$layout_kendaraan.=
				"<tr valign='center'><td colspan=3>
				<table width='100%'>
					<tr>
						<td class='layout_kursi'>
							<div id='rewrite_kursi9'>".
								$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi10'>".
								$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi11'>".
								$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi12'>".
								$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
							"</div>
						</td>
					</tr>
				</table>
				</td></tr>";    				
				
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 13C kursi ==========================================================================================
			case 133:
				/*layout kursi
					[1][13][SOP]
					[2][3][4]
					[5][6][7]
					   [8][9]
					[10][11][12]
				*/
				
				//BARIS [1][13][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi13'>".
								$this->MakeImageByStatus(13,$status_kursi[13],$status_bayar[13],$session[13],$nama[13],$no_tiket[13],$user_level,$kode_booking[13],$sub_jadwal[13],$petugas_penjual[13]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2][3][4]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [5][6][7]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
				</tr>";                                                                                       

				//BARIS [8][9]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
					<td class='layout_kursi'>
							<div id='rewrite_kursi9'>".
								$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
							"</div>
						</td>
				</tr>";   

				//BARIS [10][11][12]
				$layout_kendaraan.=
				"<tr valign='center'><td colspan=3>
				<table width='100%'>
					<tr>
						<td class='layout_kursi'>
							<div id='rewrite_kursi10'>".
								$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi11'>".
								$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi12'>".
								$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
							"</div>
						</td>
					</tr>
				</table>
				</td></tr>";    				
				
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 12 kursi ==========================================================================================
			case 14:
				/*layout kursi
					[1][14][SOP]
					[2] [3] [4]
					[5] [6] [7]
					[8][9][10]
					[11][12][13]
				*/
				
				//BARIS [1][13][SOP]
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0' border=0>
					<tr>
						<td valign='center' align='center' colspan=3 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
					</tr>
					<tr valign='center'>
						<td class='layout_kursi'>
							<div id='rewrite_kursi1'>".
								$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
							"</div>
						</td>
						<td class='layout_kursi'>
							<div id='rewrite_kursi14'>".
								$this->MakeImageByStatus(14,$status_kursi[14],$status_bayar[14],$session[14],$nama[14],$no_tiket[14],$user_level,$kode_booking[14],$sub_jadwal[14],$petugas_penjual[14]).
							"</div>
						</td>
						<td class='kursi_sopir'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  [2][3][4]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
				</tr>";                                                                                       
				
				//BARIS [5][6][7]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
				</tr>";          

				//BARIS [8][9][10]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
				</tr>";   

				//BARIS [11][12]
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi11'>".
							$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi12'>".
							$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi13'>".
							$this->MakeImageByStatus(13,$status_kursi[13],$status_bayar[13],$session[13],$nama[13],$no_tiket[13],$user_level,$kode_booking[13],$sub_jadwal[13],$petugas_penjual[13]).
						"</div>
					</td>
				</tr>";    				
				
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 19 kursi ==========================================================================================
			case 19:
				
				//BARIS 			sopir
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0'>
					<tr valign='center'>
						<td valign='center' align='center' colspan=2 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
						<td class='kursi_sopir' colspan='2'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  1
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi1'>".
							$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
						"</div>
					</td>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 2
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
						$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
						$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
						"</div>
					</td>
				</tr>";                                                                                        
				
				//BARIS  3
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi' colspan=2><H2>PINTU =></H2></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".		
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).
						"</div>
					</td>
				</tr>";                                                                                       

				//BARIS 4
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).
						"</div>
					</td>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".					
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi11'>".
							$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
						"</div>
					</td>
				</tr>";                     
				
				//BARIS 5
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi12'>".
							$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
						"</div>
					</td>
					<td class='layout_kursi'></td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi13'>".
							$this->MakeImageByStatus(13,$status_kursi[13],$status_bayar[13],$session[13],$nama[13],$no_tiket[13],$user_level,$kode_booking[13],$sub_jadwal[13],$petugas_penjual[13]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi14'>".
							$this->MakeImageByStatus(14,$status_kursi[14],$status_bayar[14],$session[14],$nama[14],$no_tiket[14],$user_level,$kode_booking[14],$sub_jadwal[14],$petugas_penjual[14]).
						"</div>
					</td>
				</tr>";     

				//BARIS 6
				$layout_kendaraan.=
				"<tr valign='center'>
					<td colspan=4>
						<table width='100%' cellspacing='2' cellpadding='0'>
							<tr>
								<td class='layout_kursi'>
									<div id='rewrite_kursi15'>".
										$this->MakeImageByStatus(15,$status_kursi[15],$status_bayar[15],$session[15],$nama[15],$no_tiket[15],$user_level,$kode_booking[15],$sub_jadwal[15],$petugas_penjual[15]).                                                                   
									"</div>
								</td>
								<td class='layout_kursi'>
									<div id='rewrite_kursi16'>".
										$this->MakeImageByStatus(16,$status_kursi[16],$status_bayar[16],$session[16],$nama[16],$no_tiket[16],$user_level,$kode_booking[16],$sub_jadwal[16],$petugas_penjual[16]).
									"</div>
								</td>
								<td class='layout_kursi'>
									<div id='rewrite_kursi17'>".
										$this->MakeImageByStatus(17,$status_kursi[17],$status_bayar[17],$session[17],$nama[17],$no_tiket[17],$user_level,$kode_booking[17],$sub_jadwal[17],$petugas_penjual[17]).
									"</div>
								</td>
								<td class='layout_kursi'>
									<div id='rewrite_kursi18'>".
										$this->MakeImageByStatus(18,$status_kursi[18],$status_bayar[18],$session[18],$nama[18],$no_tiket[18],$user_level,$kode_booking[18],$sub_jadwal[18],$petugas_penjual[18]).
									"</div>
								</td>
								<td class='layout_kursi'>
									<div id='rewrite_kursi19'>".
										$this->MakeImageByStatus(19,$status_kursi[19],$status_bayar[19],$session[19],$nama[19],$no_tiket[19],$user_level,$kode_booking[19],$sub_jadwal[19],$petugas_penjual[19]).
									"</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>";                                                                                            
        
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 29 kursi ==========================================================================================
			case 29:
			
				//BARIS 			sopir
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0'>
					<tr valign='center'>
						<td valign='center' align='center' colspan=2 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
						<td class='kursi_sopir' colspan='2'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  1
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi1'>".
							$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 2
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
					"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).	
						"</div>
					</td>
				</tr>";                                                                                                                                                         

				//BARIS 3
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).                                                 
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi11'>".
							$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi12'>".
							$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
						"</div>
					</td>
				</tr>";                     
				
				//BARIS 4
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi13'>".
							$this->MakeImageByStatus(13,$status_kursi[13],$status_bayar[13],$session[13],$nama[13],$no_tiket[13],$user_level,$kode_booking[13],$sub_jadwal[13],$petugas_penjual[13]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi14'>".
							$this->MakeImageByStatus(14,$status_kursi[14],$status_bayar[14],$session[14],$nama[14],$no_tiket[14],$user_level,$kode_booking[14],$sub_jadwal[14],$petugas_penjual[14]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi15'>".
							$this->MakeImageByStatus(15,$status_kursi[15],$status_bayar[15],$session[15],$nama[15],$no_tiket[15],$user_level,$kode_booking[15],$sub_jadwal[15],$petugas_penjual[15]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi16'>".
							$this->MakeImageByStatus(16,$status_kursi[16],$status_bayar[16],$session[16],$nama[16],$no_tiket[16],$user_level,$kode_booking[16],$sub_jadwal[16],$petugas_penjual[16]).
						"</div>
					</td>
				</tr>";     
				
				//BARIS 5
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi17'>".
							$this->MakeImageByStatus(17,$status_kursi[17],$status_bayar[17],$session[17],$nama[17],$no_tiket[17],$user_level,$kode_booking[17],$sub_jadwal[17],$petugas_penjual[17]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi18'>".
							$this->MakeImageByStatus(18,$status_kursi[18],$status_bayar[18],$session[18],$nama[18],$no_tiket[18],$user_level,$kode_booking[18],$sub_jadwal[18],$petugas_penjual[18]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi19'>".
							$this->MakeImageByStatus(19,$status_kursi[19],$status_bayar[19],$session[19],$nama[19],$no_tiket[19],$user_level,$kode_booking[19],$sub_jadwal[19],$petugas_penjual[19]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi20'>".
							$this->MakeImageByStatus(20,$status_kursi[20],$status_bayar[20],$session[20],$nama[20],$no_tiket[20],$user_level,$kode_booking[20],$sub_jadwal[20],$petugas_penjual[20]).
					"</div>
					</td>
				</tr>";                                                                                                                      
				
				//BARIS 6
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi21'>".
							$this->MakeImageByStatus(21,$status_kursi[21],$status_bayar[21],$session[21],$nama[21],$no_tiket[21],$user_level,$kode_booking[21],$sub_jadwal[21],$petugas_penjual[21]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi22'>".
							$this->MakeImageByStatus(22,$status_kursi[22],$status_bayar[22],$session[22],$nama[22],$no_tiket[22],$user_level,$kode_booking[22],$sub_jadwal[22],$petugas_penjual[22]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi23'>".
							$this->MakeImageByStatus(23,$status_kursi[23],$status_bayar[23],$session[23],$nama[23],$no_tiket[23],$user_level,$kode_booking[23],$sub_jadwal[23],$petugas_penjual[23]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi24'>".
							$this->MakeImageByStatus(24,$status_kursi[24],$status_bayar[24],$session[24],$nama[24],$no_tiket[24],$user_level,$kode_booking[24],$sub_jadwal[24],$petugas_penjual[24]).
						"</div>
					</td>
				</tr>";                                                                                                                      
				
				//BARIS 7
				$layout_kendaraan.=
				"<tr valign='center'>
					<td colspan=4>
						<table width='100%' cellspacing='2' cellpadding='0'>
							<tr>
								<td class='layout_kursi'>
									<div id='rewrite_kursi25'>".
										$this->MakeImageByStatus(25,$status_kursi[25],$status_bayar[25],$session[25],$nama[25],$no_tiket[25],$user_level,$kode_booking[25],$sub_jadwal[25],$petugas_penjual[25]).
									"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi26'>".
											$this->MakeImageByStatus(26,$status_kursi[26],$status_bayar[26],$session[26],$nama[26],$no_tiket[26],$user_level,$kode_booking[26],$sub_jadwal[26],$petugas_penjual[26]).
										"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi27'>".
											$this->MakeImageByStatus(27,$status_kursi[27],$status_bayar[27],$session[27],$nama[27],$no_tiket[27],$user_level,$kode_booking[27],$sub_jadwal[27],$petugas_penjual[27]).
										"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi28'>".
											$this->MakeImageByStatus(28,$status_kursi[28],$status_bayar[28],$session[28],$nama[28],$no_tiket[28],$user_level,$kode_booking[28],$sub_jadwal[28],$petugas_penjual[28]).
										"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi29'>".
											$this->MakeImageByStatus(29,$status_kursi[29],$status_bayar[29],$session[29],$nama[29],$no_tiket[29],$user_level,$kode_booking[29],$sub_jadwal[29],$petugas_penjual[29]).
									"</div>
								</td>
							</tr>                                                                                                                        
						</table>
					</td>
				</tr>";                                                                                            
        
				$layout_kendaraan.=
				"</table>";
			
		  break;
			
			// Layout mobil dengan 35 kursi ==========================================================================================
			case 35:
			
				//BARIS 			sopir
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0'>
					<tr valign='center'>
						<td valign='center' align='center' colspan=2 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
						<td class='kursi_sopir' colspan='2'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  1
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi1'>".
							$this->MakeImageByStatus(1,$status_kursi[1],$status_bayar[1],$session[1],$nama[1],$no_tiket[1],$user_level,$kode_booking[1],$sub_jadwal[1],$petugas_penjual[1]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi2'>".
							$this->MakeImageByStatus(2,$status_kursi[2],$status_bayar[2],$session[2],$nama[2],$no_tiket[2],$user_level,$kode_booking[2],$sub_jadwal[2],$petugas_penjual[2]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi3'>".
							$this->MakeImageByStatus(3,$status_kursi[3],$status_bayar[3],$session[3],$nama[3],$no_tiket[3],$user_level,$kode_booking[3],$sub_jadwal[3],$petugas_penjual[3]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi4'>".
							$this->MakeImageByStatus(4,$status_kursi[4],$status_bayar[4],$session[4],$nama[4],$no_tiket[4],$user_level,$kode_booking[4],$sub_jadwal[4],$petugas_penjual[4]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 2
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi5'>".
							$this->MakeImageByStatus(5,$status_kursi[5],$status_bayar[5],$session[5],$nama[5],$no_tiket[5],$user_level,$kode_booking[5],$sub_jadwal[5],$petugas_penjual[5]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi6'>".
							$this->MakeImageByStatus(6,$status_kursi[6],$status_bayar[6],$session[6],$nama[6],$no_tiket[6],$user_level,$kode_booking[6],$sub_jadwal[6],$petugas_penjual[6]).
					"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi7'>".
							$this->MakeImageByStatus(7,$status_kursi[7],$status_bayar[7],$session[7],$nama[7],$no_tiket[7],$user_level,$kode_booking[7],$sub_jadwal[7],$petugas_penjual[7]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi8'>".
							$this->MakeImageByStatus(8,$status_kursi[8],$status_bayar[8],$session[8],$nama[8],$no_tiket[8],$user_level,$kode_booking[8],$sub_jadwal[8],$petugas_penjual[8]).	
						"</div>
					</td>
				</tr>";                                                                                                                                                         

				//BARIS 3
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi9'>".
							$this->MakeImageByStatus(9,$status_kursi[9],$status_bayar[9],$session[9],$nama[9],$no_tiket[9],$user_level,$kode_booking[9],$sub_jadwal[9],$petugas_penjual[9]).                                                 
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi10'>".
							$this->MakeImageByStatus(10,$status_kursi[10],$status_bayar[10],$session[10],$nama[10],$no_tiket[10],$user_level,$kode_booking[10],$sub_jadwal[10],$petugas_penjual[10]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi11'>".
							$this->MakeImageByStatus(11,$status_kursi[11],$status_bayar[11],$session[11],$nama[11],$no_tiket[11],$user_level,$kode_booking[11],$sub_jadwal[11],$petugas_penjual[11]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi12'>".
							$this->MakeImageByStatus(12,$status_kursi[12],$status_bayar[12],$session[12],$nama[12],$no_tiket[12],$user_level,$kode_booking[12],$sub_jadwal[12],$petugas_penjual[12]).
						"</div>
					</td>
				</tr>";                     
				
				//BARIS 4
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi13'>".
							$this->MakeImageByStatus(13,$status_kursi[13],$status_bayar[13],$session[13],$nama[13],$no_tiket[13],$user_level,$kode_booking[13],$sub_jadwal[13],$petugas_penjual[13]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi14'>".
							$this->MakeImageByStatus(14,$status_kursi[14],$status_bayar[14],$session[14],$nama[14],$no_tiket[14],$user_level,$kode_booking[14],$sub_jadwal[14],$petugas_penjual[14]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi15'>".
							$this->MakeImageByStatus(15,$status_kursi[15],$status_bayar[15],$session[15],$nama[15],$no_tiket[15],$user_level,$kode_booking[15],$sub_jadwal[15],$petugas_penjual[15]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi16'>".
							$this->MakeImageByStatus(16,$status_kursi[16],$status_bayar[16],$session[16],$nama[16],$no_tiket[16],$user_level,$kode_booking[16],$sub_jadwal[16],$petugas_penjual[16]).
						"</div>
					</td>
				</tr>";     
				
				//BARIS 5
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi17'>".
							$this->MakeImageByStatus(17,$status_kursi[17],$status_bayar[17],$session[17],$nama[17],$no_tiket[17],$user_level,$kode_booking[17],$sub_jadwal[17],$petugas_penjual[17]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi18'>".
							$this->MakeImageByStatus(18,$status_kursi[18],$status_bayar[18],$session[18],$nama[18],$no_tiket[18],$user_level,$kode_booking[18],$sub_jadwal[18],$petugas_penjual[18]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi19'>".
							$this->MakeImageByStatus(19,$status_kursi[19],$status_bayar[19],$session[19],$nama[19],$no_tiket[19],$user_level,$kode_booking[19],$sub_jadwal[19],$petugas_penjual[19]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi20'>".
							$this->MakeImageByStatus(20,$status_kursi[20],$status_bayar[20],$session[20],$nama[20],$no_tiket[20],$user_level,$kode_booking[20],$sub_jadwal[20],$petugas_penjual[20]).
					"</div>
					</td>
				</tr>";                                                                                                                      
				
				//BARIS 6
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi21'>".
							$this->MakeImageByStatus(21,$status_kursi[21],$status_bayar[21],$session[21],$nama[21],$no_tiket[21],$user_level,$kode_booking[21],$sub_jadwal[21],$petugas_penjual[21]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi22'>".
							$this->MakeImageByStatus(22,$status_kursi[22],$status_bayar[22],$session[22],$nama[22],$no_tiket[22],$user_level,$kode_booking[22],$sub_jadwal[22],$petugas_penjual[22]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi23'>".
							$this->MakeImageByStatus(23,$status_kursi[23],$status_bayar[23],$session[23],$nama[23],$no_tiket[23],$user_level,$kode_booking[23],$sub_jadwal[23],$petugas_penjual[23]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi24'>".
							$this->MakeImageByStatus(24,$status_kursi[24],$status_bayar[24],$session[24],$nama[24],$no_tiket[24],$user_level,$kode_booking[24],$sub_jadwal[24],$petugas_penjual[24]).
						"</div>
					</td>
				</tr>";                                                                                                                      
				
				//BARIS 7
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi25'>".
							$this->MakeImageByStatus(25,$status_kursi[25],$status_bayar[25],$session[25],$nama[25],$no_tiket[25],$user_level,$kode_booking[25],$sub_jadwal[25],$petugas_penjual[25]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi26'>".
							$this->MakeImageByStatus(26,$status_kursi[26],$status_bayar[26],$session[26],$nama[26],$no_tiket[26],$user_level,$kode_booking[26],$sub_jadwal[26],$petugas_penjual[26]).
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi27'>".
							$this->MakeImageByStatus(27,$status_kursi[27],$status_bayar[27],$session[27],$nama[27],$no_tiket[27],$user_level,$kode_booking[27],$sub_jadwal[27],$petugas_penjual[27]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi28'>".
							$this->MakeImageByStatus(28,$status_kursi[28],$status_bayar[28],$session[28],$nama[28],$no_tiket[28],$user_level,$kode_booking[28],$sub_jadwal[28],$petugas_penjual[28]).
						"</div>
					</td>
				</tr>";                                                                                                                      
				
				//BARIS 8
				$layout_kendaraan.=
				"<tr valign='center'>
					<td class='layout_kursi'>
						&nbsp;
					</td>
					<td class='layout_kursi'>
						&nbsp;
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi29'>".
							$this->MakeImageByStatus(29,$status_kursi[29],$status_bayar[29],$session[29],$nama[29],$no_tiket[29],$user_level,$kode_booking[29],$sub_jadwal[29],$petugas_penjual[29]).                                                                   
						"</div>
					</td>
					<td class='layout_kursi'>
						<div id='rewrite_kursi30'>".
							$this->MakeImageByStatus(30,$status_kursi[30],$status_bayar[30],$session[30],$nama[30],$no_tiket[30],$user_level,$kode_booking[30],$sub_jadwal[30],$petugas_penjual[30]).
						"</div>
					</td>
				</tr>";              
				
				//BARIS 9
				$layout_kendaraan.=
				"<tr valign='center'>
					<td colspan=4>
						<table width='100%' cellspacing='2' cellpadding='0'>
							<tr>
								<td class='layout_kursi'>
									<div id='rewrite_kursi31'>".
										$this->MakeImageByStatus(31,$status_kursi[31],$status_bayar[31],$session[31],$nama[31],$no_tiket[31],$user_level,$kode_booking[31],$sub_jadwal[31],$petugas_penjual[31]).
									"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi32'>".
											$this->MakeImageByStatus(32,$status_kursi[32],$status_bayar[32],$session[32],$nama[32],$no_tiket[32],$user_level,$kode_booking[32],$sub_jadwal[32],$petugas_penjual[32]).
										"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi33'>".
											$this->MakeImageByStatus(33,$status_kursi[33],$status_bayar[33],$session[33],$nama[33],$no_tiket[33],$user_level,$kode_booking[33],$sub_jadwal[33],$petugas_penjual[33]).
										"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi34'>".
											$this->MakeImageByStatus(34,$status_kursi[34],$status_bayar[34],$session[34],$nama[34],$no_tiket[34],$user_level,$kode_booking[34],$sub_jadwal[34],$petugas_penjual[34]).
										"</div>
									</td>
									<td class='layout_kursi'>
										<div id='rewrite_kursi35'>".
											$this->MakeImageByStatus(35,$status_kursi[35],$status_bayar[35],$session[35],$nama[35],$no_tiket[35],$user_level,$kode_booking[35],$sub_jadwal[35],$petugas_penjual[35]).
									"</div>
								</td>
							</tr>                                                                                                                        
						</table>
					</td>
				</tr>";                                                                                            
        
				$layout_kendaraan.=
				"</table>";
			
		  break;
		
			// Layout mobil dengan 45 kursi ==========================================================================================
			case 45:
			
				//BARIS 			sopir
				$layout_kendaraan =
				"
				<table width='100%' cellspacing='2' cellpadding='0' bgcolor='d0d0d0'>
					<tr valign='center'>
						<td valign='center' align='center' colspan=2 bgcolor='ffffff'>
							<strong>".dateParse(FormatMySQLDateToTgl($tgl_berangkat))."<br>$kode_jadwal</strong><br><br>
							<input type='button' value='Cetak Manifest' onclick='setDialogSPJ();' />
						</td>
						<td class='kursi_sopir' colspan='2'>
							<div class='kursi_nama'>$nama_sopir</div>
						</td>
				</tr>";
				
				//BARIS  1
				$no	= 1;
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 2
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";

				//BARIS 3
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 4
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 5
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";                                                                                            
				
				//BARIS 6
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";                                                                                                    
				
				//BARIS 7
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";                                                                                            
				
				//BARIS 8
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 9
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 10
				$no++;	
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
				
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 11
				
				$layout_kendaraan.="
				<tr valign='center'>
					<td class='layout_kursi' colspan='2'>
						&nbsp;
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";
				
				//BARIS 12
				$layout_kendaraan.="
				<tr valign='center'>
					<td align='center'>
						<b>TOILET</b>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>";
					
				$no++;	
				$layout_kendaraan.="
					<td class='layout_kursi'>
						<div id='rewrite_kursi".$no."'>".
							$this->MakeImageByStatus($no,$status_kursi[$no],$status_bayar[$no],$session[$no],$nama[$no],$no_tiket[$no],$user_level,$kode_booking[$no],$sub_jadwal[$no],$petugas_penjual[$no]).
						"</div>
					</td>
				</tr>";                                                                         
        
				$layout_kendaraan.=
				"</table>";
			
		  break;
		}
		
		$db->sql_close();
		
		return $layout_kendaraan;
	}
	
	function MakeImageByStatusByNoKursi($num,$status,$status_bayar,$session_id_old,$nama,$t,$user_level,$kode_booking){
	  global $userdata;
		global $LEVEL_ADMIN;
		global $LEVEL_SUPERVISOR;
		global $LEVEL_KOORDINATOR;
		global $LEVEL_AGEN;
		global $db;
		
		$layout_kursi="";
		
		$user_level=$userdata['user_level'];
		$session_id=$userdata['user_id'];
		
		$nama	=substr($nama,0,10);
		
	  if ($status==0) {
			//kursi masih kosong
			
			$layout_kursi .="<div class='kursi_kosong' onclick='updateTemp($num)'>$num</div>";
			
	  } else
		if ($status==1 && (trim($t)=="")){
	    // Jika kursi sudah di flag
			
			if($session_id==$session_id_old){
			  //jika select memang benar dilakukan oleh user ini
					
					$layout_kursi .="
						<div class='kursi_dipilih' onclick='updateTemp($num)'>
							$num
						</div>";
			}
			else{
			  //jika select dilakukan bukan oleh user ini, maka user ini tidak berhak membatalkannya
		
				if(in_array($user_level,array($LEVEL_ADMIN,$LEVEL_SUPERVISOR,$LEVEL_KOORDINATOR))){
					//jika  admin/supervisor, berhak membatalkan transaksi orang lain
						
					$layout_kursi .="
						<div class='kursi_block' onclick='updateTemp($num)'>
							$num<br>
							<span class='kursi_warning'>Kursi tidak dapat diakses!</span>
						</div>";
				}
				else{
					//jika bukan admin/ supervisor, tidak dapat membatalkan transaksi orang lain
					$layout_kursi .="
						<div class='kursi_block'>
							$num<br>
							<span class='kursi_warning'>Kursi tidak dapat diakses!</span>
						</div>";
					
				}//endif ($user_level==$LEVEL_ADMIN || $user_level==$LEVEL_SUPERVISOR)

			}//endif $array_kursi[$num]==$userdata['session_id']
		
	  } else
	  if ($status==1 && (trim($t)!="")){
	    // Jika kursi sudah di booking
			
			//grouping kursi
			/*$sql = "SELECT f_reservasi_get_kode_booking_by_no_tiket('$t')";
							
			if ($result = $db->sql_query($sql)){
				$row = $db->sql_fetchrow($result);
				$kode_booking=$row[0];
			}
			else{
				exit;
			}*/
			
			if($status_bayar!=1){
				//jika tiket belum dibayar
				
				if($user_level!=$LEVEL_AGEN){
					//jika  bukan member, dapat melihat detail transaksi
					
					$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_booking":"kursi_booking_dipilih";
			
					$layout_kursi .="
						<td onclick='showChair($num,\"$t\");' class='$class_kursi'>
							$num
							<div class='kursi_nama'>$nama</div>
						</td>";
				}
				else{
					exit;
				}
			}
			else{
				//jika kursi sudah dibayar
				
				if($user_level!=$LEVEL_AGEN){
					//jika  bukan member, dapat melihat detail transaksi
					
					//grouping kursi
					$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_bayar":"kursi_bayar_dipilih";
					
					$layout_kursi .="
						<td onclick='showChair($num,\"$t\");' class='$class_kursi'>
							$num
							<div class='kursi_nama'>$nama</div>
						</td>";
				}
				else{
					exit;
				}
			}
		
		}
		return $layout_kursi;
	}
	
	function getStatusKursi($tgl_berangkat,$kode_jadwal,$no_kursi){
		global $userdata;
		global $db;
		global $LEVEL_ADMIN;
		global $LEVEL_SUPERVISOR;
		global $LEVEL_KOORDINATOR;
		global $LEVEL_AGEN;
		global $SESSION_TIME_EXPIRED;
		
		//INISIALISASI
		
		//mengambil data user dan levelnya
		$user_level				= $userdata['user_level'];
		
		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0,(int) $temp_array_date[1] ,(int) $temp_array_date[2]+2,(int) $temp_array_date[0]);
		
		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');
		
		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi_detail":"tbl_posisi_detail_backup";
		
		$sql = 
			"SELECT *
			FROM $nama_tbl
			WHERE 
				KodeJadwal LIKE '$kode_jadwal' 
				AND TglBerangkat='$tgl_berangkat'
				AND ((HOUR(TIMEDIFF(SessionTime,NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW())))<=$SESSION_TIME_EXPIRED 
				OR NoTiket!='' 
				OR NoTiket IS NOT NULL)
				AND NomorKursi='$no_kursi'";
		
		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);                                                                                                                                                                                                                                       
		} 
		else{	  
			//die_error('Cannot Load Transaksi');//,__LINE__,__FILE__,$sql);
			echo("Err:".__LINE__);
			exit;
		}
		
		$return_kursi	= $this->MakeImageByStatus($no_kursi,$row['StatusKursi'],$row['StatusBayar'],$row['Session'],$row['Nama'],$row['NoTiket'],$user_level,$row['KodeBooking'],0,9999);
		
		$db->sql_close();
		
		return $return_kursi;
	}
}
?>