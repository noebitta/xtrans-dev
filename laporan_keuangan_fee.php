<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$bulan			= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];


$bulan	=($bulan!='')?$bulan:date("m");
$tahun	= ($tahun!='')?$tahun:date("Y");

//INISIALISASI
$PengaturanUmum	= new PengaturanUmum();
$fee_transaksi	= $PengaturanUmum->ambilFeeTransaksi();
$fee_tiket			= $fee_transaksi['FeeTiket']; 
$fee_paket			= $fee_transaksi['FeePaket']; 

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	$list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}

// LIST
$template->set_filenames(array('body' => 'laporan_keuangan_fee/laporan_keuangan_fee_body.tpl')); 


//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}
	
//FEE PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
		IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket,
		IS_NULL(SUM(IF(FlagBatal=1,1,0)),0) AS TotalTiketBatal
	FROM tbl_reservasi
	WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1
	GROUP BY DATE(WaktuCetakTiket)
	ORDER BY DATE(WaktuCetakTiket) ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
//FEE PAKET
$sql=
	"SELECT 
		WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
	FROM tbl_paket
	WHERE MONTH(WaktuPesan)=$bulan AND YEAR(WaktuPesan)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY DATE(WaktuPesan)
	ORDER BY DATE(WaktuPesan)";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//FEE SPJ
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoSPJ),0) AS TotalSPJ
	FROM tbl_spj
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_spj = $db->sql_query($sql)){
	$data_spj = $db->sql_fetchrow($result_spj);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
$sum_spj									= 0;
$sum_tiket								= 0;
$sum_tiket_batal					= 0;
$sum_fee_tiket						= 0;
$sum_paket								= 0;
$sum_fee_paket						= 0;
$sum_total_fee						= 0;

for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$odd ='odd';
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
	
	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	if($idx_str_hari!=7){
		$font_color	= "000000";
	}
	else{
		$font_color = "ffffff";
		$odd	='red';
	}
	
	
	//OMZET PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_tiket				= $data_penumpang['TotalTiket']; 
		$total_tiket_batal	= $data_penumpang['TotalTiketBatal']; 
		$total_fee_tiket		= $total_tiket*$fee_tiket;
		$data_penumpang 		= $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_tiket				= 0;
		$total_tiket_batal	= 0;
		$total_fee_tiket		= 0;
	}
	
	//OMZET PAKET
	if($data_paket['Tanggal']==$idx_tgl+1){
		$total_paket				= $data_paket['TotalPaket']; 
		$total_fee_paket		= ($fee_paket<=1)?$data_paket['TotalOmzetPaket']*$fee_paket:$total_paket*$fee_paket;
		$data_paket 				= $db->sql_fetchrow($result_paket);
	}
	else{
		$total_paket			= 0;
		$total_fee_paket	= 0;
	}
	
	//FEE BIAYA
	if($data_spj['Tanggal']==$idx_tgl+1){
		$total_spj		= $data_spj['TotalSPJ']; 
		$data_spj 		= $db->sql_fetchrow($result_spj);
	}
	else{
		$total_spj		= 0;
	}
	
	$total_fee		=	$total_fee_tiket+$total_fee_paket;
	
	$sum_spj									+= $total_spj;
	$sum_tiket								+= $total_tiket;
	$sum_tiket_batal					+= $total_tiket_batal;
	$sum_fee_tiket						+= $total_fee_tiket;
	$sum_paket								+= $total_paket;
	$sum_fee_paket						+= $total_fee_paket;
	$sum_total_fee						+= $total_fee;
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'font_color'=>$font_color,
				'tgl'=>$tgl_transaksi,
				'spj'=>number_format($total_spj,0,",","."),
				'tiket'=>number_format($total_tiket,0,",","."),
				'tiket_batal'=>number_format($total_tiket_batal,0,",","."),
				'fee_tiket'=>number_format($total_fee_tiket,0,",","."),
				'paket'=>number_format($total_paket,0,",","."),
				'fee_paket'=>number_format($total_fee_paket,0,",","."),
				'total_fee'=>number_format($total_fee,0,",",".")
			)
		);
		
	$temp_hari++;
}
			


//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$bulan."&p2=".$tahun;
	
$script_cetak_pdf="Start('laporan_keuangan_fee_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_keuangan_fee_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('laporan_keuangan_fee.'.$phpEx).'">Laporan Rekap Fee Transaksi</a>',
	'URL'						=> append_sid('laporan_keuangan_fee.'.$phpEx).$parameter,
	'LIST_BULAN'		=> "| ".$list_bulan,
	'BULAN'					=> $bulan,
	'TAHUN'					=> $tahun,
	'SUM_SPJ'				=>number_format($sum_spj,0,",","."),
	'SUM_TIKET'			=>number_format($sum_tiket,0,",","."),
	'SUM_TIKET_BATAL'	=>number_format($sum_tiket_batal,0,",","."),
	'SUM_FEE_TIKET'	=>number_format($sum_fee_tiket,0,",","."),
	'SUM_PAKET'			=>number_format($sum_paket,0,",","."),
	'SUM_FEE_PAKET'	=>number_format($sum_fee_paket,0,",","."),
	'SUM_TOTAL_FEE'	=>number_format($sum_total_fee,0,",","."),
	'U_LAPORAN_OMZET_GRAFIK'=>append_sid('laporan_keuangan_fee_grafik.'.$phpEx).'&bulan='.$bulan.'&tahun='.$tahun,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>