<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassArea.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Jurusan	= new Jurusan();
$Cabang		= new Cabang();
$Area 		= new Area();

//==METHODS======================================================
function setComboCabang($cabang_dipilih){
	//SET COMBO CABANG
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboArea($area_dipilih){
	//SET COMBO AREA
	global $db;
	global $Area;
			
	$result=$Area->ambilData("","KodeArea","ASC");
	$opt="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($area_dipilih!=$row['KodeArea'])?"":"selected";
			$opt .="<option value='$row[KodeArea]' $selected>$row[KodeArea] ($row[NamaArea])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt;
	//END SET AREA
}

//==CONTROL===========================================================================================
	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'jurusan/add_body.tpl')); 
		$template->assign_vars(array(
			'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jurusan.'.$phpEx).'">Jurusan</a> | <a href="'.append_sid('pengaturan_jurusan.'.$phpEx."?mode=add").'">Tambah Jurusan</a> ',
			'JUDUL'			=>'Tambah Data Jurusan',
			'MODE'   		=> 'save',
			'SUB'    		=> '0',
			'OPT_ASAL'  => setComboCabang(''),
			'OPT_TUJUAN'=> setComboCabang(''),
			'OPT_AREA'	=> setComboArea(''),
			'PESAN'						=> $pesan,
		  'BGCOLOR_PESAN'		=> $bgcolor_pesan,
			'U_JURUSAN_ADD_ACT'	=> append_sid('pengaturan_jurusan.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah jurusan
		$id_jurusan  											= $HTTP_POST_VARS['id_jurusan'];
		$kode  														= str_replace(" ","",$HTTP_POST_VARS['kode']);
		$kode_jurusan_old									= str_replace(" ","",$HTTP_POST_VARS['kode_jurusan_old']);
		$asal   													= $HTTP_POST_VARS['asal'];
		$tujuan   												= $HTTP_POST_VARS['tujuan'];
		$harga_tiket											= $HTTP_POST_VARS['harga_tiket'];
		$harga_tiket_tuslah 							= $HTTP_POST_VARS['harga_tiket_tuslah'];
		$harga_paket_p_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_p_kilo_pertama'];
		$harga_paket_p_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_p_kilo_berikut'];
		$harga_paket_ga_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_ga_kilo_pertama'];
		$harga_paket_ga_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_ga_kilo_berikut'];
		$harga_paket_gd_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_gd_kilo_pertama'];
		$harga_paket_gd_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_gd_kilo_berikut'];
		$harga_paket_s_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_s_kilo_pertama'];
		$harga_paket_s_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_s_kilo_berikut'];
		$harga_paket_ca_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_ca_kilo_pertama'];
		$harga_paket_ca_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_ca_kilo_berikut'];
		$harga_paket_cd_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_cd_kilo_pertama'];
		$harga_paket_cd_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_cd_kilo_berikut'];
		$harga_paket_motor1 							= $HTTP_POST_VARS['harga_paket_motor1'];
		$harga_paket_motor2 							= $HTTP_POST_VARS['harga_paket_motor2'];
		$flag_tuslah											= $HTTP_POST_VARS['flag_tuslah'];
		$flag_aktif												= $HTTP_POST_VARS['flag_aktif'];
		$flag_jenis												= $HTTP_POST_VARS['flag_jenis'];
		$flag_op_jurusan									= $HTTP_POST_VARS['flag_op_jurusan'];
		$biaya_sopir											= $HTTP_POST_VARS['biaya_sopir'];
		$insentif_sopir										= $HTTP_POST_VARS['komisi_penumpang_sopir'];
		$biaya_tol												= $HTTP_POST_VARS['biaya_tol'];
		$biaya_parkir											= $HTTP_POST_VARS['biaya_parkir'];
		$biaya_bbm												= $HTTP_POST_VARS['biaya_bbm'];
		$kode_area												= $HTTP_POST_VARS['kode_area'];
		
		$terjadi_error=false;
		
		if($Jurusan->periksaDuplikasi($kode) && $kode!=$kode_jurusan_old){
			$pesan="<font color='white' size=3>Kode jurusan yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Jurusan";
				$path	='<a href="'.append_sid('pengaturan_jurusan.'.$phpEx."?mode=add").'">Tambah Jurusan</a> ';
				
				if($Jurusan->tambah(
					$kode,$asal,$tujuan,$harga_tiket,
					$harga_tiket_tuslah,$flag_tuslah,$biaya_sopir,$biaya_tol,$biaya_parkir,
					$biaya_bbm,$flag_jenis,$flag_aktif,
					$harga_paket_p_kilo_pertama,$harga_paket_p_kilo_berikut,
					$harga_paket_ga_kilo_pertama,$harga_paket_ga_kilo_berikut,
					$harga_paket_gd_kilo_pertama,$harga_paket_gd_kilo_berikut,
					$harga_paket_s_kilo_pertama,$harga_paket_s_kilo_berikut,
					$harga_paket_ca_kilo_pertama,$harga_paket_ca_kilo_berikut,
					$harga_paket_cd_kilo_pertama,$harga_paket_cd_kilo_berikut,
					$harga_paket_motor1,$harga_paket_motor2,
					$kode_area,$insentif_sopir,$flag_op_jurusan)){
						
					redirect(append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
					
				}
			}
			else{
				
				$judul="Ubah Data Jurusan";
				$path	='<a href="'.append_sid('pengaturan_jurusan.'.$phpEx."?mode=edit&id=$id_jurusan").'">Ubah Jurusan</a> ';
				
				if($Jurusan->ubah(
					$id_jurusan,
					$kode,$asal,$tujuan,
					$harga_tiket,$harga_tiket_tuslah,$flag_tuslah,
					$biaya_sopir,$biaya_tol,$biaya_parkir,
					$biaya_bbm,$flag_aktif,$flag_jenis,
					$harga_paket_p_kilo_pertama,$harga_paket_p_kilo_berikut,
					$harga_paket_ga_kilo_pertama,$harga_paket_ga_kilo_berikut,
					$harga_paket_gd_kilo_pertama,$harga_paket_gd_kilo_berikut,
					$harga_paket_s_kilo_pertama,$harga_paket_s_kilo_berikut,
					$harga_paket_ca_kilo_pertama,$harga_paket_ca_kilo_berikut,
					$harga_paket_cd_kilo_pertama,$harga_paket_cd_kilo_berikut,
					$harga_paket_motor1,$harga_paket_motor2,
					$kode_area,$insentif_sopir,$flag_op_jurusan)){
						
					//redirect(append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add',true));
					//die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jurusan.'.$phpEx.'?mode=edit&id='.$id_jurusan).'">Sini</a> Untuk Melanjutkan','');
					
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}
			}
			
			//exit;
			
		}
		
		$temp_var_tuslah="tuslah_".$flag_tuslah;
		$$temp_var_tuslah="selected";
		
		$temp_var_aktif="aktif_".$flag_aktif;
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="jenis_".$flag_jenis;
		$$temp_var_aktif="selected";
		
		$temp_var_sub_jurusan	="sub_jurusan_".$flag_sub_jurusan;
		$$temp_var_sub_jurusan="selected";

		$temp_var_op_jurusan	="op_".$flag_op_jurusan;
		$$temp_var_op_jurusan	="selected";
		
			$template->set_filenames(array('body' => 'jurusan/add_body.tpl')); 
			$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jurusan.'.$phpEx).'">Jurusan</a> | '.$path,
			 'JUDUL'		=>$judul,
			 'MODE'   	=> 'save',
			 'SUB'    	=> $submode,
			 'ID_JURUSAN'				=> $id_jurusan,
			 'KODE_JURUSAN_OLD'	=> $kode,
			 'KODE_JURUSAN'    	=> $kode,
			 'OPT_ASAL'    			=> setComboCabang($asal),
			 'OPT_TUJUAN'   		=> setComboCabang($tujuan),
			 'OPT_AREA'   			=> setComboArea($kode_area),
			 'HARGA_TIKET'			=> $harga_tiket,
			 'HARGA_TIKET_TUSLAH'=> $harga_tiket_tuslah,
			 'HARGA_PAKET_P_KILO_PERTAMA'	=> $harga_paket_p_kilo_pertama,
			 'HARGA_PAKET_P_KILO_BERIKUT'	=> $harga_paket_p_kilo_berikut,
			 'HARGA_PAKET_GA_KILO_PERTAMA'=> $harga_paket_ga_kilo_pertama,
			 'HARGA_PAKET_GA_KILO_BERIKUT'=> $harga_paket_ga_kilo_berikut,
			 'HARGA_PAKET_GD_KILO_PERTAMA'=> $harga_paket_gd_kilo_pertama,
			 'HARGA_PAKET_GD_KILO_BERIKUT'=> $harga_paket_gd_kilo_berikut,
			 'HARGA_PAKET_S_KILO_PERTAMA'	=> $harga_paket_s_kilo_pertama,
			 'HARGA_PAKET_S_KILO_BERIKUT'	=> $harga_paket_s_kilo_berikut,
			 'HARGA_PAKET_CA_KILO_PERTAMA'=> $harga_paket_ca_kilo_pertama,
			 'HARGA_PAKET_CA_KILO_BERIKUT'=> $harga_paket_ca_kilo_berikut,
			 'HARGA_PAKET_CD_KILO_PERTAMA'=> $harga_paket_cd_kilo_pertama,
			 'HARGA_PAKET_CD_KILO_BERIKUT'=> $harga_paket_cd_kilo_berikut,
			 'HARGA_PAKET_MOTOR1'					=> $harga_paket_motor1,
			 'HARGA_PAKET_MOTOR2'					=> $harga_paket_motor2,
			 'TUSLAH_0'					=> $tuslah_0,
			 'TUSLAH_1'					=> $tuslah_1,
			 'AKTIF_0'					=> $aktif_0,
			 'AKTIF_1'					=> $aktif_1,
			 'JENIS_0'					=> $jenis_0,
			 'JENIS_1'					=> $jenis_1,
			'OP_0'							=> $op_0,
			'OP_1'							=> $op_1,
			 'BIAYA_SOPIR'			=> $biaya_sopir,
			 'KOMISI_PENUMPANG_SOPIR'=> $insentif_sopir,
			 'BIAYA_TOL'				=> $biaya_tol,
			 'BIAYA_PARKIR'			=> $biaya_parkir,
			 'BIAYA_BBM'				=> $biaya_bbm,
			 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
			 'PESAN'						=> $pesan,
			 'U_JURUSAN_ADD_ACT'=>append_sid('pengaturan_jurusan.'.$phpEx)
			 )
			);
		
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Jurusan->ambilDataDetail($id);
		
		$temp_var_tuslah="tuslah_".$row['FlagTiketTuslah'];
		$$temp_var_tuslah="selected";
		
		$temp_var_aktif="aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="jenis_".$row['FlagLuarKota'];
		$$temp_var_aktif="selected";

		$temp_var_op_jurusan	="op_".$row['FlagOperasionalJurusan'];
		$$temp_var_op_jurusan	="selected";
		
		$temp_var_sub_jurusan	="sub_jurusan_".$row['FlagSubJurusan'];
		$$temp_var_sub_jurusan="selected";
		
		$template->set_filenames(array('body' => 'jurusan/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jurusan.'.$phpEx).'">Jurusan</a> | <a href="'.append_sid('pengaturan_jurusan.'.$phpEx."?mode=edit&id=$id").'">Ubah Jurusan</a> ',
			 'JUDUL'		=>'Ubah Data Jurusan',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'ID_JURUSAN'				=> $id,
			 'KODE_JURUSAN_OLD'	=> $row['KodeJurusan'],
			 'KODE_JURUSAN'			=> $row['KodeJurusan'],
			 'OPT_ASAL'    			=> setComboCabang($row['KodeCabangAsal']),
			 'OPT_TUJUAN'   		=> setComboCabang($row['KodeCabangTujuan']),
			 'OPT_AREA'   			=> setComboArea($row['KodeArea']),
			 'HARGA_TIKET'			=> $row['HargaTiket'],
			 'HARGA_TIKET_TUSLAH'=> $row['HargaTiketTuslah'],
			 'HARGA_PAKET_P_KILO_PERTAMA'	=> $row['HargaPaketPKiloPertama'],
			 'HARGA_PAKET_P_KILO_BERIKUT'	=> $row['HargaPaketPKiloBerikut'],
			 'HARGA_PAKET_GA_KILO_PERTAMA'=> $row['HargaPaketGAKiloPertama'],
			 'HARGA_PAKET_GA_KILO_BERIKUT'=> $row['HargaPaketGAKiloBerikut'],
			 'HARGA_PAKET_GD_KILO_PERTAMA'=> $row['HargaPaketGDKiloPertama'],
			 'HARGA_PAKET_GD_KILO_BERIKUT'=> $row['HargaPaketGDKiloBerikut'],
			 'HARGA_PAKET_S_KILO_PERTAMA'	=> $row['HargaPaketSKiloPertama'],
			 'HARGA_PAKET_S_KILO_BERIKUT'	=> $row['HargaPaketSKiloBerikut'],
			 'HARGA_PAKET_CA_KILO_PERTAMA'=> $row['HargaPaketCAKiloPertama'],
			 'HARGA_PAKET_CA_KILO_BERIKUT'=> $row['HargaPaketCAKiloBerikut'],
			 'HARGA_PAKET_CD_KILO_PERTAMA'=> $row['HargaPaketCDKiloPertama'],
			 'HARGA_PAKET_CD_KILO_BERIKUT'=> $row['HargaPaketCDKiloBerikut'],
			 'HARGA_PAKET_MOTOR1'					=> $row['HargaPaketM1'],
			 'HARGA_PAKET_MOTOR2'					=> $row['HargaPaketM2'],
			 'TUSLAH_0'					=> $tuslah_0,
			 'TUSLAH_1'					=> $tuslah_1,
			 'AKTIF_0'					=> $aktif_0,
			 'AKTIF_1'					=> $aktif_1,
			 'JENIS_0'					=> $jenis_0,
			 'JENIS_1'					=> $jenis_1,
				'OP_0'							=> $op_0,
				'OP_1'							=> $op_1,
			 'BIAYA_SOPIR'			=> $row['BiayaSopir'],
			 'KOMISI_PENUMPANG_SOPIR'=> $row['KomisiPenumpangSopir'],
			 'BIAYA_TOL'				=> $row['BiayaTol'],
			 'BIAYA_PARKIR'			=> $row['BiayaParkir'],
			 'BIAYA_BBM'				=> $row['BiayaBBM'],
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_JURUSAN_ADD_ACT'=>append_sid('pengaturan_jurusan.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus jurusan
		$list_jurusan = str_replace("\'","'",$HTTP_GET_VARS['list_jurusan']);
		
		$Jurusan->hapus($list_jurusan);
		
		exit;
	} 
	else if ($mode=='ubahstatusaktif'){
		// aksi hapus jadwal
		$id_jurusan = $HTTP_GET_VARS['id'];
	
		$Jurusan->ubahStatusAktif($id_jurusan);
		
		exit;
	}
	else if ($mode=='ubahstatustuslah'){
		// aksi hapus jadwal
		/*$id_jurusan = $HTTP_GET_VARS['id'];
		$tuslah 		= $HTTP_GET_VARS['tuslah'];
	
		$Jurusan->ubahStatusTuslah($id_jurusan,$tuslah);
		*/
		exit;
	}
	else {
		// LIST
		$template->set_filenames(array('body' => 'jurusan/jurusan_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$temp_cari=str_replace("asal=","",$cari);
		if($temp_cari==$cari){
			$kondisi_asal = "";
		}
		else{
			$kondisi_asal = "AND f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$temp_cari%' ";
			$cari=$temp_cari;
		}
		
		$temp_cari=str_replace("tujuan=","",$cari);
		if($temp_cari==$cari){
			$kondisi_tujuan = "";
		}
		else{
			$kondisi_tujuan = "AND f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$temp_cari%' ";
			$cari=$temp_cari;
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE (KodeJurusan LIKE '%$cari%' 
				OR KodeArea LIKE '%$cari%' 
				OR KodeCabangAsal LIKE '%$cari%' 
				OR KodeCabangTujuan LIKE '%$cari%'
				OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%' 
				OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%') 
				$kondisi_asal
				$kondisi_tujuan ";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdJurusan","tbl_md_jurusan","&cari=$cari",$kondisi,"pengaturan_jurusan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *,f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaCabangAsal,f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan
			FROM tbl_md_jurusan $kondisi 
			ORDER BY KodeJurusan,NamaCabangAsal,NamaCabangTujuan LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  
			while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"$row[0]\"/>";
				
				if(!$row['FlagTiketTuslah']){
					$tuslah = "";
					$css_harga_normal	= "green";
					$css_harga_tuslah	= "";
				}
				else{
					$tuslah	= "<b>TUSLAH<b>";
					$css_harga_normal	= "";
					$css_harga_tuslah	= "green";
				}
				
				$harga_tiket_normal	= "<a href='#' onClick='ubahStatusTuslah($row[IdJurusan],0)'>".number_format($row['HargaTiket'],0,",",".")."</a>";
				$harga_tiket_tuslah	= "<a href='#' onClick='ubahStatusTuslah($row[IdJurusan],1)'>".number_format($row['HargaTiketTuslah'],0,",",".")."</a>";
				
				if($row['FlagAktif']){
					$aktif = "<a href='#' onClick='ubahStatusAktif($row[IdJurusan])'>Aktif</a>";
				}
				else{
					$aktif ="<a href='#' onClick='ubahStatusAktif($row[IdJurusan])'>Tidak Aktif</a>";
					$odd	='red';
				}
				
				$act 	="<a href='".append_sid('pengaturan_jurusan.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'kode'=>$row['KodeJurusan'],
							'asal'=>$row['NamaCabangAsal'],
							'tujuan'=>$row['NamaCabangTujuan'],
							'jenis'=>($row['FlagLuarKota']==1)?"LUAR KOTA":"DALAM KOTA",
							'harga_tiket'=>$harga_tiket_normal,
							'normal'=>$css_harga_normal,
							'harga_tiket_tuslah'=>$harga_tiket_tuslah,
							'harga_paket_p'=>number_format($row['HargaPaketPKiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaketPKiloBerikut'],0,",","."),
							'harga_paket_ga'=>number_format($row['HargaPaketGAKiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaketGAKiloBerikut'],0,",","."),
							'harga_paket_gd'=>number_format($row['HargaPaketGDKiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaketGDKiloBerikut'],0,",","."),
							'harga_paket_s'=>number_format($row['HargaPaketSKiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaketSKiloBerikut'],0,",","."),
							'harga_paket_ca'=>number_format($row['HargaPaketCAKiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaketCAKiloBerikut'],0,",","."),
							'harga_paket_cd'=>number_format($row['HargaPaketCDKiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaketCDKiloBerikut'],0,",","."),
							'harga_paket_motor1'=>number_format($row['HargaPaketM1'],0,",",".")."<br>",
							'harga_paket_motor2'=>number_format($row['HargaPaketM2'],0,",",".")."<br>",
							'tuslah'=>$css_harga_tuslah,
							'status_tuslah'=>$tuslah,
							'area'=>$row['KodeArea'],
							'status_aktif'=>$aktif,
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=20 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
			
		} 
		else{
			//die_error('Cannot Load jurusan',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#master_data">Home</a> | <a href="'.append_sid('pengaturan_jurusan.'.$phpEx).'">Jurusan</a>',
			'U_JURUSAN_ADD'		=> append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_jurusan.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>