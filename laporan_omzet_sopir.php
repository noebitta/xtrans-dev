<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$username				= $userdata['username'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_sopir/laporan_omzet_sopir_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?
	" WHERE tms.KodeSopir LIKE '%' ":
	" WHERE (tms.KodeSopir LIKE '$cari%' OR tms.Nama LIKE '%$cari%')";
	

	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tms.KodeSopir":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tms.KodeSopir","tbl_md_sopir tms",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_omzet_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

/*$sql_total_jalan			= "(SELECT COUNT(DISTINCT(NoSPJ)) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_hari_kerja = "(SELECT COUNT(DISTINCT(TglBerangkat)) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_penumpang	= "(SELECT IS_NULL(COUNT(NoTiket),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_omzet			= "(SELECT IS_NULL(SUM(SubTotal),0) FROM tbl_reservasi_olap tr $kondisi)";
$sql_total_kursi			= "(SELECT SUM(IS_NULL(JumlahKursiDisediakan,0)) FROM tbl_spj 
													WHERE KodeDriver=tms.KodeSopir)";*/

$sql	= 
	"SELECT 
		KodeSopir,
		COUNT(DISTINCT(NoSPJ)) AS Jalan,
		COUNT(DISTINCT(TglBerangkat)) AS TotalHariKerja,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet
	FROM tbl_reservasi_olap
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND CetakSPJ=1
	GROUP BY KodeSopir";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__ );exit;
}

$data_detail	= array();

while ($row = $db->sql_fetchrow($result)){
	$kode_sopir	= $row['KodeSopir'];
	$data_detail[$kode_sopir]['Jalan']					= $row['Jalan'];
	$data_detail[$kode_sopir]['TotalHariKerja']	= $row['TotalHariKerja'];
	$data_detail[$kode_sopir]['TotalPenumpang']	= $row['TotalPenumpang'];
	$data_detail[$kode_sopir]['TotalOmzet']			= $row['TotalOmzet'];
}

/*$sql	=
	"SELECT tms.KodeSopir,tms.Nama, 
		$sql_total_jalan AS Jalan, 
		$sql_total_hari_kerja AS TotalHariKerja,
		$sql_total_penumpang AS TotalPenumpang, 
		$sql_total_omzet AS TotalOmzet, 
		$sql_total_kursi AS JumlahKursi, 
		f_sopir_get_insentif(KodeSopir,'$tanggal_mulai_mysql','$tanggal_akhir_mysql') AS TotalInsentif,
		100*$sql_total_penumpang/$sql_total_kursi AS Produktifitas
	FROM tbl_md_sopir tms 
	$kondisi_cari
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";*/

$sql	=
	"SELECT tms.KodeSopir,tms.Nama, 
		f_sopir_get_insentif(KodeSopir,'$tanggal_mulai_mysql','$tanggal_akhir_mysql') AS TotalInsentif
	FROM tbl_md_sopir tms 
	$kondisi_cari
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$act 	="<a href='".append_sid('laporan_omzet_sopir_grafik.'.$phpEx).'&kode_driver='.$row['KodeSopir'].'&bulan='.$bulan.'&tahun='.$tahun.
						'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$kode_cabang.'&sort_by='.$sort_by.'&order='.$order."'>Grafik<a/>";
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'nama'=>$row['Nama'],
					'nrp'=>$row['KodeSopir'],
					'total_hari_kerja'=>number_format($data_detail[$row['KodeSopir']]['TotalHariKerja'],0,",","."),
					'total_jalan'=>number_format($data_detail[$row['KodeSopir']]['Jalan'],0,",","."),
					'total_penumpang'=>number_format($data_detail[$row['KodeSopir']]['TotalPenumpang'],0,",","."),
					'total_omzet'=>number_format($data_detail[$row['KodeSopir']]['TotalOmzet'],0,",","."),
					'total_insentif'=>number_format($row['TotalInsentif'],0,",","."),
					'act'=>$act
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_sopir',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$temp_var		= "select_".$sort_by;
$$temp_var	= "selected";

$opt_sort_by=
	"<option value='Nama' $select_Nama>Nama Sopir</option>
	<option value='KodeSopir'$select_Kode>NRP</option>
	<option value='TotalHariKerja' $select_TotalHariKerja>Total Hari Kerja</option>
	<option value='Jalan' $select_Jalan>Total Jalan</option>
	<option value='TotalPenumpang' $select_TotalPenumpang>Total Penumpang</option>
	<option value='TotalOmzet' $select_TotalOmzet>Total Omzet</option>
	<option value='produktifitas' $select_produktifitas>Produktifitas</option>";
	
$temp_var		= "select_".$order;
$$temp_var	= "selected";

$opt_order=
	"<option value='asc' $select_asc>Menaik</option>
	<option value='desc' $select_desc>Menurun</option>";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$kode_cabang.
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
	
$script_cetak_pdf="Start('laporan_omzet_sopir_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_omzet_sopir_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT
	
$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_omzet">Home</a> | <a href="'.append_sid('laporan_omzet_sopir.'.$phpEx).'">Laporan Omzet Sopir</a>',
	'ACTION_CARI'		=> append_sid('laporan_omzet_sopir.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'OPT_SORT'			=> $opt_sort_by,
	'OPT_ORDER'			=> $opt_order,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel

	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>