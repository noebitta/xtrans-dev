<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN,$LEVEL_SUPERVISOR))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_area  		= isset($HTTP_GET_VARS['kode_area'])? $HTTP_GET_VARS['kode_area'] : $HTTP_POST_VARS['kode_area'];

//METHOD


//OPERATION

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_jurusan/index.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeJurusan LIKE '$cari%' 
		OR tmc.Nama LIKE '%$cari%' 
		OR tmc2.Nama LIKE '%$cari%')";

/*if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabangAsal='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}		*/	

$kondisi_area	= $kode_area==""?"":" AND KodeArea='$kode_area'";

$kondisi_cari	.= $kondisi_cabang.$kondisi_area;
	
$sql=
	"SELECT 
		IdJurusan,KodeJurusan,tmc.Nama AS CabangAsal,
		tmc2.Nama AS CabangTujuan,
		KodeCabangAsal,KodeArea,(SELECT Kota FROM tbl_md_cabang WHERE KodeCabang=KodeCabangAsal) AS Kota
	FROM (tbl_md_jurusan tmj LEFT JOIN tbl_md_cabang tmc ON tmj.KodeCabangAsal=tmc.KodeCabang)
		LEFT JOIN tbl_md_cabang tmc2 ON tmj.KodeCabangTujuan=tmc2.KodeCabang
	$kondisi_cari
	ORDER BY KodeArea,Kota,CabangAsal,CabangTujuan";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

/*$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),HargaTiket)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM $tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";*/

$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3 AND Total != 0,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(((JenisPenumpang='G' AND JenisPembayaran!=3) OR (JenisPembayaran!=3 AND Total = 0)),NoTiket,NULL)),0) AS TotalPenumpangG,
		
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,

		IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB, 
		
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,0),Total)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM $tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){

	die(mysql_error());

	 echo("Err:".__LINE__);

}

while ($row = $db->sql_fetchrow($result))
{
	$data_tiket_total[$row['IdJurusan']]= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['IdJurusan']					= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']				= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']					= $row['CabangAsal']."->".$row['CabangTujuan'];
	$temp_array[$idx]['KodeCabangAsal']				= $row['KodeCabangAsal'];
	$temp_array[$idx]['KodeArea']					= $row['KodeArea'];
	$temp_array[$idx]['Kota']						= $row['Kota'];
	$temp_array[$idx]['TotalPenumpangU']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangG']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangT']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangT'];

	$temp_array[$idx]['TotalPenumpangRB']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangRB'];
	
	$temp_array[$idx]['TotalPenumpangR']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangVR']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangVR'];
	$temp_array[$idx]['TotalPenumpangV']			= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangV'];
	$temp_array[$idx]['TotalBerangkat']				= $data_tiket_total[$row['IdJurusan']]['TotalBerangkat'];
	$temp_array[$idx]['TotalKomisiOnline']			= $data_tiket_total[$row['IdJurusan']]['TotalKomisiOnline'];
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['IdJurusan']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']		= $data_tiket_total[$row['IdJurusan']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']				= $data_tiket_total[$row['IdJurusan']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenumpangPerTrip']		= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']						= $temp_array[$idx]['TotalPenjualanTiket']  - $temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
	
	$idx++;
}

$idx=0;
$area_terakhir	= "";
$kota_terakhir	= "";
$gt_trip				= 0;
$gt_pnp_u				= 0;
$gt_pnp_m				= 0;
$gt_pnp_k				= 0;
$gt_pnp_kk				= 0;
$gt_pnp_g				= 0;
$gt_pnp_o				= 0;

$gt_pnp_rb				= 0;

$gt_pnp_r				= 0;
$gt_pnp_vr				= 0;
$gt_pnp_v				= 0;
$gt_pnp_t				= 0;
$gt_omz_pnp				= 0;
$gt_disc				= 0;
$gt_omzet_net			= 0;

$jumlah_data	= count($temp_array);

//PLOT DATA
while($idx<=$jumlah_data){
	
	//MENGKLASIFIKASI BERDASARKAN AREA
	if($area_terakhir!=$temp_array[$idx]['KodeArea'] || $idx==$jumlah_data){
		$nama_area								= "<tr><th colspan='20' style='font-size:14px;'><b>".$temp_array[$idx]['KodeArea']."</b></th></tr>";
		$area_terakhir						= $temp_array[$idx]['KodeArea'];
		$idx_area									= 1;

		if($idx>0){
		
			$total_per_area		= 
				"<tr style='background:green;color:white;'>
					<td colspan='3' align='center'><b>Sub Total ".$temp_array[$idx-1]['KodeArea']."</b></td>
					<td align='right'>".number_format($total_trip_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_u_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_m_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_k_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_kk_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_g_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_o_perarea,0,",",".")."</td>

					<td align='right'>".number_format($total_pnp_rb_perarea,0,",",".")."</td>
					
					<td align='right'>".number_format($total_pnp_r_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_vr_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_v_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_pnp_t_perarea,0,",",".")."</td>
					<td align='right' bgcolor='white'>&nbsp;</td>
					<td align='right'>".number_format($total_omz_pnp_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_disc_perarea,0,",",".")."</td>
					<td align='right'>".number_format($total_omzet_net_perarea,0,",",".")."</td>
					<td align='right' bgcolor='white'>&nbsp;</td>
				</tr>";
			
		}

		$total_trip_perarea				= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perarea			= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perarea			= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perarea			= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perarea			= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perarea			= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perarea			= $temp_array[$idx]['TotalPenumpangT'];
		
		$total_pnp_rb_perarea			= $temp_array[$idx]['TotalPenumpangRB'];
		
		$total_pnp_r_perarea			= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perarea			= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_v_perarea			= $temp_array[$idx]['TotalPenumpangV'];
		$total_pnp_t_perarea			= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perarea		= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perarea				= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perarea	= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
	}
	else{
		$nama_area								= "";
		$idx_area++;
		$total_trip_perarea				+= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perarea			+= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perarea			+= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perarea			+= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perarea			+= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perarea			+= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perarea			+= $temp_array[$idx]['TotalPenumpangT'];
		
		$total_pnp_rb_perarea			+= $temp_array[$idx]['TotalPenumpangRB'];
		
		$total_pnp_r_perarea			+= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perarea			+= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_v_perarea			+= $temp_array[$idx]['TotalPenumpangV'];
		$total_pnp_t_perarea			+= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perarea		+= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perarea				+= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perarea	+= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
		$total_per_area						= "";
	}
	
	//MENGKLASIFIKASI BERDASARKAN KOTA UNTUK BTK
	if($temp_array[$idx]['KodeArea']=="BTK" || $idx==$jumlah_data){
		if($kota_terakhir!=$temp_array[$idx]['Kota'] || $idx==$jumlah_data){
			$nama_kota								= "<tr><td colspan='20' style='font-size:14px;'><b>".$temp_array[$idx]['Kota']."</b></td></tr>";
			$kota_terakhir						= $temp_array[$idx]['Kota'];
			$idx_area									= 1;
	
			if($idx>0 && $temp_array[$idx-1]['KodeArea']!="BDP"){
			
				$total_per_kota		= 
					"<tr style='background:yellow;'>
						<td colspan='3' align='center'><b>Sub Total ".$temp_array[$idx-1]['Kota']."</b></td>
						<td align='right'>".number_format($total_trip_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_u_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_m_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_k_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_kk_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_g_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_o_perkota,0,",",".")."</td>
						
						<td align='right'>".number_format($total_pnp_rb_perkota,0,",",".")."</td>
						
						<td align='right'>".number_format($total_pnp_r_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_vr_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_v_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_pnp_t_perkota,0,",",".")."</td>
						<td align='right'>&nbsp;</td>
						<td align='right'>".number_format($total_omz_pnp_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_disc_perkota,0,",",".")."</td>
						<td align='right'>".number_format($total_omzet_net_perkota,0,",",".")."</td>
						<td align='right'>&nbsp;</td>
					</tr>";
				
			}
	
			$total_trip_perkota				= $temp_array[$idx]['TotalBerangkat'];
			$total_pnp_u_perkota			= $temp_array[$idx]['TotalPenumpangU'];
			$total_pnp_m_perkota			= $temp_array[$idx]['TotalPenumpangM'];
			$total_pnp_k_perkota			= $temp_array[$idx]['TotalPenumpangK'];
			$total_pnp_kk_perkota			= $temp_array[$idx]['TotalPenumpangKK'];
			$total_pnp_g_perkota			= $temp_array[$idx]['TotalPenumpangG'];
			$total_pnp_o_perkota			= $temp_array[$idx]['TotalPenumpangT'];

			$total_pnp_rb_perkota			= $temp_array[$idx]['TotalPenumpangRB'];
			
			$total_pnp_r_perkota			= $temp_array[$idx]['TotalPenumpangR'];
			$total_pnp_rb_perkota			= $temp_array[$idx]['TotalPenumpangRB'];
			$total_pnp_vr_perkota			= $temp_array[$idx]['TotalPenumpangVR'];
			$total_pnp_v_perkota			= $temp_array[$idx]['TotalPenumpangV'];
			$total_pnp_t_perkota			= $temp_array[$idx]['TotalTiket'];
			$total_omz_pnp_perkota		= $temp_array[$idx]['TotalPenjualanTiket'];
			$total_disc_perkota				= $temp_array[$idx]['TotalDiscount'];
			$total_omzet_net_perkota	= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
		}
		else{
			$nama_kota								= "";
			$total_trip_perkota				+= $temp_array[$idx]['TotalBerangkat'];
			$total_pnp_u_perkota			+= $temp_array[$idx]['TotalPenumpangU'];
			$total_pnp_m_perkota			+= $temp_array[$idx]['TotalPenumpangM'];
			$total_pnp_k_perkota			+= $temp_array[$idx]['TotalPenumpangK'];
			$total_pnp_kk_perkota			+= $temp_array[$idx]['TotalPenumpangKK'];
			$total_pnp_g_perkota			+= $temp_array[$idx]['TotalPenumpangG'];
			$total_pnp_o_perkota			+= $temp_array[$idx]['TotalPenumpangT'];
			
			$total_pnp_rb_perkota			+= $temp_array[$idx]['TotalPenumpangRB'];
			
			$total_pnp_r_perkota			+= $temp_array[$idx]['TotalPenumpangR'];
			$total_pnp_vr_perkota			+= $temp_array[$idx]['TotalPenumpangVR'];
			$total_pnp_v_perkota			+= $temp_array[$idx]['TotalPenumpangV'];
			$total_pnp_t_perkota			+= $temp_array[$idx]['TotalTiket'];
			$total_omz_pnp_perkota		+= $temp_array[$idx]['TotalPenjualanTiket'];
			$total_disc_perkota				+= $temp_array[$idx]['TotalDiscount'];
			$total_omzet_net_perkota	+= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
			$total_per_kota						= "";
		}
	}
	
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"laporan_omzet_jurusan_detail.php?sid=".$userdata['session_id']."&is_today=".$is_today."&kodearea=".$kode_area."&tglmulai=".$tanggal_mulai."&tglakhir=".$tanggal_akhir."&idjurusan=".$temp_array[$idx]['IdJurusan']."\");return false'>Detail</a>&nbsp;+&nbsp;";		
	
	$act 	.="<a href='".append_sid('laporan_omzet_jurusan_grafik.'.$phpEx).'&kode_cabang='.$temp_array[$idx]['KodeCabangAsal'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang']."'>Grafik<a/>";		
	
	if($idx<$jumlah_data){
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$idx_area,
					'nama_area'						=>$nama_area,
					'nama_kota'						=>$nama_kota,
					'kode_jurusan'					=>$temp_array[$idx]['KodeJurusan'],
					'jurusan'						=>$temp_array[$idx]['Jurusan'],
					'total_keberangkatan'	=>number_format($temp_array[$idx]['TotalBerangkat'],0,",","."),
					'total_penumpang_u'		=>number_format($temp_array[$idx]['TotalPenumpangU'],0,",","."),
					'total_penumpang_m'		=>number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."),
					'total_penumpang_k'		=>number_format($temp_array[$idx]['TotalPenumpangK'],0,",","."),
					'total_penumpang_kk'	=>number_format($temp_array[$idx]['TotalPenumpangKK'],0,",","."),
					'total_penumpang_g'		=>number_format($temp_array[$idx]['TotalPenumpangG'],0,",","."),
					'total_penumpang_o'		=>number_format($temp_array[$idx]['TotalPenumpangT'],0,",","."),
					
					'total_penumpang_rb'	=>number_format($temp_array[$idx]['TotalPenumpangRB'],0,",","."),
					
					'total_penumpang_r'		=>number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."),
					'total_penumpang_vr'	=>number_format($temp_array[$idx]['TotalPenumpangVR'],0,",","."),
					'total_penumpang_v'		=>number_format($temp_array[$idx]['TotalPenumpangV'],0,",","."),
					'total_penumpang'		=>number_format($temp_array[$idx]['TotalTiket'],0,",","."),
					'rata_pnp_per_trip'		=>number_format($temp_array[$idx]['TotalPenumpangPerTrip'],0,",","."),
					'total_omzet'			=>number_format($temp_array[$idx]['TotalPenjualanTiket'],0,",","."),
					'total_discount'		=>number_format($temp_array[$idx]['TotalDiscount'],0,",","."),
					'total'					=>number_format($temp_array[$idx]['Total'],0,",","."),
					'act'					=>$act,
					'total_per_kota'		=>$total_per_kota,
					'total_per_area'		=>$total_per_area
				)
			);
	}
	else{
		$template->
			assign_block_vars(
				'ROW',
				array(
					'total_per_kota'			=>$total_per_kota,
					'total_per_area'			=>$total_per_area
				)
			);
	}
	
	$gt_trip			+= $temp_array[$idx]['TotalBerangkat'];
	$gt_pnp_u			+= $temp_array[$idx]['TotalPenumpangU'];
	$gt_pnp_m			+= $temp_array[$idx]['TotalPenumpangM'];
	$gt_pnp_k			+= $temp_array[$idx]['TotalPenumpangK'];
	$gt_pnp_kk			+= $temp_array[$idx]['TotalPenumpangKK'];
	$gt_pnp_g			+= $temp_array[$idx]['TotalPenumpangG'];
	$gt_pnp_o			+= $temp_array[$idx]['TotalPenumpangT'];
	
	$gt_pnp_rb			+= $temp_array[$idx]['TotalPenumpangRB'];
	
	$gt_pnp_r			+= $temp_array[$idx]['TotalPenumpangR'];
	$gt_pnp_vr			+= $temp_array[$idx]['TotalPenumpangVR'];
	$gt_pnp_v			+= $temp_array[$idx]['TotalPenumpangV'];
	$gt_pnp_t			+= $temp_array[$idx]['TotalTiket'];
	$gt_omz_pnp			+= $temp_array[$idx]['TotalPenjualanTiket'];
	$gt_disc			+= $temp_array[$idx]['TotalDiscount'];
	$gt_omzet_net		+= $temp_array[$idx]['Total'];
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tglmulai=".$tanggal_mulai."&tglakhir=".$tanggal_akhir."&cabangasal=".$temp_array[$idx]['KodeCabangAsal'].
										"&kodearea=".$kode_area."&cari=".$cari."&is_today=".$is_today;
													
$script_cetak_excel="Start('laporan_omzet_jurusan_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT
	
$temp_var		= "is_today".($is_today==""?"1":$is_today);
$$temp_var	= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'#laporan_omzet">Home</a> | <a href="'.append_sid('laporan_omzet_jurusan.'.$phpEx).'">Laporan Omzet Jurusan</a>',
	'ACTION_FORM'		=> append_sid('laporan_omzet_jurusan.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'IS_TODAY'			=> $is_today,
	'IS_TODAY1'			=> $is_today1,
	'IS_TODAY0'			=> $is_today0,
	'GT_TRIP'				=> number_format($gt_trip,0,",","."),
	'GT_PNP_U'			=> number_format($gt_pnp_u,0,",","."),
	'GT_PNP_M'      => number_format($gt_pnp_m,0,",","."),			
	'GT_PNP_K'      => number_format($gt_pnp_k,0,",","."),		
	'GT_PNP_KK'     => number_format($gt_pnp_kk,0,",","."),		
	'GT_PNP_G'      => number_format($gt_pnp_g,0,",","."),		
	'GT_PNP_O'      => number_format($gt_pnp_o,0,",","."),
	
	'GT_PNP_RB'      => number_format($gt_pnp_rb,0,",","."),				
	
	'GT_PNP_R'      => number_format($gt_pnp_r,0,",","."),		
	'GT_PNP_VR'     => number_format($gt_pnp_vr,0,",","."),
	'GT_PNP_V'    	=> number_format($gt_pnp_v,0,",","."),		
	'GT_PNP_T'      => number_format($gt_pnp_t,0,",","."),		
	'GT_OMZ'        => number_format($gt_omz_pnp,0,",","."),		
	'GT_DISC'       => number_format($gt_disc,0,",","."),
	'GT_OMZ_NET'    => number_format($gt_omzet_net,0,",",".")
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>