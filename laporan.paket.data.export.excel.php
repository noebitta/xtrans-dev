<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassUser.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_SUPERVISOR_PAKET,$LEVEL_STAFF_KEUANGAN_PAKET))){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal'];
$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir'];
$asal  					= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  				= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

$tgl_awal					= ($tgl_awal!='')?$tgl_awal:dateD_M_Y();
$tgl_akhir				= ($tgl_akhir!='')?$tgl_akhir:dateD_M_Y();
$tgl_awal_mysql		= FormatTglToMySQLDate($tgl_awal);
$tgl_akhir_mysql	= FormatTglToMySQLDate($tgl_akhir);
//end pemgaturan periode

//pengaturan pemilihan tabel pencarian
$tbl_pencarian	= "tbl_paket";
//end pengatran pemilihan tabel pencarian

$template->set_filenames(array('body' => 'laporan.paket/paket.data.tpl'));

//DATA USER
$User   = new User();
$res_user = $User->ambilData("","user_id","ASC");

$users  = array();
while($data_user=$db->sql_fetchrow($res_user)){
  $users[$data_user["user_id"]]=$data_user["nama"];
}

$kondisi	= " WHERE (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND IdJurusan AND CetakTiket=1  ";

$kondisi	.= ($cari=="")?"":" AND (NoTiket LIKE '%$cari%' OR NamaPengirim LIKE '%$cari%' OR AlamatPengirim LIKE '%$cari%' OR TelpPengirim LIKE '%$cari%' OR NamaPenerima LIKE '%$cari%' OR AlamatPenerima LIKE '%$cari%' OR TelpPenerima LIKE '%$cari%' OR KodeJadwal LIKE '$cari%')";

if($asal==""){
  $kondisi	.= "";
}
else{
  $kondisi	.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) LIKE '$asal' ";

  if($tujuan!=""){
    $kondisi	.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) LIKE '$tujuan' ";
  }
}

//DATA PENJUALAN TIKET
$sql =
  "SELECT *
		FROM $tbl_pencarian
		$kondisi
		ORDER BY TglBerangkat,JamBerangkat,KodeJadwal";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Data Paket Periode Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', '#Resi');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Waktu Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Nama Pengirim');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Alamat Pengirim');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Telp Pengirim');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Nama Penerima');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Alamat Penerima');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Telp Penerima');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Berat (Kg)');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Harga (Rp.)');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Diskon (Rp.)');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Bayar (Rp.)');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'Layanan');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Jenis Pembayaran');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);

$idx=0;
$idx_row=4;

while ($row = $db->sql_fetchrow($result_laporan)){

  if($row['NoSPJ']==""){
    $status				= "Belum Dikirim";
  }
  else{

    if($row['IsSampai']==0 && $row['StatusDiambil']==0){
      $status				= "DIKIRIM";
    }
    else{

      if($row['StatusDiambil']==0){
        $status="SAMPAI";
      }
      else{
        $status="DIAMBIL/DITERIMA";
      }
    }
  }

  if($row["FlagBatal"]!=0){
    $status .= " | DIBATALKAN oleh:".$users[$row["PetugasPembatalan"]]." | pada:".dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuPembatalan"]));
  }

	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $status);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NamaPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['AlamatPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, "'".$row['TelpPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['NamaPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['AlamatPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['TelpPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['Berat']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $row['HargaPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['Diskon']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $row['TotalBayar']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $row['Layanan']);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, ($row['JenisPembayaran']==0?"TUNAI":"LANGGANAN"));

	$idx_row++;
	$idx++;
}
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Data Paket Periode Tanggal '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
