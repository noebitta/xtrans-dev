<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage 		= $config['perpage'];

$cari=$HTTP_GET_VARS["cari"];
		
$kondisi	=($cari=="")?""
	:" WHERE KodeKendaraan LIKE '%$cari%'
			OR NoPolisi LIKE '%$cari%'
			OR Merek LIKE '%$cari%'
			OR NoSTNK LIKE '%$cari%'
			OR PerusahaanAsuransi LIKE '%$cari%'
			OR ATPM LIKE '%$cari%'
			OR NoKIR LIKE '%$cari%'
			OR NoKontrak LIKE '%$cari%'
			OR PerusahaanLeasing LIKE '%$cari%'
			OR NoPolis LIKE '%$cari%'
			OR TJH LIKE '%$cari%'
			OR NoPolHitam LIKE '%$cari%'";

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Master Data Kendaraan');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('B3:H3');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'FISIK');
$objPHPExcel->getActiveSheet()->mergeCells('I3:K3');
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'SNTK');
$objPHPExcel->getActiveSheet()->mergeCells('L3:M3');
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'KIR');
$objPHPExcel->getActiveSheet()->mergeCells('N3:S3');
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Leasing');
$objPHPExcel->getActiveSheet()->mergeCells('T3:W3');
$objPHPExcel->getActiveSheet()->setCellValue('T3', 'Asuransi');
$objPHPExcel->getActiveSheet()->mergeCells('X3:Z3');
$objPHPExcel->getActiveSheet()->setCellValue('X3', 'BPKB');

$sql = 
	"SELECT *,(SELECT Nama FROM tbl_md_sopir WHERE KodeSopir=KodeSopir1) AS Sopir
	FROM tbl_md_kendaraan $kondisi 
	ORDER BY KodeKendaraan";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+4;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglSerahTerima'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoRangka']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['NoMesin']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeKendaraan']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Merek']." ".$row['Jenis']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, substr($row['JumlahKursi'],0,2)."-".substr($row['JumlahKursi'],2,1));
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['ATPM']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['NoPolisi']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, dateparse(FormatMySQLDateToTgl($row['STNKExpired'])));
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, dateparse(FormatMySQLDateToTgl($row['PajakExpired'])));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['NoKIR']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, dateparse(FormatMySQLDateToTgl($row['KIRExpired'])));
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['NoKontrak']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $row['PerusahaanLeasing']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $row['HargaOTR']);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $row['AngsuranPerBulan']);
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, $row['AwalAngsuran']);
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglJatuhTempo'])));
	$objPHPExcel->getActiveSheet()->setCellValue('T'.$idx_row, $row['NoPolis']);
	$objPHPExcel->getActiveSheet()->setCellValue('U'.$idx_row, $row['PerusahaanAsuransi']);
	$objPHPExcel->getActiveSheet()->setCellValue('V'.$idx_row, $row['TJH']);
	$objPHPExcel->getActiveSheet()->setCellValue('W'.$idx_row, dateparse(FormatMySQLDateToTgl($row['MasaBerlakuAsuransi'])));
	$objPHPExcel->getActiveSheet()->setCellValue('X'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBPKBDiterima'])));
	$objPHPExcel->getActiveSheet()->setCellValue('Y'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBPKBDilepas'])));
	$objPHPExcel->getActiveSheet()->setCellValue('Z'.$idx_row, $row['NoPolHitam']);
	
}
$temp_idx=$idx_row;

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Master Data Kendaraan.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}

  
?>
