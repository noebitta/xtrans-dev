<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Cabang{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Cabang(){
		$this->ID_FILE="C-CBG";
	}
	
	//BODY
	
	function periksaDuplikasi($kode){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT f_cabang_periksa_duplikasi('$kode') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($kode,$nama,$alamat,$kota,$telp,$fax,$flag_agen,$saldo){
	  
		/*
		ID	: 002
		IS	: data cabang belum ada dalam database
		FS	:Data cabang baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		//$sql = "CALL sp_cabang_tambah ('$kode','$nama','$alamat','$kota','$telp','$fax',$flag_agen)";

		$sql = "INSERT INTO tbl_md_cabang (KodeCabang,Nama, Alamat,Kota,Telp,Fax,FlagAgen,SaldoDropCash)
                VALUES ('$kode','$nama','$alamat','$kota','$telp','$fax',$flag_agen,$saldo);";
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_md_cabang
			WHERE 
				KodeCabang LIKE '$pencari' 
				OR Nama LIKE '%$pencari%' 
				OR Alamat LIKE '%$pencari%'
				OR Telp LIKE '$pencari'
				OR Kota LIKE '$pencari'
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err:". __LINE__);
		}
		
	}//  END ambilData
	
	function ubah($kode_old,$kode,$nama,$alamat,$kota,$telp,$fax,$flag_agen,$saldo){
	  
		/*
		ID	: 004
		IS	: data cabang sudah ada dalam database
		FS	:Data cabang diubah 
		*/
		
		//kamus
		global $db;
		
		//MENGUBAH DATA DI DATABASE
		//$sql ="CALL sp_cabang_ubah ('$kode_old','$kode','$nama','$alamat','$kota','$telp','$fax',$flag_agen)";

        $sql = "UPDATE tbl_md_cabang SET
                 KodeCabang='$kode', Nama='$nama', Alamat='$alamat', Kota='$kota', Telp='$telp',
                 Fax='$fax', FlagAgen=$flag_agen, SaldoDropCash=$saldo
               WHERE KodeCabang='$kode_old';";
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_cabang){
	  
		/*
		ID	: 005
		IS	: data Cabang sudah ada dalam database
		FS	:Data Cabang dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_cabang
			WHERE KodeCabang IN($list_cabang);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($kode=""){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_cabang
			WHERE KodeCabang='$kode';";
		
		if ($result = $db->sql_query($sql,TRUE)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$error	= $db->sql_error();
			die_error("Err:$this->ID_FILE ".__LINE__);
		}
		
	}//  END ambilData
	
	function setComboCabang($kota=""){
		
		/*
		Desc	:Mengembalikan data Cabang sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		if($kota!=""){
			$kondisi_kota	= " WHERE Kota='$kota'";
			$order_by			= " Nama ";
		}
		else{
			$kondisi_kota	= " WHERE 1";
			$order_by			= " Kota,Nama ";
		}
		
		$sql = 
			"SELECT KodeCabang,Nama,Kota
			FROM tbl_md_cabang
			$kondisi_kota AND FlagAgen!=1
			ORDER BY $order_by;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err: $this->ID_FILE". __LINE__);
		}
		
	}//  END ambilData
	
	
	
	function ambilCabang(){
				
		
		//kamus
		global $db;		
		
		$sql = 
			"SELECT *
			FROM tbl_md_cabang";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err:". __LINE__);
		}
		
	}//  END ambilCabang
	
	function setInterfaceComboCabangByKota($kota="",$cabang_dipilih="",$cabang_default=""){
		//SET COMBO cabang
		global $db;
		
		//return "<option>$cabang_dipilih</option>";
		
		if($cabang_default==""){
			$opt_cabang="<option value='' >silahkan pilih...</option>";
			$kondisi_tambahan="";
		}
		else{
			$opt_cabang="silahkan pilih...";
			$kondisi_tambahan=" AND KodeCabang='$cabang_default'";
		}
		
		if($kota==""){
			$kondisi_tambahan .="";
		}
		else{
			$kondisi_tambahan .=" AND Kota='$kota'";
		}
		
		$sql = 
				"SELECT KodeCabang,Nama,Kota
				FROM tbl_md_cabang
				WHERE 1 $kondisi_tambahan
				ORDER BY Kota,Nama;";
					
		if (!$result = $db->sql_query($sql)){
			echo("Err:$this->ID_FILE". __LINE__);
			exit;
		}
			
		if($result){
			
			$kota="";
			
			while ($row = $db->sql_fetchrow($result)){
				$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
				
				if($kota!=$row['Kota']){
						
					if($kota!=""){
						$opt_cabang .= "</optgroup>";
					}
						
					$kota=$row['Kota'];
					$opt_cabang .="<optgroup label='$kota'>";
				}
				
				$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] ($row[KodeCabang])</option>";
			}
		}
		else{
			echo("Error :".__LINE__);exit;
		}		
		return $opt_cabang;
		//END setInterfaceComboCabang
	}
	
	function setInterfaceComboCabang($cabang_dipilih="",$cabang_default=""){
		//SET COMBO cabang
		global $db;
		
		if($cabang_default==""){
			$opt_cabang="<option value='' >silahkan pilih...</option>";
			$kondisi_tambahan="";
		}
		else{
			$opt_cabang="";
			$kondisi_tambahan="WHERE KodeCabang='$cabang_default'";
		}
		
		$sql = 
				"SELECT KodeCabang,Nama,Kota
				FROM tbl_md_cabang
				$kondisi_tambahan
				ORDER BY Kota,Nama;";
					
		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE". __LINE__);
			exit;
		}
			
		if($result){
			
			$kota="";
			
			while ($row = $db->sql_fetchrow($result)){
				$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
				
				if($kota!=$row['Kota']){
						
					if($kota!=""){
						$opt_cabang .= "</optgroup>";
					}
						
					$kota=$row['Kota'];
					$opt_cabang .="<optgroup label='$kota'>";
				}
				
				$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] ($row[KodeCabang])</option>";
			}
		}
		else{
			echo("Error :".__LINE__);exit;
		}		
		return $opt_cabang;
		//END setInterfaceComboCabang
	}
	
	function setInterfaceComboCabangTujuan($cabang_asal="",$cabang_dipilih=""){
		//SET COMBO cabang
		global $db;
		
		$opt_cabang="<option value='' >silahkan pilih...</option>";
		
		if($cabang_asal==""){
			return $opt_cabang;
		}
		
		$sql = 
				"SELECT KodeCabangTujuan,Nama,Kota
				FROM tbl_md_jurusan tmj INNER JOIN tbl_md_cabang tmc ON KodeCabangTujuan=KodeCabang
				WHERE KodeCabangAsal='$cabang_asal'
				ORDER BY Kota,Nama;";
					
		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE". __LINE__);
			exit;
		}
		
		if($result){
			
			$kota="";
			
			while ($row = $db->sql_fetchrow($result)){
				$selected	=($cabang_dipilih!=$row['KodeCabangTujuan'])?"":"selected";
				
				if($kota!=$row['Kota']){
						
					if($kota!=""){
						$opt_cabang .= "</optgroup>";
					}
						
					$kota=$row['Kota'];
					$opt_cabang .="<optgroup label='$kota'>";
				}
				
				$opt_cabang .="<option value='$row[KodeCabangTujuan]' $selected>$row[Nama] ($row[KodeCabangTujuan])</option>";
			}
		}
		else{
			echo("Error :".__LINE__);exit;
		}		
		return $opt_cabang;
		//END setInterfaceComboCabang
	}
	
	function setInterfaceComboCabangAsalByTujuan($cabang_tujuan="",$cabang_dipilih=""){
		//SET COMBO cabang
		global $db;
		
		$opt_cabang="<option value='' >silahkan pilih...</option>";
		
		if($cabang_tujuan==""){
			return $opt_cabang;
		}
		
		$sql = 
				"SELECT KodeCabangAsal,Nama,Kota
				FROM tbl_md_jurusan tmj INNER JOIN tbl_md_cabang tmc ON KodeCabangAsal=KodeCabang
				WHERE KodeCabangTujuan='$cabang_tujuan'
				ORDER BY Kota,Nama;";
					
		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE". __LINE__);
			exit;
		}
		
		if($result){
			
			$kota="";
			
			while ($row = $db->sql_fetchrow($result)){
				$selected	=($cabang_dipilih!=$row['KodeCabangAsal'])?"":"selected";
				
				if($kota!=$row['Kota']){
						
					if($kota!=""){
						$opt_cabang .= "</optgroup>";
					}
						
					$kota=$row['Kota'];
					$opt_cabang .="<optgroup label='$kota'>";
				}
				
				$opt_cabang .="<option value='$row[KodeCabangAsal]' $selected>$row[Nama] ($row[KodeCabangAsal])</option>";
			}
		}
		else{
			echo("Error :".__LINE__);exit;
		}		
		return $opt_cabang;
		//END setInterfaceComboCabang
	}

}
?>