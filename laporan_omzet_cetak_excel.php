<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

//INISIALISASI
$kode_area	= $HTTP_GET_VARS['kodearea'];
$asal				= $HTTP_GET_VARS['asal'];
$tujuan			= $HTTP_GET_VARS['tujuan'];
$bulan			= $HTTP_GET_VARS['bulan']; 
$tahun			= $HTTP_GET_VARS['tahun'];

		
$kondisi_area			= $kode_area==""?"":" AND (SELECT KodeArea FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=tr.IdJurusan)='$kode_area'";

if($asal!=""){
	$kondisi_jurusan	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tr.IdJurusan)='$asal'";
	$kondisi_jurusan	.= $tujuan==""?"" : " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tr.IdJurusan)='$tujuan'";
}

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	//$list_bulan	.="<a href='".append_sid('laporan_omzet_cabang_grafik.'.$phpEx).$parameter."'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
	$list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}

// LIST
$template->set_filenames(array('body' => 'laporan_omzet/laporan_omzet_body.tpl')); 


//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}

//QUERY PENUMPANG TANGGAL HARI INI
if(date("Y-m")==$tahun."-".$bulan){
	$sql=
		"SELECT 
			WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
			IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
			IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
			IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
			IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
			IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,

			IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,

			IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB,
			IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,

			IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
			IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
			IS_NULL(COUNT(NoTiket),0) AS TotalTiket,

			IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
			IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),HargaTiket)),0) AS TotalPenjualanTiket, 
			IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
		FROM tbl_reservasi tr
		WHERE DAY(TglBerangkat)=DAY(NOW()) AND MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_area $kondisi_jurusan
		GROUP BY TglBerangkat";
	
	if ($result_penumpang = $db->sql_query($sql)){
		$data_penumpang_hari_ini = $db->sql_fetchrow($result_penumpang);
	} 
	else{
		//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
		echo("Error:".__LINE__);exit;
	}
}

//QUERY PENUMPANG OLAP
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,
		IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,

		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,Total),HargaTiket)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM tbl_reservasi_olap tr
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	$kondisi_area $kondisi_jurusan
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//EXPORT KE MS-EXCEL

			
$i=1;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Penumpang Area:'.$kode_area.' Jurusan:'.$asal.'-'.$tujuan.' periode '.BulanString($bulan).' '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jumlah Penumpang Umum');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Jumlah Penumpang Mahasiswa');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jumlah Penumpang Khusus');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jumlah Penumpang Keluarga Karyawan');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jumlah Penumpang Gratis');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jumlah Penumpang Online');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jumlah Penumpang Red Bus');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Jumlah Penumpang Return');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Jumlah Penumpang Voucher');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Jumlah Penumpang Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Jumlah Penumpang/Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Pendapatan (Rp.)');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);


$idx=0;

for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$idx++;
	
	$idx_row=$idx+3;
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	//OMZET PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_trip			= $data_penumpang['TotalBerangkat'];
		$total_pnp_u		= $data_penumpang['TotalPenumpangU'];
		$total_pnp_m		= $data_penumpang['TotalPenumpangM'];
		$total_pnp_k		= $data_penumpang['TotalPenumpangK'];
		$total_pnp_kk		= $data_penumpang['TotalPenumpangKK'];
		$total_pnp_g		= $data_penumpang['TotalPenumpangG'];
		$total_pnp_o		= $data_penumpang['TotalPenumpangT'];

		$total_pnp_rb		= $data_penumpang['TotalPenumpangRB'];

		$total_pnp_r		= $data_penumpang['TotalPenumpangR'];
		$total_pnp_vr		= $data_penumpang['TotalPenumpangVR'];
		$total_pnp_t		= $data_penumpang['TotalTiket'];
		$total_pnp_trip		= ($data_penumpang['TotalBerangkat']>0)?$data_penumpang['TotalTiket']/$data_penumpang['TotalBerangkat']:0;
		$total_omz_net		= $data_penumpang['TotalPenjualanTiket']-$data_penumpang['TotalDiscount']-$data_penumpang['TotalKomisiOnline'];
		
		$data_penumpang = $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_trip			= 0;
		$total_pnp_u		= 0;
		$total_pnp_m		= 0;
		$total_pnp_k		= 0;
		$total_pnp_kk		= 0;
		$total_pnp_g		= 0;
		$total_pnp_o		= 0;

		$total_pnp_rb		= 0;

		$total_pnp_r		= 0;
		$total_pnp_vr		= 0;
		$total_pnp_t		= 0;
		$total_pnp_trip		= 0;
		$total_omz_net		= 0;
	}
	
	if(date("Y-m-d")==$tahun."-".$bulan."-".($idx_tgl+1)){
		$total_trip			= $data_penumpang_hari_ini['TotalBerangkat'];
		$total_pnp_u		= $data_penumpang_hari_ini['TotalPenumpangU'];
		$total_pnp_m		= $data_penumpang_hari_ini['TotalPenumpangM'];
		$total_pnp_k		= $data_penumpang_hari_ini['TotalPenumpangK'];
		$total_pnp_kk		= $data_penumpang_hari_ini['TotalPenumpangKK'];
		$total_pnp_g		= $data_penumpang_hari_ini['TotalPenumpangG'];
		$total_pnp_o		= $data_penumpang_hari_ini['TotalPenumpangT'];

		$total_pnp_rb		= $data_penumpang_hari_ini['TotalPenumpangRB'];

		$total_pnp_r		= $data_penumpang_hari_ini['TotalPenumpangR'];
		$total_pnp_vr		= $data_penumpang_hari_ini['TotalPenumpangVR'];
		$total_pnp_t		= $data_penumpang_hari_ini ['TotalTiket'];
		$total_pnp_trip		= ($data_penumpang_hari_ini['TotalBerangkat']>0)?$data_penumpang_hari_ini['TotalTiket']	/$data_penumpang_hari_ini['TotalBerangkat']:0;
		$total_omz_net		= $data_penumpang_hari_ini['TotalPenjualanTiket']-$data_penumpang_hari_ini['TotalDiscount']-$data_penumpang_hari_ini['TotalKomisiOnline'];
	}
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $tgl_transaksi);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, number_format($total_trip,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, number_format($total_pnp_u,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, number_format($total_pnp_m,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($total_pnp_k,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($total_pnp_kk,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($total_pnp_g,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($total_pnp_o,0,"",""));

	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($total_pnp_rb,0,"",""));
	
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($total_pnp_r,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($total_pnp_vr,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($total_pnp_t,0,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($total_pnp_trip,1,"",""));
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($total_omz_net,0,"",""));

	
	$temp_hari++;
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, '=SUM(B4:B'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, '=SUM(C4:C'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D4:D'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(K4:K'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, '=SUM(L4:L'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, '=SUM(M4:M'.$temp_idx.')');

	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet Penumpang Area '.$kode_area.' Jurusan '.$asal.'-'.$tujuan.' periode '.BulanString($bulan).' '.$tahun.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
 
  
  
?>
