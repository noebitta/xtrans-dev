<?php
 //
 // File Konfigurasi  
 //VERSI UPDATE: 29 September 2010 20:14
 //
 
 // PHP
 $phpEx = 'php';
 
 // konfigurasi mysql local
 $dbhost    = 'localhost';
 $dbuser    = 'root'; // user
 $dbpasswd  = '';     // password
 $dbname    = 'tiketux_xtrans';    // nama database*/
 
 // konfigurasi mysql live
// $dbhost    = 'localhost';
// $dbuser    = 'root'; // user
// $dbpasswd  = '';     // password
// $dbname    = 'tiketux_xtrans';    // nama database*/

 
 $dbms = "mysql";
 
 // langguage
 $lang['ENCODING']    = 'iso-8859-1';
 $lang['DIRECTION']   = 'ltr';
 $lang['LEFT']        = 'left';
 $lang['RIGHT']       = 'right';
 $lang['DATE_FORMAT'] = 'd M Y';
 $lang['Title']       = 'Travel Manager';
 $lang['Next']        = ' >';
 $lang['Previous']    = '< ';
 $lang['Goto_page']   = '';
 
 $lang['Error_login'] = 'Invalid Username Or Password Mismatch !!!';
 $lang['Click_return_login'] = 'Click <a href="./index.php">Here</a> To Return To Login';
 
 // Error codes
 define('GENERAL_MESSAGE', 'General');
 define('GENERAL_ERROR', 'Error');
 define('CRITICAL_MESSAGE', 'Critical');
 define('CRITICAL_ERROR', 'Critical'); 

 // public config
 $config['perpage'] = 50; // jumlah item per halaman
 $config['zip'] = FALSE;   // kita lakukan gzip encoding ?
 $config['protocol'] = '';
 $config['name']   = '.';
 $config['script'] = '';	 
 $config['template'] = '';	 
 $config['jadwal'] = '';
 $config['range_show_sisa_kursi'] = 2; //jam
 $config['key_token']  = "indonesiatanahairbeta";
 
	$LIST_KOTA=array(
		"BANDUNG",
		"JAKARTA",
    "BEKASI",
    "TANGERANG",
    "SERANG/CILEGON",
    "PEKALONGAN",
    "SEMARANG",
    "SOLO",
		"YOGYAKARTA");
		
	$LIST_JENIS_PEMBAYARAN=array(
		"TUNAI",
		"DEBIT",
		"KREDIT",
		"VOUCHER RETURN",
		"VOUCHER DISKON");
		
	$LIST_JENIS_BIAYA=array(
		"JASA",
		"TOL",
		"SOPIR",
		"BBM",
		"PARKIR");
	
	$FLAG_BIAYA_JASA	= 0;
	$FLAG_BIAYA_TOL		= 1;
	$FLAG_BIAYA_SOPIR	= 2;
	$FLAG_BIAYA_BBM		= 3;	
	$FLAG_BIAYA_PARKIR  = 4;	
	
	$LEVEL_ADMIN			= 0.0;
	$LEVEL_MANAJEMEN	= 1.0;
	$LEVEL_MANAJER		= 1.2;
	$LEVEL_SUPERVISOR	= 1.3;
	$LEVEL_SUPERVISOR_PAKET	= 1.4;
	$LEVEL_CSO				= 2.0;
	$LEVEL_CSO_PAKET	= 2.1;
	$LEVEL_SCHEDULER	= 3.0;
	$LEVEL_KASIR			= 4.0;
	$LEVEL_KEUANGAN		= 5.0;
	$LEVEL_STAFF_KEUANGAN		= 5.1;
	$LEVEL_STAFF_KEUANGAN_PAKET		= 5.2;
	$LEVEL_CCARE			= 6.0;
	
	$USER_LEVEL= array(
		0.0=>"Admin",
		1.0=>"Manajemen",
		1.2=>"Manajer",
		1.3=>"Supervisor",
		1.4=>"Supervisor Paket",
		2.0=>"CSO",
		2.1=>"CSO Paket",
		3.0=>"Scheduler",
		4.0=>"Kasir",
		5.0=>"Keuangan",
		5.1=>"Staff Keuangan",
		5.2=>"Staff Keuangan Paket",
		6.0=>"Customer Care");
	
	$TOLERANSI_KEBERANGKATAN=125; //menit
	
	$TOP_UP_AWAL=150000; //menit
	$BIAYA_PENDAFTARAN=10000; //menit
	
	//PAGING
	$VIEW_PER_PAGE=50;
	$PAGE_PER_SECTION=7;
	$MAX_DATA_EXPORTED_PER_PAGE=1;
	
	//LAYOUT MAKSIMUM UNTUK TBL_POSISI
	$LAYOUT_MAKSIMUM=17;
	$LAYOUT_KURSI_DEFAULT=7;
	
	//INSENTIF SOPIR
	$INSENTIF_SOPIR_JUMLAH=2500;
	$INSENTIF_SOPIR_JUMLAH_PNP_MINIMUM=6;
	$INSENTIF_SOPIR_LAYOUT_MAKSIMUM=20;
	
	//PENGATURAN MEMBER
	$MINIMUM_KEBERANGKATAN_JADI_MEMBER	= 10;
	$THRESHOLD_MEMBER_EXPIRED	= 90; //hari sebelum expired
	
	//PENGATURAN PAKET
	$LIST_JENIS_PEMBAYARAN_PAKET=array(
		"TUNAI",
		"LANGGANAN",
		"BAYAR DI TUJUAN");
	
	$PAKET_CARA_BAYAR_TUNAI			= 0;
	$PAKET_CARA_BAYAR_LANGGANAN	= 1;
	$PAKET_CARA_BAYAR_DI_TUJUAN	= 2;
	
	$LIST_JENIS_LAYANAN_PAKET=array(
		"P"=>"Platinum",
		"GA"=>"Gold Antar",
		"GD"=>"Gold Diambil",
		"S"=>"Silver",
		"CA"=>"Cargo Antar",
		"CD"=>"Cargo Diambil",
		"M1"=>"Motor Kecil",
		"M2"=>"Motor Besar",
    "I"=>"INTERNAL");
	
	//SESSION TIME
	$SESSION_TIME_EXPIRED	= 600; // Detik
	$JUMLAH_MINIMUM_DISCOUNT_GROUP	= 4;
  
  #KONFIGURASI SMS GATEWAY
	$sms_config['url']				= "http://sms.3trust.com:88/";
	$sms_config['user']				= "tiketux";
	$sms_config['password']		= "Or10n";
	$sms_config['range_reminder']		= 35;
	$HEADER_NO_TELP	= array("02","03","04","05","06","07","08","09");
 
?>