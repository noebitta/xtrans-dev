<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_STAFF_KEUANGAN)))
{
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$is_today  				= $HTTP_GET_VARS['is_today'];
$tanggal_mulai 	 		= $HTTP_GET_VARS['tglmulai'];
$tanggal_akhir 	 		= $HTTP_GET_VARS['tglakhir'];
$kode_cabang  			= $HTTP_GET_VARS['cabangasal'];
$kode_area  			= $HTTP_GET_VARS['kodearea'];
$cari  					= $HTTP_GET_VARS['cari'];
$sort_by				= $HTTP_GET_VARS['sortby'];
$order					= $HTTP_GET_VARS['order'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeJurusan LIKE '$cari%' 
		OR tmc.Nama LIKE '%$cari%' 
		OR tmc2.Nama LIKE '%$cari%')";

/*if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabangAsal='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}*/

$kondisi_area	= $kode_area==""?"":" AND KodeArea='$kode_area'";

$kondisi_cari	.= $kondisi_cabang.$kondisi_area;g;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Jurusan":$sort_by;
		
$sql=
	"SELECT 
		IdJurusan,KodeJurusan,tmc.Nama AS CabangAsal,
		tmc2.Nama AS CabangTujuan,
		KodeCabangAsal,KodeArea,(SELECT Kota FROM tbl_md_cabang WHERE KodeCabang=KodeCabangAsal) AS Kota
		FROM (tbl_md_jurusan tmj LEFT JOIN tbl_md_cabang tmc ON tmj.KodeCabangAsal=tmc.KodeCabang)
		LEFT JOIN tbl_md_cabang tmc2 ON tmj.KodeCabangTujuan=tmc2.KodeCabang
	$kondisi_cari
	ORDER BY KodeArea,Kota,CabangAsal,CabangTujuan";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang= 'M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang= 'K' AND JenisPembayaran!=3 AND Total != 0,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang= 'KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(((JenisPenumpang='G' AND JenisPembayaran!=3) OR (JenisPembayaran!=3 AND Total = 0)),NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangT,

		IS_NULL(COUNT(IF(JenisPenumpang = 'RB' AND JenisPembayaran != 3,NoTiket,NULL)),0) AS TotalPenumpangRB,
		
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,

		IS_NULL(SUM(IF(JenisPenumpang='T' AND JenisPembayaran!=3,Komisi,NULL)),0) AS TotalKomisiOnline,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,SubTotal,0),Total)),0) AS TotalPenjualanTiket,
		IS_NULL(SUM(IF(JenisPenumpang!='R' AND JenisPembayaran!=3,Discount,0)),0) AS TotalDiscount
	FROM $tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2 $kondisi_area
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql))
{
	die(mysql_error());
	echo ("Err: ".__LINE__);
}
while ($row = $db->sql_fetchrow($result))
{
	$data_tiket_total[$row['IdJurusan']] = $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['IdJurusan']				= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']			= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']				= $row['CabangAsal']."->".$row['CabangTujuan'];
	$temp_array[$idx]['KodeCabangAsal']			= $row['KodeCabangAsal'];
	$temp_array[$idx]['KodeArea']				= $row['KodeArea'];
	$temp_array[$idx]['Kota']					= $row['Kota'];
	$temp_array[$idx]['TotalPenumpangU']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangG']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangT']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangT'];

	$temp_array[$idx]['TotalPenumpangRB']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangRB'];

	$temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangVR']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangVR'];
	$temp_array[$idx]['TotalPenumpangV']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangV'];
	$temp_array[$idx]['TotalBerangkat']			= $data_tiket_total[$row['IdJurusan']]['TotalBerangkat'];
	$temp_array[$idx]['TotalTiket']				= $data_tiket_total[$row['IdJurusan']]['TotalTiket'];
	$temp_array[$idx]['TotalKomisiOnline']		= $data_tiket_total[$row['IdJurusan']]['TotalKomisiOnline'];
	$temp_array[$idx]['TotalPenjualanTiket']	= $data_tiket_total[$row['IdJurusan']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['IdJurusan']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenumpangPerTrip']	= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']					= $temp_array[$idx]['TotalPenjualanTiket'] - $temp_array[$idx]['TotalDiscount']- $temp_array[$idx]['TotalKomisiOnline'];
	
	$idx++;
}

$idx=$idx_awal_record;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$idx_ascii=65;
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Penumpang Area '.$kode_area.' Per Jurusan per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Jurusan');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', '#Jurusan');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Umum');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Mahasiswa');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Khusus');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Keluarga Karyawan');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Gratis');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;

$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Tiketux Online');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;

$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Red Bus');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;

$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Return');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Voucher Return');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Voucher');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Penumpang/Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Omzet Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Disc');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;

$idx=0;
$idx_row=4;
$area_terakhir	= "";
$kota_terakhir	= "";
$gt_trip				= 0;
$gt_pnp_u				= 0;
$gt_pnp_m				= 0;
$gt_pnp_k				= 0;
$gt_pnp_kk				= 0;
$gt_pnp_g				= 0;
$gt_pnp_o				= 0;

$gt_pnp_rb				= 0;

$gt_pnp_r				= 0;
$gt_pnp_vr				= 0;
$gt_pnp_v				= 0;
$gt_pnp_t				= 0;
$gt_omz_pnp				= 0;
$gt_disc				= 0;
$gt_omzet_net			= 0;

$jumlah_data	= count($temp_array);

while ($idx<$jumlah_data){
	//MENGKLASIFIKASI BERDASARKAN AREA
	if($area_terakhir!=$temp_array[$idx]['KodeArea'] || $idx==$jumlah_data){
		$area_terakhir						= $temp_array[$idx]['KodeArea'];
		$idx_area									= 1;

		if($idx>0){
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':C'.$idx_row);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, "SUB TOTAL ".$temp_array[$idx-1]['KodeArea']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, number_format($total_trip_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($total_pnp_u_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($total_pnp_m_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($total_pnp_k_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($total_pnp_kk_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($total_pnp_g_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($total_pnp_o_perarea,0,"",""));

			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($total_pnp_rb_perarea,0,"",""));

			$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($total_pnp_r_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($total_pnp_vr_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($total_pnp_v_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($total_pnp_t_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($total_omz_pnp_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($total_disc_perarea,0,"",""));
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($total_omzet_net_perarea,0,"",""));
			
			$idx_row++;
		}
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':Q'.$idx_row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $temp_array[$idx]['KodeArea']);
		$idx_row++;

		$total_trip_perarea				= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perarea			= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perarea			= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perarea			= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perarea			= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perarea			= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perarea			= $temp_array[$idx]['TotalPenumpangT'];
		
		$total_pnp_rb_perarea			= $temp_array[$idx]['TotalPenumpangRB'];

		$total_pnp_r_perarea			= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perarea			= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_v_perarea			= $temp_array[$idx]['TotalPenumpangV'];
		$total_pnp_t_perarea			= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perarea			= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perarea				= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perarea		= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
	}
	else
	{
		$nama_area								= "";
		$idx_area++;
		$total_trip_perarea				+= $temp_array[$idx]['TotalBerangkat'];
		$total_pnp_u_perarea			+= $temp_array[$idx]['TotalPenumpangU'];
		$total_pnp_m_perarea			+= $temp_array[$idx]['TotalPenumpangM'];
		$total_pnp_k_perarea			+= $temp_array[$idx]['TotalPenumpangK'];
		$total_pnp_kk_perarea			+= $temp_array[$idx]['TotalPenumpangKK'];
		$total_pnp_g_perarea			+= $temp_array[$idx]['TotalPenumpangG'];
		$total_pnp_o_perarea			+= $temp_array[$idx]['TotalPenumpangT'];

		$total_pnp_rb_perarea			+= $temp_array[$idx]['TotalPenumpangRB'];

		$total_pnp_r_perarea			+= $temp_array[$idx]['TotalPenumpangR'];
		$total_pnp_vr_perarea			+= $temp_array[$idx]['TotalPenumpangVR'];
		$total_pnp_v_perarea			+= $temp_array[$idx]['TotalPenumpangV'];
		$total_pnp_t_perarea			+= $temp_array[$idx]['TotalTiket'];
		$total_omz_pnp_perarea			+= $temp_array[$idx]['TotalPenjualanTiket'];
		$total_disc_perarea				+= $temp_array[$idx]['TotalDiscount'];
		$total_omzet_net_perarea		+= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
		$total_per_area						= "";
	}
	
	//MENGKLASIFIKASI BERDASARKAN KOTA UNTUK BTK
	if($temp_array[$idx]['KodeArea']=="BTK" || $idx==$jumlah_data){
		if($kota_terakhir!=$temp_array[$idx]['Kota'] || $idx==$jumlah_data){
			$kota_terakhir						= $temp_array[$idx]['Kota'];
			$idx_area									= 1;
	
			if($idx>0 && $temp_array[$idx-1]['KodeArea']!="BDP"){
				$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':C'.$idx_row);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, "SUB TOTAL ".$temp_array[$idx-1]['Kota']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, number_format($total_trip_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($total_pnp_u_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($total_pnp_m_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($total_pnp_k_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($total_pnp_kk_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($total_pnp_g_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($total_pnp_o_perkota,0,"",""));

				$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($total_pnp_rb_perkota,0,"",""));

				$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($total_pnp_r_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($total_pnp_vr_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($total_pnp_v_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($total_pnp_t_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($total_omz_pnp_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($total_disc_perkota,0,"",""));
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($total_omzet_net_perkota,0,"",""));
				
				$idx_row++;
			}
			
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':Q'.$idx_row);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Kota']);
			$idx_row++;
		
			$total_trip_perkota				= $temp_array[$idx]['TotalBerangkat'];
			$total_pnp_u_perkota			= $temp_array[$idx]['TotalPenumpangU'];
			$total_pnp_m_perkota			= $temp_array[$idx]['TotalPenumpangM'];
			$total_pnp_k_perkota			= $temp_array[$idx]['TotalPenumpangK'];
			$total_pnp_kk_perkota			= $temp_array[$idx]['TotalPenumpangKK'];
			$total_pnp_g_perkota			= $temp_array[$idx]['TotalPenumpangG'];
			$total_pnp_o_perkota			= $temp_array[$idx]['TotalPenumpangT'];
			$total_pnp_rb_perkota			= $temp_array[$idx]['TotalPenumpangRB'];
			$total_pnp_r_perkota			= $temp_array[$idx]['TotalPenumpangR'];
			$total_pnp_vr_perkota			= $temp_array[$idx]['TotalPenumpangVR'];
			$total_pnp_v_perkota			= $temp_array[$idx]['TotalPenumpangV'];
			$total_pnp_t_perkota			= $temp_array[$idx]['TotalTiket'];
			$total_omz_pnp_perkota			= $temp_array[$idx]['TotalPenjualanTiket'];
			$total_disc_perkota				= $temp_array[$idx]['TotalDiscount'];
			$total_omzet_net_perkota		= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
		}
		else{
			$nama_kota								= "";
			$total_trip_perkota				+= $temp_array[$idx]['TotalBerangkat'];
			$total_pnp_u_perkota			+= $temp_array[$idx]['TotalPenumpangU'];
			$total_pnp_m_perkota			+= $temp_array[$idx]['TotalPenumpangM'];
			$total_pnp_k_perkota			+= $temp_array[$idx]['TotalPenumpangK'];
			$total_pnp_kk_perkota			+= $temp_array[$idx]['TotalPenumpangKK'];
			$total_pnp_g_perkota			+= $temp_array[$idx]['TotalPenumpangG'];
			$total_pnp_o_perkota			+= $temp_array[$idx]['TotalPenumpangT'];
			$total_pnp_rb_perkota			+= $temp_array[$idx]['TotalPenumpangRB'];
			$total_pnp_r_perkota			+= $temp_array[$idx]['TotalPenumpangR'];
			$total_pnp_vr_perkota			+= $temp_array[$idx]['TotalPenumpangVR'];
			$total_pnp_v_perkota			+= $temp_array[$idx]['TotalPenumpangV'];
			$total_pnp_t_perkota			+= $temp_array[$idx]['TotalTiket'];
			$total_omz_pnp_perkota			+= $temp_array[$idx]['TotalPenjualanTiket'];
			$total_disc_perkota				+= $temp_array[$idx]['TotalDiscount'];
			$total_omzet_net_perkota		+= $temp_array[$idx]['TotalPenjualanTiket']-$temp_array[$idx]['TotalDiscount']-$temp_array[$idx]['TotalKomisiOnline'];
			$total_per_kota					= "";
		}
	}
	
	$idx_ascii=65;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $idx_area);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['Jurusan']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['KodeJurusan']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalBerangkat'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangU'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangM'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangK'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangKK'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangG'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangT'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangRB'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangR'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangVR'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenumpangV'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalTiket'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangPerTrip']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalPenjualanTiket'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['TotalDiscount'],0,"",""));$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, number_format($temp_array[$idx]['Total'],0,"",""));$idx_ascii++;
	
	$gt_trip			+= $temp_array[$idx]['TotalBerangkat'];
	$gt_pnp_u			+= $temp_array[$idx]['TotalPenumpangU'];
	$gt_pnp_m			+= $temp_array[$idx]['TotalPenumpangM'];
	$gt_pnp_k			+= $temp_array[$idx]['TotalPenumpangK'];
	$gt_pnp_kk			+= $temp_array[$idx]['TotalPenumpangKK'];
	$gt_pnp_g			+= $temp_array[$idx]['TotalPenumpangG'];
	$gt_pnp_o			+= $temp_array[$idx]['TotalPenumpangT'];

	$gt_pnp_rb			+= $temp_array[$idx]['TotalPenumpangRB'];
	
	$gt_pnp_r			+= $temp_array[$idx]['TotalPenumpangR'];
	$gt_pnp_vr			+= $temp_array[$idx]['TotalPenumpangVR'];
	$gt_pnp_v			+= $temp_array[$idx]['TotalPenumpangV'];
	$gt_pnp_t			+= $temp_array[$idx]['TotalTiket'];
	$gt_omz_pnp			+= $temp_array[$idx]['TotalPenjualanTiket'];
	$gt_disc			+= $temp_array[$idx]['TotalDiscount'];
	$gt_omzet_net		+= $temp_array[$idx]['Total'];
	
	$idx_row++;
	$idx++;
}

$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, "SUB TOTAL ".$temp_array[$idx-1]['KodeArea']);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, number_format($total_trip_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($total_pnp_u_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($total_pnp_m_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($total_pnp_k_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($total_pnp_kk_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($total_pnp_g_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($total_pnp_o_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($total_pnp_rb_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($total_pnp_r_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($total_pnp_vr_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($total_pnp_v_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($total_pnp_t_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($total_omz_pnp_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($total_disc_perarea,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($total_omzet_net_perarea,0,"",""));
$idx_row++;

$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, "GRAND TOTAL ");
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, number_format($gt_trip,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($gt_pnp_u,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($gt_pnp_m,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($gt_pnp_k,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($gt_pnp_kk,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($gt_pnp_g,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($gt_pnp_o,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($gt_pnp_rb,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($gt_pnp_r,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($gt_pnp_vr,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, number_format($gt_pnp_v,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, number_format($gt_pnp_t,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, number_format($gt_omz_pnp,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, number_format($gt_disc,0,"",""));
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, number_format($gt_omzet_net,0,"",""));

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

//if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet Penumpang Area '.$kode_area.' Per Jurusan per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
//}
  
?>
