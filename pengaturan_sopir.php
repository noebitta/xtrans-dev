<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Sopir	= new Sopir();


	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'sopir/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_sopir.'.$phpEx).'">Sopir</a> | <a href="'.append_sid('pengaturan_sopir.'.$phpEx."?mode=add").'">Tambah Sopir</a> ',
		 'JUDUL'		=>'Tambah Data Sopir',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '0',
		 'PESAN'						=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_SOPIR_ADD_ACT'	=> append_sid('pengaturan_sopir.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah sopir
		$kode_sopir  		= trim(str_replace(" ","",$HTTP_POST_VARS['kode_sopir']));
		$kode_sopir_old	= trim(str_replace(" ","",$HTTP_POST_VARS['kode_sopir_old']));
		$nama   	= $HTTP_POST_VARS['nama'];
		$alamat   = $HTTP_POST_VARS['alamat'];
		$hp				= $HTTP_POST_VARS['hp'];
		$no_sim		= $HTTP_POST_VARS['no_sim'];
		$status_aktif = $HTTP_POST_VARS['aktif'];
		
		$terjadi_error=false;
		
		if($Sopir->periksaDuplikasi($kode_sopir) && $kode_sopir!=$kode_sopir_old){
			$pesan="<font color='white' size=3>Kode sopir yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Sopir";
				$path	='<a href="'.append_sid('pengaturan_sopir.'.$phpEx."?mode=add").'">Tambah Sopir</a> ';
				
				if($Sopir->tambah($kode_sopir,$nama,$hp,$alamat,$no_sim,$status_aktif)){
					
					redirect(append_sid('pengaturan_sopir.'.$phpEx.'?mode=add&pesan=1',true));
					
				}
			}
			else{
				
				$judul="Ubah Data Sopir";
				$path	='<a href="'.append_sid('pengaturan_sopir.'.$phpEx."?mode=edit&id=$kode_sopir_old").'">Ubah Sopir</a> ';
				
				if($Sopir->ubah($kode_sopir_old,$kode_sopir,$nama,$hp,$alamat,$no_sim,$status_aktif)){
					
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
					
				}
			}
			
		}
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'sopir/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_sopir.'.$phpEx).'">Sopir</a> | '.$path,
			 'JUDUL'		=>$judul,
			 'MODE'   	=> 'save',
			 'SUB'    	=> $submode,
			 'KODE_SOPIR_OLD' => $kode_sopir,
			 'KODE_SOPIR'    	=> $kode_sopir,
			 'NAMA'    	=> $nama,
			 'ALAMAT'   => $alamat,
			 'HP'				=> $hp,
			 'NO_SIM'		=> $no_sim,
			 'AKTIF_1'	=> $status_aktif_1,
			 'AKTIF_0'	=> $status_aktif_0,
			 'PESAN'		=> $pesan,
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_SOPIR_ADD_ACT'=>append_sid('pengaturan_sopir.'.$phpEx)
			)
		);
	
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Sopir->ambilDataDetail($id);
		
		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'sopir/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_sopir.'.$phpEx).'">Sopir</a> | <a href="'.append_sid('pengaturan_sopir.'.$phpEx."?mode=edit&id=$id").'">Ubah Sopir</a> ',
			 'JUDUL'		=>'Ubah Data Sopir',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'KODE_SOPIR_OLD'	=> $row['KodeSopir'],
			 'KODE_SOPIR'    	=> $row['KodeSopir'],
			 'NAMA'    	=> $row['Nama'],
			 'ALAMAT'   => $row['Alamat'],
			 'HP'				=> $row['HP'],
			 'NO_SIM'		=> $row['NoSIM'],
			 'AKTIF_1'					=> $status_aktif_1,
			 'AKTIF_0'					=> $status_aktif_0,
			 'U_SOPIR_ADD_ACT'=>append_sid('pengaturan_sopir.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus sopir
		$list_sopir = str_replace("\'","'",$HTTP_GET_VARS['list_sopir']);
		//echo($list_sopir. " asli :".$HTTP_GET_VARS['list_sopir']);
		$Sopir->hapus($list_sopir);
		
		exit;
	} 
	else if ($mode=='ubahstatus'){
		// aksi hapus jadwal
		$kode_sopir = str_replace("\'","'",$HTTP_GET_VARS['kode_sopir']);
	
		$Sopir->ubahStatusAktif($kode_sopir);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'sopir/sopir_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE KodeSopir LIKE '%$cari%' 
				OR Nama LIKE '%$cari%' 
				OR Alamat LIKE '%$cari%' 
				OR HP LIKE '%$cari%'
				OR NoSIM LIKE '%$cari%'";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"KodeSopir","tbl_md_sopir","",$kondisi,"pengaturan_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *
			FROM tbl_md_sopir $kondisi 
			ORDER BY Nama LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['FlagAktif']){
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeSopir]\")'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeSopir]\")'>Tidak Aktif</a>";
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_sopir.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama'=>$row['Nama'],
							'kode_sopir'=>$row['KodeSopir'],
							'alamat'=>$row['Alamat'],
							'hp'=>$row['HP'],
							'no_sim'=>$row['NoSIM'],
							'aktif'	=>$status,
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load sopir',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_sopir.'.$phpEx).'">Sopir</a>',
			'U_SOPIR_ADD'		=> append_sid('pengaturan_sopir.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_sopir.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>