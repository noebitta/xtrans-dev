<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class User{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function User(){
		$this->ID_FILE="C-USR";
	}
	
	//BODY
	
	function periksaDuplikasi($user_name){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT f_user_periksa_duplikasi('$user_name') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($username, $user_password , $NRP,
  $nama, $telp, $hp,
  $email,$address, $kode_cabang,
  $status_online, $berlaku, $user_level,
  $flag_aktif){
	  
		/*
		ID	: 002
		IS	: data user belum ada dalam database
		FS	:Data user baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"CALL sp_user_tambah('$username','".md5($user_password)."', '$NRP',
				'$nama', '$telp', '$hp',
				'$email','$address', '$kode_cabang',
				$status_online, '".FormatTglToMySQLDate($berlaku)."', $user_level,
				$flag_aktif)";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah(
		$user_id,
		$username, $user_password , $nrp,
		$nama, $telp, $hp,
		$email,$address, $kode_cabang,
		$status_online, $berlaku, $user_level,
		$flag_aktif){
	  
		/*
		ID	: 004
		IS	: data kendaraan sudah ada dalam database
		FS	:Data kendaraan diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"CALL sp_user_ubah(
				'$user_id',
				'$username', '$nrp',
				'$nama', '$telp', '$hp',
				'$email','$address', '$kode_cabang',
				$status_online, '".FormatTglToMySQLDate($berlaku)."', $user_level,
				$flag_aktif)";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
		return true;
	}
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_user
			WHERE 
				username LIKE '%$pencari%' 
				OR nama LIKE '%$pencari%' 
				OR address LIKE '%$pencari%'
				OR hp LIKE '$pencari%'
				OR telp LIKE '$pencari%'
				OR email LIKE '%$pencari%'
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Error $sql:".$this->ID_FILE. __LINE__);
		}
		
	}//  END ambilData
	
	function hapus($list_user){
	  
		/*
		ID	: 005
		IS	: data member sudah ada dalam database
		FS	:Data member dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_user
			WHERE user_id IN($list_user);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($user_id){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_user
			WHERE user_id='$user_id';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ubahStatus($user_id){
	  
		/*
		ID	: 008
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_user
			SET 
				user_active=1-IF(user_active IS NULL,0,user_active)
			WHERE user_id='$user_id';";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
	
	function ubahPassword($user_id,$user_password){
	  
		/*
		ID	: 009
		IS	: data user sudah ada dalam database
		FS	: password diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_user
			SET 
				user_password='".md5($user_password)."'
			WHERE user_id='$user_id';";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubah password
	
	function ambilDataDetailByUsername($username){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $LEVEL_SUPERVISOR;
		
		$sql = 
			"SELECT *
			FROM tbl_user
			WHERE username='$username' AND user_level<=$LEVEL_SUPERVISOR;";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilDataDetailByUsernameCSO($username){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $LEVEL_SUPERVISOR;
		
		$sql = 
			"SELECT *
			FROM tbl_user
			WHERE username='$username'";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilNamaById($user_id){
		
		/*
		Desc	:Mengembalikan nama user sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT Nama
			FROM tbl_user
			WHERE user_id='$user_id';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilNamaById
	
	function ambilNamaUser($user_id){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT nama
			FROM tbl_user
			WHERE user_id='$user_id';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilNamaUser
}
?>